<?php

/**
 * Your Email. All Contact messages will be sent there
 */
$your_email = 'info@fundacionaulasmart.org';


/* Do not change any code below this line unless you are sure what you are doing. */
$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

$errors = array();

if ($name == '')
{
	$errors['name'] = 'Por favor, ingrese su nombre!';
}

if ($phone == '')
{
	$errors['phone'] = 'Por favor, introduzca su teléfono!';
}

if ( ! filter_var($email, FILTER_VALIDATE_EMAIL))
{
	$errors['email'] = 'Por favor, introduzca un email válido!';
}
if ($message == '')
{
	$errors['message'] = 'Por favor introduzca el mensaje!';
}

if (count($errors) == 0)
{
	require 'inc/class.phpmailer.php';
	$mail = new PHPMailer;

	$mail->AddAddress($your_email);

	$mail->From = $email;
	$mail->FromName = '';
	$mail->Subject = 'Formulario de contacto de http://'.$_SERVER['HTTP_HOST'].'/';
	$mail->Body = "Name: ".$name."\n"."Email: ".$email."\n"."Phone: ".$phone."\n\n"."Message:\n".$message;

	if($mail->Send()) {
		$response = array ('success' => 1);
		echo json_encode($response);
		exit;

	} else {
		$errors['sending'] = 'Se ha producido un error al enviar su mensaje! Por favor, inténtelo de nuevo más tarde.';

	}

}

$response = array ('success' => 0, 'errors' => $errors);
echo json_encode($response);
exit;
