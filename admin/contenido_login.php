<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head profile="http://gmpg.org/xfn/11">
		<title>AulaInteractiva - Administraci&oacute;n</title>
		
		<base href="<?php echo URL_BASE_ADMIN?>" />
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<meta name="robots" content="noindex,nofollow,noimageindex" />
		        
		<link rel="shortcut icon" href="../archivos/config_web/favicon.ico" />
		
		<link href="../css_default_admin.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="../js_default_admin.js"></script>

		<link href="../css/admin.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		<div id="general">
			<div class='page'>
				<div class='col2'>
					<?php
						$ruta = mvc::obtenerRutaControlador();
						if(isset($ruta))
						{
							mvc::importar($ruta);
						}
						else
						{
							Url::lanzar404();	
						}
					?>
				</div>
			</div>
		</div>
		
		<?php
			if(isset($get['m']) && Usuario::is_login())
			{
				if($get['m'] != 'solicitudes')
				{
					mvc::cargarModuloSoporte('solicitudes');
				}
				$objModelo = new AModeloSolicitudes();
				$solicitudes = $objModelo->obtenerSolicitudes();
				if($solicitudes->num_rows > 0)
				{
					//echo '<div id="modalAlertAdmin">' . $solicitudes->num_rows . ' solicitud/es pendientes</div>';
				}
			}
		?>
		
		<?php if(!empty($logotipo['imagen_fondo_admin'])):?>
			<div id="fondoWebAdmin">
				<img src="../archivos/config_web/<?php echo $logotipo['imagen_fondo_admin'];?>" alt="" />
			</div>
		<?php endif;?>
	
		<?php Alerta::leerMensajeInfo()?>
				
		<script type="text/javascript">
			$(document).ready(function(){
			      setTimeout(function(){ $(".mensajes").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);}, 3000); 
			});
		</script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				// actualiza el hash de la web, menos la de los enlaces que dirigen a archivos con extensiones
				$('a').click(function(){
					var hash = $(this).attr('href');

					var followHash = '1';
					if($(this).attr('data-follow-hash') != undefined)
					{
						followHash = $(this).attr('data-follow-hash');
					}
	
					if(followHash == '1' && hash != "" && hash != "#" && !hash.match(/\.([a-zA-Z]+)$/))
					{
						parent.window.location.hash = '/' + hash;
						//parent.history.back();
					}
				});

				// evento al dar al boton atras del navegador
				// hacer este evento
			});
		</script>
		
	</body>
</html>