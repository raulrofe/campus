<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head profile="http://gmpg.org/xfn/11">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>AulaInteractiva - Administraci&oacute;n</title>     
        <base href="<?php echo URL_BASE_ADMIN?>" />

        <meta name="description" content="FlatApp is a Premium, Responsive and Flat Bootstrap Admin Dashboard Template created by pixelcave and published on Themeforest." />
        <meta name="author" content="Campus aula_interactiva" />
        <meta name="robots" content="noindex,nofollow,noimageindex" />

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="../archivos/config_web/favicon.ico" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57-precomposed.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- The roboto font is included from Google Web Fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic" />

        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="../css/admin_flat/bootstrap.css" />

        <!-- Related styles of various icon packs and javascript plugins -->
        <link rel="stylesheet" href="../css/admin_flat/plugins.css" />

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="../css/admin_flat/main.css" />

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of all the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements (must included last) -->
        <link rel="stylesheet" href="../css/admin_flat/themes.css" />
        <!-- END Stylesheets -->

  		<link rel="stylesheet" href="../css/admin_flat/steps.css" />
  		
  		<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.1/themes/smoothness/jquery-ui.css" />

        <!-- Get Jquery library from Google ... -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <!-- ... but if something goes wrong get Jquery from local file -->
        <script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Modernizr (Browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it) -->
        <script src="js/admin_flat/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
                
        <script src="js/admin_flat/steps.js"></script>        
    </head>
	
	<body>
        <!-- Page Container -->
        <div id="page-container" class="full-width">
        	
        	<?php require_once 'menu_acordeon.php';?>
        
            <!-- Page Content -->
            <div id="page-content">
				<div id="general">
					<div class='page'>
						<div class='col2'>
							<?php
								$ruta = mvc::obtenerRutaControlador();
								if(isset($ruta))
								{
									mvc::importar($ruta);
								}
								else
								{
									Url::lanzar404();	
								}
								
							?>
						</div>
					</div>
				</div>
				
				<?php
					if(isset($get['m']) && Usuario::is_login())
					{
						if($get['m'] != 'solicitudes')
						{
							mvc::cargarModuloSoporte('solicitudes');
						}
						$objModelo = new AModeloSolicitudes();
						$solicitudes = $objModelo->obtenerSolicitudes();
						if($solicitudes->num_rows > 0)
						{
							//echo '<div id="modalAlertAdmin">' . $solicitudes->num_rows . ' solicitud/es pendientes</div>';
						}
					}
				?>
				
				<?php if(!empty($logotipo['imagen_fondo_admin'])):?>
					<div id="fondoWebAdmin">
						<img src="../archivos/config_web/<?php echo $logotipo['imagen_fondo_admin'];?>" alt="" />
					</div>
				<?php endif;?>
			
				<?php Alerta::leerMensajeInfo()?>
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <footer>
                <div class="pull-right">
                <strong>Fundaci&oacute;n AULA_SMART &reg;</strong>
                </div>
                <div class="pull-left">
                    <span id="year-copy"></span> <strong>Campus AULA_INTERACTIVA &copy;</strong>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="icon-chevron-up"></i></a>
     
        <!-- Excanvas for Flot (Charts plugin) support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Bootstrap.js -->
        <script src="js/admin_flat/vendor/bootstrap.min.js"></script>

        <!--
        Include Google Maps API for global use.
        If you don't want to use  Google Maps API globally, just remove this line and the gmaps.js plugin from js/plugins.js (you can put it in a seperate file)
        Then iclude them both in the pages you would like to use the google maps functionality
        -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/admin_flat/plugins.js"></script>
        <script src="js/admin_flat/main.js"></script>
        
        <!-- Javascript code only for this page -->
        <script>
            $(function() {
                var dashChart = $('#dashboard-chart');
                var dashMap = $('#dashboard-map');
                var componentHeight = '300px';

                // Initialize Slimscroll
                $('.scrollable').slimScroll({
                    height: componentHeight,
                    color: '#333',
                    size: '10px',
                    alwaysVisible: true,
                    railVisible: true,
                    railColor: '#333',
                    railOpacity: 0.1
                });
            });
        </script>

<?php //Alerta::leerMensajeInfo(); ?>
				
		<script type="text/javascript">
			$(document).ready(function(){setTimeout(function(){ $(".mensajes").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);}, 3000);});
		</script>	 		
	</body>
</html>
