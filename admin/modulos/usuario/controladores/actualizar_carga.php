<?php

$objUsuario = new AModeloUsuario();

require(PATH_ROOT . 'lib/excel/PHPExcel/IOFactory.php');

$inputFileName = PATH_ROOT . 'cifs.xlsx';

$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$primeraFila = true;

$fila = 0;
$registros = 0;
$log = array();

foreach($sheetData as $cell)
{
	$primeraFila = false;

	if(!$primeraFila)
	{
		//Obtengo los Datos
		$nif			= $cell['C'];
		$cif 			= $cell['D'];

		if(!empty($nif) && !empty($cif))
		{
			$idCentro = null;
			$idAlumno = null;

			//Busco al centro
			if(!empty($cif))
			{
				$centro = $objUsuario->obtenerCentro($cif);
				$centro = mysqli_fetch_object($centro);
				$idCentro = $centro->idcentros;
			}

			//Actualizo el cif
			if(isset($idCentro)){
				$objUsuario->actualizarCifAlumno($nif, $idCentro);
			}
		}
	}
}
