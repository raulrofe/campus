<?php
$objModelo = new AModeloUsuario();

$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'listar')
{
	// Buscamos a los invitados
	$invitados = $objModelo->obtenerInvitados();
	$cursos = $objModelo->obtenerCursos();
	
	$loadView = 'listado_invitado';
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(isset($post['nombrec'], $post['curso'], $post['usuario'], $post['password']))
		{
			if($objModelo->insertarInvitado($post['nombrec'], $post['curso'], $post['usuario'], md5($post['password'])))
			{
				Alerta::guardarMensajeInfo("Usuario a&ntilde;adido");
				Url::redirect('usuarios/invitados');
			}
		}
	}

	//Obtengo los cursos para asignar al invitado a un curso
	$cursos = $objModelo->obtenerCursos();
	
	$loadView = 'nuevo_invitado';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idInvitado']) && is_numeric($get['idInvitado']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			if(isset($post['nombrec'], $post['curso'], $post['usuario'], $post['password']))
			{
				if($objModelo->actualizarInvitado($get['idInvitado'], $post['nombrec'], $post['curso'], $post['usuario'], $post['password']))
				{
					Alerta::guardarMensajeInfo("Usuario editado correctamente");	
					Url::redirect('usuarios/invitados');			
				}
			}
			
		}
		
		//Obtengo los cursos para asignar al invitado a un curso
		$cursos = $objModelo->obtenerCursos();

		//Obtengo los datos del invitado
		$invitado = $objModelo->obtenerUnInvitado($get['idInvitado']);
		$invitado = $invitado->fetch_object();
			
		$loadView = 'editar_invitado';
	}	
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idInvitado']) && is_numeric($get['idInvitado']))
	{
		$invitado = $objModelo->obtenerUnInvitado($get['idInvitado']);
		if($invitado->num_rows == 1)
		{
			if($objModelo->eliminarInvitado($get['idInvitado']))
			{
				Alerta::guardarMensajeInfo('invitado eliminado');
				Url::redirect('usuarios/invitados');
			}
		}
	}	
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'invitados');	


