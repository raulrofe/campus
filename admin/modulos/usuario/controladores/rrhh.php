<?php

$get = Peticion::obtenerGet();
$objModelo = new AModeloUsuario();

if(isset($get['perfil'])) $perfil = $get['perfil'];

$objCurso = new Cursos();

if(isset($perfil))
{
	//Obtengo el idPerfil
	$idPerfil = Usuario::obtenerIdPerfilAdmin($perfil);
	if(isset($get['f']) && $get['f'] == 'listar')
	{
		// listado de rrhh
		$rrhh2 = $objModelo->obtenerRRHH($perfil);

		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'rrhh');
	}

	else if(isset($get['f']) && $get['f'] == 'insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(isset($post['nombrec'], $post['apellido1'], $post['apellido2'], $post['alias'], $post['email'], $post['pass'],
				 	 $post['telefono'], $post['direccion'], $post['cp'], $post['localidad'], $post['provincia'], $post['pais'],
				 	 $post['idPerfil']))
			{
				if(empty($post['telefono'])){$post['telefono'] = 'NULL';}
				if(empty($post['cp'])){$post['cp'] = 'NULL';}

				$nif = null;
				if(isset($post['nif'])) $nif = $post['nif'];

				// SI EXISTE EL ARCHIVO EN PDF CURRICULUM
				if(!empty($_FILES['cv']['tmp_name'])){

					$archivo = $objModelo->guardarCurriculumVitae(strtotime("now"), null);
				}


				if($objModelo->insertarFormacion($nif, $post['nombrec'], $post['apellido1'], $post['apellido2'], $post['alias'],
							   md5($post['pass']), $post['email'], $post['telefono'], $post['direccion'], $post['cp'], $post['provincia'],
							   $post['pais'], $post['idPerfil'], $post['localidad'], $archivo))
				{
					Alerta::guardarMensajeInfo('Usuario a&ntilde;adido');
					Url::redirect('usuarios/' . $get['perfil'] . 'es');
				}
			}
		}

		//Obtener perfiles
		$perfiles = $objModelo->obtenerPerfiles();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'rrhh_nuevo');
	}
	else if(isset($get['f']) && $get['f'] == 'editar')
	{
		if(isset($get['idrrhh']) && is_numeric($get['idrrhh']))
		{
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if(isset($post['nombrec'], $post['apellido1'], $post['apellido2'], $post['alias'], $post['email'], $post['pass'], $post['telefono'], $post['direccion'],
						 $post['cp'], $post['localidad'], $post['provincia'], $post['pais']))
				{

					// SI EXISTE EL ARCHIVO EN PDF CURRICULUM
					if(!empty($_FILES['cv']['tmp_name'])){

						$tutorData = $objModelo->obtenerUnRRHH($get['idrrhh']);
						$tutor = $tutorData->fetch_object();

						if(!is_null($tutor->cv) && file_exists(PATH_ROOT . $tutor->cv)){
							chmod(PATH_ROOT . $tutor->cv, 0777);
							unlink(PATH_ROOT . $tutor->cv);
						}

						$objModelo->guardarCurriculumVitae(strtotime("now"), $get['idrrhh']);
					}


					if(empty($post['telefono'])){ $post['telefono'] = 'NULL'; }
					if(empty($post['cp'])){ $post['cp'] = 'NULL'; }

					$password = null;

					if(!empty($post['pass']) && ($post['pass'] != $post['passwordOld']))
					{
						$password = $post['pass'];
					}

					$nif = null;
					if(isset($post['nif'])) $nif = $post['nif'];

					if($objModelo->actualizarFormacion($nif, $post['idrrhh'], $post['nombrec'], $post['apellido1'], $post['apellido2'],
								   $post['alias'], $password, $post['email'], $post['telefono'], $post['direccion'], $post['cp'],
								   $post['provincia'], $post['pais'], $post['localidad']))
					{
						Alerta::guardarMensajeInfo('Usuario actualizado');
						Url::redirect('usuarios/' . $get['perfil'] . 'es');

					}
				}
			}

			$rrhh = $objModelo->obtenerUnRRHH($get['idrrhh']);

			if($rrhh->num_rows == 1)
			{
				$rrhh = $rrhh->fetch_object();
				require_once mvc::obtenerRutaVista(dirname(__FILE__), 'rrhh_editar');
			}
			else
			{
				Url::redirect('usuarios/' . $get['perfil'] . 'es');
			}

		}
	}
	else if(isset($get['f']) && $get['f'] == 'eliminar')
	{
		if(isset($get['idrrhh']) && is_numeric($get['idrrhh']))
		{
			$rrhh = $objModelo->obtenerUnRRHH($get['idrrhh']);

			if($rrhh->num_rows == 1)
			{
				if($objModelo->eliminarRRHH($get['idrrhh']))
				{
					Alerta::guardarMensajeInfo($perfil.' eliminado');
					Url::redirect('usuarios/' . $get['perfil'] . 'es');
				}
			}
		}
	}
}
