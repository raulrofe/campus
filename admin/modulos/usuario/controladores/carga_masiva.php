<?php

$post = Peticion::obtenerPost();

if(Peticion::isPost())
{
	$log = array();

	// si se ha subido un archivo del modo correcto ...
	if(isset($_FILES['participantesMatricular']['tmp_name']) && !empty($_FILES['participantesMatricular']['tmp_name']) && is_uploaded_file($_FILES['participantesMatricular']['tmp_name']))
	{

		if(preg_match('#(.+)\.xls$#i', $_FILES['participantesMatricular']['name']) || preg_match('#(.+)\.xlsx$#i', $_FILES['participantesMatricular']['name']))
		{
			$objUsuario = new AModeloUsuario();
			$objCurso 	= new Cursos();
			$objModulo  = new Modulos();			

			require(PATH_ROOT . 'lib/excel/PHPExcel/IOFactory.php');

			$inputFileName = $_FILES['participantesMatricular']['tmp_name'];

			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

			$primeraFila = true;

			$fila = 0;
			$registros = 0;
			$log = array();
			
			// Crear conversacion a partir del id curso

			foreach($sheetData as $cell)
			{
				if($cell['A'] == 'NOMBRE' && $cell['B'] == 'APELLIDO 1' && $cell['C'] == 'APELLIDO 2' && $cell['D'] == 'NIF'
					 && $cell['E'] == 'EMAIL' && $cell['F'] == 'TELEFONO' && $cell['G'] == 'ID CURSO' && $cell['H'] == 'CIF'
					 && $cell['I'] == 'RAZÓN SOCIAL')
				{
					$primeraFila = true;
					$fila++;
				}
				else
				{
					$primeraFila = false;
				}

				if(!$primeraFila)
				{
					//Obtengo los Datos
					$nombre 		= $cell['A'];
					$apellido1		= $cell['B'];
					$apellido2		= $cell['C'];
					$apellidos		= $apellido1 . ' ' . $apellido2;
					$nif 			= $cell['D'];
					$email 			= $cell['E'];
					$telefono 		= $cell['F'];
					$idCurso		= $cell['G'];
					$cif 			= $cell['H'];
					$razonSocial	= utf8_encode($cell['I']);

					if(!empty($nombre) && !empty($apellido1) && !empty($nif) && !empty($email) && !empty($idCurso))
					{
						$idCentro = null;
						$idAlumno = null;

						//Busco al centro
						if(!empty($cif))
						{
							$centro = $objUsuario->obtenerCentro($cif);
							if($centro->num_rows == 0)
							{
								//Inserto el centro si no existe existe
								if($objUsuario->insertarCentro($cif, $razonSocial, $razonSocial))
								{
									//Obtengo el ultimo id insertado
									$idCentro = $objUsuario->obtenerUltimoIdInsertado();
								}
							}
							else
							{
								$centro = mysqli_fetch_object($centro);
								$idCentro = $centro->idcentros;
							}
						}

						//Compruebo el NIF del alumno
						$alumno = $objUsuario->obtenerAlumno($nif);
						if($alumno->num_rows == 0)
						{
							//Inserto al alumno previamente genero el usuario y el password

							$checkType = substr($nif, 0, 1);

							if(is_numeric($checkType))
							{
								$usuario = substr($nif, 0, -1);
							}
							else
							{
								$usuario = substr($nif, 1, -1);
							}

							$arraySearch = array(' ', 'á', 'é', 'í', 'ó', 'ú', 'ñ');
							$arrayReplace = array('', 'a', 'e', 'i', 'o', 'u', 'n');

							$apellidoPass = str_replace($arraySearch, $arrayReplace, mb_strtolower($apellido1));
							$pass = md5(trim($apellidoPass));

							if($objUsuario->insertarAlumno($nombre, $apellidos, $apellido1, $apellido2, $nif, $email, $pass, $usuario, $idCentro))
							{
								//Obtengo el ultimo id insertado
								$idAlumno = $objUsuario->obtenerUltimoIdInsertado();
							}
						}
						else
						{
							//El alumno existe actualizo los datos?
							$alumno = mysqli_fetch_object($alumno);
							$idAlumno = $alumno->idalumnos;

							//Actualizo el numero de telefono e email
							$objUsuario->actualizarTelefonoEmailAlumno($idAlumno, $telefono, $email);
						}

						if(isset($idAlumno) && is_numeric($idAlumno))
						{
							//Comprobamos antes si el alumno existe en curso matriculado
							$matricula = $objUsuario->obtenerUnaMatriculaPorAlumnoCurso($idAlumno, $idCurso);
							if($matricula->num_rows == 0)
							{
								//Matriculamos al alumno
								if($objUsuario->insertarMatricula($idAlumno, $idCurso))
								{
									//Obtenmos los datos del curso para insertar los datos necesarios para la web wwww.aulasmarteditorial.com
									$objCurso->set_curso($idCurso);
									$curso = $objCurso->buscar_curso();
									if($curso){
										$curso = mysqli_fetch_object($curso);

										$tituloDesglosado = explode('(', $curso->titulo);
										list($codigoAF, $codigoGrupo) = explode('/', $tituloDesglosado[1]);
										$modulo = $objModulo->buscar_modulo_referencia((int)$codigoAF);
										if($modulo){
											$modulo = mysqli_fetch_object($modulo);	

											$existColaboracion = $objCurso->getAcuerdoColaboracion($idAlumno);

											if($existColaboracion->num_rows == 0){
												//Inserto los registros
												$objCurso->insertAcuerdoColaboracion($idAlumno, $modulo->id_ase, date("d/m/Y", strtotime($curso->f_inicio)), date("d/m/Y", strtotime($curso->f_fin)), date("Y"));
											} else {
												$existColaboracion = mysqli_fetch_object($existColaboracion);
												$objCurso->updateAcurdoColaboracion($existColaboracion->id, date("d/m/Y", strtotime($curso->f_fin)), $modulo->id_ase);
											}
										}
									}

									$registros++;
								}
								else
								{
									$log[] = 'No se pudo matricula al participante en el curso. Linea Excel: ' . $fila;
								}
							}
							else
							{
								$log[] = 'El participante ya se encuentra matriculado en el curso. Linea Excel: ' . $fila;
							}
						}
						else
						{
							$log[] = 'No existe participante para matricular. Linea Excel: ' . $fila;
						}
					}
					else
					{
						$log[] = 'Faltan datos obligatorios (nombre, apellido 1, nif, email e idcurso son datos obligatorios). Linea Excel: ' . $fila;
					}
				}

				$fila++;
				
				
				// Añadir participante a la conversacion
			}
		}
	}
}

$loadView = 'carga_masiva';
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'alumnos');