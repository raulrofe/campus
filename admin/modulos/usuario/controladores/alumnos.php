<?php
$get = Peticion::obtenerGet();
$objModelo = new AModeloUsuario();

mvc::cargarModuloSoporte('cursos');
$objConvocatoria = new Convocatoria();

$objCurso = new Cursos();

if(isset($get['f']) && $get['f'] == 'listar')
{
	// listado de cursos
	$cursos = $objModelo->obtenerCursos();
	$cursos2 = $objModelo->obtenerCursos();

	$loadView = 'listado_alumno';
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();

		if(isset($post['nombre'], $post['apellido1'], $post['apellido2'], $post['email'], $post['f_nacimiento'], $post['lugar_nacimiento'],
		$post['telefono'], $post['telefono2'], $post['direccion'], $post['cp'], $post['localidad'], $post['provincia'], $post['pais'],
		$post['idcentro'], $post['dni'], $post['idsexo'], $post['pas'], $post['user']) && is_numeric($post['idsexo']))
		{
			$nombreUsuario = $post['user'];

			require_once PATH_ROOT . 'modulos/datos_personales/datos_personales.php';
			if(DatosPersonales::validarDatos($post['nombre'], $nombreUsuario, $post['email'], $post['cp'], $post['telefono'], $post['telefono2']))
			{
				// obtiene el centro al que pertence
				$idCentro = null;
				if(is_numeric($post['idcentro']))
				{
					$idCentro = $post['idcentro'];
				}

				// genera la contrasenia
				$pass = md5($post['pas']);

				$alumnoDni = $objModelo->obtenerAlumno($post['dni']);
				if($alumnoDni->num_rows == 0)
				{
					if($objModelo->insertarAlumnoCompleto($post['nombre'], $post['apellido1'], $post['apellido2'], $post['dni'],
						$post['email'], Fecha::invertir_fecha($post['f_nacimiento'], '/'), Fecha::invertir_fecha($post['lugar_nacimiento'], '/'), $post['telefono'],
						$post['telefono2'], $post['direccion'], $post['cp'], $post['provincia'], $post['pais'], $nombreUsuario,
						$pass, $idCentro, $post['localidad'], $post['idsexo']))
					{
						$idAlumno = $objModelo->obtenerUltimoIdInsertado();
						if(isset($post['idcurso']) && is_numeric($post['idcurso']))
						{
							$resultMatricula = $objModelo->obtenerUnaMatriculaPorAlumnoCurso($idAlumno, $post['idcurso']);
							if($resultMatricula->num_rows == 0)
							{
								if($objModelo->insertarMatricula($idAlumno, $post['idcurso']))
								{
									Alerta::guardarMensajeInfo('Se ha insertdo el alumno y se ha matriculado al alumno en el curso');
								}
							}
							else
							{
								$rowMatricula = $resultMatricula->fetch_object();
								if($objModelo->reactivarMatricula($rowMatricula->idmatricula))
								{
									Alerta::guardarMensajeInfo('Se ha insertdo el alumno y se ha matriculado al alumno en el curso');
								}
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('Se ha a&ntilde;adido al alumno, ahora puedes matricularlo');
						}
					}
				}
				else
				{
					$datosAlumno = $alumnoDni->fetch_object();

					if($datosAlumno->borrado == 1)
					{
						if($objModelo->restablecerBorradoLogico($datosAlumno->idalumnos))
						{
							Alerta::guardarMensajeInfo('El alumno ha sido dado de alta nuevamente');
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('El alumno ya existe');
					}
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('Los datos introcucidos no son validos');
			}
		}
	}

	// listado de cursos
	$cursos = $objModelo->obtenerCursosActivos();
	$cursos2 = $objModelo->obtenerCursosActivos();

	$loadView = 'nuevo_alumno';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idalumno']) && is_numeric($get['idalumno']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			$objModelo->actualizarAlumno($get['idalumno'], $post['nombre'], $post['apellido1'], $post['apellido2'], $post['dni'], $post['email'],
										 $post['f_nacimiento'], $post['lugar_nacimiento'], $post['telefono'], $post['telefono2'], $post['direccion'],
										 $post['cp'], $post['localidad'], $post['provincia'], $post['pais'], $post['usuario'], $post['password'], $post['idCentro'],
										 $post['idsexo']);
		}

		$alumnoData	= $objModelo->obtenerUnAlumno($get['idalumno'], true);
		$alum 		= $alumnoData->fetch_array();

		$loadView 	= 'editar_alumno';
	}
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{

}
else if(isset($get['f']) && $get['f'] == 'buscar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();

		if(isset($post['nombre'], $post['apellidos'], $post['dni'], $post['email'], $post['telefono'], $post['idcentro']))
		{
			if(isset($post['estado']) && ($post['estado'] == 'activos' || $post['estado'] == 'borrados'))
			{
				$estadoAlumnos = $post['estado'];
			}
			else
			{
				$estadoAlumnos = 'todos';
			}

			$alumnos = $objModelo->obtenerAlumnos($post['nombre'], $post['apellidos'], $post['dni'], $post['email'], $post['telefono'], $post['idcentro'], $post['idcurso'],
								   $estadoAlumnos);
		}
	}

	$loadView = 'listado_alumno';
}

// listado de centros
$centros = $objModelo->obtenerCentros();
$centros2 = $objModelo->obtenerCentros();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'alumnos');
