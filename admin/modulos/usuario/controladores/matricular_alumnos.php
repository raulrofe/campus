<?php
$get = Peticion::obtenerGet();

$objModelo = new AModeloUsuario();

if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	// matricula a un alumno
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		if(isset($post['idcurso']) && is_numeric($post['idcurso']))
		{
			$resultMatricula = $objModelo->obtenerUnaMatriculaPorAlumnoCurso($get['idalumno'], $post['idcurso']);
			if($resultMatricula->num_rows == 0)
			{
				if($objModelo->insertarMatricula($get['idalumno'], $post['idcurso']))
				{
					Alerta::guardarMensajeInfo('Se ha matriculado al alumno en el curso');
				}
			}
			else
			{
				$rowMatricula = $resultMatricula->fetch_object();
				if($objModelo->reactivarMatricula($rowMatricula->idmatricula))
				{
					Alerta::guardarMensajeInfo('Se ha matriculado al alumno en el curso');
				}
			}
		}
	}
	
	$alumno = $objModelo->obtenerUnAlumno($get['idalumno']);
	$cursosMatriculados = $objModelo->obtenerCursosMatriculadosPorAlumno($get['idalumno']);
	
	// inicializacion
	$cursosMatriculadosArray = array();
	$cursosNoMatriculados = array();
	$cursosArray = array();
	
	// obttiene todos los cursos disponibles
	$cursos = $objModelo->obtenerCursosActivos();
	
	// pasamos los datos de cursos y matriculas de las BBDD a arrays
	if($cursos->num_rows > 0)
	{
		while($curso = $cursos->fetch_object())
		{
			$cursosArray[] = array('idcurso' => $curso->idcurso, 'titulo' => $curso->titulo);
		}
	}
	if($cursosMatriculados->num_rows > 0)
	{
		while($cursoMatriculado = $cursosMatriculados->fetch_object())
		{
			$cursosMatriculadosArray[] = array('idmatricula' => $cursoMatriculado->idmatricula, 'idcurso' => $cursoMatriculado->idcurso, 'titulo' => $cursoMatriculado->titulo);
		}
	}
	
	// obtenemos los cursos en los que no esta matriculado el alumno
	$coincidencia = false;
	foreach($cursosArray as $item)
	{
		foreach($cursosMatriculadosArray as $item2)
		{
			if($item['idcurso'] == $item2['idcurso'])
			{
				$coincidencia = true;
			}
		}
		
		if(!$coincidencia)
		{
			$cursosNoMatriculados[] = $item;
		}
		$coincidencia = false;
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'matricular_alumnos');
}