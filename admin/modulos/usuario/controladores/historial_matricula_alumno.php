<?php
$get = Peticion::obtenerGet();

$objModelo = new AModeloUsuario();

if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	$alumno = $objModelo->obtenerUnAlumno($get['idalumno'], true);
	if($alumno->num_rows == 1)
	{
		$alumno = $alumno->fetch_object();
		
		$cursosMatriculados = $objModelo->obtenerHistorialMatriculadosPorAlumno($get['idalumno']);
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'historial_matricula_alumno');
}