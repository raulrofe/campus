<?php
$get = Peticion::obtenerGet();
if(isset($get['idmatricula']) && is_numeric($get['idmatricula']))
{
	$objModelo = new AModeloUsuario();
		
	$matricula = $objModelo->obtenerUnaMatricula($get['idmatricula']);
	if($matricula->num_rows == 1)
	{
		if(Peticion::isPost())
		{
			$matricula = $matricula->fetch_object();
			
			$post = Peticion::obtenerPost();
			if(isset($post['msg']) && !empty($post['msg']))
			{
				if($objModelo->eliminarMatricula($get['idmatricula'], $post['msg']))
				{
					Alerta::guardarMensajeInfo('Se ha eliminado la matricula del alumno');
					Url::redirect('usuarios/alumno/matriculas/' . $matricula->idalumnos);
				}
			}
		}
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'eliminar_matricula_motivo');
	}
}