<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Usuarios</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es"><?php echo Texto::textoPlano(ucfirst($get['perfil'])) ?>es</a></li>
    <li class="float-right"><a href='usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/nuevo'>Introducir <?php echo Texto::textoPlano($get['perfil'])?></a></li>
    <li class="clear"></li>
</ul>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>INTRODUCIR <?php echo strtoupper($get['perfil']); ?></h5></div>
	<div class="block-content full">
		<form class="form-inline" id="frm_admin_rrhh_nuevo" action='usuarios/<?php echo $get['perfil']?>es/nuevo' method='post' enctype="multipart/form-data">
			<div class="row-fluid">
				<?php if($get['perfil'] == 'tutor'): ?>
					<div class="row-form-content-inline">
						<div class='row-fluid-inline1'>
							<div class='etiquetafrm'>NIF*</div>
							<div class='campofrm'><input type='text' name='nif' maxlength="255" /></div>
						</div>
						<div class='row-fluid-inline2'>
							<div class='etiquetafrm'>Nombre*</div>
							<div class='campofrm'><input type='text' name='nombrec' maxlength="255" /></div>
						</div>
						<div class="clear"></div>
					</div>
				<?php endif; ?>
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Primer apellido*</div>
						<div class='campofrm'><input type='text' name='apellido1' maxlength="255" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Segundo apellido*</div>
						<div class='campofrm'><input type='text' name='apellido2' maxlength="255" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>E-mail<?php if($get['perfil'] != 'supervisor') echo ' *'?></div>
						<div class='campofrm'><input type='text' name='email' maxlength="255" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Tel&eacute;fono</div>
						<div class='campofrm'><input type='text' name='telefono' maxlength="9" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Usuario *</div>
						<div class='campofrm'><input type='text' name='alias' maxlength="255" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Contrase&ntilde;a *</div>
						<div class='campofrm'><input type='password' name='pass' maxlength="60" class="required" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Direcci&oacute;n</div>
						<div class='campofrm'><input type='text' name='direccion'  /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>C&oacute;digo postal</div>
						<div class='campofrm'><input type='text' name='cp' maxlength="5" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Localidad</div>
						<div class='campofrm'><input type='text' name='localidad'  /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Provincia</div>
						<div class='campofrm'><input type='text' name='provincia' /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Pa&iacute;s</div>
						<div class='campofrm'><input type='text' name='pais'  /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Curriculum vitae</div>
						<div class='campofrm'><input type='file' name='cv' /></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>

			<input type="hidden" name="idPerfil" value="<?php echo $idPerfil ?>" />

			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>