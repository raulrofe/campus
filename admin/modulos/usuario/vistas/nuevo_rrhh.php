			<p class='subtitleAdmin'>INTRODUCIR <?php echo strtoupper($get['perfil']); ?></p>
			<div class='formulariosAdmin'>			
				<form id="frm_admin_rrhh_nuevo" action='usuarios/<?php echo $get['perfil']?>es' method='post'>
						<div class='filafrm'>
							<div class='etiquetafrm'>NIF *</div>
							<div class='campofrm'><input type='text' name='nif' size='60'  maxlength="255" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Nombre y apellidos *</div>
							<div class='campofrm'><input type='text' name='nombrec' size='60'  maxlength="255" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Usuario *</div>
							<div class='campofrm'><input type='text' name='alias' size='60'  maxlength="255" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Contrase&ntilde;a *</div>
							<div class='campofrm'><input type="password" name='pass' size='60'  maxlength="60" class="required" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>E-mail<?php if($get['perfil'] != 'supervisor') echo ' *'?></div>
							<div class='campofrm'><input type='text' name='email' size='60' /></div>
						</div>					
						<div class='filafrm'>
							<div class='etiquetafrm'>Tel&eacute;fono</div>
							<div class='campofrm'><input type='text' name='telefono' size='60' maxlength='9' /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Direcci&oacute;n</div>
							<div class='campofrm'><input type='text' name='direccion' size='60' /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>C&oacute;digo postal</div>
							<div class='campofrm'><input type='text' name='cp' size='60' maxlength='5' /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Localidad</div>
							<div class='campofrm'><input type='text' name='localidad' size='60' /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Provincia</div>
							<div class='campofrm'><input type='text' name='provincia' size='60' /></div>
						</div>		
						<div class='filafrm'>
							<div class='etiquetafrm'>Pa&iacute;s</div>
							<div class='campofrm'><input type='text' name='pais' size='60' /></div>
						</div>
						<div class='filafrm'>
							<div class='campofrm'>
								<?php while($perfil = $perfiles->fetch_object()):?>
									<?php if($perfil->perfil == $get['perfil']):?>
										<input type="hidden" name="idperfil" value="<?php echo $perfil->idperfil?>" />
									<?php endif;?>
								<?php endwhile;?>
							</div>
							<span id="admin_formacion_perfil" class="hide"><?php echo Texto::textoPlano($get['perfil'])?></span>
						</div>
						<div class='filafrm'>* Campos obligatorios</div>
						
					<div><input type='submit' value='A&ntilde;adir'></input></div>
					<input type='hidden' name='boton' value='Insertar' />	
				</form>
			</div>