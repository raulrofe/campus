<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Usuarios</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es"><?php echo Texto::textoPlano(ucfirst($get['perfil'])) ?>es</a></li>
    <li class="float-right"><a href='usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/nuevo'>Introducir <?php echo Texto::textoPlano($get['perfil'])?></a></li>
    <li class="clear"></li>
</ul>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR DATOS <?php echo Texto::textoPlano(strtoupper($get['perfil']))?></h5></div>
	<div class="block-content full">
		<form class="form-inline" id="frm_admin_rrhh_nuevo" action='usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/editar/<?php echo Texto::textoPlano($rrhh->idrrhh) ?>' method='post' enctype="multipart/form-data">
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>NIF*</div>
						<div class='campofrm'><input type='text' name='nif' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->nif)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Nombre*</div>
						<div class='campofrm'><input type='text' name='nombrec' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->nombre)?>" /></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Primer apellido*</div>
						<div class='campofrm'><input type='text' name='apellido1' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->apellido1)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Segundo apellido*</div>
						<div class='campofrm'><input type='text' name='apellido2' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->apellido2)?>" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>E-mail<?php if($get['perfil'] != 'supervisor') echo ' *'?></div>
						<div class='campofrm'><input type='text' name='email' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->email)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Tel&eacute;fono</div>
						<div class='campofrm'><input type='text' name='telefono' maxlength="9" value="<?php echo Texto::textoPlano($rrhh->telefono)?>" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Usuario *</div>
						<div class='campofrm'><input type='text' name='alias' maxlength="255" value="<?php echo Texto::textoPlano($rrhh->alias)?>"/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Contrase&ntilde;a *<span style="font-size:0.8em;"> (Escribe la nueva contraseña para actualizarla)</span></div>
						<div class='campofrm'><input type='password' name='pass' maxlength="60" class="required" value="<?php echo $rrhh->pass ?>"/></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Direcci&oacute;n</div>
						<div class='campofrm'><input type='text' name='direccion' value="<?php echo Texto::textoPlano($rrhh->direccion)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>C&oacute;digo postal</div>
						<div class='campofrm'><input type='text' name='cp' maxlength="5" value="<?php echo Texto::textoPlano($rrhh->cp)?>" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Localidad</div>
						<div class='campofrm'><input type='text' name='localidad' value="<?php echo Texto::textoPlano($rrhh->localidad)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Provincia</div>
						<div class='campofrm'><input type='text' name='provincia' value="<?php echo Texto::textoPlano($rrhh->provincia)?>" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Pa&iacute;s</div>
						<div class='campofrm'><input type='text' name='pais' value="<?php echo Texto::textoPlano($rrhh->pais)?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Curriculum vitae</div>
						<div class='campofrm'><input type='file' name='cv' /></div>
					</div>

					<div class="clear"></div>
				</div>
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>&nbsp;</div>
						<div class='campofrm'>&nbsp;</div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Curriculum vitae Adjunto</div>
						<div class='campofrm'>
							<?php if(!is_null($rrhh->cv) && file_exists(PATH_ROOT . $rrhh->cv)): ?>
								<a href="<?php echo '../' . $rrhh->cv ?>" title="" target="_blank">Ver curriculum vitae</a>
							<?php else: ?>
								No se ha adjuntado ningun curriculum
							<?php endif; ?>
						</div>
					</div>

					<div class="clear"></div>
				</div>
			</div>

			<input type='hidden' name='idrrhh' value='<?php echo $rrhh->idrrhh; ?>' />
			<input type='hidden' name='passwordOld' value='<?php echo $rrhh->pass; ?>' />
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Guardar</button></div>
		</form>
	</div>
</div>