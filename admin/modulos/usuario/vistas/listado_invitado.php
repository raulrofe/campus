	<?php if($invitados->num_rows > 0):?>
		<table class="table table-condensed">
			<thead>
	        	<tr>
	            	<th class="span1 text-center"><input type="checkbox"></th>
	                <th class="span1 text-center">Id</th>
	                <th>Nombre</th>
	                <th class="span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
	  		<?php while($invt = $invitados->fetch_object()):?>
	        	<tr>
	            	<td class="span1 text-center"><input type="checkbox" name="checkbox2-2" id="checkbox2-2"></td>
	                <td class="span1 text-center"><?php echo $invt->idrrhh ?></td>
	                <td><a href="usuarios/invitados/editar/<?php echo $invt->idrrhh ?>"><?php echo $invt->nombrec ?></a></td>
	                <td class="span1 text-center">
	                	<div class="btn-group">
	                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="usuarios/invitados/editar/<?php echo $invt->idrrhh ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
	                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="usuarios/invitados/eliminar/<?php echo $invt->idrrhh ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
	                    </div>
	                </td>
	            </tr>
	  		<?php endwhile;?>
  		</table>
  	<?php else: ?>
		<div class="text-center">No existen usuarios creados como invitados</div>
  	<?php endif; ?>