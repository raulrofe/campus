			<p class='subtitleAdmin'>ACTUALIZAR DATOS <?php echo Texto::textoPlano(strtoupper($rrhh->perfil))?></p>
			<div class='formulariosAdmin'>
				<form id="frm_admin_rrhh_nuevo" action='' method='post'>
						<div class='filafrm'>
							<div class='etiquetafrm'>Nombre y apellidos *</div>
							<div class='campofrm'><input type='text' name='nombrec' size='60' value="<?php echo Texto::textoPlano($rrhh->nombrec)?>" maxlength="255" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Usuario *</div>
							<div class='campofrm'><input type='text' name='alias' size='60' value="<?php echo Texto::textoPlano($rrhh->alias)?>" maxlength="255" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Contrase&ntilde;a</div>
							<div class='campofrm'><input type="password" name='pass' size='60' maxlength="60" /></div>
							<div class="clear" style="margin-left: 140px;">Si no pones nada no se actualizar&aacute;</div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>E-mail<?php if($rrhh->perfil != 'supervisor') echo ' *'?></div>
							<div class='campofrm'><input type='text' name='email' size='60' value="<?php echo Texto::textoPlano($rrhh->email)?>" /></div>
						</div>					
						<div class='filafrm'>
							<div class='etiquetafrm'>Tel&eacute;fono</div>
							<div class='campofrm'><input type='text' name='telefono' size='60' maxlength='9' value="<?php echo Texto::textoPlano($rrhh->telefono)?>" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Direcci&oacute;n</div>
							<div class='campofrm'><input type='text' name='direccion' size='60' value="<?php echo Texto::textoPlano($rrhh->direccion)?>" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>C&oacute;digo postal</div>
							<div class='campofrm'><input type='text' name='cp' size='60' maxlength='5' value="<?php echo Texto::textoPlano($rrhh->cp)?>" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Localidad</div>
							<div class='campofrm'><input type='text' name='localidad' size='60' value="<?php echo Texto::textoPlano($rrhh->localidad)?>" /></div>
						</div>
						<div class='filafrm'>
							<div class='etiquetafrm'>Provincia</div>
							<div class='campofrm'><input type='text' name='provincia' size='60' value="<?php echo Texto::textoPlano($rrhh->provincia)?>" /></div>
						</div>		
						<div class='filafrm'>
							<div class='etiquetafrm'>Pa&iacute;s</div>
							<div class='campofrm'><input type='text' name='pais' size='60' value="<?php echo Texto::textoPlano($rrhh->pais)?>" /></div>
						</div>
						<span id="admin_formacion_perfil" class="hide"><?php echo Texto::textoPlano($rrhh->perfil)?></span>
						<div class='filafrm'>* Campos obligatorios</div>
						
					<div><input type='submit' value='Actualizar'></input></div>	
					<input type='hidden' name='boton' value='<?php echo $post['boton']; ?>' />
					<input type='hidden' name='idrrhh' value='<?php echo $post['idrrhh']; ?>' />
				</form>
			</div>