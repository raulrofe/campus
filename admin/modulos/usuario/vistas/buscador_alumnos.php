<div class="block block-themed themed-default">
	<div class="block-title"><h5>BUSCADOR DE ALUMNOS</h5></div>
	<div class="block-content full">
		<form id="frm_busqueda_alumnos" class="form-inline" action='usuarios/alumnos/buscar' method='post'>
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Nombre</div>
						<div class='campofrm'><input type='text' name='nombre'/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Apellidos</div>
						<div class='campofrm'><input type='text' name='apellidos' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>DNI</div>
						<div class='campofrm'><input type='text' name='dni' size='60' maxlength="9" /></div>
					</div>	
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>E-mail</div>
						<div class='campofrm'><input type='text' name='email' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Tel&eacute;fono</div>
						<div class='campofrm'><input type='text' name='telefono' size='60' maxlength='9' /></div>
					</div>	
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Centro</div>
						<div class='campofrm'>
							<select name="idcentro">
								<option value=""> - Seleccione un centro - </option>
								<?php if(isset($centros) && $centros->num_rows > 0):?>
									<?php while($centro = $centros->fetch_object()):?>
										<option value="<?php echo $centro->idcentros?>">
											<?php 
												if(!empty ($centro->nombre_centro)) echo $centro->cif." - ".Texto::textoPlano($centro->nombre_centro);
												else echo $centro->cif." - ".Texto::textoPlano($centro->razon_social);
											?>
										</option>
									<?php endwhile;?>
								<?php endif;?>
							</select>
						</div>
					</div>
					<div class="clear"></div>
				</div>				
	
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Estado</div>
						<?php if(isset($cursos) && $cursos->num_rows > 0):?>
			  				<select name="estado">
			  					<option value="">Todos</option>
			  					<option value="activos">Activos</option>
			  					<option value="borrados">Borrados</option>
			  				</select>
			  			<?php else:?>
			  				<strong>No hay cursos - seleccione una convocatoria</strong>
			  			<?php endif;?>
					</div>	
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Curso</div>
						<?php if(isset($cursos) && $cursos->num_rows > 0):?>
			  				<select name="idcurso">
			  					<option value=""> - Seleccione un curso - </option>
			  					<?php while($curso = $cursos->fetch_object()):?>
			  						<option value="<?php echo $curso->idcurso?>"><?php echo Texto::textoPlano($curso->titulo)?></option>
			  					<?php endwhile;?>
			  				</select>
			  			<?php else:?>
			  				<strong>No hay cursos - seleccione una convocatoria</strong>
			  			<?php endif;?>
					</div>
					<div class="clear"></div>
				</div>			
			</div>
			
            <!-- END div.row-fluid -->
            <div><button class="btn btnFull btn-success" type="submit"><i class="icon-search"></i> Buscar</button></div>

		</form>	
	</div>
</div>
