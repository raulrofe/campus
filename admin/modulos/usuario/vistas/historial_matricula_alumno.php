<div id="admin_usuarios">
	<div class="titular">
		<h1>USUARIOS</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
	
  	<div class="stContainer" id="tabs">  	
	  	<div class="tab">
	  		<div class="submenuAdmin">HISTORIAL DE MATRICULAS</div>	
	  		<?php if(isset($cursosMatriculados)):?>
	  			<h2><?php echo Texto::textoPlano($alumno->nombre . ' ' . $alumno->apellidos)?></h2>
	  			<div class="fleft">
	  				<?php if($cursosMatriculados->num_rows > 0):?>
	  					<ul>
		  					<?php while($item = $cursosMatriculados->fetch_object()):?>
		  						<li>
		  							<div><b>Curso:</b> <?php echo Texto::textoPlano($item->titulo)?></div>
		  							<?php if(isset($item->fechaBaja)):?>
		  								<div><b>Fecha de baja:</b> <?php echo date('d/m/Y', strtotime($item->fechaBaja))?></div>
		  								<div><b>Motivos:</b> <?php echo Texto::textoFormateado($item->motivos)?></div>
		  							<?php endif;?>
		  							<br />
		  						</li>
		  					<?php endwhile;?>
		  				</ul>
	  				<?php else:?>
	  					<strong>No hay matriculas en el historial</strong>
	  				<?php endif;?>
	  			</div>
	  		</div>
	  	<?php endif;?>
	  </div>
	  <div class="clear"></div>
	  <br />
	  <a href="usuarios/alumnos" title="">Volver at&aacute;s</a>
</div>