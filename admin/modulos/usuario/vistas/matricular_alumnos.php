<div id="admin_usuarios">
	<div class="titular">
		<h1>USUARIOS</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
	
  	<div class="stContainer" id="tabs">  	
	  	<div class="tab">
	  		<div class="submenuAdmin">ALUMNOS - MATRICULAS</div>	
	  		<?php if($alumno->num_rows == 1):?>
	  		<?php $alumno = $alumno->fetch_object();?>
	  			<h2><?php echo Texto::textoPlano($alumno->nombre . ' ' . $alumno->apellidos)?></h2>
	  			<div class="fleft" style="width: 50%">
	  				<h3>Cursos matriculados</h3>
	  				<?php if(count($cursosMatriculadosArray) > 0):?>
	  					<ul>
		  					<?php foreach($cursosMatriculadosArray as $item):?>
		  						<li>
		  							<div><?php echo Texto::textoPlano($item['titulo'])?> - <a href="#" <?php echo Alerta::alertConfirmOnClick('eliminarmatricula', '¿Deseas eliminar la matricula de este curso del alumno?', 'usuarios/alumno/matricula/borrar/' . $item['idmatricula']);?> title="Eliminar alumno">Eliminar matricula</a></div>
								</li>
		  					<?php endforeach;?>
		  				</ul>
	  				<?php else:?>
	  					<strong>No esta matriculado en ningun curso</strong>
	  				<?php endif;?>
	  			</div>
	  			<div class="fright" style="width: 50%">
	  				<h3>Nueva matricula</h3>
	  				<?php if(count($cursosNoMatriculados) > 0):?>
	  					<form method="post" action="">
	  						<select name="idcurso">
		  						<?php foreach($cursosNoMatriculados as $item):?>
		  							<option value="<?php echo $item['idcurso']?>"><?php echo Texto::textoPlano($item['titulo'])?></option>
		  						<?php endforeach;?>
		  					</select>
		  					<div class="clear"></div>
		  					<br />
		  					<button type="submit">Matricular</button>
	  					</form>
	  				<?php else:?>
	  					<strong>No hay cursos en los que no este matriculado</strong>
	  				<?php endif;?>
	  			</div>
	  			<div class="clear"></div>
	  		<?php else:?>
	  			<strong>No se encontro al alumno</strong>
	  		<?php endif;?>
	  	</div>
	 	 <div class="clear"></div>
	  	<br /><br />
	  	<a href="usuarios/alumnos" title="">Volver at&aacute;s</a>
	  </div>
</div>