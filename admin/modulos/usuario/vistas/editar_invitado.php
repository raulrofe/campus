<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR DATOS INVITADOS</h5></div>
	<div class="block-content full">
		<form class="form-inline" id="frm_admin_invitado_nuevo" action='usuarios/invitados/editar/<?php echo $get['idInvitado'] ?>' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                            
            	<!-- 1st Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Nombre y apellidos *</label>
                        	<div class="controls"><input type='text' name='nombrec' size='60' value="<?php echo Texto::textoPlano($invitado->nombrec)?>"/></div>
                        </div>
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Nombre de usuario *</label>
	                        <div class="controls"><input type='text' name='usuario' size='60' value="<?php echo Texto::textoPlano($invitado->alias)?>" /></div>
                        </div>
					</div>
                <!-- END 1st Column -->

                <!-- 2nd Column -->
                	<div class="span6">
                    	<div class="control-group">
                        	<label for="columns-select" class="control-label">Curso *</label>
                            <div class="controls">
								<select name='curso'>
									<?php while($curso = $cursos->fetch_object()):?>
										<option value="<?php echo $curso->idcurso?>" <?php if($invitado->idcurso == $curso->idcurso) echo 'selected="selected"'?>"><?php echo Texto::textoPlano($curso->titulo)?></option>
									<?php endwhile;?>
								</select>
	                        </div>
	                    </div>	                                
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Contrase&ntilde;a *</label>
	                        <div class="controls"><input type='password' name='password' size='60' maxlength="10" /></div>
	                        <div class="clear" style="font-size:0.9em;">* La contrase&ntilde;a no se actualizar&aacute; si no se cumplimenta este campo</div>
	                    </div>
	                 </div>	                                
                 <!-- END 2nd Column -->
              </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-save"></i> guardar</button></div>
		</form>
	</div>
</div>
