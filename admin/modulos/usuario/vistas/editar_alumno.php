		<p class='subtitleAdmin'>ACTUALIZAR DATOS ALUMNO</p>
		<div class='formulariosAdmin'>
			<form id="frm_admin_alumno_editar" action='usuarios/alumno/editar/<?php echo $get['idalumno'] ?>' method='post'>
				<img src="../imagenes/fotos/<?php echo $alum['foto']?>" alt="" />

				<div class='filafrm'>
						<div class='etiquetafrm'>Nombre</div>
						<div class='campofrm'><input type='text' name='nombre' size='60'  maxlength="255" value='<?php echo $alum['nombre'];?>' /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Primer apellido</div>
						<div class='campofrm'><input type='text' name='apellido1' size='60' maxlength="255" value='<?php echo $alum['apellido1'];?>' /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Segundo apellido</div>
						<div class='campofrm'><input type='text' name='apellido2' size='60' maxlength="255" value="<?php echo $alum['apellido2'];?>" /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>DNI</div>
						<div class='campofrm'><input type='text' name='dni' size='60' maxlength="9" value='<?php echo $alum['dni'];?>' /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>E-mail</div>
						<div class='campofrm'><input type='text' name='email' size='60' value='<?php echo $alum['email'];?>' /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Usuario</div>
						<div class='campofrm'><input type='text' name='usuario' size='60' value='<?php echo $alum['usuario'];?>'/></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Contrase&ntilde;a</div>
						<div class='campofrm'><input placeholder="******" type='text' name='password' size='60' value=''/></div>
						<div class='obligatorio'>(*)</div>
						<div class="clear"><span class="obligatorio" style="margin-left:140px;">Si no se rellena no se actualizara este campo</span></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Fecha nacimiento</div>
						<div class='campofrm'><input placeholder="dd/mm/aaaa" type='text' name='f_nacimiento' size='60' maxlength="10" value='<?php if($alum['f_nacimiento'] != '0000-00-00') echo date('d/m/Y', strtotime($alum['f_nacimiento']));?>' /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Lugar nacimiento</div>
						<div class='campofrm'><input type='text' name='lugar_nacimiento' size='60' value='<?php echo $alum['lugar_nacimiento'];?>' /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Sexo</div>
						<div class='campofrm'>
							<select name="idsexo">
								<option value="1" <?php if($alum['idsexo'] == 1) echo 'selected="selected"'?>>Hombre</option>
								<option value="2" <?php if($alum['idsexo'] == 2) echo 'selected="selected"'?>>Mujer</option>
							</select>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Tel&eacute;fono</div>
						<div class='campofrm'><input type='text' name='telefono' size='60' maxlength='9' value='<?php if(!empty($alum['telefono'])) echo $alum['telefono'];?>' /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Tel&eacute;fono secundario</div>
						<div class='campofrm'><input type='text' name='telefono2' size='60' maxlength='9' value='<?php if(!empty($alum['telefono_secundario'])) echo $alum['telefono_secundario'];?>' /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Direcci&oacute;n</div>
						<div class='campofrm'><input type='text' name='direccion' size='60' value='<?php echo $alum['direccion'];?>'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>C&oacute;digo postal</div>
						<div class='campofrm'><input type='text' name='cp' size='60' maxlength='5' value='<?php echo $alum['cp'];?>'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Localidad</div>
						<div class='campofrm'><input type='text' name='localidad' size='60' value='<?php echo $alum['poblacion'];?>'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Provincia</div>
						<div class='campofrm'><input type='text' name='provincia' size='60' value='<?php echo $alum['provincia'];?>'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Pa&iacute;s</div>
						<div class='campofrm'><input type='text' name='pais' size='60' value='<?php echo $alum['pais'];?>'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Centro</div>
						<div class='campofrm'>
							<select name="idcentro">
								<option value="">Sin determinar</option>
								<?php if($centros->num_rows > 0):?>
									<?php while($cen = $centros2->fetch_object()):?>
										<option value="<?php echo $cen->idcentros?>" <?php if($cen->idcentros == $alum['idcentros']) echo 'selected'?>><?php echo Texto::textoPlano($cen->razon_social)?>
									<?php endwhile;?>
								<?php endif;?>
							</select>
						</div>
					</div>
					<div class='filafrm'>* Campos obligatorios</div>

				<div><input type='submit' value='Guardar'></input></div>
			<input type='hidden' name='idalumno' value='<?php echo $alum['idalumnos']; ?>' />
			</form>
		</div>