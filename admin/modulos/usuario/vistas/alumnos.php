<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Usuarios</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="usuarios/invitados">Alumnos</a></li>
    <li class="float-right">&nbsp;&nbsp;&nbsp;&nbsp;<a href='usuarios/alumnos/carga-masiva'>Carga masiva</a></li>
    <li class="float-right"><a href='usuarios/alumnos/nuevo'>Introducir alumno</a></li>
    <li class="clear"></li>
</ul>

<div id="admin_usuarios">

	
  	<div class="stContainer" id="tabs">	  	
		<div class="tab">			
			<!-- Formulario de busqueda de alumno -->
			<?php require_once('buscador_alumnos.php'); ?>
					
			<!-- Vista dinámica para el alumno -->
	  		<div class="tab"><?php require_once mvc::obtenerRutaVista(dirname(__FILE__), $loadView); ?></div>
	  	</div>
	</div>
</div>