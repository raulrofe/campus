<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Usuarios</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="usuarios/invitados">Invitados</a></li>
    <li class="float-right"><a href='usuarios/invitados/nuevo'>Introducir invitado</a></li>
    <li class="clear"></li>
</ul>

<div class="stContainer" id="tabs">
  	<!-- Navegacion de las pestañas -->	  
  	<h5><?php if(isset($f['nombre'])) echo $f['nombre']?></h5>			
  	<div class="tab"><?php require_once mvc::obtenerRutaVista(dirname(__FILE__), $loadView); ?></div>
</div>


<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar a este invitado?');</script>