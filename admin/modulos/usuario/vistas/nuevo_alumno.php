<div class="block block-themed themed-default">
	<div class="block-title"><h5>INTRODUCIR ALUMNO</h5></div>
	<div class="block-content full">
		<form class="form-inline" id="frm_admin_alumno_nuevo" action='usuarios/alumnos/nuevo' enctype="multipart/form-data" method='post'>
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>DNI</div>
						<div class='campofrm'><input type='text' name='dni' size='60' maxlength="9" onchange='MyAdminUsuarios.obtenerUsuario();'/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Nombre</div>
						<div class='campofrm'><input type='text' name='nombre' size='60'  maxlength="255" onchange='MyAdminUsuarios.obtenerContrasena();'/></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Primer apellido</div>
						<div class='campofrm'><input type='text' name='apellido1' size='60' maxlength="255" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Segundo apellido</div>	
						<div class='campofrm'><input type='text' name='apellido2' size='60' maxlength="255" /></div>
					</div>
					<div class="clear"></div>
				</div>
	
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Fecha nacimiento</div>
						<div class='campofrm'><input type='text' name='f_nacimiento' size='60' maxlength="10" placeholder="dd/mm/aaaa" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Lugar nacimiento</div>
						<div class='campofrm'><input type='text' name='lugar_nacimiento' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
	
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>E-mail</div>
						<div class='campofrm'><input type='text' name='email' size='60' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Curso</div>
						<?php if(isset($cursos2) && $cursos2->num_rows > 0):?>
			  				<select name="idcurso">
			  					<option value=""> - Seleccione un curso - </option>
			  					<?php while($curse = $cursos2->fetch_object()):?>
			  						<option value="<?php echo $curse->idcurso?>"><?php echo Texto::textoPlano($curse->titulo)?></option>
			  					<?php endwhile;?>
			  				</select>
			  			<?php else:?>
			  				<strong>No hay cursos - seleccione una convocatoria</strong>
			  			<?php endif;?>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Usuario</div>	
						<div class='campofrm'><input type='text' name='user' size='60'/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Contrase&ntilde;a</div>
						<div class='campofrm'><input type='password' name='pas' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Tel&eacute;fono</div>
						<div class='campofrm'><input type='text' name='telefono' size='60' maxlength='9' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Tel&eacute;fono secundario</div>
						<div class='campofrm'><input type='text' name='telefono2' size='60' maxlength='9' /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Direcci&oacute;n</div>
						<div class='campofrm'><input type='text' name='direccion' size='60' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>C&oacute;digo postal</div>
						<div class='campofrm'><input type='text' name='cp' size='60' maxlength='5' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Localidad</div>
						<div class='campofrm'><input type='text' name='localidad' size='60' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Provincia</div>
						<div class='campofrm'><input type='text' name='provincia' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
	
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Pa&iacute;s</div>
						<div class='campofrm'><input type='text' name='pais' size='60' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Sexo</div>
						<div class='campofrm'>
							<select name="idsexo">
								<option value="1">Hombre</option>
								<option value="2">Mujer</option>
							</select>
						</div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>CIF</div>
						<div class='campofrm'><input type='text' name='cifCentro' size='60' id="cifDetected" maxlength="9"/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Nombre centro</div>
						<div class='campofrm'><input type='text' name='nombreCentro' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Centro</div>
						<div class='campofrm'>
							<select name="idcentro">
								<option value=""> - Sin determinar - </option>
								<?php if($centros->num_rows > 0):?>
									<?php while($ctr = $centros2->fetch_object()):?>
										<option value="<?php echo $ctr->idcentros?>">
											<?php 
												if(!empty ($ctr->nombre_centro)) echo $ctr->cif.' - '.Texto::textoPlano($ctr->nombre_centro);
												else echo $ctr->cif.' - '.Texto::textoPlano($ctr->razon_social);
											?>
										</option>
									<?php endwhile;?>
								<?php endif;?>
							</select>
						</div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">

					<div class="clear"></div>
				</div>
				
				<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('input[name="cifCentro"]').keyup(function()
	{
		if($('input[name="cifCentro"]').val().length == 9)
		{
			alert('insert CIF');
		}
	});

	MyAdminUsuarios = {};

	MyAdminUsuarios.obtenerUsuario = function()
	{
		var name = '';
		var dni = $('.formulariosAdmin form input[name="dni"]').val();

		if(dni.length == 9)
		{
			name = dni.substr(0, 8);
		}
		else
		{
			name = '';
		}
		
		$('.formulariosAdmin form input[name="user"]').val(name);
		MyAdminUsuarios.obtenerContrasena();
	};

	MyAdminUsuarios.obtenerContrasena = function()
	{
		var pass = $('.formulariosAdmin form input[name="apellido1"]').val().replace( /\s+/g, '' );
		var dni = $('.formulariosAdmin form input[name="dni"]').val();

		if(dni.length == 9)
		{
			pass += dni.substr(8, 9);
			$('.formulariosAdmin form input[name="pas"]').val(pass.toLowerCase());
		}
		else
		{
			$('.formulariosAdmin form input[name="pas"]').val('');
		}
				
	};
</script>