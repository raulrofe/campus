<?php if(isset($alumnos)):?>
  <div id="admin_usuarios_listado">
    <h2>Listado Alumnos</h2>
    <?php if($alumnos->num_rows > 0):?>
      <?php while($alumno = $alumnos->fetch_object()):?>
        <div class="elemento">
          <div class="elementoContenido fleft" style="width:50%">
            <?php echo $alumno->nombre . ' ' . $alumno->apellidos ?>
            <?php if(!empty($post['idcurso']) && $alumno->desmatriculado == 1) echo '<b>(Desmatriculado)</b>'?>
          </div>
          <div class="elementoPie fright" style="width:50%">
            <a href="usuarios/alumno/matriculas/historial/<?php echo $alumno->idalumnos?>" title="Historial de matriculas del alumno">Historial</a>
            <?php if($alumno->borrado == '1'):?>
              <a href="#" onclick="alumnosMostrarMotivosBorrados(this); return false;" title="">Motivos</a>
            <?php else:?>
              <a href="usuarios/alumno/matriculas/<?php echo $alumno->idalumnos?>" title="Gestionar matriculas del alumno">Matriculas</a>
              <a href="usuarios/alumno/editar/<?php echo $alumno->idalumnos?>" title="Editar alumno"><img src="imagenes/editar2.png" alt="Editar" /></a>
              <a href="usuarios/alumno/borrar/<?php echo $alumno->idalumnos;?>" title="Eliminar alumno"><img src="imagenes/delete2.png" alt="Eliminar" /></a>
            <?php endif;?>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <?php if($alumno->borrado == '1'):?>
            <div class="elementoBorradoMotivos cursiva">
              Motivos de borrado: <?php echo Texto::textoFormateado($alumno->motivo_borrado)?>
            </div>
          <?php endif;?>
        </div>
      <?php endwhile;?>
    <?php else:?>
      <strong>No se encontraron alumnos para esta b&uacute;squeda</strong>
    <?php endif;?>
  </div>
<?php endif;?>
