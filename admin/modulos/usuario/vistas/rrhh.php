<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Usuarios</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es"><?php echo Texto::textoPlano(ucfirst($get['perfil'])) ?>es</a></li>
    <li class="float-right"><a href='usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/nuevo'>Introducir <?php echo Texto::textoPlano($get['perfil'])?></a></li>
    <li class="clear"></li>
</ul>


<table class="table table-hover">
	<thead>
        <tr>
            <th class="span1 text-center">Id</th>
            <th>NIF</th>
            <th>Nombre</th>
            <th class="span1 text-center"><i class="icon-bolt"></i></th>
		</tr>
	</thead>
  	<?php while($item = $rrhh2->fetch_object()):?>
       	<tr>
            <td class="span1 text-center"><?php echo $item->idrrhh ?></td>
            <td><?php echo $item->nif ?></td>
            <td><a href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/editar/<?php echo $item->idrrhh ?>"><?php echo Texto::textoPlano($item->nombrec) ?></a></td>
            <td class="span1 text-center">
	          	<div class="btn-group">
	    	      	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/editar/<?php echo $item->idrrhh ?>" data-original-title="Editar"><i class="icon-pencil"></i></a>
	                <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="usuarios/<?php echo Texto::textoPlano($get['perfil']) ?>es/eliminar/<?php echo $item->idrrhh ?>" data-original-title="Eliminar"><i class="icon-remove"></i></a>
	            </div>
          	</td>
        </tr>
  	<?php endwhile;?>
</table>