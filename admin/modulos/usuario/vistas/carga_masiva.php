<div>
	<a href="../archivos/plantilla_matriculacion.xlsx" title="">Descargar plantilla Excel</a><br/>
	<a href="cursos/listado-ids" target="_blank" title="">Ver ids curso</a>
</div>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>CARGA MASIVA</h5></div>
	<div class="block-content full">
		<form id="frm_carga_masiva_alumnos" class="form-inline" action="usuarios/alumno/carga-masiva" enctype="multipart/form-data" method='post'>
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Archivo de carga masiva</div>
						<input type="file" name="participantesMatricular" />
					</div>
					<div class="clear"></div>
				</div>
				<div><button class="btn btnFull btn-success" type="submit"><i class="icon-tasks"></i> Enviar</button></div>
			</div>
		</form>
	</div>
</div>

<!-- Log en caso de que haya errores -->

<?php 
	if(isset($fila))
	{
		if(empty($log) && $fila == 1)
		{
			echo 'El archivo Excel est&aacute; vac&iacute;o';
		}
		else if(empty($log) && $fila > 1)
		{
			echo 'Registros Procesados: ' . $fila . '<br/>';
			echo 'Registros correctos: ' . $registros . '<br/>';
			echo 'Registros Incorrectos: ' . $fila - $registros . '<br/>';
		}
		else if(!empty($log))
		{
			echo 'Registros Procesados: ' . $fila . '<br/>';
			echo 'Registros correctos: ' . $registros . '<br/>';
			echo 'Registros Incorrectos: ' . $fila - $registros . '<br/>';
			var_dump($log);			
		}
	}
?>

