<?php
class AModeloUsuario extends modeloExtend
{
	public function obtenerInvitados()
	{
		$sql = 'SELECT *, rh.idrrhh FROM rrhh AS rh LEFT JOIN staff AS st on st.idrrhh = rh.idrrhh WHERE rh.idperfil = 6 AND rh.borrado = 0 GROUP BY rh.idrrhh';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnInvitado($idinvitado)
	{
		$sql = 'SELECT *, rh.idrrhh FROM rrhh AS rh LEFT JOIN staff AS st on st.idrrhh = rh.idrrhh WHERE rh.idperfil = 6 AND rh.idrrhh = ' . $idinvitado . ' AND rh.borrado = 0 GROUP BY rh.idrrhh';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnaMatricula($idMatricula)
	{
		$sql = 'SELECT m.idmatricula, m.idalumnos FROM matricula AS m WHERE m.borrado = 0 AND m.idmatricula = ' . $idMatricula;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnaMatriculaPorAlumnoCurso($idAlumno, $idCurso)
	{
		// no poner aqui el borrado = 0
		$sql = 'SELECT m.idmatricula FROM matricula AS m WHERE m.idalumnos = ' . $idAlumno . ' AND m.idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function reactivarMatricula($idMatricula)
	{
		$sql = 'UPDATE matricula SET borrado = 0 WHERE idmatricula = ' . $idMatricula;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarMatricula($idMatricula)
	{
		$sql = 'UPDATE matricula SET borrado = 1, motivos = "' . $motivos . '", fechaBaja = "' . date('Y-m-d H:i:s') . '" WHERE idmatricula = ' . $idMatricula;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCursos()
	{
		$sql = 'SELECT c.idcurso, c.titulo' .
		' FROM curso AS c';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCursosActivos()
	{
		$sql = 'SELECT c.idcurso, c.titulo' .
		' FROM curso AS c '.
		'WHERE borrado = 0';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnos($nombre = '', $apellidos = '', $dni = '', $email = '', $telefono = '', $idcentro = '', $idcurso = '', $estado = 'activos')
	{
		preg_quote($nombre, '-');
		$nombre = $nombre;

		preg_quote($apellidos, '-');
		$apellidos = $apellidos;

		preg_quote($dni, '-');
		$dni = $dni;

		preg_quote($email, '-');
		$email = $email;

		preg_quote($telefono, '-');
		$telefono = $telefono;

		$addSql1 = null;
		if(!empty($idcurso))
		{
			$addSql1 = ' AND m.idcurso = ' . $idcurso;
		}
		$addSql2 = null;
		if(!empty($idcentro))
		{
			$addSql2 = ' AND al.idcentros = ' . $idcentro;
		}

		$addSql4 = '';
		if($estado == 'activos')
		{
			$addSql4 = 'al.borrado = 0 AND ';
		}
		else if($estado == 'borrados')
		{
			$addSql4 = 'al.borrado = 1 AND ';
		}

		$sql = 'SELECT al.idalumnos, al.nombre, al.apellidos, al.dni, al.foto, al.email, m.borrado AS desmatriculado, al.borrado, al.motivo_borrado' .
		' FROM alumnos AS al' .
		' LEFT JOIN matricula AS m ON al.idalumnos = m.idalumnos' .
		' LEFT JOIN curso AS c ON c.idcurso = m.idcurso' .
		//' LEFT JOIN centros AS ct ON m.idcentros = ct.idcentros' .
		' WHERE ' . $addSql4 . 'al.nombre LIKE "%' . $nombre . '%" AND al.apellidos LIKE "%' . $apellidos . '%"' .
		' AND al.dni LIKE "%' . $dni . '%" AND al.email LIKE "%' . $email . '%"' . $addSql1 . $addSql2 .
		' GROUP BY al.idalumnos ORDER BY al.nombre ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnAlumno($idAlumno, $incluirBorrados = false)
	{
		$addSql = null;

		if(!$incluirBorrados)
		{
			$addSql = ' borrado = 0 AND';
		}

		$sql = 'SELECT * FROM alumnos WHERE' . $addSql . ' idalumnos = ' . $idAlumno . $addSql;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnoPorEmail($email, $idconv)
	{
		$sql = 'SELECT al.email, al.dni, ct.cif, al.nombre, al.apellidos FROM alumnos AS al' .
		' LEFT JOIN matricula AS m ON m.idalumnos = al.idalumnos' .
		//' LEFT JOIN curso AS c ON c.idconvocatoria = ' . $idconv .
		' LEFT JOIN centros_convocatorias AS cv ON al.idcentros = cv.idcentro' .
		' LEFT JOIN centros AS ct ON ct.idcentros = cv.idcentro' .
		' WHERE al.borrado = 0 AND al.email = "' . $email . '" AND cv.idconvocatoria = ' . $idconv .
		' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnoPorConv($idconv)
	{
		$sql = 'SELECT al.idalumnos, al.email, al.dni, ct.cif, al.nombre, al.apellidos FROM alumnos AS al' .
		' LEFT JOIN matricula AS m ON m.idalumnos = al.idalumnos' .
		//' LEFT JOIN curso AS c ON c.idconvocatoria = ' . $idconv .
		' LEFT JOIN centros_convocatorias AS cv ON al.idcentros = cv.idcentro' .
		' LEFT JOIN centros AS ct ON ct.idcentros = cv.idcentro' .
		' WHERE al.borrado = 0 AND cv.idconvocatoria = ' . $idconv .
		' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarAlumno($idAlumno, $msg)
	{
		$sql = 'UPDATE alumnos SET borrado = 1, motivo_borrado = "' . $msg . '" WHERE idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarInvitado($idinvitado)
	{
		$sql = 'UPDATE rrhh SET borrado = 1 WHERE idrrhh = ' . $idinvitado;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCursosMatriculadosPorAlumno($idAlumno)
	{
		$sql = 'SELECT m.idmatricula, c.idcurso, c.idaccion_formativa, c.titulo, c.metodologia, cv.nombre_convocatoria' .
		' FROM matricula AS m LEFT JOIN curso AS c ON c.idcurso = m.idcurso' .
		' LEFT JOIN convocatoria AS cv ON cv.idconvocatoria = c.idconvocatoria' .
		' WHERE m.borrado = 0 AND m.idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerHistorialMatriculadosPorAlumno($idAlumno)
	{
		$sql = 'SELECT m.idmatricula, c.idcurso, c.idaccion_formativa, c.titulo, c.metodologia, cv.nombre_convocatoria, m.motivos, m.fechaBaja' .
		' FROM matricula AS m LEFT JOIN curso AS c ON c.idcurso = m.idcurso' .
		' LEFT JOIN convocatoria AS cv ON cv.idconvocatoria = c.idconvocatoria' .
		' WHERE m.idalumnos = ' . $idAlumno . ' GROUP BY m.idmatricula';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerRRHH($perfil)
	{
		$sql = 'SELECT rh.idrrhh, rh.nombrec, rh.nif
		FROM rrhh AS rh
		LEFT JOIN perfil AS pf ON pf.idperfil = rh.idperfil
		WHERE rh.borrado = 0 AND pf.perfil = "' . $perfil . '"';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnRRHH($idrrhh)
	{
		$sql = 'SELECT * FROM rrhh AS rh LEFT JOIN perfil AS pf ON pf.idperfil = rh.idperfil WHERE rh.borrado = 0 AND rh.idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTutorPorNif($nif)
	{
		$sql = 'SELECT * FROM rrhh AS rh ' .
		'LEFT JOIN perfil AS pf ON pf.idperfil = rh.idperfil ' .
		'WHERE rh.borrado = 0 ' .
		'AND rh.nif = "' . $nif . '" ' .
		'AND rh.idperfil = 4';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnRRHHPorAlias($alias)
	{
		$sql = 'SELECT * FROM rrhh AS rh WHERE rh.borrado = 0 AND rh.alias = "' . $alias . '"';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnRRHHPorNif($tutor)
	{
		$sql = 'SELECT * FROM rrhh AS rh WHERE rh.borrado = 0 AND rh.nif = "' . $tutor . '"';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerRRHHCurso($idcurso)
	{
		$sql = 'SELECT * FROM rrhh AS rh LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh WHERE rh.borrado = 0 AND st.idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarNifRRHH($nifTutor, $perfil){
		$sql = "INSERT into rrhh (nif, idperfil) VALUES ('" . $nifTutor ."', '" . $perfil . "')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarRRHH($idrrhh)
	{
		$sql = 'UPDATE rrhh SET borrado = 1 WHERE idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function guardarCurriculumVitae($titulo, $id)
	{
		$resultado = false;

		if(isset($_FILES['cv']) && !empty($_FILES['cv']['tmp_name']))
		{
			if(!file_exists(PATH_ROOT . "archivos/curriculums"))
			{
				mkdir(PATH_ROOT . "archivos/curriculums");
			}


			$dir = "archivos/curriculums/";

			if(file_exists(PATH_ROOT . $dir))
			{
				//obtenemos la extension del archivo recien subido
				$extension = Texto::obtenerExtension($_FILES['cv']['name']);

				// si el tipo de archivo es valido
				// if(in_array($_FILES['cv']['type'], $this->mimesListWhite) && in_array($extension, $this->extensionsListWhite))
				// {
				if($extension == 'pdf')
				{

					chmod(PATH_ROOT . $dir, 0777);

					if(move_uploaded_file($_FILES['cv']['tmp_name'],PATH_ROOT . $dir . $titulo . "." . $extension))
					{
						$archivo = $dir . $titulo . "." . $extension;
						//Actualizamos el registro e insesrtamos el enlace en la base de datos

						if(isset($id) && is_numeric($id)){
							$sql="UPDATE rrhh SET cv = '" . $archivo . "' where idrrhh = ". $id;
							$resultado = $this->consultaSql($sql);
						}
						else{
							$resultado = $archivo;
						}
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('El archivo debe ser .pdf');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El directorio no ha sido creado');
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('No existe fichero');
		}

		return $resultado;
	}

	public function obtenerCentro($cif)
	{
		$sql = 'SELECT idcentros FROM centros WHERE borrado = 0 AND cif = "' . $cif . '"';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCentros()
	{
		$sql = 'SELECT idcentros, nombre_centro, razon_social, cif FROM centros WHERE borrado = 0 order by cif ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerPerfiles()
	{
		$sql = 'SELECT * FROM perfil';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerIdPerfil($perfil)
	{
		$sql = 'SELECT * FROM perfil WHERE ';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumno($dni)
	{
		//$sql = 'SELECT idalumnos FROM alumnos WHERE borrado = 0 AND dni = "' . $dni . '"';
		$sql = 'SELECT idalumnos, borrado FROM alumnos WHERE dni = "' . $dni . '"';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarAlumno($nombre, $apellidos, $primerApellido, $segundoApellido, $dni, $email, $pass, $usuario, $idCentro)
	{
		$sql = 'INSERT INTO alumnos (nombre, apellidos, apellido1, apellido2, dni, email, usuario, pass, idsexo, idcentros)' .
		' VALUES ("' . $nombre . '", "' . $apellidos . '", "' . $primerApellido . '", "' . $segundoApellido . '", ' .
		' "' . $dni . '", "' . $email . '",' .
		' "' . $usuario . '", "' . $pass . '", "1", "'. $idCentro .'")';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarInvitado($nombrec, $idcurso, $usuario, $password)
	{
		$sql = 'INSERT INTO rrhh (nombrec, alias, pass, idperfil)' .
		' VALUES ("' . $nombrec . '", "' . $usuario . '", "' . $password . '", 6)';
		$resultado = $this->consultaSql($sql);

		if($resultado)
		{
			$idrrhh = $this->obtenerUltimoIdInsertado();

			$sql = 'INSERT INTO staff (idrrhh, status, idcurso)' .
			' VALUES (' . $idrrhh . ', "invitado", ' . $idcurso . ')';
			$resultado = $this->consultaSql($sql);
		}

		return $resultado;
	}

	public function insertarFormacion($nif, $nombrec, $apellido1, $apellido2, $alias, $pass, $email, $telefono, $direccion, $cp, $provincia,
					$pais, $idPerfil, $localidad, $archivo)
	{
		$nombreCompleto = $nombrec . ' ' . $apellido1 . ' ' . $apellido2;

		$sql = 'INSERT INTO rrhh (nif, nombrec, nombre, apellido1, apellido2,  alias, pass, email, telefono, direccion, cp, provincia, pais, idperfil, localidad,  cv)' .
		' VALUES ("' . $nif .'", "' . $nombreCompleto . '", "' . $nombrec .'", "' . $apellido1 . '", "' . $apellido2 .'",' .
		' "' . $alias . '", "' . $pass . '", "' . $email . '", ' . $telefono . ',' .
		' "' . $direccion . '", ' . $cp . ', "' . $provincia . '",' .
		' "' . $pais . '", ' . $idPerfil . ', "' . $localidad . '", "' . $archivo . '")';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarFormacion($nif, $idrrhh, $nombrec, $apellido1, $apellido2, $alias, $pass, $email, $telefono, $direccion, $cp,
					$provincia, $pais)
	{
		$addQuery = null;
		if(!empty($pass))
		{
			$addQuery = ', pass = "' . md5($pass) . '"';
		}

		$nombreCompleto = $nombrec . ' ' . $apellido1 . ' ' . $apellido2;

		$sql = 'UPDATE rrhh
		SET nombrec =  "' . $nombreCompleto . '",' .
		' nif = "' . $nif . '",' .
		' nombre = "' . $nombrec . '",' .
		' apellido1 = "' . $apellido1 . '",' .
		' apellido2 = "' . $apellido2 . '",' .
		' alias = "' . $alias . '"' .
		$addQuery . ',' .
		' email = "' . $email . '",' .
		' telefono = ' . $telefono . ',' .
		' direccion = "' . $direccion . '",' .
		' cp = ' . $cp . ',' .
		' provincia = "' . $provincia . '",' .
		' pais = "' . $pais . '"' .
		' WHERE borrado = 0' .
		' AND idrrhh = ' . $idrrhh;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarInvitado($idrrhh, $nombrec, $idcurso, $usuario, $password)
	{
		$addQuery = null;
		if(!empty($password))
		{
			$addQuery = ', pass = "' . md5($password) . '"';
		}

		$sql = 'UPDATE rrhh SET nombrec =  "' . $nombrec . '"' . $addQuery . ', alias = "' . $usuario . '"' .
		' WHERE borrado = 0 AND idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);

		if($resultado)
		{
			$sql = 'UPDATE staff SET idcurso = ' . $idcurso .
			' WHERE idrrhh = ' . $idrrhh . ' AND idcurso = (SELECT idcurso FROM rrhh WHERE idrrhh = ' . $idrrhh . ')';
			$resultado = $this->consultaSql($sql);
		}

		return $resultado;
	}

	public function insertarAlumnoCompleto($nombre, $apellido1, $apellido2, $dni, $email, $f_nacimiento, $lugar_nacimiento, $telefono, $telefono2, $direccion,
										$cp, $provincia, $pais, $usuario, $password, $idCentro, $localidad, $idsexo)
	{
		if(!isset($idCentro))
		{
			$idCentro = 'NULL';
		}

		$apellidos = $apellido1 . ' ' . $apellido2;

		$sql = 'INSERT INTO alumnos (nombre, apellidos, apellido1, apellido2, dni, email, telefono, usuario, pass, idcentros, f_nacimiento, telefono_secundario, direccion,' .
		' cp, poblacion, pais, lugar_nacimiento, idsexo)' .
		' VALUES ("' . $nombre . '", "' . $apellidos . '", "' . $apellido1 . '" , "' .
		$apellido2 . '", "' . $dni . '", "' . $email . '", "' . $telefono . '",' .
		' "' . $usuario . '", "' . $password . '", ' . $idCentro . ',' .
		' "' . $f_nacimiento . '", "' . $telefono2 . '", "' . $direccion . '",' .
		' "' . $cp . '", "' . $localidad . '", "' . $pais . '", "' . $lugar_nacimiento . '", ' . $idsexo . ')';
		$resultado = $this->consultaSql($sql);


		return $resultado;
	}

	public function insertarMatricula($idAlumno, $idCurso)
	{
		$sql = 'INSERT INTO matricula (fecha, idalumnos, idcurso)' .
		' VALUES ("' . date('Y-m-d') . '", ' . $idAlumno . ', ' . $idCurso . ')';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarCentro($cif, $razonSocial = '' , $nombre = '' )
	{
		$sql = 'INSERT INTO centros (nombre_centro, cif, razon_social) VALUES ("' . $nombre . '", "' . $cif . '", "' . $razonSocial . '")';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarAlumno($idAlumno, $nombre, $apellido1, $apellido2, $dni, $email, $f_nacimiento, $lugar_nacimiento, $telefono, $telefono2, $direccion,
										$cp, $localidad, $provincia, $pais, $usuario, $password, $idCentro, $idsexo)
	{
		$addQuery = null;
		if(!empty($password))
		{
			$addQuery = ', pass = "' . md5($password) . '"';
		}

		if(!isset($idCentro))
		{
			$idCentro = 'NULL';
		}

		$apellidos = $apellido1 . ' ' . $apellido2;

		$sql = 'UPDATE alumnos SET' .
		' nombre = "' . $nombre . '", apellidos = "' . $apellidos . '", apellido1 = "' . $apellido1 . '", apellido2 = "' . $apellido2 . '", f_nacimiento = "' . $f_nacimiento . '",' .
		' email = "' . $email . '", lugar_nacimiento = "' . $lugar_nacimiento . '", telefono = "' . $telefono . '",' .
		' telefono_secundario = "' . $telefono2 . '", direccion = "' . $direccion . '", cp = "' . $cp . '",' .
		' poblacion = "' . $localidad . '", provincia = "' . $provincia . '", pais = "' . $pais . '",' .
		' usuario = "' . $usuario . '", dni = "' . $dni . '"' .
		$addQuery . ', idcentros = ' . $idCentro . ', idsexo = ' . $idsexo .
		' WHERE borrado = 0 AND idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnCursoPorGrupo($grupo, $idConv)
	{
		$sql = 'SELECT idcurso FROM curso C, accion_formativa AF '.
		'WHERE C.idconvocatoria = ' .$idConv.
		' and C.idaccion_formativa = AF.idaccion_formativa'.
		' and AF.accion_formativa = '.$grupo;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function restablecerBorradoLogico($idAlumno)
	{
		$sql = 'UPDATE alumnos '.
		'SET borrado = 0 '.
		'WHERE idalumnos = ' .$idAlumno;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarTelefonoEmailAlumno($idAlumno, $telefono, $email)
	{
		$sql = 'UPDATE alumnos '.
		'SET telefono = "'. $telefono . '", email = "' . $email . '" ' .
		'WHERE idalumnos = ' .$idAlumno;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarCifAlumno($nif, $idCentro){
		$sql = 'UPDATE alumnos '. 'SET idcentros = ' . $idCentro . ' WHERE dni = "' . $nif .'"';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTutorPorEmail($email) {
		$sql = 'SELECT * FROM rrhh AS rh WHERE rh.email = "' . $email . '" AND rh.idperfil = 4';
		$resultado = $this->consultaSql($sql);
		return $resultado;		
	}

	public function obtenerSupervisorPorAlias($alias) {
		$sql = 'SELECT * FROM rrhh AS rh WHERE rh.alias = "Supervisor(' . $alias . ')"';
		$resultado = $this->consultaSql($sql);
		return $resultado;			
	}

}