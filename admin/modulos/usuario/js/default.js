$(document).ready(function()
{
	// perfil de alumno
	if($('#frm_admin_alumno_nuevo').size() == 1)
	{
		$("#frm_admin_alumno_nuevo").validate({
			errorElement: "div",
			messages: {
				nombre: {
					required: 'Introduce su nombre',
					minlength : 'El nombre debe contener al menos 3 caracteres'
				},
				apellidos: {
					required: 'Introduce sus apellidos',
					minlength : 'Los apellidos deben contener al menos 2 caracteres'
				},
				email : {
					required  : 'Introduce su email',
					email     : 'El email no es v&aacute;lido'
				},
				dni : {
					required  : 'Introduce su DNI',
					minlength : 'Debe tener 9 d&iacute;ditos'
				},
				telefono : {
					digits    : 'S&oacute;lo se admiten numeros',
					minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
				},
				telefono2 : {
					digits    : 'S&oacute;lo se admiten numeros',
					minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
				},
				cp : {
					digits    : 'S&oacute;lo se admiten numeros',
					minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
				},
				usuario: {
					required: 'Introduce tu apodo',
					minlength : 'El apodo debe contener al menos 3 caracteres'
				},
				f_nacimiento : {
					date : 'La fecha de nacimiento no es v&aacute;lida',
					minlength : 'La fecha debe tener el formato 00/00/0000'
				},
				pas : {
					required : 'Introudce una contrase&ntilde;a'
				},
				user : {
					required : 'Introudce un nombre de usuario'
				}
			},
			rules: {
				nombre : {
					required  : true,
					minlength : 3
				},
				apellidos : {
					required  : true,
					minlength : 2
				},
				email : {
					required  : true,
					email     : true
				},
				dni : {
					required  : true,
					minlength : 9
				},
				telefono : {
					digits    : true,
					minlength : 9
				},
				telefono2 : {
					digits    : true,
					minlength : 9
				},
				cp : {
					digits    : true,
					minlength : 5
				},
				usuario : {
					required  : true,
					minlength : 3
				},
				f_nacimiento : {
					date : true,
					minlength : 10
				},
				pas : {
					required : true
				},
				user : {
					required : true
				}
			}
		});
	}
	// perfil de invitado
	else if($('#frm_admin_invitado_nuevo').size() == 1)
	{
		$("#frm_admin_invitado_nuevo").validate({
			errorElement: "div",
			messages: {
				nombrec: {
					required: 'Introduce su nombre y apellidos',
					minlength : 'El nombre y apellidos debe contener al menos 3 caracteres'
				},
				cargo: {
					required: 'Introduce su cargo'
				},
				curso : {
					required  : 'Introduce el curso'
				},
				usuario : {
					required : 'Introduce un nombre de usuario',
					minlength : 'Debe tener al menos 3 caracteres'
				},
				password : {
					required  : 'Introduce una contrase&ntilde;a',
					minlength : 'Debe tener 4 caracteres'
				}
			},
			rules: {
				nombrec : {
					required  : true,
					minlength : 3
				},
				cargo : {
					required  : true
				},
				curso : {
					required  : true
				},
				usuario : {
					required : true,
					minlength : 3
				},
				password : {
					required  : true,
					minlength : 4
				}
			}
		});
	}
	// perfiles de formacion
	else if($('#frm_admin_rrhh_nuevo').size() == 1)
	{
		var perfil_formacion = $('#admin_formacion_perfil').html();
		
		if(perfil_formacion == 'supervisor')
		{
			$("#frm_admin_rrhh_nuevo").validate({
				errorElement: "div",
				messages: {
					nombrec: {
						required: 'Introduce tu nombre',
						minlength : 'El nombre debe contener al menos 3 caracteres'
					},
					alias: {
						required: 'Introduce tu alias',
						minlength : 'El nombre debe contener al menos 3 caracteres'
					},
					email : {
						email     : 'El email no es v&aacute;lido'
					},
					telefono : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
					},
					cp : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
					},
					pass: {
						required: 'Introduce tu contrase&ntilde;a',
						minlength : 'La contrase&ntilde;a debe contener al menos 4 caracteres'
					},
					telefono : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
					}
				},
				rules: {
					nombrec : {
						required  : true,
						minlength : 3
					},
					alias : {
						required  : true,
						minlength : 3
					},
					email : {
						email     : true
					},
					telefono : {
						digits    : true,
						minlength : 9
					},
					cp : {
						digits    : true,
						minlength : 5
					},
					pass : {
						minlength : 4
					},
					atelefono : {
						digits    : true,
						minlength : 9
					}
				}
			});
		}
		else
		{
			$("#frm_admin_rrhh_nuevo").validate({
				errorElement: "div",
				messages: {
					nombrec: {
						required: 'Introduce tu nombre',
						minlength : 'El nombre debe contener al menos 3 caracteres'
					},
					alias: {
						required: 'Introduce tu alias',
						minlength : 'El nombre debe contener al menos 3 caracteres'
					},
					email : {
						required  : 'Introduce tu email',
						email     : 'El email no es v&aacute;lido'
					},
					telefono : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
					},
					cp : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
					},
					pass: {
						required: 'Introduce tu contrase&ntilde;a',
						minlength : 'La contrase&ntilde;a debe contener al menos 4 caracteres'
					},
					telefono : {
						digits    : 'S&oacute;lo se admiten numeros',
						minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
					}
				},
				rules: {
					nombrec : {
						required  : true,
						minlength : 3
					},
					alias : {
						required  : true,
						minlength : 3
					},
					email : {
						required  : true,
						email     : true
					},
					telefono : {
						digits    : true,
						minlength : 9
					},
					cp : {
						digits    : true,
						minlength : 5
					},
					pass : {
						minlength : 4
					},
					atelefono : {
						digits    : true,
						minlength : 9
					}
				}
			});
		}
	}
});

function alumnosMostrarMotivosBorrados(element)
{
	if($(element).parents('.elemento').find('.elementoBorradoMotivos').hasClass('hide'))
	{
		$(element).parents('.elemento').find('.elementoBorradoMotivos').removeClass('hide');
	}
	else
	{
		$(element).parents('.elemento').find('.elementoBorradoMotivos').addClass('hide');
	}
}