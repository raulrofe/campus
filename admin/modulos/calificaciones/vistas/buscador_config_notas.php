<div id="calificaciones_pond_notas">
	<div class="titular"><h1>Calificaciones - Configuraci&oacute;n de notas - Listado</h1></div>
	
  		<div class="stContainer" id="tabs">
  		
  		<div class="navegacion">
	  		<div class="btn_navegacion"><a href="calificaciones/configuracion-de-notas">Listado de configuraci&oacute;n de notas</a></div>
	  		<div class="btn_navegacion"><a href="calificaciones/configuracion-de-notas/nueva">Nueva configuracion&oacute;n de notas</a></div>
	  		<div class="btn_navegacion">
	  			<select name="idconv" onchange="buscar_config_notas_cambiar_conv(this); return false;">
		  			<option value="0">- Selecciona una convocatoria -</option>
			  		<?php while($conv = $convocatorias->fetch_object()):?>
			  			<option value="<?php echo $conv->idconvocatoria?>" <?php if(isset($get['idconv']) && $conv->idconvocatoria == $get['idconv']) echo 'selected';?>><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
			  		<?php endwhile;?>
		  		</select>	  		
	  		</div>
  		</div>
  		
  		<div class="tab">
		  	
		  	<?php if(isset($cursos) && !isset($configNota)):?>
  				<?php if($cursos->num_rows > 0):?>
  					<form method="post" action="">
	  					<table class="tabla">
					  		<thead>
					  			<tr>
					  				<td></td>
					  				<td>Curso</td>
					  				<td>Opciones</td>
					  			</tr>
					  		</thead>
					  		<tbody>
					  			<?php while($curso = $cursos->fetch_object()):?>
				  					<tr>
					  					<td><input id="config_notas_listado_cursos_<?php echo $curso->idcurso?>" type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>" /></td>
					  					<td><?php echo Texto::textoPlano($curso->titulo)?> <span style="font-size: 0.9em;">(Fecha inicio:<?php echo date('d-m-Y', strtotime($curso->f_inicio))?> - Fecha fin:<?php echo date('d-m-Y', strtotime($curso->f_fin))?>)</span></td>
					  					<td><a href="calificaciones/configuracion-de-notas/<?php echo $get['idconv']?>/<?php echo $curso->idcurso?>" title="">Editar</a></td>
					  				</tr>
					  			<?php endwhile;?>
						  	</tbody>
						</table>
						<br />
						<div>
						  	<label style="width: auto;">Selecciona la configuraci&oacute;n de notas que desees establecer a los cursos</label>
							<select style="width: 100%;" name="id_config_nota">
						  		<option value="0">- Selecciona una configuraci&oacute;n de notas -</option>
							  	<?php while($item = $configNotasSelect->fetch_object()):?>
							  		<option value="<?php echo $item->idconf_calificaciones_notas?>">
							  			<?php echo 'Num accesos:' . $item->n_accesos . ' - Num Trabajos:' . $item->num_trabajos . ' - Num Msg Foro:' . $item->num_msg_foro . ' - Calf. Scorm:' . $item->calificacion_scorm;?>
							  		</option>
							  	<?php endwhile;?>
							  </select>
						</div>
						<br />
						<div class="t_center">
						  	<input type="submit" value="Asignar a los cursos" />
						</div>
						<div class="clear"></div>
					</form>
  				<?php else:?>
  					<div>Esta convocatoria no tiene cursos</div>
  				<?php endif;?>
  			<?php endif;?>
  			
  			<?php if(isset($configNota) && $configNota->num_rows == 1):?>
  				<?php $configNota = $configNota->fetch_object();?>
  				<form method="post" action="">
					<ul>
						<li>
							<label for="calf_pond_notas_scorm">N&deg; de accesos</label>
							<input id="calf_pond_notas_scorm" type="text" name="n_accesos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->n_accesos)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<label for="calf_pond_notas_autoeval">N&deg; de mensajes de foro</label>
							<input id="calf_pond_notas_autoeval" type="text" name="num_msg_foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_msg_foro)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<label for="calf_pond_notas_trabajo_equipo">N&deg; de mensajes por m&oacute;dulo</label>
							<input id="calf_pond_notas_trabajo_equipo" type="text" name="num_trabajos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_trabajos)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<input type="hidden" name="option" value="editar" />
							<button type="submit">Guardar</button>
						</li>
					</ul>
				</form>
  			<?php endif;?>
	  	</div>
	 </div>
</div>