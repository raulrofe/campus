
  			
  			<select name="idconv" onchange="buscar_pond_notas_cambiar_conv(this); return false;">
  				<option value="0">- Selecciona una convocatoria -</option>
	  			<?php while($conv = $convocatorias->fetch_object()):?>
	  				<option value="<?php echo $conv->idconvocatoria?>" <?php if(isset($get['idconv']) && $conv->idconvocatoria == $get['idconv']) echo 'selected';?>><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
	  			<?php endwhile;?>
  			</select>
  			
	  		<?php if(isset($cursos) && !isset($configNota)):?>
  				<br /><br />
	  		
  				<?php if($cursos->num_rows > 0):?>
  					<form method="post" action="">
	  					<table id="ponderacion_notas_listado_cursos" class="tabla">
					  		<thead>
					  			<tr>
					  				<td></td>
					  				<td>Curso</td>
					  				<td>Opciones</td>
					  			</tr>
					  		</thead>
					  		<tbody id="ponderacion_notas_listado_cursos_labels">
					  			<?php while($curso = $cursos->fetch_object()):?>
				  					<tr>
				  						<td><input id="ponderacion_notas_listado_cursos_<?php echo $curso->idcurso?>" type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>" /></td>
					  					<td><label for="ponderacion_notas_listado_cursos_<?php echo $curso->idcurso?>"><?php echo Texto::textoPlano($curso->titulo)?> <span style="font-size: 0.9em;">(Fecha inicio:<?php echo date('d-m-Y', strtotime($curso->f_inicio))?> - Fecha fin:<?php echo date('d-m-Y', strtotime($curso->f_fin))?>)</span></label></td>
					  					<td><a href="calificaciones/buscador-ponderacion-notas/<?php echo $get['idconv']?>/<?php echo $curso->idcurso?>" title="">Editar</a></td>
					  				</tr>
					  			<?php endwhile;?>
					  		</tbody>
					  	</table>
					  	
					  	<br />
					  	<div>
					  		<a id="ponderacion_notas_listado_cursos_select_all" href="#" onclick="admin_pond_notas_select_all_cursos(); return false;" title="">Selecionar todos</a>
					  		<a id="ponderacion_notas_listado_cursos_select_none" class="hide" href="#" onclick="admin_pond_notas_select_none_cursos(); return false;" title="">Deseleccionar todos</a>
					  	</div>
					  	<br />
					  	<div>
					  		<label>Selecciona la ponderaci&oacute;n de notas que desees establecer  a los cursos</label>
					  		<select style="width: 100%;" name="id_config_ponderacion">
				  				<option value="0">- Selecciona una ponderaci&oacute;n de notas -</option>
					  			<?php while($item = $configNotasSelect->fetch_object()):?>
					  				<option value="<?php echo $item->idconf_calificaciones?>">
					  					<?php echo 'Scorm:' . $item->scorm . '% - Autoeval:' . $item->autoevaluaciones . '% - T. pr&aacute;cticos:' . $item->trabajos_practicos . '% -' . 
					  					'T. equipo:' . $item->trabajo_equipo . '% - Tutor&iacute;a:' . $item->tutoria . '% - Extra I:' . $item->extra1 . '% - Extra II:' . $item->extra2 . ' - Coordinador:' . $item->nota_coordinador . '%'?>
					  				</option>
					  			<?php endwhile;?>
				  			</select>
					  	</div>
					  	<div class="clear"></div>
					  	<br />
					  	<div class="t_center">
					  		<input type="submit" value="Asignar a los cursos" />
					  	</div>
					  </form>
		  			
		  			<?php if(isset($configNotas)):?>
			  			<?php if($configNotas->num_rows > 0):?>
				  			<table class="tabla">
				  				<thead>
				  					<tr>
				  						<td>Contenido Scorm</td>
				  						<td>Autoevaluaciones</td>
				  						<td>Trabajos pr&aacute;cticos</td>
				  						<td>Trabajos en equipo</td>
				  						<td>Foro</td>
				  						<td>Tutor&iacute;a</td>
				  						<td>Extra I</td>
				  						<td>Extra II</td>
				  						<td>Nota coordinador</td>
				  						<td>Opciones</td>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<?php while($item = $configNotas->fetch_object()):?>
				  						<tr>
				  							<td><?php echo $item->scorm?>%</td>
				  							<td><?php echo $item->autoevaluaciones?>%</td>
				  							<td><?php echo $item->trabajos_practicos?>%</td>
				  							<td><?php echo $item->trabajo_equipo?>%</td>
				  							<td><?php echo $item->foro?>%</td>
				  							<td><?php echo $item->tutoria?>%</td>
				  							<td><?php echo $item->extra1?>%</td>
				  							<td><?php echo $item->extra2?>%</td>
				  							<td><?php echo $item->nota_coordinador?>%</td>
				  							<td><a href="calificaciones/ponderacion-notas/editar/<?php echo $item->idconf_calificaciones?>" title="Editar esta ponderaci&oacute;n de nota">Editar</a></td>
										</tr>
				  					<?php endwhile;?>
				  				</tbody>
				  			</table>
				  		<?php else:?>
				  			<div><strong>No hay ponderaciones de notas</strong></div>
				  		<?php endif;?>
				  	<?php endif;?>
			  		
  				<?php else:?>
  					<div>Esta convocatoria no tiene cursos</div>
  				<?php endif;?>
  			<?php endif;?>
  			
  			<?php if(isset($configNota, $configNota->num_rows) && $configNota->num_rows == 1):?>
  				<?php $configNota = $configNota->fetch_object();?>
  				<br /><br />
  				<div id="calificaciones_pond_notas">
	  				<h2>Editar ponderaci&oacute;n de notas de este curso</h2>
		  			<form method="post" action="">
						<ul>
							<li>
								<label for="calf_pond_notas_scorm">Contenido scorm (%)</label>
								<input id="calf_pond_notas_scorm" type="text" name="scorm" maxlength="3" value="<?php echo Texto::textoPlano($configNota->scorm)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_autoeval">Autoevaluaciones (%)</label>
								<input id="calf_pond_notas_autoeval" type="text" name="autoeval" maxlength="3" value="<?php echo Texto::textoPlano($configNota->autoevaluaciones)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_autoeval">Trabajos pr&aacute;cticos (%)</label>
								<input id="calf_pond_notas_autoeval" type="text" name="trabajos_practicos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->trabajos_practicos)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_trabajo_equipo">Trabajos en equipo (%)</label>
								<input id="calf_pond_notas_trabajo_equipo" type="text" name="trabajo_equipo" maxlength="3" value="<?php echo Texto::textoPlano($configNota->trabajo_equipo)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_foro">Foro (%)</label>
								<input id="calf_pond_notas_foro" type="text" name="foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->foro)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_tutoria">Tutor&iacute;a (%)</label>
								<input id="calf_pond_notas_tutoria" type="text" name="tutoria" maxlength="3" value="<?php echo Texto::textoPlano($configNota->tutoria)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_extra2">Extra I (%)</label>
								<input id="calf_pond_notas_extra2" type="text" name="extra1" maxlength="3" value="<?php echo Texto::textoPlano($configNota->extra1)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_extra2">Extra II (%)</label>
								<input id="calf_pond_notas_extra2" type="text" name="extra2" maxlength="3" value="<?php echo Texto::textoPlano($configNota->extra2)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_nota_coord">Nota coordinador (%)</label>
								<input id="calf_pond_notas_nota_coord" type="text" name="nota_coordinador" maxlength="3" value="<?php echo Texto::textoPlano($configNota->nota_coordinador)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<input type="hidden" name="option" value="editar" />
								<button type="submit">Guardar</button>
							</li>
						</ul>
					</form>
					<br />
					<a href="#" onclick="history.back(); return false;" title=""><b>Ir hacia atr&aacute;s</b></a>
				</div>
			<?php endif;?>