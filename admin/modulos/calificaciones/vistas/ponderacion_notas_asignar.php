<div id="admin_usuarios">
	<div class="titular">
		<h1>CALIFICACIONES</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
  	<div class="stContainer" id="tabs">
  		<div class="tab">
	  		<div class="submenuAdmin">ASIGNAR PONDERACI&Oacute;N DE NOTAS</div>
			<div class='buscador'>	
				<form name='frmConv' method='post' action='calificaciones/ponderacion-notas/asignar' class='t_center'>
					<select name='idConvocatoria' onchange='this.form.submit();'>
						<option value=''> - Seleccione una convocatoria - </option>
						<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
							<option value='<?php echo $convocatoria['idconvocatoria']; ?>'><?php echo $convocatoria['nombre_convocatoria']; ?></option>
						<?php endwhile; ?>
					</select>
				</form>
		  	</div>
		  	
	  		<?php if(isset($cursos) && !isset($configNota)):?>
  				<br /><br />
	  		
  				<?php if($cursos->num_rows > 0):?>
  					<form method="post" action="">
	  					<table id="ponderacion_notas_listado_cursos" class="tabla">
					  		<thead>
					  			<tr>
					  				<td></td>
					  				<td>Curso</td>
					  				<td>Opciones</td>
					  			</tr>
					  		</thead>
					  		<tbody id="ponderacion_notas_listado_cursos_labels">
					  			<?php while($curso = $cursos->fetch_object()):?>
				  					<tr>
				  						<td><input id="ponderacion_notas_listado_cursos_<?php echo $curso->idcurso?>" type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>" /></td>
					  					<td><label for="ponderacion_notas_listado_cursos_<?php echo $curso->idcurso?>"><?php echo Texto::textoPlano($curso->titulo)?> <span style="font-size: 0.9em;">(Fecha inicio:<?php echo date('d-m-Y', strtotime($curso->f_inicio))?> - Fecha fin:<?php echo date('d-m-Y', strtotime($curso->f_fin))?>)</span></label></td>
					  					<td><a href="calificaciones/ponderacion-notas/asignar/<?php echo $post['idConvocatoria']?>/<?php echo $curso->idcurso?>" title="">Editar</a></td>
					  				</tr>
					  			<?php endwhile;?>
					  		</tbody>
					  	</table>
					  	
					  	<br />
					  	<div>
					  		<a id="ponderacion_notas_listado_cursos_select_all" href="#" onclick="admin_pond_notas_select_all_cursos(); return false;" title="">Selecionar todos</a>
					  		<a id="ponderacion_notas_listado_cursos_select_none" class="hide" href="#" onclick="admin_pond_notas_select_none_cursos(); return false;" title="">Deseleccionar todos</a>
					  	</div>
					  	<br />
					  	<div>
					  		<label>Selecciona la ponderaci&oacute;n de notas que desees establecer  a los cursos</label>
					  		<select style="width: 100%;" name="id_config_ponderacion">
				  				<option value="0">- Selecciona una ponderaci&oacute;n de notas -</option>
					  			<?php while($item = $configNotasSelect->fetch_object()):?>
					  				<option value="<?php echo $item->idconf_calificaciones?>">
					  					<?php echo 'Scorm:' . $item->scorm . '% - Autoeval:' . $item->autoevaluaciones . '% - T. pr&aacute;cticos:' . $item->trabajos_practicos . '% -' . 
					  					'T. equipo:' . $item->trabajo_equipo . '% - Tutor&iacute;a:' . $item->tutoria . '% - Extra I:' . $item->extra1 . '% - Extra II:' . $item->extra2 . ' - Coordinador:' . $item->nota_coordinador . '%'?>
					  				</option>
					  			<?php endwhile;?>
				  			</select>
					  	</div>
					  	<div class="clear"></div>
					  	<br />
					  	<div class="t_center">
					  		<input type="submit" value="Asignar a los cursos" />
					  	</div>
					  	<input type='hidden' value='asignar' name='option' />
					  </form>
				<?php endif; ?>
			<?php endif; ?>  			
	  	</div>
	 </div>
</div>