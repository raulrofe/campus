<!-- Listado de ponderacion de notas -->
<div id="cursosList">
	<?php if($configNotasList->num_rows > 0):?>
		<form id="defaultPonderacionNotas" method="post" action="calificaciones/ponderacion-notas/establecer-defecto">
			<table class="table table-hover">
				<thead>
		        	<tr>
		        		<th class="text-center">Marca por defecto</th>
	  					<th class="text-center">Contenido SCORM</th>
	  					<th class="text-center">Autoevaluaciones</th>
	  					<th class="text-center">Trabajos pr&aacute;cticos</th>
	  					<th class="text-center">Trabajos en equipo</th>
	  					<th class="text-center">Foro</th>
	  					<th class="text-center">Tutor&iacute;a</th>
	  					<th class="text-center">Extra I</th>
	  					<th class="text-center">Extra II</th>
	  					<th class="text-center">Nota coordinador</th>
		                <th class="span1 text-center"><i class="icon-cogs"></i></th>
					</tr>
				</thead>		
	  			<?php while($item = $configNotasList->fetch_object()):?>
	  				<tr>
	  					<td class="text-center"><input type="radio" name="idPonderacionDefecto" value="<?php echo $item->idconf_calificaciones ?>" onclick="enviarFormulario('defaultPonderacionNotas')"; <?php if(isset($item->defecto) && $item->defecto == 1) echo 'checked' ?> /></td>
	  					<td class="text-center"><?php echo $item->scorm?>%</td>
	  					<td class="text-center"><?php echo $item->autoevaluaciones?>%</td>
	  					<td class="text-center"><?php echo $item->trabajos_practicos?>%</td>
	  					<td class="text-center"><?php echo $item->trabajo_equipo?>%</td>
	  					<td class="text-center"><?php echo $item->foro?>%</td>
	  					<td class="text-center"><?php echo $item->tutoria?>%</td>
	  					<td class="text-center"><?php echo $item->extra1?>%</td>
	  					<td class="text-center"><?php echo $item->extra2?>%</td>
	  					<td class="text-center"><?php echo $item->nota_coordinador?>%</td>
		                <td class="span1 text-center">
		                	<div class="btn-group">
		                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="calificaciones/ponderacion-notas/editar/<?php echo $item->idconf_calificaciones ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
		                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="calificaciones/ponderacion-notas/eliminar/<?php echo $item->idconf_calificaciones ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
		                    </div>
		                </td>
					</tr>
	  			<?php endwhile;?>
		  	</table> 
	  	</form>
	<?php else: ?>
		<div class="text-center">No existen ponderaciones de notas creadas</div>
	<?php endif;?>
</div>
  	
<script type="text/javascript">
	function enviarFormulario(idFormulario)
	{
		$('#' + idFormulario).submit();
	}
</script>  	