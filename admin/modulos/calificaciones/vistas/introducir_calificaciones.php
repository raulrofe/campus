<div id="calificaciones_introducir_calificaciones">
  	<div class="titular"><h1>Calificaciones - Introducir calificaciones</h1></div>
	
	<div class="stContainer" id="tabs">
  		
  		<div class="navegacion">
	  		<div class="btn_navegacion"><a href="calificaciones/configuracion-de-notas">Listado de configuraci&oacute;n de notas</a></div>
	  		<div class="btn_navegacion"><a href="calificaciones/configuracion-de-notas/nueva">Nueva configuracion&oacute;n de notas</a></div>
  		</div>
  			
  		<div class="tab">
  			<select name="idconv" onchange="intr_calf_cambiar_conv(this); return false;">
  				<option value="0">- Selecciona una convocatoria -</option>
	  			<?php while($conv = $convocatorias->fetch_object()):?>
	  				<option value="<?php echo $conv->idconvocatoria?>" <?php if(isset($get['idconv']) & $conv->idconvocatoria == $get['idconv']) echo 'selected';?>><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
	  			<?php endwhile;?>
  			</select>
  			
  			<?php if(isset($cursos)):?>
  				<?php if($cursos->num_rows > 0):?>
  					<select name="idcurso" onchange="intr_calf_cambiar_curso(<?php echo $get['idconv']?>, this); return false;">
			  			<option value="0">- Selecciona un curso</option>
			  			<?php while($curso = $cursos->fetch_object()):?>
			  				<option value="<?php echo $curso->idcurso?>" <?php if($curso->idcurso == $get['idcurso']) echo 'selected';?>><?php echo Texto::textoPlano($curso->titulo)?></option>
			  			<?php endwhile;?>
		  			</select>
  				<?php else:?>
  					<div>Esta convocatoria no tiene cursos</div>
  				<?php endif;?>
  			<?php endif;?>
  			
  			<?php require_once mvc::obtenerRutaVista(dirname(__FILE__), 'notas');?>
  		</div>
  	</div>
</div>