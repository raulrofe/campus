<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="calificaciones/asignar">Acciones formativas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="calificaciones/asignar">Asignar calificaciones</a></li>
    <li class="clear"></li>
</ul>

<h4><?php echo $curso->titulo?></h4>
<div class="block block-tabs block-themed">
	<ul data-toggle="tabs" class="nav nav-tabs">
    	<li class='<?php if(empty($get['tabCalificacion']) || (isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'ponderacion-notas')) echo "active" ?>'><a href="#example-tabs3-ponderacion-notas">Ponderaci&oacute;n de notas</a></li>
        <li class='<?php if(isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'configuracion-notas') echo "active" ?>'><a href="#example-tabs3-configuracion-de-notas">Configuraci&oacute;n de notas</a></li>
        <li class='<?php if(isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'notas-automaticas') echo "active" ?>'><a href="#example-tabs3-calificaciones-automaticas">Notas autom&aacute;ticas</a></li>
    </ul>
    <div class="tab-content">
    	<div id="example-tabs3-ponderacion-notas" class="tab-pane <?php if(empty($get['tabCalificacion']) || (isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'ponderacion-notas')) echo "active" ?>"><?php require_once 'tab_calificaciones/ponderacion_notas.php'; ?></div>
        <div id="example-tabs3-configuracion-de-notas" class="tab-pane <?php if(isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'configuracion-notas') echo "active" ?>"><?php require_once 'tab_calificaciones/configuracion_notas.php'; ?></div>
        <div id="example-tabs3-calificaciones-automaticas" class="tab-pane <?php if(isset($get['tabCalificacion']) && $get['tabCalificacion'] == 'notas-automaticas') echo "active" ?>"><?php require_once 'tab_calificaciones/notas_automaticas.php'; ?></div>
	</div>
</div>
