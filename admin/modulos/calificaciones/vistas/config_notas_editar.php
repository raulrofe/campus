<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR DATOS CONFIGURACION NOTAS</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmNuevoConfiguracionNotas" action='calificaciones/configuracion-de-notas/editar/<?php echo $get['idconf_calf'] ?>' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">N&deg; de mensajes por foro</label>
	                        	<div class="controls"><input id="calf_pond_notas_scorm" type="text" name="num_msg_foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_msg_foro)?>" /></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">N&deg; de trabajos por m&oacute;dulo</label>
		                        <div class="controls"><input id="calf_pond_notas_autoeval" type="text" name="num_trabajos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_trabajos)?>" /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
		         
		         <!-- END div.row-fluid -->
            	 <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		     </div>
		 </form>
	</div>
</div>

<script type="text/javascript" src="js-calificaciones-actualizar_configuracionnotas.js"></script>
