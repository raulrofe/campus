<div id="admin_usuarios">
	<div class="titular">
		<h1>CALIFICACIONES</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
  	<div class="stContainer" id="tabs">
  		<div class="tab">
	  		<div class="submenuAdmin">ASIGNAR PONDERACI&Oacute;N DE NOTAS</div>
	  		<p class='subtitleAdmin'>ACTUALIZAR DATOS ASIGNACION DE PONDERACI&Oacute;N DE NOTAS A CURSO</p>
			<div class='formulariosAdmin'>	
		  			<form method="post" action="calificaciones/ponderacion-notas/asignar">
						<ul>
							<li>
								<label for="calf_pond_notas_scorm">Contenido scorm (%)</label>
								<input id="calf_pond_notas_scorm" type="text" name="scorm" maxlength="3" value="<?php echo Texto::textoPlano($configNota->scorm)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_autoeval">Autoevaluaciones (%)</label>
								<input id="calf_pond_notas_autoeval" type="text" name="autoeval" maxlength="3" value="<?php echo Texto::textoPlano($configNota->autoevaluaciones)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_autoeval">Trabajos pr&aacute;cticos (%)</label>
								<input id="calf_pond_notas_autoeval" type="text" name="trabajos_practicos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->trabajos_practicos)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_trabajo_equipo">Trabajos en equipo (%)</label>
								<input id="calf_pond_notas_trabajo_equipo" type="text" name="trabajo_equipo" maxlength="3" value="<?php echo Texto::textoPlano($configNota->trabajo_equipo)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_foro">Foro (%)</label>
								<input id="calf_pond_notas_foro" type="text" name="foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->foro)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_tutoria">Tutor&iacute;a (%)</label>
								<input id="calf_pond_notas_tutoria" type="text" name="tutoria" maxlength="3" value="<?php echo Texto::textoPlano($configNota->tutoria)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_extra2">Extra I (%)</label>
								<input id="calf_pond_notas_extra2" type="text" name="extra1" maxlength="3" value="<?php echo Texto::textoPlano($configNota->extra1)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_extra2">Extra II (%)</label>
								<input id="calf_pond_notas_extra2" type="text" name="extra2" maxlength="3" value="<?php echo Texto::textoPlano($configNota->extra2)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label for="calf_pond_notas_nota_coord">Nota coordinador (%)</label>
								<input id="calf_pond_notas_nota_coord" type="text" name="nota_coordinador" maxlength="3" value="<?php echo Texto::textoPlano($configNota->nota_coordinador)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<input type="hidden" name="option" value="asignar" />
								<input type="hidden" name="idcurso" value="<?php echo $get['idcurso']; ?>" />
								<button type="submit">Guardar</button>
							</li>
						</ul>
					</form>	
				</div>
	  	</div>
	 </div>
</div>
