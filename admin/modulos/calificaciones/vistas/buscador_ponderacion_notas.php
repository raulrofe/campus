<div id="seguimiento">
	<div class="titular"><h1>Calificaciones - Ponderaci&oacute;n de notas - Buscador</h1></div>
	
  		<div class="stContainer" id="tabs">
  		
  		<!-- Navegacion de las pestañas -->
  		<div class="navegacion">
	  		<div class="btn_navegacion"><a href="calificaciones/ponderacion-notas">Listado de ponderaciones de notas</a></div>
	  		<div class="btn_navegacion"><a href="calificaciones/ponderacion-notas/nueva">Nueva ponderaci&oacute;n de notas</a></div>
	  	</div>
	  			
  		<div class="tab">
  			<?php require 'modulos/calificaciones/vistas/buscador_ponderacion_notas_inside.php'?>
  		</div>
  	</div>
</div>