<?php $configNota = $configNota->fetch_object();?>
<div id="admin_usuarios">
	<div class="titular">
		<h1>CALIFICACIONES</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
  	<div class="stContainer" id="tabs">
  		<div class="tab">
	  		<div class="submenuAdmin">ASIGNAR CONFIGURACI&Oacute;N DE NOTAS</div>
	  		<p class='subtitleAdmin'>ACTUALIZAR DATOS CONFIGURACI&Oacute;N DE  NOTAS A CURSO</p>
			<div class='formulariosAdmin'>	
			<p class="t_center negrita">Curso: <?php echo $configNota->titulo; ?></p>
			<br/>
  				<form method="post" action="calificaciones/configuracion-de-notas/asignar">
					<ul>
						<li>
							<label for="calf_pond_notas_autoeval">N&deg; de mensajes de foro</label>
							<input id="calf_pond_notas_autoeval" type="text" name="num_msg_foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_msg_foro)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<label for="calf_pond_notas_trabajo_equipo">N&deg; de mensajes por m&oacute;dulo</label>
							<input id="calf_pond_notas_trabajo_equipo" type="text" name="num_trabajos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_trabajos)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<input type="hidden" name="option" value="buscador" />
							<input type='hidden' name='idcurso' value='<?php echo $get['idcurso']; ?>' />
							<button type="submit">Guardar</button>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>