<div class="">
	<div class="full">
		<form class="form-inline" enctype="multipart/form-data" id="frmEditarPonderacionNotas" action='calificaciones/notas-automaticas/asignar/<?php echo $get['idCurso'] ?>' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Autoevaluaciones</label>
	                        	<div class="controls">
									<select name="idconf_autoeval" class='fleft'>
								  		<?php foreach($configCalfAutoArray as $item):?>
								  			<?php if( $item['idconf_calf_auto'] != 4):?>
								  				<?php if($curso->tipo_autoeval == $item['idconf_calf_auto']):	?>
								  					<option value="<?php echo $item['idconf_calf_auto']?>" selected ><?php echo $item['nombre']?></option>
								  				<?php else: ?>
								  					<option value="<?php echo $item['idconf_calf_auto']?>"><?php echo $item['nombre']?></option>
								  				<?php endif; ?>
								  			<?php endif;?>
								  		<?php endforeach;?>
								  	</select>	                        	
	                        	</div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">Contenido SCORM</label>
		                        <div class="controls">
	                        		<select name="idconf_scorm" class='fleft'>
								  		<?php foreach($configCalfAutoArray as $item):?>
								  			<?php if($curso->tipo_scorm == $item['idconf_calf_auto']):	?>
									  			<option value="<?php echo $item['idconf_calf_auto']?>" selected ><?php echo $item['nombre']?></option>
									  		<?php else: ?>
									  			<option value="<?php echo $item['idconf_calf_auto']?>"><?php echo $item['nombre']?></option>
									  		<?php endif; ?>
								  		<?php endforeach;?>
								  	</select>
		                        </div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>                	                	                
             </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-save"></i> Guardar</button></div>
		</form>
	</div>
</div>
