<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="calificaciones/asignar">Acciones formativas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="calificaciones/asignar">Asignar calificaciones</a></li>
    <li class="clear"></li>
</ul>

<!-- Listado de Cursos -->
<div id="cursosList">
	<?php if($cursos->num_rows > 0):?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th>Nombre Curso</th>
	                <th>Fecha inicio</th>
	                <th>Fecha fin</th>
				</tr>
			</thead>
	  		<?php while($curso = mysqli_fetch_object($cursos)):?>
	        	<tr>
	                <td><a href="calificaciones/asignar/<?php echo $curso->idcurso ?>"><?php echo $curso->titulo ?></a></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_inicio, '-', '/') ?></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_fin, '-', '/') ?></td>
	            </tr>
	  		<?php endwhile; ?>
	  	</table> 
	<?php else: ?>
		<div class="text-center">No existen cursos</div>
	<?php endif;?>
</div>