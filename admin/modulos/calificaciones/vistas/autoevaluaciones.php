<div id="calificaciones_introducir_calificaciones">
  	<div class="titular"><h1>Calificaciones - Autoevaluaciones</h1></div>
	
	<div class="stContainer" id="tabs">
  		
  		<!-- Navegacion de las pestañas -->
  		<div class="navegacion">
  			<div class="btn_navegacion"><a href="calificaciones/autoevaluaciones">Ver autoevaluaciones</a></div>
  		</div>
  			
  		<div class="tab">
  		
  		
  			<!-- Select de convocatorias -->
  			<div>
	  			<select name="idconv" onchange="intr_calf_cambiar_conv(this); return false;">
	  				<option value="0">- Selecciona una convocatoria -</option>
		  			<?php while($conv = mysqli_fetch_assoc($convocatorias)):?>
		  				<option value="<?php echo $conv['idconvocatoria']?>" <?php if(isset($idconv) & $conv['idconvocatoria'] == $idconv) echo 'selected';?>><?php echo Texto::textoPlano($conv['nombre_convocatoria'])?></option>
		  			<?php endwhile;?>
	  			</select>
  			</div>
			<br/>
					
  			<!-- Select de cursos -->
  			<div>
  			<?php if(isset($cursos)):?>
  				<?php if(mysqli_num_rows($cursos) > 0):?>
  					<select name="idcurso" onchange="intr_calf_cambiar_curso(<?php echo $get['idconv']?>, this); return false;">
			  			<option value="0">- Selecciona un curso</option>
			  			<?php while($curso = mysqli_fetch_assoc($cursos)):?>
			  				<option value="<?php echo $curso['idcurso']?>" <?php if($curso['idcurso'] == $get['idcurso']) echo 'selected';?>><?php echo Texto::textoPlano($curso['titulo'])?></option>
			  			<?php endwhile;?>
		  			</select>
  				<?php else:?>
  					<div>Esta convocatoria no tiene cursos</div>
  				<?php endif;?>
  			<?php endif;?>
  			
  			<?php //require_once mvc::obtenerRutaVista(dirname(__FILE__), 'notas');?>
  			</div>
			<br/>
			
			<!-- Select de alumnos -->
			<div>
  			<?php if(isset($alumnos)):?>
  				<?php if(mysqli_num_rows($alumnos) > 0):?>
  					<select name="idalumno" onchange="intr_calf_cambiar_alumno(<?php echo $get['idconv']?>, <?php echo $get['idcurso']?>, this); return false;">
			  			<option value="0">- Selecciona un alumno</option>
			  			<?php while($alumno = mysqli_fetch_assoc($alumnos)):?>
			  				<option value="<?php echo $alumno['idalumnos']?>" <?php if($alumno['idalumnos'] == $get['idalumno']) echo 'selected';?>><?php echo Texto::textoPlano($alumno['nombrec'])?></option>
			  			<?php endwhile;?>
		  			</select>
  				<?php else:?>
  					<div>Este curso no tiene alumnos</div>
  				<?php endif;?>
  			<?php endif;?>			
			</div>
			<br/>
			
			<div>
			<?php if(isset($idAlumno)):?>
				<div class='titulo_curso'><?php  echo $miAlumno['nombre']." ".$miAlumno['apellidos']." (NIF: ".$miAlumno['dni'].") - ".$miCurso['titulo']; ?></div>
				<table cellspacing='0'>
					<tr class='celda_titulo'>
						<td class='celda_autoevaluacion'>Autoevaluaciones</td>
						<td class='celda'>Fecha realizaci&oacute;n</td>
						<td class='celda'>Nota</td>
					</tr>
						<?php
						while($notaTest = mysqli_fetch_assoc($notasTestAlumno))
						{	
							$resultado_autoeval = $objCalificaciones->datosAutoevaluacion($notaTest['idautoeval']);					
							$fila_autoeval = mysqli_fetch_assoc($resultado_autoeval);
							
							echo "<tr>";
								echo "<td class='celda_autoevaluacion'>".$fila_autoeval['titulo_autoeval']."</td>";
								echo "<td class='celda'>".Fecha::obtenerFechaFormateada($notaTest['fecha'],false)."</td>";
								echo "<td class='celda'>".$notaTest['nota']."</td>";
							echo "</tr>";
						}
						?>
				</table>
			<?php endif;?>
			</div>
   			
  		</div>
  	</div>
</div>
