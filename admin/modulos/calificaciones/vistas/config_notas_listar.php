<!-- Listado de ponderacion de notas -->
<div id="cursosList">
	<?php if($configNotas->num_rows > 0):?>
		<form id="defaultPonderacionNotas" method="post" action="calificaciones/ponderacion-notas/establecer-defecto">
			<table class="table table-hover">
				<thead>
		        	<tr>
		        		<th class="text-center">N&deg; mensajes de foro</th>
	  					<th class="text-center">N&deg; trabajos por m&oacute;dulos</th>
	  					<th class="text-center">Calificaci&oacute;n SCORM</th>
		                <th class="span1 text-center"><i class="icon-cogs"></i></th>
					</tr>
				</thead>		
	  			<?php while($item = $configNotas->fetch_object()):?>
	  				<tr>
	  					<!-- <td class="text-center"><input type="radio" name="idPonderacionDefecto" value="<?php //echo $item->idconf_calificaciones ?>" onclick="enviarFormulario('defaultPonderacionNotas')"; <?php //if($item->defecto == 1) echo 'checked' ?> /></td> -->
	  					<td class="text-center"><?php echo $item->num_msg_foro ?></td>
	  					<td class="text-center"><?php echo $item->num_trabajos ?></td>
	  					<td class="text-center"><?php echo $item->calificacion_scorm ?></td>
		                <td class="span1 text-center">
		                	<div class="btn-group">
		                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="calificaciones/configuracion-de-notas/editar/<?php echo $item->idconf_calificaciones_notas ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
		                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="calificaciones/configuracion-de-notas/eliminar/<?php echo $item->idconf_calificaciones_notas ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
		                    </div>
		                </td>
					</tr>
	  			<?php endwhile;?>
		  	</table> 
	  	</form>
	<?php else: ?>
		<div class="text-center">No existen ponderaciones de notas creadas</div>
	<?php endif;?>
</div>