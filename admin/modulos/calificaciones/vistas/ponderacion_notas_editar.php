<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR PONDERACI&Oacute;N DE NOTAS</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmEditarPonderacionNotas" action='calificaciones/ponderacion-notas/editar/<?php echo $get['idPonderacionNotas'] ?>' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Contenido SCORM (%)</label>
	                        	<div class="controls"><input id="calf_pond_notas_scorm" type="text" name="scorm" maxlength="3" value="<?php echo $configNota->scorm?>" /></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">Autoevaluaciones (%)</label>
		                        <div class="controls"><input id="calf_pond_notas_autoeval" type="text" name="autoeval" maxlength="3" value="<?php echo $configNota->autoevaluaciones?>" /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
		         
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Trabajos pr&aacute;cticos (%)</label>
	                        	<div class="controls"><input id="trabajos_practicos" type="text" name="trabajos_practicos" maxlength="3" value="<?php echo $configNota->trabajos_practicos?>" /></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">Trabajos en equipo (%)</label>
		                        <div class="controls"><input id="calf_pond_notas_trabajo_equipo" type="text" name="trabajo_equipo" maxlength="3" value="<?php echo $configNota->trabajo_equipo?>" /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>    
		         
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Foro (%)</label>
	                        	<div class="controls"><input id="calf_pond_notas_foro" type="text" name="foro" maxlength="3" value="<?php echo $configNota->foro?>" /></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">Tutor&iacute;a (%)</label>
		                        <div class="controls"><input id="calf_pond_notas_tutoria" type="text" name="tutoria" maxlength="3" value="<?php echo $configNota->tutoria?>" /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 		     
  
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Extra I (%)</label>
	                        	<div class="controls"><input id="calf_pond_notas_extra1" type="text" name="extra1" maxlength="3" value="<?php echo $configNota->extra1?>" /></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">Extra II (%)</label>
		                        <div class="controls"><input id="calf_pond_notas_extra2" type="text" name="extra2" maxlength="3" value="<?php echo $configNota->extra2?>" /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 
  
                 <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Nota coordinador (%)</label>
	                        	<div class="controls"><input id="calf_pond_notas_nota_coord" type="text" name="nota_coordinador" maxlength="3" value="<?php echo $configNota->nota_coordinador?>" /></div>
	                        </div>
						</div>
		                
		            <div class="clear"></div>
		         </div> 
                	                	                
             </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-calificaciones-actualizar_ponderacionnotas.js"></script>