<div id="notas">
		<?php if(isset($alumnos) && $alumnos->num_rows > 0):?>
			<form id="frm_notas" method="post" action="">
				<table class="tabla">
					<thead>
						<tr>
							<td>Alumnos</td>
							<td>Foro (<?php echo $configNotas->foro?>%)</td>
							<td>Trabajos pr&aacute;cticos (<?php echo $configNotas->trabajos_practicos?>%)</td>
							<td>Autoevaluaci&oacute;n (<?php echo $configNotas->autoevaluaciones?>%)</td>
							<td>Nota media</td>
						</tr>
					</thead>
					<tbody>
						<?php while($alumno = $alumnos->fetch_object()):?>
							<tr>
								<td><?php echo Texto::textoPlano($alumno->nombrec)?></td>
								<td>
									<?php 
									if($alumno->media_foro == null)
									{
										$foroPuntos = 0;
										
										$temas = $objModelo->obtenerTemas($alumno->idalumnos, $idCurso);
										if($temas->num_rows > 0)
										{
											while($tema = $temas->fetch_object())
											{
												if($tema->numMsg >= $configNotas->num_msg_foro)
												{
													$foroPuntos++;
												}
											}
											
											// calcula los puntos para la nota
											$foroPuntos = round(($foroPuntos / $numTemas) * ($configNotas->foro / 10), 2);
										}
										else
										{
											$foroPuntos = 0;
										}
									}
									else
									{
										$foroPuntos = round($alumno->media_foro, 2);
									}
									?>
									
									<input type="text" name="notaforo_<?php echo $alumno->idalumnos?>" value="<?php echo $foroPuntos;?>" />
								</td>
								<td>
									<?php
									$notaTP = $objNotas->calcularMediaTrabajosPract($alumno->idalumnos, $configNotas->num_trabajos);
									$notaTP = round(($notaTP / 10) * ($configNotas->trabajos_practicos / 10), 2);
									echo $notaTP;
									?>
								</td>
								<td>
									<?php
										$notaFinalTests = 0;
										
										// numero de test de este curso
										$numTests = $objModelo->obtenerNumTests($idCurso);
										$numTests = $numTests->fetch_object();
										$numTests = $numTests->numTests;
										
										$notasTests = $objModelo->obtenerNotasTests($alumno->idalumnos, $idCurso);
										if($notasTests->num_rows > 0)
										{
											$idautoeval = 0;
											$notasPorTest = array();
											
											while($notaTest = $notasTests->fetch_object())
											{
												if($idautoeval != $notaTest->idautoeval)
												{
													if(count($notasPorTest) > 0)
													{
														$notaFinalTests += max($notasPorTest);
													}
													
													$notasPorTest = array();
												}
												
												$notasPorTest[] = $notaTest->nota;
												
												$idautoeval = $notaTest->idautoeval;
											}
											$notaFinalTests += max($notasPorTest);
										}
										
										if($numTests > 0)
										{
											$mediaTests = ($notaFinalTests / $numTests) * ($configNotas->autoevaluaciones / 10);
											$mediaTests = ($configNotas->autoevaluaciones * $mediaTests) / 100;
										}
										else 
										{
											$mediaTests = 0;
										}
										
										echo round($mediaTests, 2);
									?>
								</td>
								<td>
									<?php
									if(isset($alumno->nota_coordinador))
									{
										$notaMediaFinal = round($alumno->nota_coordinador, 2);
									}
									else
									{
										$notaMediaFinal = round($foroPuntos + $notaTP + $mediaTests, 2);
									}
									?>
									<input type="text" name="notacoordinador_<?php echo $alumno->idalumnos?>" value="<?php echo $notaMediaFinal?>" />
								</td>
							</tr>
						<?php endwhile;?>	
					</tbody>	
				</table>
				
				<div class="t_right">
					<br />
					<input type="hidden" name="actualizarNotas" value="1" />
					<input type="submit" value="Guardar" />
				</div>
			</form>
		<?php endif;?>
</div>