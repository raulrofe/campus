<div id="admin_usuarios">
	<div class="titular">
		<h1>CALIFICACIONES</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>
  	<div class="stContainer" id="tabs">
  		<div class="tab">
	  		<div class="submenuAdmin">ASIGNAR CONFIGURACI&Oacute;N DE NOTAS</div>
			<div class='buscador'>	
				<form name='frmConv' method='post' action='calificaciones/configuracion-de-notas/asignar' class='t_center'>
					<select name='idConvocatoria' onchange='this.form.submit();'>
						<option value=''> - Seleccione una convocatoria - </option>
						<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
							<option value='<?php echo $convocatoria['idconvocatoria']; ?>'><?php echo $convocatoria['nombre_convocatoria']; ?></option>
						<?php endwhile; ?>
					</select>
				</form>
		  	</div>
		  	<br/>
		  	<?php if(isset($cursos)):?>
  				<?php if($cursos->num_rows > 0):?>
  					<form method="post" action="">
	  					<table class="tabla">
					  		<thead>
					  			<tr>
					  				<td></td>
					  				<td>Curso</td>
					  				<td>Opciones</td>
					  			</tr>
					  		</thead>
					  		<tbody>
					  			<?php while($curso = $cursos->fetch_object()):?>
				  					<tr>
					  					<td><input id="config_notas_listado_cursos_<?php echo $curso->idcurso?>" type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>" /></td>
					  					<td><?php echo Texto::textoPlano($curso->titulo)?> <span style="font-size: 0.9em;">(Fecha inicio:<?php echo date('d-m-Y', strtotime($curso->f_inicio))?> - Fecha fin:<?php echo date('d-m-Y', strtotime($curso->f_fin))?>)</span></td>
					  					<td><a href="calificaciones/configuracion-de-notas/asignar/<?php echo $post['idConvocatoria']?>/<?php echo $curso->idcurso?>" title="">Editar</a></td>
					  				</tr>
					  			<?php endwhile;?>
						  	</tbody>
						</table>
						<br />
						<div>
						  	<label style="width: auto;">Selecciona la configuraci&oacute;n de notas que desees establecer a los cursos</label>
							<select style="width: 100%;" name="id_config_nota">
						  		<option value="0">- Selecciona una configuraci&oacute;n de notas -</option>
							  	<?php while($item = $configNotasSelect->fetch_object()):?>
							  		<option value="<?php echo $item->idconf_calificaciones_notas?>">
							  			<?php echo 'Num. Trabajos:' . $item->num_trabajos . ' - Num. Mensaje Foro:' . $item->num_msg_foro;?>
							  		</option>
							  	<?php endwhile;?>
							  </select>
						</div>
						<br />
						<div class="t_center">
						  	<input type="submit" value="Asignar a los cursos" />
						</div>
						<div class="clear"></div>
						<input type='hidden' name='option' value='asignar' />
					</form>
  				<?php else:?>
  					<div>Esta convocatoria no tiene cursos</div>
  				<?php endif;?>
  			<?php endif;?> 	
  			
  			<?php if(isset($configNota) && $configNota->num_rows == 1):?>
  				<?php $configNota = $configNota->fetch_object();?>
  				<form method="post" action="">
					<ul>
						<li>
							<label for="calf_pond_notas_autoeval">N&deg; de mensajes de foro</label>
							<input id="calf_pond_notas_autoeval" type="text" name="num_msg_foro" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_msg_foro)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<label for="calf_pond_notas_trabajo_equipo">N&deg; de mensajes por m&oacute;dulo</label>
							<input id="calf_pond_notas_trabajo_equipo" type="text" name="num_trabajos" maxlength="3" value="<?php echo Texto::textoPlano($configNota->num_trabajos)?>" />
							<div class="clear"></div>
						</li>
						<li>
							<input type="hidden" name="option" value="editar" />
							<button type="submit">Guardar</button>
						</li>
					</ul>
				</form>
  			<?php endif;?>		
	  	</div>
	 </div>
</div>

