<?php
mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objCurso = new Cursos();
$objAlumno = new Alumnos();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();

if(mysqli_num_rows ($convocatorias) > 0)
{
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$idconv = $get['idconv'];
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	else $idconv = 0;
	
	
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		require_once PATH_ROOT . 'modulos/notas/modelos/modelo.php';
		require_once PATH_ROOT . 'modulos/notas/notas.php';
		require_once PATH_ROOT . 'modulos/calificaciones/modelos/modelo.php';
		
		$idCurso = $get['idcurso'];
		$objModelo = new ModeloNotas();
		$objCalificaciones = new TrabajosPracticos();
		
		$alumnos = $objModelo->obtenerAlumnos($idCurso);
		
		if(isset($get['idalumno']) && is_numeric($get['idalumno']))
		{
			//Datos para el alumno
			$idAlumno = $get['idalumno'];
			$objAlumno->set_alumno($idAlumno);
			$elAlumno = $objAlumno->ver_alumno();
			if(mysqli_num_rows($elAlumno) >0)
			{
				$miAlumno = mysqli_fetch_assoc($elAlumno);
			}
			
			//Datos Curso
			$objCurso->set_curso($idCurso);
			$elCurso = $objCurso->buscar_curso();
			if(mysqli_num_rows($elCurso) >0)
			{
				$miCurso = mysqli_fetch_assoc($elCurso);
			}
			
			//Obtenemos las notas
			$notasTestAlumno = $objModelo->obtenerNotasTests($idAlumno, $idCurso);
		}
				
		$objNotas = new Notas();
		$objNotas->setCurso($idCurso);
		
		$configNotas = $objModelo->obtenerConfigPorcentajes($idCurso);
		$configNotas = $configNotas->fetch_object();
		
		$numTemas = $objModelo->obtenerNumTemas($idCurso);
		$numTemas = $numTemas->num_rows;
		
		// actualizar notas de los foros manualmente
		if(Peticion::isPost())
		{

		}

		$alumnos = $objModelo->obtenerAlumnos($idCurso);
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');