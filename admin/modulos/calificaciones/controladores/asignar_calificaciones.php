<?php
$objModeloCalificaciones = new AModeloCalificaciones();
$get = Peticion::obtenerGet();

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objCurso = new Cursos();

if(isset($get['f']) && $get['f'] == 'asignar')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		$ponderacionNotas = array();

		$curso = $objModeloCalificaciones->obtenerUnCurso($get['idCurso']);	
		$curso = mysqli_fetch_object($curso);

		//Obtengo las calificaciones que se han establecido por defecto para la creacion de cursos
		$configCalfDefault = $objModeloCalificaciones->obtenerConfigPonderacionNotas(true);
		if($configCalfDefault->num_rows == 1)
		{
			$calificacionDefecto = $configCalfDefault->fetch_object();
		}

		//Establezco los valores en un array del curso para la ponderacion de nota
		$ponderacionNotas['scorm'] 				= $curso->scorm;
		$ponderacionNotas['autoevaluaciones'] 	= $curso->autoevaluaciones;
		$ponderacionNotas['trabajosPracticos'] 	= $curso->trabajos_practicos;
		$ponderacionNotas['trabajosEquipo'] 	= $curso->trabajo_equipo;
		$ponderacionNotas['foro'] 				= $curso->foro;
		$ponderacionNotas['tutoria'] 			= $curso->tutoria;
		$ponderacionNotas['extra1'] 			= $curso->extra1;
		$ponderacionNotas['extra2'] 			= $curso->extra2;
		$ponderacionNotas['coordinador'] 		= $curso->nota_coordinador;
				
		//Opciones para mostrar en un select las notas automaticas
		$configCalfAutoArray = array();
		$configCalfAuto = $objModeloCalificaciones->obtenerConfigNotasAuto();
		
		while($item = $configCalfAuto->fetch_object())
		{
			$configCalfAutoArray[] = array('idconf_calf_auto' => $item->idconf_calf_auto, 'nombre' => $item->nombre);
		}

		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'cursos_editar_asignar_calificaciones'));
	}
	else
	{
		$cursos = $objCurso->all_cursos();
		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'cursos_asignar_calificaciones'));
	}
}		


