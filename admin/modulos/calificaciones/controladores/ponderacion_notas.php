<?php
$objModeloCalificaciones = new AModeloCalificaciones();
$get = Peticion::obtenerGet();

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objCurso = new Cursos();

// Obtengo el listado de configuracion de notas
$configNotasList = $objModeloCalificaciones->obtenerConfigPonderacionNotas();

if(isset($get['f']) && $get['f'] == 'listar')
{
	$loadView = 'ponderacion_notas_listado';
}
elseif(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(isset($post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'],
				 $post['nota_coordinador'])
				 && (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajos_practicos']) && is_numeric($post['trabajo_equipo']) 
				 && is_numeric($post['tutoria']) && is_numeric($post['foro']) && is_numeric($post['extra1']) && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador'])))
		{
			if($objModeloCalificaciones->insertarConfigPonderacionNotas($post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], 
										 $post['foro'], $post['extra1'], $post['extra2'], $post['nota_coordinador']))
			{
				Alerta::guardarMensajeInfo('Se ha creado la ponderaci&oacute;n de notas');
				Url::redirect('calificaciones/ponderacion-notas');
			}
			else
			{
				Alerta::guardarMensajeInfo('Ya existe una ponderaci&oacute;n de notas igual en la base de datos');
				Url::redirect('calificaciones/ponderacion-notas');	
			}
		}
		
	}
	
	$loadView = 'ponderacion_notas_insertar';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idPonderacionNotas']) && is_numeric($get['idPonderacionNotas']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(isset($post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], 
					 $post['extra2'], $post['nota_coordinador']) && (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajos_practicos']) 
					 && is_numeric($post['trabajo_equipo']) && is_numeric($post['tutoria']) && is_numeric($post['foro']) && is_numeric($post['extra1']) 
					 && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador'])))
		 	{				
				if($objModeloCalificaciones->editarConfigPonderacionNotas($get['idPonderacionNotas'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], 
											 $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['nota_coordinador']))
				{
					Alerta::guardarMensajeInfo('Se ha actualizado la ponderaci&oacute;n de notas');
					Url::redirect('calificaciones/ponderacion-notas');
				}
				else
				{
					Alerta::guardarMensajeInfo('Ya existe una ponderaci&oacute;n de notas igual en la base de datos');
					Url::redirect('calificaciones/ponderacion-notas');				
				}
		 	}
		}
		
		$configNota = $objModeloCalificaciones->obtenerUnaConfigPonderacionNotas($get['idPonderacionNotas']);
		$configNota = mysqli_fetch_object($configNota);
		$loadView = 'ponderacion_notas_editar';	
	}
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idPonderacionNotas']) && is_numeric($get['idPonderacionNotas']))
	{
		$configNota = $objModeloCalificaciones->obtenerUnaConfigPonderacionNotas($get['idPonderacionNotas']);
		if($configNota->num_rows == 1)
		{
			if($objModeloCalificaciones->eliminarConfigPonderacionNotas($get['idPonderacionNotas']))
			{
				Alerta::guardarMensajeInfo('La ponderación de notas fue eliminada');
			}
		}	
	}	
	
	Url::redirect('calificaciones/ponderacion-notas');
}
else if(isset($get['f']) && $get['f'] == 'asignar')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(isset($post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], 
			 		 $post['extra2'], $post['nota_coordinador'], $get['idCurso']) && 
			 		 (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajos_practicos']) && is_numeric($post['trabajo_equipo']) && 
			 		  is_numeric($post['tutoria']) && is_numeric($post['foro']) && is_numeric($post['extra1']) && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador'])
			 		  && is_numeric($get['idCurso'])))
					 {
					 	if($objModeloCalificaciones->editarConfigNotasPonderacionParaCurso($get['idCurso'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], 
					 							  $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['nota_coordinador']))
					 							  {
					 							  	Alerta::guardarMensajeInfo('La ponderaci&oacute;n de notas ha sido actualizada correctamente para el curso');
					 							  	Url::redirect('calificaciones/asignar/' . $get['idCurso'] . '/ponderacion-notas');
					 							  }	
					 }	
		}
		
		$curso = $objModeloCalificaciones->obtenerUnCurso($get['idCurso']);	
		$curso = mysqli_fetch_object($curso);
		
		//var_dump($curso);
		
		//Opciones para mostrar en un select las notas automaticas
		$configCalfAutoArray = array();
		$configCalfAuto = $objModeloCalificaciones->obtenerConfigNotasAuto();
		
		while($item = $configCalfAuto->fetch_object())
		{
			$configCalfAutoArray[] = array('idconf_calf_auto' => $item->idconf_calf_auto, 'nombre' => $item->nombre);
		}
		
		$loadView = 'ponderacion_notas_editar_asignar';
	}
	else 
	{
		$cursos = $objCurso->all_cursos();
		$loadView = 'ponderacion_notas_asignar';	
	}	
}
else if(isset($get['f']) && $get['f'] == 'pordefecto')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(isset($post['idPonderacionDefecto']) && is_numeric($post['idPonderacionDefecto']))
		{
			//Restauramos los valores por defecto a ninguna
			if($objModeloCalificaciones->restoreDefectoPonderacionNotas())
			{
				if($objModeloCalificaciones->establerDefectoPonderacionNotas($post['idPonderacionDefecto']))
				{
					Url::redirect('calificaciones/ponderacion-notas');
				}		
			}
		}
	}
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_ponderacion_notas'));

/*
// ----- BUSCADOR --------------
// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias(true);
// ------ FIN BSUCADOR ---------

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();

	if(isset($post['option']) && $post['option'] == 'asignar')
	{
		
		//echo "entro if";
		
		// asignacion masiva de ponderaciones a cursos
		if(isset($post['id_config_ponderacion'], $_POST['cursos']) && is_numeric($post['id_config_ponderacion']) && is_array($_POST['cursos']))
		{
			//echo "cargo";
			$configNotaPonderacion = $objModeloCalificaciones->obtenerUnaConfigPonderacionNotas($post['id_config_ponderacion']);
			$configNotaPonderacion = $configNotaPonderacion->fetch_object();
				
			foreach($_POST['cursos'] as $itemIdCurso)
			{
				$objModeloCalificaciones->editarConfigNotasPonderacionParaCurso(
					$itemIdCurso, $configNotaPonderacion->autoevaluaciones, $configNotaPonderacion->scorm, $configNotaPonderacion->trabajos_practicos, $configNotaPonderacion->trabajo_equipo,
					$configNotaPonderacion->tutoria, $configNotaPonderacion->foro, $configNotaPonderacion->extra1, $configNotaPonderacion->extra2, $configNotaPonderacion->nota_coordinador
				);
			}
			
			Alerta::mostrarMensajeInfo('nuevaponderacion','Se han guardado las nuevas ponderaciones de notas para los cursos seleccionados');
		}
		// edicion de la ponderacion de notas de un curso
		else if(isset($post['idcurso'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['option'], $post['nota_coordinador'])
				 && is_numeric($post['idcurso']) && (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajo_equipo']) && is_numeric($post['tutoria']) && is_numeric($post['foro'])
					&& is_numeric($post['extra1']) && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador']) && is_numeric($post['trabajos_practicos'])))
		{
			//echo "cargo asig";
			$objModeloCalificaciones->editarConfigNotasPonderacionParaCurso(
				$post['idcurso'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'],
				$post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['nota_coordinador']
			);
			
			Alerta::guardarMensajeInfo('Se ha guardado la nueva ponderaci&oacute;n de notas del curso');
			
			//Url::redirect('calificaciones/ponderacion-notas/asignar');
		}
	}
	
	if(isset($post['idConvocatoria']) && is_numeric($post['idConvocatoria']))
	{
		$cursos = $objCurso->cursosConvocatoriaActivos($post['idConvocatoria']);
		$configNotasSelect = $objModeloCalificaciones->obtenerConfigPonderacionNotas();
	}
	
	if(isset($post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['option'],
				$post['nota_coordinador'])
		&& (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajos_practicos']) && is_numeric($post['trabajo_equipo']) && is_numeric($post['tutoria']) && is_numeric($post['foro'])
			&& is_numeric($post['extra1']) && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador'])))
	{

	}
}

// Obtengo el listado de configuracion de notas
$configNotasList = $objModeloCalificaciones->obtenerConfigPonderacionNotas();

if(isset($get['option']))
{

	else if($get['option'] == 'asignar')
	{
		if(isset($get['idconv']))
		{
			if(isset($get['idcurso']) && is_numeric($get['idcurso']))
			{
				$configNota = $objModeloCalificaciones->obtenerUnCurso($get['idcurso']);
				$configNota = $configNota->fetch_object();
				require_once mvc::obtenerRutaVista(dirname(__FILE__), 'ponderacion_notas_editar_asignar');	
			}
		}
		else
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'ponderacion_notas_asignar');	
		}
	}
}
*/