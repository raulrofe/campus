<?php
$objModeloCalificaciones = new AModeloCalificaciones();
$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'listar')
{
	// obtenemos en un array los elementos del combobox
	$configCalfAutoArray = array();
	$configCalfAuto = $objModeloCalificaciones->obtenerConfigNotasAuto();
	
	while($item = $configCalfAuto->fetch_object())
	{
		$configCalfAutoArray[] = array('idconf_calf_auto' => $item->idconf_calf_auto, 'nombre' => $item->nombre);
	}

	$loadView = 'notas_automaticas';	
}
else if(isset($get['f']) && $get['f'] == 'asignar')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			if(isset($post['idconf_autoeval'], $post['idconf_scorm']) && is_numeric($post['idconf_autoeval']) && is_numeric($post['idconf_scorm']))
			{
				if($objModeloCalificaciones->actualizarConfigNotasAuto($get['idCurso'], $post['idconf_autoeval'], $post['idconf_scorm']))
				{
					Alerta::guardarMensajeInfo('Notas autom&aacute;ticas ha sido actualizada');
					Url::redirect('calificaciones/asignar/' . $get['idCurso'] . '/notas-automaticas');
				}
			}
		}	
	}	
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general_notas_automaticas');


