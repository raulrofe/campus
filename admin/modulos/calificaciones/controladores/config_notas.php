<?php
$objModeloCalificaciones = new AModeloCalificaciones();
$get = Peticion::obtenerGet();

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objCurso = new Cursos();

$configNotas = $objModeloCalificaciones->obtenerConfiguracionNotasAccesos();

if(isset($get['f']) && $get['f'] == 'listar')
{
	$loadView = 'config_notas_listar';	
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(isset($post['num_msg_foro'], $post['num_trabajos']) && (is_numeric($post['num_msg_foro'])&& is_numeric($post['num_trabajos'])))
		{
			if($objModeloCalificaciones->insertarConfigNotas($post['num_msg_foro'], $post['num_trabajos']))
			{
				Alerta::guardarMensajeInfo("Se ha a&ntilde;adido la nueva configuraci&oacute;n de notas");		
				Url::redirect('calificaciones/configuracion-de-notas');
			}
			else
			{
				Alerta::guardarMensajeInfo("Ya existe una configuraci&oacute;n de notas igual en la base de datos");		
				Url::redirect('calificaciones/configuracion-de-notas');				
			}			
		}
	}
	
	$loadView = 'config_notas_insertar';	
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idconf_calf']) && is_numeric($get['idconf_calf']))
	{		
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
		
			if(isset($post['num_msg_foro'], $post['num_trabajos']) && (is_numeric($post['num_msg_foro'])&& is_numeric($post['num_trabajos'])))
			{
				if($objModeloCalificaciones->editarConfigNotas($get['idconf_calf'], $post['num_msg_foro'], $post['num_trabajos']))
				{
					Alerta::guardarMensajeInfo("Se ha actualizado la configuraci&oacute;n de notas");	
					Url::redirect('calificaciones/configuracion-de-notas');
				}
				else
				{
					Alerta::guardarMensajeInfo("Ya existe una configuraci&oacute;n de notas igual en la base de datos");	
					Url::redirect('calificaciones/configuracion-de-notas');
				}				
			}
		}
		
		//Busco los datos de la configuracion de notas
		$configNota = $objModeloCalificaciones->obtenerUnaConfiguracionNotasAccesos($get['idconf_calf']);
		$configNota = $configNota->fetch_object();
		
		$loadView = 'config_notas_editar';	
	}	
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idconf_calf']) && is_numeric($get['idconf_calf']))
	{
		if($objModeloCalificaciones->eliminarConfigNotas($get['idconf_calf']))
		{
			Alerta::guardarMensajeInfo("Se ha eliminado la configuraci&oacute;n de notas");
			Url::redirect('calificaciones/configuracion-de-notas');
		}		
	}		
}
else if(isset($get['f']) && $get['f'] == 'asignar')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
		
			if(isset($post['num_msg_foro'], $post['num_trabajos']) && is_numeric($post['num_msg_foro']) && is_numeric($post['num_trabajos']))
			{
				if($objModeloCalificaciones->editarConfigNotasParaCurso($get['idCurso'], $post['num_msg_foro'], $post['num_trabajos']))
				{
					Alerta::guardarMensajeInfo("Se ha guardado la configuraci&oacute;n de notas para el curso");
					Url::redirect('calificaciones/asignar/' . $get['idCurso'] . '/configuracion-notas');					
				}
			}	
		}	
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general_config_notas');

/*
if(isset($get['idconv']) && is_numeric($get['idconv']))
{
	$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
}


if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	//print_r($post);
	
	if(isset($post['idConvocatoria']) && is_numeric($post['idConvocatoria']))
	{
		$cursos = $objCurso->cursosConvocatoriaActivos($post['idConvocatoria']);
		$configNotasSelect = $objModeloCalificaciones->obtenerConfiguracionNotasAccesos();
		//print_r($configNotasSelect);
	}
	
	if(isset($post['num_msg_foro'], $post['num_trabajos'])
		&& (is_numeric($post['num_msg_foro'])&& is_numeric($post['num_trabajos'])))
	{
		if($post['option'] == 'nueva')
		{
			if($objModeloCalificaciones->insertarConfigNotas($post['num_msg_foro'], $post['num_trabajos']))
			{
				Alerta::guardarMensajeInfo("Se ha a&ntilde;adido la nueva configuraci&oacute;n de notas");		
				Url::redirect('calificaciones/configuracion-de-notas');
			}
			else
			{
				Alerta::guardarMensajeInfo("Ya existe una configuraci&oacute;n de notas igual en la base de datos");		
				Url::redirect('calificaciones/configuracion-de-notas');				
			}
		}
		else if($post['option'] == 'editar' && isset($post['idconf_calf']) && is_numeric($post['idconf_calf']))
		{
			
		}
		else if($post['option'] == 'buscador' && isset($post['idcurso']) && is_numeric($post['idcurso']))
		{
			if($objModeloCalificaciones->editarConfigNotasParaCurso($post['idcurso'], $post['num_msg_foro'], $post['num_trabajos']))
			{
				Alerta::guardarMensajeInfo("Se ha actualizado la configuraci&oacute;n de notas para este curso");
				
				Url::redirect('calificaciones/configuracion-de-notas/asignar');
			}
		}
	}
	else if(isset($post['cursos'], $post['id_config_nota']) && is_numeric($post['id_config_nota']) && is_array($post['cursos']) && count($post['cursos']) > 0)
	{
		$rowConfigNota = $objModeloCalificaciones->obtenerUnaConfiguracionNotasAccesos($post['id_config_nota']);
		if($rowConfigNota->num_rows == 1)
		{
			$rowConfigNota = $rowConfigNota->fetch_object();
			
			foreach($post['cursos'] as $itemIdCurso)
			{
				$objModeloCalificaciones->editarConfigNotasParaCurso($itemIdCurso, $rowConfigNota->num_msg_foro, $rowConfigNota->num_trabajos);
				
				Alerta::guardarMensajeInfo("Se han actualizado los cursos seleccionados con la nueva configuraci&oacute;n de notas");
			}
		}
	}
}

// cuando se ha seleccionado un curso
if(isset($get['idcurso']) && is_numeric($get['idcurso']))
{		
	$configNota = $objModeloCalificaciones->obtenerUnCurso($get['idcurso']);
}




if(isset($get['option']))
{
	else if($get['option'] == 'asignar')
	{
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'config_notas_asignar');
	}
}
*/