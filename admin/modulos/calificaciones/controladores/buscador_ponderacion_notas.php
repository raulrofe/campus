<?php
$objModeloCalificaciones = new AModeloCalificaciones();
$get = Peticion::obtenerGet();

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
if($convocatorias->num_rows > 0)
{
	//require_once PATH_ROOT . 'modulos/informe/modelos/modelo.php';
	//$objModeloInforme = new ModeloInforme();
	
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		// asignacion masiva de ponderaciones a cursos
		if(isset($post['id_config_ponderacion'], $_POST['cursos']) && is_numeric($post['id_config_ponderacion']) && is_array($_POST['cursos']))
		{
			$configNotaPonderacion = $objModeloCalificaciones->obtenerUnaConfigPonderacionNotas($post['id_config_ponderacion']);
			$configNotaPonderacion = $configNotaPonderacion->fetch_object();
				
			foreach($_POST['cursos'] as $itemIdCurso)
			{
				$objModeloCalificaciones->editarConfigNotasPonderacionParaCurso(
					$itemIdCurso, $configNotaPonderacion->autoevaluaciones, $configNotaPonderacion->scorm, $configNotaPonderacion->trabajos_practicos, $configNotaPonderacion->trabajo_equipo,
					$configNotaPonderacion->tutoria, $configNotaPonderacion->foro, $configNotaPonderacion->extra1, $configNotaPonderacion->extra2, $configNotaPonderacion->nota_coordinador
				);
			}
			
			Alerta::mostrarMensajeInfo('nuevaponderacion','Se han guardado las nuevas ponderaciones de notas para los cursos seleccionados');
		}
		// edicion de la ponderacion de notas de un curso
		else if(isset($get['idcurso'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'], $post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['option'], $post['nota_coordinador'])
				 && is_numeric($get['idcurso']) && (is_numeric($post['autoeval']) && is_numeric($post['scorm']) && is_numeric($post['trabajo_equipo']) && is_numeric($post['tutoria']) && is_numeric($post['foro'])
					&& is_numeric($post['extra1']) && is_numeric($post['extra2']) && is_numeric($post['nota_coordinador']) && is_numeric($post['trabajos_practicos'])))
		{
			$objModeloCalificaciones->editarConfigNotasPonderacionParaCurso(
				$get['idcurso'], $post['autoeval'], $post['scorm'], $post['trabajos_practicos'], $post['trabajo_equipo'],
				$post['tutoria'], $post['foro'], $post['extra1'], $post['extra2'], $post['nota_coordinador']
			);
			
			Alerta::guardarMensajeInfo('Se ha guardado la nueva ponderaci&oacute;n de notas del curso');
			
			Url::redirect('calificaciones/buscador-ponderacion-notas/' . $get['idconv']);
		}
	}
	
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		$configNota = $objModeloCalificaciones->obtenerUnCurso($get['idcurso']);
	}
}

$configNotasSelect = $objModeloCalificaciones->obtenerConfigPonderacionNotas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'buscador_ponderacion_notas');