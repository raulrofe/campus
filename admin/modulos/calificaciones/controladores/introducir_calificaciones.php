<?php
mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
if($convocatorias->num_rows > 0)
{
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		require_once PATH_ROOT . 'modulos/notas/modelos/modelo.php';
		require_once PATH_ROOT . 'modulos/notas/notas.php';
		
		$idCurso = $get['idcurso'];
		$objModelo = new ModeloNotas();
				
		$objNotas = new Notas();
		$objNotas->setCurso($idCurso);
		
		$configNotas = $objModelo->obtenerConfigPorcentajes($idCurso);
		$configNotas = $configNotas->fetch_object();
		
		$numTemas = $objModelo->obtenerNumTemas($idCurso);
		$numTemas = $numTemas->num_rows;
		
		// actualizar notas de los foros manualmente
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			if(isset($post['actualizarNotas']) && $post['actualizarNotas'] == 1)
			{
				foreach($post as $item => $value)
				{
					$p_foro = explode('_', $item);
					
					// NOTA FORO
					if(count($p_foro) == 2 && $p_foro[0] == 'notaforo' && is_numeric($p_foro[1]))
					{
						$foroPuntos = 0;
						
						if(is_numeric($value) && ($value >= 0 && $value <= ($configNotas->foro / 10)))
						{
							$foroPuntos = 0;
							$temas = $objModelo->obtenerTemas($p_foro[1], $idCurso);
							if($temas->num_rows > 0)
							{
								while($tema = $temas->fetch_object())
								{
									if($tema->numMsg >= $configNotas->num_msg_foro)
									{
										$foroPuntos++;
									}
								}
							}
							
							// calcula los puntos para la nota
							if($numTemas > 0)
							{
								$foroPuntos = round(($foroPuntos / $numTemas) * ($configNotas->foro / 10), 2);
							}
							
							if($foroPuntos == $value)
							{
								$value = null;
							}
						}
						else
						{
							$value = null;
						}
						
						// si la nueva nota es distinta  la no automatica se actualiza la nota manual
						if(isset($value))
						{
							$objModelo->actualizarNotaForo($p_foro[1], $idCurso, $value);
						}
						else
						{
							$objModelo->actualizarNotaForo($p_foro[1], $idCurso, 'NULL');
						}
					}
					// NOTA COORDINADOR
					else if(count($p_foro) == 2 && $p_foro[0] == 'notacoordinador' && is_numeric($p_foro[1]))
					{
						if(is_numeric($value) && ($value >= 0 && $value <= 10))
						{
							
						}
						else if(empty($value))
						{
							$value = 'NULL';
						}
						else
						{
							$value = null;
						}
						
						if(isset($value))
						{
							$objModelo->actualizarNotaCoordinador($p_foro[1], $idCurso, $value);
						}
					}
				}
			}
		}

		$alumnos = $objModelo->obtenerAlumnos($idCurso);
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'introducir_calificaciones');
}