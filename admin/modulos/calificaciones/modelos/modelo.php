<?php
class AModeloCalificaciones extends modeloExtend
{
	/**
	 * Restauro los valores por defecto de las ponderaciones de nota
	 */
	
	public function restoreDefectoPonderacionNotas()
	{
		$sql = 'UPDATE conf_calificaciones SET defecto = 0';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}
	
	/**
	 * EStablezco una ponderacion de notas por defecto
	 */
	
	public function establerDefectoPonderacionNotas($idPonderacionDefecto)
	{
		$sql = 'UPDATE conf_calificaciones SET defecto = 1 WHERE idconf_calificaciones = ' . $idPonderacionDefecto;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene todas las configuraciones de ponderaciones de notas
	 */
	public function obtenerConfigPonderacionNotas($default = null)
	{
		$addQuery = '';
		if(isset($default))
		{
			$addQuery .= ' WHERE defecto = 1';
		}

		$sql = 'SELECT * FROM conf_calificaciones' . $addQuery;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene todas las configuraciones de notas
	 */
	public function obtenerConfigNotas()
	{
		$sql = 'SELECT * FROM conf_calificaciones';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene una configuracion de ponderacion de notas
	 * @param unknown_type $idconf_calificaciones
	 */
	public function obtenerUnaConfigPonderacionNotas($idconf_calificaciones)
	{
		$sql = 'SELECT * FROM conf_calificaciones WHERE idconf_calificaciones = ' . $idconf_calificaciones;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Insertar nueva ponderacion de notas
	 * @param unknown_type $autoevaluaciones
	 * @param unknown_type $scorm
	 * @param unknown_type $trabajos_practicos
	 * @param unknown_type $trabajo_equipo
	 * @param unknown_type $tutoria
	 * @param unknown_type $foro
	 * @param unknown_type $extra1
	 * @param unknown_type $extra2
	 * @param unknown_type $nota_coordinador
	 */
	public function insertarConfigPonderacionNotas($autoevaluaciones, $scorm, $trabajos_practicos, $trabajo_equipo, $tutoria, $foro, $extra1, $extra2, $nota_coordinador)
	{
		$sql = "SELECT idconf_calificaciones from conf_calificaciones
		where autoevaluaciones = $autoevaluaciones 
		and scorm = $scorm 
		and trabajos_practicos = $trabajos_practicos 
		and trabajo_equipo = $trabajo_equipo 
		and tutoria = $tutoria 
		and foro = $foro 
		and extra1 = $extra1 
		and extra2 = $extra2 
		and nota_coordinador = $nota_coordinador";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = 'INSERT conf_calificaciones' .
			' (autoevaluaciones, scorm, trabajos_practicos, trabajo_equipo, tutoria, foro, extra1, extra2, nota_coordinador)' .
			' VALUES (' . $autoevaluaciones . ', ' . $scorm . ', ' . $trabajos_practicos . ', ' . $trabajo_equipo . ', ' . $tutoria . ', ' . $foro . ', ' . $extra1 . ', ' . $extra2 . ', ' . $nota_coordinador . ')';
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}
	
	/**
	 * Edita una ponderacion de notas
	 * @param unknown_type $idconf_calificaciones
	 * @param unknown_type $autoevaluaciones
	 * @param unknown_type $scorm
	 * @param unknown_type $trabajos_practicos
	 * @param unknown_type $trabajo_equipo
	 * @param unknown_type $tutoria
	 * @param unknown_type $foro
	 * @param unknown_type $extra1
	 * @param unknown_type $extra2
	 * @param unknown_type $nota_coordinador
	 */
	public function editarConfigPonderacionNotas($idconf_calificaciones, $autoevaluaciones, $scorm, $trabajos_practicos, $trabajo_equipo, $tutoria, $foro, $extra1, $extra2, $nota_coordinador)
	{
		$sql = "SELECT idconf_calificaciones from conf_calificaciones
		where autoevaluaciones = $autoevaluaciones 
		and scorm = $scorm 
		and trabajos_practicos = $trabajos_practicos 
		and trabajo_equipo = $trabajo_equipo 
		and tutoria = $tutoria 
		and foro = $foro 
		and extra1 = $extra1 
		and extra2 = $extra2 
		and nota_coordinador = $nota_coordinador";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{		
			$sql = 'UPDATE conf_calificaciones SET' .
			' autoevaluaciones = ' . $autoevaluaciones . ', scorm = ' . $scorm . ', trabajos_practicos = ' . $trabajos_practicos .  ', trabajo_equipo = ' . $trabajo_equipo . ', tutoria = ' . $tutoria . ', foro = ' . $foro .
			', extra1 = ' . $extra1 . ', extra2 = ' . $extra2 . ', nota_coordinador = ' . $nota_coordinador .
			' WHERE idconf_calificaciones = ' . $idconf_calificaciones;
			$resultado = $this->consultaSql($sql);	
			return $resultado;
		}
	}
	
	public function eliminarConfigPonderacionNotas($idconf_calificaciones)
	{
		$sql = 'DELETE FROM conf_calificaciones' .
		' WHERE idconf_calificaciones = ' . $idconf_calificaciones;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarConfigNotas($idconf_calificaciones)
	{
		$sql = 'DELETE FROM conf_calificaciones_notas' .
		' WHERE idconf_calificaciones_notas = ' . $idconf_calificaciones;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Edita una configuracion de nota de un curso especifico
	 * @param unknown_type $idcurso
	 * @param unknown_type $autoevaluaciones
	 * @param unknown_type $scorm
	 * @param unknown_type $trabajos_practicos
	 * @param unknown_type $trabajo_equipo
	 * @param unknown_type $tutoria
	 * @param unknown_type $foro
	 * @param unknown_type $extra1
	 * @param unknown_type $extra2
	 * @param unknown_type $nota_coordinador
	 */
	public function editarConfigNotasPonderacionParaCurso($idcurso, $autoevaluaciones, $scorm, $trabajos_practicos, $trabajo_equipo, $tutoria, $foro, $extra1, $extra2, $nota_coordinador)
	{
		$sql = 'UPDATE curso SET' .
		' autoevaluaciones = ' . $autoevaluaciones . ', scorm = ' . $scorm . ', trabajos_practicos = ' . $trabajos_practicos . ', trabajo_equipo = ' . $trabajo_equipo . ', tutoria = ' . $tutoria . ', foro = ' . $foro .
		', extra1 = ' . $extra1 . ', extra2 = ' . $extra2 . ', nota_coordinador = ' . $nota_coordinador . ', trabajos_practicos = ' . $trabajos_practicos .
		' WHERE idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Insertar una nueva configuracion de notas
	 * @param unknown_type $n_accesos
	 * @param unknown_type $num_msg_foro
	 * @param unknown_type $num_trabajos
	 */
	public function insertarConfigNotas($num_msg_foro, $num_trabajos)
	{
		$sql = "SELECT * from conf_calificaciones_notas 
		where num_msg_foro = $num_msg_foro 
		and num_trabajos = $num_trabajos ";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = 'INSERT conf_calificaciones_notas' .
			' (num_msg_foro, num_trabajos)' .
			' VALUES (' . $num_msg_foro . ', ' . $num_trabajos. ')';
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}
	
	/**
	 * Editar una ponderacion de notas
	 * @param unknown_type $idconf_calificaciones
	 * @param unknown_type $n_accesos
	 * @param unknown_type $num_msg_foro
	 * @param unknown_type $num_trabajos
	 */
	public function editarConfigNotas($idconf_calificaciones, $num_msg_foro, $num_trabajos)
	{
		$sql = "SELECT * from conf_calificaciones_notas 
		where num_msg_foro = $num_msg_foro 
		and num_trabajos = $num_trabajos ";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = 'UPDATE conf_calificaciones_notas SET' .
			' num_msg_foro = ' . $num_msg_foro . 
			', num_trabajos = ' . $num_trabajos .
			' WHERE idconf_calificaciones_notas = ' . $idconf_calificaciones;
			$resultado = $this->consultaSql($sql);		
			return $resultado;
		}
	}
	
	public function editarConfigNotasParaCurso($idcurso, $num_msg_foro, $num_trabajos)
	{
		$sql = 'UPDATE curso SET' .
		' num_msg_foro = ' . $num_msg_foro . ', num_trabajos = ' . $num_trabajos .
		' WHERE idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	
	/*public function editarConfigNotasPonderacionParaCurso($idcurso, $n_accesos, $num_msg_foro, $num_trabajos)
	{
		$sql = 'UPDATE curso SET' .
		' n_accesos = ' . $n_accesos . ', num_msg_foro = ' . $num_msg_foro . ', num_trabajos = ' . $num_trabajos .
		' WHERE idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}*/
	
	/**
	 * Actualizar configuracion de notas de un curso especifico
	 * @param unknown_type $idcurso
	 * @param unknown_type $idconf_autoeval
	 * @param unknown_type $idconf_scorm
	 */
	public function actualizarConfigNotasAuto($idcurso, $idconf_autoeval, $idconf_scorm)
	{
		$sql = 'UPDATE curso SET' .
		' tipo_autoeval = ' . $idconf_autoeval . ', tipo_scorm = ' . $idconf_scorm .
		' WHERE idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene las opciones de una configuracion de notas automaticas
	 */
	public function obtenerConfigNotasAuto()
	{
		$sql = 'SELECT * FROM conf_calificaciones_auto';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene todos los cursos
	 */
	public function obtenerCursos()
	{
		$sql = 'SELECT * FROM curso';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene todos los cursos activos
	 */
	public function obtenerCursosActivos()
	{
		$sql = 'SELECT * FROM curso where borrado = 0';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtener un curso especifico
	 * @param unknown_type $idcurso
	 */
	public function obtenerUnCurso($idcurso)
	{
		$sql = 'SELECT * FROM curso AS c WHERE c.idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	// Obtiene todos los valores de conf_calificaciones_notas , que son numero accesos, numer mensajes foro y numero de trabajos obligatorios
	public function obtenerConfiguracionNotasAccesos()
	{
		$sql = 'SELECT * FROM conf_calificaciones_notas';
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	/**
	 * Obtener una configiguracion notas
	 */
	public function obtenerUnaConfiguracionNotasAccesos($idconf_calificaciones)
	{
		$sql = 'SELECT * FROM conf_calificaciones_notas WHERE idconf_calificaciones_notas = ' . $idconf_calificaciones;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}