$("#frmNuevoPonderacionNotas").validate({
		errorElement: "div",
		messages: {
			scorm: {
				required : 'Introduce un valor para el contenido scorm',
				digits   : 'Introduce un valor para el contenido scorm'
			},
			autoeval:	   {
				required  : 'Introduce un valor para las autoevaluaciones',
				digits    : 'Introduce un valor para las autoevaluaciones'
			},
			trabajos_practicos:	   {
				required  : 'Introduce un valor para los trabajos pr&aacute;cticos',
				digits    : 'Introduce un valor para los trabajos pr&aacute;cticos'
			},
			trabajo_equipo:	   {
				required  : 'Introduce un valor para los trabajos en equipo',
				digits    : 'Introduce un valor para los trabajos en equipo'
			},
			foro:	   {
				required  : 'Introduce un valor para los foro',
				digits    : 'Introduce un valor para los foro'
			},
			tutoria:	   {
				required  : 'Introduce un valor para las tutor&iacute;as',
				digits    : 'Introduce un valor para las tutor&iacute;as'
			},
			extra1:	   {
				required  : 'Introduce un valor para los trabajos extra 1',
				digits    : 'Introduce un valor para los trabajos extra 1'
			},
			extra2:	   {
				required  : 'Introduce un valor para los trabajos extra 2',
				digits    : 'Introduce un valor para los trabajos extra 2'
			},
			nota_coordinador:	   {
				required  : 'Introduce un valor para la nota del coordinador',
				digits    :	'Introduce un valor para la nota del coordinador'
			}
		},
		rules: {
			scorm : {
				required  : true,
				digits    : true
			},
			autoeval : {
				required  : true,
				digits    : true
			},
			trabajos_practicos : {
				required  : true,
				digits    : true
			},
			trabajo_equipo : {
				required  : true,
				digits    : true
			},
			foro : {
				required  : true,
				digits    : true
			},
			tutoria : {
				required  : true,
				digits    : true
			},
			extra1 : {
				required  : true,
				digits    : true
			},
			extra2 : {
				required  : true,
				digits    : true
			},
			nota_coordinador : {
				required  : true,
				digits    : true
			}
		}
	});