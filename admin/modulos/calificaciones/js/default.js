function intr_calf_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('calificaciones/introducir-calificaciones/' + idconv);
	}
}

function intr_calf_cambiar_curso(idconv, elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('calificaciones/introducir-calificaciones/' + idconv + '/' + idcurso);
	}
}

///////////////

function buscar_pond_notas_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('calificaciones/buscador-ponderacion-notas/' + idconv);
	}
}

/*
function buscar_pond_notas_cambiar_curso(idconv, elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		window.location.href = 'calificaciones/buscador-ponderacion-notas/' + idconv + '/' + idcurso;
	}
}
*/

//////////////////

function notas_auto_cambiar_curso(elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idcurso != 0)
	{
		urlRedirect('calificaciones/notas-automaticas/curso/' + idcurso);
	}
}

///////////////

function buscar_config_notas_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('calificaciones/configuracion-de-notas/' + idconv);
	}
}

//////////////////////

function admin_pond_notas_select_all_cursos(elemento)
{
	$("#ponderacion_notas_listado_cursos").find('input[type="checkbox"]').prop('checked', 'checked');
	
	$("#ponderacion_notas_listado_cursos_select_all").addClass("hide");
	$("#ponderacion_notas_listado_cursos_select_none").removeClass("hide");
}

function admin_pond_notas_select_none_cursos(elemento)
{
	$("#ponderacion_notas_listado_cursos").find('input[type="checkbox"]').removeProp('checked');
	
	$("#ponderacion_notas_listado_cursos_select_all").removeClass("hide");
	$("#ponderacion_notas_listado_cursos_select_none").addClass("hide");
}