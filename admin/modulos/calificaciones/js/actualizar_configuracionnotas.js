$("#frmNuevoConfiguracionNotas").validate({
		errorElement: "div",
		messages: {
			num_msg_foro: {
				required : 'Introduce un valor para el n&uacute;mero de mensajes de foro',
				digits   : 'Introduce un valor para el n&uacute;mero de mensajes de foro'
			},
			num_trabajos:	   {
				required  : 'Introduce un valor para el n&uacute;mero de trabajos por m&oacute;dulo',
				digits    : 'Introduce un valor para el n&uacute;mero de trabajos por m&oacute;dulo'
			}
		},
		rules: {
			num_msg_foro : {
				required  : true,
				digits    : true
			},
			num_trabajos : {
				required  : true,
				digits    : true
			}
		}
	});