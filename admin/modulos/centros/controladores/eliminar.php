<?php
$objModeloCentros = new AModeloCentros();

$get = Peticion::obtenerGet();
if(isset($get['idcentro']) && is_numeric($get['idcentro']))
{
	$rowCentro = $objModeloCentros->obtenerCentro($get['idcentro']);
	if($rowCentro->num_rows == 1)
	{
		if($objModeloCentros->eliminarCentro($get['idcentro']))
		{
			Alerta::guardarMensajeInfo('Se ha eliminado el centro');
		}
	}
}

Url::redirect('centros', true);