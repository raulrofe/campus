<?php
$objModeloCentros = new AModeloCentros();

$get = Peticion::obtenerGet();
if(isset($get['idcentro'], $get['idmsg'], $get['idadjuntomsg']) && is_numeric($get['idcentro']) && is_numeric($get['idmsg']) && is_numeric($get['idadjuntomsg']))
{
	// si existe el centro
	$rowCentro = $objModeloCentros->obtenerCentro($get['idcentro']);
	if($rowCentro->num_rows == 1)
	{
		$rowCentro = $rowCentro->fetch_object();
		
		// si existe el mensaje
		$rowMensaje = $objModeloCentros->obtenerUnMensajeCentro($get['idmsg']);
		if($rowMensaje->num_rows == 1)
		{
			// obtenemos el adjunto del mensaje
			$rowAdjMensaje = $objModeloCentros->obtenerUnAdjuntoMensaje($get['idadjuntomsg']);
			if($rowAdjMensaje->num_rows == 1)
			{
				$rowAdjMensaje = $rowAdjMensaje->fetch_object();
				
				if($objModeloCentros->eliminarAdjuntoMensajeCentro($get['idmsg'], $get['idadjuntomsg']))
				{
					// eliminamos el archivo fisicamente
					$file = PATH_ROOT . 'archivos/centros/' . $rowCentro->idcentros . '/' . $rowAdjMensaje->nombre_adjunto;
					if(is_file($file))
					{
						unlink($file);
					}
					
					Alerta::guardarMensajeInfo('Se ha eliminado el archivo adjunto de este mensaje de centro');
				}
			}
		}
	}
}

Url::redirect('centros', true);