<?php
$objModeloCentros = new AModeloCentros();

$get = Peticion::obtenerGet();
if(isset($get['idcentro'], $get['idmsg']) && is_numeric($get['idcentro']) && is_numeric($get['idmsg']))
{
	$rowCentro = $objModeloCentros->obtenerCentro($get['idcentro']);
	if($rowCentro->num_rows == 1)
	{
		if($objModeloCentros->eliminarMensajeCentro($get['idmsg']))
		{
			Alerta::guardarMensajeInfo('Se ha eliminado el mensaje del centro');
		}
	}
}

Url::redirect('centros', true);