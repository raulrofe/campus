<?php
$objModeloCentros = new AModeloCentros();

$get = Peticion::obtenerGet();
if(isset($get['idcentro']) && is_numeric($get['idcentro']))
{
	$rowCentro = $objModeloCentros->obtenerCentro($get['idcentro']);
	if($rowCentro->num_rows == 1)
	{
		$rowCentro = $rowCentro->fetch_object();
		
		if(isset($get['boton']))
		{
			switch($get['boton'])
			{
				case 'Insertar':
					if(Peticion::isPost())
					{
						$post = Peticion::obtenerPost();
						
						if(isset($post['mensaje']))
						{
							// para saber si por POST viene algun archivo
							$isPostFiles = false;
							foreach($_FILES['adjunto']['tmp_name'] as $i => $path)
							{
							 	if(!empty($path))
							 	{
							 		$isPostFiles = true;
								}
							}
							
							if(!empty($post['mensaje']) || $isPostFiles)
							{
								if($objModeloCentros->insertarMensajeCentro($get['idcentro'], Usuario::getIdUser(), $post['mensaje'], date('Y-m-d H:i:s')))
								{
									$idMsgCentro = $objModeloCentros->obtenerUltimoIdInsertado();
									
									if(!is_dir(PATH_ROOT . '/archivos/centros/' . $get['idcentro']))
									{
										mkdir(PATH_ROOT . '/archivos/centros/' . $get['idcentro']);
									}
									
									//$cont = 1;
									foreach($_FILES['adjunto']['tmp_name'] as $i => $path)
									{
										if(!empty($path))
										{
											$newNameFile = /*$cont . '_' . */Texto::quitarTildes($_FILES['adjunto']['name'][$i]);
											$objModeloCentros->insertarAdjuntoMensajeCentro($idMsgCentro, $newNameFile);
											move_uploaded_file($path, PATH_ROOT . '/archivos/centros/' . $get['idcentro'] . '/' . $newNameFile);
												
											//$cont++;
										}
									}
									
									Alerta::guardarMensajeInfo('Mensaje guardado en el centro');
									Url::redirect('centros/mensajes/' . $rowCentro->idcentros);
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('Debes poner alg&uacute;n mensaje y/o adjuntar alg&uacute;n archivo');
							}
						}
					}
					break;
					
				case 'Actualizar':
					if(isset($get['idmsg']) && is_numeric($get['idmsg']))
					{
						$rowMensaje = $objModeloCentros->obtenerUnMensajeCentro($get['idmsg']);
						if($rowMensaje->num_rows == 1)
						{
							$rowMensaje = $rowMensaje->fetch_object();
							
							//obtenemos los archivos adjuntos de este mensaje
							$mensajeCentroAdjunto = $objModeloCentros->obtenerAdjuntoMensajesCentro($get['idmsg']);
							
							// POST //
							if(Peticion::isPost())
							{
								$post = Peticion::obtenerPost();
								if(isset($post['mensaje']))
								{
									if($objModeloCentros->actualizarMensajeCentro($get['idmsg'], $post['mensaje']))
									{
										// ADJUNTAR ARCHIVOS
										if(!is_dir(PATH_ROOT . '/archivos/centros/' . $get['idcentro']))
										{
											mkdir(PATH_ROOT . '/archivos/centros/' . $get['idcentro']);
										}
										
										foreach($_FILES['adjunto']['tmp_name'] as $i => $path)
										{
											if(!empty($path))
											{
												$newNameFile = Texto::quitarTildes($_FILES['adjunto']['name'][$i]);
												$objModeloCentros->insertarAdjuntoMensajeCentro($get['idmsg'], $newNameFile);
												move_uploaded_file($path, PATH_ROOT . '/archivos/centros/' . $get['idcentro'] . '/' . $newNameFile);
											}
										}
									
										Alerta::guardarMensajeInfo('Se ha actualizado el mensaje del centro');
										Url::redirect('centros/mensajes/' . $get['idcentro']);
									}
								}
							}
						}
					}
					break;
				default:
			}
		}
		else
		{
			/* -------- BUSCADOR ------- */
			$busqueda = null;
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();
				if(isset($post['busqueda']) && !empty($post['busqueda']))
				{
					$busqueda = $post['busqueda'];
				}
			}
			/* -------- FIN BUSCADOR ------- */
			
			/* - PAGINADO - */
			$numPagina = 1;
			if(isset($_GET['pagina']) && is_numeric($_GET['pagina']))
			{
				$numPagina = $_GET['pagina'];
			}
				
			$centrosCount = $objModeloCentros->obtenerMensajesCentro($get['idcentro'], null, null, $busqueda);
			$maxElementsPaging = 7;
			$maxPaging = $centrosCount->num_rows;
			$maxPaging = ceil($maxPaging / $maxElementsPaging);
			
			$elementsIni = ($numPagina - 1) * $maxElementsPaging;
			/* --- FIN PAGINADO  -*/
	
			// obtenemos los mensajes creados para este centro
			$mensajesCentro = $objModeloCentros->obtenerMensajesCentro($get['idcentro'], $elementsIni, $maxElementsPaging, $busqueda);
		}
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'mensajes');
	}
}