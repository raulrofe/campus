<?php
$htmlLog = null;

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	$log = array();
	$filaCsv = 1;
	
	// si se ha subido un archivo del modo correcto ...
	if(isset($_FILES['archivo']['tmp_name']) && !empty($_FILES['archivo']['tmp_name']))
	{
		// comprobamos la extension del archivo (debe ser CSV)
		if(preg_match('#(.+)\.csv$#i', $_FILES['archivo']['name']))
		{
			// abre el archivo CSV
			if(($handle = fopen($_FILES['archivo']['tmp_name'], "r")) !== false)
			{
				$objModeloCentro = new AModeloCentros();
				
				// para evitar procesar la cabecera del CSV
				$cabecera = true;
				
				// contadores de alumnos
				$contSuccess = 0;
				$contTotal = 0;
				
				// se mueve por cada registro del CSV
			    while(($data = fgetcsv($handle, 0, ";")) !== false)
			    {
			    	// si no es la cabecera del archivo, es decir, no es la primera linea
			    	if(!$cabecera)
			    	{
			    		// volcamos los datos del CSV para trabajarlos mas facilmente
				       $cif = mb_strtoupper($data[17]);
				       $nombreCentro = utf8_encode(mb_strtolower($data[18]));
				       $razonSocial = utf8_encode(mb_strtolower($data[19]));
				       $tipoCentro = 2;
				       //$informeRlt = $data[10];		// en el excel es el RLT
				       //$valorInforme = $data[11];	//en el excel es el informe RLT
				       
				       //if(!empty($cif) && !empty($informeRlt))
				       if(!empty($cif))
				       {
				       		// valor por defecto para el informe RLT
				   			/*if(empty($valorInforme))
				   			{
				   				$in$valorInformeformeRlt = 'B';
				   			}*/
				   			
				   			// insertamos el centro si no existe
				   			$rowCentro = $objModeloCentro->obtenerCentroPorCIF($cif);
				   			if($rowCentro->num_rows == 0)
				   			{
				   				if($objModeloCentro->insertarCentro($cif, $razonSocial, $nombreCentro, '', '', '', '', '', '', '', '', '', '', '', '', $tipoCentro))
				   				{
				   					$idcentro = $objModeloCentro->obtenerUltimoIdInsertado();
				   					$contSuccess++;
				   				}
				   			}
				   			// sino obtenemos el id del centro
				   			else
				   			{
				   				$rowCentro = $rowCentro->fetch_object();
				   				$idcentro = $rowCentro->idcentros;
				   			}
				   			
				   			// asignamos el centro a la convocatoria, si no existe
				   			$rowCentroConv = $objModeloCentro->obtenerCentroConvocatoria($idcentro, $post['idconv']);
				   			if($rowCentroConv->num_rows == 0)
				   			{
					   			if($objModeloCentro->asignarCentroConvocatoria($idcentro, $post['idconv']))
					   			{
					   				$contSuccess++;
					   			}
				   			}
				       }
				       else
				       {
				       		$log[] = 'El CIF y el Informe RLT no pueden esta vacios en la fila ' . $filaCsv;
				       }
			    	}
			    	else
			    	{
			    		$cabecera = false;
			    	}
			    	
			    	$contTotal++;
			    	$filaCsv++;
			    }
			    fclose($handle);
			}
			
			// estadisticas de la carga masiva
			$htmlLog .= '<div>Total centros: ' . $contTotal . '</div><div>Total centros insertados: ' . $contSuccess . '</div><div>Total centros no insertados: ' . ($contTotal - $contSuccess) . '</div><br />';
		}
		else
		{
			$log[] = 'El archivo debe ser tipo CSV';
		}
	}
	else
	{
		$log[] = 'Selecciona un archivo CSV para hacer la carga masiva';
	}
	
	// errores durante el proceso de la carga
	if(count($log) > 0)
	{
		foreach($log as $item)
		{
			$htmlLog .= '<div>- ' . $item . '</div>';
		}
	}
}

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'carga_masiva');