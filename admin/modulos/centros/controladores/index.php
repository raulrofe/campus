<?php
$objModeloCentros = new AModeloCentros();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

var_dump($post);

if(isset($post['boton']))
{
	if(Peticion::isPost())
	{
		if($post['boton'] == 'Insertar')
		{
			if(isset($post['cif'], $post['razon_social'], $post['nombre_centro'], $post['telefono'], $post['fax'], $post['email'], 
					 $post['direccion'], $post['provincia'], $post['localidad'], $post['tipo_centro']))
			{

				if(isset($post['tipo_centro']) && is_numeric($post['tipo_centro']) && (!empty($post['cif']) || !empty($post['razon_social']) || !empty($post['nombre_centro'])))
				{
					if($objModeloCentros->insertarCentro($post['cif'], $post['razon_social'], $post['nombre_centro'], $post['telefono'],
					   $post['fax'], $post['email'], $post['direccion'], $post['provincia'], $post['localidad'], $post['tipo_centro']))
					{
						Alerta::guardarMensajeInfo('Se ha a&ntilde;adido el centro');	
					}
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('cumplimentarcampos','Debes cumplimentar todos los campos');
			}
		}
		else if($post['boton'] == 'Actualizar')
		{
			if(isset($post['cif'], $post['razon_social'], $post['nombre_centro'], $post['telefono'], $post['fax'], $post['email'], $post['direccion'], $post['provincia'], $post['localidad'],
			$post['tipo_centro'], $post['idCentro']) && is_numeric($post['idCentro']))
			{
				if($objModeloCentros->editarCentro($post['idCentro'], $post['cif'], $post['razon_social'], $post['nombre_centro'], $post['telefono'], $post['fax'], $post['email'],
					$post['direccion'], $post['provincia'], $post['localidad'], $post['tipo_centro']))
				{
					Alerta::guardarMensajeInfo('Se han actualizado los datos del centro');
					
					Url::redirect('centros');
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('cumplimentarcampos','Debes cumplimentar todos los campos');
			}
		}
	}
}


// para la edicion de centros
if(isset($get['idcentro']) && is_numeric($get['idcentro']))
{
	$rowCentro = $objModeloCentros->obtenerCentro($get['idcentro']);
	if($rowCentro->num_rows == 1)
	{		
		$rowCentro = $rowCentro->fetch_object();
	}
}

/* -------- BUSCADOR ------- */
$busqueda = null;
if(isset($get['busqueda']))
{
	$busqueda = $get['busqueda'];
}
/* -------- FIN BUSCADOR ------- */

/* - PAGIANADO - */
$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $_GET['pagina'];
}
	
$centrosCount = $objModeloCentros->obtenerCentros(null, null, $busqueda);
$maxElementsPaging = 20;
$maxPaging = $centrosCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;
/* --- FIN PAGINADO  -*/

// usamos paginadoo obtenido anteriormente para obtener los centro
$centros = $objModeloCentros->obtenerCentros($elementsIni, $maxElementsPaging, $busqueda);


$tipoCentros = $objModeloCentros->getTipoCentro();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');