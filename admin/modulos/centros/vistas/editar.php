<div id="admin_usuarios">
	<p class="subtitleAdmin">Centros - Editar</p>
		  				
  	<div class="formulariosAdmin">
		  <form id="frmCentro" action="" method="post">
		  				<ul>
		  					<li>
		  						<label class="negrita">CIF</label>
		  						<input type="text" name="cif" value="<?php echo Texto::textoPlano($rowCentro->cif)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Raz&oacute;n social</label>
		  						<input type="text" name="razon_social" value="<?php echo Texto::textoPlano($rowCentro->razon_social)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Nombre centro</label>
		  						<input type="text" name="nombre_centro" value="<?php echo Texto::textoPlano($rowCentro->nombre_centro)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Tel&eacute;fono</label>
		  						<input type="text" name="telefono" value="<?php echo Texto::textoPlano($rowCentro->telefono)?>" maxlength="9" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Fax</label>
		  						<input type="text" name="fax" value="<?php echo Texto::textoPlano($rowCentro->fax)?>" maxlength="9" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Email</label>
		  						<input type="text" name="email" value="<?php echo Texto::textoPlano($rowCentro->email)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Direcci&oacute;n</label>
		  						<input type="text" name="direccion" value="<?php echo Texto::textoPlano($rowCentro->direccion)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Provincia</label>
		  						<input type="text" name="provincia" value="<?php echo Texto::textoPlano($rowCentro->provincia)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Localidad</label>
		  						<input type="text" name="localidad" value="<?php echo Texto::textoPlano($rowCentro->localidad)?>" />
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<label class="negrita">Tipo centro</label>
		  						<select name="tipo_centro">
		  							<?php while($tipoCentro = $tipoCentros->fetch_object()):?>
		  								<?php if($rowCentro->tipo_centro == $tipoCentro->idtipo_centro):?>
		  									<option value="<?php echo $tipoCentro->idtipo_centro; ?>" selected><?php echo $tipoCentro->tipo_centro; ?></option>
		  								<?php else: ?>
		  									<option value="<?php echo $tipoCentro->idtipo_centro; ?>"><?php echo $tipoCentro->tipo_centro; ?></option>
		  								<?php endif; ?>
		  							<?php endwhile; ?>
		  						</select>
		  						<div class="clear"></div>
		  					</li>
		  					<li>
		  						<input type="submit" value="Guardar" />
		  						<input type="hidden" name="boton" value="Actualizar" />
		  						<input type="hidden" name="idCentro" value="<?php echo $get['idcentro']; ?>" />
		  					</li>
		  				</ul>
		  				<div class="clear"></div>
		  			</form>
		  		</div>
</div>