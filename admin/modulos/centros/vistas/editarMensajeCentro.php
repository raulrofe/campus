<p class="subtitleAdmin">Editar mensaje para el centro: <?php echo Texto::textoPlano($rowCentro->nombre_centro)?></p>

<div class="formulariosAdmin">	
	<form id="frmNuevoMsgCentro" method="post" action="" enctype="multipart/form-data">
		 <ul>
		  	<li>
		  		<label>Mensaje</label>
		  		<textarea name="mensaje" rows="1" cols="1"><?php echo $rowMensaje->mensaje?></textarea>
				<div class="clear"></div>
		  	</li>
		  	<li>
		  		<label>Archivo adjunto</label>
				<div class="clear"></div>
		  		<input type="file" name="adjunto[]" class="multi" />
				<div class="clear"></div>
		  	</li>
		  	<li>
		  		<input type="submit" value="Guardar" />
		  	</li>
		  </ul>
		<div class="clear"></div>
	</form>
		  			
	<?php if($mensajeCentroAdjunto->num_rows > 0):?>
		<?php while($item = $mensajeCentroAdjunto->fetch_object()):?>
			<ul>
				<li>
					- <a href="../archivos/centros/<?php echo $rowCentro->idcentros?>/<?php echo Texto::textoPlano($item->nombre_adjunto)?>" title=""><?php echo Texto::textoPlano($item->nombre_adjunto)?></a>
					- <a href="#" <?php echo Alerta::alertConfirmOnClick('borrardocumento', '¿Deseas borrar el documento de este mensaje del centro?', 'centros/' . $rowCentro->idcentros . '/mensajes/' . $rowMensaje->idcentro_mensaje . '/adjunto/eliminar/' . $item->idcentro_mensaje_adjunto)?> title="">Eliminar</a>
				</li>
			</ul>
		<?php endwhile;?>
	<?php endif;?>
</div>