<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Centros</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
	
  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas 
	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="centros">Listado</a></div>
	  		<div class="btn_navegacion"><a href="centros/nuevo">Nuevo</a></div>
	  		<div class="btn_navegacion"><a href="centros/carga-masiva">Carga masiva</a></div>
	  	</div>
	  	-->
	  	
		  	<div class="tab">
		  		<div class="submenuAdmin">CENTROS</div>
		  		<div id="centros">
		  			<div class='addAdmin'><a href='centros/nuevo'>Introducir Centro</a></div>
		  			<div class='buscador'>
		  				<p class='t_center'>Buscador de centros que hay en el sistema en este momento</p><br/>
			  			<div id='centrosBuscador' class='t_center'>
			  				<form method="post" action="" onsubmit="buscadorCentrosCambiarUrl();return false;" class='t_center'>
			  					<input type="text" name="busqueda" value="<?php echo Texto::textoPlano($busqueda)?>" style='width:400px;' />
			  					<a href="#" onclick="return false;" title="Busca por el CIF, la Razon Social o el Nombre de Centro" rel="tooltip"><img src="../imagenes/help_icon.png" alt="" /></a>
			  					<br/><br/>
			  					<input type="submit" value="Buscar" />
			  				</form>
			  			</div>
			  		</div>
			  		
		  			<?php if(isset($get['busqueda'])):?>
			  			<?php if($centros->num_rows > 0):?>
			  				<p class='subtitleAdmin'>LISTADO DE CENTROS</p>
			  				<div class="listarElementos">
								<?php while($centro = $centros->fetch_object()):?>
									<div class="elemento">
										<div class="elementoContenido">
											<p>
												<?php if(!empty ($centro->nombre_centro)):?>
													<?php echo $centro->cif . " - " . Texto::textoPlano($centro->nombre_centro)?>
												<?php else:?>
													<?php echo $centro->cif . " - " . Texto::textoPlano($centro->razon_social)?>
												<?php endif;?>
											</p>
										</div>
										<div class="elementoPie">
											<ul class="elementoListOptions fright">
												<li><a rel="tooltip" href="centros/mensajes/<?php echo $centro->idcentros?>" title="Administrar los mensajes y archivos asignados a este centro">Mensajes y archivos del centro</a></li>
												<li><a rel="tooltip" href="centros/editar/<?php echo $centro->idcentros?>" title="Editar este centro"><img src="../imagenes/options/edit.png" alt="" /></a></li>
												<li><a rel="tooltip" href="#" <?php echo Alerta::alertConfirmOnClick('borrarcentro', '¿Deseas borrar este centro?', 'centros/eliminar/' . $centro->idcentros)?> title="Borrar este centro"><img src="../imagenes/options/delete.png" alt="" /></a></li>
											</ul>
											<div class="clear"></div>
										</div>
									<div class="clear"></div>
									</div>
								<?php endwhile;?>
								
								<?php echo Paginado::crear($numPagina, $maxPaging, 'centros/pagina/', '?busqueda=' . Texto::textoPlano($busqueda))?>
							</div>
						<?php else:?>
							<div class="elementosNoEncontrados">No hay centros</div>
						<?php endif;?>
					<?php endif; ?>
					
					<?php if(isset($get['boton'])):?>
						<?php if($get['boton'] == 'Insertar'):?>
							<?php require_once('nuevo.php');?>
						<?php else:?>
							<?php require_once('editar.php');?>
						<?php endif;?>
					<?php endif; ?>					
		  		</div>
		  	</div>
	</div>
</div>