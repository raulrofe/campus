 			<div>
		  				
		  				<p class="subtitleAdmin">Administrar mensajes y archivos del centro: <?php echo Texto::textoPlano($rowCentro->nombre_centro)?></p>
		  				
		  				<?php if($mensajesCentro->num_rows > 0):?>
			  				<div class="listarElementos">
								<?php while($mensajeCentro = $mensajesCentro->fetch_object()):?>
									<?php $mensajeCentroAdjunto = $objModeloCentros->obtenerAdjuntoMensajesCentro($mensajeCentro->idcentro_mensaje)?>
									
									<div class="elemento">
										<div class="elementoContenido"><p>
											(<?php echo date('d/m/y H:i', strtotime($mensajeCentro->fecha))?>)
											<?php if(empty($mensajeCentro->mensaje)):?>
												<span class="cursiva">Este mensaje no tiene t&iacute;tulo</span>
											<?php else:?>
												<?php echo Texto::textoFormateado($mensajeCentro->mensaje)?>
											<?php endif;?>
										</p></div>
										<?php if($mensajeCentroAdjunto->num_rows > 0):?>
											<?php while($item = $mensajeCentroAdjunto->fetch_object()):?>
												<ul>
													<li>
														- <a href="../archivos/centros/<?php echo $rowCentro->idcentros?>/<?php echo Texto::textoPlano($item->nombre_adjunto)?>" title="" target="_blank"><?php echo Texto::textoPlano($item->nombre_adjunto)?></a>
													</li>
												</ul>
											<?php endwhile;?>
										<?php endif;?>
										<div class="elementoPie hide">
											<ul class="elementoListOptions fright">
												<li><a href="centros/<?php echo $rowCentro->idcentros?>/mensajes/editar/<?php echo $mensajeCentro->idcentro_mensaje?>" title="Editar este centro">Editar</a></li>
												<li><a href="#" <?php echo Alerta::alertConfirmOnClick('borrarmensajecentro','¿Deseas borrar este mensaje de centro?', 'centros/' . $rowCentro->idcentros . '/mensajes/eliminar/' . $mensajeCentro->idcentro_mensaje)?> title="Borrar este centro">Borrar</a></li>
											</ul>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</div>
								<?php endwhile;?>
								
								<?php echo Paginado::crear($numPagina, $maxPaging, 'centros/mensajes/' . $rowCentro->idcentros . '/pagina/')?>
							</div>
						<?php else:?>
							<div class="elementosNoEncontrados">No hay mensajes ni documentos para este centro</div>
						<?php endif;?>
		  			</div>