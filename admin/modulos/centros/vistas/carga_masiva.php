<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Centros - Carga Masiva</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
	
  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas 
	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="centros">Listado</a></div>
	  		<div class="btn_navegacion"><a href="centros/nuevo">Nuevo</a></div>
	  		<div class="btn_navegacion"><a href="centros/carga-masiva">Carga masiva</a></div>
	  	</div>
	  	-->
	  	
		  	<div class="tab">
		  		<div class="submenuAdmin">CENTROS - CARGA MASIVA</div>
					<div class="formulariosAdmin">
			  		<form method="post" action="" enctype="multipart/form-data">
				  			<ul>
				  				<li><input type="file" name="archivo" /></li>
				  				<li>
				  					<select name="idconv">
				  						<?php while($conv = $convocatorias->fetch_object()):?>
				  							<option value="<?php echo $conv->idconvocatoria?>"><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
				  						<?php endwhile;?>
				  					</select>
				  				</li>
				  				<li><button type="submit">Cargar</button></li>
				  			</ul>
				  			<div>
				  				<a href="../archivos/plantilla.csv" title="">Descargar plantilla CSV</a>
				  				<a href="../archivos/plantilla.xlsx" title="">Descargar plantilla Excel</a>
				  			</div>
				  		</form>
				  		<?php echo $htmlLog;?>
			  		</div>
			  	</div>
	  </div>
</div>