<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Centros</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
	
  	<div class="stContainer" id="tabs">
		  	<div class="tab">
		  		<div class="submenuAdmin">CENTROS - MENSAJES</div>
		  		<div id="centros">
		  		
		  			<div class='addAdmin'><a href='centros/<?php echo $get['idcentro']?>/mensajes/nuevo'>Introducir Mensaje Centro</a></div>
		  			<div class='buscador'>
		  				<p class='t_center'>Buscador de centros que hay en el sistema en este momento</p><br/>
			  			<div id='centrosBuscador' class='t_center'>
			  				<form method="post" action="">
			  					<div class="t_center">
			  						<input type="text" name="busqueda" style="width: 400px;" value="<?php if(isset($get['busqueda'])) echo Texto::textoPlano($busqueda)?>" />
			  						<br /><br />
			  					</div>
			  					<div class="t_center">
			  						<input type="submit" value="Buscar" />
			  					</div>
			  				</form>
			  			</div>
			  		</div>
						
					<?php 
					if(isset($get['boton']))
					{
						switch($get['boton'])
						{
							case 'Insertar':
								require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoMensaje');
								break;
							case 'Actualizar':
								require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editarMensajeCentro');
								break;
							default:
						}
					}
					else
					{
						require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listadoMensajes');
					}
					?>
		  		</div>
		  	</div>
	</div>
</div>