<p class="subtitleAdmin">Nuevo mensaje para el centro: <?php echo Texto::textoPlano($rowCentro->nombre_centro)?></p>

<div class="formulariosAdmin">	  				
	<form id="frmNuevoMsgCentro" method="post" action="" enctype="multipart/form-data">
		<ul>
			<li>
				<label>Mensaje</label>
				<textarea name="mensaje" rows="1" cols="1"></textarea>
				<div class="clear"></div>
			</li>
			<li>
				<label>Archivo adjunto</label>
				<div class="clear"></div>
				<input type="file" name="adjunto[]" class="multi" />
				<div class="clear"></div>
			</li>
			<li class="t_right">
				<input type="submit" value="Guardar" />
			</li>
		</ul>
	</form>
</div>