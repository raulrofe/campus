  <div id="centros">
  	<p class="subtitleAdmin">NUEVO CENTRO</p>
  	<div class="formulariosAdmin">
  		<form id="frmCentro" action="" method="post">
  			<ul>
  				<li>
  					<label>CIF</label>
  					<input type="text" name="cif" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Raz&oacute;n social</label>
  					<input type="text" name="razon_social" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Nombre centro</label>
  					<input type="text" name="nombre_centro" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Tel&eacute;fono</label>
  					<input type="text" name="telefono" maxlength="9"/>
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Fax</label>
  					<input type="text" name="fax" maxlength="9" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Email</label>
  					<input type="text" name="email" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Direcci&oacute;n</label>
  					<input type="text" name="direccion" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Provincia</label>
  					<input type="text" name="provincia" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Localidad</label>
  					<input type="text" name="localidad" />
  					<div class="clear"></div>
  				</li>
  				<li>
  					<label>Tipo centro</label>
  					<select name="tipo_centro">
  						<?php while($tipoCentro = $tipoCentros->fetch_object()):?>
  							<option value="<?php echo $tipoCentro->idtipo_centro; ?>"><?php echo $tipoCentro->tipo_centro; ?></option>
  						<?php endwhile; ?>
  					</select>
  					<div class="clear"></div>
  				</li>

  				<li class="t_right">
  					<input type="submit" value="Guardar" />
  					<input type="hidden" name="boton" value="Insertar" />
  				</li>
  			</ul>
  			<div class="clear"></div>
  		</form>
  	</div>
  </div>