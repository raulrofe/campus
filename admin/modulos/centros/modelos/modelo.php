<?php
class AModeloCentros extends modeloExtend
{
	public function obtenerCentros($limitIni = null, $limitEnd = null, $search = null)
	{
		$addQuery1 = null;
		if(isset($search))
		{
			$search = preg_quote($search, '-');
			$addQuery1 = ' AND (nombre_centro LIKE "%' . $search . '%" OR cif LIKE "%' . $search . '%" OR razon_social LIKE "%' . $search . '%")';
		}
		
		$addQuery2 = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery2 = ' ORDER BY cif ASC LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = 'SELECT * FROM centros' .
		' WHERE borrado = 0' . $addQuery1 . $addQuery2;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarCentro($cif, $razon_social, $nombre_centro = '', $telefono = '', $fax = '', $email = '', $direccion = '', 
					$provincia = '', $localidad = '', $tipo_centro = '')
	{
		echo $sql = 'INSERT INTO centros (cif, razon_social, nombre_centro, telefono, fax, email, direccion, provincia, localidad, tipo_centro)' .
		' VALUES ("' . $cif . '", "' . $razon_social . '", "' . $nombre_centro . '", "' . $telefono . '", "' . $fax . '", "' . $email . '", "' . $direccion . '", "' .
		$provincia . '", "' . $localidad . '", "' . $tipo_centro . '")';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function editarCentro($idcentro, $cif, $razon_social, $nombre_centro, $telefono, $fax, $email, $direccion, $provincia, $localidad, $tipo_centro,
		$informe_rlt,  $valorinf,  $fechadis, $resuelto15, $representante_legal, $recibir_boletin)
	{
		$sql = 'UPDATE centros SET cif ="' . $cif . '" , razon_social = "' . $razon_social . '", nombre_centro = "' . $nombre_centro . '", telefono = "' . $telefono . '",' .
		' fax = "' . $fax . '", email = "' . $email . '", direccion = "' . $direccion . '", provincia = "' . $provincia . '", localidad = "' . $localidad . '",' .
		' tipo_centro = "' . $tipo_centro . '", informe_rlt = "' . $informe_rlt . '",  valorinf = "' . $valorinf . '",  fechadis = "' . $fechadis . '", resuelto15 = "' . $resuelto15 . '",' .
		' representante_legal = "' . $representante_legal . '", recibir_boletin = "' . $recibir_boletin . '"' .
		' WHERE idcentros = ' . $idcentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function asignarCentroConvocatoria($idcentro, $idconvocatoria)
	{
		$sql = 'INSERT INTO centros_convocatorias (idcentro, idconvocatoria) VALUES (' . $idcentro . ', ' . $idconvocatoria . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerCentroConvocatoria($idcentro, $idconvocatoria)
	{
		$sql = 'SELECT * FROM centros_convocatorias WHERE idcentro = ' . $idcentro . ' AND idconvocatoria = ' . $idconvocatoria;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerCentro($idcentro)
	{
		$sql = 'SELECT * FROM centros WHERE borrado = 0 AND idcentros = ' . $idcentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerCentroPorCIF($cif)
	{
		$sql = 'SELECT * FROM centros WHERE borrado = 0 AND cif = "' . $cif . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function eliminarCentro($idcentro)
	{
		$sql = 'UPDATE centros SET borrado = 1 WHERE idcentros = ' . $idcentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarMensajeCentro($idcentro, $idrrhh, $mensaje, $fecha)
	{
		$sql = 'INSERT INTO centros_mensajes (idcentro, idrrhh, mensaje, fecha) VALUES (' . $idcentro . ', ' . $idrrhh . ', "' . $mensaje . '", "' . $fecha . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarAdjuntoMensajeCentro($idMsgCentro, $adjunto)
	{
		$sql = 'INSERT INTO centros_mensajes_adjuntos (idcentro_mensaje, nombre_adjunto) VALUES (' . $idMsgCentro . ', "' . $adjunto . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajesCentro($idCentro, $limitIni = null, $limitEnd = null, $search = null)
	{
		$addQuery1 = null;
		if(isset($search))
		{
			$search = preg_quote($search, '-');
			$addQuery1 = ' AND mensaje LIKE "%' . $search . '%"';
		}
		
		$addQuery2 = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery2 = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = 'SELECT cm.idcentro_mensaje, cm.mensaje, cm.fecha FROM centros_mensajes AS cm' .
		' WHERE cm.borrado = 0 AND cm.idcentro = ' . $idCentro . $addQuery1 .
		' GROUP BY cm.idcentro_mensaje ORDER BY cm.idcentro_mensaje DESC' . $addQuery2;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnMensajeCentro($idMensajeCentro)
	{
		$sql = 'SELECT * FROM centros_mensajes AS cm' .
		' WHERE cm.idcentro_mensaje = ' . $idMensajeCentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarMensajeCentro($idMensajeCentro, $mensaje)
	{
		$sql = 'UPDATE centros_mensajes SET mensaje = "' . $mensaje . '"' .
		' WHERE idcentro_mensaje = ' . $idMensajeCentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAdjuntoMensajesCentro($idMensajeCentro)
	{
		$sql = 'SELECT cma.idcentro_mensaje_adjunto, cma.nombre_adjunto FROM centros_mensajes_adjuntos AS cma' .
		' WHERE cma.idcentro_mensaje = ' . $idMensajeCentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAdjuntoMensaje($idAdjMensaje)
	{
		$sql = 'SELECT * FROM centros_mensajes_adjuntos' .
		' WHERE idcentro_mensaje_adjunto = ' . $idAdjMensaje;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarMensajeCentro($idMsgCentro)
	{
		$sql = 'UPDATE centros_mensajes SET borrado = 1 WHERE idcentro_mensaje = ' . $idMsgCentro;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarAdjuntoMensajeCentro($idmsg, $idAdjuntoMsg)
	{
		$sql = 'DELETE FROM centros_mensajes_adjuntos WHERE idcentro_mensaje_adjunto = ' . $idAdjuntoMsg . ' AND idcentro_mensaje = ' . $idmsg;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerCentrosRLT()
	{	
		$sql = 'SELECT * FROM centros' .
		' WHERE borrado = 0 and informe_rlt = "S"' .
		' ORDER BY cif ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerCentrosRLTConvocatoria($idConv)
	{	
		$sql = 'SELECT * FROM centros C, accionformativarlt AFRLT' .
		' WHERE C.borrado = 0 and C.informe_rlt = "S"' .
		' and C.idcentros = AFRLT.idCentro' .
		' and AFRLT.idConvocatoria = ' .$idConv .
		' GROUP BY C.idcentros' .
		' ORDER BY C.cif ASC';
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function getTipoCentro()
	{
		$sql = "SELECT * FROM tipo_centro";

		$result = $this->consultaSql($sql);
		
		return $result;
	}
}