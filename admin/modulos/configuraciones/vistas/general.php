<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="configuracion/general">Configuraci&oacute;n</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="configuracion/general">Configuraci&oacute;n general</a></li>
    <li class="clear"></li>
</ul>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>CONFIGURACI&Oacute;N GENERAL</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmEditarPonderacionNotas" action='' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Responsable</label>
                        	<div class="controls"><input type="text" name="responsable" value="<?php echo Texto::textoPlano($rowConfig->responsable)?>" /></div>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Tel&eacute;fono contacto</label>
	                        <div class="controls"><input type="text" name="telefono_contacto" value="<?php echo Texto::textoPlano($rowConfig->telefono_contacto)?>" /></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>
		         
                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">CIF</label>
                        	<div class="controls"><input type="text" name="cif" value="<?php echo Texto::textoPlano($rowConfig->cif)?>" /></div>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Nombre entidad</label>
	                        <div class="controls"><input type="text" name="nombre_entidad" value="<?php echo Texto::textoPlano($rowConfig->nombre_entidad)?>" /></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>		 

                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Direcci&oacute;n</label>
                        	<div class="controls"><input type="text" name="direccion" value="<?php echo $rowConfig->direccion?>" /></div>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">C&oacute;digo postal</label>
	                        <div class="controls"><input type="text" name="cp" value="<?php echo $rowConfig->cp?>" /></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>	

                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Localidad</label>
                        	<div class="controls"><input type="text" name="localidad" value="<?php echo $rowConfig->localidad?>" /></div>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Tel&eacute;fono</label>
	                        <div class="controls"><input type="text" name="telefono" value="<?php echo $rowConfig->telefono?>" /></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>	

                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Email sistema</label>
                        	<div class="controls"><input type="text" name="email_sistema" value="<?php echo $rowConfig->email_sistema?>" /></div>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Email secretaria</label>
	                        <div class="controls"><input type="text" name="email_secretaria" value="<?php echo $rowConfig->email_secretaria?>" /></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>
	
                <div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                    	<label for="columns-text" class="control-label">Email notificaciones</label>
                        	<div class="controls"><input type="text" name="email_notificaciones" value="<?php echo $rowConfig->email_notificaciones?>" /></div>
                        </div>
					</div>
		                
		            <div class="clear"></div>
		         </div>
		     </div>
		     
		     <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-save"></i> Guardar configuraci&oacute;n</button></div>
		     
		 </form>
	</div>
</div>