<?php $url_img_config = URL_BASE . "/archivos/config_web/";?>
<div id="admin_usuarios">
	<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
	    <li class="float-left"><a href="configuracion/grafica">Configuraci&oacute;n</a><span class="divider"><i class="icon-angle-right"></i></span></li>
	    <li class="float-left"><a href="configuracion/grafica">Configuraci&oacute;n gr&aacute;fica</a></li>
	    <li class="clear"></li>
	</ul>
	
  	<div class="stContainer" id="tabs">  
  	<div class="tab">	 
  	<div class="submenuAdmin">CONFIGURACI&Oacute;N GR&Aacute;FICA</div>  
		  	<div id="configuracion_admin" class="tab">
		  		<form method="post" action="" enctype="multipart/form-data">
		  			<div class="tituloGrafica"><label>Cambiar logo</label></div>
		  			<div>
		  				<?php if(!empty($rowConfig->imagen_logo)):?>
		  					<div><img src="<?php echo $url_img_config . $rowConfig->imagen_logo;?>" alt="" /></div>
		  				<?php else: ?>
		  					<p class="imagenVacia">No existe imagen</p>
		  				<?php endif ?>
		  				<input type="file" name="logo" />
		  				<a href="configuracion/grafica/eliminar/1" title=""> | Eliminar imagen</a>
		  				<div class="clear"></div>
		  			</div>
					<br/>
					
					<div class="tituloGrafica"><label>Cambiar fondo p&uacute;blico</label></div>
					<?php if(!empty($rowConfig->imagen_fondo_publico)):?>
						<div><img src="<?php echo $url_img_config . $rowConfig->imagen_fondo_publico;?>" alt="" /></div>
		  			<?php else: ?>
		  				<p class="imagenVacia">No existe imagen</p>						
					<?php endif ?>
		  			<div>
		  				<input type="file" name="fondo_publico" />
		  				<a href="configuracion/grafica/eliminar/2" title=""> | Eliminar imagen</a>
		  				<div class="clear"></div>
		  			</div>
		  			<br/>
		  			
		  			<div class="tituloGrafica"><label>Cambiar fondo admin</label></div>
		  			<?php if(!empty($rowConfig->imagen_fondo_admin)):?>
		  				<div><img src="<?php echo $url_img_config . $rowConfig->imagen_fondo_admin;?>" alt="" /></div>
		  			<?php else: ?>
		  				<p class="imagenVacia">No existe imagen</p>
		  			<?php endif ?>
		  			<div>
		  				<input type="file" name="fondo_admin" />
		  				<a href="configuracion/grafica/eliminar/3" title=""> | Eliminar imagen</a>
		  				<div class="clear"></div>
		  			</div>
		  			<br/>
		  			
					<div class="tituloGrafica"><label>Cambiar favicon (.ico)</label></div>
					<?php if(!empty($rowConfig->imagen_favicon)):?>
						<div><img src="<?php echo $url_img_config . $rowConfig->imagen_favicon;?>" alt="" /></div>
					<?php else: ?>
		  				<p class="imagenVacia">No existe imagen</p>
					<?php endif ?>
		  			<div>
		  				<input type="file" name="favicon" />
		  				<a href="admin/configuracion/grafica/eliminar/4" title=""> | Eliminar imagen</a>
		  				<div class="clear"></div>
		  			</div>
		  			<br/>
					<input type="submit" value="Guardar" />
		  		</form>
		  	</div>
	</div>
	</div>
</div>