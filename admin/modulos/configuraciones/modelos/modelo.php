<?php
class AModeloConfig extends modeloExtend
{
	public function obtenerConfig()
	{
		$sql = 'SELECT * FROM entidad_organizadora';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function insertarConfig()
	{
		$sql = 'INSERT INTO entidad_organizadora (responsable) VALUES ("")';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function actualizarConfig($responsable, $telefono_contacto, $cif, $nombre_entidad, $direccion, $cp, $localidad, $telefono, $email_sistema, $email_secretaria, $email_notificaciones)
	{
		$sql = 'UPDATE entidad_organizadora SET responsable = "' . $responsable . '", telefono_contacto = "' . $telefono_contacto . '", cif = "' . $cif . '", ' .
		'nombre_entidad = "' . $nombre_entidad . '", direccion = "' . $direccion . '", cp = "' . $cp . '", localidad = "' . $localidad . '", telefono = "' . $telefono . '", ' .
		'email_sistema = "' . $email_sistema . '", email_secretaria = "' .$email_secretaria . '", email_notificaciones = "' . $email_notificaciones . '"';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function actualizarLogo($nameImage)
	{
		$sql = 'UPDATE entidad_organizadora SET imagen_logo = "' . $nameImage . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFondoPublico($nameImage)
	{
		$sql = 'UPDATE entidad_organizadora SET imagen_fondo_publico = "' . $nameImage . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFondoAdmin($nameImage)
	{
		$sql = 'UPDATE entidad_organizadora SET imagen_fondo_admin = "' . $nameImage . '"';

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFavicon($nameImage)
	{
		$sql = 'UPDATE entidad_organizadora SET imagen_favicon = "' . $nameImage . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}