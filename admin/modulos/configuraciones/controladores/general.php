<?php
$objModeloConfig = new AModeloConfig();

$rowConfig = $objModeloConfig->obtenerConfig();
if($rowConfig->num_rows == 1)
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		if(isset($post['responsable'], $post['telefono_contacto'], $post['cif'], $post['nombre_entidad'], $post['direccion'], $post['cp'], $post['localidad'], $post['telefono'],
			$post['email_sistema'], $post['email_secretaria'], $post['email_notificaciones']))
		{
			if($objModeloConfig->actualizarConfig(
				$post['responsable'], $post['telefono_contacto'], $post['cif'], $post['nombre_entidad'], $post['direccion'], $post['cp'], $post['localidad'],
				$post['telefono'], $post['email_sistema'], $post['email_secretaria'], $post['email_notificaciones']
			))
			{
				Alerta::guardarMensajeInfo('La configuraci&oacute;n general ha sido actualizada');
			}
			
			// actualizamos la variable del registro
			$rowConfig = $objModeloConfig->obtenerConfig();
		}
	}
	
	$rowConfig = $rowConfig->fetch_object();
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general');
}
else
{
	// si no existe una entidad organizadora ceramos una
	if($objModeloConfig->insertarConfig())
	{
		Alerta::guardarMensajeInfo('La configuraci&oacute;n general ha sido creada');
		$rowConfig = $objModeloConfig->obtenerConfig();
		if($rowConfig->num_rows == 1)
		{
			$rowConfig = $rowConfig->fetch_object();
			
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general');
		}
	}
}