<?php
$objModeloConfig = new AModeloConfig();

$rowConfig = $objModeloConfig->obtenerConfig();
if($rowConfig->num_rows == 1)
{
	$result = false;
	$rowConfig = $rowConfig->fetch_object();
	
	// ruta de la carpeta de imagenes
	$pathImage = PATH_ROOT . 'archivos/config_web/';	
		
	// SUBIR IMAGEN
	if(Peticion::isPost())
	{
		require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';
			
		// CAMBIAR LOGO WEB
		if(isset($_FILES['logo']['tmp_name']) && !empty($_FILES['logo']['tmp_name']))
		{
			$objThumb = PhpThumbFactory::create($_FILES['logo']['tmp_name']);
			
			// eliminamos la imagen anterior
			if(!empty($rowConfig->imagen_logo) && is_file($pathImage . $rowConfig->imagen_logo))
			{
				chmod($pathImage . $rowConfig->imagen_logo, 0777);
				unlink($pathImage . $rowConfig->imagen_logo);
			}
			
			// nuevo nombre de la imagen
			$newFilename = 'logo.' . strtolower($objThumb->getFormat());
			
			// redimensionamos la imagen y la guardamos
			$objThumb->resize(0, 60);
			$objThumb->save($pathImage . $newFilename);
			
			unset($objThumb);
			
			// guardamos el nombre de la imagen en BBDD
			$result = $objModeloConfig->actualizarLogo($newFilename);
		}

		// CAMBIA EL FONDO DE LA WEB PUBLICO
		if(isset($_FILES['fondo_publico']['tmp_name']) && !empty($_FILES['fondo_publico']['tmp_name']))
		{
			$objThumb = PhpThumbFactory::create($_FILES['fondo_publico']['tmp_name']);
		exit('idnsert');
			// eliminamos la imagen anterior
			if(!empty($rowConfig->imagen_fondo_publico) && is_file($pathImage . $rowConfig->imagen_fondo_publico))
			{
				chmod($pathImage . $rowConfig->imagen_fondo_publico, 0777);
				unlink($pathImage . $rowConfig->imagen_fondo_publico);
			}
			
			// nuevo nombre de la imagen
			$newFilename = 'fondo_publico.' . strtolower($objThumb->getFormat());
			
			// redimensionamos la imagen y la guardamos
			$objThumb->resize(0, 0);
			$objThumb->save($pathImage . $newFilename);
			exit('inserfgt');
			unset($objThumb);
			
			// guardamos el nombre de la imagen en BBDD
			$result = $objModeloConfig->actualizarFondoPublico($newFilename);
		}

		// CAMBIA EL FONDO DE LA WEB ADMIN
		if(isset($_FILES['fondo_admin']['tmp_name']) && !empty($_FILES['fondo_admin']['tmp_name']))
		{
			$objThumb = PhpThumbFactory::create($_FILES['fondo_admin']['tmp_name']);

			// eliminamos la imagen anterior
			if(!empty($rowConfig->imagen_fondo_admin) && is_file($pathImage . $rowConfig->imagen_fondo_admin))
			{
				chmod($pathImage . $rowConfig->imagen_fondo_admin, 0777);
				unlink($pathImage . $rowConfig->imagen_fondo_admin);
			}
			
			// nuevo nombre de la imagen
			$newFilename = 'fondo_admin.' . strtolower($objThumb->getFormat());

			// redimensionamos la imagen y la guardamos
			$objThumb->resize(0, 0);
			$objThumb->save($pathImage . $newFilename);

			unset($objThumb);

			// guardamos el nombre de la imagen en BBDD
			$result = $objModeloConfig->actualizarFondoAdmin($newFilename);
		}

		// CAMBIA EL FAVICON DE LA WEB
		if(isset($_FILES['favicon']['tmp_name']) && !empty($_FILES['favicon']['tmp_name']))
		{
			// para .ico
			if($_FILES['favicon']['type'] == 'image/x-icon')
			{
				// eliminamos la imagen anterior
				if(!empty($rowConfig->imagen_favicon) && is_file($pathImage . $rowConfig->imagen_favicon))
				{
					chmod($pathImage . $rowConfig->imagen_favicon, 0777);
					unlink($pathImage . $rowConfig->imagen_favicon);
				}
			
				if(move_uploaded_file($_FILES['favicon']['tmp_name'], $pathImage . 'favicon.ico'))
				{
					// guardamos el nombre de la imagen en BBDD
					$result = $objModeloConfig->actualizarFavicon('favicon.ico');
				}
			}
		}
		
		if($result)
		{
			Alerta::mostrarMensajeInfo('preferenciasactualizadas','Se han actualizado tus preferencias');
		}

		Url::redirect('configuracion/grafica');
	}
	// ELIMINAR IMAGENES
	else
	{
		$get = Peticion::obtenerGet();
		
		if(isset($get['idtipo']) && is_numeric($get['idtipo']))
		{
			switch($get['idtipo'])
			{
				case '1':
					if(!empty($rowConfig->imagen_logo) && is_file($pathImage . $rowConfig->imagen_logo))
					{
						chmod($pathImage . $rowConfig->imagen_logo, 0777);
						unlink($pathImage . $rowConfig->imagen_logo);
						
						// vaciamos el registro de la BBDD
						$result = $objModeloConfig->actualizarLogo('');
						
						Alerta::mostrarMensajeInfo('eliminadoimagen','Se han eliminado la imagen');
					}
					break;
				case '2':
					if(!empty($rowConfig->imagen_fondo_publico) && is_file($pathImage . $rowConfig->imagen_fondo_publico))
					{
						chmod($pathImage . $rowConfig->imagen_fondo_publico, 0777);
						unlink($pathImage . $rowConfig->imagen_fondo_publico);
						
						// vaciamos el registro de la BBDD
						$result = $objModeloConfig->actualizarFondoPublico('');
						
						Alerta::mostrarMensajeInfo('eliminadoimagen','Se han eliminado la imagen');
					}
					break;
				case '3':
					if(!empty($rowConfig->imagen_fondo_admin) && is_file($pathImage . $rowConfig->imagen_fondo_admin))
					{
						chmod($pathImage . $rowConfig->imagen_fondo_admin, 0777);
						unlink($pathImage . $rowConfig->imagen_fondo_admin);
						
						// vaciamos el registro de la BBDD
						$result = $objModeloConfig->actualizarFondoAdmin('');
						
						Alerta::mostrarMensajeInfo('eliminadoimagen','Se han eliminado la imagen');
					}
					break;
				case '4':
					if(!empty($rowConfig->imagen_favicon) && is_file($pathImage . $rowConfig->imagen_favicon))
					{
						chmod($pathImage . $rowConfig->imagen_favicon, 0777);
						unlink($pathImage . $rowConfig->imagen_favicon);
						
						// vaciamos el registro de la BBDD
						$result = $objModeloConfig->actualizarFavicon('');
						
						Alerta::mostrarMensajeInfo('eliminadoimagen','Se han eliminado la imagen');
					}
					break;
				default:
					break;
			}		
		}
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'grafica');
}