<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	require PATH_ROOT . 'lib/mail/phpmailer.php';
	require PATH_ROOT . 'lib/mail/mail.php';
	$objMail = Mail::obtenerInstancia();
	
	$objModeloAcceso = new ModeloAcceso();
	
	if(isset($post['email']) && !empty($post['email']) && filter_var($post['email'], FILTER_VALIDATE_EMAIL))
	{
		$rowAlumno = $objModeloAcceso->obtenerAlumnoPorEmail($post['email']);
		if($rowAlumno->num_rows == 1)
		{
			$rowAlumno = $rowAlumno->fetch_object();
			
			$newPassword = Texto::cadenaAleatoria(10);
			
			if($objModeloAcceso->actualizarPassAlumno($rowAlumno->idalumnos, md5($newPassword)))
			{
				$plantilla = file_get_contents('plantillas/email_lost_pass.html');
				$mensaje = str_replace('{new_password}', $newPassword, $plantilla);
		
				if($objMail->enviar(
					CONFIG_EMAIL_LOST_PASS_EMAIL,
				 	$rowAlumno->email,
				 	CONFIG_EMAIL_LOST_PASS_NAME,
				 	$rowAlumno->nombre . ' ' . $rowAlumno->apellidos,
					Texto::decode('Solicitud de nueva contrase&ntilde;a'),
					$mensaje))
				{
					Alerta::mostrarMensajeInfo('nuevacontraseña','Se ha enviado tu nueva contraseña a tu email');
				}
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('noemail','No existe ningu&uacute;n usuario con este email');
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('emailvalido','Introduce un email v&aacute;lido');
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'lost_pass');