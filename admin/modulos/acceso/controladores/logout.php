<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

session_unset();
session_destroy();
Url::redirect('panel');