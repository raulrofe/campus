<?php
$mi_modeloAcceso = new ModeloAccesoAdmin();

// eliminamos la session de logueo
if(Usuario::is_login())
{
	session_unset();
	session_destroy();
	Url::redirect('');
}

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	extract($post);
	
	if(isset($usuario, $pass, $idperfil))
	{
		$acceso_login = true;
		
		if($acceso_login)
		{
			$pass = md5($pass);
			
			$resultado = $mi_modeloAcceso->loggin($idperfil,$usuario,$pass);
			
			if($resultado->num_rows == 1)
			{
				$f = mysqli_fetch_assoc($resultado);
				
				if($idperfil == '1')
				{
					$_SESSION['perfil'] = $f['perfil'];
					$_SESSION['idusuario'] = $f['idrrhh'];
				}
				
				// sessiones de fechas de acceso
				$_SESSION['usuario_fecha_actual_acceso'] = date('Y-m-d H:i:s');
				$_SESSION['usuario_fecha_ult_interaccion_foro'] = array();
				$_SESSION['usuario_fecha_notifi'] = date('Y-m-d H:i:s');
				
				// para el sistema de notificaciones
				$_SESSION['usuario_fecha_acceso_curso'] = null;
				$_SESSION['usuario_fecha_listado_conectados'] = array();
				
				
				if($_SESSION['perfil'] == 'administrador')
				{
					$session = $_SESSION;
					session_destroy();	
					session_name("campusadmin");
					session_start();
					$_SESSION = $session;
					
					Url::redirect('');
				}
				else
				{
					Url::redirect('');
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('usuarioincorrecto','La contraseña o el usuario introducido es incorrecto, intentelo de nuevo');
			}
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'login');