<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

Class ModeloAccesoAdmin extends modeloExtend {

	// Sistema de Logeo
	public function loggin($idperfil,$usuario,$pass)
	{
		if($idperfil == '1')
		{
			$sql = "SELECT * from rrhh RH , perfil P
			where borrado = 0 AND RH.alias = '".$usuario."' 
			and RH.pass = '".$pass."' 
			and RH.idperfil = ".$idperfil." 
			and P.idperfil = RH.idperfil GROUP BY RH.idrrhh";
			$resultado = $this->consultaSql($sql);
		}	
		return $resultado;
	}
	
	public function obtenerAlumnoPorEmail($email)
	{
		$sql = "SELECT * from alumnos AS al
		where borrado = 0 AND al.email = '".$email."'";
		
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function actualizarPassAlumno($idalumno, $pass)
	{
		$sql = 'UPDATE alumnos SET pass = "' . $pass . '" WHERE idalumnos = ' . $idalumno;
		
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	

	public function obtenerLogErrorAccess()
	{
		$sql = 'SELECT COUNT(ip_address) AS cont FROM log_error_access WHERE ip_address = "' . Url::getIpAddress() . '" AND date >= "' . date('Y-m-d H:i:s', time() - 3600) . '"';
		
		$resultado = $this->consultaSql($sql);
		$resultado = $resultado->fetch_object();
		$resultado = $resultado->cont;
		
		return $resultado;
	}
	
	public function eliminarLogErrorAccess()
	{
		$sql = 'DELETE FROM log_error_access WHERE ip_address = "' . Url::getIpAddress() . '"';
		
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function insertarLogErrorAccess()
	{
		$sql = 'INSERT INTO log_error_access (ip_address, user_agent, date) VALUES ("' . Url::getIpAddress() . '", "' . $_SERVER['HTTP_USER_AGENT'] . '", "' . date('Y-m-d H:i:s') . '")';
		
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}