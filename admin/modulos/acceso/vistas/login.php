<?php if(!defined('I_EXEC')) exit('Acceso no permitido');?>

<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'white',
   lang : 'es'
};
</script>


<div id="acceso_campus_logo"><img src="../imagenes/logo.jpg" alt="" /></div>

<form action='' method='post'>
	<div id="login-box">
		<H2>Acceso al Campus</H2>
		<p>Bienvenido al Campus Virtual de Formaci&oacute;n AULA_INTERACTIVA</p>
		<br />
		<div id="login-box-name" style="margin-top:20px;">Usuario:</div>
		<div id="login-box-field" style="margin-top:20px;"><input type='text' name='usuario' tabindex="1" autofocus="autofocus" class="form-login" title="Username" value="" size="30" maxlength="2048" /></div>
		<div id="login-box-name">Contrase&ntilde;a:</div><div id="login-box-field"><input name='pass' tabindex="2" type="password" class="form-login" title="Password" value="" size="30" maxlength="2048" /></div>
		<br />
		<!-- <span class="login-box-options"><a id="acceso_campus_lost_pass" href="solicitar-nueva-contrasena">He olvidado mi contrase&ntilde;a</a></span> -->
		<br />
		<br />

		<div class='t_center'>
			<button type='submit' tabindex="4"><img src="../imagenes/login-btn.png" width="103" height="42" /></button>
		</div>
		<br/>
		<div id="acceso_campus_mail" class='t_center'>Si no puede acceder p&oacute;ngase en <a href="#" title="">contacto con nosotros</a></div>
	</div>
	<input type="hidden" name="idperfil" value="1" />
</form>