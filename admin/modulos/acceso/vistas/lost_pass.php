<?php if(!defined('I_EXEC')) exit('Acceso no permitido');?>

<div id="acceso_campus">
	<div id="acceso_campus_logo"><img src="imagenes/logo.jpg" alt="" /></div>
	<h2>Nueva contrase&ntilde;a para Campus AULA_INTERACTIVA</h2>
	<div id="acceso_campus_container">
		<div id="acceso_campus_form">
			<form action='' method='post'>
				<div class='login'>
					<div class='fila_log'>
						<div class='t_log fleft'>Email : </div>
						<div class='t_log fright'><input type='text' name='email' /></div>
					</div>
					<div class='fila_log' style="font-size:0.85em;">
						<p>* La nueva contrase&ntilde;a ser&aacute; enviada al email con el que se di&oacute; de alta en el campus (solo disponible para el alumnado)</p>
					</div>
					<div id="acceso_campus_btn" class='fila_log'>
						<div class='t_log'>
							<input id="acceso_campus_btn_login" type='submit' value='Solicitar nueva contrase&ntilde;a' />
						</div>
					</div>
					<div class="clear"></div>
					<div>
						<br />
						<a id="acceso_campus_lost_pass" href="<?php echo URL_BASE?>">Accede con tu cuenta de usuario</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<link type="text/css" rel="stylesheet" href="modulos/acceso/css/default.css">