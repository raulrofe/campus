<h2>Generar XML</h2><br/>
	<div class='updt'>	
		<div style='overflow:hidden;'>
		<form name='frmConv' method='post' action=''>
			<div class='filafrm'>
				<div class='etiquetafrm'>Convocatorias</div>
				<div class='campofrm'>
					<select name='idConvocatoria' onchange='this.form.submit()'>
						<option value=''>Selecciona una convocatoria</option>
						<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
							<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(isset ($post['idConvocatoria']) && $convocatoria['idconvocatoria'] == $post['idConvocatoria'])echo 'selected'?>><?php echo $convocatoria['nombre_convocatoria']?></option>
						<?php endwhile;?>
					</select>
				</div>
			</div>
		</form>
			<?php if(isset ($accionesFormativasRLT)):?>	
				<form id="frmAF" name='frmAF' method='post' action='generarxml/xml_af'>
					<div class='subtitle'>Acciones Formativas <a href='#' onclick="checkall('frmAF'); return false;">(seleccionar todo)</a></div>
					<?php while($accionFormativaRLT = mysqli_fetch_assoc($accionesFormativasRLT)):?>
						<div class='columCheck'>
							<input type='checkbox' name='idAF[]' id='af_<?php echo $accionFormativaRLT['idaccion_formativa']?>' value='<?php echo $accionFormativaRLT['idaccion_formativa']?>' />
							<label for='af_<?php echo $accionFormativaRLT['idaccion_formativa']?>' class='lbCheckbox' ><?php echo $accionFormativaRLT['accion_formativa']?></label>
						</div>
					<?php endwhile;?>
					<div class='clear'></div><br/>
					<div>
					<input type='hidden' name='idConvocatoria' value='<?php echo $post['idConvocatoria']?>' />
					<input type='submit' value='Generar Acciones Formativas' />
					</div>
				</form>
			<?php endif;?>
			
			<?php if(isset ($iniciosGrupos)):?>	
				<form id='frmIG' name='frmIG' method='post' action='generarxml/xml_ig'>
					<div class='subtitle'>Inicios de Grupos <a href='#' onclick="checkall('frmIG'); return false;">(seleccionar todo)</a></div>
					<?php while($inicioGrupo = mysqli_fetch_assoc($iniciosGrupos)):?>
						<div class='columCheck'>
							<input type='checkbox' name='idGrupo[]' id='grupo_<?php echo $inicioGrupo['idinicio_grupo']?>' value='<?php echo $inicioGrupo['idinicio_grupo']?>' />
							<label for='grupo_<?php echo $inicioGrupo['idinicio_grupo']?>' class='lbCheckbox' ><?php echo $inicioGrupo['codigoIG']?></label>
						</div>								
					<?php endwhile;?>
				<div class='clear'></div><br/>
				<input type='hidden' name='idConvocatoria' value='<?php echo $post['idConvocatoria']?>' />
				<div><input type='submit' value='Generar Inicio Grupos' /></div>
				</form>
			<?php endif;?>
		</div>
	</div>	
	
<script type="text/javascript">
function checkall(id)
{
	if($('#' + id).find('input[type="checkbox"]:checked').size() > 0)
	{
		$('#' + id).find('input[type="checkbox"]').removeAttr('checked');
	}
	else
	{
		$('#' + id).find('input[type="checkbox"]').attr('checked', 'checked');
	}
}

</script>