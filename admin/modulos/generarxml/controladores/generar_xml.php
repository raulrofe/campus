<?php
mvc::cargarModuloSoporte('cursos');
$objConvocatoria = new Convocatoria();
mvc::cargarModuloSoporte('contenidos');
$objAF = new AccionFormativa();
mvc::cargarModuloSoporte('grupos');
$objGrupo = new Grupos();

$post = Peticion::obtenerPost();

//print_r($post);

$convocatorias = $objConvocatoria->convocatorias();
$accionesFormativas = $objAF->af();

if(isset ($post['idConvocatoria']) && is_numeric($post['idConvocatoria']))
{
	$accionesFormativasRLT = $objAF->afConvocatoria($post['idConvocatoria']);
	$iniciosGrupos = $objGrupo->obtenerInicioGruposPorConv($post['idConvocatoria']);
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'generar_xml');	