<?php
ob_start();

header("Content-disposition: attachment; filename=Inicio_grupos.xml");
header("Content-type: text/xml");
// header("Content-type: application/octet-stream");

$post = Peticion::obtenerPost();

mvc::cargarModuloSoporte('cursos');
mvc::cargarModuloSoporte('grupos');
mvc::cargarModuloSoporte('centros');

$objCentros = new AModeloCentros();

$objConvocatoria = new Convocatoria();
$objIG = new Grupos();

$f = $objConvocatoria->buscar_convocatoria($post['idConvocatoria']);

$codigo= array("á","é","í","ó", "ú","ü","ñ","Á","É","Í","Ó","Ú","Ñ","Ü");
$cambiar = array("a","e","i","o","u","u","n","A","E","I","O","U","N","U");

$ig = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<grupos xmlns="http://www.fundaciontripartita.es/schemas">';


for ($i=0;$i<=count($post['idGrupo'])-1;$i++){

$iniciosGrupos = $objIG->obtenerAFInicioGrupo($post['idGrupo'][$i]);
$inicioGrupo = mysqli_fetch_assoc($iniciosGrupos);

$descripcionTitulo = Texto::quitarTildes(utf8_decode($inicioGrupo['descripcion']));

$tutorias = $objIG->obtenerTutoriasInicioGrupo($post['idGrupo'][$i]);
$tutoria = mysqli_fetch_assoc($tutorias);

$entidadOrganizadora = $objIG->obtenerEntidadOrganizadora($post['idGrupo'][$i]);
$eo = mysqli_fetch_assoc($entidadOrganizadora);

$ig.='<grupo>
    <idAccion>'.$inicioGrupo['accion_formativa'].'</idAccion>
    <idGrupo>'.$inicioGrupo['codigoIG'].'</idGrupo>
    <descripcion>'.$descripcionTitulo.'</descripcion>
    <cumAportPrivada>false</cumAportPrivada>
    <tipoFormacion>
      <mediosPropios>'.$inicioGrupo['mediosPropios'].'</mediosPropios>
      <mediosEntidadOrganizadora>'.$inicioGrupo['mediosEntidad'].'</mediosEntidadOrganizadora>
      <mediosCentro>'.$inicioGrupo['mediosCentro'].'</mediosCentro>
    </tipoFormacion>
    <NumeroParticipante>'.$inicioGrupo['nParticipantes'].'</NumeroParticipante>
    <fechaInicio>'.Fecha::invertir_fecha($f['f_inicio'],'-','/').'</fechaInicio>
    <fechaFin>'.Fecha::invertir_fecha($f['f_fin'],'-','/').'</fechaFin>
	<responsable>'.$eo['responsable'].'</responsable>
    <telefonoContacto>'.$eo['telefono_contacto'].'</telefonoContacto>
    <distanciaTeleformacion>
	<asistenciaTeleformacion>	 
			  <centro>
				<cif>'.$eo['cif'].'</cif>
				<nombreCentro>'.$eo['nombre_entidad'].'</nombreCentro>
				<direccionDetallada>'.Texto::quitarTildes(utf8_decode($eo['direccion'])).'</direccionDetallada>
				  <codPostal>'.$eo['cp'].'</codPostal>
				  <localidad>'.$eo['localidad'].'</localidad>
			  </centro>
			  <telefono>'.$eo['telefono'].'</telefono>		 
	 </asistenciaTeleformacion>
     <horario>
			<horaTotales>'.$tutoria['n_horas'].'</horaTotales>
			<horaInicioMa'.html_entity_decode('&ntilde;', ENT_QUOTES, 'UTF-8').'ana>'.$tutoria['hora_inicio'].'</horaInicioMa'.html_entity_decode('&ntilde;', ENT_QUOTES, 'UTF-8').'ana>
			<horaFinMa'.html_entity_decode('&ntilde;', ENT_QUOTES, 'UTF-8').'ana>'.$tutoria['hora_fin'].'</horaFinMa'.html_entity_decode('&ntilde;', ENT_QUOTES, 'UTF-8').'ana>
			<dias>'.$tutoria['dias_tutoria'].'</dias>
     </horario>';
		
	$tutores = $objIG->obtenerTutoresInicioGrupo($post['idGrupo'][$i]);
	$numeroTutores = mysqli_num_rows($tutores);
	while($f2 = mysqli_fetch_assoc($tutores))
	{
     $ig.='<Tutor>
			<numeroHoras>'.$f['n_horas']/$numeroTutores.'</numeroHoras>
			<nif>'.$f2['nif'].'</nif>
			<nombre>'.Texto::quitarTildes(utf8_decode($f2['nombre'])).'</nombre>
			<apellido1>'.Texto::quitarTildes(utf8_decode($f2['apellido1'])).'</apellido1>
			<apellido2>'.Texto::quitarTildes(utf8_decode($f2['apellido2'])).'</apellido2>
     </Tutor>';
	}
     $ig.='</distanciaTeleformacion>
     <EmpresasParticipantes>';

	$centros = $objIG->obtenerCentrosInicioGrupo($post['idGrupo'][$i]);
		
	while($rf = mysqli_fetch_assoc($centros)){
		//print_r($rf);
		$center = $objCentros->obtenerCentro($rf['idcentros']);
		$miCenter = mysqli_fetch_assoc($center);
		$ig.= '<empresa>
			<cifEmpresaParticipante>'.$miCenter['cif'].'</cifEmpresaParticipante>
	   </empresa>';
	}
    $ig.='</EmpresasParticipantes>
			<observaciones>'.Texto::quitarTildes(utf8_decode($inicioGrupo['observaciones'])).'</observaciones>
	</grupo>';
}
$ig.= '</grupos>';

echo $ig;
/*
	$archivo='InicioGrupos';
	$archivo.=".xml";
	$contenido2=$ig;
 
	$ft=fopen($archivo,"w"); 
	fwrite($ft,$contenido2);
	fclose($ft);
*/
	//header("Location: ../index.php");

	ob_end_flush();
?>
