<?php
header("Content-disposition: attachment; filename=AccionesFormativas2011.xml");
header("Content-type: text/xml");

$post = Peticion::obtenerPost();

//print_r($post);

mvc::cargarModuloSoporte('cursos');
mvc::cargarModuloSoporte('contenidos');

$objConvocatoria = new Convocatoria();
$objAF = new AccionFormativa();

$f = $objConvocatoria->buscar_convocatoria($post['idConvocatoria']);

$codigo= array("á","é","í","ó", "ú","ü","ñ","Á","É","Í","Ó","Ú","Ñ","Ü");
$cambiar = array("a","e","i","o","u","u","n","A","E","I","O","U","N","U");

$af = '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<AccionesFormativas xmlns="http://www.fundaciontripartita.es/schemas">';

for($i=0;$i<=count($post['idAF'])-1;$i++){
	
	$accionFormativa = $objAF->buscar_af($post['idAF'][$i]);
	$f2 = mysqli_fetch_assoc($accionFormativa);
	
  $af.='
  <AccionFormativa>
    <codAccion>'.$f2['accion_formativa'].'</codAccion>
    <nombreAccion>Nuevas Tecnologias aplicadas a la Educacion</nombreAccion>
    <codGrupoAccion>'.$f['codigo_convocatoria'].'</codGrupoAccion>
    <denominacionCncp>
      <codCertificado>0</codCertificado>
      <realDecreto>0</realDecreto>
      <unidadCompetencia>0</unidadCompetencia>
    </denominacionCncp>
    <modalidadSimple>
    	<horas>'.$f['n_horas'].'</horas>
    	<modalidad>'.$f['metodologia'].'</modalidad>
    </modalidadSimple>
    <modalidadMixta>
      <horasPr>0</horasPr>
      <horasTe>0</horasTe>      
    </modalidadMixta>
    <uri>http://www.campusaulainteractiva.com</uri>
    <usuario>'.$f['usuario'].'</usuario>
    <password>'.$f['pass'].'</password>
    <observaciones>'.$f['observaciones'].'</observaciones>
    <tipoAccion>'.$f['tipo_accion'].'</tipoAccion>
    <nivelFormacion>'.$f['nivel_formacion'].'</nivelFormacion>
    <modTecnologia>0</modTecnologia>
    <modPrevRiesgos>0</modPrevRiesgos>    
    <modSensiMedAmb>0</modSensiMedAmb>
    <modPromocion>0</modPromocion>';
    
  	$modulosAF = $objAF->modulosAF($post['idAF'][$i]);
  
  	$contenidos='';
  	$objetivos='';
  	$cont=1;
  	while($f3 = mysqli_fetch_assoc($modulosAF))
  	{
  		$contenido = str_replace("\n","",$f3['contenidos']);	
  		$contenidos.="\nMODULO ".$cont.": ".$f3['nombre']."\n".strip_tags($contenido);
  		$objetivo = str_replace("<br/>","",$f3['objetivos']);
  		$objetivos.=strip_tags($objetivo);
  		$cont++;
  	}
  	
  	$contenidos = utf8_decode($contenidos);
  	$contenidos = Texto::quitarTildes($contenidos);
  	$objetivos = utf8_decode($objetivos);
  	$objetivos = Texto::quitarTildes($objetivos);
  
  	$af.='
  	<objetivos>'.$objetivos.'</objetivos>
    <contenidos>'.$contenidos.'</contenidos>';
  
 
  	//Creamos el bucle para las empresas participantes que pertenecen a la accion formativa del momento
  	$centrosAF = $objAF->obtenerCentroAF($post['idAF'][$i], $post['idConvocatoria']);
  	while($f4 = mysqli_fetch_assoc($centrosAF)){
	  $af.='<empParticipantes>
	      <cif>'.$f4['cif'].'</cif>
	      <infRLT>
	        <informaRLT>'.$f4['informe_rlt'].'</informaRLT>';
			
			if($f4['informe_rlt'] == 'S')
			{
	        $af.='<informe>
	          <valorinf>'.$f4['valorinf'].'</valorinf>';
		        if($f4['valorinf'] == 'D'){
		        $af.='<discrepancia>
		            <fechaDis>'.$f4['fechadis'].'</fechaDis>
		            <resuelto15>'.$f4['resuelto15'].'</resuelto15>
		          </discrepancia>';
		        }
	        $af.='</informe>';
			}
			
	     $af.=' </infRLT>            
		</empParticipantes>'; 
  	}

  $af.= '</AccionFormativa>';
}
$af.='</AccionesFormativas>';

ob_clean();

echo $af;
/*
	$archivo='AccionesFormativas2011';
	$archivo.=".xml";
	$contenido2=$af;
 
	$ft=fopen($archivo,"w"); 
	fwrite($ft,$contenido2);
	fclose($ft);
*/
	ob_end_flush();
?>
