<?php

$get = Peticion::obtenerGet();

if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	$objModelo = new AModeloSolicitudes();
	
	// obtenemos la solicitud de cambio de datos del alumno
	$solicitud = $objModelo->obtenerUnaSolicitud($get['idalumno']);
	if($solicitud->num_rows == 1)
	{
		$solicitud = $solicitud->fetch_object();
	
		// aceptar o no un cambio de datos
		if(isset($get['respuesta']))
		{
			if($get['respuesta'] == 'si')
			{
				$objModelo->actualizarAlumno(
					$solicitud->idalumnos, $solicitud->p_nombre, $solicitud->p_apellidos, $solicitud->p_dni, $solicitud->p_f_nacimiento, $solicitud->p_telefono,
					$solicitud->p_telefono_secundario, $solicitud->p_direccion, $solicitud->p_cp, $solicitud->p_poblacion, $solicitud->p_provincia, $solicitud->p_pais,
					$solicitud->p_usuario, $solicitud->p_lugar_nacimiento, $solicitud->p_email, $solicitud->p_idsexo
				);
				
				// alimina la solicitud
				$objModelo->eliminarAlumnnoSolicitud($solicitud->idalumnos);
			
				Alerta::guardarMensajeInfo('Se han aceptado los cambios de datos del alumno');
			}
			else if($get['respuesta'] == 'no')
			{
				// alimina la solicitud
				$objModelo->eliminarAlumnnoSolicitud($solicitud->idalumnos);
			
				Alerta::guardarMensajeInfo('Se han denegado los cambios de datos del alumno');
			}
			
			
			Url::redirect('solicitudes/cambio-datos-personales');
		}
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'ver_cambio_datos_personales');
	}
}