<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Solicitudes</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="#" onclick="return false;">Cambio de datos personales</a></li>
    <li class="clear"></li>
</ul>

<?php if($solicitudes->num_rows > 0):?>
	<table class="table table-hover">
		<thead>
	        <tr>
	            <th>Nombre</th>
	            <th style="text-align:right"><i class="icon-bolt"></i></th>
			</tr>
		</thead>
	  	<?php while($solicitud = $solicitudes->fetch_object()):?>
	       	<tr>
	            <td><?php echo $solicitud->nombrec?></td>
	            <td>
		          	<div style="text-align:right">
		    	      	<a href="solicitudes/cambio-datos-personales/ver/<?php echo $solicitud->idalumnos?>" title="Editar alumno">Ver solicitud</a>
		            </div>
	          	</td>
	        </tr>
	  	<?php endwhile;?>
	</table>
<?php else: ?>
	<div><b>No hay solicitudes pendientes</b></div>
<?php endif; ?>