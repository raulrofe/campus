<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Solicitudes</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="#" onclick="return false;">Solicitud de cambio de datos</a></li>
    <li class="clear"></li>
</ul>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>SOLICITUD</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmNuevoContenido" action='contenidos/insertar' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<div class="span6" style="background-color: #CFCFCF;line-height: 29px;padding-left:5px;"><b>DATOS ACTUALES</b></div>	
                	<div class="span6" style="background-color: #CFCFCF;line-height: 29px;padding-left:5px;"><b>DATOS NUEVOS</b></div>	        
		            <div class="clear"></div>
		         </div>

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Nombre<br/><?php echo Texto::textoPlano($solicitud->nombre)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_nombre != $solicitud->nombre) echo 'negrita'?>">Nombre<br/><?php echo Texto::textoPlano($solicitud->p_nombre)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
                
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Apellidos<br/><?php echo Texto::textoPlano($solicitud->apellidos)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_apellidos != $solicitud->apellidos) echo 'negrita'?>">Apellidos<br/><?php echo Texto::textoPlano($solicitud->p_apellidos)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>   

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">NIF<br/><?php echo Texto::textoPlano($solicitud->dni)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_dni != $solicitud->dni) echo 'negrita'?>">NIF<br/><?php echo Texto::textoPlano($solicitud->p_dni)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Fecha de nacimiento<br/><?php echo date('d-m-Y', strtotime($solicitud->f_nacimiento))?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_f_nacimiento != $solicitud->p_f_nacimiento) echo 'negrita'?>">Fecha de nacimiento<br/><?php echo date('d-m-Y', strtotime($solicitud->p_f_nacimiento))?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
                
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Lugar de nacimiento<br/><?php echo Texto::textoPlano($solicitud->lugar_nacimiento)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_lugar_nacimiento != $solicitud->lugar_nacimiento) echo 'negrita'?>">Lugar de nacimiento<br/><?php echo Texto::textoPlano($solicitud->p_lugar_nacimiento)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>   

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Sexo<br/>
			                        <?php echo $solicitud->idsexo ?>
	                        	</div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_idsexo != $solicitud->idsexo) echo 'negrita'?>">Sexo<br/>
			                        <?php echo $solicitud->p_idsexo ?>
	                        	</div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Tel&eacute;fono<br/><?php echo Texto::textoPlano($solicitud->telefono)?> </div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_telefono != $solicitud->telefono) echo 'negrita'?>">Tel&eacute;fono<br/><?php echo Texto::textoPlano($solicitud->p_telefono)?></div>
	                        </div>
		                </div>	
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Tele&eacute;fono secundario<br/><?php echo Texto::textoPlano($solicitud->telefono_secundario)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_telefono_secundario != $solicitud->telefono_secundario) echo 'negrita'?>">Tele&eacute;fono secundario<br/><?php echo Texto::textoPlano($solicitud->p_telefono_secundario)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
                
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Direcci&oacute;n<br/><?php echo Texto::textoPlano($solicitud->direccion)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_direccion != $solicitud->direccion) echo 'negrita'?>">Direcci&oacute;n<br/><?php echo Texto::textoPlano($solicitud->p_direccion)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>   

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">C&oacute;digo postal<br/><?php echo Texto::textoPlano($solicitud->cp)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_cp != $solicitud->cp) echo 'negrita'?>">C&oacute;digo postal<br/><?php echo Texto::textoPlano($solicitud->p_cp)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Localidad<br/><?php echo Texto::textoPlano($solicitud->poblacion)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_poblacion != $solicitud->poblacion) echo 'negrita'?>">Localidad<br/><?php echo Texto::textoPlano($solicitud->p_poblacion)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
                
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Provincia<br/><?php echo Texto::textoPlano($solicitud->provincia)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_provincia != $solicitud->provincia) echo 'negrita'?>">Provincia<br/><?php echo Texto::textoPlano($solicitud->p_provincia)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>   

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Pa&iacute;s<br/><?php echo Texto::textoPlano($solicitud->pais)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_pais != $solicitud->pais) echo 'negrita'?>">Pa&iacute;s<br/><?php echo Texto::textoPlano($solicitud->p_pais)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">E-mail<br/><?php echo Texto::textoPlano($solicitud->email)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_email != $solicitud->email) echo 'negrita'?>">E-mail<br/><?php echo Texto::textoPlano($solicitud->p_email)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 

                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
	                        	<div class="controls">Usuario<br/><?php echo Texto::textoPlano($solicitud->usuario)?></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                        <div class="controls <?php if($solicitud->p_usuario != $solicitud->usuario) echo 'negrita'?>">Usuario<br/><?php echo Texto::textoPlano($solicitud->p_usuario)?></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div> 

	                <div>      
		            	<!-- Aceptar -->
		            	<div class="span6">
		            		<div class="control-group">
	                        	<div class="controls">
	                        		<a class="btn btnFull btn-success" style="width:94%" href="solicitudes/cambio-datos-personales/asignar/<?php echo $solicitud->idalumnos?>/si">Aceptar cambios</a>
	                        	</div>
	                        </div>
	                    </div>
								
						<!-- Denegar -->
	                	<div class="span6">
	                		<div class="control-group">
	                        	<div class="controls">
	                				<a class="btn btnFull btn-warning" style="width:94%" href="solicitudes/cambio-datos-personales/asignar/<?php echo $solicitud->idalumnos?>/no">Denegar cambios</a>
	                			</div>
	                		</div>
	                	</div>	
			                
			            <div class="clear"></div>
			        </div> 		      
            </div>                         
		</form>
	</div>
</div>