<?php
class AModeloSolicitudes extends modeloExtend
{
	public function obtenerSolicitudes()
	{
		$sql = 'SELECT a.idalumnos, CONCAT(a.apellidos, ", ", a.nombre) AS nombrec FROM alumnos_peticiones AS ap LEFT JOIN alumnos AS a ON a.idalumnos = ap.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnaSolicitud($idalumno)
	{
		$sql = 'SELECT a.idalumnos, a.apellidos, a.nombre, a.dni, a.f_nacimiento, a.lugar_nacimiento, a.idsexo, a.telefono, a.telefono_secundario, a.direccion, a.cp, a.poblacion, a.provincia, a.pais, a.email, a.usuario,' .
		' ap.apellidos AS p_apellidos, ap.nombre AS p_nombre, ap.dni AS p_dni, ap.f_nacimiento AS p_f_nacimiento, ap.lugar_nacimiento AS p_lugar_nacimiento, ap.idsexo AS p_idsexo, ap.telefono AS p_telefono, ap.telefono_secundario AS p_telefono_secundario,' .
		' ap.direccion AS p_direccion, ap.cp AS p_cp, ap.poblacion AS p_poblacion, ap.provincia AS p_provincia, ap.pais AS p_pais, ap.email AS p_email, ap.usuario AS p_usuario' .
		' FROM alumnos_peticiones AS ap LEFT JOIN alumnos AS a ON a.idalumnos = ap.idalumnos WHERE a.idalumnos =' . $idalumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarAlumno($idalumno, $anombrec, $aapellidos, $adni, $af_nacimiento, $atelefono, $atelefono2, $adireccion, $acp, $alocalidad, $aprovincia, $apais, $ausuario, $alugar_nacimiento, $aemail, $aidsexo)
	{
		$sql = 'UPDATE alumnos SET ' .
		'nombre="' . $anombrec . '", apellidos="' . $aapellidos . '", dni="' . $adni . '", f_nacimiento="' . $af_nacimiento . '",' .
		'lugar_nacimiento="' . $alugar_nacimiento . '", telefono="' . $atelefono . '",' .
		'telefono_secundario="' . $atelefono2 . '", direccion="' . $adireccion . '", cp="' . $acp . '",' . 
		'poblacion="' . $alocalidad . '", provincia="' . $aprovincia . '", pais="' . $apais . '", email="' . $aemail . '",' .
		'usuario="' . $ausuario . '", idsexo = ' . $aidsexo . 
		' where idalumnos=' . $idalumno;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarAlumnnoSolicitud($idalumnos)
	{
		$sql = 'DELETE FROM alumnos_peticiones WHERE idalumnos = ' . $idalumnos;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}