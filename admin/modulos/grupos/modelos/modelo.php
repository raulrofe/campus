<?php

class Grupos extends modeloExtend{
	
	public function insertarTutoria($post)
	{
		$dias='';
		$numDias = count($post['dias'])-1;
		for($i=0;$i<=$numDias;$i++)
		{
			if($i == $numDias) $dias.= $post['dias'][$i];
			else $dias.= $post['dias'][$i].","; 
		}
		$sql = "Insert into tutoriasxml (nombre, dias_tutoria, n_horas, hora_inicio, hora_fin) 
		VALUES ('".$post['nombreTutoria']."', '".$dias."', '".$post['nHoras']."', '".$post['horaInicio']."', '".$post['horaFin']."')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	
	
	public function obtenerTutorias()
	{
		$sql = "SELECT * from tutoriasxml";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function obtenerInicioGruposPorConv($idconvocatoria)
	{
		$sql = 'SELECT * FROM inicio_grupo AS ig WHERE borrado = 0 AND ig.idconvocatoria = ' . $idconvocatoria . ' ORDER BY codigoIG ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerInicioGruposPorId($idIniGrupo)
	{
		$sql = 'SELECT * FROM inicio_grupo AS ig WHERE borrado = 0 AND ig.idinicio_grupo = ' . $idIniGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutoriasInicioGrupo($idIniGrupo)
	{
		$sql = "SELECT * from inicio_grupo IG, tutoriasxml T where IG.idinicio_grupo = ".$idIniGrupo." and IG.tutorias = T.idtutorias";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function obtenerCentrosInicioGrupo($idIniGrupo)
	{
		$sql = 'SELECT idcentros FROM inicio_grupo_centros WHERE idinicio_grupo = ' . $idIniGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnCentroPorCif($cif)
	{
		$sql = 'SELECT idcentros FROM centros WHERE cif = "' . $cif . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnCentroInicioGrupoCif($idinicio_grupo, $cif)
	{
		$sql = 'SELECT c.idcentros FROM centros AS c' .
		' LEFT JOIN inicio_grupo_centros AS igc ON c.idcentros = igc.idcentros' .
		' LEFT JOIN inicio_grupo AS ig ON ig.idinicio_grupo = igc.idinicio_grupo' .
		' WHERE ig.idinicio_grupo = ' . $idinicio_grupo . ' AND c.cif ="' . $cif . '"';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerInicioGrupoPorCodigo($codigoIG, $idconv)
	{
		$sql = 'SELECT * FROM inicio_grupo WHERE codigoIG = "' . $codigoIG . '" AND idconvocatoria = ' . $idconv;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAFInicioGrupo($inicioGrupo)
	{
		$sql = "SELECT * from inicio_grupo IG, accion_formativa AF where IG.idinicio_grupo = ".$inicioGrupo." and IG.idaccion_formativa = AF.idaccion_formativa";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function obtenerTutoresInicioGrupo($inicioGrupo)
	{
		$sql = "SELECT * from inicio_grupo_tutores IGT, rrhh RH where IGT.idinicio_grupo = ".$inicioGrupo." and IGT.idrrhh = RH.idrrhh";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function obtenerEntidadOrganizadora($inicioGrupo)
	{
		$sql = "SELECT * from entidad_organizadora EO, inicio_grupo IG where idinicio_grupo = ".$inicioGrupo." and IG.identidad_organizadora = EO.identidad_organizadora";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function insertarInicioGrupo($idConvocatoria, $idAF, $codigoIG, $descripcion, $nParticipantes, $tutorias,
			$empresaOrganizadora, $observaciones, $mediosPropios, $mediosEntidad, $mediosCentro)
	{
		$sql = 'INSERT INTO inicio_grupo' .
			' (idconvocatoria, idaccion_formativa, codigoIG, descripcion, nParticipantes, tutorias, identidad_organizadora , observaciones, mediosPropios, mediosEntidad, mediosCentro)' .
			' VALUES (' . $idConvocatoria . ', ' . $idAF . ', "' . $codigoIG . '", "' . $descripcion . '", "' . $nParticipantes . '", ' . $tutorias . ', ' .
			$empresaOrganizadora . ', "' . $observaciones . '", "' . $mediosPropios . '", "' . $mediosEntidad . '", "' . $mediosCentro . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	public function actualizarInicioGrupo($idInicioGrupo, $idConvocatoria, $idAF, $codigoIG, $descripcion, $nParticipantes, $tutorias,
			$empresaOrganizadora, $observaciones, $mediosPropios, $mediosEntidad, $mediosCentro)
	{
		$sql = 'UPDATE inicio_grupo SET' .
			' idconvocatoria = ' . $idConvocatoria . ', idaccion_formativa = ' . $idAF . ', codigoIG = "' . $codigoIG . '", descripcion = "' . $descripcion . '",' .
			' nParticipantes = "' . $nParticipantes . '", tutorias = ' . $tutorias . ', identidad_organizadora = ' . $empresaOrganizadora . ', observaciones = "' . $observaciones . '",' .
			' mediosPropios = "' . $mediosPropios . '", mediosEntidad = "' . $mediosEntidad . '", mediosCentro = "' . $mediosCentro . '"' .
			' WHERE idinicio_grupo = ' . $idInicioGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarInicioGrupoCentro($idInicioCentro, $idCentro)
	{
		$sql = 'INSERT INTO inicio_grupo_centros (idinicio_grupo, idcentros) VALUES (' . $idInicioCentro . ', ' . $idCentro . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	public function insertarInicioGrupoTutor($idInicioCentro, $idTutor)
	{
		$sql = 'INSERT INTO inicio_grupo_tutores (idinicio_grupo, idrrhh) VALUES (' . $idInicioCentro . ', ' . $idTutor . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarInicioGrupoTutores($idInicioGrupo)
	{
		$sql = 'DELETE FROM inicio_grupo_tutores WHERE idinicio_grupo = ' . $idInicioGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarInicioGrupoCentros($idInicioGrupo)
	{
		$sql = 'DELETE FROM inicio_grupo_centros WHERE idinicio_grupo = ' . $idInicioGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarInicioGrupo($idInicioGrupo)
	{
		$sql = 'UPDATE inicio_grupo SET borrado = 1 WHERE idinicio_grupo = ' . $idInicioGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}