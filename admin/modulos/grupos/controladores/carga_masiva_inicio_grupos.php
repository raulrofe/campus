<?php	

$htmlLog = null;

$get = Peticion::obtenerGet();

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objModeloGrupos = new Grupos();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	$log = array();
	
	if(isset($post['idconv']) && is_numeric($post['idconv']))
	{
		// si se ha subido un archivo del modo correcto ...
		if(isset($_FILES['archivo']['tmp_name']) && !empty($_FILES['archivo']['tmp_name']))
		{
			// comprobamos la extension del archivo (debe ser CSV)
			if(preg_match('#(.+)\.csv$#i', $_FILES['archivo']['name']))
			{
				// abre el archivo CSV
				if(($handle = fopen($_FILES['archivo']['tmp_name'], "r")) !== false)
				{
					// contadores de cursos
					$contSuccess = 0;
					$contTotal = 0;
					
					// pasamos la cabecera
					$dataHeader = fgetcsv($handle, 1000, ";");
					if(count($dataHeader) == 23)
					{
					
						// se mueve por cada registro del CSV
					    while(($data = fgetcsv($handle, 1000, ";")) !== false)
					    {
					    	$cif = $data[2];
					    	$AF = $data[5];
							$drecibi = $data[15];
							$dlogoTripartita = $data[16];
							$dlogoMec = $data[17];
							$codigoIG = $data[6];
							$observaciones = $data[22];
							
							if($drecibi == 'S')
							{
								$recibi = '1';
							}
							else if($drecibi == 'N')
							{
								$recibi = '0';
							}
							
							if($dlogoTripartita == 'S')
							{
								$logoTripartita = '1';
							}
							else if($dlogoTripartita == 'N')
							{
								$logoTripartita = '0';
							}
							
							if($dlogoMec == 'S')
							{
								$logoMec = '1';
							}
							else if($dlogoMec == 'N')
							{
								$logoMec = '0';
							}
							
							if(isset($recibi, $logoTripartita, $logoMec))
							{
								$rowIniGrupo = $objModeloGrupos->obtenerInicioGrupoPorCodigo($codigoIG, $post['idconv']);
								if($rowIniGrupo->num_rows == 0)
								{
									if($objModeloGrupos->insertarInicioGrupo($post['idconv'], $AF, $codigoIG, '', 20, 1, 1, $observaciones, $recibi, $logoTripartita, $logoMec))
									{
										$idinicio_grupo = $objModeloGrupos->obtenerUltimoIdInsertado();
										
										//$log[] = 'Se ha insertado el inicio de grupo ' . $codigoIG;
									}
									else
									{
										$log[] = 'Hubo un error al insertar el inicio grupo (SQL)';
									}
								}
								else
								{
									$rowIniGrupo = $rowIniGrupo->fetch_object();
									$idinicio_grupo = $rowIniGrupo->idinicio_grupo;
									/*$rowIniGrupo = $rowIniGrupo->fetch_object();
									if($objModeloGrupos->actualizarInicioGrupo(
										$rowIniGrupo->idinicio_grupo,
										$rowIniGrupo->idconvocatoria,
										$AF,
										$rowIniGrupo->codigoIG,
										$rowIniGrupo->descripcion,
										$rowIniGrupo->nParticipantes,
										$rowIniGrupo->tutorias,
										$rowIniGrupo->identidad_organizadora,
										$observaciones,
										$recibi,
										$logoTripartita,
										$logoMec))
									{
										$log[] = 'Se ha actualizado el inicio grupo ' . $codigoIG;
									}
									else
									{
										$log[] = 'Hubo un error al insertar el inicio grupo (SQL)';
									}*/
								}
								
								if(isset($idinicio_grupo))
								{
									$rowCentro1 = $objModeloGrupos->obtenerUnCentroInicioGrupoCif($idinicio_grupo, $cif);
									if($rowCentro1->num_rows == 0)
									{
										$rowCentro2 = $objModeloGrupos->obtenerUnCentroPorCif($cif);
										if($rowCentro2->num_rows == 1)
										{
											$rowCentro2 = $rowCentro2->fetch_object();
											
											if($objModeloGrupos->insertarInicioGrupoCentro($idinicio_grupo, $rowCentro2->idcentros))
											{
												//$log[] = 'Se ha insertado el centro con CIF ' . $cif . ' en l inicio de grupo ' . $codigoIG;
											}
											else
											{
												$log[] = 'Hubo un error al insertar la relacion entre el inicio grupo y el centro (SQL)';
											}
										}
										else
										{
											$log[] = 'No se pudo insertar el centro con CIF ' . $cif . ' porque no existe en la BBDD';
										}
									}
									else
									{
										$log[] = 'La relacion de grupo ' . $codigoIG . ' con el centro con el CIF ' . $cif . ' ya existe en la BBDD';
									}
								}
							}
							else
							{
								$log[] = 'Algun campo no es valido';
							}
						
					    	$contTotal++;
					    }
					}
					
					fclose($handle);
				}
				else
				{
					$log[] = 'El numero de campos es incorrecto';
				}
			}
			else
			{
				$log[] = 'El archivo debe ser tipo CSV';
			}
		}
		else
		{
			$log[] = 'Selecciona un archivo CSV para hacer la carga masiva';
		}
	}
	else
	{
		$log[] = 'Selecciona una convocatoria';
	}
	
	// errores durante el proceso de la carga
	if(count($log) > 0)
	{
		foreach($log as $item)
		{
			$htmlLog .= '<div>- ' . $item . '</div>';
		}
	}
}

if($convocatorias->num_rows > 0)
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'carga_masiva_inicio_grupos');
}
else
{
	echo 'Debes crear antes una convocatoria';
}