<?php
mvc::cargarModuloSoporte('cursos');
$objConvocatoria = new Convocatoria();


$get = Peticion::obtenerGet();

if(!empty($get['idconv']) && is_numeric($get['idconv']))
{
	$objGrupos = new Grupos();
	$inicio_grupos = $objGrupos->obtenerInicioGruposPorConv($get['idconv']);
}

$convocatorias = $objConvocatoria->convocatorias();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listar_inicio_grupos');