<?php
$get = Peticion::obtenerGet();
if(isset($get['idinicio_grupo']) && is_numeric($get['idinicio_grupo']))
{
	$objGrupos = new Grupos();
	
	// obtenemos el inicio de grupo
	$rowIniGrupo = $objGrupos->obtenerInicioGruposPorId($get['idinicio_grupo']);
	if($rowIniGrupo->num_rows == 1)
	{
		if($objGrupos->eliminarInicioGrupo($get['idinicio_grupo']))
		{
			Alerta::guardarMensajeInfo('Se ha borrado el inicio de grupo');
			Url::redirect('grupos/inicio_grupos', true);
		}
	}
}

