<?php
$get = Peticion::obtenerGet();
if(isset($get['idinicio_grupo']) && is_numeric($get['idinicio_grupo']))
{
	// carga de objetos
	mvc::cargarModuloSoporte('cursos');
	$objConvocatoria = new Convocatoria();
	
	mvc::cargarModuloSoporte('contenidos');
	$objAF = new AccionFormativa();
	
	mvc::cargarModuloSoporte('usuario');
	$objUsuario = new AModeloUsuario();
	
	mvc::cargarModuloSoporte('configuraciones');
	$objConfig = new AModeloConfig();
	
	$objGrupos = new Grupos();
	
	mvc::cargarModuloSoporte('centros');
	$objCentros = new AModeloCentros();
	
	// obtenemos el inicio de grupo
	$rowIniGrupo = $objGrupos->obtenerInicioGruposPorId($get['idinicio_grupo']);
	if($rowIniGrupo->num_rows == 1)
	{
		$rowIniGrupo = $rowIniGrupo->fetch_object();
		
		// POST
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			if(isset($post['idConvocatoria'], $post['idAF'], $post['codigoIG'], $post['descripcion'], $post['nParticipantes'],
			$post['tutorias'], $post['idTutor'], $post['empresaOrganizadora'], $post['observaciones'], $post['mediosPropios'],
			$post['mediosEntidad'], $post['mediosCentro'], $post['centrosRLT'])
			&&(!empty($post['idAF'])) && !empty($post['empresaOrganizadora']) && is_array($post['idTutor']) && is_array($post['centrosRLT']))
			{
				// insertamos el INICIO DE GRUPO
				$resultado = $objGrupos->actualizarInicioGrupo($get['idinicio_grupo'], $post['idConvocatoria'], $post['idAF'], $post['codigoIG'], $post['descripcion'], $post['nParticipantes'], $post['tutorias'],
					$post['empresaOrganizadora'], $post['observaciones'], $post['mediosPropios'], $post['mediosEntidad'], $post['mediosCentro']);
				
				if($resultado)
				{
					$idInicioCentro = $objGrupos->obtenerUltimoIdInsertado();
					Alerta::mostrarMensajeInfo('creadogrupo','Se ha creado el inicio de grupo');
				}
				
				// si se ha insertadio correctamente ...
				if(isset($idInicioCentro))
				{
					$objGrupos->eliminarInicioGrupoTutores($get['idinicio_grupo']);
					// insertamos sus tutores
					foreach($post['idTutor'] as $idTutor)
					{
						$objGrupos->insertarInicioGrupoTutor($get['idinicio_grupo'], $idTutor);
					}
					
					$objGrupos->eliminarInicioGrupoCentros($get['idinicio_grupo']);
					// insertamos los centros
					foreach($post['centrosRLT'] as $idCentro)
					{
						$objGrupos->insertarInicioGrupoCentro($get['idinicio_grupo'], $idCentro);
					}
				}
				
				// actualizamos el registro del inicio de grupo
				$rowIniGrupo = $objGrupos->obtenerInicioGruposPorId($get['idinicio_grupo']);
				$rowIniGrupo = $rowIniGrupo->fetch_object();
			}
		}
		
		// obtenemos los datos
		$accionesFormativas = $objAF->af();
		$centros = $objCentros->obtenerCentros();
		$nCentros = mysqli_num_rows($centros);
		$columnaCentros = 5;
		$filaCentros = $nCentros/$columnaCentros;
		
		while($centro = mysqli_fetch_assoc($centros))
		{
			$regCentro['id'][] = $centro['idcentros'];
			$regCentro['cif'][] = $centro['cif'];
		}
		
		$convocatorias = $objConvocatoria->convocatorias();
		$accionesFormativas = $objAF->af();
		$tutores = $objUsuario->obtenerRRHH('tutor');
		$empresasOrganizadoras = $objConfig->obtenerConfig();
		$tutorias = $objGrupos->obtenerTutorias();
		
		// obtenemos los tutores de este inicio de grupo
		$arrayIniGrupoTutores = array();
		$resultIniGrupoTutores = $objGrupos->obtenerTutoresInicioGrupo($rowIniGrupo->idinicio_grupo);
		if($resultIniGrupoTutores->num_rows > 0)
		{
			while($rowIniGrupoTutor = $resultIniGrupoTutores->fetch_object())
			{
				$arrayIniGrupoTutores[] = $rowIniGrupoTutor->idrrhh;
			}
		}		
		
		// obtenemos los centros de este inicio de grupo
		$arrayIniGrupoCentros = array();
		$resultIniGrupoCentros = $objGrupos->obtenerCentrosInicioGrupo($rowIniGrupo->idinicio_grupo);
		if($resultIniGrupoCentros->num_rows > 0)
		{
			while($rowIniGrupoCentros = $resultIniGrupoCentros->fetch_object())
			{
				$arrayIniGrupoCentros[] = $rowIniGrupoCentros->idcentros;
			}
		}		
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar_inicio_grupo');
	}
}

