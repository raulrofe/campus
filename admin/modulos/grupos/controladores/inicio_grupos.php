<?php

mvc::cargarModuloSoporte('cursos');
$objConvocatoria = new Convocatoria();

mvc::cargarModuloSoporte('contenidos');
$objAF = new AccionFormativa();

mvc::cargarModuloSoporte('usuario');
$objUsuario = new AModeloUsuario();

mvc::cargarModuloSoporte('configuraciones');
$objConfig = new AModeloConfig();

$objGrupos = new Grupos();

mvc::cargarModuloSoporte('centros');
$objCentros = new AModeloCentros();

$get = Peticion::obtenerGet();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	if(isset($post['idTutor']) && !empty($post['idConvocatoria']) && ($post['idAF']) && ($post['codigoIG']) && ($post['descripcion']) && ($post['nParticipantes']) && ($post['tutorias']) && ($post['idTutor']) && ($post['empresaOrganizadora']) && ($post['mediosPropios']) && ($post['mediosEntidad']) && ($post['mediosCentro']) && ($post['centrosRLT']))
	{
		// insertamos el INICIO DE GRUPO
		$resultado = $objGrupos->insertarInicioGrupo($post['idConvocatoria'], $post['idAF'], $post['codigoIG'], $post['descripcion'], $post['nParticipantes'], $post['tutorias'],
			$post['empresaOrganizadora'], $post['observaciones'], $post['mediosPropios'], $post['mediosEntidad'], $post['mediosCentro']);
		
		if($resultado)
		{
			$idInicioCentro = $objGrupos->obtenerUltimoIdInsertado();
			Alerta::mostrarMensajeInfo('creadogrupo','Se ha creado el inicio de grupo');
		}
		
		// si se ha insertadio correctamente ...
		if(isset($idInicioCentro))
		{
			// insertamos sus tutores
			foreach($post['idTutor'] as $idTutor)
			{
				$objGrupos->insertarInicioGrupoTutor($idInicioCentro, $idTutor);
			}
			
			// insertamos los centros
			foreach($post['centrosRLT'] as $idCentro)
			{
				$objGrupos->insertarInicioGrupoCentro($idInicioCentro, $idCentro);
			}
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('camposobligatorios','Todos los campos son obligatorios');
	}
}

$accionesFormativas = $objAF->af();
$centros = $objCentros->obtenerCentros();
$nCentros = mysqli_num_rows($centros);
$columnaCentros = 5;
$filaCentros = $nCentros/$columnaCentros;

while($centro = mysqli_fetch_assoc($centros))
{
	$regCentro['id'][] = $centro['idcentros'];
	$regCentro['cif'][] = $centro['cif'];
}

$convocatorias = $objConvocatoria->convocatorias();
$accionesFormativas = $objAF->af();
$tutores = $objUsuario->obtenerRRHH('tutor');
$empresasOrganizadoras = $objConfig->obtenerConfig();
$tutorias = $objGrupos->obtenerTutorias();


// ----------- //
$numeroEdicionConv = null;
if(!empty($get['idconv']) && is_numeric($get['idconv']))
{
	$rowConvocatoria = $objConvocatoria->buscar_convocatoria($get['idconv']);
	
	preg_match_all('/\s([VILX]+)\s/s', $rowConvocatoria['nombre_convocatoria'], $match);
	if(isset($match[1][0]))
	{
		$numeroEdicionConv = $match[1][0];
		$numeroEdicionConv = str_pad(NumRomanos::romanoDecimal($numeroEdicionConv), 2, '0', STR_PAD_LEFT);
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'grupos');	