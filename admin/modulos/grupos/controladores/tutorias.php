<?php

$objGrupos = new Grupos();

$post = Peticion::obtenerPost();

if(isset ($post) && !empty ($post))
{
	$objGrupos->insertarTutoria($post);
}

$tutorias = $objGrupos->obtenerTutorias();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');	

?>