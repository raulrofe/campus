$(document).ready(function()
{
	// BUSCADOR DE CENTROS
	if($('#buscadorCentros').size() == 1)
	{
		$('#buscadorCentros').keyup(function(event)
		{
			var wordSearch = $(this).val();
			
			$('#listadoCentros').find('.rlt').addClass('hide');
			
			if(wordSearch != "")
			{
				var titleScorm = '';
				var searchMatch = null;
				$('#listadoCentros').find('.rlt').each(function(index)
				{
					titleScorm = $(this).find('label').html();
					//searchMatch = '/^(' + wordSearch + ')(.+)/i';
					//alert(titleScorm);
					wordSearch = LibMatch.escape(wordSearch);
					var regex = new RegExp('^(' + wordSearch + ')(.+)', 'i');
					if(titleScorm.match(regex))
					{
						$(this).removeClass('hide');
					}
				});
			}
			else
			{
				$('#listadoCentros').find('.rlt').removeClass('hide');
			}
		});
	}
	
	// BUSCADOR DE INICIO DE GRUPOS
	if($('#ig_search_grupo').size() == 1)
	{
		$('#ig_search_grupo').keyup(function(event)
		{
			var wordSearch = $(this).val();
			
			$('#listado_inicio_grupos').find('.elemento').addClass('hide');
			
			if(wordSearch != "")
			{
				var ig = '';
				var searchMatch = null;
				$('#listado_inicio_grupos').find('.elemento').each(function(index)
				{
					ig = $(this).attr('data-search-ig');
					wordSearch = LibMatch.escape(wordSearch);
					var regex = new RegExp('^(' + wordSearch + ')(.+)', 'i');
					if(ig.match(regex))
					{
						$(this).removeClass('hide');
					}
				});
			}
			else
			{
				$('#listado_inicio_grupos').find('.elemento').removeClass('hide');
			}
		});
	}
});

function adminGrupoInsertarCodIniGrupo(elemento)
{
	var numeroEdicionConvHidden = $('#numeroEdicionConvHidden').html();
	var codAF = $(elemento).find('option:selected').text();
	
	//codAF = parseInt(codAF);
	if(numeroEdicionConvHidden != '' && !isNaN(parseInt(codAF)))
	{
		codAF = codAF.substr(2, codAF.lenght);
		$('input[name="codigoIG"]').val(codAF + '' + numeroEdicionConvHidden);
	}
}