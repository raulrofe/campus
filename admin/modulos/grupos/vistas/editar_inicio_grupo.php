<div id="admin_usuarios">
	<div class="titular"><h1>Inicio Grupos</h1></div>
	
  	<div class="stContainer" id="tabs">

	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos">Insertar</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/listar">Listado inicio de grupos</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/carga_masiva">Carga masiva inicio de grupos</a></div>
	  	</div>
	  			
	  	<div class="tab">
	  		<form name='frmig' method='post' action=''>
					<div class='filafrm'>
						<div class='etiquetafrm'>Convocatorias</div>
						<div class='campofrm'>
							<select id="frmig_idConvocatoria" name='idConvocatoria'>
								<option value=''>Selecciona una convocatoria</option>
								<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
									<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if($rowIniGrupo->idconvocatoria == $convocatoria['idconvocatoria']) echo 'selected="selected"'?>><?php echo $convocatoria['nombre_convocatoria']?></option>
								<?php endwhile;?>
							</select>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Acciones Formativas</div>
						<div class='campofrm'>
							<select name='idAF'>
								<option value=''>Selecciona una accion formativa</option>
								<?php while($accionFormativa = mysqli_fetch_assoc($accionesFormativas)):?>
									<option value='<?php echo $accionFormativa['idaccion_formativa']?>' <?php if($rowIniGrupo->idaccion_formativa == $accionFormativa['idaccion_formativa']) echo 'selected="selected"'?>><?php echo $accionFormativa['accion_formativa']?></option>
								<?php endwhile;?>
							</select>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>C&oacute;digo Inicio grupo</div>
						<div class='campofrm'><input type='text' name='codigoIG' size='75' maxlength='5' value="<?php echo Texto::textoPlano($rowIniGrupo->codigoIG)?>" /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><input type='text' name='descripcion' size='75' value="<?php echo Texto::textoPlano($rowIniGrupo->descripcion)?>" /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>N&uacute;mero Participantes</div>
						<div class='campofrm'><input type='text' name='nParticipantes' size='75' maxlength='5' value="<?php echo Texto::textoPlano($rowIniGrupo->nParticipantes)?>" /></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Tutorias</div>
						<div class='campofrm'>
							<select name='tutorias'>
								<?php while($tutoria = mysqli_fetch_assoc($tutorias)):?>
									<option value='<?php echo $tutoria['idtutorias']?>' <?php if($rowIniGrupo->tutorias == $tutoria['idtutorias']) echo 'checked="checked"'?>><?php echo $tutoria['dias_tutoria']."  ".$tutoria['hora_inicio']." - ".$tutoria['hora_fin']?></option>
								<?php endwhile;?>
							</select>
						</div>
					</div>			
					<div class='filafrm'>
						<div class='etiquetafrm'>Tutores</div>
						<div class='campofrm'>
							<?php while($tutor = mysqli_fetch_assoc($tutores)):?>
								<div>
									<input type='checkbox' name='idTutor[]' id='t_<?php echo $tutor['idrrhh']?>' value='<?php echo $tutor['idrrhh']?>' <?php if(in_array($tutor['idrrhh'], $arrayIniGrupoTutores)) echo 'checked="checked"'?> />
									<label for='t_<?php echo $tutor['idrrhh']?>' class='lbCheckbox' ><?php echo $tutor['nombrec']?></label>
								</div>
							<?php endwhile;?>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Empresa organizadora</div>
						<select name='empresaOrganizadora'>
							<option value=''>Selecciona una empresa oraganizadora</option>
							<?php while($empresaOrganizadora = mysqli_fetch_assoc($empresasOrganizadoras)):?>
								<option value='<?php echo $empresaOrganizadora['identidad_organizadora']?>' <?php if($rowIniGrupo->identidad_organizadora == $empresaOrganizadora['identidad_organizadora']) echo 'selected="selected"'?>><?php echo $empresaOrganizadora['nombre_entidad']?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Observaciones</div>
						<div class='campofrm'><textarea class='estilotextarea' name='observaciones'><?php echo $rowIniGrupo->observaciones?></textarea></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Medios propios</div>
						<div class='campofrm'>
							<select name='mediosPropios'>
								<option value='true' <?php if($rowIniGrupo->mediosPropios == 'true') echo 'selected="selected"'?>>Si</option>
								<option value='false' <?php if($rowIniGrupo->mediosPropios == 'false') echo 'selected="selected"'?>>No</option>
							</select>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Medios entidad organizadora</div>
						<div class='campofrm'>
							<select name='mediosEntidad'>
								<option value='true' <?php if($rowIniGrupo->mediosEntidad == 'true') echo 'selected="selected"'?>>Si</option>
								<option value='false'<?php if($rowIniGrupo->mediosEntidad == 'false') echo 'selected="selected"'?>>No</option>
							</select>
						</div>
					</div>	
					<div class='filafrm'>
						<div class='etiquetafrm'>Medios centro</div>
						<div class='campofrm'>
							<select name='mediosCentro'>
								<option value='true'<?php if($rowIniGrupo->mediosCentro == 'true') echo 'selected="selected"'?>>Si</option>
								<option value='false' <?php if($rowIniGrupo->mediosCentro == 'false') echo 'selected="selected"'?>>No</option>
							</select>
						</div>
					</div>
					<div class='subtitle'>Centros</div>
						<input id="buscadorCentros" type="text" />
						<div id="listadoCentros">
							<?php if($nCentros <= 20):?>
								<?php for($i=0;$i<$nCentros;$i++):?>
									<div class='rlt'><input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$i]?>' value='<?php echo $regCentro['id'][$i]?>' <?php if(in_array($regCentro['id'][$i], $arrayIniGrupoCentros)) echo 'checked="checked"'?> />
									<label for='centro_<?php echo $regCentro['id'][$i]?>' class='lbCheckbox' ><?php echo $regCentro['cif'][$i]?></label></div>
								<?php endfor;?>
							<?php endif;?>
							
							<?php 
							if($nCentros > 20):
							$nRegistro=0;
							?>
							<?php for($i=1;$i<=$columnaCentros;$i++):?>
								<div class='fleft'>
									<?php 
									for($j=0;$j<$filaCentros;$j++):
									?>
										<div class='rlt'><input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$nRegistro]?>' value='<?php echo $regCentro['id'][$nRegistro]?>' <?php if(in_array($regCentro['id'][$nRegistro], $arrayIniGrupoCentros)) echo 'checked="checked"'?> />
										<label for='centro_<?php echo $regCentro['id'][$nRegistro]?>' class='lbCheckbox' ><?php echo $regCentro['cif'][$nRegistro]?></label></div>
									<?php 
									$nRegistro++;
									endfor;
									?>
								</div>
							<?php endfor;?>
						<?php endif;?>
					
					</div>
					<div class="clear"></div>
					<input type='submit' value='Actualizar' />
			</form>
	  	</div>
	  </div>
</div>

<script type="text/javascript">
	$('#frmig_idConvocatoria').change(function()
	{
		urlRedirect('grupos/inicio_grupos/listar/' + $(this).val());
	});
</script>