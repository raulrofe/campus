<h2>TUTORIAS</h2>
<div class='updt'>	
	<div style='overflow:hidden;'>
		<form name='frmTutoriaTri' method='post' action=''>
			<div class='filafrm'>
				<div class='etiquetafrm'>Nombre identificativo</div>
				<div class='campofrm'><input type='text' name='nombreTutoria' size='70' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Dias de tutoria</div>
				<div class='campofrm'>
					<input type='checkbox' name='dias[]' id='l' value='L' /><label for='l' class='lbCheckbox' >Lunes</label>
					<input type='checkbox' name='dias[]' id='m' value='M' /><label for='m' class='lbCheckbox' >Martes</label>
					<input type='checkbox' name='dias[]' id='x' value='X' /><label for='x' class='lbCheckbox' >Mi&eacute;rcoles</label>
					<input type='checkbox' name='dias[]' id='j' value='J' /><label for='j' class='lbCheckbox' >Jueves</label>
					<input type='checkbox' name='dias[]' id='v' value='V' /><label for='v' class='lbCheckbox' >Viernes</label>
					<input type='checkbox' name='dias[]' id='s' value='S' /><label for='s' class='lbCheckbox' >S&aacute;bado</label>
					<input type='checkbox' name='dias[]' id='d' value='D' /><label for='d' class='lbCheckbox' >Domingo</label>					
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>N&uacute;mero horas</div>
				<div class='campofrm'><input type='text' name='nHoras' size='70' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Hora inicio ma&ntilde;ana</div>
				<div class='campofrm'><input type='text' name='horaInicio' size='70' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>hora inicio fin</div>
				<div class='campofrm'><input type='text' name='horaFin' size='70' /></div>
			</div>
		<input type='hidden' name='accion' value='<?php if(isset($accion)) echo $accion;?>' />
		<input type='submit' value = 'Enviar'/>
		</form>
	</div>
</div>
<br/>
<h2>Listado Tutorias: </h2>
<br/>

<?php while($tutoria = mysqli_fetch_assoc($tutorias)):?>
	<div>
		<div class='fleft'><?php echo $tutoria['nombre']?></div>
		<div class='fleft'><?php echo $tutoria['dias_tutoria']?></div>
		<div class='fleft'><?php echo $tutoria['n_horas']?></div>
		<div class='fleft'><?php echo $tutoria['hora_inicio']?></div>
		<div class='fleft'><?php echo $tutoria['hora_fin']?></div>
	</div>
<?php endwhile;?>
