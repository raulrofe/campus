<div id="admin_usuarios">
	<div class="titular"><h1>Inicio Grupos</h1></div>
	
  	<div class="stContainer" id="tabs">

	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos">Insertar</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/listar">Listado inicio de grupos</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/carga_masiva">Carga masiva inicio de grupos</a></div>
	  	</div>
	  			
	  	<div class="tab">
	  		<div>
				<div class="fleft">
					<div class='etiquetafrm'>Convocatorias</div>
					<div class='campofrm'>
						<select id="frmig_idConvocatoria" name='idConvocatoria'>
							<option value=''>Selecciona una convocatoria</option>
							<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
								<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(!empty($get['idconv']) && $get['idconv'] == $convocatoria['idconvocatoria']) echo 'selected="selected"'?>><?php echo $convocatoria['nombre_convocatoria']?></option>
							<?php endwhile;?>
						</select>
					</div>
				</div>
				<div class="fright"><input id="ig_search_grupo" type="text" placeholder="Cod Inicio Grupo" /></div>
			</div>
			<div class="clear"></div>
						
			<?php if(!empty($get['idconv'])):?>
				<?php if($inicio_grupos->num_rows > 0):?>
			  		<div id="listado_inicio_grupos" class="listarElementos">
				  		<?php while($item = $inicio_grupos->fetch_object()):?>
				  			<div class="elemento" data-search-ig="<?php echo $item->codigoIG?>">
								<div class="elementoContenido fleft"><p><?php echo Texto::textoPlano($item->descripcion)?> (<?php echo $item->codigoIG?>)</p></div>
								<div class="elementoPie hide">
									<ul class="elementoListOptions fright">
										<li><a href="grupos/inicio_grupos/editar/<?php echo $item->idinicio_grupo?>" title="Editar este inicio de grupo">Editar</a></li>
										<li><a href="#" <?php echo Alerta::alertConfirmOnClick('borrarinicio', '¿Deseas borrar este inicio de grupo?', 'grupos/inicio_grupos/borrar/' . $item->idinicio_grupo)?> title="Borrar este inicio de grupo">Borrar</a></li>
									</ul>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
				  			</div>
				  		<?php endwhile;?>
				  	</div>
				  <?php else:?>
				  	<div class="elementosNoEncontrados">No se encontraron inicios de grupo para esta convocatoria</div>
			  	<?php endif;?>
		  	<?php endif;?>
	 	</div>
	 </div>
</div>

<script type="text/javascript">
	$('#frmig_idConvocatoria').change(function()
	{
		urlRedirect('grupos/inicio_grupos/listar/' + $(this).val());
	});
</script>