<div id="admin_usuarios">
	<div class="titular"><h1>Inicio Grupos</h1></div>
	
  	<div class="stContainer" id="tabs">

	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos">Insertar</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/listar">Listado inicio de grupos</a></div>
	  		<div class="btn_navegacion"><a href="grupos/inicio_grupos/carga_masiva">Carga masiva inicio de grupos</a></div>
	  	</div>
	  			
	  	<div class="tab">
			<form name='frmig' method='post' action='' enctype="multipart/form-data">
				<div class='filafrm'>
					<div class='etiquetafrm'>Convocatorias</div>
					<div class='campofrm'>
						<select id="frmig_idConvocatoria" name='idconv'>
							<option value=''>Selecciona una convocatoria</option>
							<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
								<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(!empty($get['idconv']) && $get['idconv'] == $convocatoria['idconvocatoria']) echo 'selected="selected"'?>><?php echo Texto::textoPlano($convocatoria['nombre_convocatoria'])?></option>
							<?php endwhile;?>
						</select>
						<div class="clear"></div>
					</div>
				</div>
				<?php if(!empty($get['idconv'])):?>
					<div class='filafrm'>
						<input type="file" name="archivo" />
					</div>
					<div class='filafrm'>
						<input type="submit" value="Cargar datos" />
					</div>
					<div class="clear"></div>
				<?php endif;?>
			</form>
		</div>
	</div>
</div>

<?php echo $htmlLog;?>

<script type="text/javascript">
	$('#frmig_idConvocatoria').change(function()
	{
		urlRedirect('grupos/inicio_grupos/carga_masiva/' + $(this).val());
	});
</script>