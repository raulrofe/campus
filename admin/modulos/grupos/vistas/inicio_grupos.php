<div class='subtitle'>Crear Inicio de Grupo</div>
	<div class='updt'>	
		<div style='overflow:hidden;'>
		<form name='frmig' method='post' action=''>
			<div class='filafrm'>
				<div class='etiquetafrm'>Convocatorias</div>
				<div class='campofrm'>
					<select id="frmig_idConvocatoria" name='idConvocatoria'>
						<option value=''>Selecciona una convocatoria</option>
						<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
							<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(!empty($get['idconv']) && $get['idconv'] == $convocatoria['idconvocatoria']) echo 'selected="selected"'?>><?php echo Texto::textoPlano($convocatoria['nombre_convocatoria'])?></option>
						<?php endwhile;?>
					</select>
				</div>
			</div>
			<?php if(!empty($get['idconv'])):?>
				<div class='filafrm'>
					<div class='etiquetafrm'>Acciones Formativas</div>
					<div class='campofrm'>
						<select name='idAF' onchange="adminGrupoInsertarCodIniGrupo(this);">
							<option value=''>Selecciona una accion formativa</option>
							<?php while($accionFormativa = mysqli_fetch_assoc($accionesFormativas)):?>
								<option value='<?php echo $accionFormativa['idaccion_formativa']?>'><?php echo $accionFormativa['accion_formativa']?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>C&oacute;digo Inicio grupo</div>
					<div class='campofrm'><input type='text' name='codigoIG' size='75' maxlength='5'/></div>
					<span id="numeroEdicionConvHidden" class="hide"><?php echo $numeroEdicionConv?></span>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Descripci&oacute;n</div>
					<div class='campofrm'><input type='text' name='descripcion' size='75' value='Nuevas Tecnolog&iacute;as aplicadas a la educaci&oacute;n'/></div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>N&uacute;mero Participantes</div>
					<div class='campofrm'><input type='text' name='nParticipantes' size='75' maxlength='5' value='20'/></div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Tutorias</div>
					<div class='campofrm'>
						<select name='tutorias'>
							<?php while($tutoria = mysqli_fetch_assoc($tutorias)):?>
								<option value='<?php echo $tutoria['idtutorias']?>'><?php echo $tutoria['dias_tutoria']."  ".$tutoria['hora_inicio']." - ".$tutoria['hora_fin']?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>			
				<div class='filafrm'>
					<div class='etiquetafrm'>Tutores</div>
					<div class='campofrm'>
						<?php while($tutor = mysqli_fetch_assoc($tutores)):?>
							<div>
								<input type='checkbox' name='idTutor[]' id='t_<?php echo $tutor['idrrhh']?>' value='<?php echo $tutor['idrrhh']?>' />
								<label for='t_<?php echo $tutor['idrrhh']?>' class='lbCheckbox' ><?php echo $tutor['nombrec']?></label>
							</div>
						<?php endwhile;?>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Empresa organizadora</div>
					<div class='campofrm'>
						<select name='empresaOrganizadora'>
							<option value=''>Selecciona una empresa oraganizadora</option>
							<?php while($empresaOrganizadora = mysqli_fetch_assoc($empresasOrganizadoras)):?>
							<option value='<?php echo $empresaOrganizadora['identidad_organizadora']?>'><?php echo Texto::textoPlano($empresaOrganizadora['nombre_entidad'])?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Observaciones</div>
					<div class='campofrm'><textarea class='estilotextarea' name='observaciones'></textarea></div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Medios propios</div>
					<div class='campofrm'>
						<select name='mediosPropios'>
							<option value='true'>Si</option>
							<option value='false' selected>No</option>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Medios entidad organizadora</div>
					<div class='campofrm'>
						<select name='mediosEntidad'>
							<option value='true' selected>Si</option>
							<option value='false'>No</option>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>	
				<div class='filafrm'>
					<div class='etiquetafrm'>Medios centro</div>
					<div class='campofrm'>
						<select name='mediosCentro'>
							<option value='true' selected>Si</option>
							<option value='false'>No</option>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm subtitle'>Centros</div>
					<div class='campofrm'><input id="buscadorCentros" type="text" value='introduzca cif'/></div>
				</div>
						<div id="listadoCentros">
								<?php if($nCentros <= 20):?>
									<?php for($i=0;$i<$nCentros;$i++):?>
										<div class='rlt'>
											<input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$i]?>' value='<?php echo $regCentro['id'][$i]?>' />
											<label for='centro_<?php echo $regCentro['id'][$i]?>' class='lbCheckbox' >
												<?php echo $regCentro['cif'][$i]?>
											</label>
										</div>
									<?php endfor;?>
								<?php endif;?>
						
								<?php 
								if($nCentros > 20):
								$nRegistro=0;
								?>
								<?php for($i=1;$i<=$columnaCentros;$i++):?>
									<div class='fleft'>
										<?php 
										for($j=0;$j<$filaCentros;$j++):
										?>
											<div class='rlt'><input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$nRegistro]?>' value='<?php echo $regCentro['id'][$nRegistro]?>' /><label for='centro_<?php echo $regCentro['id'][$nRegistro]?>' class='lbCheckbox' ><?php echo $regCentro['cif'][$nRegistro]?></label></div>
										<?php 
										$nRegistro++;
										endfor;
										?>
									</div>
								<?php endfor;?>
							<?php endif;?>
						</div>
				<div class="clear"></div><br/>
				<input type='submit' value='Enviar' />
				<br/><br/>
				<div class='obligatorio'>Campos obligatorios (*)</div>	
			<?php endif;?>
		</form>			
		</div>
	</div>

<script type="text/javascript">
	$('#frmig_idConvocatoria').change(function()
	{
		urlRedirect('grupos/inicio_grupos/' + $(this).val());
	});
</script>