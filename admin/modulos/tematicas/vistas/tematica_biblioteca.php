<div class="submenuAdmin">TEM&Aacute;TICAS BIBLIOTECA</div>

<!--  Opciones de tematicas -->
<div class='addAdmin'><a href='tematicas/biblioteca/insertar'>Introducir Tem&aacute;tica</a></div>
<!-- Listado de Convocatorias -->
	<div class='buscador'>
		<P class='t_center'>Tematicas introducidas en este momento en el sistema:</P><br/>
			<form name='busca_tipo_curso' action='tematicas/biblioteca' method='post' id='frmTematicasBiblio'>
				<div class='t_center'>
					<select name='idtematica'>
						<option value='0'> - Tem&aacute;tica com&uacute;n - </option>
						<?php while ($tematica = mysqli_fetch_assoc($resultado)):?>
							<?php if($tematica['idtematica'] == $post['idtematica']):?>
								<option value='<?php echo $tematica['idtematica']; ?>' selected ><?php echo $tematica['nombre']; ?></option>
							<?php else:?>
								<option value='<?php echo $tematica['idtematica']; ?>'><?php echo $tematica['nombre']; ?></option>
							<?php endif; ?>
						<?php endwhile;?>
					</select>
				</div>
				<br/>	
				<div class='t_center'>
					<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
					<span id="frmBtnBorrar" class='boton_borrar'><input type='submit' name='boton' value='Borrar' /></span>
				</div>
			</form>
	</div>
<!-- FIN de Opciones de tematicas -->

<?php 
if(isset($boton))
{
	switch ($boton)
	{
		case 'Insertar': 
			require('nueva_tematica_biblioteca.php');
			break;
			
		case 'Actualizar': 
			require('actualizar_tematica_biblioteca.php');
			break;		
	}
}
?>
<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar esta temática?');</script>
