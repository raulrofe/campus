<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Tem&aacute;ticas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="tematicas/biblioteca">Tem&aacute;ticas biblioteca</a></li>
    <li class="float-right"><a href='tematicas/biblioteca/insertar'>Introducir tem&aacute;tica</a></li>
    <li class="clear"></li>
</ul>

<table class="table table-hover">
	<thead>
    <tr>
      <th>Nombre tem&aacute;tica</th>
      <th class="span1 text-center"><i class="icon-bolt"></i></th>
		</tr>
	</thead>

  	<?php while ($tematica = mysqli_fetch_assoc($resultado)):?>
       	<tr>
            <td><a href="javascript:void(0)"><?php echo $tematica['nombre']; ?></a></td>
            <td class="span1 text-center">
	          	<div class="btn-group">
	    	      	 <a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="tematicas/biblioteca/editar/<?php echo $tematica['idtematica']; ?>" data-original-title="Editar"><i class="icon-pencil"></i></a>
	               <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="tematicas/biblioteca/eliminar/<?php echo $tematica['idtematica']; ?>" data-original-title="Eliminar"><i class="icon-remove"></i></a>
	            </div>
          	</td>
        </tr>
  	<?php endwhile;?>
</table>


