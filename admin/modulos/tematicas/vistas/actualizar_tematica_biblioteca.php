<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
    <li class="float-left"><a href="#" onclick="return false;">Tem&aacute;ticas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
    <li class="float-left"><a href="tematicas/biblioteca">Tem&aacute;ticas biblioteca</a></li>
    <li class="float-right"><a href='tematicas/biblioteca/insertar'>Introducir biblioteca</a></li>
    <li class="clear"></li>
</ul>

<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR DATOS TEM&Aacute;TICA</h5></div>
	<div class="block-content full">
		<form method='post' action='tematicas/biblioteca/editar/<?php echo $rowTematica['idtematica'] ?>' name='insertar tematica' id='frmNuevaTematicaBiblio' class="form-inline" >
        	<!-- div.row-fluid -->
            <div class="row-fluid">         
            	<!-- 1st Column -->
	            <div class="span6">
	            	<div class="control-group">
	                	<label for="columns-text" class="control-label">Tem&aacute;tica</label>
                        <div class="controls"><input type='text' name='atematica' value='<?php echo $rowTematica['nombre'];?>' /></div>
                    </div>
				</div>
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Guardar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-tematicas-edita_tematica_biblioteca.js"></script>



