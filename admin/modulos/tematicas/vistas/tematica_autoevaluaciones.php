<div class="submenuAdmin">TEM&Aacute;TICAS AUTOEVALUACIONES</div>

<!--  Opciones de tematicas -->
<div class='addAdmin'><a href='tematicas/autoevaluaciones/insertar'>Introducir Tem&aacute;tica</a></div>
<!-- Listado de Convocatorias -->
	<div class='buscador'>
		<P class='t_center'>Tematicas introducidas en este momento en el sistema:</P><br/>
			<form name='busca_tipo_curso' action='tematicas/autoevaluaciones' method='post'>
				<div class='t_center'>
					<select name='idtematica'>
						<option value='0'> - Tem&aacute;tica com&uacute;n - </option>
						<?php while ($tematica = mysqli_fetch_assoc($resultado)):?>
							<?php if($tematica['idtematica_autoeval'] == $post['idtematica']):?>
								<option value='<?php echo $tematica['idtematica_autoeval']; ?>' selected ><?php echo $tematica['tematica_autoeval']; ?></option>
							<?php else:?>
								<option value='<?php echo $tematica['idtematica_autoeval']; ?>'><?php echo $tematica['tematica_autoeval']; ?></option>
							<?php endif; ?>
						<?php endwhile;?>
					</select>
				</div>
				<br/>	
				<div class='t_center'>
					<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
					<span id="frmBtnBorrar" class='boton_borrar'><input type='submit' name='boton' value='Borrar' /></span>
				</div>
			</form>
	</div>
<!-- FIN de Opciones de tematicas -->

<?php 
if(isset($boton))
{
	switch ($boton)
	{
		case 'Insertar': 
			require('nueva_tematica_autoevaluacion.php');
			break;
			
		case 'Actualizar': 
			require('actualizar_tematica_autoevaluacion.php');
			break;		
	}
}
?>
<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar esta temática?');</script>
