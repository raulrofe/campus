<?php

class TematicasAdmin extends modeloExtend{
	
/***********************************************************************************************************************
 * TEMATICAS PARA LAS AUTOEVALUACIONES
 ***********************************************************************************************************************/
	
	// buscamos todas las tematicas de la autoevaluacion
	public function tematicas(){
		$sql = "SELECT * from tematica_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la autoevaluacion segun su id
	public function buscarTematicas($idTematica){
		$sql = "SELECT * from tematica_autoeval where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// insertamos un tipo de formato para la autoevaluacion
	public function insertarTematica($tematica){
		$sql = "INSERT into tematica_autoeval (tematica_autoeval) VALUES ('$tematica')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluacion
	public function eliminarTematica($idTematica)
	{
		$sql = "SELECT PTidautoeval from plantilla_autoeval where PTidtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = "DELETE from tematica_autoeval where idtematica_autoeval = ".$idTematica;
			$resultado = $this->consultaSql($sql);	
			return $resultado;
		}
		else
		{
			Alerta::guardarMensajeInfo('No puedes borrar esta tem&aacutetica porque hay autoevaluaciones que dependen de ella');
		}
	}
	
	// actualizamos el tipo de autoevaluacion
	public function actualizarTematica($tematica, $idTematica){
		$sql = "UPDATE tematica_autoeval SET tematica_autoeval = '$tematica' where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
/***********************************************************************************************************************
 * TEMATICAS PARA LA BIBLIOTECA
 ***********************************************************************************************************************/

	public function obtenerTematicasBiblioteca()
	{
		$sql = 'SELECT * FROM biblioteca_tematica';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnaTematicaBiblioteca($idtematica)
	{
		$sql = 'SELECT * FROM biblioteca_tematica WHERE idtematica = ' . $idtematica;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarTeamaticaBiblioteca($nombreTematica)
	{
		$sql = 'INSERT into biblioteca_tematica (nombre) VALUES ("' . $nombreTematica . '")';
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function editarTeamaticaBiblioteca($nombreTematica, $idtematica)
	{
		$sql = 'UPDATE biblioteca_tematica ' .
		'SET nombre = "' . $nombreTematica . '" ' .
		'WHERE idtematica = ' . $idtematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function eliminarTeamaticaBiblioteca($idtematica)
	{
		$sql = 'DELETE from biblioteca_tematica ' .
		'WHERE idtematica = ' . $idtematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
}
