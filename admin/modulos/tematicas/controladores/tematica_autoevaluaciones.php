<?php 

$objTematicas = new TematicasAdmin();

$get = Peticion::obtenerGet();

if(isset($get['f']))
{
	if($get['f'] == 'listar')
	{
		//Busco todas las tematicas para ser actualizada
		$resultado = $objTematicas->tematicas();
		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'listado_tematica_autoevaluacion'));
	}

	else if($get['f'] == 'insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(!empty($post['tematica']))
			{
				if($objTematicas->insertarTematica($post['tematica']))
				{
					Alerta::guardarMensajeInfo('Tem&aacute;tica insertada');
					Url::redirect('tematicas/autoevaluaciones');
				}
			}
		}

		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_tematica_autoevaluacion'));		
	}

	else if($get['f'] == 'editar')
	{
		if(isset($get['idTematica']) && is_numeric($get['idTematica']))
		{	
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if(!empty($post['atematica']))
				{
					if($objTematicas->actualizarTematica($post['atematica'], $get['idTematica']))
					{
						Alerta::guardarMensajeInfo('Tem&aacute;tica actualizada');
						Url::redirect('tematicas/autoevaluaciones');
					}
				}
			}

			$tematicas = $objTematicas->buscarTematicas($get['idTematica']);
			if($tematicas->num_rows == 1)
			{
				$rowTematica = mysqli_fetch_assoc($tematicas);
				require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'actualizar_tematica_autoevaluacion'));	
			}
		}
	}

	else if($get['f'] == 'eliminar')
	{
		if(isset($get['idTematica']) && $get['idTematica'])
		{
			if($objTematicas->eliminarTematica($get['idTematica']))
			{
				Alerta::guardarMensajeInfo('Temática eliminada');
				Url::redirect('tematicas/autoevaluaciones');
			}
		}
	}
}

