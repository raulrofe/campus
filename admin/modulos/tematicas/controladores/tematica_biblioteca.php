<?php 

$objTematicas = new TematicasAdmin();

$get = Peticion::obtenerGet();


if(isset($get['f']))
{
	if($get['f'] == 'listar')
	{
		//Busco todas las tematicas para ser actualizada
		$resultado = $objTematicas->obtenerTematicasBiblioteca();
		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'listado_tematica_biblioteca'));
	}

	else if($get['f'] == 'insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(!empty($post['tematica']))
			{
				if($objTematicas->insertarTeamaticaBiblioteca($post['tematica']))
				{
					Alerta::guardarMensajeInfo('Tem&aacute;tica insertada');
					Url::redirect('tematicas/biblioteca');
				}
			}
		}

		require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_tematica_biblioteca'));		
	}

	else if($get['f'] == 'editar')
	{
		if(isset($get['idTematica']) && is_numeric($get['idTematica']))
		{	
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if(!empty($post['atematica']))
				{
					if($objTematicas->editarTeamaticaBiblioteca($post['atematica'], $get['idTematica']))
					{
						Alerta::guardarMensajeInfo('Tem&aacute;tica actualizada');
						Url::redirect('tematicas/biblioteca');
					}
				}
			}

			$tematicas = $objTematicas->obtenerUnaTematicaBiblioteca($get['idTematica']);
			if($tematicas->num_rows == 1)
			{
				$rowTematica = mysqli_fetch_assoc($tematicas);
				require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'actualizar_tematica_biblioteca'));	
			}
		}
	}

	else if($get['f'] == 'eliminar')
	{
		if(isset($get['idTematica']) && is_numeric($get['idTematica']))
		{
			if($objTematicas->eliminarTeamaticaBiblioteca($get['idTematica']))
			{
				Alerta::guardarMensajeInfo('Temática eliminada');
				Url::redirect('tematicas/biblioteca');
			}
		}
	}
}



/*
if(isset($get['boton']))
{
	$boton = $get['boton'];
}
else if(isset($post['boton']))
{
	$boton = $post['boton'];
}

if(isset($boton))
{
	if($boton == 'Insertar')
	{
		if(!empty($post['tematica']))
		{
			if($objTematicas->insertarTeamaticaBiblioteca($post['tematica']))
			{
				Alerta::guardarMensajeInfo('Tem&aacute;tica insertada');
			}
		}
	}
	else if($boton == 'Actualizar')
	{
		if(isset($post['idtematica']) && is_numeric($post['idtematica']))
		{			
			if(!empty($post['atematica']))
			{
				if($objTematicas->editarTeamaticaBiblioteca($post['atematica'], $post['idtematica']))
				{
					Alerta::guardarMensajeInfo('Tem&aacute;tica actualizada');
				}
			}
			
			$tematicas = $objTematicas->obtenerUnaTematicaBiblioteca($post['idtematica']);
			$rowTematica = mysqli_fetch_assoc($tematicas);
		}
	}
	else if($boton == 'Borrar')
	{
		if(isset($post['idtematica']) && is_numeric($post['idtematica']))
		{

		}
	}
}

//Busco todas las tematicas para ser actualizada
$resultado = $objTematicas->obtenerTematicasBiblioteca();

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'index'));
*/
