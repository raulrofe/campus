$(document).ready(function()
{
	$("#frmNuevaTematicaBiblio").validate({
			errorElement: "div",
			messages: {
				atematica: {
					required: 'Introduce una tem&aacute;tica'
				}
			},
			rules: {
				atematica: {
					required  : true
				}
			}
		});
});