$(document).ready(function()
{
	$("#frmNuevaTematicaAutoeval").validate({
			errorElement: "div",
			messages: {
				atematica: {
					required: 'Introduce una tem&aacute;tica'
				}
			},
			rules: {
				atematica: {
					required  : true
				}
			}
		});
});