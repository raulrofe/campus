$(document).ready(function()
{
	$("#frmNuevaTematicaAutoeval").validate({
			errorElement: "div",
			messages: {
				tematica: {
					required: 'Introduce una tem&aacute;tica'
				}
			},
			rules: {
				tematica : {
					required  : true
				}
			}
		});
});