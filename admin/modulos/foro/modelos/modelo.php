<?php
class AModeloForo extends modeloExtend
{
	public function nuevoTema($titulo, $idrrhh, $idmodulo, $temaEsGenerals)
	{
		$sql = 'INSERT INTO foro_temas (titulo, idrrhh, fecha, idmodulo, es_general)' .
		' VALUES ("' . $titulo . '", "' . $idrrhh . '", "' . date('Y-m-d H:i:s') . '", ' . $idmodulo . ', ' . $temaEsGeneral . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoTemaCurso($titulo, $idrrhh, $idmodulo, $temaEsGeneral, $idCurso)
	{
		$sql = 'INSERT INTO foro_temas (titulo, idrrhh, fecha, idmodulo, es_general, idcurso)' .
		' VALUES ("' . $titulo . '", "' . $idrrhh . '", "' . date('Y-m-d H:i:s') . '", ' . $idmodulo . ', ' . $temaEsGeneral . ', ' . $idCurso . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerModulos()
	{
		$sql = 'SELECT md.idmodulo, md.nombre' .
		' FROM modulo AS md';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasPorModulo($idModulo)
	{
		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, rrhh.nombrec FROM foro_temas AS ft' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE ft.es_general = 0 AND ft.idmodulo = ' . $idModulo .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasGenerales()
	{
		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, rrhh.nombrec FROM foro_temas AS ft' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE ft.es_general = 1' .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerNombreCurso($idCurso)
	{
		$sql = 'SELECT titulo FROM curso AS c' .
		' WHERE c.idcurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnTema($idTema)
	{
		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.idmodulo, ft.es_general, ft.fecha, ft.idrrhh, rrhh.nombrec FROM foro_temas AS ft' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE ft.idforo_temas = ' . $idTema .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnTemaPorTitulo($idModulo, $titulo)
	{
		$sql = 'SELECT * FROM foro_temas AS ft' .
		' WHERE (ft.idmodulo = ' . $idModulo . ' OR ft.es_general = 1) AND ft.titulo = "' . $titulo . '"' .
		' GROUP BY ft.idforo_temas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarTema($idTema, $mensaje, $idModulo, $temaEsGeneral)
	{
		$sql = 'UPDATE foro_temas SET titulo = "' . $mensaje . '", idmodulo = ' . $idModulo . ', es_general = ' . $temaEsGeneral .
		' WHERE foro_temas.idforo_temas = ' . $idTema;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarTema($idTema)
	{
		$sql = 'DELETE FROM foro_temas' .
		' WHERE foro_temas.idforo_temas = ' . $idTema;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajes($idTema, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		$sql = 'SELECT fm.idforo_mensaje, fm.contenido, fm.fecha, fm.idusuario, foro_respuesta.idforo_mensaje AS id_mensaje_respuesta,' .
		' rrhh.foto AS t_foto, rrhh.nombrec AS t_nombrec, alumnos.foto AS a_foto, CONCAT(alumnos.nombre, " ", alumnos.apellidos) AS a_nombrec' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN rrhh ON CONCAT("t_", rrhh.idrrhh) = fm.idusuario' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIN foro_respuesta ON foro_respuesta.idforo_respuesta = fm.idforo_mensaje' .
		//' LEFT JOIN rrhh ON rrhh.idrrhh ON rrhh.idrrhh = fm.idusuario' .
		' WHERE foro_respuesta.idforo_respuesta IS NULL AND fm.idforo_temas = ' . $idTema .
		' GROUP BY fm.idforo_mensaje ORDER BY fm.fecha DESC' . $addQuery;
		/*' WHERE ft.idtemario = temario.idCurso';*/

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/**********************************************************************************************************************
	FORO PLANTILLAS
	**********************************************************************************************************************/
	public function obtenerUnTemaPorTituloPlantilla($idModulo, $titulo)
	{
		$sql = 'SELECT * FROM plantilla_foro_temas AS ft' .
		' WHERE (ft.idmodulo = ' . $idModulo . ' OR ft.es_general = 1) AND ft.titulo = "' . $titulo . '"' .
		' GROUP BY ft.idforo_temas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasPorModuloPlantilla($idModulo)
	{
		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, ft.prioridad, rrhh.nombrec FROM plantilla_foro_temas AS ft' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE ft.es_general = 0 AND ft.idmodulo = ' . $idModulo .
		' GROUP BY ft.idforo_temas ORDER BY ft.prioridad ASC, ft.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoTemaPlantilla($titulo, $idrrhh, $idmodulo, $temaEsGeneral, $prioridad)
	{
		$sql = 'INSERT INTO plantilla_foro_temas (titulo, idrrhh, fecha, idmodulo, es_general, prioridad)' .
		' VALUES ("' . $titulo . '", "' . $idrrhh . '", "' . date('Y-m-d H:i:s') . '", ' . $idmodulo . ', ' . $temaEsGeneral . ', ' . $prioridad .')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnTemaPlantilla($idTema)
	{
		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.idmodulo, ft.es_general, ft.fecha, ft.idrrhh' .
		' FROM plantilla_foro_temas AS ft' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE ft.idforo_temas = ' . $idTema .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

}