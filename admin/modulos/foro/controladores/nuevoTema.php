<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$objModelo = new modeloForo();

//if(Usuario::compareProfile(array('tutor', 'coodinador')))
//{
	// POST
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$post = Peticion::obtenerPost();
		if(isset($post['titulo'], $post['idModulo']) && is_numeric($post['idModulo']))
		{
			if(!empty($post['titulo']))
			{
				$rowTema = $objModelo->obtenerUnTemaPorTitulo($post['idModulo'], $post['titulo']);
				if($rowTema->num_rows == 0)
				{
					if($objModelo->nuevoTema($post['titulo'], $_SESSION['idusuario'], $post['idModulo']))
					{
						Alerta::guardarMensajeInfo('Se ha creado el nuevo tema');
						
						$idTema = $objModelo->obtenerUltimoIdInsertado();
						Url::redirect('aula/foro/' . $idTema);
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('Se ya existe un tema en ese m&oacute;dulo con el mismo nombre (o es un tema general)');
				}
			}
		}
		
		Url::redirect('aula/foro', true);
	}
	else
	{
		$modulos = $objModelo->obtenerModulos($_SESSION['idcurso']);
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoTema');
	}
//}