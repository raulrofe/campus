<?php
$objModelo = new AModeloForo();

$get = Peticion::obtenerGet();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['idtema'], $post['boton']) && is_numeric($post['idtema']) && $post['boton'] == 'Borrar')
	{
		if($objModelo->eliminarTema($post['idtema']))
		{
			Alerta::mostrarMensajeInfo('temaborrado','Se ha borrado el tema seleccionado');
		}
	}
	else if(isset($get['boton']) && $get['boton'] == 'Insertar')
	{
		if(isset($post['titulo'], $post['idModulo']) && is_numeric($post['idModulo']))
		{
			if(!empty($post['titulo']))
			{
				$temaEsGeneral = '0';
				if(isset($post['general']) && $post['general'] == '1')
				{
					$temaEsGeneral = '1';
				}
				
				$rowTema = $objModelo->obtenerUnTemaPorTitulo($post['idModulo'], $post['titulo']);
				if($rowTema->num_rows == 0)
				{
					if($objModelo->nuevoTema($post['titulo'], $_SESSION['idusuario'], $post['idModulo'], $temaEsGeneral))
					{
						Alerta::guardarMensajeInfo('Se ha creado el nuevo tema');
						
						if($temaEsGeneral)
						{
							Url::redirect('foros/-1');
						}
						else
						{
							Url::redirect('foros/' . $post['idModulo']);
						}
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('Se ya existe un tema en ese m&oacute;dulo con el mismo nombre (o es un tema general)');
				}
			}
		}
	}
}

$modulos = $objModelo->obtenerModulos();

if(isset($get['boton']) && $get['boton'] == 'Insertar')
{
	$modulos2 = $objModelo->obtenerModulos(); // para nuevo tema de foro
}
else if(isset($post['boton'], $post['idtema']) && $post['boton'] == 'Actualizar' && is_numeric($post['idtema']))
{
	$modulos2 = $objModelo->obtenerModulos(); // para nuevo tema de foro
	
	$unTema = $objModelo->obtenerUnTema($post['idtema']);
	if($unTema->num_rows == 1)
	{
		$unTema = $unTema->fetch_object();
		
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			if(isset($post['titulo'], $post['idModulo']) && is_numeric($post['idModulo']))
			{
				if(!empty($post['titulo']))
				{
					$temaEsGeneral = '0';
					if(isset($post['general']) && $post['general'] == '1')
					{
						$temaEsGeneral = '1';
					}
				
					$objModelo->actualizarTema($post['idtema'], $post['titulo'], $post['idModulo'], $temaEsGeneral);
					
					Alerta::guardarMensajeInfo('Tema editado correctamente');
					Url::redirect('foros');
				}
			}	
		}
	}
}

if(!empty($get['idmodulo']) && is_numeric($get['idmodulo']))
{
	if($get['idmodulo'] == '-1')
	{
		$temas = $objModelo->obtenerTemasGenerales();
	}
	else
	{
		$temas = $objModelo->obtenerTemasPorModulo($get['idmodulo']);
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'temas');