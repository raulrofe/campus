<?php

if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();
$objModelo = new modeloForo();

if(isset($get['idtema']) && is_numeric($get['idtema']))
{
	$tema = $objModelo->obtenerUnTema(Usuario::getIdCurso(), $get['idtema']);
	if($tema->num_rows == 1)
	{
		//$tema = $tema->fetch_object();
		//if(Usuarios::comprobarPermisosLogueo($tema->idrrhh, $tema->idperfil))
		//{
			Alerta::guardarMensajeInfo('Tema borrado');
			$objModelo->eliminarTema($get['idtema']);
		//}
	}
}

Url::redirect('aula/foro');