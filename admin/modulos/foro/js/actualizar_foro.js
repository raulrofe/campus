$(document).ready(function()
{
	$("#foroNuevoTema").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				idModulo: {
					required: 'Seleccione un m&oacute;dulo',
					min:      'Seleccione un m&oacute;dulo'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				idModulo : {
					required  : true,
					digits    : true,
					min       : 1
				}
			}
		});
});