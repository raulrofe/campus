<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Contenidos</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
	
  	<div class="stContainer" id="tabs">
		  	<div class="tab">
		  		<div class="submenuAdmin">Foros</div>
		  		<div id="admin_foro">
					<div>
						<div class='addAdmin'><a href='foros/tema/nuevo'>Introducir Tema Nuevo</a></div>
			  			<div class='buscador'>
			  				<p class='t_center'>Buscador de temas de foro que hay en el sistema en este momento</p><br/>
				  			<div class='t_center'>
				  					
				  				<select name="idmodulo" onchange="urlRedirect($(this).val());">
									<option value="foros">- Selecciona un m&oacute;dulo -</option>
				  					<option value="foros/-1" <?php if(isset($get['idmodulo']) && $get['idmodulo'] == '-1') echo 'selected="selected"'?>>Temas comunes</option>
					  				<?php if($modulos->num_rows > 0):?>
					  					<?php while($modulo = $modulos->fetch_object()):?>
											<option <?php if(isset($get['idmodulo']) && $get['idmodulo'] == $modulo->idmodulo) echo 'selected="selected"'?>
												value="foros/<?php echo $modulo->idmodulo?>"><?php echo $modulo->nombre?></option>
										<?php endwhile;?>
									<?php endif;?>
								</select>
								
				  				<?php if(isset($temas)):?>
				  					<br /><br />
					  				<?php if($temas->num_rows > 0):?>
										<form action="" method="post">
											<div class='t_center'>
												<select name="idtema">
													<?php while($tema = $temas->fetch_object()):?>
														<option value="<?php echo $tema->idforo_temas?>"><?php echo Texto::textoPlano($tema->titulo)?></option>
													<?php endwhile;?>
												</select>
												<br /><br />
											</div>
											<div class='t_center'>
												<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
												<span class='boton_borrar'><input id="frmBtnBorrar" type='submit' name='boton' value='Borrar' /></span>
											</div>
										</form>
									<?php else:?>
										<div class="t_center">Este m&oacute;dulo no tiene temas</div>
									<?php endif;?>
								<?php endif;?>
				  			</div>
				  		</div>
					</div>
					<div class='clear'></div>
				</div>
						
				<?php 
					if(isset($get['boton']) && $get['boton'] == 'Insertar')
					{
						require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoTema');
					}
					else if(isset($post['boton'], $unTema) && $post['boton'] == 'Actualizar')
					{
						require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editarTema');
					}
				?>
			</div>
	</div>
</div>

<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar este tema?');</script>