<div id="admin_usuarios">
	<p class="subtitleAdmin">INTRODUCIR NUEVO TEMA DE FORO</p>
		  				
  	<div class="formulariosAdmin">
	  	<div id="foro">
			<form id="foroNuevoTema" action="" method="post">
				<div class='filafrm'>
					<div class='etiquetafrm'>T&iacute;tulo del tema</div>
					<div class='campofrm'><input id="foroNuevotema_titulo" type="text" name="titulo" /></div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>M&oacute;dulo del curso</div>
					<div class='campofrm'>
						<select id="foroNuevotema_modulo" name="idModulo">
							<?php while($modulo = $modulos2->fetch_object()):?>
								<option value="<?php echo $modulo->idmodulo?>"><?php echo $modulo->nombre?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div><input type="checkbox" name="general" value="1" />
					<label>Este tema es general</label>
				</div>
				</div>
				<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>	
				</div>
				<button type="submit">Publicar en el foro</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-foro-nuevo_foro.js"></script>