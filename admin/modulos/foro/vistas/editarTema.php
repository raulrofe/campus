<div id="admin_usuarios">
	<p class="subtitleAdmin">ACTUALIZAR NUEVO TEMA DE FORO</p>
		  				
  	<div class="formulariosAdmin">
		<div id="foro">
			<form id="foroNuevoTema" action="foros/tema/editar/<?php echo $post['idtema']?>" method="post">
					<div class='filafrm'>
						<div class='etiquetafrm'>T&iacute;tulo del tema</div>
						<div class='campofrm'><input id="foroNuevotema_titulo" type="text" name="titulo" value="<?php echo Texto::textoPlano($unTema->titulo)?>" /></div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>M&oacute;dulo del curso</div>
						<div class='campofrm'>
							<select id="foroNuevotema_modulo" name="idModulo">
								<?php while($modulo = $modulos2->fetch_object()):?>
									<option value="<?php echo $modulo->idmodulo?>" <?php if($modulo->idmodulo == $unTema->idmodulo) echo 'selected="selected"'?>><?php echo $modulo->nombre?></option>
								<?php endwhile;?>
							</select>
						</div>
						<div class='obligatorio'>(*)</div>
					</div>
					<div class='filafrm'>
						<input type="checkbox" name="general" value="1" <?php if($unTema->es_general == 1) echo 'checked="checked"';?> /><label>Este tema es general</label>
					</div>
					<div class='filafrm'>
						<div class='obligatorio'>Campos obligatorios(*)</div>
					</div>

						<input type="hidden" name="boton" value="Actualizar" />
						<input type="hidden" name="idtema" value="<?php echo $post['idtema']?>" />
						<button type="submit">Actualizar tema</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-foro-actualizar_foro.js"></script>