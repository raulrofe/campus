$("#frm_agenda_ev_editar").validate({
		errorElement: "div",
		messages: {
			contenido: {
				required : 'Introduce una descripci&oacute;n para el evento'
			},
			fecha:	   {
				required  : 'Introduce una fecha correcta'
			}
		},
		rules: {
			contenido : {
				required  : true
			},
			fecha : {
				required  : true
			}
		}
	});