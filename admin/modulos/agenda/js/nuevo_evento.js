$("#frm_agenda_ev_nuevo").validate({
		errorElement: "div",
		messages: {
			contenido: {
				required : 'Introduce una descripci&oacute;n para el evento'
			},
			fecha:	   {
				required  : 'Introduce una fecha correcta'
			}
		},
		rules: {
			contenido : {
				required  : true
			},
			fecha : {
				required  : true
			}
		}
	});