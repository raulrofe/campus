$(document).ready(function()
{	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#agenda_recomendaciones_buscador input').keyup(function(event)
	{
		var wordSearch = $(this).val();
		wordSearch = LibMatch.escape(wordSearch);
		
		if(wordSearch != "")
		{
			var nombreCurso = '';
			$('#agenda_recomendaciones_listado_cursos > div').each(function(index)
			{
				nombreCurso = $(this).find('label').html();
				var regex = new RegExp('(' + wordSearch + ')', 'i');
				if(nombreCurso.match(regex))
				{
					$(this).removeClass('hide');
				}
				else
				{
					$(this).addClass('hide');
				}
			});
		}
		else
		{
			$('#agenda_recomendaciones_listado_cursos > div').removeClass('hide');
		}
	});
});