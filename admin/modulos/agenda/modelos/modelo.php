<?php
class AModeloAgendaAdmin extends modeloExtend
{
	public function actualizarResetearCursosRecomendacion($idconvocatoria)
	{
		$sql = 'UPDATE curso SET idrecomendacion = NULL WHERE idconvocatoria = ' . $idconvocatoria;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarCursoRecomendacion($idcurso, $idrecomnedacion)
	{
		$sql = 'UPDATE curso SET idrecomendacion = ' . $idrecomnedacion . ' WHERE idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function nuevoEventoRecomendacion($idRecomendacion, $contenido)
	{
		$sql = 'INSERT INTO agenda_recomendacion (id_recomendacion, contenido) VALUES (' . $idRecomendacion . ', "' . $contenido . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarEventoRecomendacion($idEvento, $contenido)
	{
		$sql = 'UPDATE agenda_recomendacion SET contenido = "' . $contenido . '"' .
		' WHERE idagenda_recomendacion = ' . $idEvento;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerEventosPorRecomendacion($idRecomendacion)
	{
		$sql = 'SELECT agd.idagenda_recomendacion, agd.id_recomendacion, agd.contenido, agd.fecha FROM agenda_recomendacion AS agd' .
		' WHERE agd.id_recomendacion = ' . $idRecomendacion .
		' ORDER BY agd.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnEventoRecomendacion($idEvento)
	{
		$sql = 'SELECT agd.idagenda_recomendacion, agd.id_recomendacion, agd.contenido, agd.fecha FROM agenda_recomendacion AS agd' .
		' WHERE agd.idagenda_recomendacion = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerListadoRecomendaciones()
	{
		$sql = 'SELECT rd.idrecomendacion, rd.titulo_recomendacion AS titulo, rd.descripcion, count(ra.idagenda_recomendacion) as nEvents' .
		' FROM recomendacion AS rd' .
		' LEFT JOIN agenda_recomendacion ra ON ra.id_recomendacion = rd.idrecomendacion' .
		' GROUP BY rd.idrecomendacion' .
		' ORDER BY titulo DESC';

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarEventoRecomendacion($idEvento)
	{
		$sql = 'DELETE FROM agenda_recomendacion WHERE idagenda_recomendacion = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	// RECOMENDACIONES //
	public function addRecomendacion($titulo, $descripcion)
	{
		$sql = 'INSERT INTO recomendacion (titulo_recomendacion, descripcion) VALUES ("' . $titulo . '", "' . $descripcion . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function updateRecomendacion($idrecomendacion, $titulo, $descripcion)
	{
		$sql = 'UPDATE recomendacion SET titulo_recomendacion = "' . $titulo . '", descripcion = "' . $descripcion . '" WHERE idrecomendacion = ' . $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRecomendacion($idrecomendacion)
	{
		$sql = 'SELECT idrecomendacion, titulo_recomendacion AS titulo, descripcion FROM recomendacion WHERE idrecomendacion = '. $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarRecomendacion($idrecomendacion)
	{
		$sql = 'DELETE FROM recomendacion WHERE idrecomendacion = ' . $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}