<?php
$objModeloAgenda = new AModeloAgendaAdmin();

$get = Peticion::obtenerGet();

if(isset($get['f']))
{
	if($get['f'] == 'listar')
	{
		//Vista por defecto		
		$recomendaciones = $objModeloAgenda->obtenerListadoRecomendaciones();
		$loadView = 'listar_agendas';
	}
	else if($get['f'] == 'Insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			if(isset($post['titulo']) && !empty($post['titulo']))
			{
				if($objModeloAgenda->addRecomendacion($post['titulo'], $post['descripcion']))
				{
					Alerta::guardarMensajeInfo('anadidoagenda','Se ha a&ntilde;adido la agenda');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('tituloagenda','Introduce un t&iacute;tulo para la agenda');
			}
		}

		$loadView = 'nueva_recomendacion';	
	}
	else if($get['f'] == 'Eliminar')
	{
		if(isset($get['idRcd']) && is_numeric($get['idRcd']))
		{
			$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($get['idRcd']);
			if($unaRecomendacion->num_rows == 1)
			{
				if($objModeloAgenda->eliminarRecomendacion($get['idRcd']))
				{
					Alerta::mostrarMensajeInfo('eliminadoagenda','Se ha eliminado la agenda');
				}
			}

			unset($unaRecomendacion);

			Url::redirect('agenda/recomendaciones');
		}
	}
	else if($get['f'] == 'ListarEventos')
	{
		if(isset($get['idRcd']) && is_numeric($get['idRcd']))
		{
			$recomendacion = $objModeloAgenda->obtenerRecomendacion($get['idRcd']);
			if($recomendacion->num_rows == 1)
			{
				//Recomendacion que engloba a los eventos
				$recomendacion = $recomendacion->fetch_object();

				//Eventos
				$eventos = $objModeloAgenda->obtenerEventosPorRecomendacion($get['idRcd']);	

				$loadView = 'listar_eventos';
			}
		}
	}
	else if($get['f'] == 'NuevoEvento')
	{
		if(isset($get['idRcd']) && is_numeric($get['idRcd']))	
		{
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if(isset($post['contenido']) && !empty($post['contenido']))
				{
							
					$objModeloAgenda->nuevoEventoRecomendacion($get['idRcd'], $post['contenido']);
					
					Alerta::guardarMensajeInfo('anadidoevento','Se ha creado el evento');
					Url::redirect('agenda/recomendacion/' . $get['idRcd'] . '/eventos');
				}	
			}

			$loadView = 'nuevo_evento'; 
		}
	}
	else if($get['f'] == 'EditarEvento')
	{
		if(isset($get['idEvento']) && is_numeric($get['idEvento']))
		{
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();		

				if(isset($post['contenido']) && !empty($post['contenido']))
				{
					$eventoRecomendacion = $objModeloAgenda->obtenerUnEventoRecomendacion($get['idEvento']);
					if($eventoRecomendacion->num_rows == 1)
					{
						$unEvento = $eventoRecomendacion->fetch_object();

						$objModeloAgenda->actualizarEventoRecomendacion($get['idEvento'], $post['contenido']);
					
						Alerta::guardarMensajeInfo('actualizadoevento','Se ha actualizado el evento');
					
						Url::redirect('agenda/recomendacion/' . $unEvento->id_recomendacion . '/eventos');
					}
				}
			}

			$eventoRecomendacion = $objModeloAgenda->obtenerUnEventoRecomendacion($get['idEvento']);
			if($eventoRecomendacion->num_rows == 1)
			{
				$unEvento = $eventoRecomendacion->fetch_object();
				$loadView = 'editar_Evento';
			}				
		}	
	}
	else if($get['f'] == 'EliminarEvento')
	{
		if(isset($get['idEvento']) && is_numeric($get['idEvento']))
		{
			$eventoRecomendacion = $objModeloAgenda->obtenerUnEventoRecomendacion($get['idEvento']);
			if($eventoRecomendacion->num_rows == 1)
			{
				$eventoRecomendacion = $eventoRecomendacion->fetch_object();
				if($objModeloAgenda->eliminarEventoRecomendacion($get['idEvento']))
				{
					Alerta::guardarMensajeInfo('eliminadoevento','Se ha eliminado el evento');
					Url::redirect('agenda/recomendacion/' . $eventoRecomendacion->id_recomendacion . '/eventos');
				}
			}				
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');

/*

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['boton']))
	{
		// insertar recoemndacion
		if($post['boton'] == 'Insertar')
		{
			if(isset($post['titulo'], $post['descripcion']))
			{
				if(!empty($post['titulo']))
				{
					if($objModeloAgenda->addRecomendacion($post['titulo'], $post['descripcion']))
					{
						Alerta::guardarMensajeInfo('Se ha a&ntilde;adido la agenda');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('Introduce un t&iacute;tulo para la agenda');
				}
			}
		}
		// actualizar recoemdnacion
		else if($post['boton'] == 'Actualizar')
		{
			if(isset($post['idRcd']) && is_numeric($post['idRcd']))
			{
				// obtenemos la recomendacion
				$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($post['idRcd']);
				if($unaRecomendacion->num_rows == 1)
				{
					$unaRecomendacion = $unaRecomendacion->fetch_object();
					
					if(isset($post['titulo'], $post['descripcion']))
					{
						if(!empty($post['titulo']))
						{
							if($objModeloAgenda->updateRecomendacion($post['idRcd'], $post['titulo'], $post['descripcion']))
							{
								Alerta::guardarMensajeInfo('Se ha actualizado la agenda');
								
								// asi actualizamos los datos de la recomendacion
								$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($post['idRcd']);
								$unaRecomendacion = $unaRecomendacion->fetch_object();
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('Introduce un t&iacute;tulo para la agenda');
						}
					}
				}
			}
		}
		// eliminar recomendacion
		else if($post['boton'] == 'Borrar')
		{
			if(isset($post['idRcd']) && is_numeric($post['idRcd']))
			{
				$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($post['idRcd']);
				if($unaRecomendacion->num_rows == 1)
				{
					if($objModeloAgenda->eliminarRecomendacion($post['idRcd']))
					{
						Alerta::mostrarMensajeInfo('eliminadoagenda','Se ha eliminado la agenda');
					}
				}
				unset($unaRecomendacion);
			}
		}
		// asignar cursos a una recomendacion
		else if($post['boton'] == 'Asignar')
		{
			if(isset($post['idRcd']) && is_numeric($post['idRcd']))
			{
				$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($post['idRcd']);
				if($unaRecomendacion->num_rows == 1)
				{
					$unaRecomendacion = $unaRecomendacion->fetch_object();
					
					mvc::cargarModeloAdicional('cursos', 'modelo');
					$objConvocatoria = new Convocatoria();

					//Busco las convocatorias
					$convocatorias = $objConvocatoria->convocatorias();
					
					if(isset($post['idConv']) && is_numeric($post['idConv']))
					{
						$unaConvocatoria = $objConvocatoria->buscar_convocatoria($post['idConv']);
						if(!empty($unaConvocatoria))
						{
							$objCurso = new Cursos();
							
							if(isset($post['cursos'], $post['boton2']) && is_array($post['cursos']) && $post['boton2'] == 'AsignarCursos')
							{
								$objModeloAgenda->actualizarResetearCursosRecomendacion($post['idConv']);
								
								foreach($post['cursos'] as $itemIdCurso)
								{
									if($objModeloAgenda->actualizarCursoRecomendacion($itemIdCurso, $post['idRcd']))
									{
										// nada
									}
								}
							
								Alerta::mostrarMensajeInfo('actualizadoagenda','Se ha  actualizado la agenda de este curso');
							}
							
							$cursos = $objCurso->cursosConvocatoria($post['idConv']);
						}
						else
						{
							$unaConvocatoria = null;
						}
					}
				}
			}
		}
		// listado de eventos de una recomendacion
		else if($post['boton'] == 'Eventos')
		{
			if(isset($post['idRcd']) && is_numeric($post['idRcd']))
			{
				$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($post['idRcd']);
				if($unaRecomendacion->num_rows == 1)
				{
					$unaRecomendacion = $unaRecomendacion->fetch_object();
					
					$eventos = $objModeloAgenda->obtenerEventosPorRecomendacion($unaRecomendacion->idrecomendacion);
				}
			}
		}
		// insertar evento en una recomendacion
		else if(isset($post['boton']) && $post['boton'] == 'InsertarEvento')
		{
			if(isset($post['contenido'], $post['fecha']) && !empty($post['contenido']))
			{
				$arrayFecha = explode('/', $post['fecha']);
				if(is_array($arrayFecha) && count($arrayFecha) == 3)
				{
					// comprueba si la fecha es valida
					if(checkdate($arrayFecha[1], $arrayFecha[0], $arrayFecha[2]))
					{
						// formatea la fecha para insertarla
						$fecha = $arrayFecha[2] . '-' . $arrayFecha[1] . '-' . $arrayFecha[0];
						
						$objModeloAgenda->nuevoEventoRecomendacion($get['idRcd'], $post['contenido'], $fecha);
						
						Alerta::guardarMensajeInfo('Se ha creado el evento');
					}
				}
			}
		}
		// actualizar un evento de una recomendacion
		else if(isset($post['boton']) && $post['boton'] == 'ActualizarEvento')
		{
			if(isset($post['contenido'], $post['fecha']) && !empty($post['contenido']))
			{
				$arrayFecha = explode('/', $post['fecha']);
				if(is_array($arrayFecha) && count($arrayFecha) == 3)
				{
					// comprueba si la fecha es valida
					if(checkdate($arrayFecha[1], $arrayFecha[0], $arrayFecha[2]))
					{
						// formatea la fecha para insertarla
						$fecha = $arrayFecha[2] . '-' . $arrayFecha[1] . '-' . $arrayFecha[0];
				
						$objModeloAgenda->actualizarEventoRecomendacion($get['idEvento'], $get['idRcd'], $post['contenido'], $fecha);
					
						Alerta::guardarMensajeInfo('Se ha actualizado el evento');
					
						Url::redirect('agenda/recomendacion/' . $get['idRcd'] . '/eventos');
					}
				}
			}
		}
	}
}

if(isset($get['boton']))
{
	// obtiene los datos para las vista de los eventos de las recomedanciones
	if($get['boton'] == 'InsertarEvento' || $get['boton'] == 'EditarEvento'
		|| $get['boton'] == 'BorrarEvento' || $get['boton'] == 'ListarEventos')
	{
		if(isset($get['idRcd']) && is_numeric($get['idRcd']))
		{
			$unaRecomendacion = $objModeloAgenda->obtenerRecomendacion($get['idRcd']);
			if($unaRecomendacion->num_rows == 1)
			{
				$unaRecomendacion = $unaRecomendacion->fetch_object();
			}
		}
		
		if($get['boton'] == 'ListarEventos')
		{
			$eventos = $objModeloAgenda->obtenerEventosPorRecomendacion($unaRecomendacion->idrecomendacion);
		}
		else if($get['boton'] == 'EditarEvento')
		{
			if(isset($get['idEvento']) && is_numeric($get['idEvento']))
			{
				$unEvento = $objModeloAgenda->obtenerUnEventoRecomendacion($get['idEvento']);
				if($unEvento->num_rows == 1)
				{
					$unEvento = $unEvento->fetch_object();
				}
			}
		}
		else if($get['boton'] == 'BorrarEvento')
		{
			if($objModeloAgenda->eliminarEventoRecomendacion($get['idEvento']))
			{
				Alerta::guardarMensajeInfo('Se ha eliminado el evento');
				Url::redirect('agenda/recomendacion/' . $get['idRcd'] . '/eventos');
			}
		}
	}
}

*/
