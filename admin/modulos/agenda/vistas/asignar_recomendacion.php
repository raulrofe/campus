<div id="admin_usuarios">
	<p class="subtitleAdmin">ASIGNAR T&Iacute;TULO DE AGENDA</p>
		  				
  	<div class="formulariosAdmin">
	  	<div id="agenda_recomendaciones">
			<form id="frm_agenda_recomendacion" method="post" action="agenda/recomendaciones">
				<ul>
					<li>
						<?php if(isset($post['boton']) && $post['boton'] == 'Asignar'):?>
					  		<select name="idConv" onchange="this.form.submit();">
					  			<option value="">- Selecciona una convocatoria -</option>
								<?php if($convocatorias->num_rows > 0):?>
						  			<?php while($convocatoria = $convocatorias->fetch_object()):?>
							  			<option value="<?php echo $convocatoria->idconvocatoria?>"
							  				<?php if(!empty($unaConvocatoria) && $convocatoria->idconvocatoria == $unaConvocatoria['idconvocatoria']) echo 'selected="selected"'?>>
							  					<?php echo Texto::textoFormateado($convocatoria->nombre_convocatoria)?></option>
							  		<?php endwhile;?>
							  	<?php endif;?>
						  	</select>
						  	
						  	<?php if(!empty($unaConvocatoria)):?>
							  	<br /><br />
							  	<div id="agenda_recomendaciones_buscador">
							  		<label>Buscador</label>
							  		<input type="text" name="buscador" />
							  	</div>
					  		<?php endif;?>
					  	<?php endif;?>

						<input type="hidden" name="idRcd" value="<?php echo $unaRecomendacion->idrecomendacion?>" />
						<input type="hidden" name="boton" value="Asignar" />
					</li>
				</ul>
				<div class="clear"></div>
			</form>
	
			<?php if(!empty($unaConvocatoria)):?>
				<br /><br />
				<form id="frm_agenda_recomendacion" method="post" action="agenda/recomendaciones">
					<div id="agenda_recomendaciones_listado_cursos">
						<?php if($cursos->num_rows > 0):?>
							<?php while($curso = $cursos->fetch_object()):?>
					  			<div>
					  				<input type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>"
					  					<?php if(isset($curso->idrecomendacion)) echo 'checked="checked"'?> />
					  				<label><?php echo Texto::textoFormateado($curso->titulo)?></label>
					  			</div>
					  		<?php endwhile;?>
							  		
					  		<br /><br />
					  		<div>
						  		<input type='hidden' name='boton' value='Asignar' />
						  		<input type='hidden' name='boton2' value='AsignarCursos' />
								<input type="hidden" name="idConv" value="<?php echo $unaConvocatoria['idconvocatoria']?>" />
								<input type="hidden" name="idRcd" value="<?php echo $unaRecomendacion->idrecomendacion?>" />
								<button type="submit">Asignar t&Iacute;tulo de agenda</button>
					  		</div>
								  		
					  	<?php else:?>
					  		<div>No hay cursos en esta convocatoria</div>
					  	<?php endif;?>
					</div>
				</form>
			<?php endif;?>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-agenda-asignar_recomendacion.js"></script>