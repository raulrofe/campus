<div id="admin_usuarios">
	<ul data-offset-top="250" data-spy="affix" class="breadcrumb affix-top">
	    <li class="float-left"><a href="agenda/recomendaciones">Acciones formativas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
	    <li class="float-left"><a href="agenda/recomendaciones">Agenda</a></li>
	    <li class="float-right"><a href='agenda/recomendacion/nueva'>Introducir agenda</a></li>
	    <li class="clear"></li>
	</ul>

  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas -->	  
	  	<h5><?php if(isset($f['nombre'])) echo $f['nombre']?></h5>			
	  	<div class="tab"><?php require_once mvc::obtenerRutaVista(dirname(__FILE__), $loadView); ?></div>
	</div>
</div>