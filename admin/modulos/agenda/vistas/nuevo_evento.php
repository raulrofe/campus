<div class="block block-themed themed-default">
	<div class="block-title"><h5>NUEVO EVENTO</h5></div>
	<div class="block-content full">
		<form id="frm_agenda_ev_nuevo" method="post" action="agenda/recomendacion/<?php echo $get['idRcd'] ?>/evento/nuevo">
        	<div>      
                <label for="columns-text" class="control-label">Descripci&oacute;n del evento</label><div class="clear"></div>					
                <textarea id="frm_agenda_ev_contenido" name="contenido" style="width:100%;height:200px;"></textarea>               		
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
  	</div>
</div>

<script type="text/javascript" src="js-agenda-nuevo_evento.js"></script>