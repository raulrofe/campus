<div class="block block-themed themed-default">
	<div class="block-title"><h5>EDITAR EVENTO</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="editEvent" action='agenda/recomendacion/evento/editar/<?php echo $get['idEvento'] ?>' method='post'>
        	<div>      
                <label for="columns-text" class="control-label">Descripci&oacute;n del evento</label><div class="clear"></div>					
                <textarea id="frm_agenda_ev_contenido" name="contenido" style="width:100%;height:200px;"><?php echo $unEvento->contenido?></textarea>               		
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-save"></i> Guardar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-agenda-actualizar_evento.js"></script>