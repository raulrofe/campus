<div id="admin_usuarios">
	<p class="subtitleAdmin">EDITAR T&Iacute;TULO DE AGENDA</p>
		  				
  	<div class="formulariosAdmin">
	  	<div id="agenda_recomendaciones">
			<form id="frm_agenda_recomendacion" method="post" action="">
				<div class='filafrm'>
					<div class='etiquetafrm'>T&iacute;tulo</div>
					<div class='campofrm'><input id="frm_agenda_recomendacion_titulo" type="text" name="titulo" value="<?php echo Texto::textoPlano($unaRecomendacion->titulo)?>" /></div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Descripci&oacute;n</div>
					<div class='campofrm'><textarea id="frm_agenda_recomendacion_descripcion" name="descripcion" cols="1" rows="1"><?php echo $unaRecomendacion->descripcion?></textarea></div>
				</div>
				<div class='filafrm'>
					<div class='obligatorio'>Campos obligatorios (*)</div>	
				</div>

						<input type="hidden" name="idRcd" value="<?php echo $unaRecomendacion->idrecomendacion?>" />
						<input type="hidden" name="boton" value="Actualizar" />
						<button type="submit">Actualizar agenda</button>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-agenda-actualizar_agenda.js"></script>