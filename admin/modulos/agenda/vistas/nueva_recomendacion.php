<div class="block block-themed themed-default">
	<div class="block-title"><h5>CREAR AGENDA</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmNuevoContenido" action='agenda/recomendacion/nueva' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
                	<div>
                    	<label for="columns-text" class="control-label">T&iacute;tulo</label>
                    	<div class="controls"><input type='text' name='titulo' style="width:100%;"/></div>
                    </div>
		                
		            <div class="clear"></div>
		         </div>
                
				<!-- Column -->
                	<div>
	                	<label>Descripción</label>
                        <div class="controls"><textarea name='descripcion' class="AreaFullWidth"></textarea></div>
	                </div>	                                                	                	                
             </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Crear Agenda</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-nuevo_modulo.js"></script>

<script type="text/javascript" src="js-agenda-nuevo_agenda.js"></script>