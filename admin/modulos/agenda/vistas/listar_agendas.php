<?php if($recomendaciones->num_rows > 0):?>
	<table class="table table-hover">
		<thead>
        	<tr>
                <th>Nombre de la agenda</th>
                <th style="text-align:center">N&ordm; de enventos</th>
                <th class="span1 text-center"><i class="icon-cogs"></i></th>
			</tr>
		</thead>
  		<?php while($recomendacion = $recomendaciones->fetch_object()):?>
        	<tr>
                <td><a href="agenda/recomendacion/<?php echo $recomendacion->idrecomendacion ?>/eventos"><?php echo Texto::textoFormateado($recomendacion->titulo)?></a></td>
                <td style="text-align:center"><?php echo $recomendacion->nEvents ?></td>
                <td class="span1 text-center">
                	<div class="btn-group">
                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="agenda/recomendacion/<?php echo $recomendacion->idrecomendacion ?>/eventos" data-original-title="Edit"><i class="icon-pencil"></i></a>
                        <a  class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="agenda/recomendacion/borrar/<?php echo $recomendacion->idrecomendacion ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
                    </div>
                </td>
            </tr>
  		<?php endwhile; ?>
  	</table> 
<?php endif;?>