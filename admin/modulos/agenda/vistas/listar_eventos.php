<h4><?php echo $recomendacion->titulo ?></h4>
<table class="table table-hover">
	<thead>
    	<tr>
            <th>Eventos agenda</th>
            <th style="width:15%;text-align:center"><a style="color:#FFF" href="agenda/recomendacion/<?php echo $recomendacion->idrecomendacion ?>/evento/nuevo">Introducir evento</a></th>
		</tr>
	</thead>	
    <?php if(isset($eventos) && $eventos->num_rows > 0):?>
		<?php while($evento = $eventos->fetch_object()):?>
      	<tr>
            <td><?php echo Texto::textoFormateado($evento->contenido)?></td>
            <td class="span1 text-center">
            	<div class="btn-group">
                	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="agenda/recomendacion/evento/editar/<?php echo $evento->idagenda_recomendacion ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
                    <a  class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="agenda/recomendacion/evento/borrar/<?php echo $evento->idagenda_recomendacion ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
                </div>
            </td>
        </tr>
		<?php endwhile;?>
    <?php else: ?>
        <tr><td>No se encontraron eventos</td></tr>
    <?php endif;?>
	</table> 
