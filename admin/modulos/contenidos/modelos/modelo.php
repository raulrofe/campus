<?php

/***********
ACCIONES FORMATIVAS
***********/

class AccionFormativa extends modeloExtend{
		
	// Buscar acciones formativas
	public function af($activos = 'true')
	{
		$sql = "SELECT * from accion_formativa "; 
		if($activos == 'true')
		{
			$sql.="where borrado = 0 ";
		}
		$sql.= "order by accion_formativa ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;	
	}
	
	//Accion formativa segun un id
	public function buscar_af($id)
	{
		if(is_numeric($id))
		{
			$sql = "SELECT * from accion_formativa AF, temario T, modulo M
			where AF.idaccion_formativa = ".$id." 
			and AF.idaccion_formativa = T.idaccion_formativa
			and T.idmodulo = M.idmodulo
			order by M.referencia_modulo ASC";
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}
	
	//Accion formativa segun codigo
	public function buscar_afCodigo($codigo)
	{
			$sql = "SELECT * from accion_formativa where accion_formativa = ".$codigo;
			$resultado = $this->consultaSql($sql);
			return $resultado;
	}
	
	//Buscamos las acciones formativas de una convocatoria
	public function afConvocatoria($idConvocatoria)
	{
		$sql = "SELECT * from accionformativarlt AFRLT , accion_formativa AF where AFRLT.idConvocatoria = ".$idConvocatoria." 
		and AFRLT.idAccionFormativa = AF.idaccion_formativa GROUP BY AF.idaccion_formativa ORDER BY accion_formativa ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos los modulos de una accion formativa
	public function modulosAF($idAF)
	{
		$sql = "SELECT * from temario T, modulo M where idaccion_formativa = ".$idAF." and T.idmodulo = M.idmodulo";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	//Insertamos una accion formativa y devolvemos su id
	public function insertAF($codigoAF)
	{
		$sql = "INSERT into accion_formativa " .
		"(accion_formativa)" .
		" VALUES ('" . $codigoAF . "')";
		
		$this->consultaSql($sql);
		
		return $this->obtenerUltimoIdInsertado();
	}
	
	// Creamos la Accion Formativa y relacionamos los modulos
	public function crear_af($nombre_af, $codigo_af, $idmodulo)
	{
		$sql = "SELECT * from accion_formativa where accion_formativa = " . $codigo_af;
		$resultado = $this->consultaSql($sql);
		$dataAF = mysqli_fetch_assoc($resultado);
		$activo = $dataAF['borrado'];
		$idAccion = $dataAF['idaccion_formativa'];
		if((mysqli_num_rows($resultado) == 0) and (is_numeric($codigo_af)))
		{
			$sql = "INSERT into accion_formativa" .
			" (accion_formativa, nombre_accion_formativa)" .
			" VALUES ('" . $codigo_af . "', '" . $nombre_af . "')";
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			for($i=0;$i<=count($idmodulo)-1;$i++)
			{				
				$sql = "SELECT * from temario 
				where idaccion_formativa = ".$id." 
				and idmodulo = ".$idmodulo[$i];
				$resultado = $this->consultaSql($sql);
				if(mysqli_num_rows($resultado) == 0)
				{
					$sql = "INSERT into temario (idaccion_formativa,idmodulo) VALUES ('$id','$idmodulo[$i]')";
					$this->consultaSql($sql);
				}				
			}
			Alerta::guardarMensajeInfo('La accion formativa ha sido creada');
			return $id;
		}
		else
		{
			if($activo == 1)
			{
				// Activo la accion formativa
				$sql = "UPDATE accion_formativa 
				SET borrado = 0 
				where accion_formativa = '".$nombre_af."'";
				$this->consultaSql($sql);
	
				//borro los modulos del temario
				$sql = "DELETE from temario where idaccion_formativa = ".$idAccion;
				$this->consultaSql($sql);
				
				// Inserto los modulos del temario
				for($i=0;$i<=count($idmodulo)-1;$i++)
				{				
					$sql = "INSERT into temario (idaccion_formativa,idmodulo) VALUES ('$idAccion','$idmodulo[$i]')";
					$resultado = $this->consultaSql($sql);		
				}
				Alerta::guardarMensajeInfo('La accion formativa ha sido creada');
				return $idAccion;
			}
			else
			{
				Alerta::alertSimple('accioncreada','La acción formativa ya esta creada');
			}
		}
	}
	
	// Creamos la Accion Formativa y relacionamos los modulos
	public function crearAfPorReferencia($nombre_af,$referencias)
	{
		$sql = "SELECT * from accion_formativa where accion_formativa = ".$nombre_af;
		$resultado = $this->consultaSql($sql);
		if((mysqli_num_rows($resultado) == 0) and (is_numeric($nombre_af)))
		{
			$sql = "INSERT into accion_formativa (accion_formativa) VALUES ('$nombre_af')";
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			for($i=0;$i<=count($referencias)-1;$i++)
			{	
				if(is_numeric($referencias[$i]))
				{
					$sql="SELECT * from modulo where referencia_modulo = '".$referencias[$i]."'";
					$resultado = $this->consultaSql($sql);
					$referencia = mysqli_fetch_assoc($resultado);
					$idmodulo = $referencia['idmodulo'];
					
					$sql = "SELECT * from temario 
					where idaccion_formativa = ".$id." 
					and idmodulo = ".$idmodulo;
					$resultado = $this->consultaSql($sql);
					if(mysqli_num_rows($resultado) == 0)
					{
						$sql = "INSERT into temario (idaccion_formativa,idmodulo) VALUES ('$id','$idmodulo')";
						$this->consultaSql($sql);
					}	
				}			
			}
			return $id;
		}
		//else echo "<script>alert('Ya existe una acci&oacute;n formativa con ese nombre en la base de datos')</script>";
	}
	
	// Asignamos modulos a los cursos
	public function asignar_modulo($idmodulo,$idcurso)
	{
		for($i=0;$i<=count($idmodulo)-1;$i++)
		{
			$sql = "select * 
			from curso C, accion_formativa AF, temario T
			where C.idcurso = ".$idcurso." 
			and C.idaccion_formativa = T.idaccion_formativa
			and T.idmodulo = ".$idmodulo[$i];
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			if(mysqli_num_rows($resultado) == 0)
			{
				$sql = "INSERT into temario (idmodulo,idaccion_formativa) VALUES ('$idmodulo[$i]','".$faf['idaccion_formativa']."')";
				echo $sql;
				$this->consultaSql($sql);
			}
		}
	}
	
	// Buscamos todos los modulos que pertenecen a un curso
	public function modulos_asignado_curso($idcurso)
	{
		$sql="SELECT DISTINCT * 
		from temario T , modulo M, curso C, accion_formativa AF 
		where C.idcurso = ".$idcurso."
		and C.idaccion_formativa = AF.idaccion_formativa
		and AF.idaccion_formativa = T.idaccion_formativa
		and T.idmodulo = M.idmodulo
		order by T.prioridad";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	

	// Actualizar una accion formativa a_adiendo un nuevo modulo
	public function actualizar_af($id, $nombreAF, $costeAF, $idmodulo)
	{
		$sql = "UPDATE accion formativa SET nombre_accion_formativa = '" . $nombreAF . "', coste = '" . $costeAF . "'" .
		" WHERE idaccion_formativa = " . $id;
		$this->consultaSql($sql);	
		
		for($i=0;$i<=count($idmodulo)-1;$i++)
		{
			$sql = "SELECT * from temario 
			where idaccion_formativa = ".$id." 
			and idmodulo = ".$idmodulo[$i];
			$resultado = $this->consultaSql($sql);
			if((mysqli_num_rows($resultado) == 0) and is_numeric($idmodulo[$i]))
			{
				$sql = "INSERT into temario (idaccion_formativa,idmodulo) VALUES ('$id','$idmodulo[$i]')";
				$this->consultaSql($sql);
			}				
		}
		return($id);
	}
	
	// Eliminar el modulo asignado momentaneamente
	public function eliminar_modulo_asignado($id,$idmodulo)
	{
		$sql = "SELECT * from temario where idaccion_formativa = ".$id." and idmodulo = ".$idmodulo;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) > 0)
		{
			$sql = "DELETE from temario where idaccion_formativa = ".$id." and idmodulo = ".$idmodulo;
			$this->consultaSql($sql);
		}
		return $id;
	}
	
	// Eliminamos la acci?n formativa al completo
	public function eliminar_accion_formativa($idaccion_formativa)
	{
		$sql = "UPDATE accion_formativa SET borrado = 1 where idaccion_formativa = ".$idaccion_formativa;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Asignamos los centros con rlt a la accion formativa
	public function asignarCentroAF($array)
	{
		$nCentros = count($array['centrosRLT']);
		for($i=0;$i<=$nCentros-1;$i++)
		{
			$sql = "SELECT * from accionformativarlt 
			where idConvocatoria = ".$array['idConvocatoria']." 
			and idAccionFormativa = ".$array['idAF']." 
			and idCentro = ".$array['centrosRLT'][$i];
			$resultado = $this->consultaSql($sql);
			if(mysqli_num_rows($resultado) == 0)
			{
				$sql = "INSERT into accionformativarlt (idConvocatoria,idAccionFormativa,idCentro) 
				VALUES ('".$array['idConvocatoria']."','".$array['idAF']."','".$array['centrosRLT'][$i]."')";
				$resultado = $this->consultaSql($sql);
			}
		}
	}
	
	//Asignamos los centros con rlt a la accion formativa
	public function asignarUnCentroAF($idConv,$idAF,$idRLT)
	{
		$sql = "INSERT into accionformativarlt (idConvocatoria,idAccionFormativa,idCentro) 
		VALUES ('".$idConv."','".$idAF."','".$idRLT."')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos los centros con rlt que perteneces a la accion formativa
	public function obtenerCentroAF($idAF, $idConv)
	{
		$sql = "SELECT * from accionformativarlt AFRLT, centros C 
		where AFRLT.idAccionFormativa = ".$idAF." and AFRLT.idConvocatoria = ".$idConv." and AFRLT.idCentro = C.idcentros";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos si un centro pertenece a una accion formativa
	public function obtenerUnCentroAF($idAF, $idCentro, $idConv)
	{
		$sql = "SELECT * from accionformativarlt AFRLT, centros C 
		where AFRLT.idAccionFormativa = ".$idAF." and AFRLT.idCentro = ".$idCentro." and AFRLT.idConvocatoria = ".$idConv;
		$resultado = $this->consultaSql($sql);
		return $resultado;		
	}
	
	public function volcarArchivosCurso($idAf, $idCurso)
	{
		$resultado = false;
		
		$sql = "SELECT * FROM plantilla_archivos_temario AS AT" .
		" LEFT JOIN temario AS T ON AT.idmodulo = T.idmodulo" .
		" LEFT JOIN modulo AS M ON M.idmodulo = T.idmodulo" .
		" WHERE T.idaccion_formativa = " . $idAf;
		$resultado1 = $this->consultaSql($sql);
		if($resultado1->num_rows > 0 )
		{
			while($row = $resultado1->fetch_object())
			{
				$enlaceArchivoPlantilla = $row->enlace_archivo;

				//Establezco el nombre de la carpeta
				switch($row->idcategoria_archivo)
				{
					case 1 :
						$folder = 'temas';
						break;
					
					case 2 :
						$folder = 'trabajos_practicos';
						break;
				}
				
				// Establecemos la ruta
				if(file_exists(PATH_ROOT . $row->enlace_archivo))
				{
					if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso))
					{
						mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso);
					}
					if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/" . $folder))
					{
						mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso . "/" . $folder);
					}
					if(file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/" . $folder))
					{
						$fecha = Fecha::fechaActual(false);
						$sql = "INSERT into archivo_temario" .
						" (titulo, descripcion, f_creacion, prioridad, idcurso, idmodulo, idcategoria_archivo)" .
						" VALUES ('$row->titulo', '$row->descripcion', '$fecha', '$row->prioridad', '$idCurso', '$row->idmodulo', '$row->idcategoria_archivo')";
						if($resultado = $this->consultaSql($sql))
						{
							$dir = "archivos/cursos/" . $idCurso . "/" . $folder . "/";
							$nombreArchivo = $this->obtenerUltimoIdInsertado();
							$extensionArchivo = Fichero::obtenerExtension(PATH_ROOT . $row->enlace_archivo);
							$enlaceArchivo = $dir . $nombreArchivo . "." . $extensionArchivo;
							chmod(PATH_ROOT . "archivos/cursos/" . $idCurso . "/" . $folder, 0777);
							chmod(PATH_ROOT . $enlaceArchivoPlantilla, 0777);
						
							if(copy(PATH_ROOT . $enlaceArchivoPlantilla, PATH_ROOT . $enlaceArchivo))
							{
								$sql = "UPDATE archivo_temario SET enlace_archivo = '$enlaceArchivo' where idarchivo_temario = " . $nombreArchivo;
								$resultado = $this->consultaSql($sql);
							}	
						}
					}
				}
			}
		}
		else
		{
			$resultado = $resultado1;
		}
		return $resultado;
	}
	
}
?>