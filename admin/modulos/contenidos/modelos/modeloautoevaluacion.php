<?php
class AutoevaluacionAdmin extends modeloExtend{

	private $idautoevaluacion;
	private $max;
	private $maxr;

	//constructor
	public function __construct(){parent::__construct();}

/******************************************************************************************
AUTOEVALUACIONES
******************************************************************************************/
	//Obtengo letras
	public static function obtener_letra($letra)
	{
		switch ($letra){
			case '1':
				$letra = 'A - ';
				break;
			case '2':
				$letra = 'B - ';
				break;
			case '3':
				$letra = 'C - ';
				break;
			case '4':
				$letra = 'D - ';
				break;
			case '5':
				$letra = 'E - ';
				break;
			case '6':
				$letra = 'F - ';
				break;
			case '7':
				$letra = 'G - ';
				break;
			case '8':
				$letra = 'H - ';
				break;
			case '9':
				$letra = 'I - ';
				break;
			case '10':
				$letra = 'J - ';
				break;
		}
		return $letra;
	}

	public function obtenerNumAutoevalsPorAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT ae.titulo_autoeval, c.fecha, c.nota FROM autoeval AS ae' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idAlumno . ' AND m.idcurso = ' . $idCurso .
		' LEFT JOIN calificaciones AS c ON c.idmatricula = m.idmatricula' .
		' WHERE ae.idautoeval = c.idautoeval GROUP BY c.idcalificaciones';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}


	// Establecemos el id de la autoevaluacion
	public function set_autoeval($idautoevaluacion)
	{
		$this->idautoevaluacion = $idautoevaluacion;
	}

	// buscamos todas las autoevaluaci_n
	public function autoeval()
	{
		$sql = "SELECT * from autoeval ORDER BY titulo_autoeval ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaci_n activas
	public function autoevalActivas()
	{
		$sql = "SELECT * from autoeval where borrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos autoevaluacion segun id pasado como parametro
	public function buscar_autoeval()
	{
		$sql = "SELECT * from autoeval where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos las autoevaluaciones de un curso
	public function autoeval_curso()
	{
		$sql = "SELECT *
		from curso C, accion_formativa AF, temario T, modulo M, autoeval A, test TE
		where C.idcurso = ".$_SESSION['idcurso']."
		and C.idaccion_formativa = AF.idaccion_formativa
		and AF.idaccion_formativa = T.idaccion_formativa
		and T.idmodulo = M.idmodulo
		and M.idmodulo = TE.idmodulo
		and A.borrado = 0
		GROUP BY TE.idautoeval";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// comprobamos si la autoevaluacion pertene al curso
	public function compruebaAutoeval($idautoevaluacion)
	{
		$sql = "SELECT *
		from curso C, accion_formativa AF, temario T, modulo M, autoeval A, test TE
		where C.idcurso = ".$_SESSION['idcurso']."
		and C.idaccion_formativa = AF.idaccion_formativa
		and AF.idaccion_formativa = T.idaccion_formativa
		and T.idmodulo = TE.idmodulo
		and TE.idautoeval = A.idautoeval
		and A.idautoeval = ".$idautoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) > 0)
		{
			return true;
		}
	}

	// insertamos un formato para la autoevaluacion
	public function insertar_autoevaluacion($titulo, $descripcion, $fechaCreacion, $idTipoAutoeval){
		$sql = "INSERT into autoeval (titulo_autoeval,descripcion,f_creacion,idtipo_autoeval) " .
		"VALUES ('" . mysqli_real_escape_string($this->_con, $titulo) . "', '" . mysqli_real_escape_string($this->_con, $descripcion) . "', '" . $fechaCreacion . "', '" . $idTipoAutoeval . "')";

		$this->consultaSql($sql);

		return $this->obtenerUltimoIdInsertado();
	}

	// eliminamos un tipo de formato para la autoevaluacion
	public function eliminar_autoeval()
	{
		$sql = "UPDATE autoeval SET borrado = 1 where idautoeval = ".$this->idautoevaluacion;
		$this->consultaSql($sql);
	}

	// actualizamos la autoevaluacion
	public function actualizar_autoeval($titulo,$idtematica,$descripcion,$horas,$minutos)
	{
		$titulo = $titulo;
		$descripcion = $descripcion;
		$tiempo = $horas.":".$minutos;
		$sql = "UPDATE autoeval SET
		titulo_autoeval = '$titulo',
		idtipo_autoeval = '$idtematica',
		descripcion = '$descripcion',
		tiempo = '$tiempo'
		where idautoeval = ".$this->idautoevaluacion;
		//echo $sql;
		$this->consultaSql($sql);
	}

	//Buscamos todas las autoevaluaciones que pertenecen a un curso
	public function autoevaluaciones_curso($idcurso){
		$sql = "SELECT * from curso C, modulo M, autoeval A, temario T
		where C.idcurso = ".$idcurso."
		and C.idcurso = T.idcurso
		and T.idmodulo = M.idmodulo
		and M.idmodulo = A.idmodulo";
		//echo $sql;
		$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}

	//Corregimos la autoevaluacion y devolvemos el resultado
	public function corregir_test()
	{
		$post = Peticion::obtenerPost();

		$sql = "SELECT * from preguntas where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$numero_preguntas = mysqli_num_rows($resultado);

		$matriz = 1;
		$acertada = 0;
		$respuestasBlanca = 0;
		$fallada = 0;

		$correccion = array();

		for($i=0;$i<=$numero_preguntas-1;$i++)
		{
			//Comprueba si existe una eleccion del alumno al enviar el test
			if(isset($post[$matriz.'eleccion']))
			{
				$eleccion = explode("_",$post[$matriz.'eleccion']);

				// Compruebo que existe una respuesta , la respuesta es $eleccion[1]
				if (!empty ($eleccion[1]))
				{
					$sql = "SELECT * from respuesta where idpreguntas = ".$eleccion[0]." and idrespuesta = ".$eleccion[1];
					//echo $sql;
					$resultado = $this->consultaSql($sql);
					$f = mysqli_fetch_assoc($resultado);

					if($f['correcta'] == '1')
					{
						$acertada++;
						$correccion['errores'][]=$eleccion[0];
					}
					else
					{
						$fallada++;
					}
				}
				$matriz++;
			}
			else
			{
				$respuestasBlanca++;
				$matriz++;
			}
		}


		$valor_preguntas = 10/$numero_preguntas;
		$acertadas = $valor_preguntas * $acertada;

		//Obtengo el valor de las respuestas contestadas mal
		$sql = "SELECT penalizacion_nc, penalizacion_blanco, nota_minima from test where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);

		$falladas = $f['penalizacion_nc'] * $fallada;
		$repuestasBlancas = $f['penalizacion_blanco'] * $respuestasBlanca;
		$valorErrores = $repuestasBlancas + $falladas;
		$nota = $acertadas - $valorErrores;

		if($nota<0)
		{
			$nota = 0;
		}

		$nota = number_format($nota, 2);
		$correccion['nota'] = $nota;
		$correccion['nota_minima'] = $f['nota_minima'];
		return $correccion;
	}

	public function copyConfigTest($penalizacionBlanco, $penalozacionNC, $nIntentos, $notaMinima, $tiempo, $correccion, $aleatorias, $idAutoevaluacion, $idModulo, $idCurso)
	{
		//Copio la configuracion
		$sql = "INSERT INTO test (penalizacion_blanco, penalizacion_nc, n_intentos, nota_minima, tiempo, correccion, aleatorias, idautoeval, idmodulo, idcurso) " .
		"VALUES ('" . $penalizacionBlanco . "', '" . $penalozacionNC . "', '" . $nIntentos . "', '" . $notaMinima . "', '" . $tiempo . "', '" .
					  $correccion . "', '" . $aleatorias . "', '" . $idAutoevaluacion . "', '" . $idModulo . "', '" . $idCurso . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

/******************************************************************************************
PREGUNTA Y RESPUESTAS
******************************************************************************************/

	// insertamos un pregunta
	public function insertar_pregunta($enunciado)
	{
		$sql = "SELECT * from preguntas where pregunta = '".$enunciado."' and idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		if(mysqli_num_rows($resultado) == 0)
		{
			$enunciado = $enunciado;
			$sql = "INSERT into preguntas (pregunta,idautoeval) VALUES ('$enunciado','$this->idautoevaluacion')";
			//echo $sql;
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
			{
				$tipoArchivo = $_FILES['imagen_pregunta']['type'];
				if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $id, 'pregunta');
			}
			$this->actualizar_pregunta($enunciado,$id);
		}
		else
		{
			Alerta::guardarMensajeInfo('La pregunta no ha sido creada porque ya existe en nuestra base de datos para esta autoevaluación');
			Url::redirect('contenidos/autoevaluaciones');
		}
	}

	// borramos una pregunta
	public function borrar_pregunta($idpregunta){
		$sql = "SELECT * from preguntas P, respuesta R
		where P.idpreguntas = ".$idpregunta."
		and P.idpreguntas = R.idpreguntas";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		while ($f = mysqli_fetch_assoc($resultado))
		{
			if(!empty ($f['imagen_pregunta']) and file_exists($f['imagen_pregunta']))
			{
				unlink($f['imagen_pregunta']);
			}
			if(!empty ($f['imagen_respuesta']) and file_exists($f['imagen_respuesta']))
			{
				unlink($f['imagen_respuesta']);
			}

		}

		$sql = "DELETE from preguntas where idpreguntas = ".$idpregunta;
		$this->consultaSql($sql);
	}

	// obtenemos todas las preguntas de una autoevaluacion
	public function preguntas(){
		$sql = "SELECT * from preguntas where idautoeval = ".$this->idautoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la ultima pregunta insertada en la base de datos
	public function buscar_ultima_pregunta(){
		$sql = "SELECT MAX(idpreguntas) from preguntas";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->max = $f['MAX(idpreguntas)'];
		return $this->max;
	}

	// buscamos una pregunta segun su id
	public function buscar_pregunta($idpregunta){
		$sql = "SELECT * from preguntas where idpreguntas = ".$idpregunta;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta
	public function buscarRespuesta($idRespuesta)
	{
		$sql = "SELECT * from respuesta where idrespuesta = ".$idRespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos el id de autoevaluacion segun su el id de pregunta
	public function idautoevaluacionPregunta($idpregunta){
		$sql = "SELECT idautoeval from preguntas where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f;
	}

	// Actualizamos una pregunta
	public function actualizar_pregunta($enunciado,$idpregunta){
		$return = true;

		$enunciado = $enunciado;
		$sql = "UPDATE preguntas SET pregunta = '$enunciado' where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
		{
			$sql = "SELECT * from preguntas where idpreguntas = ".$idpregunta;
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			$f = mysqli_fetch_assoc($resultado);
			if(!empty ($f['imagen_pregunta'])) {unlink($f['imagen_pregunta']);}
			$tipoArchivo = $_FILES['imagen_pregunta']['type'];
			if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/gif' || $tipoArchivo == 'image/png')
			{
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $idpregunta, 'pregunta');
			}
			else
			{
				Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');

				$return = false;
			}
		}
		return $return;
	}

	// buscamos una ultima respuesta
	public function buscar_ultima_respuesta(){
		$sql = "SELECT MAX(idrespuesta) from respuesta";
		$resultado = mysqli_query($this->con,$sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->maxr = $f['MAX(idrespuesta)'];
	}

	// buscamos todas las repuestas que pertenezcan a una pregunta
		public function buscar_respuestas($idpregunta){
		$sql = "SELECT * from respuesta where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta segun el id pasado
	public function buscar_una_respuesta($idrespuesta){
		$sql = "SELECT * from respuesta where idrespuesta = ".$idrespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos las respuestas para una pregunta
	public function insertar_respuesta($respuesta,$correcta,$max)
	{
		//print_r($respuesta);

		for($i=0;$i<=count($respuesta)-1;$i++){
			if(!empty ($respuesta[$i]))
			{

				$respuesta[$i] = $respuesta[$i];

				if($correcta[0]-1 == $i) $bien = 1;
				else $bien = 0;
				$sql = "Insert into respuesta (respuesta,correcta,idpreguntas) VALUES ('$respuesta[$i]','$bien','$max')";
				//echo $sql."<br/>";
				$this->consultaSql($sql);
				$num = $i+1;

				$id = $this->obtenerUltimoIdInsertado();


				if(isset($_FILES['imagen_respuesta_'.$num]['tmp_name']) && !empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
				{
					$tipoArchivo = $_FILES['imagen_respuesta_'.$num]['type'];
					if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
					{
						Autoevaluaciones::guardarFoto($_FILES['imagen_respuesta_'.$num], $id, 'respuesta');
					}
					else
					{
						Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
					}
				}
			}

			}
		}

	// actualizamos una respuesta
	public function actualizar_respuestas($respuesta,$correcta,$idrespuesta)
	{
			for($i=0;$i<=count($respuesta)-1;$i++)
			{
				if(!empty ($respuesta[$i]) && is_numeric($idrespuesta[$i]))
				{

					$respuesta[$i] = $respuesta[$i];

					if($correcta[0]-1 == $i) $bien = 1;
					else $bien = 0;
					$sql = "UPDATE respuesta SET
					respuesta = '$respuesta[$i]',
					correcta = '$bien'
					where idrespuesta = ".$idrespuesta[$i];
					//echo $sql."<br/>";
					$this->consultaSql($sql);
					$num = $i+1;

					if(!empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
					{

						$consulta = "SELECT * from respuesta where idrespuesta = ".$idrespuesta[$i];
						$resultado = $this->consultaSql($consulta);
						$f = mysqli_fetch_assoc($resultado);
						if(!empty ($f['imagen_respuesta'])) unlink($f['imagen_respuesta']);

							$info=GetImageSize($_FILES['imagen_respuesta_'.$num]['tmp_name']);
							switch ($info[2])
							{
							case 1:
								$extension='gif';
								break;
							case 2:
								$extension='jpg';
								break;
							case 3:
								$extension='png';
								break;
							default:
								$extension = '';
								break;
							}

						if($extension == 'gif' || $extension == 'jpg' || $extension == 'png')
						{

							$dir = "imagenes/autoevaluaciones/respuestas/";
							chmod($dir,0777);
							copy($_FILES['imagen_respuesta_'.$num]['tmp_name'],$dir.$idrespuesta[$i].".".$extension);
							$sql = "UPDATE respuesta SET imagen_respuesta = '".$dir.$idrespuesta[$i].".".$extension."'
							where idrespuesta = ".$idrespuesta[$i];
							$this->consultaSql($sql);
						}
						else
						{
							Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
						}
					}
				}
			}
	}
/******************************************************************************************
PLANTILLAS PARA LA AUTOEVALUACION
******************************************************************************************/

	//Buscardor de plantilla autoevaluacion
	public function buscadorPlantillaAutoevaluacion($idTematica, $tituloAutoevaluacion)
	{
		$addSql = '';
		if(!empty($idTematica))
		{
			$addSql = "AND pa.PTidtematica_autoeval = " . $idTematica . " ";
		}

		if(!empty($titulo))
		{
			$addSql .= "AND pa.PTtitulo_autoeval = " . $tituloAutoevaluacion . " ";
		}

		$sql = "SELECT * FROM plantilla_autoeval as pa LEFT JOIN tematica_autoeval AS ta on ta.idtematica_autoeval = pa.PTidtematica_autoeval " .
		"WHERE pa.PTborrado = 0 " . $addSql .
		" GROUP BY PTidautoeval";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos las autoevaluaciones de un curso
	public function obtenerAutoevaluacionesModulo($idModulo)
	{
		$sql = "SELECT * FROM plantilla_autoeval WHERE PTidmodulo = " .	$idModulo . " AND PTborrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaciones de la tabla plantillas
	public function plantillasAutoeval(){
		$sql = "SELECT * from plantilla_autoeval ORDER BY PTtitulo_autoeval ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaciones activas de la plantilla
	public function autoevalActivasPlantilla(){
		$sql = "SELECT * from plantilla_autoeval where PTborrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaciones activas de la plantilla segun su tematica
	public function autoevalActivasTematicaPlantilla($idTematica){
		$sql = "SELECT * from plantilla_autoeval
		where PTborrado = 0
		and PTidtematica_autoeval = ".$idTematica;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Obtengo una autoevaluacion de la plantilla segun id pasado como parametro
	public function getAutoevaluacionPlantilla($idAutoevaluacion){
		$sql = "SELECT * from plantilla_autoeval where PTidautoeval = " . $idAutoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos autoevaluacion segun id pasado como parametro de la tabla plantilla
	public function buscar_autoevalPlantilla(){
		$sql = "SELECT * from plantilla_autoeval where PTidautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un formato para la autoevaluacion plantilla
	public function insertar_autoevaluacionPlantilla($titulo, $descripcion, $fechaCreacion, $idTipoAutoeval, $idTematiAutoeval,	$penalizacionBlanco,
					$penalizacionNoContesta, $numeroIntentos, $notaMinima, $tiempoEjecucion, $modoCorreccion, $aleratorias){

		$sql = "INSERT into plantilla_autoeval " .
		"(PTtitulo_autoeval, PTdescripcion, PTf_creacion, PTidtipo_autoeval, PTidtematica_autoeval, PTpenalizacion_blanco,
		  PTpenalizacion_nc, PTn_intentos, PTnota_minima, PTtiempo, PTcorreccion, PTaleatorias) " .
		" VALUES ('$titulo', '$descripcion', '$fechaCreacion', '$idTipoAutoeval', '$idTematiAutoeval', '$penalizacionBlanco',
				  '$penalizacionNoContesta', '$numeroIntentos', '$notaMinima', '$tiempoEjecucion', '$modoCorreccion', '$aleratorias')";

		$this->consultaSql($sql);

		return $this->obtenerUltimoIdInsertado();
	}

	// actualizamos la autoevaluacion de la plantilla
	public function actualizar_autoevalPlantilla($titulo, $descripcion, $idTipoAutoeval, $idTematiAutoeval, $penalizacionBlanco,
					$penalizacionNoContesta, $numeroIntentos, $notaMinima, $tiempoEjecucion, $modoCorreccion, $aleatorias, $idAutoeval){


		$sql = "UPDATE plantilla_autoeval" .
		" SET PTtitulo_autoeval = '" . $titulo . "'," .
		" PTdescripcion = '" . $descripcion ."'," .
		" PTn_intentos = '" . $numeroIntentos . "'," .
		" PTnota_minima = '" . $notaMinima . "'," .
		" PTpenalizacion_blanco = '" . $penalizacionBlanco ."'," .
		" PTpenalizacion_nc = '" . $penalizacionNoContesta ."'," .
		" PTcorreccion = '" . $modoCorreccion ."'," .
		" PTidtematica_autoeval = '" . $idTematiAutoeval . "'," .
		" PTidtipo_autoeval = '" . $idTipoAutoeval ."'," .
		" PTtiempo = '" . $tiempoEjecucion . "'," .
		" PTaleatorias = '" . $aleatorias . "'" .
		" WHERE PTidautoeval = " . $idAutoeval;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluacion para la tabla plantilla
	public function eliminar_autoevalPlantilla($idAutoevaluacion)
	{
		$sql = "UPDATE plantilla_autoeval " .
		"SET PTborrado = 1 " .
		"WHERE PTidautoeval = " . $idAutoevaluacion;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function copiarPregunta($enunciado, $img, $idautoeval)
	{
		//echo $enunciado . 'ss</br>';exit;
		$enunciadoo = addslashes($enunciado);
		$sql = "INSERT into preguntas (pregunta, imagen_pregunta, idautoeval) VALUES ('" . $enunciadoo . "', '" . $img . "', '" .$idautoeval . "')";
		$this->consultaSql($sql);
		return $this->obtenerUltimoIdInsertado();
	}

	public function copiarRespuesta($respuesta, $img, $correcta, $idpregunta)
	{
		$respuesta = addslashes($respuesta);
		$sql = "INSERT into respuesta (respuesta, imagen_respuesta, correcta, idpreguntas) VALUES ('" . $respuesta . "', '" . $img . "', '" . $correcta . "', '" . $idpregunta . "')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un pregunta para la tabla plantilla
	public function insertar_preguntaPlantilla($enunciado)
	{
		$sql = "SELECT * from plantilla_preguntas where PTpregunta = '".$enunciado."' and PTidautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		if(mysqli_num_rows($resultado) == 0)
		{
			$enunciado = $enunciado;
			$sql = "INSERT into plantilla_preguntas (PTpregunta,PTidautoeval) VALUES ('$enunciado','$this->idautoevaluacion')";
			//echo $sql;
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
			{
				$tipoArchivo = $_FILES['PTimagen_pregunta']['type'];
				if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $id, 'pregunta');
			}
			$this->actualizar_pregunta($enunciado,$id);
		}
		else
		{
			Alerta::guardarMensajeInfo('La pregunta no ha sido creada porque ya existe en nuestra base de datos para esta autoevaluación');
			Url::redirect('contenidos/autoevaluaciones');
		}
	}

	// Actualizamos una pregunta
	public function actualizar_preguntaPlantilla($enunciado,$idpregunta)
	{
		$confirmacion='';
		$enunciado = $enunciado;

		$sql="SELECT PTidpreguntas from plantilla_preguntas where PTpregunta = '$enunciado' and PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = "UPDATE plantilla_preguntas SET PTpregunta = '$enunciado' where PTidpreguntas = ".$idpregunta;
			$resultado = $this->consultaSql($sql);
			if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
			{
				$sql = "SELECT * from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
				//echo $sql;
				$resultado = $this->consultaSql($sql);
				$f = mysqli_fetch_assoc($resultado);
				if(!empty ($f['PTimagen_pregunta'])) {unlink($f['PTimagen_pregunta']);}
				$tipoArchivo = $_FILES['imagen_pregunta']['type'];
				if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/gif' || $tipoArchivo == 'image/png')
				{
					Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $idpregunta, 'pregunta');
					$confirmacion.= 'Imagen pregunta actualizada';
				}
				else
				{
					Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
					$confirmacion.= 'error en la extensi&oacute;n de la imagen de la pregunta';
				}
			}
			$confirmacion.= ', pregunta actualiza';
		}
		else
		{
			$confirmacion.=', el enunciado de la pregunta es el mismo';
		}
		return $confirmacion;
	}

	// borramos una pregunta para la tabla plantilla
	public function borrar_preguntaPlantilla($idpregunta)
	{
		$sql = "SELECT * from plantilla_preguntas P, plantilla_respuesta R
		where P.PTidpreguntas = ".$idpregunta."
		and P.PTidpreguntas = R.PTidpreguntas";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		while ($f = mysqli_fetch_assoc($resultado))
		{
			if(!empty ($f['PTimagen_pregunta']) and file_exists($f['PTimagen_pregunta']))
			{
				unlink($f['PTimagen_pregunta']);
			}
			if(!empty ($f['PTimagen_respuesta']) and file_exists($f['PTimagen_respuesta']))
			{
				unlink($f['PTimagen_respuesta']);
			}

		}

		$sql = "DELETE from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// obtenemos todas las preguntas de una autoevaluacion de la tabla plantilla
	public function preguntasPlantilla($idAutoevaluacion){
		$sql = "SELECT * FROM plantilla_preguntas AS pp " .
		"LEFT JOIN plantilla_respuesta AS pr ON pp.PTidpreguntas = pr.PTidpreguntas " .
		"WHERE pp.PTidpreguntas IS NOT NULL AND PTidautoeval = " . $idAutoevaluacion . ' GROUP BY pp.PTidpreguntas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// buscamos la ultima pregunta insertada en la base de datos para la plantilla
	public function buscar_ultima_preguntaPlantilla(){
		$sql = "SELECT MAX(PTidpreguntas) from plantilla_preguntas";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->max = $f['MAX(PTidpreguntas)'];
		return $this->max;
	}

	// buscamos una pregunta segun su id para la tabla plantilla
	public function buscar_preguntaPlantilla($idpregunta){
		$sql = "SELECT * from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// borramos una respuesta para la tabla plantilla
	public function borrar_respuestasPlantilla($idrespuesta)
	{
		$sql = "SELECT * from plantilla_respuesta R
		where R.PTidrespuesta = ".$idrespuesta;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) > 0)
		{
			if(!empty ($f['PTimagen_respuesta']) and file_exists($f['PTimagen_respuesta']))
			{
				unlink($f['PTimagen_respuesta']);
			}
			$sql = "DELETE from plantilla_respuesta where PTidrespuesta = ".$idrespuesta;
			$this->consultaSql($sql);
		}
	}

	//buscamos una respuesta de la tabla plantilla
	public function buscarRespuestaPlantilla($idRespuesta)
	{
		$sql = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idRespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos el id de autoevaluacion segun su el id de pregunta de la tabla plantilla
	public function idautoevaluacionPreguntaPlantilla($idpregunta){
		$sql = "SELECT PTidautoeval from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f;
	}

	// buscamos una ultima respuesta para la plantilla
	public function buscar_ultima_respuestaPlantilla(){
		$sql = "SELECT MAX(PTidrespuesta) from plantilla_respuesta";
		$resultado = mysqli_query($this->con,$sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->maxr = $f['MAX(PTidrespuesta)'];
	}

	// buscamos todas las repuestas que pertenezcan a una pregunta para la plantilla
	public function buscar_respuestasPlantilla($idpregunta){
		$sql = "SELECT * from plantilla_respuesta where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta segun el id pasado
	public function buscar_una_respuestaPlantilla($idrespuesta){
		$sql = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idrespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos las respuestas para una pregunta
	public function insertar_respuestaPlantilla($respuesta,$correcta,$max){
		//print_r($respuesta);

		for($i=0;$i<=count($respuesta)-1;$i++)
		{
			if(!empty ($respuesta[$i]))
			{

				$respuesta[$i] = $respuesta[$i];

				if($correcta[0]-1 == $i) $bien = 1;
				else $bien = 0;
				$sql = "Insert into plantilla_respuesta (PTrespuesta,PTcorrecta,PTidpreguntas) VALUES ('$respuesta[$i]','$bien','$max')";
				$this->consultaSql($sql);
				$num = $i+1;

				$id = $this->obtenerUltimoIdInsertado();


				if(isset($_FILES['imagen_respuesta_'.$num]['tmp_name']) && !empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
				{
					$tipoArchivo = $_FILES['imagen_respuesta_'.$num]['type'];
					if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
					{
						Autoevaluaciones::guardarFoto($_FILES['imagen_respuesta_'.$num], $id, 'respuesta');
					}
					else
					{
						Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
					}
				}
			}

		}
	}

	// actualizamos una respuesta para la plantilla
	public function actualizar_respuestasPlantilla($respuesta,$correcta,$idrespuesta){
		$confirmacion='';
			for($i=0;$i<=count($respuesta)-1;$i++)
			{
				if(!empty ($respuesta[$i]) && is_numeric($idrespuesta[$i]))
				{
					echo $i;

					$respuesta[$i] = $respuesta[$i];

					if($correcta[0]-1 == $i) $bien = 1;
					else $bien = 0;
					$sql = "UPDATE plantilla_respuesta SET
					PTrespuesta = '$respuesta[$i]',
					PTcorrecta = '$bien'
					where PTidrespuesta = ".$idrespuesta[$i];
					//echo $sql."<br/>";
					$this->consultaSql($sql);
					$num = $i+1;

					if(!empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
					{

						$consulta = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idrespuesta[$i];
						$resultado = $this->consultaSql($consulta);
						$f = mysqli_fetch_assoc($resultado);
						if(!empty ($f['PTimagen_respuesta'])) unlink($f['PTimagen_respuesta']);

							$info=GetImageSize($_FILES['imagen_respuesta_'.$num]['tmp_name']);
							switch ($info[2])
							{
								case 1:
									$extension='gif';
									break;
								case 2:
									$extension='jpg';
									break;
								case 3:
									$extension='png';
									break;
								default:
									$extension = '';
									break;
							}

						if($extension == 'gif' || $extension == 'jpg' || $extension == 'png')
						{

							$dir = "imagenes/autoevaluaciones/respuestas/";
							chmod($dir,0777);
							copy($_FILES['imagen_respuesta_'.$num]['tmp_name'],$dir.$idrespuesta[$i].".".$extension);
							$sql = "UPDATE plantilla_respuesta SET PTimagen_respuesta = '".$dir.$idrespuesta[$i].".".$extension."'
							where PTidrespuesta = ".$idrespuesta[$i];
							$this->consultaSql($sql);
						}
						else
						{
							Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
						}
					}
				}
				else
				{
					if(is_numeric($idrespuesta[$i]))
					{
						$sql = "SELECT PTcorrecta from plantilla_respuesta where PTidrespuesta = ".$idrespuesta[$i];
						$resultado = $this->consultaSql($sql);
						$f = mysqli_fetch_assoc($resultado);
						if($f['PTcorrecta'] != 1)
						{
							$sql = "DELETE from plantilla_respuesta where PTidrespuesta = ".$idrespuesta[$i];
							$this->consultaSql($sql);
						}
						else
						{
							$confirmacion.= 'No ha sido posible borar la respuesta por ser la correcta';
						}
					}
				}
			}
		if(empty($confirmacion))
		{
			$confirmacion.='Respuesta actualizada';
		}
		return $confirmacion;
	}

	//eliminamos la imagen de una respuesta
	public function eliminarImagenRespuestaPlantilla($id){
		$sql = "UPDATE plantilla_respuesta SET PTimagen_respuesta = '' where PTidrespuesta = ".$id;
		//echo $sql;
		return $this->consultaSql($sql);
	}

	public function configuracionTest($blanco, $nc, $nIntentos, $notaMinima, $aleatorio, $tiempo, $modoCorreccion, $idmodulo, $idautoeval)
	{
		$sql = "INSERT into test
		(penalizacion_blanco, penalizacion_nc, n_intentos, nota_minima, correccion, tiempo, aleatorias, idmodulo, idautoeval)
		VALUES ('$blanco','$nc','$nIntentos','$notaMinima','$modoCorreccion','$tiempo','$aleatorio','$idmodulo','$idautoeval')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function actualizarAsignacion($idmodulo, $n_intentos, $penalizacion_blanco, $penalizacion_nc, $nota_minima, $tiempo,
	$modoCorreccion, $aleatoria, $idAsignacion)
	{
		$sql = "UPDATE test SET
		penalizacion_blanco = '$penalizacion_blanco',
		penalizacion_nc = '$penalizacion_nc',
		n_intentos = '$n_intentos',
		nota_minima = '$nota_minima',
		tiempo = '$tiempo',
		correccion = '$modoCorreccion',
		aleatorias = '$aleatoria',
		idmodulo = '$idmodulo'
		where idtest = ".$idAsignacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

/******************************************************************************************
TIPO AUTOEVALUACION TEST / DESARROLLO
******************************************************************************************/

	// buscamos todas las tematicas de la autoevaluaci_n
	public function tematica_autoevaluacion(){
		$sql = "SELECT * from tipo_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un tipo de formato para la autoevaluaci_n
	public function tipo_autoeval($formato){
		$sql = "INSERT into tipo_autoeval (formato) VALUES ('$formato')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluaci_n
	public function eliminar_tipo_autoeval($formato){
		$sql = "DELETE tipo_autoeval where idtipo_autoeval = ".$idtipo_autoeval;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// actualizamos el tipo de autoevaluacion
	public function actualizar_tipo_autoeval($formato){
		$sql = "UPDATE tipo_autoeval SET formato = '$formato' where idtipo_autoeval = ".$idtipo_autoeval;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


/******************************************************************************************
TEMATICAS, RAMAS O FAMILIAS DE AUTOEVALUACION
******************************************************************************************/

	// buscamos todas las tematicas de la autoevaluacion
	public function tematicas(){
		$sql = "SELECT * from tematica_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la autoevaluacion segun su id
	public function buscarTematicas($idTematica){
		$sql = "SELECT * from tematica_autoeval where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un tipo de formato para la autoevaluacion
	public function insertarTematica($tematica){
		$sql = "INSERT into tematica_autoeval (tematica_autoeval) VALUES ('$tematica')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluacion
	public function eliminarTematica($idTematica){
		$sql = "SELECT PTidautoeval from plantilla_autoeval where PTidtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = "DELETE from tematica_autoeval where idtematica_autoeval = ".$idTematica;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
		else
		{
			Alerta::guardarMensajeInfo('No puedes borrar esta tem&aacutetica porque hay autoevaluaciones que dependen de ella');
		}
	}

	// actualizamos el tipo de autoevaluacion
	public function actualizarTematica($tematica, $idTematica){
		$sql = "UPDATE tematica_autoeval SET tematica_autoeval = '$tematica' where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


/*******************************************************************************************
INTRODUCCION DE CALIFICACIONES
*******************************************************************************************/
	//Creamos el registro para que cuente como un intento
	public function cuenta_intento($idmatricula,$idautoevaluacion)
	{
		$sql = "INSERT into calificaciones (idmatricula,idautoeval) VALUES ('$idmatricula','$idautoevaluacion')";
		//echo $sql.'<br/>';
		$this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		return $id;
	}

	//Introducimos la nota de la autoevaluacion
	public function insertar_calificacion($idmatricula,$idautoevaluacion,$nota,$idcalificaciones)
	{
		$fecha = date("Y-m-d H:i:s");
		$sql = "UPDATE calificaciones SET
		nota = '$nota',
		fecha = '$fecha'
		where idcalificaciones = ".$idcalificaciones;
		//echo $sql.'<br/>';
		$this->consultaSql($sql);
	}

	//Comprobar numero de intentos
	public function numero_intentos($idmatricula,$idautoevaluacion)
	{
		$sql = "SELECT * from calificaciones where idmatricula = ".$idmatricula." and idautoeval = ".$idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Obtengo el numero de acceso de la configuracion de calificaciones
	public function configuracion_numero_acceso($idautoevaluacion)
	{
		$sql = "SELECT n_intentos from test
		where idautoeval = ".$idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f['n_intentos'];
	}


	//Respuestas de los alumnos
	public function respuestasAlumnos($idCalificacion)
	{
		$post = Peticion::obtenerPost();

		$sql = "SELECT * from preguntas where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$numero_preguntas = mysqli_num_rows($resultado);

		$matriz = 1;
		for($i=0;$i<=$numero_preguntas-1;$i++)
		{
			//Comprueba si existe una eleccion del alumno al enviar el test
			if(isset($post[$matriz.'eleccion']))
			{
				$eleccion = explode("_",$post[$matriz.'eleccion']);

				// Compruebo que existe una respuesta , la respuesta es $eleccion[1]
				if (!empty ($eleccion[1]))
				{
					$sql = "INSERT into respuestasalumno (idCalificaciones,idPreguntas,idRespuestas) VALUES ('$idCalificacion','$eleccion[0]','$eleccion[1]')";
					//echo $sql;
					$this->consultaSql($sql);
				}
				$matriz++;
			}
			else{$matriz++;}
		}
	}

	//Buscasmos la autoevaluacion segun el id de Calificacion
	public function datosCalificacion($idCalificacion)
	{
		$sql = "SELECT * from Calificaciones where idcalificaciones = ".$idCalificacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos las respuestas almacenadas de los alumnos segun el id de calificacion
	public function alumnosCalificacion($idCalificacion)
	{
		$sql = "SELECT * from respuestasalumno where idCalificaciones = ".$idCalificacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos el alumnos a partir del id de calificacion
	public function alumnoCursoCalificacion($idCalificacion)
	{
		$sql = "SELECT * from alumnos A, matricula M, calificaciones C, curso CU
		where C.idcalificaciones = ".$idCalificacion."
		and C.idmatricula = M.idmatricula
		and M.idalumnos = A.idalumnos
		and M.idcurso = CU.idcurso";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Eliminamos una autoevaluacion de un modulo, desasignamos
	public function eliminarAsignacionAutoevaluacion($idAsignar)
	{
		$sql="DELETE from test where idtest = ".$idAsignar;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos una asignacion
	public function obtenerAsignacion($idAsignacion)
	{
		$sql = "SELECT * from test where idtest = ".$idAsignacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/*******************************************************************************************************************************************
	* TIPO AUTOEVAL
	*******************************************************************************************************************************************/

	public function getTipoAutoevaluaciones()
	{
		$sql = "SELECT * FROM tipo_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/*******************************************************************************************************************************************
	* PREGUNTAS PLANTILLA
	*******************************************************************************************************************************************/

	//Buscar pregunta de una atuevluacion por el enunciado
	public function getPreguntaPlatillaAutoevaluacionEnunciado($enunciado, $idAutoevaluacion)
	{
		$sql = "SELECT * from plantilla_preguntas where PTpregunta = '" . $enunciado . "' and PTidautoeval = " . $idAutoevaluacion;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscar pregunta de una atuevluacion por el id de la prugunta
	public function getPreguntaPlatillaAutoevaluacionId($idPregunta, $idAutoevaluacion)
	{
		$sql = "SELECT * from plantilla_preguntas where PTidpreguntas = '" . $idPregunta . "' and PTidautoeval = " . $idAutoevaluacion;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscar pregunta de una atuevluacion por el id de la prugunta
	public function getPreguntaPlatillaId($idPregunta)
	{
		$sql = "SELECT * FROM plantilla_preguntas where PTidpreguntas = '" . $idPregunta . "'";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// insertamos un pregunta para la tabla plantilla
	public function insertPreguntaPlantillaAutoevaluacion($enunciado, $idAutoevaluacion)
	{
		$sql = "INSERT into plantilla_preguntas (PTpregunta, PTidautoeval) VALUES ('" . $enunciado . "', '" . $idAutoevaluacion . "')";

		$resultado = $this->consultaSql($sql);

		return  $this->obtenerUltimoIdInsertado();
	}

	//Acutalizar pregunta
	public function updatePreguntaPlantillaId($idPregunta, $enunciado)
	{
		$sql = "UPDATE plantilla_preguntas SET " .
		"PTpregunta = '" . $enunciado . "' " .
		" WHERE PTidpreguntas = " . $idPregunta;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/*******************************************************************************************************************************************
	* RESPUESTAS PLANTILLA
	********************************************************************************************************************************************/

	// insertamos las respuestas para una pregunta
	public function insertRespuestaPlantillaAutoevaluacion($respuesta, $correcta, $idPregunta)
	{
		$sql = "INSERT INTO plantilla_respuesta (PTrespuesta, PTcorrecta, PTidpreguntas) VALUES ('" . $respuesta . "', '" . $correcta . "', '" . $idPregunta ."')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/*******************************************************************************************************************************************
	* AUTOEVALUACIONES PLANTILLAS ASIGNADAS
	********************************************************************************************************************************************/

	//Buscamos las autoevaluaciones asignadas por ID
	public function autevaluacionesModuloAsignadasPorId($idAutoevaluacionModulo)
	{
		$sql = "SELECT pa.*, pam.PTidautoeval_modulo, pam.PTidmodulo FROM plantilla_autoeval_modulo as pam " .
		"LEFT JOIN plantilla_autoeval AS pa ON pa.PTidautoeval = pam.PTidautoeval " .
		"WHERE PTidautoeval_modulo = " . $idAutoevaluacionModulo;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function autoevaluacionIDAutoevalIdModulo($idAutoevaluacionModulo, $idModulo)
	{
		$sql = "SELECT pa.*, pam.PTidautoeval_modulo, pam.PTidmodulo FROM plantilla_autoeval_modulo as pam " .
		"LEFT JOIN plantilla_autoeval AS pa ON pa.PTidautoeval = pam.PTidautoeval " .
		"WHERE pa.PTidautoeval = " . $idAutoevaluacionModulo . " AND pam.PTidmodulo = " . $idModulo;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos las autoevaluaciones asignadas al modulo
	public function autevaluacionesModuloAsignadas($idModulo)
	{
		$sql = "SELECT pa.*, pam.PTidautoeval_modulo, pam.PTidmodulo FROM plantilla_autoeval_modulo as pam " .
		"LEFT JOIN plantilla_autoeval AS pa ON pa.PTidautoeval = pam.PTidautoeval " .
		"WHERE PTidmodulo = " . $idModulo;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos las autoevaluaciones asignadas al modulo
	public function asignarAutoevaluacionModulo($idModulo, $idAutoevaluacion)
	{
		$sql = "INSERT INTO plantilla_autoeval_modulo (PTidmodulo, PTidautoeval) VALUES ('" . $idModulo . "', '" . $idAutoevaluacion . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos las autoevaluaciones asignadas al modulo
	public function quitarAutoevaluacionModulo($idModulo, $idAutoevaluacion)
	{
		$sql = "DELETE from plantilla_autoeval_modulo WHERE PTidmodulo = " . $idModulo . " AND PTidautoeval = " . $idAutoevaluacion;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
}