<?php 

$mi_curso = new Cursos();
$mi_modulo = new Modulos();

if(isset($_POST['idmodulo']))
{
	$idmodulo = $_POST['idmodulo'];
	for($i=0;$i<=count($idmodulo)-1;$i++)
	{
		$idmodulo[$i] = trim($idmodulo[$i]);
	}
	
}

if(isset ($_POST['prioridad']))	$prioridad = $_POST['prioridad'];
if(isset ($_POST['idcurso']))	$idcurso = $_POST['idcurso'];

if(!isset ($idcurso)) $idcurso = 4;

$registros = $mi_curso->all_cursos();
$registros2 = $mi_modulo->ver_modulos();

// Asignamos los modulos al curso elejido
if(!empty($idmodulo)){$mi_modulo->asignar_modulo($idmodulo,$idcurso);}

// Damos prioridad a los modulos para su ordenacion
if(!empty($idtemario)){$mi_modulo->priorizar($idtemario,$prioridad);} 

// Si existe el curso lo buscamos
if(isset($idcurso)){
	$mi_curso->set_curso($idcurso);
	$f = mysqli_fetch_assoc($mi_curso->buscar_curso());
	$f_inicio = Fecha::invertir_fecha($f['f_inicio']);
	$f_fin = Fecha::invertir_fecha($f['f_fin']);
	$f_matriculacion = Fecha::invertir_fecha($f['fin_matriculacion']);
	$i_matriculacion = Fecha::invertir_fecha($f['inicio_matriculacion']);
}



// Insertamos curso
if (!empty ($titulo)){
	$f_inicio = $mi_general->invertir_fecha($f_inicio);
	$f_fin = $mi_general->invertir_fecha($f_fin);
	$i_matriculacion = $mi_general->invertir_fecha($i_matriculacion);
	$f_matriculacion = $mi_general->invertir_fecha($f_matriculacion);
	$mi_curso->insertar_curso($titulo,$descripcion,$metodologia,$n_horas,$f_inicio,$f_fin,$i_matriculacion,$f_matriculacion,$idtipo_curso);
	$mi_curso->insertar_tema_curso();
}

// Actualizamos el curso
if (!empty ($atitulo)){
	$af_inicio = $mi_general->invertir_fecha($af_inicio);
	$af_fin = $mi_general->invertir_fecha($af_fin);
	$ai_matriculacion = $mi_general->invertir_fecha($ai_matriculacion);
	$af_matriculacion = $mi_general->invertir_fecha($af_matriculacion);
	$mi_curso->actualizar_curso($atitulo,$ametodologia,$an_horas,$af_inicio,$af_fin,$ai_matriculacion,$af_matriculacion,$adescripcion,$idtipo_curso,$idcurso);
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'contenidos');	