<?php

$mi_modulo = new Modulos();

//$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($get['idarch'])) $idarch = $get['idarch'];

if(isset($idarch) && is_numeric($idarch))
{

	$mi_modulo->set_archivo($idarch);
		
	$resultado = $mi_modulo->buscar_archivo();
	
	if($mi_modulo->eliminar_archivoPlantilla($resultado))
	{
		Alerta::guardarMensajeInfo('El archivo se ha eliminado correctamente');
		Url::redirect('contenidos/trabajos_practicos');
	}
	else
	{
		Alerta::guardarMensajeInfo('Error elimando un archivo');
		Url::redirect('admin/contenidos/trabajos_practicos');
	}
}