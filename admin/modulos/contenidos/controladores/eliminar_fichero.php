<?php

$mi_modulo = new Modulos();

//$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($get['idmodulo'])) $idmodulo = $get['idmodulo'];
if(isset ($get['idarch'])) $idarch = $get['idarch'];

if(isset($idmodulo, $idarch) && is_numeric($idmodulo) && is_numeric($idarch))
{
	$mi_modulo->set_modulo($idmodulo);
	$mi_modulo->set_archivo($idarch);
		
	$resultado = $mi_modulo->buscar_archivo();
	
	if($mi_modulo->eliminar_archivoPlantilla($resultado))
	{
		Alerta::guardarMensajeInfo('El archivo se ha eliminado correctamente');
		Url::redirect('contenidos/ficheros');
	}
}
