<?php

$mi_modulo = new Modulos();

$get = Peticion::obtenerGet();

if(isset($get['f']))
{
	if($get['f'] == 'listar')
	{
		$registros = $mi_modulo->ver_modulos();
		$loadView = 'modulos_listado';	
	}
	else if($get['f'] == 'buscar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			$mi_modulo->set_modulo($get['idModulo']);

			//Obtengo los datos del modulo
			$f = mysqli_fetch_assoc($mi_modulo->buscar_modulo($get['idModulo']));
	
			mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
			$objAutoevaluacionAdmin = new AutoevaluacionAdmin();

			if(isset($post['actionAutoeval']) && $post['actionAutoeval'] == 'more')
			{
				if(isset($post['idAutoevaluacionAsignar']) && is_array($post['idAutoevaluacionAsignar']))
				{
					foreach($post['idAutoevaluacionAsignar'] as $idAutoevaluacion)
					{
						$objAutoevaluacionAdmin->asignarAutoevaluacionModulo($get['idModulo'], $idAutoevaluacion);
					}
				}
			}
			else if(isset($post['actionAutoeval']) && $post['actionAutoeval'] == 'less')
			{
				if(isset($post['idAutoevaluacionQuitar']) && is_array($post['idAutoevaluacionQuitar']))
				{
					foreach($post['idAutoevaluacionQuitar'] as $idAutoevaluacion)
					{
						$objAutoevaluacionAdmin->quitarAutoevaluacionModulo($get['idModulo'], $idAutoevaluacion);
					}						
				}
			}	

			//Obtengo el temario del modulo	
			$mi_modulo->set_categoria_archivo('1');
			$archivosTemario = $mi_modulo->archivos_moduloPlantilla();

			//Obtengo el contenido SCORM del modulo
			mvc::cargarModuloSoporte('scorm');
			$objModeloScorm = new ModeloScorm();
			$scorms = $objModeloScorm->obtenerScorms($get['idModulo']);
			
			//Obtengo las tematicas de autoevaluacion
			$tematicasAutoevaluaciones = $objAutoevaluacionAdmin->tematicas();	

			$idTematica = '';
			$tituloAutoevaluacion = '';

			if(!empty($post['idTematica']))
			{
				$idTematica = $post['idTematica'];	
			}
			if(!empty($post['tituloAutoevaluacion']))
			{
				$tituloAutoevaluacion = $post['tituloAutoevaluacion'];
			}

			$autoevaluacionesBuscadas = $objAutoevaluacionAdmin->buscadorPlantillaAutoevaluacion($idTematica, $tituloAutoevaluacion);	
			$autoevaluacionesAsignar = array();
			while($autoevaluacionesBuscada = $autoevaluacionesBuscadas->fetch_object())
			{
				$autoevaluacionesAsignar[$autoevaluacionesBuscada->PTidautoeval] = $autoevaluacionesBuscada->PTtitulo_autoeval;
			}

			//Obtengo las autoevaluaciones asignadas
			$autoevaluacionesModulo = $objAutoevaluacionAdmin->autevaluacionesModuloAsignadas($get['idModulo']);	
			$autoevaluacionesAsignadas = array();
			while($autoevaluacionesAsignada = $autoevaluacionesModulo->fetch_object())
			{
				$autoevaluacionesAsignadas[$autoevaluacionesAsignada->PTidautoeval] = $autoevaluacionesAsignada->PTtitulo_autoeval;
			}

			$autoevaluaciones = array_diff_key($autoevaluacionesAsignar, $autoevaluacionesAsignadas);

			//Obtengo los trabajos practicos del modulo
			$mi_modulo->set_categoria_archivo('2');
			$archivosTrabajosPracticos = $mi_modulo->archivos_moduloNoBorradosPlantilla();	
			
			//Obtengo los foros del modulo
			mvc::cargarModuloSoporte('foro');
			$objModelo = new AModeloForo();
			$temas = $objModelo->obtenerTemasPorModulo($get['idModulo']);
			
			$loadView = 'modulos_actualizar';
		}
	}
	else if($get['f'] == 'insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
							
			if (!empty ($post['titulo']) and ($post['nmodulo']))
			{
				if($mi_modulo->insertar_modulo($post['titulo'],$post['nmodulo'],$post['descripcion'],$post['contenidos'],$post['objetivos']))
				{
					Alerta::guardarMensajeInfo('M&oacute;dulo creado');
					Url::redirect('contenidos');
				}
			}
		}

		$loadView = 'modulos_nuevo';
	}
	else if($get['f'] == 'editar')
	{
		if(isset($get['idModulo']) && $get['idModulo'])
		{
			$mi_modulo->set_modulo($get['idModulo']);
			
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();
					
				if(!empty ($post['atitulo']))
				{
					if($mi_modulo->actualizar_modulo($post['atitulo'],$post['anmodulo'],$post['adescripcion'],$post['acontenidos'],$post['aobjetivos']))
					{
						Alerta::guardarMensajeInfo('M&oacute;dulo actualizado');
					}		
				}
			}
			
			//Obtengo los datos del modulo
			$f = mysqli_fetch_assoc($mi_modulo->buscar_modulo($get['idModulo']));
			
			//Obtengo el temario del modulo	
			$mi_modulo->set_categoria_archivo('1');
			$archivosTemario = $mi_modulo->archivos_moduloPlantilla();

			//Obtengo el contenido SCORM del modulo
			mvc::cargarModuloSoporte('scorm');
			$objModeloScorm = new ModeloScorm();
			$scorms = $objModeloScorm->obtenerScorms($get['idModulo']);
			
			//Obtengo las tematicas de autoevaluacion
			mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
			$objAutoevaluacionAdmin = new AutoevaluacionAdmin();
			$tematicasAutoevaluaciones = $objAutoevaluacionAdmin->tematicas();	

			//Obtengo las tematicas de autoevaluacion
			mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
			$objAutoevaluacionAdmin = new AutoevaluacionAdmin();
			$tematicasAutoevaluaciones = $objAutoevaluacionAdmin->tematicas();	

			$autoevaluacionesBuscadas = $objAutoevaluacionAdmin->plantillasAutoeval();	
			$autoevaluacionesAsignar = array();
			while($autoevaluacionesBuscada = $autoevaluacionesBuscadas->fetch_object())
			{
				$autoevaluacionesAsignar[$autoevaluacionesBuscada->PTidautoeval] = $autoevaluacionesBuscada->PTtitulo_autoeval;
			}

			//Obtengo las autoevaluaciones asignadas
			$autoevaluacionesModulo = $objAutoevaluacionAdmin->autevaluacionesModuloAsignadas($get['idModulo']);	
			$autoevaluacionesAsignadas = array();
			while($autoevaluacionesAsignada = $autoevaluacionesModulo->fetch_object())
			{
				$autoevaluacionesAsignadas[$autoevaluacionesAsignada->PTidautoeval] = $autoevaluacionesAsignada->PTtitulo_autoeval;
			}
					
			$autoevaluaciones = array_diff_key($autoevaluacionesAsignar, $autoevaluacionesAsignadas);	

			//Obtengo los trabajos practicos del modulo
			$mi_modulo->set_categoria_archivo('2');
			$archivosTrabajosPracticos = $mi_modulo->archivos_moduloNoBorradosPlantilla();	
			
			//Obtengo los foros del modulo
			mvc::cargarModuloSoporte('foro');
			$objModelo = new AModeloForo();
			
			//$temas = $objModelo->obtenerTemasPorModulo($get['idModulo']);
			$temas = $objModelo->obtenerTemasPorModuloPlantilla($get['idModulo']);
			
			$loadView = 'modulos_actualizar';
		}		
	}
	else if($get['f'] == 'eliminar')
	{
		if(isset($get['idModulo']) and is_numeric($get['idModulo']))
		{			
			if($mi_modulo->eliminar_modulo($get['idModulo']))
			{
				Alerta::guardarMensajeInfo('M&oacute;dulo eliminado');
			}
		}
		
		Url::redirect('contenidos');	
	}
}

//Cargo la vista		
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'contenidos');	