<?php 

$mi_curso = new Cursos();
$mi_modulo = new Modulos();
$mi_af = new AccionFormativa();

if(isset ($_GET['idmodulo']) and ($_GET['id']))
{
	$idmodulo = $_GET['idmodulo'];
	$id = $_GET['id'];
	
	if(is_numeric($id) and ($idmodulo))
	{
		$id = $mi_af->eliminar_modulo_asignado($id,$idmodulo);
	}
	Alerta::guardarMensajeInfo('El m&oacutedulo ha sido eliminado de la acci&oacuten formativa');	
	Url::redirect('contenidos/acciones_formativas/'.$id);
}

if(isset ($_GET['id']) && is_numeric($_GET['id']) && !isset ($_GET['idmodulo']))
{
	$idaccion_formativa = $_GET['id'];
	$mi_af->eliminar_accion_formativa($idaccion_formativa);
	Alerta::guardarMensajeInfo('La acci&oacute;n formativa ha sido eliminada');
}
Url::redirect('contenidos/acciones_formativas/actualizar');
