<?php

$mi_modulo = new Modulos();

$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(!empty($post['titulo']))
		{
			if(isset($post['idModulo']) && is_numeric($post['idModulo']))
			{
				$mi_modulo->set_modulo($post['idModulo']);
				
				if(!empty($_FILES['archivo']['tmp_name']))
				{
					//$mi_modulo->ultimo_registro();
					if($mi_modulo->guardar_fichero($post['titulo'],$post['descripcion'],$post['prioridad'],2))
					{
						Url::redirect('contenidos/editar/' . $post['idModulo'] . '/tp');
						Alerta::mostrarMensajeInfo('archivoadjuntado','Se ha adjuntado un nuevo archivo al m&oacute;dulo correspondiente');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('No existe ningun archivo');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El m&oacute;dulo es obligatorio');
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('El t&iacute;tulo es obligatorio');
		}	
	}	
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	/*
	if(isset($get['idTemario']) && is_numeric($get['idTemario']))
	{
		$archivoTemario = $mi_modulo->getTemerioFilePlantillaById($get['idTemario']);
		if($archivoTemario->num_rows == 1)
		{
			$tema = mysqli_fetch_object($archivoTemario);
			
			var_dump($tema);

			//Eliminar archivo del temario
			if(file_exists(PATH_ROOT . $tema->enlace_archivo))
			{
				chmod(PATH_ROOT . $tema->enlace_archivo, 0777);
				unlink(PATH_ROOT . $tema->enlace_archivo);					
			}

			//elimino el registro de la base de datos
			if($mi_modulo->deleteTemarioFilePlantilla($get['idTemario']))
			{
				Alerta::guardarMensajeInfo('El archivo se ha eliminado correctamente');
				Url::redirect('contenidos/editar/' . $tema->idmodulo . '/temario');			
			}
		}
	}
	*/
}