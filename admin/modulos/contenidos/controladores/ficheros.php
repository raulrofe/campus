<?php

$mi_modulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

//print_r($post);

// Establecemos el id modulo para el post
if(isset ($post['idmodulo']) && is_numeric($post['idmodulo'])) 
{
	$idmodulo = $post['idmodulo'];
	$mi_modulo->set_modulo($idmodulo);
}

// Establecemos el id modulo para el get
if(isset ($get['idmodulo']) && is_numeric($get['idmodulo']))
{
	$idmodulo = $get['idmodulo'];
	$mi_modulo->set_modulo($idmodulo);	
}

// Establecemos el valor de la variable boton para el post
if(isset($post['boton']))
{
	$boton = $post['boton'];
}

// Establecemos el valor de la variable boton para el get
if(isset($get['boton']))
{
	$boton = $get['boton'];
}

//Actualizamos el archivo
if(isset($post['idarch']))
{
	$mi_modulo->set_archivo($post['idarch']);
	if(!empty($post['titulo']))
	{
		if($mi_modulo->actualizar_archivoPlantilla($post['titulo'],$post['descripcion'],$post['prioridad'],$idmodulo))
		{
			Alerta::guardarMensajeInfo('Fichero actualizado');
		}		
	}
}

// Insertamos el archivo pdf sin scorm
if(Peticion::isPost())
{
	if(!isset($post['idarch']))
	{
		if(isset($boton) && $boton == 'Insertar')
		{
			if(!empty($post['titulo']))
			{
				if(isset($post['idmodulo']) && $post['idmodulo'] != 0)
				{
					if(!empty($_FILES['archivo']['tmp_name']))
					{
			
							//$mi_modulo->ultimo_registro();
							$mi_modulo->guardar_fichero($post['titulo'],$post['descripcion'],$post['prioridad'],1);
							Alerta::mostrarMensajeInfo('archivoadjuntado','Se ha adjuntado un nuevo archivo al m&oacute;dulo correspondiente');
					}
					else
					{
						Alerta::guardarMensajeInfo('No existe ningun archivo');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('El m&oacute;dulo es obligatorio');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El t&iacute;tulo es obligatorio');
			}
		}
	}	
}

// Buscamo los datos del archivo si existe
if(isset($get['idarch']) && is_numeric($get['idarch']))
{
	// Establecemos el id del archivo
	$mi_modulo->set_archivo($get['idarch']);
	$resultadoArchivos = $mi_modulo->buscar_archivoPlantilla();
	$f = mysqli_fetch_assoc($resultadoArchivos);	
}


//Buscamos los modulos
$registros = $mi_modulo->ver_modulos();
$registros2 = $mi_modulo->ver_modulos();

//Buscamos los archivos de temario
if(isset ($idmodulo))
{
	$mi_modulo->set_categoria_archivo('1');
	$resultado = $mi_modulo->archivos_moduloPlantilla();
	$n_archivos = mysqli_num_rows($resultado);
}

//if(!isset($idcategoria_archivos)) $idcategoria_archivos = 1;

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'contenidos');
