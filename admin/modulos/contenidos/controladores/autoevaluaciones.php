<?php 

mvc::cargarModeloAdicional('contenidos','modeloautoevaluacion');
$objAutoevaluacionAdmin = new AutoevaluacionAdmin();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset($post['idTematica']))
{
	$idTematica = $post['idTematica'];
}
else if(isset($get['idTematica']))
{
	$idTematica = $get['idTematica'];
}
else
{
	$idTematica = 0;
}

// Opciones del controlador para la tematica
if(isset($post['boton']) && $post['boton'] == 'InsertarTematica')
{
	if(isset($post['tematica']))
	{
		if($objAutoevaluacionAdmin->insertarTematica($post['tematica']))
		{
			Alerta::guardarMensajeInfo('Tem&aacute;tica insertada correctamente');
		}
	}
}

else if(isset($post['boton']) && $post['boton'] == 'Actualizar Tematica')
{
	if(isset($post['idtematica']) && is_numeric($post['idtematica']))
	{
		if(isset($post['atematica']))
		{
			if($objAutoevaluacionAdmin->actualizarTematica($post['atematica'], $post['idtematica']))
			{
				Alerta::guardarMensajeInfo('Tem&aacute;tica actualizada correctamente');
			} 
		}
		$datosTematica = $objAutoevaluacionAdmin->buscarTematicas($post['idtematica']);
		$rowTematica = mysqli_fetch_assoc($datosTematica);
	}
}


else if(isset($post['boton']) && $post['boton'] == 'Borrar Tematica')
{
	if(isset($post['idtematica']) && is_numeric($post['idtematica']))
	{
		if($objAutoevaluacionAdmin->eliminarTematica($post['idtematica']))
		{
			Alerta::guardarMensajeInfo('Tem&aacute;tica borrada');
		}
	}
}

else if(isset($post['boton']) && $post['boton'] == 'Insertar')
{
	if(!empty($post['titulo']))
	{
		$fecha_creacion = date("Y-m-d");
		$ultimo_id = $objAutoevaluacionAdmin->insertar_autoevaluacionPlantilla($post['titulo'],$post['descripcion'],$fecha_creacion,$post['idtematica'], $post['idTematica']);
		Alerta::guardarMensajeInfo('La autoevaluaci&oacute;n ha sido creada correctamente');
	}	
}

else if(isset($post['boton']) && $post['boton'] == 'Actualizar')
{
	$tematicas2 = $objAutoevaluacionAdmin->tematicas();
	
	if(isset($post['idAutoevaluacion']) && is_numeric($post['idAutoevaluacion']))
	{	
		// Establecemos el id para la autoevaluacion
		$objAutoevaluacionAdmin->set_autoeval($post['idAutoevaluacion']);
		
		// Cogemos los parametros y actualizamos la autoevaluacion
		if(!empty ($post['titulo']))
		{
			if($objAutoevaluacionAdmin->actualizar_autoevalPlantilla($post['titulo'],$post['idtematica'],$post['descripcion'],$post['idtipo_autoeval']))
			{
				Alerta::guardarMensajeInfo('Autoevaluaci&oacute;n actualizada correctamente');
			}
		}
		
		//Buscamos una autoevaluacion segun id
		$autoevaluacion = $objAutoevaluacionAdmin->buscar_autoevalPlantilla();
		$fila = mysqli_fetch_assoc($autoevaluacion);
		
	}
}

else if(isset($post['boton']) && $post['boton'] == 'Preguntas y respuestas')
{
	
	// Contamos el numero de preguntas que contiene esta autoevaluacion
	if(isset($post['idAutoevaluacion']) && is_numeric($post['idAutoevaluacion']))
	{
		$objAutoevaluacionAdmin->set_autoeval($post['idAutoevaluacion']);
		$preguntas = $objAutoevaluacionAdmin->preguntasPlantilla();
		$numero_preguntas = mysqli_num_rows($preguntas);
	
		//Insertamos las respuestas
		if(!empty ($post['respuesta']))
		{
			$respuesta = $post['respuesta'];
			if(isset($post['correcta']))
			{
				for($i=0;$i<=count($respuesta)-1;$i++){$respuesta[$i] = trim($respuesta[$i]);}
				
				$max = $objAutoevaluacionAdmin->buscar_ultima_preguntaPlantilla();
				$objAutoevaluacionAdmin->insertar_respuestaPlantilla($respuesta,$post['correcta'],$max);
				Alerta::guardarMensajeInfo('Ha creado una nueva pregunta con respuestas');
				//Url::redirect('autoevaluaciones/preguntas/'.$idautoevaluacion);
			}
			else
			{
				Alerta::mostrarMensajeInfo('respuestacorreca','Debes marcar una respuesta como correcta');
			}
		}
		
		//Insertamos pregunta
		if((!empty ($post['respuesta']) && !isset ($post['correcta'])) || !isset($post['respuesta']))
		{	
			
		
			// Si no esta vacio el enunciado de la pregunta la insertamos en caso contrario mostramos la vista autoevaluaciones
			if(!empty($post['enunciado']) && !isset($post['correcta']) && !isset($post['respuesta']))
			{
				if(is_numeric($post['n_respuestas']) && $post['n_respuestas'] > 1)
				{
					$objAutoevaluacionAdmin->insertar_preguntaPlantilla($post['enunciado']);
					//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'crearRespuestas');
				}
				else
				{
					Alerta::mostrarMensajeInfo('mayor1','El numero de respuestas debe ser mayor a 1');
					//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
				}
			}
		}
	}
	else
	{
		Alerta::guardarMensajeInfo('Debes seleccionar una autoevaluaci&oacute;n');
	}	
}

else if(isset($post['boton']) && $post['boton'] == 'Borrar')
{

	if(isset($post['idAutoevaluacion']) && is_numeric($post['idAutoevaluacion']))
	{
		$objAutoevaluacionAdmin->set_autoeval($post['idAutoevaluacion']);
		$autoevaluacion = $objAutoevaluacionAdmin->buscar_autoevalPlantilla();
			
		if(mysqli_num_rows($autoevaluacion)>0)
		{
			if($_SESSION['perfil'] != 'alumno')
			{
				if($objAutoevaluacionAdmin->eliminar_autoevalPlantilla())
				{
					Alerta::guardarMensajeInfo('Autoevaluaci&oacute;n borrada');
					//Url::redirect('admin/contenidos/autoevaluaciones');
				}
			}
		}
	}
}

else if(isset($post['boton']) && $post['boton'] == 'Plantilla preliminar')
{
	if(isset($post['idAutoevaluacion']) && is_numeric($post['idAutoevaluacion']))
	{
		$objAutoevaluacionAdmin->set_autoeval($post['idAutoevaluacion']);
		$preguntas = $objAutoevaluacionAdmin->preguntasPlantilla();
		$numero_preguntas = mysqli_num_rows($preguntas);
	}	
}

// Opciones del controlador para la autoevaluacion
if(isset($get['boton']) && $get['boton'] == 'Insertar')
{
	$tematicas3 = $objAutoevaluacionAdmin->tematicas();
	$tipos = $objAutoevaluacionAdmin->tematica_autoevaluacion();
}

else if(isset($get['boton']) && $get['boton'] == 'editarPregunta')
{
	$confirmacion='';
	// Insertamos una nueva respuesta
	if(isset($post['idPregunta']) && is_numeric($post['idPregunta']))
	{
		$objAutoevaluacionAdmin->insertar_respuestaPlantilla($post['respuesta'],$post['correcta'],$post['idPregunta']);
	}
	
	// Actualizamos la pregunta
	if(isset ($post['idrespuesta']))
	{
			$respuesta = $post['respuesta'];
			$correcta = $post['correcta'];
			$confirmacion.= $objAutoevaluacionAdmin->actualizar_respuestasPlantilla($respuesta,$correcta,$post['idrespuesta']);
	}
	
	// elimina una imagen de un respuesta de la autoevaluacion
	else if(isset($get['idrespuesta'], $get['option']) && $get['option'] == 'eliminar_imagen' && is_numeric($get['idrespuesta']))
	{
		$rowRespuesta = $objAutoevaluacionAdmin->buscar_una_respuestaPlantilla($get['idrespuesta']);
		if($rowRespuesta->num_rows == 1)
		{
			$rowRespuesta = $rowRespuesta->fetch_array();
			
			$filename = PATH_ROOT . $rowRespuesta['PTimagen_respuesta'];
			if(!empty($rowRespuesta['PTimagen_respuesta']) && file_exists($filename))
			{
				if($objAutoevaluacionAdmin->eliminarImagenRespuestaPlantilla($rowRespuesta['PTidrespuesta']))
				{
					chmod($filename, 0777);
					unlink($filename);
				}
			}
		}
		$confirmacion.= 'Imagen eliminada';
		
		//Url::redirect('autoevaluaciones', true);
	}
	
	// Actualizamos la pregunta
	if(!empty ($post['enunciado']))
	{
		$enunciado = trim($post['enunciado']);
		$confirmacion.= $objAutoevaluacionAdmin->actualizar_preguntaPlantilla($enunciado,$get['idpregunta']);
		Alerta::mostrarMensajeInfo($confirmacion);
	}
	
	// Buscamos la preguntas con sus repuestas para mostrarla
	if(isset($get['idpregunta']) && is_numeric($get['idpregunta']))
	{
		$registros_preguntas = $objAutoevaluacionAdmin->buscar_preguntaPlantilla($get['idpregunta']);
		$pre = mysqli_fetch_assoc($registros_preguntas);
		$registros_respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($get['idpregunta']);
	}
}

else if(isset($get['boton']) && $get['boton'] == 'eliminarPregunta')
{
	if(isset($get['idpregunta']) && is_numeric($get['idpregunta']))
	{
		$pregunta = $objAutoevaluacionAdmin->buscar_preguntaPlantilla($get['idpregunta']);
		$lasRespuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($get['idpregunta']);
		if(mysqli_num_rows($pregunta)>0)
		{
			$f = $objAutoevaluacionAdmin->idautoevaluacionPreguntaPlantilla($get['idpregunta']);
			$idautoevaluacion = $f['PTidautoeval'];
			if($objAutoevaluacionAdmin->borrar_preguntaPlantilla($get['idpregunta']))
			{
				while($idrespuesta = mysqli_fetch_assoc($lasRespuestas))
				{
					echo $idrespuesta['PTidrespuesta'];
					$objAutoevaluacionAdmin->borrar_respuestasPlantilla($idrespuesta['PTidrespuesta']);	
				}
				Alerta::guardarMensajeInfo('Pregunta borrada');
			}	
			//Url::redirect('autoevaluaciones/'.$idautoevaluacion);
		}
	}
}

//Busco todas las tematicas
$tematicas = $objAutoevaluacionAdmin->tematicas();
$tematicas2 = $objAutoevaluacionAdmin->tematicas();

//Busco las autoevaluaciones que pertenecen a una automatica
$autoevaluaciones = $objAutoevaluacionAdmin->autoevalActivasTematicaPlantilla($idTematica);


require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_autoevaluaciones'));
