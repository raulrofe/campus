<?php

//cargo el controlador de agenda
mvc::cargarModuloSoporte('agenda');
$objAgenda = new AModeloAgenda();
$resultado_recomendacion = $objAgenda->obtenerListadoRecomendaciones();

$objConvocatoria = new Convocatoria();

$post = Peticion::obtenerPost();

if(isset($post['idconvocatorias']) and is_numeric($post['idconvocatorias']))
{
	if(!empty($post['titulo']) and ($post['metodologia']) and ($post['n_horas']) and ($post['f_inicio']) and ($post['f_fin']))
	{
		$objConvocatoria->actualizarConvocatoria($post);
	}
	
	$la_convocatoria = $objConvocatoria->buscar_convocatoria($post['idconvocatorias']);
	$fechai = Fecha::invertir_fecha($la_convocatoria['f_inicio'],'-','/');
	$fechaf = Fecha::invertir_fecha($la_convocatoria['f_fin'],'-','/');
	$i_mat = Fecha::invertir_fecha($la_convocatoria['inicio_matriculacion'],'-','/');
	$f_mat = Fecha::invertir_fecha($la_convocatoria['fin_matriculacion'],'-','/');
}

$resultado = $objConvocatoria->convocatorias();

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_convocatoria'));