<?php

$mi_modulo = new Modulos();
$get = Peticion::obtenerGet();

if(isset ($get['idmodulo']) and is_numeric($get['idmodulo']))
{
	$idmodulo = $get['idmodulo']; 
	if($mi_modulo->eliminar_modulo($idmodulo))
	{
		Alerta::guardarMensajeInfo('M&oacute;dulo eliminado');
	}
}

Url::redirect('contenidos');
