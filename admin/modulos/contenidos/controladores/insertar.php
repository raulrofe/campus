<?php

$mi_modulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($post['idmodulo']) && is_numeric($post['idmodulo'])) 
{
	$idmodulo = $post['idmodulo'];
	$mi_modulo->set_modulo($idmodulo);
	
	if(isset ($post['boton']))
	{
		if($post['boton'] == 'Actualizar')
		{
			$mi_modulo->set_modulo($idmodulo);
			$f = mysqli_fetch_assoc($mi_modulo->buscar_modulo($idmodulo));
		}
		if($post['boton'] == 'Borrar')
		{
			Alerta::alertConfirm('borrarmodulo', '¿Estas seguro que desea borrar el módulo?','contenidos/eliminar/'.$idmodulo);
		}
	}
}


// Insertamos modulo o actualizamos el modulo
if(Peticion::isPost())
{	
	if(!empty ($post['atitulo']))
	{
		if($mi_modulo->actualizar_modulo($post['atitulo'],$post['anmodulo'],$post['adescripcion'],$post['acontenidos'],$post['aobjetivos']))
		{
			Alerta::guardarMensajeInfo('M&oacute;dulo actualizado');
		}		
	}
	
	if (!empty ($post['titulo']) and ($post['nmodulo']))
	{
		if($mi_modulo->insertar_modulo($post['titulo'],$post['nmodulo'],$post['descripcion'],$post['contenidos'],$post['objetivos']))
		{
			Alerta::guardarMensajeInfo('M&oacutedulo creado');
		}
	}
}

//obtengo los modulos para el desplegable select
$registros = $mi_modulo->ver_modulos();
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'contenidos');
