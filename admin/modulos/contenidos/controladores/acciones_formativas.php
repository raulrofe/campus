<?php 
$mi_curso = new Cursos();
$mi_modulo = new Modulos();
$mi_af = new AccionFormativa();

$get = Peticion::obtenerGet();

if(isset($get['f']))
{
	if($get['f'] == 'listar')
	{
		//Buscamos todas las acciones formativas
		$resultado = $mi_af->af();
		$loadView = 'acciones_formativas';	
	}
	else if($get['f'] == 'actualizar')
	{
		if(isset($get['idAccionFormativa']) && is_numeric($get['idAccionFormativa']))
		{
			$accionFormativa = $mi_af->buscar_af($get['idAccionFormativa']);
			$accionFormativa = mysqli_fetch_object($accionFormativa);
			
			//var_dump($accionFormativa);
			
			$registros2 = $mi_modulo->ver_modulos();
			$loadView = 'acciones_formativas_actualizar';
		}
	}
	else if($get['f'] == 'insertar')
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			if(!empty($post['nombreAF']))
			{
				if(isset($post['idmodulo']))
				{
					$id = $mi_af->crear_af($post['nombreAF'], $post['codigoAF'], $post['idmodulo']);
					$registros = $mi_af->buscar_af($id);
				}
				else
				{
					Alerta::guardarMensajeInfo('Debes seleccionar un m&oacute;dulo');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El c&oacute;digo de la acci&oacute;n formativa es obligatorio');
			}
		}	
			
		$registros2 = $mi_modulo->ver_modulos();
		$loadView = 'acciones_formativas_insertar';		
	}
}


// Establecemos las variables
if(isset($post['idmodulo']))
{
	$idmodulo = $post['idmodulo'];
}
if(isset($post['idAccionF']) && is_numeric($post['idAccionF'])) $idAccionF = $post['idAccionF'];
if(isset($get['idAccionF']) && is_numeric($get['idAccionF'])) $idAccionF = $get['idAccionF'];
if(isset($get['boton'])) $boton = $get['boton'];
if(isset($post['boton'])) $boton = $post['boton'];

if(isset($boton) && $boton == 'Borrar')
{
	if(isset ($idAccionF) && is_numeric($idAccionF))
	{
		if($mi_af->eliminar_accion_formativa($idAccionF))
		{
			Alerta::guardarMensajeInfo('Acci&oacute;n formativa eliminada');
		}
	}
	Url::redirect('contenidos/acciones_formativas/actualizar');
}



if(isset($boton) && $boton == 'Actualizar')
{
	// Actualizarmos la accion formativa con sus modulos
	if(!empty($idmodulo))
	{
		$id = $mi_af->actualizar_af($idAccionF, $post['nombreAF'], $post['costeAF'], $idmodulo);
	}

	// Buscamos la accion formativa segun id pasado com parametro
	if(isset($idAccionF) && is_numeric($idAccionF))
	{
		$registros = $mi_af->buscar_af($idAccionF);
	}
}

//$registros = $mi_curso->all_cursos();



require_once mvc::obtenerRutaVista(dirname(__FILE__), 'af');	
