<?php

mvc::cargarModuloSoporte('cursos');
$objCursos = new Convocatoria();
$convocatorias = $objCursos->convocatorias();

$htmlLog = null;

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	$log = array();
	$filaCsv = 1;
	
	// si se ha subido un archivo del modo correcto ...
	if(isset($_FILES['archivo']['tmp_name']) && !empty($_FILES['archivo']['tmp_name']))
	{
		// comprobamos la extension del archivo (debe ser CSV)
		if(preg_match('#(.+)\.csv$#i', $_FILES['archivo']['name']))
		{
			// abre el archivo CSV
			if(($handle = fopen($_FILES['archivo']['tmp_name'], "r")) !== false)
			{
				$objAF = new AccionFormativa();
				mvc::cargarModuloSoporte('centros');
				$objCentro = new AModeloCentros();
				
				// para evitar procesar la cabecera del CSV
				$cabecera = true;
				
				// contadores de alumnos
				$contSuccess = 0;
				$contTotal = 0;
				
				// se mueve por cada registro del CSV
			    while(($data = fgetcsv($handle, 0, ";")) !== false)
			    {
			    	// si no es la cabecera del archivo, es decir, no es la primera linea
			    	if(!$cabecera)
			    	{
			    		// volcamos los datos del CSV para trabajarlos mas facilmente
				       $cif = $data[7];
				       $razonSocial = utf8_encode($data[9]);
				       $accionFormativa = $data[5];		
				       $modulos = $data[4];	
				       $informeRLT = $data[10];
				       
				       if(!empty($cif) && !empty($accionFormativa) && !empty($modulos))
				       {
				       	
				       	$idModulos = array();
				   			
				   			// insertamos el centro si no existe
				   			$rowAF = $objAF->buscar_afCodigo($accionFormativa);

				   			if(mysqli_num_rows($rowAF) == 0)
				   			{
				   				$refModulo = explode('.',$modulos);
				   				foreach($refModulo as $idmodulo)
				   				{
				   					$idModulos[] = $idmodulo;
				   				}
				   				
				   				if($objAF->crearAfPorReferencia($accionFormativa,$idModulos))
				   				{
				   					if($informeRLT == 'S')
				   					{
					   					$idAccionFormativa = $objAF->buscar_afCodigo($accionFormativa);
					   					$AF = mysqli_fetch_assoc($idAccionFormativa);
						   				$idAF = $AF['idaccion_formativa'];
					   					$centros = $objCentro->obtenerCentroPorCIF($cif);
					   					
					   					if(mysqli_num_rows($centros) == 0)
					   					{
					   						$objCentro->insertarCentro($cif,$razonSocial);
					   						$idCentro = $objCentro->obtenerUltimoIdInsertado();
					   					}
					   					
					   					else
					   					{
						   					$idCentros = mysqli_fetch_assoc($centros);
						   					$idCentro = $idCentros['idcentros'];
					   					}
					   					
						   					$centrosAsignados = $objAF->obtenerUnCentroAF($idAF, $idCentro, $post['idconv']);
						   					if(mysqli_num_rows($centrosAsignados) == 0)
						   					{
							   					$objAF->asignarUnCentroAF($post['idconv'],$idAF,$idCentro);
						   					}
				   					}
				   				$contSuccess++;
				   				}
				   			}
				   			// sino obtenemos el id del centro
				   			else
				   			{
					   			//$idAccionFormativa = $objAF->obtenerUltimoIdInsertado();
				   				if($informeRLT == 'S')
				   				{
						   			$idAccionFormativa = $objAF->buscar_afCodigo($accionFormativa);
						   			$AF = mysqli_fetch_assoc($idAccionFormativa);
						   			$idAF = $AF['idaccion_formativa'];
						   			$centros = $objCentro->obtenerCentroPorCIF($cif);
						   			
						   			if(mysqli_num_rows($centros) == 0)
						   			{
						   				$objCentro->insertarCentro($cif, utf8_encode($razonSocial));
					   					$idCentro = $objCentro->obtenerUltimoIdInsertado();
						   			}
						   			else
						   			{
							   			$idCentros = mysqli_fetch_assoc($centros);
							   			$idCentro = $idCentros['idcentros'];
						   			}
						   			
				   					$centrosAsignados = $objAF->obtenerUnCentroAF($idAF, $idCentro, $post['idconv']);
				   					if(mysqli_num_rows($centrosAsignados) == 0)
				   					{				   				
						   				$objAF->asignarUnCentroAF($post['idconv'],$idAF,$idCentro);
				   					}
				   				}
				   			}
				       }
				       else
				       {
				       		$log[] = 'El CIF , La ACCION FORMATIVA y el/los MODULOS no pueden estar vacios en la fila ' . $filaCsv;
				       }
			    	}
			    	else
			    	{
			    		$cabecera = false;
			    	}
			    	
			    	$contTotal++;
			    	$filaCsv++;
			    }
			    fclose($handle);
			}
			
			// estadisticas de la carga masiva
			$htmlLog .= '<div>Total de Registros: ' . $contTotal . '</div><div>Total registros insertados: ' . $contSuccess . '</div><div>Total registros no insertados: ' . ($contTotal - $contSuccess) . '</div><br />';
		}
		else
		{
			$log[] = 'El archivo debe ser tipo CSV';
		}
	}
	else
	{
		$log[] = 'Selecciona un archivo CSV para hacer la carga masiva';
	}
	
	// errores durante el proceso de la carga
	if(count($log) > 0)
	{
		foreach($log as $item)
		{
			$htmlLog .= '<div>- ' . $item . '</div>';
		}
	}
}

/*
mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
*/

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'carga_masiva');
