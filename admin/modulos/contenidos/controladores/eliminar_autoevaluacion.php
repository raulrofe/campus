<?php 

	mvc::cargarModeloAdicional('contenidos','modeloautoevaluacion');
	$objAutoevaluacionAdmin = new AutoevaluacionAdmin();
	$get = Peticion::obtenerGet();	

	if(isset($get['idAutoevaluacion']) && is_numeric($get['idAutoevaluacion']))
	{
		$objAutoevaluacionAdmin->set_autoeval($get['idAutoevaluacion']);
		$autoevaluacion = $objAutoevaluacionAdmin->buscar_autoevalPlantilla();
			
		if(mysqli_num_rows($autoevaluacion)>0)
		{
			if($_SESSION['perfil'] != 'alumno')
			{
				if($objAutoevaluacionAdmin->eliminar_autoevalPlantilla())
				{
					Alerta::guardarMensajeInfo('Autoevaluaci&oacute;n borrada');
					Url::redirect('admin/contenidos/autoevaluaciones');
				}
			}
		}
	}
