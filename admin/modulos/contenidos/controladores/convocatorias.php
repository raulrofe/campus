<?php

//cargo el controlador de agenda
mvc::cargarModuloSoporte('agenda');
$objAgenda = new ModeloAgendaAdmin();
$resultado_recomendacion = $objAgenda->obtenerListadoRecomendaciones();

$objConvocatoria = new Convocatoria();
$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(!empty ($post))
{	
	if(isset($post['boton']) && $post['boton'] == 'Insertar')
	{
		//invierto las fechas
		$post['f_inicio'] = Fecha::invertir_fecha($post['f_inicio'],'/','-');
		$post['f_fin'] = Fecha::invertir_fecha($post['f_fin'],'/','-');
		$post['i_matriculacion'] = Fecha::invertir_fecha($post['i_matriculacion'],'/','-');
		$post['f_matriculacion'] = Fecha::invertir_fecha($post['f_matriculacion'],'/','-');
			
			if($post['f_inicio']<$post['f_fin'])
			{
				if($post['f_matriculacion']>$post['i_matriculacion'])
				{
					if($post['i_matriculacion']<$post['f_inicio'] && $post['f_matriculacion']<$post['f_inicio'])
					{
						if(!empty ($post['titulo']) && ($post['n_horas']) && ($post['i_matriculacion']) 
						&& ($post['f_matriculacion']) && ($post['f_inicio']) && ($post['f_fin']))
						{
							$add = $objConvocatoria->insertarConvocatoria($post);
							if($add)
							{
								Alerta::mostrarMensajeInfo('convocatoriacreada','La convocatoria se ha creado correctamente');
							}
						}
						else {Alerta::guardarMensajeInfo('Los campos con (*) son obligatorios');}
					}
					else {Alerta::guardarMensajeInfo('La fecha de incio y fin de matriculaci&oacute;n debe ser menor a la fecha de inicio del curso');}
				}	
				else {Alerta::guardarMensajeInfo('La fecha de incio de matriculaci&oacute;n debe ser menor a la fecha de fin de matriculaci&oacute;n');}
			}
			else {Alerta::guardarMensajeInfo('La fecha de incio del curso debe ser menor a la fecha de fin de curso');}
	}
	if(isset($post['boton']) && $post['boton'] == 'Actualizar')
	{
		if(isset($post['idConvocatoria']) && is_numeric($post['idConvocatoria']))
		{
			$idconvocatorias = $post['idConvocatoria'];
			$la_convocatoria = $objConvocatoria->buscar_convocatoria($post['idConvocatoria']);
			$fechai = Fecha::invertir_fecha($la_convocatoria['f_inicio'], '-', '/');
			$fechaf = Fecha::invertir_fecha($la_convocatoria['f_fin'], '-', '/');
			$i_mat = Fecha::invertir_fecha($la_convocatoria['inicio_matriculacion'], '-', '/');
			$f_mat = Fecha::invertir_fecha($la_convocatoria['fin_matriculacion'], '-', '/');
		}	
		if(isset($post['idconvocatorias']) && is_numeric($post['idconvocatorias']))
		{
			//invierto las fechas
			$post['f_inicio'] = Fecha::invertir_fecha($post['f_inicio'],'/','-');
			$post['f_fin'] = Fecha::invertir_fecha($post['f_fin'],'/','-');
			$post['i_matriculacion'] = Fecha::invertir_fecha($post['i_matriculacion'],'/','-');
			$post['f_matriculacion'] = Fecha::invertir_fecha($post['f_matriculacion'],'/','-');
				
			if($post['f_inicio']<$post['f_fin'])
			{
				if($post['f_matriculacion']>$post['i_matriculacion'])
				{
					if($post['i_matriculacion']<$post['f_inicio'] && $post['f_matriculacion']<$post['f_inicio'])
					{
						if(!empty ($post['titulo']) && ($post['n_horas']) && ($post['i_matriculacion'])
						&& ($post['f_matriculacion']) && ($post['f_inicio']) && ($post['f_fin']))
						{
							$add = $objConvocatoria->actualizarConvocatoria($post);
							if($add)
							{
								Alerta::mostrarMensajeInfo('convocatoriacreada','La convocatoria se ha creado correctamente');
							}
						}
						else {Alerta::guardarMensajeInfo('Los campos con (*) son obligatorios');
						}
					}
					else {Alerta::guardarMensajeInfo('La fecha de incio y fin de matriculaci&oacute;n debe ser menor a la fecha de inicio del curso');
					}
				}
				else {Alerta::guardarMensajeInfo('La fecha de incio de matriculaci&oacute;n debe ser menor a la fecha de fin de matriculaci&oacute;n');
				}
			}
			else {Alerta::guardarMensajeInfo('La fecha de incio del curso debe ser menor a la fecha de fin de curso');
			}
		}			
	}
	if(isset ($post['boton']) && $post['boton'] == 'Borrar')
	{
		$idconvocatorias = $post['idConvocatoria'];
		if($objConvocatoria->eliminarConvocatoria($idconvocatorias))
		{
			Alerta::guardarMensajeInfo('Convocatoria eliminada');
		}
	}
}

//Busco las convocatorias
$resultado = $objConvocatoria->convocatorias();

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_convocatoria'));