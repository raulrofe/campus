<?php 
mvc::cargarModeloAdicional('tematicas');
$objTematicas = new TematicasAdmin();

mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
$objAutoevaluacionAdmin = new AutoevaluacionAdmin();

$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'buscar')
{

	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();

		$autoevluaciones = $objAutoevaluacionAdmin->buscadorPlantillaAutoevaluacion($post['idTematica'], $post['titulo']);
	}

	$tematicas = $objTematicas->tematicas();
	$loadView = 'autoevaluaciones_search';
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();

		$fechaCreacion = date("Y-m-d");
		$tiempoEjecucion = "00:00:00";
		$aleratorias = 0;

		if(!isset($post['tiempoRealizacion']))
		{
			$tiempoEjecucion = $post['horas'] . ":" . $post['minutos'] . ":00";	
		}

		if(isset($post['aleatorias']))
		{
			$aleatorias = 1;
		}

		$objAutoevaluacionAdmin->insertar_autoevaluacionPlantilla($post['titulo'], $post['descripcion'], $fechaCreacion, $post['idTipoAutoevaluacion'], 
								 $post['idTematica'], $post['penalizacionBlanco'], $post['penalizacionNoContesta'], $post['numeroIntentos'], 
								 $post['notaMinima'], $tiempoEjecucion, $post['modoCorreccion'], $aleratorias);

	}
	//Obtengo el tipo de autoevaluaciones
	$tipoAutoeval = $objAutoevaluacionAdmin->getTipoAutoevaluaciones();

	//Obtengo las tematicas de autoevaluaciones
	$tematicas = $objAutoevaluacionAdmin->tematicas();

	$loadView = 'autoevaluaciones_insert';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idAutoevaluacion']) && is_numeric($get['idAutoevaluacion']))
	{
		$autoevaluacion = $objAutoevaluacionAdmin->getAutoevaluacionPlantilla($get['idAutoevaluacion']);

		if($autoevaluacion->num_rows == 1)
		{
			$autoevaluacionRow = $autoevaluacion->fetch_object();

			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				$tiempoEjecucion = "00:00:00";

				$aleatorias = 0;

				if(!isset($post['tiempoRealizacion']))
				{
					$tiempoEjecucion = $post['horas'] . ':' . $post['minutos'] . ':00';
				}

				if(isset($post['aleatorias']))
				{
					$aleatorias = 1;
				}

				if($objAutoevaluacionAdmin->actualizar_autoevalPlantilla($post['titulo'], $post['descripcion'], $post['idTipoAutoevaluacion'], 
										  	$post['idTematica'], $post['penalizacionBlanco'], $post['penalizacionNoContesta'], 
										  	$post['numeroIntentos'], $post['notaMinima'], $tiempoEjecucion, $post['modoCorreccion'], 
										  	$aleatorias, $get['idAutoevaluacion']))
				{
					url::redirect('contenidos/autoevaluaciones');
				}
			}

			//Obtengo el tipo de autoevaluaciones
			$tipoAutoeval = $objAutoevaluacionAdmin->getTipoAutoevaluaciones();

			//Obtengo las tematicas de autoevaluaciones
			$tematicas = $objAutoevaluacionAdmin->tematicas();

			$loadView = 'autoevaluaciones_edit';
		}
	}	
}
else if(isset($get['f']) && $get['f'] == 'preguntar')
{
	if(isset($get['idAutoevaluacion']) && is_numeric($get['idAutoevaluacion']))
	{
		$autoevaluacion = $objAutoevaluacionAdmin->getAutoevaluacionPlantilla($get['idAutoevaluacion']);

		if($autoevaluacion->num_rows == 1)
		{
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if(isset($post['pregunta'], $post['respuesta'], $post['correcta']))
				{
					$idPregunta = null;
					//Busco si existe la pregunta
					$pregunta = $objAutoevaluacionAdmin->getPreguntaPlatillaAutoevaluacionEnunciado($post['pregunta'], $get['idAutoevaluacion']);
					if($pregunta->num_rows == 0)
					{
						$idPregunta = $objAutoevaluacionAdmin->insertPreguntaPlantillaAutoevaluacion($post['pregunta'], $get['idAutoevaluacion']);

						if(is_numeric($idPregunta))
						{
							$contResponse = 1;
							foreach ($post['respuesta'] as $response) 
							{
								if(!empty($response))
								{
									$correcta = 0;
									if($contResponse == $post['correcta'])
									{
										$correcta = 1;
									}

									$objAutoevaluacionAdmin->insertRespuestaPlantillaAutoevaluacion($response, $correcta, $idPregunta);
									$contResponse++;
								}
							}
						}


					}
				}
			}

			//Busco las preguntas con las respuestas
			$autoevaluacion = $autoevaluacion->fetch_object();

			$autoevaluacionContent = array();

			//busco las preguntas
			$preguntas = $objAutoevaluacionAdmin->preguntasPlantilla($get['idAutoevaluacion']);
			$contPregunta = 0;

			if($preguntas->num_rows > 0)
			{
				while($pregunta = $preguntas->fetch_object())
				{
					$autoevaluacionContent[$contPregunta]['idPregunta'] = $pregunta->PTidpreguntas;
					$autoevaluacionContent[$contPregunta]['enunciado'] = $pregunta->PTpregunta;

					//busco las respuestas
					$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($pregunta->PTidpreguntas);
					$contRespuesta = 0;

					if($respuestas->num_rows > 0)
					{
						while($respuesta = $respuestas->fetch_object())
						{
							$autoevaluacionContent[$contPregunta]['respuestas'][$contRespuesta]['respuesta'] = $respuesta->PTrespuesta;
							$autoevaluacionContent[$contPregunta]['respuestas'][$contRespuesta]['correcta'] = $respuesta->PTcorrecta;

							$contRespuesta++;
						}
					}

					$contPregunta++;
				}
			}				

			//Muestro el formulario para crear pregunta
			$loadView = 'autoevaluaciones_pyr';
		}		
	}
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idAutoevaluacion']) && is_numeric($get['idAutoevaluacion']))
	{
		$autoevaluacion = $objAutoevaluacionAdmin->getAutoevaluacionPlantilla($get['idAutoevaluacion']);

		if($autoevaluacion->num_rows == 1)
		{
			$autoevaluacion = $autoevaluacion->fetch_object();

			//busco las preguntas
			$preguntas = $objAutoevaluacionAdmin->preguntasPlantilla($get['idAutoevaluacion']);
			if($preguntas->num_rows > 0)
			{
				while($pregunta = $preguntas->fetch_object())
				{
					//busco las respuestas
					$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($pregunta->PTidpreguntas);
					if($respuestas->num_rows > 0)
					{
						while($respuesta = $respuestas->fetch_object())
						{
							//Elimino las respuestas una a una mediante el bucle
							$objAutoevaluacionAdmin->borrar_respuestasPlantilla($respuesta->PTidrespuesta);
						}
					}

					//Elimino las preguntas una a una mediante el bucle
					$objAutoevaluacionAdmin->borrar_preguntaPlantilla($pregunta->PTidpreguntas);
				}
			}

			//elimino la autoevaluacion y redirijo
			$objAutoevaluacionAdmin->eliminar_autoevalPlantilla($get['idAutoevaluacion']);
			url::redirect('contenidos/autoevaluaciones');
		}
	}
}
else if(isset($get['f']) && $get['f'] == 'editarPregunta')
{
	if(isset($get['idPregunta']) && is_numeric($get['idPregunta']))
	{
				//Busco si existe la pregunta
		$preguntas = $objAutoevaluacionAdmin->getPreguntaPlatillaId($get['idPregunta']);
		if($preguntas->num_rows == 1)
		{
			$pregunta = $preguntas->fetch_object();

			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();

				if($objAutoevaluacionAdmin->updatePreguntaPlantillaId($get['idPregunta'], $post['pregunta']))
				{
					//Busco las respuestas
					$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($get['idPregunta']);
					if($respuestas->num_rows > 0)
					{
						while($respuesta = $respuestas->fetch_object())
						{
							//Elimino las respuestas una a una mediante el bucle
							$objAutoevaluacionAdmin->borrar_respuestasPlantilla($respuesta->PTidrespuesta);
						}
					}

					//Inserto las nuevas respuestas
					$contResponse = 1;
					foreach ($post['respuesta'] as $response) 
					{
						$correcta = 0;
						if($contResponse == $post['correcta'])
						{
							$correcta = 1;
						}

						$objAutoevaluacionAdmin->insertRespuestaPlantillaAutoevaluacion($response, $correcta, $get['idPregunta']);
						$contResponse++;
					}
				}

				url::redirect('contenidos/autoevaluaciones/preguntar/' . $pregunta->PTidautoeval);
			}	

			$autoevaluacion = $objAutoevaluacionAdmin->getAutoevaluacionPlantilla($pregunta->PTidautoeval);

			if($autoevaluacion->num_rows == 1)
			{
				$autoevaluacion = $autoevaluacion->fetch_object();
				$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($get['idPregunta']);
			}
		}

		$loadView = 'autoevaluaciones_pyr_edit';
	}
}
else if(isset($get['f']) && $get['f'] == 'eliminarPregunta')
{
	if(isset($get['idPregunta']) && is_numeric($get['idPregunta']))
	{
		//Busco si existe la pregunta
		$preguntas = $objAutoevaluacionAdmin->getPreguntaPlatillaId($get['idPregunta']);
		if($preguntas->num_rows == 1)
		{
			$pregunta = $preguntas->fetch_object();
				
			//busco las respuestas
			$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($get['idPregunta']);
			if($respuestas->num_rows > 0)
			{
				while($respuesta = $respuestas->fetch_object())
				{
					//Elimino las respuestas una a una mediante el bucle
					$objAutoevaluacionAdmin->borrar_respuestasPlantilla($respuesta->PTidrespuesta);
				}
			}

			//Elimino la pregunta una a una mediante el bucle
			$objAutoevaluacionAdmin->borrar_preguntaPlantilla($get['idPregunta']);	

			url::redirect('contenidos/autoevaluaciones/preguntar/' . $pregunta->PTidautoeval);
		}
	}
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones_general'));							