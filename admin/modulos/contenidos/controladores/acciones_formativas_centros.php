<?php

//cargo los modulos necesarios
$objAF = new AccionFormativa();
mvC::cargarModuloSoporte('centros');
$objCentros = new AModeloCentros();
mvC::cargarModuloSoporte('cursos');
$objCursos = new Convocatoria();

$convocatorias = $objCursos->convocatorias();

$post=Peticion::obtenerPost();
//print_r($post);
if(isset($post['idConvocatoria']) && isset($post['idAF']) && isset($post['centrosRLT']))
{
	if(!empty ($post['idConvocatoria']) && ($post['idAF']) && ($post['centrosRLT']))
	{
		$objAF->asignarCentroAF($post);
	}
}

if(isset ($post['idConvocatoria']) and !empty ($post['idConvocatoria']))
{
	$accionesFormativas = $objAF->af();
	$centros = $objCentros->obtenerCentrosRLTConvocatoria($post['idConvocatoria']);
	$nCentros = mysqli_num_rows($centros);
	$columnaCentros = 5;
	$filaCentros = $nCentros/$columnaCentros;

	while($centro = mysqli_fetch_assoc($centros))
	{
		$regCentro['id'][] = $centro['idcentros'];
		$regCentro['cif'][] = $centro['cif'];
	}
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'af'));