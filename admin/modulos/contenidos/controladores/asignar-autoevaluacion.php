<?php

mvc::cargarModeloAdicional('contenidos','modeloautoevaluacion');
$objAutoevaluacionAdmin = new AutoevaluacionAdmin();
$objModulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

/*
print_r($post);
echo "<br/>---------<br/>";
print_r($get);
*/

if(isset($post['idTematica']))
{
	$idTematica = $post['idTematica'];
}
else if(isset($get['idTematica']))
{
	$idTematica = $get['idTematica'];
}
else
{
	$idTematica = 0;
}

if(isset($get['boton']) && $get['boton'] == 'Insertar' || $get['boton'] == 'Actualizar')
{
	$resultado = $objModulo->ver_modulos();
	$resultado2 = $objModulo->ver_modulos();
	$autoevaluaciones = $objAutoevaluacionAdmin->autoevalActivasTematicaPlantilla($idTematica);
	$tematicas = $objAutoevaluacionAdmin->tematicas();
	if(isset($get['idAsignacion']) && is_numeric($get['idAsignacion']))
	{
		$objModulo->set_modulo($get['idModulo']);
		$datosAsignacion = $objAutoevaluacionAdmin->obtenerAsignacion($get['idAsignacion']);
		$rowAsignacion = mysqli_fetch_assoc($datosAsignacion);
	}
}

if(isset($post['boton']) && $post['boton'] == 'Insertar')
{
	if(isset($post['idAutoevaluacion']))
	{
		$cont = count($post['idAutoevaluacion'])-1;
		for($i=0;$i<=$cont;$i++)
		{
			$objAutoevaluacionAdmin->set_autoeval($post['idAutoevaluacion'][$i]);
			$eval = $objAutoevaluacionAdmin->buscar_autoevalPlantilla();
			$datos = mysqli_fetch_assoc($eval);
			
			// Comprobamos si existe un modulo para la asignacion de la autoevaluacion
			if(isset($post['idmodulo']) && is_numeric($post['idmodulo']) && $post['idmodulo'] != 0)
			{
				//Comprobacion del tiempo para el test
				if($post['horas'] == '00' && $post['minutos'] == '00' && !isset($post['limitTime']))
				{
					Alerta::mostrarMensajeInfo('asignartiempo','Debes seleccionar el tiempo de realizaci&oacute;n');
				}
				else
				{
					//Establecesmos el limite de tiempo
					if(isset($limitTime) && $post['limitTime'] == 0)
					{
						$post['horas'] = '00';
						$post['minutos'] = '00';
					}
					
					$tiempoRealizacion = $post['horas'].":".$post['minutos'];
				
					//copiamos la autoevaluacion de la platilla a la tabla de autoevaluaciones de preguntas y plantillas del curso
					$objAutoevaluacionAdmin->insertar_autoevaluacion($datos['PTtitulo_autoeval'], $datos['PTdescripcion'], 
					$datos['PTf_creacion'], $datos['PTidtipo_autoeval']);
					$idasignacion = $objAutoevaluacionAdmin->obtenerUltimoIdInsertado();
					$preguntas = $objAutoevaluacionAdmin->preguntasPlantilla();
					while($pregunta = mysqli_fetch_assoc($preguntas))
					{
						if($objAutoevaluacionAdmin->copiarPregunta($pregunta['PTpregunta'], $pregunta['PTimagen_pregunta'], $idasignacion))
						{
							$idUltimaPregunta = $objAutoevaluacionAdmin->obtenerUltimoIdInsertado();
							$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($pregunta['PTidpreguntas']);
							while($respuesta = mysqli_fetch_assoc($respuestas))
							{
								$objAutoevaluacionAdmin->copiarRespuesta($respuesta['PTrespuesta'], $respuesta['PTimagen_respuesta'], $respuesta['PTcorrecta'], $idUltimaPregunta);
			
							}
						}
					}

					if($objAutoevaluacionAdmin->configuracionTest($post['penalizacion_blanco'], $post['penalizacion_nc'], $post['n_intentos'], $post['nota_minima'], $post['aleatoria'], $tiempoRealizacion, $post['modoCorreccion'], $post['idmodulo'], $idasignacion))
					{
						Alerta::guardarMensajeInfo('Asignaci&oacute; completada');
					}
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El M&oacute;dulo es obligatorio');
			}
		}
	}
}

if(isset($post['boton']) && $post['boton'] == 'Actualizar')
{
	if(isset($post['idAsignacion']) && is_numeric($post['idAsignacion']))
	{
		if(isset($post['limitTime']))
		{
			$tiempo = '00:00';
		}
		else
		{
			if($post['horas'] == '00' && $post['minutos'] == '00')
			{
				$tiempo = $post['tiempo'];
			}
			else
			{
				$tiempo = $post['horas'].':'.$post['minutos'];
			}
		}
		
		if($objAutoevaluacionAdmin->actualizarAsignacion($post['idmodulo'], $post['n_intentos'], 
			$post['penalizacion_blanco'], $post['penalizacion_nc'], $post['nota_minima'], $tiempo, 
			$post['modoCorreccion'], $post['aleatoria'], $post['idAsignacion']))
		{
			Alerta::guardarMensajeInfo('Asignaci&oacute;n actualizada correctamente');
		}
	} 
}

if(isset($post['idmodulo']) && is_numeric($post['idmodulo']) && !isset($post['idAsignacion']))
{
	$objModulo->set_modulo($post['idmodulo']);
	$autoevalModulos = $objModulo->autoevaluacionesModulo($post['idmodulo']);
	$elModulo = $objModulo->buscar_modulo($post['idmodulo']);
	$datoModulo = mysqli_fetch_assoc($elModulo);
}

if(isset($get['boton']) && $get['boton'] == 'Eliminar')
{
	if(isset($get['idAsignacion']) && is_numeric($get['idAsignacion']))
	{
		if($objAutoevaluacionAdmin->eliminarAsignacionAutoevaluacion($get['idAsignacion']))
		{
			Alerta::guardarMensajeInfo('La autoevaluaci&oacute;n ha sido eliminada del m&oacute;dulo');
		}	
	}
}

$modulos =$objModulo->ver_modulos();

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general-asignar-autoevaluaciones'));