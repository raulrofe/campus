<?php
mvc::cargarModuloSoporte('foro');
$objModelo = new AModeloForo();

$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		$idusuario = Usuario::getIdUser();
		$temaGeneral = 0;
		
		if(isset($post['general']) && is_numeric($post['general']))
		{
			$temaGeneral = $post['general'];
		}
		
		if(isset($post['titulo'], $post['idModulo']) && is_numeric($post['idModulo']))
		{
			if(!empty($post['titulo']))
			{
				$rowTema = $objModelo->obtenerUnTemaPorTituloPlantilla($post['idModulo'], $post['titulo']);
				if($rowTema->num_rows == 0)
				{
					
					if($objModelo->nuevoTemaPlantilla($post['titulo'], $idusuario, $post['idModulo'], $temaGeneral, $post['prioridad']))
					{
						Alerta::guardarMensajeInfo('Se ha creado el nuevo tema');
						
						$idTema = $objModelo->obtenerUltimoIdInsertado();
						Url::redirect('contenidos/editar/' . $post['idModulo'] . '/foro');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('Ya existe un tema en ese m&oacute;dulo con el mismo nombre (o es un tema general)');
				}
			}
		}
	}	
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{	
	if(isset($get['idTemaForo']) && is_numeric($get['idTemaForo']))
	{
		$tema = $objModelo->obtenerUnTema($get['idTemaForo']);
		if($tema->num_rows == 1)
		{
			$tema = $tema->fetch_object();
			$objModelo->eliminarTema($get['idTemaForo']);
			Alerta::guardarMensajeInfo('Tema de foro eliminado');
			Url::redirect('contenidos/editar/' . $tema->idmodulo . '/foro');
		}
	}
}
