$(document).ready(function()
{
	$('#scormSearch').keyup(function(event)
	{
		var wordSearch = $(this).val();
		
		$('#ListCenter').find('.rlt').addClass('hide');
		
		if(wordSearch != "")
		{
			var titleScorm = '';
			var searchMatch = null;
			$('#ListCenter').find('.rlt').each(function(index)
			{
				titleScorm = $(this).find('label').html();
				//searchMatch = '/^(' + wordSearch + ')(.+)/i';
				//alert(titleScorm);
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('^(' + wordSearch + ')(.+)', 'i');
				if(titleScorm.match(regex))
				{
					$(this).removeClass('hide');
				}
			});
		}
		else
		{
			$('#ListCenter').find('.rlt').removeClass('hide');
		}
	});
});