$(document).ready(function()
{
	$("#frmNuevoContenido").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				idmodulo: {
					required: 'Seleccione un m&oacute;dulo',
					min:	  'Seleccione un m&oacute;dulo'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				idmodulo : {
					required  : true,
					min		  : 1,
					digits    :true
				}
			}
		});
});