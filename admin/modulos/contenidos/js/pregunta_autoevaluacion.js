$(document).ready(function()
		{
			$("#frmPreguntaAutoevaluacion").validate({
					errorElement: "div",
					messages: {
						enunciado: {
							required: 'Introduce un enunciado'
						}
					},
					rules: {
						enunciado : {
							required  : true
						}
					}
				});
		});