$(document).ready(function()
{
	$("#frmNuevoContenido").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				nmodulo: {
					required: 'Introduce un n&uacute;mero de referencia para el m&oacute;dulo',
					maxlength: 'El nombre debe contener al menos 3 caracteres'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				nmodulo : {
					required  : true,
					maxlength : 3
				}
			}
		});
});