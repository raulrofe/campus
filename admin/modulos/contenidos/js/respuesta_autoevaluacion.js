$("#nRespuestasAutoeval").keyup(function() {
  var nInput = $("#nRespuestasAutoeval").val();

	if(!isNaN(nInput))
  	{
		  if(nInput != '' && nInput != undefined)
		  {
		  	$("#RespuestasAutoevaluacion").empty();

			for ( var i = 1; i <= nInput; i++ )
			{

				var htmlInput = '<div>';
				htmlInput += '<input type="radio" name="correcta" value="' + i + '" style="margin-right:10px;"/>';
				htmlInput += '<label for="columns-text" class="control-label">Respuesta: ' + i + '</label>';
				htmlInput += '<input type="text" name="respuesta[]" />';
				htmlInput += '</div>';

				$("#RespuestasAutoevaluacion").append(htmlInput);
			}

		  }
		  else
		  {
		  		$("#RespuestasAutoevaluacion").empty();
		  }
	}
	else
	{
		$("#RespuestasAutoevaluacion").empty();
	}

});