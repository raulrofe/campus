$(document).ready(function()
{
	$("#frmNuevoContenido").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				idmodulo: {
					required: 'Seleccine un m&oacute;dulo'
				},
				nmodulo: {
					required: 'Introduce un n&uacute;mero de referencia para el m&oacute;dulo',
					maxlength: 'El nombre debe contener al menos 3 caracteres'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				idmodulo : {
					required  : true,
					min		  : 1,
					digits    :true
				}
			}
		});
});