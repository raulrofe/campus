$(document).ready(function()
{
	$("#frmNuevoAutoevaluacion").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				idTematica: {
					required: 'Seleccione una tem&aacute;tica'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				idTematica : {
					required  : true,
					digits    :true
				}
			}
		});
});