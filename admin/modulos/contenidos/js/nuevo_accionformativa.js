$(document).ready(function()
{
	$("#frmNuevaAccionFormativa").validate({
			errorElement: "div",
			messages: {
				nombre_af: {
					required: 'Introduce el c&oacute;digo de Acci&oacute;n formativa'
				},
				idmodulo: {
					required: 'Selecciona un m&oacute;dulo'
				}
			},
			rules: {
				nombre_af : {
					required  : true
				},
				idmodulo : {
					required  : true,
					digits    : true
				}
			}
		});
});