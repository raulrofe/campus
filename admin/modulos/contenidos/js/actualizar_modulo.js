$(document).ready(function()
{
	$("#frmNuevoContenido").validate({
			errorElement: "div",
			messages: {
				atitulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				anmodulo: {
					required: 'Introduce un n&uacute;mero de referencia para el m&oacute;dulo',
					maxlength: 'El nombre debe contener al menos 3 caracteres'
				}
			},
			rules: {
				atitulo : {
					required  : true
				},
				anmodulo : {
					required  : true,
					maxlength : 3
				}
			}
		});
});