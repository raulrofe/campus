<div class="submenuAdmin">ASIGNAR AUTOEVALUACIONES</div>
<div class='addAdmin'><a href='contenidos/asignar-autoevaluaciones/insertar'>Nuevo asignaci&oacute;n</a></div>
<!-- Listado de Convocatorias -->
	<div class='buscador'>
		<P class='t_center'>Seleccione un m&oacute;dulo para ver las autoevaluaciones que tiene asignado</P><br/>
			<br/>
			<form name='busca_tipo_curso' action='contenidos/autoevaluaciones/asignar' method='post'>
				<div class='t_center'>
					<?php $objModulo->select_modulos($modulos); ?>
				</div>
				<br/><br/>
			</form>
	</div>
<!-- fin Listado de convocatorias -->

<?php 
	if (isset($get['boton']) && $get['boton'] == 'Insertar')
	{
		require_once('nueva-asignacion-autoevaluacion.php');
	}
	if (isset($post['idmodulo']) && is_numeric($post['idmodulo']) && !isset($post['idAsignacion']))
	{
		require_once('asignaciones-autoevaluaciones-modulo.php');
	}
	if (isset($get['boton']) && $get['boton'] == 'Actualizar')
	{
		require_once('editar-asignar.php');
	}
?>
