	<p class='subtitleAdmin'>INTRODUCIR M&Oacute;DULO</p>
	<div class='formulariosAdmin'>
		<form action='contenidos' method='post' enctype="multipart/form-data" id="frmNuevoContenido">
			<div class='filafrm'>
				<div class='etiquetafrm'>Nombre m&oacute;dulo</div>
				<div class='campofrm'><input type='text' name='titulo' size='45'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>N&uacute;mero asignado</div>
				<div class='campofrm'><input type='text' name='nmodulo' size='45' maxlength='3' /></div>
				<div class='obligatorio'>(*)</div>		
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Descripci&oacute;n</div>
				<div class='campofrm'><textarea name='descripcion' class='estilotextarea2'></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Contenidos</div>
				<div class='campofrm'><textarea name='contenidos' class='estilotextarea2'></textarea></div>
			</div>	
			<div class='filafrm'>
				<div class='etiquetafrm'>Objetivos</div>
				<div class='campofrm'><textarea name='objetivos' class='estilotextarea2'></textarea></div>
			</div>	
			<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>
			</div>	
			<div class='clear'></div>					
			<div><input type='submit' value='Insertar'></input></div>	
			<input type='hidden' name='boton' value='Insertar' />			
		</form>
	</div>

<script type="text/javascript" src="js-contenidos-nuevo_modulo.js"></script>