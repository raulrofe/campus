<p class='subtitleAdmin'>ACTUALIZAR DATOS DEL M&Oacute;DULO</p>
<div class='formulariosAdmin'>
  	<form name='frm_curso' action='contenidos' method='post' enctype="multipart/form-data" id="frmNuevoContenido">
		<div class='filafrm'>
			<div class='etiquetafrm'>Nombre m&oacute;dulo</div>
			<div class='campofrm'><input type='text' name='atitulo' size='60' value='<?php echo $f['nombre'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>N&uacute;mero asignado</div>
			<div class='campofrm'><input type='text' name='anmodulo' size='60' value='<?php echo $f['referencia_modulo'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Descripci&oacute;n</div>
			<div class='campofrm'>
				<textarea name='adescripcion' class='estilotextarea2'><?php echo $f['descripcion'];?></textarea>
			</div>
		</div>	
		<div class='filafrm'>
			<div class='etiquetafrm'>Contenidos</div>
			<div class='campofrm'>
				<textarea name='acontenidos' class='estilotextarea2'><?php echo $f['contenidos'];?></textarea>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Objetivos</div>
			<div class='campofrm'>
				<textarea name='aobjetivos' class='estilotextarea2'><?php echo $f['objetivos'];?></textarea>
			</div>
		</div>	
		<div class='clear'></div>			
		<div>
			<input type='submit' value='Actualizar datos' />		
		</div>
		<input type='hidden' name='idmodulo' value='<?php echo $idmodulo; ?>' />
	</form> 
</div>

<script type="text/javascript" src="js-contenidos-actualizar_modulo.js"></script>
