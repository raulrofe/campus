<br/>
<?php if(isset($post['idmodulo'], $datoModulo)):?>
	<div class='subtitleModulo t_center'>Autoevaluaciones asignadas al m&oacute;dulo <?php echo $datoModulo['nombre']?></div>
	<?php if(mysqli_num_rows($autoevalModulos) > 0):?>
		<?php while($autoeval = mysqli_fetch_assoc($autoevalModulos)):?>
			<div style='border:1px solid #ccc;margin-bottom:5px;padding:5px;'>
				<?php echo $autoeval['titulo_autoeval']?> - <?php echo $autoeval['f_creacion']?>
				<span class='fright'><a href='#' <?php echo Alerta::alertConfirmOnClick('eliminarasignacion', '¿Estas seguro que desea eliminar la asignación?','contenidos/autoevaluaciones/eliminar-asignar/'.$autoeval['idtest'])?>>eliminar</a></span>
				<span class='fright' style='margin-right:10px;'><a href='contenidos/autoevaluaciones/editar-asignar/<?php echo $autoeval['idtest'].'/'.$post['idmodulo'];?>') title='editar asignacion'>editar</a></span>
			</div>
		<?php endwhile;?>
	<?php else:?>
		<p class='t_center borde' style='margin-bottom:5px;padding:5px;'>No existen autoevaluaciones asignadas para este m&oacute;dulo</p>
	<?php endif; ?>
<?php endif; ?>
