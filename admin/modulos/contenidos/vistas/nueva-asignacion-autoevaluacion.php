<br/>
<div class='subtitleModulo'>Asignar Autoevaluaciones a M&oacute;dulo</div>
<div class='cajafrm'>
<form method='post' action='contenidos/asignar-autoevaluaciones/insertar' name='tematica'>
	<div class='filafrm'>
		<div class='fleft'>
			<select name='idTematica' onchange = "this.form.submit()">
				<option value='0'>Tem&aacute;tica com&uacute;n</option>
				<?php 
					while($f = mysqli_fetch_assoc($tematicas)){
						if($idTematica == $f['idtematica_autoeval']) echo "<option value='".$f['idtematica_autoeval']."' selected>".Texto::textoPlano($f['tematica_autoeval'])."</option>";
						else echo "<option value='".$f['idtematica_autoeval']."'>".Texto::textoPlano($f['tematica_autoeval'])."</option>";
							
					}
				?>
			</select>
		</div>
	</div>
</form>
	<form method='post' action='contenidos/asignar-autoevaluaciones/insertar' name='autoevaluaciones'>
		<div class='filafrm'>
			<div style='line-height:25px;'>
				<?php if(mysqli_num_rows($autoevaluaciones) > 0):?>
					<?php while($autoevaluacion = mysqli_fetch_assoc($autoevaluaciones)):?>
						<input type='checkbox' name='idAutoevaluacion[]' value='<?php echo $autoevaluacion['PTidautoeval']?>' /> <?php echo Texto::textoPlano($autoevaluacion['PTtitulo_autoeval'])?><br/>
					<?php endwhile;?>
				<?php else:?>
					<p>No existen Autoevaluaciones relacionadas con esta tem&aacute;tica</p>
				<?php endif?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>M&oacute;dulo</div>
			<div class='campofrmExtra'><?php $objModulo->select_modulos_noauto($resultado2); ?></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>N&uacute;mero m&aacute;ximo de intentos</div>
			<div class='campofrmExtra'><input type='text' name='n_intentos' value='2'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Penalizaci&oacute;n por respuesta en blanco</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='0.25' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Penalizaci&oacute;n por respuesta no contestada</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='0.25' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Nota m&iacute;nima</div>
			<div class='campofrmExtra'><input type='text' name='nota_minima' value='5'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Tiempo realizaci&oacute;n</div>
			<div class='campofrmExtra'>
				<input type="checkbox" name="limitTime" value="0" />&nbsp;Sin l&iacute;mite de tiempo<br/><br/>
				Horas : <select name='horas'><?php Html::horas();?></select>
				Minutos : <select name='minutos'><?php Html::minutos();?></select>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Seleccione el modo de correción</div>
			<div class='campofrmExtra'>
				<input type="radio" name="modoCorreccion" value="0" />&nbsp; Mostrar s&oacute;lo el resultado final<br/>
				<input type="radio" name="modoCorreccion" value="1" />&nbsp; Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final<br/>
				<input type="radio" name="modoCorreccion" value="2" checked/>&nbsp; Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Preguntas aleatorias</div>
			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' /> Sí &nbsp;&nbsp;
				<input type='radio' name='aleatoria' value='0' checked /> No
			</div>
		</div>
		<input type='submit' value='Asignar' />
		<input type='hidden' name='boton' value='Insertar' />
	</form>
</div>
