<p class='subtitleAdmin'>ACTUALIZAR CONTENIDO SIN SCORM</p>
<div class='formulariosAdmin'>
	<form name='frm_modulo' action='contenidos/ficheros' method='post' enctype='multipart/form-data' id="frmNuevoContenido">
		<div class='filafrm'>
			<div class='etiquetafrm'>T&iacute;tulo</div>
			<div class='campofrm'><input type='text' name='titulo' size='60' value='<?php echo $f['titulo'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>M&oacute;dulo</div>
			<div class='campofrm' style='width:300px;'><?php $mi_modulo->select_modulos_noauto($registros2);?></div>
			<div class='obligatorio'>(*)</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Prioridad</div>
			<div class='campofrm'><input type='text' name='prioridad' size='1' value='<?php echo $f['prioridad'];?>' /></div>
		</div>	
		<div class='filafrm'>
			<div class='etiquetafrm'>Descripci&oacute;n</div>
			<div class='campofrm'><textarea name='descripcion' class='estilotextarea2'><?php echo $f['descripcion'];?></textarea></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Cambiar archivo</div>
			<div class='campofrm'><input type='file' name='archivo' size='45' /></div>
		</div>
		<div><input type='submit' value='Actualizar'></input></div>						
		<input type='hidden' name='idarch' value='<?php echo $get['idarch'] ?>' />
		<input type='hidden' name='boton' value='Ver contenido' />
	</form>
</div>	

<script type="text/javascript" src="js-contenidos-actualizar_contenidosinscorm.js"></script>
