<h4><?php echo $autoevaluacion->PTtitulo_autoeval ?></h4>
<div class="block block-themed themed-default">
	<div class="block-title"><h5>INSERTAR PREGUNTAS Y RESPUESTAS</h5></div>
	<div class="block-content full">
		<form method='post' action='contenidos/autoevaluaciones/preguntar/<?php echo $autoevaluacion->PTidautoeval ?>' name='buscar_autoevaluacion' id='frmBuscarAutoevaluacion' class="form-inline" >
            <div style="background-color:#EFEFEF;padding:10px;">
	            <div class="row-fluid">         
		            <div>
		            	<div class="control-group">
		                	<label for="columns-text" class="control-label">Enunciado</label>
	                        <div><textarea name="pregunta" style="width:100%;"></textarea></div>
	                    </div>
					</div>
				</div>
				<div class="row-fluid">         
		            <div>
		            	<div class="control-group">
		                	<label for="columns-text" class="control-label">N&uacute;mero de respuestas</label>
	                        <div><input type="text" name="nRespuestas" id="nRespuestasAutoeval"/></div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="row-fluid" id="RespuestasAutoevaluacion" style="padding:10px;">  
				<!-- Este div esta relleno mediante JQUERY con un numero determinado de bucles segun indicado en el campo de arriba -->         
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>


<script type="text/javascript" src="js-contenidos-respuesta_autoevaluacion.js"></script>

<?php if(isset($autoevaluacionContent) && count($autoevaluacionContent) > 0): ?>
	<?php $cont = 1; ?>
	<?php foreach($autoevaluacionContent as $question): ?>
		<div style="margin:10px 0">
			<div><?php echo $cont . '.- ' . $question['enunciado'] ?></div>
			<?php if(isset($question['respuestas']) && count($question['respuestas'])): ?>
				<?php $contRes = 1; ?>
				<div style="padding-left:20px;">
					<?php foreach($question['respuestas'] as $res): ?>
						<?php $letraTest = Texto::getLetraTest($contRes); ?>
						<?php $classRespuesta = '' ?>
						<?php if($res['correcta'] == 1): ?>
							<?php $classRespuesta = 'negrita'?>
						<?php endif; ?> 
						<div class="<?php echo $classRespuesta ?>"><?php echo $letraTest . ') ' . $res['respuesta'] ?></div>
						<?php $contRes++; ?>
					<?php endforeach; ?>
					<div class="btn-group">
			      		<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="contenidos/autoevaluaciones/editar/pregunta/<?php echo $question['idPregunta']; ?>" data-original-title="Editar"><i class="icon-pencil"></i></a>
			        	<a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/autoevaluaciones/eliminar/pregunta/<?php echo $question['idPregunta']; ?>" onclick="alertConfirmBtn('¿Quieres eliminar a este temática?>?');return false;" data-original-title="Eliminar"><i class="icon-remove"></i></a>
			    	</div>
				</div>
			<?php endif; ?>
			<?php $cont++; ?>
		</div>
	<?php endforeach; ?>
<?php endif; ?>

