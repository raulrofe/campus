<div id="admin_usuarios">
	<div class="titular">
		<h1>Acciones Formativas</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>

  	<div class="stContainer" id="tabs">
	  	<div class="tab">
	 		<div class="submenuAdmin">CARGA MASIVA</div>	
			<p class='subtitleAdmin'>REALIZAR CARGA MASIVA DE ACCIONES FORMATIVAS</p>
			<div class='formulariosAdmin'>		
				<form method="post" action="" enctype="multipart/form-data" class='t_center'>
					<p class='t_center'>Seleccione una convocatoria para la carga masiva de acciones formativas</p><br/>
					<select name="idconv">
					  	<option value=''> - Seleccione una convocatoria - </option>
					  	<?php while($conv = $convocatorias->fetch_object()):?>
					  		<option value="<?php echo $conv->idconvocatoria?>"><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
					  	<?php endwhile;?>
					  </select>
					  <br/><br/>
					  <input type="file" name="archivo" />
					  <br/><br/>
					  <button type="submit">Cargar</button>
				</form>
				<?php echo $htmlLog;?>
		  	</div>
		</div>
	</div>
</div>
