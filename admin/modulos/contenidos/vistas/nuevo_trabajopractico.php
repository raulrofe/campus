	<p class='subtitleAdmin'>INTRODUCIR TRABAJO PR&Aacute;CTICO</p>
	<div class='formulariosAdmin'>
		<form name='frm_modulo' action='contenidos/trabajos_practicos' method='post' enctype='multipart/form-data' id='frmNuevoContenido'>
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo</div>
				<div class='campofrm'><input type='text' name='titulo' size='60'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>M&oacute;dulo</div>
				<div class='campofrm' style='width:512px;'><?php $mi_modulo->select_modulos_noauto($registros2);?></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Prioridad</div>
				<div class='campofrm'><input type='text' name='prioridad' size='1' /></div>
			</div>	
			<div class='filafrm'>
				<div class='etiquetafrm'>Descripci&oacute;n</div>
				<div class='campofrm'><textarea name='descripcion' class='estilotextarea2'></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Subir archivo</div>
				<div class='campofrm'><input type='file' name='archivo' size='60' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>
			</div>
			<div class='clear'></div>
			<div><input type='submit' value='Insertar'></input></div>	
			<input type='hidden' name='boton' value='Insertar' />					
		</form>
	</div>

<script type="text/javascript" src="js-contenidos-nuevo_trabajopractico.js"></script>
