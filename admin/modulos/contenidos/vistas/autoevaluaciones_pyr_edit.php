<h4><?php echo $autoevaluacion->PTtitulo_autoeval ?></h4>
<div class="block block-themed themed-default">
	<div class="block-title"><h5>INSERTAR PREGUNTAS Y RESPUESTAS</h5></div>
	<div class="block-content full">
		<form method='post' action='contenidos/autoevaluaciones/editar/pregunta/<?php echo $pregunta->PTidpreguntas ?>' name='buscar_autoevaluacion' id='frmBuscarAutoevaluacion' class="form-inline" >
            <div class="row-fluid">         
	            <div>
	            	<div class="control-group">
	                	<label for="columns-text" class="control-label">Enunciado</label>
                        <div><textarea name="pregunta" style="width:100%;"><?php echo $pregunta->PTpregunta ?></textarea></div>
                    </div>
				</div>
			</div>
			<div class="row-fluid">         
	            <div>
	            	<div class="control-group">
	                	<label for="columns-text" class="control-label">N&uacute;mero de respuestas</label>
                        <div><input type="text" name="nRespuestas" id="nRespuestasAutoeval" value="<?php echo $respuestas->num_rows ?>"/></div>
                    </div>
				</div>
			</div>
			<div class="row-fluid" id="RespuestasAutoevaluacion">  
				<?php $cont = 1; ?>
				<?php while($respuesta = $respuestas->fetch_object()): ?>
					<div>
						<label for="columns-text" class="control-label">Respuesta: <?php echo $cont ?></label>
						<input type="text" name="respuesta[]" value="<?php echo $respuesta->PTrespuesta ?>"/>
					</div>
					<div>
						<input type="radio" name="correcta" value="<?php echo $cont ?>" style="margin-right:5px;" <?php if($respuesta->PTcorrecta == 1) echo 'checked' ?> />				
						<label for="columns-text" class="control-label" >Marcar <b>Respuesta <?php echo $cont ?></b> como <b>correcta</b></label>
					</div>
					<?php $cont++; ?> 
				<?php endwhile; ?>   
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-respuesta_autoevaluacion.js"></script>

