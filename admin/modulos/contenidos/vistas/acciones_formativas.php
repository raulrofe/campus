<div class="submenuAdmin">ACCIONES FORMATIVAS</div>

<div class='addAdmin'><a href='contenidos/acciones_formativas/insertar'>Introducir Acci&oacute;n Formativa</a></div>
<!-- Listado de Acciones formativas -->
	<div class='buscador'>
		<P class='t_center'>Acciones formativas introducidas en este momento en el sistema:</P><br/>
			<form name='busca_tipo_curso' action='contenidos/acciones_formativas' method='post' class='t_center'>
					<select name='idAccionF'>
						<option value=''> - Seleccione una Acci&oacute;nFormativa - </option>
						<?php while ($rowAF = mysqli_fetch_assoc($resultado)):?>
							<?php if(isset($post['idAccionF']) && ($rowAF['idaccion_formativa'] == $post['idAccionF'])):?>
								<option value='<?php echo $rowAF['idaccion_formativa']; ?>' selected><?php echo $rowAF['accion_formativa']; ?></option>
							<?php else:?>
								<option value='<?php echo $rowAF['idaccion_formativa']; ?>'><?php echo $rowAF['accion_formativa']; ?></option>
							<?php endif;?>
						<?php endwhile; ?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
				<br/><br/>
				<div class='t_center'>
					<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
					<span class='boton_borrar'><input id="frmBtnBorrar" type='submit' name='boton' value='Borrar' /></span>
				</div>
			</form>
	</div>
<!-- fin Listado acciones formativas -->

<?php 
	if(isset($boton) && $boton == 'Insertar')
	{
		require_once('nueva_acciones_formativas.php');
	}
	if(isset($boton) && $boton == 'Actualizar')
	{
		require_once('actualizar_acciones_formativas.php');
	}
?>

<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar a esta acci&oacute;n formativa?');</script>