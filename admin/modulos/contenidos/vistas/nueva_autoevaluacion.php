<p class='subtitleAdmin'>INTRODUCIR AUTOEVALUACI&Oacute;N</p>
<div class='formulariosAdmin'>
	<div class='cajafrm'>
		<form method='post' action='contenidos/autoevaluaciones' id='frmNuevoAutoevaluacion'>
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo autoevaluaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='titulo' size='60' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tem&aacute;tica</div>
				<div class='campofrm'>
					<select name='idTematica' class='select100'>
						<option value='0'> Tem&aacute;tica com&uacute;n</option>
						<?php 
							while($data = mysqli_fetch_assoc($tematicas3))
							{
								echo "<option value='".$data['idtematica_autoeval']."'>".$data['tematica_autoeval']."</option>";
							}
						?>
					</select>
				</div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tipo autoevaluaci&oacute;n</div>
				<div class='campofrm'>
					<select name='idtematica' class='select100'>
						<?php 
							while($fila = mysqli_fetch_assoc($tipos))
							{
								echo "<option value='".$fila['idtipo_autoeval']."'>".ucfirst($fila['formato'])."</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Descripci&oacute;n</div>
				<div class='campofrm'><textarea name='descripcion' class='estilotextarea2'></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>
			</div>
			<input type='hidden' name='boton' value='Insertar' />
			<input type='submit' value='Insertar' />
		</form>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-nuevo_autoevaluacion.js"></script>