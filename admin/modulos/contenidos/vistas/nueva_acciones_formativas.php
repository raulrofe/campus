	<p class='subtitleAdmin'>INTRODUCIR ACCI&Oacute;N FORMATIVA</p>
	<div class='formulariosAdmin'>		
		<form name='frm_multiple' action='' method='post' id='frmNuevaAccionFormativa'>
			<div class='filafrm'>
				<div class='etiquetafrm'>C&oacute;digo Acci&oacute;n formativa</div>
				<div class='campofrm'><input type='text' name='nombre_af' size='40' maxlength='5'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<?php $mi_modulo->selectmultiple_modulos($registros2); ?>
			<div class='filafrm'>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='obligatorio'>Campos obligatorios (*)</div>
			<input type='submit' value='Crear' class='btn_separado'/>
		</form>
	</div>
		<!-- Este es el que muestra los modulos que pertenecen a la accion formativa -->	
		<div class='fleft' style='width:100%;margin:5px 0 10px 0;'>
			<div class='view' >
				<div class='header'>
					<div class='itemg3'>Acci&oacute;n Formativa - M&oacute;dulos</div>
				</div>
				<div class='boder'>
					<form name='frm_ordenacion' method='post' action=''>
						<?php $num=1; ?>
							<?php if(!empty ($registros) && mysqli_num_rows($registros) > 0): ?>
								<?php while($f = mysqli_fetch_assoc($registros)):?>											
									<div class='bod_cont'>
										<div class='itemg3'>(<?php echo $f['accion_formativa'];?>) - M&oacute;dulo <?php echo $num.": ".$f['nombre']; ?></div>
									 </div>
									<?php $num++; ?>
								<?php endwhile; ?>	
								<div class='rojo'>* Para poder acutalizarla selecione la acci&oacute;n formativa en la lista y pulse actualizar</div>										
							<?php endif; ?>
					</form>
				</div>
			</div>
		</div>

<script type="text/javascript" src="js-contenidos-nuevo_accionformativa.js"></script>