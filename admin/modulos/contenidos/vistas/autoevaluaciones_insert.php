<div class="block">
	<div class="block-title"><h5>INTRODUCIR AUTOEVALUACI&Oacute;N</h5></div>
	<div class="block-content full">
		<form class="form-inline" name='frm_modulo' action='contenidos/autoevaluaciones/insertar' method='post' enctype='multipart/form-data' id="frmNuevoContenidoAutoevaluacion" >
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;po autoevaluaci&oacute;n</div>
						<div class='campofrm'>
							<select name="idTipoAutoevaluacion">
								<?php while($tipoAutoevaluacion = $tipoAutoeval->fetch_object()): ?>
									<option value="<?php echo $tipoAutoevaluacion->idtipo_autoeval ?>"><?php echo $tipoAutoevaluacion->tipo_autoeval ?></option>
								<?php endwhile; ?>
							</select>
						</div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Tem&aacute;tica</div>
						<div class='campofrm'>							
							<select name="idTematica">
								<?php while($tematica = $tematicas->fetch_object()): ?>
									<option value="<?php echo $tematica->idtematica_autoeval ?>"><?php echo $tematica->tematica_autoeval ?></option>
								<?php endwhile; ?>
							</select></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;tulo</div>
						<div class='campofrm'><input type='text' name='titulo' /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><input type='text' name='descripcion' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>N&uacute;mero de intentos</div>
						<div class='campofrm'><input type='text' name='numeroIntentos' size='1' /></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Nota m&iacute;nima</div>
						<div class='campofrm'><input id="file" type='text' name='notaMinima' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Penalizaci&oacute;n respuesta en blanco</div>
						<div class='campofrm'><input type='text' name='penalizacionBlanco' size='1' /></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Penalizaci&oacute;n respuesta no contestada</div>
						<div class='campofrm'><input id="file" type='text' name='penalizacionNoContesta' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Tiempo realizaci&oacute;n</div>
						<div class='campofrm'><input id="limitTime" type="checkbox" name="tiempoRealizacion" value="1" style="margin-right:5px;"/><label for="limitTime">Sin l&iacute;mite de tiempo</label></div>
						<div class='campofrm'>
							<label>Horas</label>
							<select name="horas" style="width:13%;margin-right:10px;">
								<option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="19">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
							<label>Minutos</label>
							<select name="minutos" style="width:13%">
								<option value="00">00</option>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>							
							</select>
						</div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Modo de correcci&oacute;n</div>
						<div class='campofrm'><input type="radio" value="0" name="modoCorreccion" style="margin-right:5px;" /><span style="vertical-align:middle">Mostrar s&oacute;lo resultado final</span></div>
						<div class='campofrm'><input type="radio" value="1" name="modoCorreccion" style="margin-right:5px;" /><span style="vertical-align:middle">Mostrar "CORRECTA" o "INCORRECTA" y el resultado final</span></div>
						<div class='campofrm'><input type="radio" value="2" name="modoCorreccion" style="margin-right:5px;" /><span style="vertical-align:middle">Mostrar "CORRECTA" o "INCORRECTA", resaltar item correcto y resultado final</span></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'></div>
						<div class='campofrm'>
							<input type="checkbox" name="aleatorias" value="1" style="margin-right: 10px;"/><label>¿Mostrar las preguntas aleatoriamente?</label>
						</div>
					</div>
					<div class="clear"></div>
				</div>

			</div>
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>