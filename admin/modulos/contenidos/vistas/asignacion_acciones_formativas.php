<h2>Acciones formativas y centros con RLT</h2><br/>
<form name='afrlt' method='post' action=''>
	<div class='subtitle'>Convocatorias</div>
	<select name='idConvocatoria' onchange='this.form.submit()'>
		<option value=''>Seleccion una Convocatoria</option>
		<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
			<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(isset ($post['idConvocatoria']) && $convocatoria['idconvocatoria'] == $post['idConvocatoria']) echo 'selected';?>><?php echo $convocatoria['nombre_convocatoria']?></option>
		<?php endwhile;?>
	</select>
</form>

<?php if(isset ($post['idConvocatoria'])):?>
	<?php if (isset($AF)):?>
		<?php for($i=0;$i<=count($AF['id'])-1;$i++):?>
			<div>
				<div class='subtitle'><?php echo $AF['nombre'][$i]?></div>
					<?php 
						$centros = $objAF->obtenerCentroAF($AF['id'][$i],$post['idConvocatoria']); 
						while($centro = mysqli_fetch_assoc($centros))
						{
							echo "<div class='sangria'>".$centro['cif']."(".$centro['nombre_centro'].")</div>";
						}
					?>		
			</div>
		<?php endfor;?>
	<?php endif;?>
	<?php else: echo "<br/>No existen asignacion entre acciones formativas y centros con representante legal (RLT), para esta convocatoria<br/>";?>
<?php endif;?>