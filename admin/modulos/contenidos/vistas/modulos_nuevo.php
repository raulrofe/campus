<div class="block block-themed themed-default">
	<div class="block-title"><h5>INTRODUCIR M&Oacute;DULO</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmNuevoContenido" action='contenidos/insertar' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Nombre m&oacute;dulo</label>
	                        	<div class="controls"><input type='text' name='titulo' size='60'/></div>
	                        </div>
						</div>
	
					<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">N&uacute;mero asignado</label>
		                        <div class="controls"><input type='text' name='nmodulo' size='60' maxlength='3' /></div>
	                        </div>
		                </div>	
		                
		            <div class="clear"></div>
		         </div>
                
				<!-- Column -->
                	<div>
	                	<label>Descripción</label>
                        <div class="controls"><textarea name='descripcion' class="AreaFullWidth"></textarea></div>
	                </div>	                                
                
                <!-- Column -->
                	<div>
	                	<label>Contenidos</label>
                        <div class="controls"><textarea name='contenidos' class="AreaFullWidth"></textarea></div>
	                </div>
	                
                <!-- Column -->
                	<div>
	                	<label>Objetivos</label>
                        <div class="controls"><textarea name='objetivos' class="AreaFullWidth"></textarea></div>
	                </div>	                	                	                
             </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-nuevo_modulo.js"></script>