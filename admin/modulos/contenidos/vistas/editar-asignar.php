<br/>
<div class='subtitleModulo'>ACTUALIZAR DATOS DE ASIGNACI&Oacute;N</div>
	<form method='post' action='contenidos/autoevaluaciones/asignar' name='autoevaluaciones'>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>M&oacute;dulo</div>
			<div class='campofrmExtra'><?php $objModulo->select_modulos_noauto($resultado2); ?></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>N&uacute;mero m&aacute;ximo de intentos</div>
			<div class='campofrmExtra'><input type='text' name='n_intentos' value='<?php echo $rowAsignacion['n_intentos'];?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Penalizaci&oacute;n por respuesta en blanco</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='<?php echo $rowAsignacion['penalizacion_blanco'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Penalizaci&oacute;n por respuesta no contestada</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='<?php echo $rowAsignacion['penalizacion_nc'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Nota m&iacute;nima</div>
			<div class='campofrmExtra'><input type='text' name='nota_minima' value='<?php echo $rowAsignacion['nota_minima'];?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Tiempo establecido</div>
			<div class='campofrmExtra'><?php echo $rowAsignacion['tiempo'];?></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Tiempo realizaci&oacute;n</div>
			<div class='campofrmExtra'>
				<input type="checkbox" name="limitTime" value="0" <?php if($rowAsignacion['tiempo'] == '00:00:00') echo 'checked="checked"';?> />&nbsp;Sin l&iacute;mite de tiempo<br/><br/>
				Horas : <select name='horas'><?php Html::horas();?></select>
				Minutos : <select name='minutos'><?php Html::minutos();?></select>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Seleccione el modo de correción</div>
			<div class='campofrmExtra'>
				<input type="radio" name="modoCorreccion" value="0" <?php if($rowAsignacion['correccion'] == 0) echo 'checked'; ?> />&nbsp; Mostrar s&oacute;lo el resultado final<br/>
				<input type="radio" name="modoCorreccion" value="1" <?php if($rowAsignacion['correccion'] == 1) echo 'checked'; ?> />&nbsp; Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final<br/>
				<input type="radio" name="modoCorreccion" value="2" <?php if($rowAsignacion['correccion'] == 2) echo 'checked'; ?> />&nbsp; Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra'>Preguntas aleatorias</div>
			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' <?php if($rowAsignacion['aleatorias'] == 1) echo 'checked'; ?>/> Sí &nbsp;&nbsp;
				<input type='radio' name='aleatoria' value='0' <?php if($rowAsignacion['aleatorias'] == 0) echo 'checked'; ?>/> No
			</div>
		</div>
		<input type='submit' value='Actualizar' />
		<input type='hidden' name='boton' value='Actualizar' />
		<input type='hidden' name='idAsignacion' value='<?php echo $rowAsignacion['idtest'];?>' />
		<input type='hidden' name='tiempo' value='<?php echo $rowAsignacion['tiempo']; ?>' />
	</form>