<?php if(!isset($post['enunciado']) || isset($post['enunciado'], $post['respuesta'], $post['correcta'])):?>

<!-- FORMULARIO DE PREGUNTA -->
	<p class='subtitleAdmin'>INSERTAR PREGUNTAS</p>
	<div class='formulariosAdmin'>
		<div class='cajafrm'>
			<form method='post' action='contenidos/autoevaluaciones' enctype='multipart/form-data' name='pregunta_autoevaluacion' id='frmPreguntaAutoevaluacion'>
				<div class='filafrm'>
					<div class='etiquetafrm'>Enunciado pregunta</div>
					<div class='campofrm'><textarea name='enunciado' class='estilotextarea2'></textarea></div>
					<div class='obligatorio'>(*)</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>N&uacute;mero de respuestas</div>
					<div class='campofrm'><input type='text' name='n_respuestas' size='1' value='4'/></div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Im&aacute;gen</div>
					<div class='campofrm'><input type='file' name='imagen_pregunta' size='72'/></div>
				</div>
				<div class='filafrm'>
					<div class='obligatorio'>Campos obligatorios(*)</div>
				</div>
			<input type='hidden' name='idAutoevaluacion' value='<?php  echo $post['idAutoevaluacion']; ?>' />
			<input type='hidden' name='boton' value='Preguntas y respuestas' />
			<input type='submit' value='Insertar respuestas' />
			</form>
		</div>	
	</div>
	
	<script type="text/javascript" src="js-contenidos-pregunta_autoevaluacion.js"></script>
<!-- FIN FORMULARIO DE PREGUNTA -->

<!-- RESUMEN DE LAS PREGUNTAS INSERTADAS CON SUS RESPUESTAS -->
	<div class='nota'>* N&uacute;mero total de preguntas creadas para esta evaluaci&oacute;n: <?php echo $numero_preguntas;?></div>
	<div class='preguntaTest' style='width:99%;height:300px;'>
		<?php 
		$cont=1;
		if(mysqli_num_rows($preguntas) > 0)
		{
			while($row = mysqli_fetch_assoc($preguntas))
			{
				$numLetra = 1;
				echo "<span class='negrita' style='color:#000;'>".$cont.".- </span><a href='contenidos/autoevaluaciones/editar-pregunta/".$row['PTidpreguntas']."'>
									<span class='negrita'>".$row['PTpregunta']."</span>
							   </a>
							   <span class='fright'><a href='#' ". Alerta::alertConfirmOnClick('eliminarpreguntasyrespuestas', '¿Estas seguro que desea eliminar esta pregunta con todas sus respuestas?','contenidos/autoevaluaciones/eliminar-pregunta/' . $row['PTidpreguntas'] ) . ">eliminar</a></span>
							   <span class='fright'><a href='contenidos/autoevaluaciones/editar-pregunta/".$row['PTidpreguntas']."' >editar | </a></span>
							   <br/><br/>";
				$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($row['PTidpreguntas']);
				while($respuesta = mysqli_fetch_assoc($respuestas))
				{
					$letra = AutoevaluacionAdmin::obtener_letra($numLetra);
					if($respuesta['PTcorrecta'] == 1) 
					{
						echo $letra."<span class='verde'>".$respuesta['PTrespuesta']."</span><br/>";
					}
					else
					{
						echo $letra.$respuesta['PTrespuesta']."<br/>";
					}
					$numLetra++;
				}
				$cont++;
				echo "<br/>";
			}
		}
		else
		{
			echo "No existen preguntas";
		}
		?>
	</div>
<!-- FIN DE RESUMEN -->
	
<?php else:?>
	<div class='caja_separada'>
		<p class='subtitleAdmin'>INSERTAR RESPUESTAS</p>
		<div class='formulariosAdmin'>
			<div class='cajafrm'>
			<p class='subtitleModulo'><?php  echo Texto::textoPlano(stripslashes($post['enunciado'])); ?></p>
			<p style='background-color:#ffcccc;border:1px solid #cccccc;margin:10px 0;padding:5px;'>* No olvides marcar la respuesta correcta</p>
				<form method='post' action='contenidos/autoevaluaciones' enctype='multipart/form-data' id='frmRespuestaAutoevaluacion'>
					<?php for($i=1;$i<=$post['n_respuestas'];$i++){?>
						<div class='filafrm'>
							<div class='etiquetafrm'>Respuesta <?php echo $i;?></div>
							<div class='campofrm'>
								<input type='text' name='respuesta[]' style='width:83%;' value='<?php if(isset($post['respuesta'][$i-1])) echo $post['respuesta'][$i-1];?>'/>
								<input class="respuestaAutoevalRadioCorrecta" type='radio' name='correcta[]' value='<?php echo $i;?>' />
							</div>
						</div>
						<div class='filafrm'> 
							<div class='etiquetafrm'>Imagen</div>
							<div class='campofrm'><input type='file' name='imagen_respuesta_<?php echo $i;?>' size='74'/></div>
						</div>
						<br/>
					<?php }?>
					<input type='hidden' name='idAutoevaluacion' value='<?php echo $post['idAutoevaluacion'];?>' />
					<input type='hidden' name='enunciado' value='<?php echo $post['enunciado'];?>' />
					<input type='hidden' name='n_respuestas' value='<?php echo $post['n_respuestas'];?>' />
					<input type='hidden' name='boton' value='Preguntas y respuestas' />
					<input type='submit' value='Agregar' />
				</form>
			</div>
		</div>
	</div>

<script type="text/javascript" src="js-contenidos-respuesta_autoevaluacion.js"></script>
<?php endif; ?>
