<div class="block">
	<div class="block-title"><h5>INTRODUCIR CONTENIDO SIN SCORM</h5></div>
	<div class="block-content full">
		<form class="form-inline" name='frm_modulo' action='contenidos/temario/insertar' method='post' enctype='multipart/form-data' id="frmNuevoContenidoTemario" >
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;tulo</div>
						<div class='campofrm'><input type='text' name='titulo' size='60'/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Prioridad</div>
						<div class='campofrm'><input type='text' name='prioridad' size='1' /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><textarea name='descripcion' style="width:100%;"></textarea></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Archivo</div>
						<div class='controls'><input id="file" type='file' name='archivo' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<input type="hidden" name="idModulo" value="<?php echo $get['idModulo'] ?>" />
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>
	
<script type="text/javascript" src="js-contenidos-nuevo_contenidosinscorm.js"></script>

	<?php if($archivosTemario->num_rows > 0): ?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th class="tableThCustom">Nombre</th>
	            	<th class="tableThCustom span1 text-center">Prioridad</th>
	                <th class="tableThCustom span1 text-center">Tama&ntilde;o</th>
	                <th class="tableThCustom span1 text-center">Fecha</th>          
	                <th class="tableThCustom span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
			<?php 	$peso_total = 0; ?>
			<?php while ($tema = mysqli_fetch_object($archivosTemario)): ?>
				<?php if(file_exists(PATH_ROOT . $tema->enlace_archivo)): ?>
					<?php  	$peso_archivo = filesize("../".$tema->enlace_archivo); ?>
					<?php	$peso_archivo = $peso_archivo/1024; ?>
					<?php	$peso_archivo = $peso_archivo/1024; ?>
					<?php	$peso_archivo = round($peso_archivo,2); ?>
					<?php	$peso_total = $peso_total + $peso_archivo; ?>
				<?php endif; ?>		
				<tr>
					<td><a href="<?php echo '../' . $tema->enlace_archivo ?>" title="Pdf temario <?php echo $tema->prioridad ?>" target="_blank"><?php echo $tema->titulo; ?></a></td>
		            <td class="span1 text-center"><?php echo $tema->prioridad; ?></td>
		            <td class="span1 text-center"><?php echo $peso_archivo; ?> Mb</td>         
		            <td class="span1 text-center"><?php echo Fecha::invertir_fecha($tema->f_creacion,'-','/');?></td>
		            <td class="span1 text-center">
		            	<div class="btn-group">
		            		<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="contenidos/temario/editar/<?php echo $tema->idarchivo_temario ?>" data-original-title="Editar"><i class="icon-edit"></i></a>
		                	<a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/temario/eliminar/<?php echo $tema->idarchivo_temario ?>" data-original-title="Eliminar"><i class="icon-remove"></i></a>
		                </div>
		            </td>
				</tr>
			<?php endwhile; ?>
		</table>
	<?php else: ?>
		<div class="text-center">No existen temas para este m&oacute;dulo</div>
	<?php endif;?>
