<div class="block">
	<div class="block-title"><h5>INTRODUCIR SCORM</h5></div>
	<div class="block-content full">
		<form class="form-inline" id="frm_contenido_multimedia" method="post" action="contenidos/scorm/insertar" enctype="multipart/form-data">
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;tulo</div>
						<div class='campofrm'><input type='text' name='titulo' size='60'/></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><textarea name='descripcion' style="width:100%;"></textarea></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1 control-group'>
						<div class='etiquetafrm'>Archivo</div>
						<div class='controls'><input id="file" type='file' name='file' size='60' /></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Portada</div>
						<div class='controls'><input id="file" type='file' name='image' size='60' /></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<input type="hidden" name="idModulo" value="<?php echo $get['idModulo'] ?>" />
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>
	
<script type="text/javascript" src="js-contenidos-nuevo_contenidosinscorm.js"></script>

	<?php if($scorms->num_rows > 0): ?>

		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th class="tableThCustom">Nombre SCORM</th>   
	                <th class="tableThCustom span1 text-center">Fecha</th>      
	                <th class="tableThCustom span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
			<?php while ($scorm = mysqli_fetch_object($scorms)): ?>
				<tr>
					<td>
						<img class="frontScorm" src="<?php echo '../' . $scorm->imagen_scorm ?>" alt="Portada SCORM" title="La portada del SCORM" />
						<?php echo $scorm->titulo; ?>
					</td>
					<td class="span1 text-center"><?php echo Fecha::obtenerFechaFormateada($scorm->fecha, false);?></td>
		            <td class="span1 text-center">
		            	<div class="btn-group">
		                	<a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/scorm/eliminar/<?php echo $scorm->idscorm ?>" data-original-title="Eliminar"><i class="icon-remove"></i></a>
		                </div>
		            </td>
				</tr>
			<?php endwhile; ?>
		</table>
	<?php else: ?>
		<div class="text-center">No existen contenido SCORM para este m&oacute;dulo</div>
	<?php endif;?>
