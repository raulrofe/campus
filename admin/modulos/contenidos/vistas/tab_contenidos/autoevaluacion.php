<div class="block">
	<div class="block-title"><h5>Buscar Autoevaluaci&oacute;nes</h5></div>
	<div class="block-content full">
		<form class="form-inline" name='frm_modulo_buscar_arutoevaluacion' action='contenidos/autoevaluacion/buscar/<?php echo $get['idModulo'] ?>' method='post' enctype='multipart/form-data' id="frmBuscarAutoevaluacion" >
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Tematicas</div>
						<div class='campofrm'>
							<select name="idTematica">
								<?php while($tematica = $tematicasAutoevaluaciones->fetch_object()): ?>
									<?php if(isset($post['idTematica']) && $post['idTematica'] == $tematica->idtematica_autoeval): ?>
										<option value="<?php echo $tematica->idtematica_autoeval ?>" selected><?php echo $tematica->tematica_autoeval ?></option>
									<?php else: ?>
										<option value="<?php echo $tematica->idtematica_autoeval ?>"><?php echo $tematica->tematica_autoeval ?></option>
									<?php endif; ?>
								<?php endwhile; ?>
							</select>
						</div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><input type='text' name='tituloAutoevaluacion' /></div>
					</div>
					<div class="clear"></div>
				</div>

			</div>
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-search"></i> Buscar</button></div>
		</form>
	</div>
</div>

<form id="AddDeleteAutoeval" action="contenidos/autoevaluacion/buscar/<?php echo $get['idModulo'] ?>" method="post">
	<div>
		<div style="width:55%;float:left;font-weight:bold;">Autoevaluaciones Disponibles</div>
		<div style="width:45%;float:left;font-weight:bold;">Autoevaluaciones Asignadas</div>
	</div>
	<div>
		<select name="idAutoevaluacionAsignar[]" multiple="multiple" style="width:45%;height:500px;float:left;border:1px solid #CCC">
			<?php if(isset($autoevaluaciones) && count($autoevaluaciones) > 0): ?>
				<?php foreach($autoevaluaciones as $key => $value): ?>
					<option value="<?php echo $key ?>"><?php echo $value; ?></option>
				<?php endforeach; ?>
			<?php else: ?>
					<option value=""></option>	
			<?php endif;?>
		</select>		
	</div>
	<div style="width:7%;height:500px;float:left;margin:0 1%;">
		<div style="border:1px solid #CFCFCF;wdith:6%;text-align:center;margin: 5px 5px 10px 10px;padding:0 5px;">
			<a href="#" onclick="asignatedAutoeval('more');return false;">A&ntilde;adir</a>
		</div>
		<div style="border:1px solid #CFCFCF;wdith:6%;text-align:center;margin: 5px 5px 5px 10px;padding:0 5px;">
			<a href="#" onclick="asignatedAutoeval('less');return false;">Quitar</a>
		</div>
	</div>
	<div>
		<select name="idAutoevaluacionQuitar[]" multiple="multiple" style="width:45%;height:500px;float:right;border:1px solid #CCC">
			<?php if(count($autoevaluacionesAsignadas) > 0): ?>
				<?php foreach($autoevaluacionesAsignadas as $key => $value): ?>
					<option value="<?php echo $key ?>"><?php echo $value; ?></option>
				<?php endforeach; ?>
			<?php else: ?>
					<option value=""></option>	
			<?php endif;?>
		</select>
	</div>

	<input type="hidden" name="actionAutoeval" value="" />
	<?php if(isset($post['idTematica'])): ?>
		<input type="hidden" name="idTematica" value="<?php echo $post['idTematica'] ?>" />
	<?php endif; ?>

</form>

<script type="text/javascript" src="js-contenidos-asignar_autoevaluacion.js"></script>