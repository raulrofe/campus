<div class="block">
	<div class="block-title"><h5>INTRODUCIR TEMA DE FORO</h5></div>
	<div class="block-content full">
		<form class="form-inline" name='frm_modulo' action='contenidos/foros/insertar' method='post' enctype='multipart/form-data' id="frmNuevoForo" >
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;tulo del tema</div>
						<div class='campofrm'><input id="foroNuevotema_titulo" type="text" name="titulo" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Prioridad</div>
						<div class='campofrm'><input id="foroNuevotema_prioridad" type="text" name="prioridad" /></div>
					</div> 
					<div class="clear"></div>
				</div>
			</div>
			
			<input type="hidden" name="idModulo" value="<?php echo $get['idModulo'] ?>" />
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>
	
<script type="text/javascript" src="js-contenidos-nuevo_contenidosinscorm.js"></script>

	<?php if($temas->num_rows > 0): ?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th class="tableThCustom">Tema foro</th>    
	                <th class="tableThCustom span1 text-centers">Prioridad</th>
	                <th class="tableThCustom span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
			<?php while ($tema = mysqli_fetch_object($temas)): ?>	
				<tr>
					<td><?php echo $tema->titulo; ?></td>
					<td class="span1 text-center"><?php echo $tema->prioridad; ?></td>
		            <td class="span1 text-center">
		            	<div class="btn-group">
		            		<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="contenidos/foros/editar/<?php echo $tema->idforo_temas ?>" data-original-title="Editar"><i class="icon-edit"></i></a>
		                	<a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/foros/eliminar/<?php echo $tema->idforo_temas ?>" data-original-title="Eliminar"><i class="icon-remove"></i></a>
		                </div>
		            </td>
				</tr>
			<?php endwhile; ?>
		</table>
	<?php else: ?>
		<div class="text-center">No existen temas de foro para este m&oacute;dulo</div>
	<?php endif;?>
