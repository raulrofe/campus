<form class="form-inline" name='frm_curso' action='contenidos/editar/<?php echo $get['idModulo']; ?>' method='post' enctype="multipart/form-data" id="frmNuevoContenido">
	<!-- div.row-fluid -->
    <div class="row-fluid">
    	<div>      
	    	<!-- Column -->
	        <div class="span6">
	        	<div class="control-group">
	            	<label for="columns-text" class="control-label">Nombre m&oacute;dulo</label>
                    <div class="controls"><input type='text' name='atitulo' size='60'  value='<?php echo $f['nombre'];?>' /></div>
                </div>
			</div>

			<!-- Column -->
            <div class="span6">
	        	<div class="control-group">
	            	<label for="columns-text" class="control-label">N&uacute;mero asignado</label>
	                <div class="controls"><input type='text' name='anmodulo' size='60' maxlength='3' value='<?php echo $f['referencia_modulo'];?>' /></div>
	            </div>
		    </div>	
		                
		    <div class="clear"></div>
		 </div>
         
         <!-- Column -->
         <div>
	     	<label>Descripción</label>
			<div class="controls"><textarea name='adescripcion' class="AreaFullWidth"><?php echo $f['descripcion'];?></textarea></div>
	     </div>	                                
                
         <!-- Column -->
         <div>
	     	<label>Contenidos</label>
            <div class="controls"><textarea name='acontenidos' class="AreaFullWidth"><?php echo $f['contenidos'];?></textarea></div>
	     </div>
	                
         <!-- Column -->
         <div>
	     	<label>Objetivos</label>
            <div class="controls"><textarea name='aobjetivos' class="AreaFullWidth"><?php echo $f['objetivos'];?></textarea></div>
	     </div>	                	                	                
    </div>
                            
    <!-- END div.row-fluid -->
    <div><button class="btn btnFull btn-success" type="submit"><i class="icon-save"></i> GUARDAR</button></div>
</form>
