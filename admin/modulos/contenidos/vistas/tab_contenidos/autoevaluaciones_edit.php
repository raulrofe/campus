<div class="block">
	<div class="block-title"><h5>ACTUALIZAR AUTOEVALUACI&Oacute;N: <?php echo $autoevaluacionRow->PTtitulo_autoeval ?></h5></div>
	<div class="block-content full">
		<form class="form-inline" name='frm_modulo' action='contenidos/autoevaluacion/editar/<?php echo $autoevaluacionRow->PTidautoeval ?>' method='post' enctype='multipart/form-data' id="frmNuevoContenidoAutoevaluacion" >
			<div class="row-fluid">
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>T&iacute;tulo</div>
						<div class='campofrm'><input type='text' name='titulo' value="<?php echo $autoevaluacionRow->PTtitulo_autoeval ?>" /></div>
					</div>
					<div class='row-fluid-inline2'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><input type='text' name='descripcion' value="<?php echo $autoevaluacionRow->PTdescripcion ?>" /></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>N&uacute;mero de intentos</div>
						<div class='campofrm'><input type='text' name='numeroIntentos' size='1' value="<?php echo $autoevaluacionRow->PTn_intentos ?>" /></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Nota m&iacute;nima</div>
						<div class='campofrm'><input id="file" type='text' name='notaMinima' size='60' value="<?php echo $autoevaluacionRow->PTnota_minima ?>" /></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Penalizaci&oacute;n respuesta en blanco</div>
						<div class='campofrm'><input type='text' name='penalizacionBlanco' size='1' value="<?php echo $autoevaluacionRow->PTpenalizacion_blanco ?>" /></div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Penalizaci&oacute;n respuesta no contestada</div>
						<div class='campofrm'><input id="file" type='text' name='penalizacionNoContesta' size='60' value="<?php echo $autoevaluacionRow->PTpenalizacion_nc ?>" /></div>
					</div>
					<div class="clear"></div>
				</div>


				<?php
					$limitTime = true;
					$time = explode(':', $autoevaluacionRow->PTtiempo);
					if($time[0] == '00' && $time[1] == '00') 
					{
						$limitTime = false;
					}
				?>

				<div class="row-form-content-inline">
					<div class='row-fluid-inline1'>
						<div class='etiquetafrm'>Tiempo realizaci&oacute;n</div>
						<div class='campofrm'><input id="limitTime" type="checkbox" name="tiempoRealizacion" value="1" style="margin-right:5px;" <?php if(!$limitTime) echo "checked" ?>/><label for="limitTime">Sin l&iacute;mite de tiempo</label></div>
						<div class='campofrm'>
							<label>Horas</label>
							<select name="horas" style="width:13%;margin-right:10px;">
								<option value="00" <?php if($time[0] == '00') echo "selected" ?>>00</option>
								<option value="01" <?php if($time[0] == '01') echo "selected" ?>>01</option>
								<option value="02" <?php if($time[0] == '02') echo "selected" ?>>02</option>
								<option value="03" <?php if($time[0] == '03') echo "selected" ?>>03</option>
								<option value="04" <?php if($time[0] == '04') echo "selected" ?>>04</option>
								<option value="05" <?php if($time[0] == '05') echo "selected" ?>>05</option>
								<option value="06" <?php if($time[0] == '06') echo "selected" ?>>06</option>
								<option value="07" <?php if($time[0] == '07') echo "selected" ?>>07</option>
								<option value="08" <?php if($time[0] == '08') echo "selected" ?>>08</option>
								<option value="09" <?php if($time[0] == '09') echo "selected" ?>>09</option>
								<option value="10" <?php if($time[0] == '10') echo "selected" ?>>10</option>
								<option value="11" <?php if($time[0] == '11') echo "selected" ?>>11</option>
								<option value="12" <?php if($time[0] == '12') echo "selected" ?>>12</option>
								<option value="13" <?php if($time[0] == '13') echo "selected" ?>>13</option>
								<option value="14" <?php if($time[0] == '14') echo "selected" ?>>14</option>
								<option value="15" <?php if($time[0] == '15') echo "selected" ?>>15</option>
								<option value="16" <?php if($time[0] == '16') echo "selected" ?>>16</option>
								<option value="17" <?php if($time[0] == '17') echo "selected" ?>>17</option>
								<option value="18" <?php if($time[0] == '18') echo "selected" ?>>18</option>
								<option value="19" <?php if($time[0] == '19') echo "selected" ?>>19</option>
								<option value="20" <?php if($time[0] == '20') echo "selected" ?>>20</option>
								<option value="21" <?php if($time[0] == '21') echo "selected" ?>>21</option>
								<option value="22" <?php if($time[0] == '22') echo "selected" ?>>22</option>
								<option value="23" <?php if($time[0] == '23') echo "selected" ?>>23</option>
							</select>
							<label>Minutos</label>
							<select name="minutos" style="width:13%">
								<option value="00" <?php if($time[1] == '00') echo "selected" ?>>00</option>
								<option value="15" <?php if($time[1] == '15') echo "selected" ?>>15</option>
								<option value="30" <?php if($time[1] == '30') echo "selected" ?>>30</option>
								<option value="45" <?php if($time[1] == '45') echo "selected" ?>>45</option>
							</select>
						</div>
					</div>
					<div class='row-fluid-inline2 control-group'>
						<div class='etiquetafrm'>Modo de correcci&oacute;n</div>
						<div class='campofrm'><input type="radio" value="0" name="modoCorreccion" style="margin-right:5px;" <?php if($autoevaluacionRow->PTcorreccion == 0) echo "checked" ?> /><span style="vertical-align:middle">Mostrar s&oacute;lo resultado final</span></div>
						<div class='campofrm'><input type="radio" value="1" name="modoCorreccion" style="margin-right:5px;" <?php if($autoevaluacionRow->PTcorreccion == 1) echo "checked" ?> /><span style="vertical-align:middle">Mostrar "CORRECTA" o "INCORRECTA" y el resultado final</span></div>
						<div class='campofrm'><input type="radio" value="2" name="modoCorreccion" style="margin-right:5px;" <?php if($autoevaluacionRow->PTcorreccion == 2) echo "checked" ?> /><span style="vertical-align:middle">Mostrar "CORRECTA" o "INCORRECTA", resaltar item correcto y resultado final</span></div>
					</div>
					<div class="clear"></div>
				</div>

			</div>
			
			<input type="hidden" name="idModulo" value="<?php echo $autoevaluacionRow->PTidmodulo ?>" />
			
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-save></i> Guardar</button></div>
		</form>
	</div>
</div>