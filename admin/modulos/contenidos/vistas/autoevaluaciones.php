<div class="submenuAdmin">AUTOEVALUACIONES</div>
<div class='addAdmin'><a href='contenidos/autoevaluaciones/insertar'>Introducir autoevaluacion</a></div>
<!-- Listado de Convocatorias -->
	<div class='buscador'>
		<P class='t_center'>Seleccione una Tematica para poder ver las Autoevaluaciones introducidas en este momento en el sistema:</P><br/>
			<form name='busca_tipo_curso' action='contenidos/autoevaluaciones' method='post'>
				<div class='t_center'>
					<select name='idTematica' onchange='this.form.submit();'>
						<option value='0'> - Tem&aacute;tica com&uacute;n - </option>
						<?php while ($tematica = mysqli_fetch_assoc($tematicas)):?>
							<?php if($tematica['idtematica_autoeval'] == $post['idTematica']):?>
								<option value='<?php echo $tematica['idtematica_autoeval']; ?>' selected ><?php echo $tematica['tematica_autoeval']; ?></option>
							<?php else:?>
								<option value='<?php echo $tematica['idtematica_autoeval']; ?>'><?php echo $tematica['tematica_autoeval']; ?></option>
							<?php endif; ?>
						<?php endwhile;?>
					</select>
				</div>
			</form>
			<br/>
			<form name='busca_tipo_curso' action='contenidos/autoevaluaciones' method='post'>
				<div class='t_center'>
					<select name='idAutoevaluacion'>
						<option value=''> - Selecciona una autoevaluaci&oacute;n - </option>
						<?php while ($autoevaluacion = mysqli_fetch_assoc($autoevaluaciones)):?>
							<?php if($autoevaluacion['PTidautoeval'] == $post['idAutoevaluacion']):?>
								<option value='<?php echo $autoevaluacion['PTidautoeval']; ?>' selected ><?php echo $autoevaluacion['PTtitulo_autoeval']; ?></option>
							<?php else:?>
								<option value='<?php echo $autoevaluacion['PTidautoeval']; ?>'><?php echo $autoevaluacion['PTtitulo_autoeval']; ?></option>
							<?php endif; ?>
						<?php endwhile;?>
					</select>
					<br/><br/>
					<div class='t_center'>
						<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
						<span id="frmBtnBorrar2" class='boton_borrar'><input type='submit' name='boton' value='Borrar' /></span>
					</div>
					<div class='t_center' style='margin-top:5px;'>
						<span class='boton_borrar'><input type='submit' name='boton' value='Preguntas y respuestas' /></span>
						<span class='boton_borrar'><input type='submit' name='boton' value='Plantilla preliminar' /></span>
					</div>
					<input type='hidden' name='idTematica' value='<?php echo $idTematica; ?>' />
				</div>
			</form>
	</div>
<!-- fin Listado de convocatorias -->

<?php 

	// Opciones de carga de archivos para las autoevaluaciones
	if (isset($get['boton']) && $get['boton'] == 'Insertar')
	{
		require_once('nueva_autoevaluacion.php');
	}

	else if(isset ($post['boton']) && $post['boton'] == 'Actualizar')
	{
		require("editar_autoevaluacion.php");
	}	
	
	else if (isset($post['boton'], $preguntas) && $post['boton'] == 'Plantilla preliminar')
	{
		require("vista_preliminar.php");
	}
	
	// Opciones de carga de archivos para las  pereguntas y respuestas
	else if(isset ($post['boton'], $post['idAutoevaluacion']) && $post['boton'] == 'Preguntas y respuestas' && is_numeric($post['idAutoevaluacion']))
	{
		require("pyr_autoevaluacion.php");
	}
	
	else if(isset ($get['boton']) && $get['boton'] == 'editarPregunta')
	{
		require("editarpregunta_autoevaluacion.php");
	}
?>

<script type="text/javascript">alertConfirmBtn('¿Estas seguro que deseas borrarla?');</script>
<script type="text/javascript">alertConfirmBtn('¿Estas seguro que deseas borrarla?', 'frmBtnBorrar2');</script>
