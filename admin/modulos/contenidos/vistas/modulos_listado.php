<div id="modulesList">
	<?php if($registros->num_rows > 0):?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th class="text-center">Referencia</th>
	                <th>Nombre</th>
	                <th class="span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
	  		<?php while($modulo = $registros->fetch_object()):?>
	        	<tr>
                	<td class="text-center"><?php echo $modulo->referencia_modulo ?></td>
	                <td><a href="contenidos/editar/<?php echo $modulo->idmodulo ?>"><?php echo $modulo->nombre ?></a></td>
	                <td class="span1 text-center">
	                	<div class="btn-group">
	                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="contenidos/editar/<?php echo $modulo->idmodulo ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
	                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/eliminar/<?php echo $modulo->idmodulo ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
	                    </div>
	                </td>
	            </tr>
	  		<?php endwhile; ?>
	  	</table> 
	<?php else: ?>
		<div class="text-center">No existen m&oacute;dulos</div>
	<?php endif;?>
</div>
