<p class='subtitleAdmin'>ACTUALIZAR DATOS AUTOEVALUACI&Oacute;N</p>
<div class='formulariosAdmin'>
	<div class='cajafrm'>
		<form method='post' action='contenidos/autoevaluaciones' id='frmNuevoAutoevaluacion'>
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo autoevaluaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='titulo' size='60' value='<?php echo Texto::textoPlano($fila['PTtitulo_autoeval']); ?>' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tem&aacute;tica autoevaluaci&oacute;n</div>
				<div class='campofrm'>
					<select name='idtematica'>
					<option value='0'>Tem&aacute;tica com&uacute;n</option>
					<?php 
						while($f = mysqli_fetch_assoc($tematicas2))
						{
							if( $f['idtematica_autoeval'] == $fila['PTidtematica_autoeval']){echo "<option value='".$f['idtematica_autoeval']."' selected>".$f['tematica_autoeval']."</option>";}
							else echo "<option value='".$f['idtematica_autoeval']."'>".$f['tematica_autoeval']."</option>";
						}
					?>
					</select>
				</div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tipo autoevaluaci&oacute;n</div>
				<div class='campofrm'>
					<select name='idtipo_autoeval'>
						<option value='1' <?php if($fila['PTidtipo_autoeval'] == 1){echo 'selected';}?>>Test</option>
						<option value='2' <?php if($fila['PTidtipo_autoeval'] == 2){echo 'selected';}?>>Desarrollo</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Descripci&oacute;n</div>
				<div class='campofrm'>
					<textarea name='descripcion' class='estilotextarea2' ><?php echo $fila['PTdescripcion']; ?></textarea>
				</div>
			</div>
			<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>
			</div>
			<input type='hidden' name='boton' value='Actualizar' />
			<input type='hidden' name='idAutoevaluacion' value='<?php echo $post['idAutoevaluacion'];?>' />
			<input type='submit' value='Actualizar' />
		</form>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-actualizar_autoevaluacion.js"></script>