<div class="subtitleModulo t_center">VISTA PRELIMINAR</div>
<!-- RESUMEN DE LAS PREGUNTAS INSERTADAS CON SUS RESPUESTAS -->
	<div id="vistaPreliminar">
		<?php 
		$cont=1;
		if(mysqli_num_rows($preguntas) > 0)
		{
			while($row = mysqli_fetch_assoc($preguntas))
			{	
				echo "<div class='burbuja' style='margin:7px 0;padding:0;'>";
				$numLetra = 1;
				echo "<div class='pregunta' style='color:#000;'>".$cont." - ".$row['PTpregunta']."</div>";
				$respuestas = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($row['PTidpreguntas']);
				while($respuesta = mysqli_fetch_assoc($respuestas))
				{
					if($respuesta['PTcorrecta'] == 1)
					{
						echo "<div class='respuesta' style='color:green'>";
					}
					else
					{
						echo "<div class='respuesta'>";
					}
					$letra = AutoevaluacionAdmin::obtener_letra($numLetra);
					echo $letra.$respuesta['PTrespuesta'];
					
					echo "</div>";
					$numLetra++;
				}
				$cont++;
				echo "</div>";
			}
		}
		else
		{
			echo "No existen preguntas";
		}
		?>
	</div>
<!-- FIN DE RESUMEN -->
<div class='t_center'>
	<input type="button" onclick="imprimir('vistaPreliminar'); return false;" value="Imprimir" />
</div>