<h2>Asignar centros con RLT a Acciones Formativas</h2><br/>
<form name='afrlt' method='post' action=''>
	<div class='subtitle'>Convocatorias</div>
	<select name='idConvocatoria' onchange='this.form.submit()'>
		<option value=''>Seleccion una Convocatoria</option>
		<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
			<option value='<?php echo $convocatoria['idconvocatoria']?>' <?php if(isset ($post['idConvocatoria']) && $convocatoria['idconvocatoria'] == $post['idConvocatoria']) echo 'selected';?>><?php echo $convocatoria['nombre_convocatoria']?></option>
		<?php endwhile;?>
	</select>
</form>
	<?php if(!empty ($post['idConvocatoria'])):?>
		<form name='afrl2' method='post' action=''>
		<div class='subtitle'>Acciones Formativas</div>
		<select name='idAF'>
			<option value=''>Seleccion una Acci&oacute;n Formativa</option>
			<?php while($accionFormativa = mysqli_fetch_assoc($accionesFormativas)):?>
				<option value ='<?php echo $accionFormativa['idaccion_formativa']?>' <?php if(isset ($post['idAF']) && $accionFormativa['idaccion_formativa'] == $post['idAF']) echo 'selected';?>><?php echo $accionFormativa['accion_formativa']?></option>
			<?php endwhile;?>
		</select>
		<div class='clear'></div>
		<br/>
		<div class='subtitle fleft'>Centros con RLT</div>
		<div class='fleft' style='margin:7px 0 0 35px;'><input id="scormSearch" type="text" /></div>
		<div class='clear'></div>
		<br/>
		
		<?php if($nCentros > 0){?>
			<div id='ListCenter'>
			<?php if($nCentros <= 20):?>
				<?php for($i=0;$i<$nCentros;$i++):?>
					<div class='rlt'><input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$i]?>' value='<?php echo $regCentro['id'][$i]?>' /><label for='centro_<?php echo $regCentro['id'][$i]?>' class='lbCheckbox' ><?php echo $regCentro['cif'][$i]?></label></div>
				<?php endfor;?>
			<?php endif;?>
		
			<?php 
			if($nCentros > 20):
			$nRegistro=0;
				for($i=1;$i<=$columnaCentros;$i++):?>
					<div class='fleft' style='margin-bottom:15px;'>
						<?php for($j=0;$j<$filaCentros-1;$j++):?>
							<div class='rlt'>
								<input type='checkbox' name='centrosRLT[]' id='centro_<?php echo $regCentro['id'][$nRegistro]?>' value='<?php echo $regCentro['id'][$nRegistro]?>' />
								<label for='centro_<?php echo $regCentro['id'][$nRegistro]?>' class='lbCheckbox' ><?php echo $regCentro['cif'][$nRegistro]?></label>
							</div>
						<?php 
						$nRegistro++;
						endfor;
						?>
					</div>
				<?php endfor;?>
			<?php endif;?>
			</div>
		<?php 
		}
		
		else echo "<div class='ListCenter'> No existen Centros con Representante Legal (RLT), para esta convocatoria</div><br/>";
		
		?>
	
		<div class='clear'></div>
		<input type='submit' value='Enviar' />
		<input type='hidden' name='idConvocatoria' value='<?php echo $post['idConvocatoria'];?>' />
		</form>
	<?php endif;?>