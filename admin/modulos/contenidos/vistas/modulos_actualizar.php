<div class="block block-tabs block-themed">
	<ul data-toggle="tabs" class="nav nav-tabs">
    	<li class=<?php if(empty($get['tab']) || (isset($get['tab']) && $get['tab'] == 'modulo')) echo 'active' ?>><a href="#example-tabs4-modulo">Datos m&oacute;dulo</a></li>
        <li class="<?php if(isset($get['tab']) && $get['tab'] == 'temario') echo 'active' ?>"><a href="#example-tabs4-temario">Contenido sin SCORM</a></li>
        <li class="<?php if(isset($get['tab']) && $get['tab'] == 'scorm') echo 'active' ?>"><a title="" href="#example-tabs4-scorm">Contenido SCORM</a></li>
        <li class="<?php if(isset($get['tab']) && $get['tab'] == 'autoevaluacion') echo 'active' ?>"><a title="" href="#example-tabs4-autoevaluacion">Autoevaluaciones</a></li>
        <li class="<?php if(isset($get['tab']) && $get['tab'] == 'tp') echo 'active' ?>"><a title="" href="#example-tabs4-trabajos">Trabajos pr&aacute;cticos</a></li>
        <li class="<?php if(isset($get['tab']) && $get['tab'] == 'foro') echo 'active' ?>"><a title="" href="#example-tabs4-foros">Foros</a></li>
    </ul>
    <div class="tab-content">
    	<div id="example-tabs4-modulo" class="tab-pane <?php if(empty($get['tab']) || (isset($get['tab']) && $get['tab'] == 'modulo')) echo 'active' ?>"><?php require_once 'tab_contenidos/datos_modulo.php'; ?></div>
        <div id="example-tabs4-temario" class="tab-pane <?php if(isset($get['tab']) && $get['tab'] == 'temario') echo 'active' ?>"><?php require_once 'tab_contenidos/temario.php'; ?></div>
        <div id="example-tabs4-scorm" class="tab-pane <?php if(isset($get['tab']) && $get['tab'] == 'scorm') echo 'active' ?>"><?php require_once 'tab_contenidos/scorm.php'; ?></div>
        <div id="example-tabs4-autoevaluacion" class="tab-pane <?php if(isset($get['tab']) && $get['tab'] == 'autoevaluacion') echo 'active' ?>">
            <?php if(isset($autoevaluacionRow)): ?>
                <?php require_once 'tab_contenidos/edit_autoevaluacion.php'; ?>
            <?php else: ?>
                <?php require_once 'tab_contenidos/autoevaluacion.php'; ?>
            <?php endif; ?>
        </div>
        <div id="example-tabs4-trabajos" class="tab-pane <?php if(isset($get['tab']) && $get['tab'] == 'tp') echo 'active' ?>"><?php require_once 'tab_contenidos/trabajos_practicos.php'; ?></div>
        <div id="example-tabs4-foros" class="tab-pane <?php if(isset($get['tab']) && $get['tab'] == 'foro') echo 'active' ?>"><?php require_once 'tab_contenidos/foro.php'; ?></div>
	</div>
</div>

<script type="text/javascript" src="js-contenidos-actualizar_modulo.js"></script>
