<div class="submenuAdmin">CONTENIDO SIN SCORM</div>

<div class='addAdmin'><a href='contenidos/ficheros/insertar'>Introducir Contenido sin SCORM</a></div>

<!-- Listado de Modulos -->
	<div class='buscador'>
		<P class='t_center'>M&oacute;dulos introducidos en este momento en el sistema:</P><br/>
			<form name='busca_tipo_curso' action='contenidos/ficheros' method='post'>
				<div class='t_center'>
					<?php $mi_modulo->select_modulos_noauto($registros);?>
					&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
				<br/>
				<div class='t_center'>
					<span class='boton_borrar'><input type='submit' name='boton' value='Ver contenido' /></span>
				</div>
			</form>
	</div>
<!-- fin buscador fijo -->


<?php 
	if(isset($get['boton']) && $get['boton'] == "Insertar")
	{
		require('nuevo_contenidosinscorm.php');
	}
?>
<?php if(isset($post['boton']) && $post['boton'] == "Ver contenido" && $post['idmodulo'] != 0):?>			
<!-- archivos contenido sin scorm -->
	<div class='view'>
		<div class='header'>
			<div class='itemg1'>Titulo</div>
			<div class='itemp'>Prioridad</div>
			<div class='itemp'>Tama&ntilde;o</div>
			<div class='itemp'>Editar</div>
			<div class='itemp'>Eliminar</div>
			<div class='itemp'>Fecha</div>
		</div>
		<div class='boder'>
			<?php 
				$peso_total = 0;
				if($n_archivos > 0):
				while ($f = mysqli_fetch_assoc($resultado)):
					$peso_archivo = filesize("../".$f['enlace_archivo']);
					$peso_archivo = $peso_archivo/1024;
					$peso_archivo = $peso_archivo/1024;
					$peso_archivo = round($peso_archivo,2);
					$peso_total = $peso_total + $peso_archivo;
			?>
			<div class='bod_cont'>
				<div class='itemg1'><a href='<?php echo "../".$f['enlace_archivo'];?>' target='_blank'><?php echo $f['titulo']?></a></div>
				<div class='itemp'><?php echo $f['prioridad']?></div>
				<div class='itemp'><?php echo $peso_archivo;?> Mb</div>
				<div class='itemp'><a href='contenidos/ficheros/actualizar/<?php echo $f['idarchivo_temario'];?>/<?php echo $idmodulo;?>'><img src='imagenes/editar2.png' alt=''/></a></div>
				<div class='itemp'>
					<a href='#' <?php echo Alerta::alertConfirmOnClick('borrarsinscorm', '¿Estas seguro que desea borrar el contenido sin scorm?', 'contenidos/ficheros/eliminar/' . $f['idarchivo_temario'] . '/' . $idmodulo )?>>
						<img src='imagenes/delete2.png' alt=''/>
					</a>
				</div>
				<div class='itemp'><?php echo $f['f_creacion'] = Fecha::invertir_fecha($f['f_creacion'],'-','/');?></div>	
			</div>
			<?php 
				endwhile;
			else:
				echo "No existen archivos";
			endif;	
			?>		
		</div>
		<div class='foot'>
			<p><?php echo $n_archivos?> Archivos | <?php echo $peso_total;?> Mbytes</p>
		</div>
	</div>	
<?php endif;?>

<?php 
if(isset($get['idarch']) && is_numeric($get['idarch']))
{
	require("actualizar_fichero.php");
}
?>
