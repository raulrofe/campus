	<div class='subtitleModulo'>Editar Pregunta y Respuestas</div>
	<div class='cajafrm'>
		<form method='post' action='contenidos/autoevaluaciones/editar-pregunta/<?php echo $get['idpregunta']; ?>' enctype='multipart/form-data'>
			<div class='filafrm'>
				<div class='etiquetafrm'>Enunciado pregunta</div>
				<div class='campofrm'><textarea name='enunciado' class='estilotextarea2'><?php echo Texto::textoPlano($pre['PTpregunta']);?></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Im&aacute;gen</div>
				<div class='campofrm'>
				<?php if(!empty ($pre['PTimagen_pregunta'])) echo "<img src='".$pre['PTimagen_pregunta']."' alt='' width='150px' class='fleft'/><br/>";?>
				<input type='file' name='imagen_pregunta' size='50'/></div>
			</div>
	<div class='subtitleModulo'>Editar Respuestas</div>
			<?php 
			$cont = 1;
			if(mysqli_num_rows($registros_respuestas) >0)
			{
				while($res = mysqli_fetch_assoc($registros_respuestas))
				{
				?>
					<div class='filafrm'>
						<div class='etiquetafrm'>Respuesta <?php echo $cont;?></div>
						<div class='campofrm'><input type='text' name='respuesta[]' size='60' value='<?php echo Texto::textoPlano($res['PTrespuesta']); ?>' /></div>
						<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='<?php echo $cont;?>' <?php if ($res['PTcorrecta'] == '1') echo 'checked';?>/></div>
					</div>
					<div class='filafrm'> 
						<div class='etiquetafrm'>Im&aacute;gen</div>
						<div class='campofrm'>
							<?php if(!empty ($res['PTimagen_respuesta'])):?>
								<?php echo "<img src='".$res['PTimagen_respuesta']."' alt='' width='150px' class='fleft'/><br/>";?>
								<a href="#" <?php echo Alerta::alertConfirmOnClick('eliminarimagen','¿Realmente quieres eliminar esta imagen?', 'autoevaluaciones/pregunta/imagen/eliminar/' . $res['PTidrespuesta'])?>>Eliminar</a>
							<?php endif;?>
							<input type='file' name='imagen_respuesta_<?php echo $cont;?>' size='50'/>
						</div>
					</div>
					<br/>
				<?php 
				$cont++;
				echo "<input type='hidden' name='idrespuesta[]' value='".$res['PTidrespuesta']."' />";
				}
			}
			else
			{
			?>
				<div class='filafrm'>
					<div class='etiquetafrm'>Respuesta 1</div>
					<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
					<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='1' /></div>
				</div>
				<div class='filafrm'> 
					<div class='etiquetafrm'>Im&aacute;gen</div>
					<div class='campofrm'>
					<input type='file' name='imagen_respuesta_1' size='50' /></div>
				</div>
					
				<div class='filafrm'>
					<div class='etiquetafrm'>Respuesta 2</div>
					<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
					<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='2' /></div>
				</div>
				<div class='filafrm'> 
					<div class='etiquetafrm'>Im&aacute;gen</div>
					<div class='campofrm'>
					<input type='file' name='imagen_respuesta_2' size='50' /></div>
				</div>
					
				<div class='filafrm'>
					<div class='etiquetafrm'>Respuesta 3</div>
					<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
					<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='3' /></div>
				</div>
				<div class='filafrm'> 
					<div class='etiquetafrm'>Im&aacute;gen</div>
					<div class='campofrm'>
					<input type='file' name='imagen_respuesta_3' size='50' /></div>
				</div>
					
				<div class='filafrm'>
					<div class='etiquetafrm'>Respuesta 4</div>
					<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
					<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='4' /></div>
				</div>
				<div class='filafrm'> 
					<div class='etiquetafrm'>Im&aacute;gen</div>
					<div class='campofrm'>
					<input type='file' name='imagen_respuesta_4' size='50' /></div>
				</div>
				<input type='hidden' name='idPregunta' value='<?php echo $pre['PTidpreguntas'];?>' />
			<?php 
			}
			?>
		<input type='submit' value='modificar' />
		<input type='hidden' name='boton' value='editarPregunta' />
		</form>
	</div>
