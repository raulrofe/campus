<div class="block block-themed themed-default">
	<div class="block-title"><h5>BUSCADOR AUTOEVALUACIONES</h5></div>
	<div class="block-content full">
		<form method='post' action='contenidos/autoevaluaciones' name='buscar_autoevaluacion' id='frmBuscarAutoevaluacion' class="form-inline" >
        	<!-- div.row-fluid -->
            <div class="row-fluid">         
            	<!-- 1st Column -->
	            <div class="span6">
	            	<div class="control-group">
	                	<label for="columns-text" class="control-label">Tem&aacute;tica</label>
                        <div class="controls">
                        	<select name="idTematica">
                        			<option value=""></option>
                        		<?php while($tematica = $tematicas->fetch_object()): ?>
                        			<option value="<?php echo $tematica->idtematica_autoeval ?>"><?php echo $tematica->tematica_autoeval ?></option>
                        		<?php endwhile ?>
                        	</select>
                        </div>
                    </div>
				</div>
	            <div class="span6">
	            	<div class="control-group">
	                	<label for="columns-text" class="control-label">T&iacute;tulo autoevaluaci&oacute;n</label>
                        <div class="controls"><input type='text' name='titulo' /></div>
                    </div>
				</div>
			</div>
			<div><button class="btn btnFull btn-success" type="submit"><i class="icon-search"></i> Buscar</button></div>
		</form>
	</div>
</div>

<?php if(isset($autoevluaciones) && $autoevluaciones->num_rows > 0): ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>T&iacute;tulo</th>
				<th>N&ordm; preguntas</th>
				<th>Tem&aacute;tica</th>
				<th>n&ordm; intentos</th>
				<th class="span1 text-center"><i class="icon-bolt"></i></th>
			</tr>
		</thead>
		<?php while($autoevaluacion = $autoevluaciones->fetch_object()): ?>
			<tr>
				<td><?php echo $autoevaluacion->PTtitulo_autoeval ?></td>
				<td><?php echo $objAutoevaluacionAdmin->preguntasPlantilla($autoevaluacion->PTidautoeval)->num_rows; ?></td>
				<td><?php echo $autoevaluacion->tematica_autoeval ?></td>
				<td><?php echo $autoevaluacion->PTn_intentos ?></td>
				<td class="span1 text-center">
		          	<div class="btn-group">
		          		<a class="btn btn-mini" title="" data-toggle="tooltip" href="contenidos/autoevaluaciones/preguntar/<?php echo $autoevaluacion->PTidautoeval; ?>" data-original-title="Preguntas y Respuestas" style="margin-right:5px;"><i class="icon-edit"></i></a>
		    	      	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="contenidos/autoevaluaciones/editar/<?php echo $autoevaluacion->PTidautoeval; ?>" data-original-title="Editar"><i class="icon-pencil"></i></a>
		                <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="contenidos/autoevaluaciones/eliminar/<?php echo $autoevaluacion->PTidautoeval; ?>" onclick="alertConfirmBtn('¿Quieres eliminar a este temática?>?');return false;" data-original-title="Eliminar"><i class="icon-remove"></i></a>
		            </div>
          		</td>
			</tr>
		<?php endwhile; ?>
	</table>
<?php endif; ?>