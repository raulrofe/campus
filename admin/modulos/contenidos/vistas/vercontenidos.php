<!-- archivos contenido sin scorm -->
	<div class='view'>
		<div class='header'>
			<div class='itemg1'>Titulo</div>
			<div class='itemp'>Prioridad</div>
			<div class='itemp'>Tama&ntilde;o</div>
			<div class='itemp'>Editar</div>
			<div class='itemp'>Eliminar</div>
			<div class='itemp'>Fecha</div>
		</div>
		<div class='boder'>
			<?php 
				$peso_total = 0;
				if($n_archivos > 0):
				while ($f = mysqli_fetch_assoc($resultado)):
					$peso_archivo = filesize("../".$f['enlace_archivo']);
					$peso_archivo = $peso_archivo/1024;
					$peso_archivo = $peso_archivo/1024;
					$peso_archivo = round($peso_archivo,2);
					$peso_total = $peso_total + $peso_archivo;
			?>
			<div class='bod_cont'>
				<div class='itemg1'><a href='<?php echo "../".$f['enlace_archivo'];?>' target='_blank'><?php echo $f['titulo']?></a></div>
				<div class='itemp'><?php echo $f['prioridad']?></div>
				<div class='itemp'><?php echo $peso_archivo;?> Mb</div>
				<div class='itemp'>
					<a href='contenidos/trabajos_practicos/actualizar/<?php echo $f['idarchivo_temario'];?>/<?php echo $idmodulo;?>'>
						<img src='imagenes/editar2.png' alt=''/>
					</a>
				</div>
				<div class='itemp'>
					<a href='#' <?php echo Alerta::alertConfirmOnClick('borrarpractico' ,'¿Estas seguro que desea borrar el trabajo pr&áctico?', 'contenidos/trabajos_practicos/eliminar/' . $f['idarchivo_temario'] )?>>
						<img src='imagenes/delete2.png' alt=''/>
					</a>
				</div>	
				<div class='itemp'><?php echo $f['f_creacion'] = Fecha::invertir_fecha($f['f_creacion'],'-','/');?></div>
			</div>
			<?php 
				endwhile;
			else:
				echo "No existen archivos";
			endif;	
			?>		
		</div>
		<div class='foot'>
			<p><?php echo $n_archivos?> Archivos | <?php echo $peso_total;?> Mbytes</p>
		</div>
	</div>	
