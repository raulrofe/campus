<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Contenidos</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas -->	  			
	  	<div class="tab">
			<div class="submenuAdmin">CONTENIDO CON SCORM</div>
			<div class='addAdmin'><a href='contenidos/multimedia/insertar'>Introducir Contenido con SCORM</a></div>
			
			<!-- Listado de Modulos -->
				<div class='buscador'>
					<P class='t_center'>M&oacute;dulos introducidos en este momento en el sistema:</P><br/>
						<form name='busca_tipo_curso' action='contenidos/multimedia' method='post'>
							<div class='t_center'>
								<select name='idmodulo'>
									<option value=''> - Seleccione M&oacute;dulo - </option>
									<?php while($f = mysqli_fetch_assoc($modulos)):?>
										<?php if(isset($idmodulo) && $f['idmodulo'] == $idmodulo):?>
											<option value='<?php echo $f['idmodulo'] ?>' selected><?php echo $f['referencia_modulo'] ." - " . $f['nombre'] ?></option>
										<?php else:?>
											<option value='<?php echo $f['idmodulo'] ?>'><?php echo $f['referencia_modulo'] ." - " . $f['nombre'] ?></option>
										<?php endif;?>
									<?php endwhile;?>
								</select>
								&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
							<br/>
							<div class='t_center'>
								<span class='boton_borrar'><input type='submit' name='boton' value='Ver contenidos' /></span>
							</div>
						</form>
				</div>
			<!-- fin buscador fijo -->
			<?php if(isset ($get['boton']) && $get['boton'] == 'Editar' && isset($rowScorm)):?>
				<p class='subtitleAdmin'>INTRODUCIR CONTENIDO CON SCORM</p>
				<div class='formulariosAdmin'>
					<form id="frm_contenido_multimedia" method="post" action="contenidos/multimedia/editar/<?php echo $rowScorm->idscorm?>/enviar" enctype="multipart/form-data">
						<ul>
							<li>
								<label>T&iacute;tulo</label>
								<input type="text" name="titulo" value="<?php echo Texto::textoPlano($rowScorm->titulo)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label>Descripci&oacute;n</label>
								<textarea rows="8" cols="1" name="descripcion"><?php echo $rowScorm->descripcion?></textarea>
								<div class="clear"></div>
							</li>
							<li>
								<label>Portada</label>
								<div class="fleft">
									<img src="../archivos/scorm/portadas/<?php echo $rowScorm->imagen_scorm?>" alt="" />
								</div>
								<div class="fright" style="width: 350px">
									<div><input type="file" name="image" /></div>
									<div>* Si no se selecciona ning&uacute;n archivo se mantendr&aacute; el antiguo</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<label>El archivo scorm</label>
								<div><input type="file" name="file" /></div>
								<div>* Si no se selecciona ning&uacute;n archivo se mantendr&aacute; el antiguo</div>
								<div class="clear"></div>
							</li>
							<li>
								<label>M&oacute;dulo</label>
								<select name="idmodulo">
									<?php while($modulo = $modulos2->fetch_object()):?>
										<option value="<?php echo $modulo->idmodulo?>" <?php if($modulo->idmodulo == $rowScorm->idmodulo) echo 'selected="selected"'?>><?php echo Texto::textoPlano($modulo->nombre)?></option>
									<?php endwhile;?>
								</select>
								<div class="clear"></div>
							</li>
							<li>
								<input type="submit" value="Actualizar" />
							</li>
						</ul>
					</form>
				</div>
			<?php elseif(isset ($get['boton']) && $get['boton'] == 'Insertar'):?>
				<p class='subtitleAdmin'>INTRODUCIR CONTENIDO CON SCORM</p>
				<div class='formulariosAdmin'>
					<form id="frm_contenido_multimedia" method="post" action="contenidos/multimedia/nuevo" enctype="multipart/form-data">
						<ul>
							<li>
								<label>T&iacute;tulo</label>
								<input type="text" name="titulo" />
								<div class="clear"></div>
							</li>
							<li>
								<label>M&oacute;dulo</label>
								<select name='idmodulo'>
									<option value=''>- Seleccione M&oacute;dulo -</option>
									<?php while($f = mysqli_fetch_assoc($modulos2)):?>
										<?php if($f['idmodulo'] == $idmodulo):?>
											<option value='<?php echo $f['idmodulo'] ?>' selected><?php echo "(".$f['referencia_modulo'] .") " . $f['nombre'] ?></option>
										<?php else:?>
											<option value='<?php echo $f['idmodulo'] ?>'><?php echo "(".$f['referencia_modulo'] .") " . $f['nombre'] ?></option>
										<?php endif;?>
									<?php endwhile;?>
								</select>
								<div class="clear"></div>
							</li>
							<li>
								<label>Descripci&oacute;n</label>
								<textarea rows="8" cols="1" name="descripcion"></textarea>
								<div class="clear"></div>
							</li>
							<li>
								<label>Portada</label>
								<input type="file" name="image" />
								<div class="clear"></div>
							</li>
							<li>
								<label>Subir archivo SCORM</label>
								<input type="file" name="file" />
								<div class="clear"></div>
							</li>
							<li>
								<input type="submit" value="Insertar" />
							</li>
						</ul>
						<div class="clear"></div>
					</form>
					</div>
					<?php endif; ?>
					<?php if(isset($post['boton']) && $post['boton'] == "Ver contenidos"):?>
	
						<!-- archivos contenido con scorm -->
							<div class='view'>
								<div class='header'>
									<div class='itemg1'>Titulo</div>
									<div class='fright' style='width:60px;'>Editar</div>
									<div class='fright' style='width:60px;'>Eliminar</div>
								</div>
								<div class='boder'>
										<?php 
										if(isset($scormsModulo) && $scormsModulo->num_rows > 0):
											while($item = $scorms->fetch_object()):
												?>
												<div class='bod_cont'>
													<div class='itemg1'><a href="<?php echo PATH_ROOT . 'archivos/scorm/' . $item->idscorm; ?>" target='_blank'><?php echo Texto::textoPlano($item->titulo); ?></a></div>
													<div class='fright' style='width:50px;'><a href='contenidos/multimedia/editar/<?php echo $item->idscorm;?>'><img src='imagenes/editar2.png' alt=''/></a></div>
													<div class='fright' style='width:50px;'>
														<a href="#" <?php echo Alerta::alertConfirmOnClick('borrarscorm', '¿Deseas borrar este contenido multimedia (scorm)?', 'contenidos/multimedia/eliminar/' . $item->idscorm)?> title="Borrar este contenido multimedia">
															<img src='imagenes/delete2.png' alt=''/>
														</a>
													</div>
												</div>
										<?php 
											endwhile;
										else:
											echo "No existen archivos";
										endif;	
										?>		
								</div>
								<div class='foot'></div>
							</div>		
	
	
	
						<!--
						Esto es un buscador de contenido con scorm
						<h2>Listado del contenido multimedia</h2>
						<input id="scormSearch" type="text" />
						<a id="scormSearchShowAll" href="#" onclick="return false;">Mostrar todos</a>
						<?php //if($scormsModulo->num_rows > 0):?>
							<div id="scormSearchList" class="listarElementos">
								<?php //while($item = $scorms->fetch_object()):?>
									<div class="elemento hide">
										<div class="elementoContenido"><p><?php //echo Texto::textoPlano($item->titulo)?></p></div>
										<div class="elementoPie hide">
											<ul class="elementoListOptions fright">
												<li><a href="contenidos/multimedia/editar/<?php // echo $item->idscorm?>" title="Editar este contenido multimedia">Editar</a></li>
												<li><a href="#" <?php //echo Alerta::alertConfirmOnClick('borrarscorm', '¿Deseas borrar este contenido multimedia (scorm)?', 'admin/contenidos/multimedia/eliminar/' . $item->idscorm)?> title="Borrar este contenido multimedia">Borrar</a></li>
											</ul>
											<div class="clear"></div>
										</div>
									<div class="clear"></div>
									</div>
								<?php //endwhile;?>
							</div>
						<?php //else:?>
							<div class="elementosNoEncontrados">No hay contenido multimedia</div>
						<?php //endif;?>	
						 -->
						 
						 
						 
					<?php endif; ?>
		</div>
	</div>
</div>


