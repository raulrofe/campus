<div id="contenido_multimedia">
	<h2>Subir un nuevo contenido Scorm</h2>
	<form id="frm_contenido_multimedia" method="post" action="" enctype="multipart/form-data" id="frmNuevoContenido">
		<ul>
			<li>
				<label>T&iacute;tulo</label>
				<input type="text" name="titulo" />
			</li>
			<li>
				<label>Descripci&oacute;n</label>
				<textarea rows="8" cols="1" name="descripcion"></textarea>
			</li>
			<li>
				<label>El archivo scorm</label>
				<input type="file" name="file" />
			</li>
			<li>
				<label>Portada</label>
				<input type="file" name="image" />
			</li>
			<li>
				<input type="submit" value="Subir" />
			</li>
		</ul>
	</form>
</div>

<script type="text/javascript" src="js-contenidos-nuevo_contenidosinscorm.js"></script>