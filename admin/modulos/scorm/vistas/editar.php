<div id="admin_usuarios">
	<div class="titular">
		<h1 class='titleAdmin'>Contenidos</h1>
		<img src='../imagenes/pie_titulos.jpg' />
	</div>
  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas -->	  			
	  	<div class="tab">
			<div class="submenuAdmin">CONTENIDO CON SCORM</div>
				<form id="frm_contenido_multimedia" method="post" action="" enctype="multipart/form-data">
						<ul>
							<li>
								<label>T&iacute;tulo</label>
								<input type="text" name="titulo" value="<?php echo Texto::textoPlano($rowScorm->titulo)?>" />
								<div class="clear"></div>
							</li>
							<li>
								<label>Descripci&oacute;n</label>
								<textarea rows="8" cols="1" name="descripcion"><?php echo $rowScorm->descripcion?></textarea>
								<div class="clear"></div>
							</li>
							<li>
								<label>El archivo scorm</label>
								<input type="file" name="file" />
								<small>* Si no se selecciona ning&uacute;n archivo se mantendr&aacute; el antiguo</small>
								<div class="clear"></div>
							</li>
							<li>
								<label>M&oacute;dulo</label>
								<select name="idmodulo">
									<?php while($modulo = $modulos->fetch_object()):?>
										<option value="<?php echo $modulo->idmodulo?>" <?php if($modulo->idmodulo == $rowScorm->idmodulo) echo 'selected="selected"'?>><?php echo Texto::textoPlano($modulo->nombre)?></option>
									<?php endwhile;?>
								</select>
								<div class="clear"></div>
							</li>
							<li>
								<input type="submit" value="Actualizar" />
							</li>
						</ul>
				</form>
			</div>
		</div>
	</div>
</div>