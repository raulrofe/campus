<?php
class ModeloScorm extends modeloExtend
{
	public function insertarScorm($idmodulo, $titulo, $descripcion)
	{
		$sql = 'INSERT INTO scorm (idmodulo, titulo, descripcion, fecha)' .
		' VALUES (' . $idmodulo . ', "' . $titulo . '", "' . $descripcion . '", "' . date('Y-m-d H:i:s') . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarScorm($idscorm, $idscorm_folder)
	{
		$sql = 'UPDATE scorm SET idscorm_folder = ' . $idscorm_folder . ' WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarScormCompleto($idscorm, $titulo, $descripcion, $idmodulo)
	{
		$sql = 'UPDATE scorm SET titulo = "' . $titulo . '", descripcion = "' . $descripcion . '", idmodulo = "' . $idmodulo . '" WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarImagenScorm($idscorm, $nameImage)
	{
		$sql = 'UPDATE scorm SET imagen_scorm = "' . $nameImage . '" WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarScorm($idscorm)
	{
		$sql = 'DELETE FROM scorm WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerModulos()
	{
		$sql = 'SELECT md.idmodulo, md.nombre, md.referencia_modulo' .
		' FROM modulo AS md' .
		' ORDER BY md.referencia_modulo ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerScorms($idmodulo)
	{
		$sql = 'SELECT * FROM scorm AS sc' .
			' WHERE sc.idmodulo = ' . $idmodulo;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnScorm($idscorm)
	{
		$sql = 'SELECT * FROM scorm WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerScormModulo($idmodulo)
	{
		$sql = 'SELECT * FROM scorm WHERE idmodulo = ' . $idmodulo;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}