/* BUSCADOR DINAMICO */
LibSearchD = {};

LibSearchD.start = function()
{
	$('#scormSearch').keyup(function(event)
	{
		var wordSearch = $(this).val();
		
		$('#scormSearchList').find('.elemento').addClass('hide');
		
		if(wordSearch != "")
		{
			var titleScorm = '';
			var searchMatch = null;
			$('#scormSearchList').find('.elemento').each(function(index)
			{
				titleScorm = $(this).find('.elementoContenido p').html();
				//searchMatch = '/^(' + wordSearch + ')(.+)/i';
				
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('^(' + wordSearch + ')(.+)', 'i');
				if(titleScorm.match(regex))
				{
					$(this).removeClass('hide');
				}
			});
		}
	});
};

$(document).ready(function()
{
	LibSearchD.start();
	
	// mostrar todos los scorms
	$('#scormSearchShowAll').click(function()
	{
		$('#scormSearch').val('');
		$('#scormSearchList').find('.elemento').removeClass('hide');
	});
});