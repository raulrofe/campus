<?php
$objModeloScorm = new ModeloScorm();

$get = Peticion::obtenerGet();
if(isset($get['idscorm']) && is_numeric($get['idscorm']))
{
	$rowScorm = $objModeloScorm->obtenerUnScorm($get['idscorm']);
	if($rowScorm->num_rows == 1)
	{
		$rowScorm = $rowScorm->fetch_array();
		
		if($objModeloScorm->eliminarScorm($get['idscorm']))
		{
			Fichero::removeDir(PATH_ROOT . 'archivos/scorm/' . $get['idscorm']);
			
			if(!empty($rowScorm['imagen_scorm']))
			{
				chmod(PATH_ROOT . 'archivos/scorm/portadas/' . $rowScorm['imagen_scorm'], 0777);
				unlink(PATH_ROOT . 'archivos/scorm/portadas/' . $rowScorm['imagen_scorm']);
			}
			
			Alerta::guardarMensajeInfo('Se ha eliminado el contenido multimedia seleccionado');
		}
	}
}

Url::redirect('contenido-multimedia', true);