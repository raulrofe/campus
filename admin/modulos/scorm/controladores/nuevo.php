<?php
$objModeloScorm = new ModeloScorm();
					
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($_FILES['file']['tmp_name'], $_FILES['image']['tmp_name'], $post['idmodulo'], $post['titulo'], $post['descripcion'])
		&& !empty($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name']) && is_numeric($post['idmodulo'])
		&& !empty($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name']))
	{
		if(!empty($post['titulo']))
		{
			if($_FILES['file']['type'] == 'application/x-zip-compressed' || $_FILES['file']['type'] == 'application/zip'
				&& (Fichero::validarTipos($_FILES['image']['name'], $_FILES['image']['type'], array('jpg', 'png', 'jpeg', 'bmp', 'gif'))))
			{
				$objZip = new ZipArchive();
				$filename = $_FILES['file']['tmp_name'];
				$image = $_FILES['image']['tmp_name'];
				
				if($objZip->open($filename, ZIPARCHIVE::CREATE) !== true)
				{
					// no se pudo abrir
					Alerta::guardarMensajeInfo('No se pudo abrir el fichero');
				}
				else
				{
					$result = false;
					
					// a_adimos el registro a la BBDD
					$result = $objModeloScorm->insertarScorm($post['idmodulo'], $post['titulo'], $post['descripcion']);
					$idScorm = $objModeloScorm->obtenerUltimoIdInsertado();
					
					if($result)
					{
						$result = false;

						require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';
		
						$objThumb = PhpThumbFactory::create($_FILES['image']['tmp_name']);
									
						$newFilename = $idScorm . '.' . strtolower($objThumb->getFormat());
						$path = PATH_ROOT . 'archivos/scorm/portadas/';
									
						$objThumb->resize(100, 100);
						if($objThumb->save($path . $newFilename))
						{
							$result = $objModeloScorm->actualizarImagenScorm($idScorm, $newFilename);
						}
					}

					if($result)
					{
						$path = PATH_ROOT . 'archivos/scorm/' . $idScorm . '/';
						
						if(mkdir($path))
						{
							$result = $objZip->extractTo($path);
						}
						
						// buscamos un archivo en el directorio del curso que acabamos descomprimir (en una carpeta especifica)
						if($result)
						{
							$result = false;
							
							$dirs = Fichero::obtenerDirectorios($path);
							foreach($dirs as $folder)
							{
								if(preg_match('/sco_([0-9]+)\/$/', $folder))
								{				
									preg_match_all('/sco_([0-9]+)\/$/', $folder, $matchs, PREG_PATTERN_ORDER);
									$codeIdScorm = $matchs[1][0];
									
									// a_adimos el id de scorm del curso scorm a la BBDD
									if($objModeloScorm->actualizarScorm($idScorm, $codeIdScorm))
									{
										// abrimos un fichero js para luego sobreescribirlo
										// reemplazamos una funcion de js por otra (window.close por una para modabox.close)
										$pathFileReplace = $folder . 'resources/LinearSNO.js';
										
										$contenidoFichero = file_get_contents(PATH_ROOT_ADMIN . 'modulos/scorm/controladores/LinearSNO.js');
										//$nuevoContenidoFichero = str_replace("ventana.close();", "parent.top.LibPopupModal.close('popupModal_scorm');", $contenidoFichero);
										
										if($fo = fopen($pathFileReplace, "w"))
										{
											fwrite($fo, $contenidoFichero);
											fclose($fo);
											
											$result = true;
										}
										
										$result = true;
									}
								}
							}
						}
					}
					
					// mostramos el mensaje de alerta sobre la operacion
					if($result)
					{
						Alerta::guardarMensajeInfo('Se ha subido el SCORM');
					}
					else
					{
						// por si se creo el registro pero hubo un error al subir el archivo, borramos el registro
						if(isset($idScorm))
						{
							$objModeloScorm->eliminarScorm($idScorm);
						}
						
						Alerta::guardarMensajeInfo('No se pudo subir el SCORM');
					}
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('El archivo de ser un zip');
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('Debes a&ntilde;adir un t&iacute;tulo al scorm');
		}
		
	}
	else
	{
		Alerta::guardarMensajeInfo('Debes subir un archivo zip');
	}
}

Url::redirect('contenidos/multimedia', true);