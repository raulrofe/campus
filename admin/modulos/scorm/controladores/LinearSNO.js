
var debugSwitchIsTrue = false;   // debug switch to help uncover LMS/SNO communication issues
var noAPI = false;       //TRUE para utilizarlo SIN LMS

var API = null; // Referencia a la API.

var vacio="";
var contentFrame = parent;
var inicio = new Date();
var dia_inicio= inicio.getDate();
var hora_inicio= inicio.getHours();
var minuto_inicio= inicio.getMinutes();
var segundo_inicio= inicio.getSeconds();
var SCOInitialized = false;      // this variable is set to true when the sco is initialized
var pageNumber = 1;      // point to current page within an SCO
var currentSCOPage = 1;  // variable used to point to the current page number within an SCO
var backSCOPage = 1;    // variable used to point to the back page number within an SCO
var score=0;
itinerario(true);
realizar_itinerario=false;
parent.inicializar();

function itinerario(param){
	parent.makeitiarray();
	realizar_itinerario=false;
	numberOfPages = parent.scoPages.length - 1;	
}

function actualizarPag_flash(){
	if (typeof(actualizaP)!='undefined'){
		actualizaP();
	}
}



var MAX_PARENTS_TO_SEARCH = 500; 

/*
ScanParentsForApi
-Searches all the parents of a given window until
 it finds an object named "API_1484_11". If an
 object of that name is found, a reference to it
 is returned. Otherwise, this function returns null.
*/
function ScanParentsForApi(win) 
{ 
	
	/*
	Establish an outrageously high maximum number of
	parent windows that we are will to search as a
	safe guard against an infinite loop. This is 
	probably not strictly necessary, but different 
	browsers can do funny things with undefined objects.
	*/
	var nParentsSearched = 0;
	
	/*
	Search each parent window until we either:
		 -find the API, 
		 -encounter a window with no parent (parent is null 
				or the same as the current window)
		 -or, have reached our maximum nesting threshold
	*/
	while (  (win.API == null) // API es el nombre de la ventana donde est� contenida la API
				&& (win.parent != null) 
				&& (win.parent != win) 
				&& (nParentsSearched <= MAX_PARENTS_TO_SEARCH) 
		  )
	{ 
		nParentsSearched++; 
		win = win.parent;		
	} 
	
	/*
	If the API doesn't exist in the window we stopped looping on, 
	then this will return null.
	*/
	return win.API; 
} 


/*
GetAPI
-Searches all parent and opener windows relative to the
 current window for the SCORM 2004 API Adapter.
 Returns a reference to the API Adapter if found or null
 otherwise.
*/
function GetAPI() { 
	
	var theAPI = null; 
	
	//Search all the parents of the current window if there are any
	if ((window.parent != null) && (window.parent != window)) 
	{ 		
		theAPI = ScanParentsForApi(window.parent); 
	} 
	
	/*
	If we didn't find the API in this window's chain of parents, 
	then search all the parents of the opener window if there is one
	*/
	if ((theAPI == null) && (window.top.opener != null))
	{ 		
		theAPI = ScanParentsForApi(window.top.opener); 
	}
		
	return theAPI;
}


var numberOfPages = parent.scoPages.length - 1; // number of pages in the SCO
// contentFrame.sco.location = parent.scoPages[1];  // set first page of content

/*
var readyStateCheckInterval = setInterval(function() {
    if (parent.top.frames.API.readyState === "complete") {
        initApiScorm();
        clearInterval(readyStateCheckInterval);
    }
}, 10);
*/

scormInitCheckInterval();

function scormInitCheckInterval()
{
	if(!noAPI)
	{
		API = GetAPI();	// objeto que apunta a la API.
		if(API != null && API.LMSInitialize != undefined)
		{
        	initApiScorm();
        }
        else
        {
        	setTimeout('scormInitCheckInterval()', 100);
        }
    }
    else
    {
        setTimeout('scormInitCheckInterval()', 100);
    }
}

function initApiScorm()
{
	if (!noAPI) {

		API = GetAPI();	// objeto que apunta a la API.
		 
		if (API != null && API.LMSInitialize != undefined) {
			var result = API.LMSInitialize(vacio);
			var error= API.LMSGetLastError();
			var mensaje= API.LMSGetErrorString(error);
			
		
			if (result=="false") {
				alert("Error: No se ha podido inicializar el sistema. Por favor cierre esta ventana y vuelva a entrar.");
			}else{
				SCOInitialized = true;        // this variable tracks the initialized state of the SCO
				
				if (debugSwitchIsTrue) {
					alert("SCO Inicializado");	
				}
							
				score= API.LMSGetValue("cmi.core.score.raw");
				var entry= API.LMSGetValue("cmi.core.entry");
				if (entry=="resume"){
					var encontrado=false;
					var currentSCOPageURL= API.LMSGetValue("cmi.core.lesson_location");
					if (currentSCOPageURL!=''){
						if (confirm("�Desea continuar por el �ltimo apartado visitado?")){
						
							contentFrame.sco.location = currentSCOPageURL;
							for(x=1; x<parent.scoPages.length; x++){
								if (parent.scoPages[x] == currentSCOPageURL){
									backSCOPage = currentSCOPage;
									currentSCOPage = x;
									x = numberOfPages+1;
									encontrado=true;
								}
							}
							if (!encontrado){
								backSCOPage = 1;
								currentSCOPage=1;
								currentSCOPageURL=parent.scoPages[currentSCOPage];
								contentFrame.sco.location=currentSCOPageURL;
								
							}
						}else{
							contentFrame.sco.location = parent.scoPages[1];
							currentSCOPageURL=parent.scoPages[1];
							API.LMSSetValue("cmi.core.lesson_location",currentSCOPageURL);
						}
					}else{
						contentFrame.sco.location = parent.scoPages[1];
					}
				}
				else{
					contentFrame.sco.location = parent.scoPages[1];
				}
			}
		} else {
			//alert("Error, no se encuentra la API. Por favor consulte con el administrador.");
			//window.top.close();
		}
	}
}


/*******************************************************
** funcion que guarda los datos b�sicos en tbvalores  **
** cada vez que cambiamos de p�gina en los contenidos **
*******************************************************/
function GuardaBasicos() {
	
	/*para actualizar el numero de paginas de mi contenido
	itinerario(realizar_itinerario);*/
	var fin = new Date();
	var dia_fin= fin.getDate();
	var dia=eval(dia_fin-dia_inicio);
	
	
	var hora_fin= fin.getHours();
	var hora=eval(hora_fin - hora_inicio);
	
	var minuto_fin= fin.getMinutes();
	var minuto=eval(minuto_fin - minuto_inicio);
	if (minuto<0){
		minuto=eval(60+minuto);
		hora=hora-1;
	}
	
	var segundo_fin= fin.getSeconds();
	var segundo=eval(segundo_fin - segundo_inicio);
	if (segundo<0){
		segundo=eval(60+segundo);
		minuto=minuto-1;
		
	}
	if (segundo<10){
		segundo="0"+segundo;
	}
	
	if (minuto<10){
		minuto="0"+minuto;
	}
	if (dia>0) {
	var temp=dia*24;
	hora=hora+temp;
	}
	if (hora<10){
		hora="0"+hora;
	}
	
	var tiempo=hora + ":" + minuto + ":" +segundo			
	
	if (SCOInitialized && !noAPI){			
		var vacio="";		
		
		if (API != null){
			if(currentSCOPage == numberOfPages){
				API.LMSSetValue("cmi.core.lesson_status","completed");
				var error=API.LMSGetLastError();
				var mensaje=API.LMSGetErrorString(error);
					
			}
		
			API.LMSSetValue("cmi.core.session_time",tiempo);
			var error=API.LMSGetLastError();
			var mensaje=API.LMSGetErrorString(error);
			
			
			API.LMSCommit(vacio);
						
		}
	}			
}


function back () {
	/* vuelve a la p�gina anterior visitada */
	parent.sno.goToPageNum(parent.sno.backSCOPage);
}

function goToPageName (parametro,dir) {
	/*
	para recalcular el array de paginas al ir a una p�gina nueva
	itinerario(realizar_itinerario);*/
	var j=0;
	var t=0;
	var cadena='';
	var parametro=new String(parametro);
	var inicio=parametro.indexOf('#')

	GuardaBasicos();
	
	if (inicio>=0){
		cadena=parametro.substring(0,inicio);
	}else{
		cadena=parametro;
	}
	for (i=1;i<=parent.scoPages.length;i++){
		if (parent.scoPages[i]==cadena){
			j=i;
		}
	}
	if (j==0){
		window.open(parametro,'newWindowContent','scrollbars=yes,resizable=yes,top=0,left=0,width=1024,height=768');
	}else{
		backSCOPage = currentSCOPage;
		currentSCOPage = j;
		if ((currentSCOPage == numberOfPages) && (SCOInitialized) && (!noAPI)) {
			var vacio="";
			if (API!=null){
				API.LMSSetValue("cmi.core.lesson_status","completed"); 
				var error=API.LMSGetLastError();
				var mensaje=API.LMSGetErrorString(error);
				API.LMSCommit(vacio);
				var error=API.LMSGetLastError();
				var mensaje=API.LMSGetErrorString(error);
			}
			
		 }  
		if (dir==true){
			parent.sco.location='../'+parent.scoPages[currentSCOPage];
		}else{
			parent.sco.location=parent.scoPages[currentSCOPage];
		}
	}
	actualizarPag_flash();
}

function goToPageNum (parametro,dir) {
	/*para recalcular el array de paginas al ir a una p�gina nueva
	itinerario(realizar_itinerario);*/
	GuardaBasicos();
	
		if (parent.scoPages.length<parametro){
			alert("Esa p�gina no existe");
		}else{
			backSCOPage = currentSCOPage;
			currentSCOPage = parametro;
			if (dir==true){
				contentFrame.sco.location='../'+parent.scoPages[currentSCOPage];
			}else{
				contentFrame.sco.location=parent.scoPages[currentSCOPage];
			}
		
		}
	actualizarPag_flash();
}

function goToNextPage () {
  
   /*para recalcular el array de paginas a la siguiente p�gina
   itinerario(realizar_itinerario);	
   */
	GuardaBasicos();
	
   if((currentSCOPage) < numberOfPages){
		backSCOPage = currentSCOPage;
		currentSCOPage = currentSCOPage + 1;
	/*para ver el nombre de la pagina a la que voy y poder referir la variable de examen desde el
	codigo javascript en el boton de resolver examen
	itinerario(realizar_itinerario);	*/
	   if ((currentSCOPage == numberOfPages) && (SCOInitialized) && (!noAPI)) {			
			var vacio="";
			if (API!=null){
				API.LMSSetValue("cmi.core.lesson_status","completed"); 
				API.LMSCommit(vacio);
			}
			
	   }  
		contentFrame.sco.location = parent.scoPages[currentSCOPage];
		
   }else{
		if ((currentSCOPage == numberOfPages) && (SCOInitialized) && (!noAPI)) {			
			var vacio="";
			if (API!=null){
				API.LMSSetValue("cmi.core.lesson_status","completed"); 
				API.LMSCommit(vacio);
			}
		}  
		alert("Esta es la �LTIMA p�gina del tema. Para salir, pulse sobre el boton 'salir'");
   }
	actualizarPag_flash();
		
}



function goToPreviousPage () {	
	/*para recalcular el array de paginas a la p�gina anterior
	itinerario(realizar_itinerario);	
	*/
	GuardaBasicos();
	
	if(currentSCOPage !=1){
		if ((currentSCOPage == numberOfPages) && (SCOInitialized) && (!noAPI)) {			
			var vacio="";
			if (API!=null){
				API.LMSSetValue("cmi.core.lesson_status","completed"); 
				API.LMSCommit(vacio);
				
			}			
		}  
		backSCOPage = currentSCOPage;
		currentSCOPage = currentSCOPage - 1;
		/*para ver el nombre de la pagina a la que voy y poder referir la variable de examen desde el
		codigo javascript en el boton de resolver examen
		itinerario(realizar_itinerario);*/
		if (SCOInitialized && !noAPI) {			
			var vacio="";
			if (API!=null) {
				if (!SCOInitialized){
					var result = API.LMSInitialize(vacio);
					if (result="true") {
						SCOInitialized = true;
						if (debugSwitchIsTrue) {alert("SCO Inicializado");}
					}
				}
			}
		}
		contentFrame.sco.location = parent.scoPages[currentSCOPage];
	}else{
		alert("Esta es la PRIMERA p�gina del tema. Para salir, pulse sobre el boton 'salir'");
	}
	actualizarPag_flash();
}




function Salir() {
	
	/*para actualizar el numero de paginas de mi contenido
	itinerario(realizar_itinerario);*/
	var fin = new Date();
	var dia_fin= fin.getDate();
	var dia=eval(dia_fin-dia_inicio);
	
	
	var hora_fin= fin.getHours();
	var hora=eval(hora_fin - hora_inicio);
	
	var minuto_fin= fin.getMinutes();
	var minuto=eval(minuto_fin - minuto_inicio);
	if (minuto<0){
		minuto=eval(60+minuto);
		hora=hora-1;
	}
	
	var segundo_fin= fin.getSeconds();
	var segundo=eval(segundo_fin - segundo_inicio);
	if (segundo<0){
		segundo=eval(60+segundo);
		minuto=minuto-1;
		
	}
	if (segundo<10){
		segundo="0"+segundo;
	}
	
	if (minuto<10){
		minuto="0"+minuto;
	}
	if (dia>0) {
	var temp=dia*24;
	hora=hora+temp;
	}
	if (hora<10){
		hora="0"+hora;
	}
	
	var tiempo=hora + ":" + minuto + ":" +segundo			
	
	if (SCOInitialized && !noAPI){			
		var vacio="";		
		
		if (API != null){
			if(currentSCOPage == numberOfPages){
				API.LMSSetValue("cmi.core.lesson_status","completed");
				var error=API.LMSGetLastError();
				var mensaje=API.LMSGetErrorString(error);
					
			}
		
			API.LMSSetValue("cmi.core.session_time",tiempo);
			var error=API.LMSGetLastError();
			var mensaje=API.LMSGetErrorString(error);
			
			var currentSCOPageURL=parent.scoPages[currentSCOPage];
			var cadenatime;
			if (confirm("�Desea que el sistema reconozca el punto en el que est� en estos momentos y que le redirija a �l cuando vuelva a entrar?")){
				cadenatime="suspend";
				API.LMSSetValue("cmi.core.lesson_location",currentSCOPageURL);
				var error=API.LMSGetLastError();
				var mensaje=API.LMSGetErrorString(error);
			
			}else{
				cadenatime="logout";
			}
			var time_allowed=API.LMSGetValue("cmi.student_data.max_time_allowed");
			var total_time=API.LMSGetValue("cmi.core.total_time");
			var suma=sumarfecha(tiempo,total_time);
			if (time_allowed==""){
				time_allowed="1000:00:00.00";
			}
			if (suma==false){
				suma=tiempo;
			}
			if (compararfecha(time_allowed,suma)<1){
				API.LMSSetValue("cmi.core.exit","time-out");
			}else{
				API.LMSSetValue("cmi.core.exit",cadenatime);
			}
			
			if (score==''){
			score=0;
		}
			score=Math.round(score);
			API.LMSSetValue("cmi.core.score.raw",score); 
			var error=API.LMSGetLastError();
			var mensaje=API.LMSGetErrorString(error);
			
			API.LMSCommit(vacio);
			
			var result = API.LMSFinish(vacio);
			if (result) {
				SCOInitialized = false;
			   if (debugSwitchIsTrue) {alert("SCO Finalizado");}
			}
			
		}
	}			
	var ventana=top;
	ventana.close();	
}


function compararfecha(fecha1,fecha2){
	if (fecha1=="" || fecha2=="") {
		ok=-2;
	} else {
		pos1h=fecha1.indexOf(":");
		pos2h=fecha2.indexOf(":");
		pos1m=fecha1.indexOf(":",pos1h+1);
		pos2m=fecha2.indexOf(":",pos2h+1);
		if ((pos1h < 0) || (pos2h < 0) || (pos1m < 0) || (pos2m < 0)) {
			ok=-3;
		} else {
			pos1s=fecha1.indexOf(".",pos1m+1);
			if (pos1s < 0) {
				pos1s=fecha1.length;
				pos1d="No";
			} else {
				pos1d=fecha1.length;
			}
			pos2s=fecha2.indexOf(".",pos2m+1);
			if (pos2s < 0) {
				pos2s=fecha2.length;
				pos2d="No";
			} else {
				pos2d=fecha2.length;
			}
			// Hasta ahora comprobaci�n del tipo de fecha, a partir de aqu� comprobar� las horas.
				
			hora1=parseInt(fecha1.substring(0,pos1h+1),10);
			hora2=parseInt(fecha2.substring(0,pos2h+1),10);
			if (hora2<hora1){
				ok=1;
			} else if (hora2>hora1) {
				ok=-1;
			} else if (hora2==hora1) {
				minuto1=parseInt(fecha1.substring(pos1h+1,pos1m+1),10);
				minuto2=parseInt(fecha2.substring(pos2h+1,pos2m+1),10);
				if (minuto2<minuto1){
					ok=1;
				} else if (minuto2>minuto1) {
					ok=-1;
				} else if (minuto2==minuto1) {
					segundo1=parseInt(fecha1.substring(pos1m+1,pos1s+1),10);
					segundo2=parseInt(fecha2.substring(pos2m+1,pos2s+1),10);
					if (segundo2<segundo1){
						ok=1;
					} else if (segundo2>segundo1) {
						ok=-1;
					} else if (segundo2==segundo1) {
						if ((pos1d == "No") && (pos2d == "No")) {
							ok=0;
						} else if ((pos1d != "No") && (pos2d == "No")) {
							ok=1;
						} else if ((pos1d == "No") && (pos2d != "No")) {
							ok=-1;
						} else if ((pos1d != "No") && (pos2d != "No")) {
							decimas1=parseInt(fecha1.substring(pos1s+1,fecha1.length),10);
							decimas2=parseInt(fecha2.substring(pos2s+1,fecha2.length),10);
							if (decimas2<decimas1){
								ok=1;
							} else if (decimas2>decimas1) {
								ok=-1;
							} else if (decimas2==decimas1) {
								ok=0;
							}
						}
					}		
				}
			}
		}
	}
	return ok;
}


function sumarfecha(fecha1,fecha2){
	if (fecha1=="" || fecha2=="") {
		return(false);
	} else {
		fecha1 = new String(fecha1);
		fecha2 = new String(fecha2);
		pos1h=fecha1.indexOf(":");
		pos2h=fecha2.indexOf(":");
		pos1m=fecha1.indexOf(":",pos1h+1);
		pos2m=fecha2.indexOf(":",pos2h+1);
		if ((pos1h < 0) || (pos2h < 0) || (pos1m < 0) || (pos2m < 0)) {
			return(false);
		} else {
			pos1s=fecha1.indexOf(".",pos1m+1);
			if (pos1s < 0) {
				pos1s=fecha1.length;
				pos1d="No";
			} else {
				pos1d=fecha1.length;
			}
			pos2s=fecha2.indexOf(".",pos2m+1);
			if (pos2s < 0) {
				pos2s=fecha2.length;
				pos2d="No";
			} else {
				pos2d=fecha2.length;
			}
			// Hasta ahora comprobaci�n del tipo de fecha, a partir de aqu� comprobar� las horas.
				
			hora1=parseInt(fecha1.substring(0,pos1h+1),10);
			hora2=parseInt(fecha2.substring(0,pos2h+1),10);
			hora = hora1 + hora2
			
			
			minuto1=parseInt(fecha1.substring(pos1h+1,pos1m),10);
			minuto2=parseInt(fecha2.substring(pos2h+1,pos2m),10);
			minuto = minuto1+minuto2;
			
			segundo1=parseInt(fecha1.substring(pos1m+1,pos1s+1),10);
			segundo2=parseInt(fecha2.substring(pos2m+1,pos2s+1),10);
			segundo = segundo1+segundo2;
			
			if ((pos1d == "No") && (pos2d == "No")) {
				decimas1=0;
				decimas2=0;
			} else if ((pos1d != "No") && (pos2d == "No")) {
				decimas1=parseInt(fecha1.substring(pos1s+1,fecha1.length),10);
				decimas2=0;
			} else if ((pos1d == "No") && (pos2d != "No")) {
				decimas1=0;
				decimas2=parseInt(fecha2.substring(pos2s+1,fecha2.length),10);
			} else if ((pos1d != "No") && (pos2d != "No")) {
				decimas1=parseInt(fecha1.substring(pos1s+1,fecha1.length),10);
				decimas2=parseInt(fecha2.substring(pos2s+1,fecha2.length),10);
			}
			decimas = decimas1+decimas2;
			if (decimas > 99) {
				decimas %= 100;
				segundo++;
			}
			
			if (segundo > 59) {
				segundo %= 60;
				minuto++;
			}
			if (segundo < 10) {
				segundo = "0"+segundo;
			}
			if (minuto > 59) {
				minuto %= 60;
				hora++;
			}
			if (minuto < 10) {
				minuto = "0"+minuto;
			}
			if (hora > 9999) {
				return(false);
			}
			if (hora < 10) {
				hora = "0"+hora;  
			}	
			
			if ((pos1d == "No") && (pos2d == "No"))
				fecha = hora+":"+minuto+":"+segundo;
			else 
				fecha = hora+":"+minuto+":"+segundo+"."+decimas;
			return(fecha);
		}
	}
}


function ValorCookie(nombre){
	var micookie=document.cookie;
	//alert(micookie);
	if (micookie==""){
		return "";
	}
	var Pos =micookie.indexOf(nombre);
	//alert(Pos);
	if (Pos!=-1){
		var Pos1= micookie.indexOf("=",Pos);
		if (Pos1!=(Pos + nombre.length)){
			return "";
		}
		var Pos2= micookie.indexOf(";",Pos) -1;
		if (Pos2==-2) {
			Pos2=micookie.length;
		}
		//alert(nombre+' --> ' +micookie.substring(Pos1+1,Pos2+1));
	
		return unescape(micookie.substring(Pos1+1,Pos2+1));
	}
	else{
		return "";
	}
}


function GuardarCookie(nombre,valor,caducidad){
	if (caducidad){
		var Caduc= new Date();
		Caduc.setTime (Caduc.getTime() + caducidad);
		document.cookie=nombre + "=" + escape(valor) + "; expires=" + Caduc.toGMTString() + ";";
	}
	else{
	document.cookie=nombre + "=" + escape(valor) + ";";
	}
}

/* devuelve el n�mero total de p�ginas de uns SCO */
function getNumberOfPages() {
	return numberOfPages;
}

/* devuelve el n�mero de la p�gina actual */
function getCurrentSCOPage() {
	return currentSCOPage;
}

/* devuelve el n�mero de la p�gina anterior */
function getBackSCOPage() {
	return backSCOPage;
}

/* devuelve la puntuaci�n */
function getScore(){
	return score;
}