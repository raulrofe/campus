<?php
$objModeloScorm = new ModeloScorm();

$get = Peticion::obtenerGet();
if(isset($get['idscorm']) && is_numeric($get['idscorm']))
{
	$rowScorm = $objModeloScorm->obtenerUnScorm($get['idscorm']);
	if($rowScorm->num_rows == 1)
	{
		$rowScorm = $rowScorm->fetch_object();
		
		if(Peticion::isPost())
		{
			$result = false;
			
			$objZip = new ZipArchive();
			$path = PATH_ROOT . 'archivos/scorm/portadas/';
			$filename = $_FILES['file']['tmp_name'];
					
			// actualizar image del scorm
			if(isset($_FILES['image']['tmp_name'])
				&& !empty($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name']))
			{
				if(Fichero::validarTipos($_FILES['image']['name'], $_FILES['image']['type'], array('jpg', 'png', 'jpeg', 'bmp', 'gif')))
				{
					$image = $_FILES['image']['tmp_name'];
					
					require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';
	
					$objThumb = PhpThumbFactory::create($_FILES['image']['tmp_name']);
								
					$newFilename = $rowScorm->idscorm . '.' . strtolower($objThumb->getFormat());
					
					@chmod($path . $rowScorm->imagen_scorm, 0777);
					@unlink($path . $rowScorm->imagen_scorm);

					$objThumb->resize(100, 100);
					if($objThumb->save($path . $newFilename))
					{
						$result = $objModeloScorm->actualizarImagenScorm($rowScorm->idscorm, $newFilename);
					}
				 }
			}

			// actualizar ZIP del scorm
			if(isset($_FILES['file']['tmp_name'])
				&& !empty($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name']))
			{
				$result = false;
				
				if($_FILES['file']['type'] == 'application/x-zip-compressed' || $_FILES['file']['type'] == 'application/zip')
				{
					if($objZip->open($filename, ZIPARCHIVE::CREATE) !== true)
					{
						// no se pudo abrir
						Alerta::mostrarMensajeInfo('noabrirfichero','No se pudo abrir el fichero');
					}
					else
					{
						$path = PATH_ROOT . 'archivos/scorm/' . $get['idscorm'] . '/';
						
						// eliminamos el antiguo paquete del curso
						if(!file_exists($path) || (file_exists($path) && Fichero::removeDir($path, false)))
						{
							// extraemos los ficheros a la carpeta del curso
							if($objZip->extractTo($path))
							{
								// buscamos un archivo en el directorio del curso que acabamos descomprimir (en una carpeta especifica)
								$dirs = Fichero::obtenerDirectorios($path);
								foreach($dirs as $folder)
								{
									if(preg_match('/sco_([0-9]+)\/$/', $folder))
									{				
										preg_match_all('/sco_([0-9]+)\/$/', $folder, $matchs, PREG_PATTERN_ORDER);
										$codeIdScorm = $matchs[1][0];
										
										// a_adimos el id de scorm del curso scorm a la BBDD
										if($objModeloScorm->actualizarScorm($get['idscorm'], $codeIdScorm))
										{
											// abrimos un fichero js para luego sobreescribirlo
											// reemplazamos una funcion de js por otra (window.close por una para modabox.close)
											$pathFileReplace = $folder . 'resources/LinearSNO.js';
											
											$contenidoFichero = file_get_contents(PATH_ROOT_ADMIN . 'modulos/scorm/controladores/LinearSNO.js');
											//$nuevoContenidoFichero = str_replace("ventana.close();", "parent.top.LibPopupModal.close('popupModal_scorm');", $contenidoFichero);
											
											if($fo = fopen($pathFileReplace, "w"))
											{
												fwrite($fo, $contenidoFichero);
												fclose($fo);
												
												$result = true;
											}
											
											$result = true;
										}
									}
								}
							}
						}
						
						// mostramos el mensaje de alerta sobre la operacion
						if($result)
						{
							Alerta::mostrarMensajeInfo('scormsubido','Se ha subido el SCORM');
						}
						else
						{
							Alerta::mostrarMensajeInfo('noscormsubido','No se pudo subir el SCORM');
						}
					}
				}
				else
				{
					Alerta::mostrarMensajeInfo('debezip','El archivo de ser un zip');
				}
			}
			
			// actualizamos los datos del scorm
			$post = Peticion::obtenerPost();
			if(isset($post['titulo'], $post['descripcion'], $post['idmodulo']))
			{
				if($objModeloScorm->actualizarScormCompleto($get['idscorm'], $post['titulo'], $post['descripcion'], $post['idmodulo']))
				{
					Alerta::guardarMensajeInfo('Se ha actualizado el contenido multimedia');
					
					Url::redirect('contenidos/multimedia');
				}
			}
		}
		
		Url::redirect('contenidos/multimedia/editar/' . $get['idscorm']);
	}
}

Url::redirect('contenidos/multimedia');