<?php
$objModeloScorm = new ModeloScorm();

//obetnemos variables
$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

$idmodulo = null;

//establecemos idmodulo para el post
if(isset($post['idmodulo']) && is_numeric($post['idmodulo']))
{
	$idmodulo = $post['idmodulo'];
}

//establecemos idmodulo para el get
else if(isset($get['idmodulo']) && is_numeric($get['idmodulo']))
{
	$idmodulo = $get['idmodulo'];
}

//print_r($post);

if(isset($post['boton'], $idmodulo) && $post['boton'] == 'Ver contenidos')
{
	$scormsModulo = $objModeloScorm->obtenerScormModulo($idmodulo);
}

if(isset($idmodulo))
{
	$scorms = $objModeloScorm->obtenerScorms($idmodulo);
}

// obtenemos los modulos
$modulos = $objModeloScorm->obtenerModulos();
$modulos2 = $objModeloScorm->obtenerModulos();

if(isset ($get['boton']) && $get['boton'] == 'Insertar')
{
	require_once mvc::obtenerRutaControlador(dirname(__FILE__), 'nuevo');
}
else if(isset ($get['boton']) && $get['boton'] == 'Editar')
{
	$get = Peticion::obtenerGet();
	if(isset($get['idscorm']) && is_numeric($get['idscorm']))
	{
		$rowScorm = $objModeloScorm->obtenerUnScorm($get['idscorm']);
		if($rowScorm->num_rows == 1)
		{
			$rowScorm = $rowScorm->fetch_object();
		}
		else
		{
			$rowScorm = null;
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');