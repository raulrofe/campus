<div class="block block-themed themed-default">
	<div class="block-title"><h5>ACTUALIZAR TIPO CURSO</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmTipoCurso" action="cursos/editar/tipo_curso/<?php echo $get['idTipoCurso'] ?>" method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
                <div>      
	            	<!-- Column -->
		            <div class="span6">
		            	<div class="control-group">
		                	<label for="columns-text" class="control-label">Tipo curso</label>
	                    	<div class="controls"><input type='text' name='atipo_curso' value='<?php echo Texto::textoPlano($tipoCurso['rama_curso']);?>' size='60'/></div>
	                    </div>
					</div>		                
		            <div class="clear"></div>
		         </div>	                	                	                
             </div>
                            
             <!-- END div.row-fluid -->
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Enviar</button></div>
		</form>
	</div>
</div>

<script type="text/javascript" src="js-cursos-actualizar_tipocurso.js"></script>
	