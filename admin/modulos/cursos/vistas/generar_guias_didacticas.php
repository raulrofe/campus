<div class='titular'>
<h1>CURSOS</h1>
<img src='../imagenes/pie_titulos.jpg'/>
</div>	
		
  	<div class="stContainer" id="tabs">
	  	<div class="tab">
	  		<div class="submenuAdmin">GENERAR GU&Iacute;A DID&Aacute;CTICA</div>
	  		<div class='formulariosAdmin'>
		  		<form id="frmGenerarGuiasDidacticas" method="post" action="" enctype="multipart/form-data" class='t_center'>
		  			<select name="idconv">
				  		<option value="">- Selecciona una convocatoria -</option>
					  	<?php while($conv = $convocatorias->fetch_object()):?>
					  		<option value="<?php echo $conv->idconvocatoria?>"><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
					  	<?php endwhile;?>
				  	</select>
				  	<br/><br/>
		  			<input id="frmGenerarGuiasDidacticas1" type="checkbox" name="sobrescribir" />
		  			<label for="frmGenerarGuiasDidacticas1">Sobrescribir las gu&iacute;as did&aacute;cticas ya guardadas de esta convocatoria</label>
		  			<br/><br/>
		  			<button type="submit">Generar</button>
		  		</form>
	  		</div>
	  		<?php echo $htmlLog;?>
	  	</div>
	  </div>
