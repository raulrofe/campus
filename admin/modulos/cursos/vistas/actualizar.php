<!-- este es un buscador fijo -->
	<?php if(isset($idconv) && $idconv != 0):?>
		<form name='busca_curso' action='' method='post'>	
			<div class='fleft' style='width:160px;'>Elije un  curso: &nbsp;&nbsp;</div>
			<select name='idcurso' onchange="this.form.submit()">
				<option value='0'>Selecciona un curso</option>
				<?php 
				while($registro = mysqli_fetch_assoc($registros))
				{
					if($idcurso == $registro['idcurso']) echo "<option value='".$registro['idcurso']."' selected>".$registro['titulo']."</option>";
					else echo "<option value='".$registro['idcurso']."'>".$registro['titulo']."</option>";
				}
				?>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<?php if(isset($idcurso) && $idcurso != 0):?>
			<span style='boton_borrar'>
				<input type='button' value='Borrar' onclick='urlRedirect("cursos/eliminar/<?php echo $f['idcurso'];?>")'/>
			</span>
			<?php endif;?>
		<input type='hidden' name='idconv' value='<?php echo $idconv;?>' />
		</form>
	<?php endif;?>
<!-- fin buscador fijo -->
	
<?php 
if(isset($post['idCurso']) && is_numeric($post['idCurso']))
{
?>
	<p class='subtitleAdmin'>ACTUALIZAR DATOS CURSO</p>
	<div class='formulariosAdmin'>				
<div class='updt'>
	<form name='frm_tipo_curso' action='cursos' method='post' enctype="multipart/form-data">
		<div class='filafrm'>
			<div class='etiquetafrm'>T&iacute;tulo curso</div>
			<div class='campofrm'><input type='text' name='atitulo' size='60' value='<?php echo $f['titulo'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Acciones Formativas</div>
			<div class='campofrm'>
				<select name='idaf'>
					<?php 
						while($registros_af = mysqli_fetch_assoc($resultado_af))
						{
							if ($registros_af['idaccion_formativa'] == $f['idaccion_formativa']) echo "<option value='".$registros_af['idaccion_formativa']."' selected>".$registros_af['accion_formativa']."</option>";
							else echo "<option value='".$registros_af['idaccion_formativa']."'>".$registros_af['accion_formativa']."</option>";
						}
					?>
				</select>
			</div>	
		</div>

		<div class='filafrm'>
			<div class='etiquetafrm'>Tipo de curso</div>
			<div class='campofrm'>
				<?php $mi_curso->select_tipo_curso_noauto($registros2,$f['idrama_curso']); ?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Metodolog&iacute;a</div>
			<div class='campofrm'>
				<select name='ametodologia'>
					<option value='10' <?php if ($f['metodologia'] == '10') echo 'selected';?>>Teleformaci&oacute;n</option>
					<option value='9' <?php if ($f['metodologia'] == '9') echo 'selected';?>>A distancia</option>
					<option value='8' <?php if ($f['metodologia'] == '8') echo 'selected';?>>Presencial</option>
				</select>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>N&uacute;mero horas</div>
			<div class='campofrm'><input type='text' name='an_horas' size='60' value='<?php echo $f['n_horas'];?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Fecha inicio</div>
			<div class='campofrm'><input type='text' name='af_inicio' id='af_inicio' size='60' value='<?php echo $f_inicio;?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Fecha finalizaci&oacute;n</div>
			<div class='campofrm'><input type='text' name='af_fin' id='af_fin' size='60' value='<?php echo $f_fin;?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Inicio matriculaci&oacute;n</div>
			<div class='campofrm'><input type='text' name='ai_matriculacion' id='ai_matriculacion' size='60' value='<?php echo $i_matriculacion;?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Fin matriculaci&oacute;n</div>
			<div class='campofrm'><input type='text' name='af_matriculacion' id='af_matriculacion' size='60' value='<?php echo $f_matriculacion;?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Descripci&oacute;n</div>
			<div class='campofrm'><textarea name='adescripcion' class='estilotextarea'><?php echo $f['descripcion'];?></textarea></div>
		</div>	
		<div class='filafrm'>
			<div class='etiquetafrm'>Eliminar gu&iacute;a did&aacute;ctica</div>
			<div class='campofrm'><input type="checkbox" name='eliminar_gd' value='1' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Gu&iacute;a did&aacute;ctica</div>
			<div class='campofrm'><input type="file" name='guiaDidactica' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Imagen gu&iacute;a did&aacute;ctica</div>
			<div class='campofrm'><input type="file" name='guiaDidacticaImagen' /></div>
		</div>
		<div class='filafrm'>
			<?php if(!empty($f['guia_didactica'])):?>
				<div><a href='../archivos/guias_didacticas/<?php echo $f['guia_didactica']?>' target='_blank'>Descargar gu&iacute;a did&aacute;ctica</a></div>
			<?php else:?>
				<div>No hay gu&iacute;a did&aacute;ctica</div>
			<?php endif;?>
		</div>								
		<div><input type='submit' value='Actualizar'></input></div>
		<input type='hidden' name='idconv' value='<?php echo $post['idConvocatoria']; ?>' />
		<input type='hidden' name='idcurso' value='<?php echo $post['idCurso']; ?>' />
	</form>
</div>
</div>
<?php }?>
<script>crearCalendario('af_inicio')</script>
<script>crearCalendario('af_fin')</script>
<script>crearCalendario('ai_matriculacion')</script>
<script>crearCalendario('af_matriculacion')</script>