<div id="admin_usuarios">
	<div class="titular">
		<h1>Acciones Formativas</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>		
	<div class="stContainer" id="tabs">
	  	<div class="tab">
	  	<div class="submenuAdmin">CARGA MASIVA</div>
		  	<p class='subtitleAdmin'>REALIZAR CARGA MASIVA DE ACCIONES FORMATIVAS</p>
		  	<div class='formulariosAdmin'>	
		  		<form method="post" action="" enctype="multipart/form-data" class='t_center'>
		  			<p class='t_center'>Seleccione una convocatoria para la carga masiva de cursos</p><br/>
			  		<select name="idconv">
				  		<option value="">- Selecciona una convocatoria -</option>
					  	<?php while($conv = $convocatorias->fetch_object()):?>
					  		<option value="<?php echo $conv->idconvocatoria?>"><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
					  	<?php endwhile;?>
				  	</select>
				  	<br/><br/>
					<input type="file" name="archivo" />
					<br/><br/>
		  			<button type="submit">Cargar</button>
		  		</form>
			</div>
			<br/>
		  	<div>
		  		1.- <a href="../archivos/plantilla_cursos.csv" title="">Descargar plantilla CSV</a>
		  		    <a href="../archivos/plantilla_cursos.xlsx" title="">Descargar plantilla Excel</a>
		  	</div>
		  	<div>
		  		2.- <a href="#" onclick="popup_real_open('admin_cursos_ramas', 'cursos/carga-masiva/listar-ramas', 400, 500); return false;">Listar ramas de curso</a>
		 		<br />
		  		3.- <a href="#" onclick="popup_real_open('admin_cursos_config', 'cursos/carga-masiva/listar-config-cursos', 400, 600); return false;">Listar configuraciones de calificaciones de cursos</a>
		  	</div>
		  	<?php echo $htmlLog;?>
	  	</div>
	</div>
</div>