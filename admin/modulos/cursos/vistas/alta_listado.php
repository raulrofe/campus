<!-- Listado de Cursos -->
<div id="cursosList">
	<?php if($registrosCursos->num_rows > 0):?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th>Nombre Curso</th>
	                <th>Fecha inicio</th>
	                <th>Fecha fin</th>
	                <th class="span1 text-center">Alta</th>
				</tr>
			</thead>
	  		<?php while($curso = mysqli_fetch_object($registrosCursos)):?>
	        	<tr>
	                <td><a href="cursos/alta/<?php echo $curso->idcurso ?>"><?php echo $curso->titulo ?></a></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_inicio, '-', '/') ?></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_fin, '-', '/') ?></td>
	                <td class="span1 text-center">
	                	<?php if($curso->alta == 1):?>
	                		<i class="icon-ok-sign"></i>
	                	<?php else:?>
	                		<i class="icon-remove-sign"></i>
	                	<?php endif;?>
	                </td>
	            </tr>
	  		<?php endwhile; ?>
	  	</table> 
	<?php else: ?>
		<div class="text-center">No existen cursos</div>
	<?php endif;?>
</div>
 