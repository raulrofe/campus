<?php if(isset($idcurso) && is_numeric($idcurso)):?>
<p class='subtitleAdmin'>ACTUALIZAR DATOS ALTA CURSO <?php echo $f['titulo']; ?></p>
<div class='formulariosAdmin'>
	<form name='frm_tipo_curso' action='cursos/alta' method='post'>
		<div class='filafrm'>
			<div class='etiquetafrm'>Coordinador</div>
			<div class='campofrm'>
				<?php 
					$idusuario1 = $mi_curso->busca_status('coordinador',$idcurso);
					$mi_curso->select_usuarios2(3,$idusuario1);
				?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>supervisor</div>
			<div class='campofrm'>
				<?php 
					$idusuario2 = $mi_curso->busca_status('supervisor',$idcurso);
					$mi_curso->select_usuarios2(2,$idusuario2);
				?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Tutor principal</div>
			<div class='campofrm'>
				<?php
					$idusuario3 = $mi_curso->busca_status('tutor1',$idcurso); 
					$mi_curso->select_usuarios2(4,$idusuario3);
				?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm'>Tutor secundario</div>
			<div class='campofrm'>
				<?php
					$idusuario4 = $mi_curso->busca_status('tutor2',$idcurso); 
					$mi_curso->select_usuarios2(4,$idusuario4);
				?>
			</div>
		</div>
		<div><input type='submit' value='Actualizar'></input></div>
		<input type='hidden' name='idcurso' value='<?php echo $idcurso; ?>' />
		<input type='hidden' name='boton' value='<?php echo $boton; ?>' />
		<input type='hidden' name='idConvocatoriaBuscador' value='<?php echo $post['idConvocatoriaBuscador']; ?>' />
	</form>
</div>
<?php endif; ?>
