<div class="block block-themed themed-default">
	<div class="block-title"><h5>REALIZAR ALTA DE CURSO</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmInsertCurso" action='cursos/alta/<?php echo $get['idCurso'] ?>' method='post'>
        	<!-- div.row-fluid -->
            <div class="row-fluid">
            	<div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                		<label for="columns-text" class="control-label">D&iacute;as Tutor&iacute;as</label><div class="clear"></div>
	                		<div style="background-color:#DFDFDF;width:40%;text-align:center;border:1px solid #AAA">
	                			<label style="padding:2px 7px 2px 4px" for="lunes">L</label>
	                			<label style="padding:2px 7px 2px 6px" for="martes">M</label>
	                			<label style="padding:2px 7px 2px 6px" for="miercoles">X</label>
	                			<label style="padding:3px 7px 2px 6px" for="jueves">J</label>
	                			<label style="padding:2px 7px 2px 6px" for="viernes">V</label>
	                			<label style="padding:2px 7px 2px 6px" for="sabado">S</label>
	                			<label style="padding:2px 7px 2px 7px" for="domingo">D</label>
	                		</div>
	                		<div style="background-color:#EFEFEF;width:40%;text-align:center;border-left:1px solid #AAA;border-right:1px solid #AAA;border-bottom:1px solid #AAA">
								<input type="checkbox" name ='dias[]' value='1' style="margin:5px" id="lunes"/>
								<input type="checkbox" name ='dias[]' value='2' style="margin:5px" id="martes"/>
								<input type="checkbox" name ='dias[]' value='3' style="margin:5px" id="miercoles"/>
								<input type="checkbox" name ='dias[]' value='4' style="margin:5px" id="jueves"/>
								<input type="checkbox" name ='dias[]' value='5' style="margin:5px" id="viernes"/>
								<input type="checkbox" name ='dias[]' value='6' style="margin:5px" id="sabado"/>
								<input type="checkbox" name ='dias[]' value='7' style="margin:5px" id="domingo"/>
	                		</div>	                		
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Horas tutor&iacute;as</label><div class="clear"></div>
	                    	<input type="text" name="horaInicio" style="width:47%" placeholder="hh:mm"/> a <input type="text" name="horaFin" style="width:47%" placeholder="hh:mm"/>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>	            	
            	<div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                		<label for="columns-text" class="control-label">Supervisores</label>
							<select name="idSupervisor">
								<option value="">Seleccione un supervisor para el curso</option>
								<?php while($supervisor = mysqli_fetch_object($supervisores)):?>
									<?php if(in_array($supervisor->idrrhh, $miembrosStaff)):?>
										<option value="<?php echo $supervisor->idrrhh ?>" selected><?php echo $supervisor->nombrec ?></option>
									<?php else: ?>
										<option value="<?php echo $supervisor->idrrhh ?>" ><?php echo $supervisor->nombrec ?></option>
									<?php endif; ?>									
								<?php endwhile; ?>
							</select>
                        </div>
					</div>

					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">Coordinadores</label>
							<select name="idCoordinador">
								<option value="">Seleccione un coordinador para el curso</option>
								<?php while($coordinador = mysqli_fetch_object($coordinadores)):?>
									<?php if(in_array($coordinador->idrrhh, $miembrosStaff)):?>
										<option value="<?php echo $coordinador->idrrhh ?>" selected><?php echo $coordinador->nombrec ?></option>
									<?php else: ?>
										<option value="<?php echo $coordinador->idrrhh ?>"><?php echo $coordinador->nombrec ?></option>
									<?php endif; ?>
							<?php endwhile; ?>
							</select>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>	
	
            	<div>      
	            	<!-- Column -->
	            	<div class="span6">
	                	<div class="control-group">
	                		<label for="columns-text" class="control-label">Tutores</label>
	                		<input type="text" id="tutoresAltaCurso" name="nifTutor" class="small" value="" placeholder="Introduzca el NIF o nombre del tutor"/>
                        </div>
					</div>
	
					<!-- Column -->
                	<div class="span6">
	                    <div class="control-group">
	                    	<label for="columns-text" class="control-label">&nbsp;</label>
	                    	<div style="height: 19px;padding:5px;background-color:#4CB9FC;border:1px solid #0FA3FF;text-align:center;cursor:pointer" onclick="asignarTutor();return false"><a href="#" onclick="" style="color:#FFFFFF;width:100%;"><i class="icon-user"></i> Asignar tutor</a></div>
                        </div>
	                </div>	
		                
		            <div class="clear"></div>
		         </div>	
		         
		         <div>

		         	<!-- Column -->
	            	<div style="margin-top:20px;">
	                	<div>
	                		<table class="table table-hover">
                				<?php while($tutorDisponibles = mysqli_fetch_object($tutoresDisponibles)):?>
									<tr id="tutor_<?php echo $tutorDisponibles->nif ?>" class="<?php if(!in_array($tutorDisponibles->idrrhh, $miembrosStaff)) echo 'hide'?>">
									    <td class="span1 text-center"><?php echo $tutorDisponibles->nif; ?></td>
									    <td><?php echo $tutorDisponibles->nombrec; ?></td>
									    <td class="span1 text-center">
									    	<div class="btn-group">
									        	<a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="#" data-original-title="Eliminar" onclick="desasignarTutor('<?php echo $tutorDisponibles->nif ?>');return false"><i class="icon-remove"></i></a>
									        </div>
									    </td>
									</tr>
								<?php endwhile; ?>
							</table>
                        </div>
					</div>
		         </div>	         		                         	                
             </div>    
             <div><button class="btn btnFull btn-success" type="submit"><i class="icon-ok"></i> Realizar alta</button></div>     
             
             <!-- Aqui ponemos mediante jquery campos ocultos con los tutores asignados -->
             <div id="idTutoresAsignarGrupo">
             	<?php if(count($miembrosStaffTutores) > 0): ?>
             		<?php foreach($miembrosStaffTutores AS $tutorInput): ?>
             			<input type="hidden" name="idTutores[]" value="<?php echo $tutorInput ?>"  />
             		<?php endforeach; ?>
             	<?php endif; ?>
             </div>                  
		</form>
	</div>
</div>

<script type="text/javascript">
    $(function() {
	    var nifs = [
	        <?php while($tutor = mysqli_fetch_object($tutores)): ?>
	       		<?php echo '"' . htmlspecialchars($tutor->nif . ' - ' . $tutor->nombrec, ENT_QUOTES, 'UTF-8') . '",'; ?>
	        <?php endwhile; ?>
	   ];
		   
		$("#tutoresAltaCurso").autocomplete({source: nifs});
    });    
</script>
<script type="text/javascript" src="js-cursos-alta.js"></script>
