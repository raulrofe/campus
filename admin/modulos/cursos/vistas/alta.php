<div class="submenuAdmin">ALTA CURSOS</div>	
	<div class='addAdmin'><a href='cursos/alta/insertar'>Nueva alta de curso</a></div>
		<!-- Listado de convocatorias y cursos -->
		<div class='buscador'>
			<form name='cursos/alta' method='post' action='' class='t_center' >
				<select name='idConvocatoriaBuscador' onchange='this.form.submit()'>
					<option value=''> - Seleccione una Convocatoria - </option>
						<?php while($convocatoria = mysqli_fetch_assoc($convocatorias)):?>
							<?php if(isset($post['idConvocatoriaBuscador']) && $post['idConvocatoriaBuscador'] == $convocatoria['idconvocatoria']):?>
								<option value='<?php echo $convocatoria['idconvocatoria'];?>' selected><?php echo $convocatoria['nombre_convocatoria'];?></option>
							<?php else: ?>
								<option value='<?php echo $convocatoria['idconvocatoria'];?>'><?php echo $convocatoria['nombre_convocatoria'];?></option>
							<?php endif; ?>
						<?php endwhile;?>
				</select>
			</form>
			<br/>
			<form name='cursos/alta' method='post' action='' class='t_center' >
				<?php if(isset($registrosCursosBuscador)):?>
					<select name='idcurso'>
						<option value=''> - Seleccione un curso - </option>
						<?php while ($rowCurso = mysqli_fetch_assoc($registrosCursosBuscador)):?>
							<?php if(isset($post['idcurso']) && $rowCurso['idcurso'] == $post['idcurso']):?>
								<option value='<?php echo $rowCurso['idcurso']; ?>' selected><?php echo $rowCurso['titulo']; ?></option>
							<?php else: ?>
								<option value='<?php echo $rowCurso['idcurso']; ?>'><?php echo $rowCurso['titulo']; ?></option>
							<?php endif; ?>
						<?php endwhile; ?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
				<?php endif; ?>
				<br/><br/>
				<div class='t_center'>
					<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
				</div>
				<?php if(isset($post['idConvocatoriaBuscador'])):?>
					<input type='hidden' name='idConvocatoriaBuscador' value='<?php echo $post['idConvocatoriaBuscador']?>' />
				<?php endif; ?>
			</form>
		</div>
		<!-- Fin listado convocatorias y cursos -->
		
<?php if(isset($boton) && $boton == 'Insertar' && !isset($post['idConvocatoriaBuscador'])):?>
	<p class='subtitleAdmin'>NUEVA ALTA CURSO</p>
	<div class='formulariosAdmin'>
		<form name='frm_curso_convocatoria' action='cursos/alta' method='post' enctype="multipart/form-data">
			<div class='filafrm'>
				<div class='etiquetafrm'>Convocatoria</div>
				<div class='campofrm'>
					<select name='idConvocatoria' onchange='this.form.submit()'>
						<option value=''> - Seleccione una convocatoria - </option>
						<?php while($convocat = mysqli_fetch_assoc($convocatorias2)):?>
							<?php if($convocat['idconvocatoria'] == $post['idConvocatoria']):?>
								<option value='<?php echo $convocat['idconvocatoria'];?>' selected ><?php echo $convocat['nombre_convocatoria']?></option>
							<?php else: ?>
								<option value='<?php echo $convocat['idconvocatoria'];?>'><?php echo $convocat['nombre_convocatoria']?></option>
							<?php endif; ?>
						<?php endwhile; ?>
					</select>
				</div>
			</div>
			<input type='hidden' name='boton' value='<?php echo $boton; ?>' />
		</form>
		<form name='frm_curso' action='cursos/alta' method='post' enctype="multipart/form-data">
			<div class='filafrm'>
				<div class='etiquetafrm'>Cursos disponibles</div>
				<div class='campofrm'>
					<select name='idcurso'>
						<option value=''> - Seleccione un curso - </option>
						<?php if(isset($registrosCursos)):?>
							<?php while ($rowCurso = mysqli_fetch_assoc($registrosCursos)):?>
								<?php if($rowCurso['idcurso'] == $post['idcurso']):?>
									<option value='<?php echo $rowCurso['idcurso']; ?>' selected><?php echo $rowCurso['titulo']; ?></option>
								<?php else: ?>
									<option value='<?php echo $rowCurso['idcurso']; ?>'><?php echo $rowCurso['titulo']; ?></option>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Coordinador</div>
				<div class='campofrm'><?php $mi_usuario->select_usuarios_perfil(3);?></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>supervisor</div>
				<div class='campofrm'><?php $mi_usuario->select_usuarios_perfil(2);?></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tutor principal</div>
				<div class='campofrm'><?php $mi_usuario->select_usuarios_perfil(4);?></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tutor secundario</div>
				<div class='campofrm'><?php $mi_usuario->select_usuarios_perfil(4);?></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Mostrar en cat&aacute;logo</div>
				<div class='campofrm'><input type='checkbox' name='mostrar' value='1'/></div>
			</div>			
			<?php if(isset($post['idConvocatoria'])):?>
				<input type='hidden' name='idConvocatoria' value='<?php echo $post['idConvocatoria']; ?>' />
			<?php endif; ?>		
			<input type='hidden' name='boton' value='Insertar' />	
			<div><input type='submit' value='Insertar'></input></div>
		</form> 
	</div>
<?php endif;?>		
<?php 
if(isset($boton) && $boton == 'Actualizar')
{
	require_once('actualizar_alta.php');
}
?>	
 