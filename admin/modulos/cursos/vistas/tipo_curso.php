<div id="modulesList">
	<?php if($registros->num_rows > 0):?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th>Tipo curso</th>
	                <th class="span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
	  		<?php while($tipoCurso = $registros->fetch_object()):?>
	        	<tr>
	                <td><a href="contenidos/editar/<?php echo $tipoCurso->idrama_curso ?>"><?php echo $tipoCurso->rama_curso ?></a></td>
	                <td class="span1 text-center">
	                	<div class="btn-group">
	                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="cursos/editar/tipo_curso/<?php echo $tipoCurso->idrama_curso ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
	                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="cursos/eliminar/tipo_curso/<?php echo $tipoCurso->idrama_curso ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
	                    </div>
	                </td>
	            </tr>
	  		<?php endwhile; ?>
	  	</table> 
	<?php else: ?>
		<div class="text-center">No existen tipos de curso</div>
	<?php endif;?>
</div>

<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar este tipo de curso?');</script>