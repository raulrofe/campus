<?php if(isset($la_convocatoria)):?>
	<p class='subtitleAdmin'>ACTUALIZAR DATOS CONVOCATORIA</p>
	<div class='formulariosAdmin'>
		<form name='frm_curso' action='cursos/convocatorias' method='post' enctype="multipart/form-data" id="frmNuevaConvocatoria">
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo convocatoria</div>
				<div class='campofrm'><input type='text' name='titulo' size='60' value='<?php echo $la_convocatoria['nombre_convocatoria']; ?>' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Modalidad</div>
				<div class='campofrm'>
					<select name='metodologia' class='select100'>
						<option value='7' <?php if($la_convocatoria['metodologia'] == 7) echo 'selected'?> >Presencial</option>
						<option value='8' <?php if($la_convocatoria['metodologia'] == 8) echo 'selected'?> >Distacia</option>
						<option value='9' <?php if($la_convocatoria['metodologia'] == 9) echo 'selected'?> >Mixta</option>
						<option value='10' <?php if($la_convocatoria['metodologia'] == 10) echo 'selected'?> >Teleformaci&oacute;n</option>
					</select>
				</div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>N&uacute;mero  horas</div>
				<div class='campofrm'><input type='text' name='n_horas' size='60' value='<?php echo $la_convocatoria['n_horas']; ?>' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha inicio</div>
				<div class='campofrm'><input type='text' name='f_inicio' id='f_inicio' size='60' value='<?php echo $fechai; ?>'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha finalizaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_fin' id='f_fin' size='60' value='<?php echo $fechaf; ?>'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Inicio matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='i_matriculacion' id='i_matriculacion' size='60' value='<?php echo $i_mat; ?>' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fin matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_matriculacion' id='f_matriculacion' size='60' value='<?php echo $f_mat; ?>' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<!-- 
			<div class='filafrm'>
				<div class='etiquetafrm'>Codigo tripartita curso</div>
				<div class='campofrm'><input type='text' name='codConv' value='<?php //echo $la_convocatoria['codigo_convocatoria']; ?>'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tipo acci&oacute;n</div>
				<div class='campofrm'>
					<select name='tipoAccion'>
						<option value='0' <?php //if($la_convocatoria['tipo_accion'] == 0) echo 'selected'?> >Gen&eacute;rica</option>
						<option value='1' <?php //if($la_convocatoria['tipo_accion'] == 1) echo 'selected'?> >Espec&iacute;fica</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Nivel Formaci&oacute;n</div>
				<div class='campofrm'>
					<select name='nivelFormacion'>
						<option value='0' <?php //if($la_convocatoria['nivel_formacion'] == 0) echo 'selected'?> >B&aacute;sico</option>
						<option value='1' <?php //if($la_convocatoria['nivel_formacion'] == 1) echo 'selected'?> >Superior</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Acceso tripartita</div>
				<div class='campofrm'>
					<select name='accesoTri'>
						<option value='0' <?php //if($la_convocatoria['uri'] == 0) echo 'selected'?> >No ex&iacute;ge</option>
						<option value='1' <?php //if($la_convocatoria['uri'] == 1) echo 'selected'?> >Si exige</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Usuario tripartita</div>
				<div class='campofrm'><input type='text' name='usuarioTri' size='60' value='<?php //echo $la_convocatoria['usuario']; ?>'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Password tripartita</div>
				<div class='campofrm'><input type='text' name='passTri' size='60' value='<?php //echo $la_convocatoria['pass']; ?>' /></div>
			</div>
			 -->
			<div class='filafrm'>
				<div class='etiquetafrm'>observaciones</div>
				<div class='campofrm'><textarea name='observaciones' class='estilotextarea' style="width:98%;" ><?php echo $la_convocatoria['observaciones']; ?></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>modelo tecnologia</div>
				<div class='campofrm'><input type='text' name='modeloTec' size='60' value='<?php echo $la_convocatoria['modelo_tecnologia']; ?>' /></div>
			</div>
			<div class='filafrm <?php if($la_convocatoria['metodologia'] != 9) echo 'hide'?>'>
				<div class='etiquetafrm'>horas presenciales</div>
				<div class='campofrm'><input type='text' name='horaPre' size='60' value='<?php echo $la_convocatoria['horapre']; ?>' /></div>
			</div>
			<div class='filafrm <?php if($la_convocatoria['metodologia'] != 9) echo 'hide'?>'>
				<div class='etiquetafrm'>horas teleformaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='horaTel' size='60' value='<?php echo $la_convocatoria['horate']; ?>' /></div>
			</div>			
			<div><input type='submit' value='Actualizar'></input></div>
			<input type='hidden' name='idconvocatorias' value='<?php  echo $idconvocatorias; ?>' />
			<input type='hidden' name='boton' value='Actualizar' />
		</form>
		</div>

<br/>	

<script>crearCalendario('f_inicio')</script>
<script>crearCalendario('f_fin')</script>
<script>crearCalendario('i_matriculacion')</script>
<script>crearCalendario('f_matriculacion')</script>
<script type="text/javascript" src="js-cursos-editarconvocatoria.js"></script>
<?php endif;?>