<!-- Listado de Cursos -->
<div id="cursosList">
	<?php if($cursos->num_rows > 0):?>
		<table class="table table-hover">
			<thead>
	        	<tr>
	                <th>Nombre Curso</th>
	                <th>Fecha inicio</th>
	                <th>Fecha fin</th>
	                <th class="span1 text-center"><i class="icon-cogs"></i></th>
				</tr>
			</thead>
	  		<?php while($curso = $cursos->fetch_object()):?>
	        	<tr>
	                <td><a href="contenidos/editar/<?php echo $curso->idcurso ?>"><?php echo $curso->titulo ?></a></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_inicio, '-', '/') ?></td>
	                <td><?php echo Fecha::invertir_fecha($curso->f_fin, '-', '/') ?></td>
	                <td class="span1 text-center">
	                	<div class="btn-group">
	                    	<a class="btn btn-mini btn-success" title="" data-toggle="tooltip" href="cursos/editar/curso/<?php echo $curso->idcurso ?>" data-original-title="Edit"><i class="icon-pencil"></i></a>
	                        <a class="btn btn-mini btn-danger" title="" data-toggle="tooltip" href="cursos/eliminar/<?php echo $curso->idcurso ?>" data-original-title="Delete"><i class="icon-remove"></i></a>
	                    </div>
	                </td>
	            </tr>
	  		<?php endwhile; ?>
	  	</table> 
	<?php else: ?>
		<div class="text-center">No existen cursos</div>
	<?php endif;?>
</div>

<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar este curso para esta convocatoria?');</script>