<h5>IDS DE REFERENCIA DE CURSOS</h5>
<table class="table table-hover">
	<thead>
    	<tr>
            <th class="text-center">Id curso</th>
            <th>T&iacute;tulo curso</th>
		</tr>
	</thead>
	<?php while($curso = mysqli_fetch_object($cursos)): ?>
    	<tr>
        	<td class="text-center"><?php echo $curso->idcurso ?></td>
            <td><?php echo $curso->titulo ?></td>
        </tr>
	<?php endwhile; ?>
</table> 