	<p class='subtitleAdmin'>INTRODUCIR CONVOCATORIA</p>
	<div class='formulariosAdmin'>
		<form id="frmNuevaConvocatoria" action='cursos/convocatorias' method='post' enctype="multipart/form-data">
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo convocatoria</div>
				<div class='campofrm'><input type='text' name='titulo' size='60'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Modalidad</div>
				<div class='campofrm'>
					<select name='metodologia' onchange="avisos_personales_seleccionar_repeticion(this);" class='select100'>
						<option value='7'>Presencial</option>
						<option value='8'>Distancia</option>
						<option value='9'>Mixta</option>
						<option value='10' selected >Teleformaci&oacute;n</option>
					</select>
				</div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>N&uacute;mero  horas</div>
				<div class='campofrm'><input type='text' name='n_horas' size='60' /></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha inicio</div>
				<div class='campofrm'><input type='text' name='f_inicio' id='f_inicio' size='60' value='dd/mm/aaaa'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha finalizaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_fin' id='f_fin' size='60' value='dd/mm/aaaa'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Inicio matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='i_matriculacion' id='i_matriculacion' size='60' value='dd/mm/aaaa'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fin matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_matriculacion' id='f_matriculacion' size='60' value='dd/mm/aaaa'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<!-- 
			<div class='filafrm'>
				<div class='etiquetafrm'>Codigo tripartita curso</div>
				<div class='campofrm'><input type='text' name='codConv' value='115-00'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Tipo acci&oacute;n</div>
				<div class='campofrm'>
					<select name='tipoAccion'>
						<option value='0'>Gen&eacute;rica</option>
						<option value='1' selected >Espec&iacute;fica</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Nivel Formaci&oacute;n</div>
				<div class='campofrm'>
					<select name='nivelFormacion'>
						<option value='0'>B&aacute;sico</option>
						<option value='1' selected >Superior</option>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Acceso tripartita</div>
				<div class='campofrm'>
					<input type='text' name='accesoTri' value='http://www.campusaulainteractiva.com' size='60'/>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Usuario tripartita</div>
				<div class='campofrm'><input type='text' name='usuarioTri' size='60' value='Supervisor' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Password tripartita</div>
				<div class='campofrm'><input type='text' name='passTri' size='60' value='tripartita' /></div>
			</div>
			 -->
			<div class='filafrm'>
				<div class='etiquetafrm'>Observaciones</div>
				<div class='campofrm'><textarea name='observaciones' class='estilotextarea' style="width:98%;"></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Modelo tecnologia</div>
				<div class='campofrm'><input type='text' name='modeloTec' size='60' /></div>
			</div>
			<div id='mixta' class='hide'>
				<div class='filafrm'>
					<div class='etiquetafrm'>horas presenciales</div>
					<div class='campofrm'><input type='text' name='horaPre' size='60' value='0'/></div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>horas teleformaci&oacute;n</div>
					<div class='campofrm'><input type='text' name='horaTel' size='60' value='0'/></div>
				</div>
			</div>
			<div class='filafrm'>
				<div class='obligatorio'>campos obligatorios(*)</div>
			</div>	
			<div><input type='submit' value='Insertar'></input></div>
			<input type='hidden' name='boton' value='Insertar' />
		</form>
	</div>	
<script>crearCalendario('f_inicio');</script>
<script>crearCalendario('f_fin');</script>
<script>crearCalendario('i_matriculacion');</script>
<script>crearCalendario('f_matriculacion');</script>
<script type="text/javascript" src="js-cursos-formacionmixta.js"></script>
<script type="text/javascript" src="js-cursos-nuevaconvocatoria.js"></script>
