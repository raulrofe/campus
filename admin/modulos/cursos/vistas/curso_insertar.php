<div class="block block-themed themed-default">
	<div class="block-title"><h5>INTRODUCIR CURSO</h5></div>
	<div class="block-content full">
		<form class="form-inline" enctype="multipart/form-data" id="frmInsertCurso" action='cursos/insertar' method='post'>
        	<!-- div.row-fluid -->
            <div  id="wizard" class="row-fluid">
            	<h2>Datos curso</h2>           	
            	<section style="overflow:auto">
            		<div class="progress progress-success progress-striped active"><div style="width: 25%" class="bar"></div></div> 
	                <div>      
		            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">Nombre curso</label>
	                        	<div class="controls"><input type='text' name='titulo' size='60'/></div>
	                        </div>
						</div>
		
						<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">N&uacute;mero horas</label>
		                        <div class="controls"><input type='text' name='n_horas' size='60' /></div>
	                        </div>
		                </div>	
			                
			            <div class="clear"></div>
			         </div>

	                <div>      
		            	<!-- Column -->
		            	<div class="span6">
		                	<div class="control-group">
		                    	<label for="columns-text" class="control-label">C&oacute;digo Acci&oacute;n Formativa</label>
	                        	<div class="controls"><input type='text' name='caf' maxlegnth="5" ></div>
	                        </div>
						</div>
		
						<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                    	<label for="columns-text" class="control-label">C&oacute;digo Grupo</label>
		                        <div class="controls"><input type='text' name='cg' maxlegnth='5' /></div>
	                        </div>
		                </div>	
			                
			            <div class="clear"></div>
			         </div>

		             <div>      
			         	<!-- Column -->
		             	<div class="span6">
		             		<div class="control-group">
		             			<label for="columns-text" class="control-label">Fecha inicio</label>
		             			<input id="input-datepicker" class="input-small input-datepicker" type="text" name="f_inicio" data-date-weekStart="1" data-date-format="dd/mm/yyyy">       					
		                    </div>
			            </div>	
			
						<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                		<label for="columns-text" class="control-label">Fecha fin</label>
		             			<input id="input-datepicker" class="input-small input-datepicker" type="text" name="f_fin" data-date-weekStart="1" data-date-format="dd/mm/yyyy"> 
	                       	</div>
		                </div>	
				                
			            <div class="clear"></div>
			         </div>

	                 <div>      
		            	<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                		<label for="columns-text" class="control-label">Fecha inicio matriculaci&oacute;n</label>
		                		<input id="input-datepicker" class="input-small input-datepicker" type="text" name="i_matriuclacion" data-date-weekStart="1" data-date-format="dd/mm/yyyy">  
	                       	</div>
		                </div>	
		
						<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                		<label for="columns-text" class="control-label">Fecha fin matriculaci&oacute;n</label>
	                       		<input id="input-datepicker" class="input-small input-datepicker" type="text" name="f_matriuclacion" data-date-weekStart="1" data-date-format="dd/mm/yyyy">
	                       	</div>
		                </div>	
			                
			            <div class="clear"></div>
			          </div>

	                 <div>      
		            	<!-- Column -->
	                	<div class="span6">
		                    <div class="control-group">
		                		<label for="columns-text" class="control-label">Modalidad</label>
	                       		<div class="controls"><input type='text' name='metodologia' size='60' /></div>
	                       	</div>
		                </div>	
	
	                	<div class="span6">
		                    <div class="control-group">
		                		<label for="columns-text" class="control-label">&nbsp;</label>
	                       		<div class="controls">
	                       			<input type='checkbox' name='guiaDidactica' size='60' /><label style="margin-left:5px;">Genera gu&iacute;a did&aacute;ctica</label>
	                       		</div>
	                       	</div>
		                </div>	
		                
			            <div class="clear"></div>
			          </div>    

	                <div>      
						<!-- Column -->
	                	<div>
		                    <div>
		                		<label for="columns-text" class="control-label">Descripci&oacute;n</label>
	                       		<div><textarea name='descripcion' class="AreaFullWidth" style="height:200px;"></textarea></div>
	                       	</div>
		                </div>	
			                
			            <div class="clear"></div>
			         </div>
			        
			     </section>
			     <h2>Asignar m&oacute;dulos</h2>		    
			     <section style="overflow:auto">
 			     	<div class="progress progress-success progress-striped active"><div style="width: 50%" class="bar"></div></div>		
			     	<?php if($modulos->num_rows > 0): ?>
		     			<?php while($moduloSelect = mysqli_fetch_object($modulosSelect)):?>
		     				<div id="myCheckBox">
		     					<input type="checkbox" name="idModulo[]" value="<?php echo $moduloSelect->idmodulo ?>" id="forText<?php echo $moduloSelect->idmodulo ?>"class="checkModulos" onclick="viewImportContent(this);"/>
		     					<label for="forText<?php echo $moduloSelect->idmodulo ?>"><?php echo '(' . $moduloSelect->referencia_modulo . ') ' . $moduloSelect->nombre ?></label>
		     					<div class="clear"></div>
		     				</div>
		     			<?php endwhile; ?>
			     	<?php endif; ?> 		     
			     </section>
			     <h2>Asignar contenidos</h2>	
			     <section style="overflow:auto">	
			     	<div class="progress progress-success progress-striped active"><div style="width: 75%" class="bar"></div></div>
			     	<?php foreach($modulosAndContent as $clave => $contentImport): ?>
			     		<div class="hide contenidos <?php echo $clave ?>">		     		
			     			<!-- Temario -->
			     			<?php if(isset($contentImport['temario'])): ?>
			     				<h4>Contenido sin SCORM</h4>
			     				<ul>
								<?php foreach($contentImport['temario'] as $temas):?>
									<li>
										<input type="checkbox" name="idTemas[]" value="<?php echo $temas['idTema'] ?>" checked disabled="disabled" />
										<label><?php echo $temas['tituloTema']?></label>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							
							<!-- Contenido SCORM -->
			     			<?php if(isset($contentImport['scorm'])): ?>
			     				<h4>Contenido SCORM</h4>
			     				<ul>
								<?php foreach($contentImport['scorm'] as $scorms):?>
									<li>
										<input type="checkbox" name="idScorms[]" value="<?php echo $scorms['idScorm'] ?>" checked disabled="disabled" />
										<label><?php echo $scorms['tituloScorm']?></label>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>																		
			     		</div>
			     	<?php endforeach; ?>
			     </section>  
				<h2>Asignar tareas</h2>	
			    <section style="overflow:auto">		     	
			    	<div class="progress progress-success progress-striped active"><div style="width: 100%" class="bar"></div></div>
			     	<?php foreach($modulosAndTask as $clave => $taskImport): ?>
			     		<div class="hide contenidos <?php echo $clave ?>">
							<!-- AUTOEVALUACIONES -->
							<?php if(isset($taskImport['autoevaluacion'])): ?>
								<h4>Autoevaluaciones</h4>
								<ul>
									<?php foreach($taskImport['autoevaluacion'] as $autoevaluaciones): ?>
										<li>
											<input type="checkbox" name="idAutoevaluacionesModulo[]" value="<?php echo $autoevaluaciones['idAutoevalModulo'] ?>" checked disabled="disabled" />
											<label><?php echo $autoevaluaciones['tituloAutoevaluacion']?></label>				
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							
							<!-- Trabajos practicos -->
			     			<?php if(isset($taskImport['trabajoPractico'])): ?>
			     				<h4>Trabajos pr&aacute;cticos</h4>
			     				<ul>
								<?php foreach($taskImport['trabajoPractico'] as $trabajosPracticos):?>
									<li>
										<input type="checkbox" name="idTrabajosPracticos[]" value="<?php echo $trabajosPracticos['idTrabajoPractico'] ?>" checked disabled="disabled" />
										<label><?php echo $trabajosPracticos['tituloTrabajoPractico']?></label>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
	
							<!-- Foros -->
			     			<?php if(isset($taskImport['foro'])): ?>
			     				<h4>Temas de Foros</h4>
			     				<ul>
								<?php foreach($taskImport['foro'] as $foros):?>
									<li>
										<input type="checkbox" name="idTemaforos[]" value="<?php echo $foros['idForo'] ?>" checked disabled="disabled" />
										<label><?php echo $foros['tituloForo']?></label>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>												
			     		</div>
			     	<?php endforeach; ?>
			     </section>  	                	                
             </div>                          
		</form>
	</div>
</div>
 
<script>
	$(function ()
    {
    	$("#wizard").steps({
	        headerTag: "h2",
	        bodyTag: "section",
	        transitionEffect: "slideLeft",
	        labels: {
	        	finish: "Finalizar",
	        	next: "Siguiente",
	        	previous: "Anterior",
	        	loading: "Cargando ..."
	        },
	        onStepChanging: function (event, currentIndex, newIndex)
	        {
	            $("#frmInsertCurso").validate().settings.ignore = ":disabled,:hidden";
	            return $("#frmInsertCurso").valid();
	        },
	        onFinishing: function (event, currentIndex)
	        {
	            $("#frmInsertCurso").validate().settings.ignore = ":disabled";
	            return $("#frmInsertCurso").valid();
	        },
	        onFinished: function (event, currentIndex)
	        {
	        	$("#frmInsertCurso").submit();
	        }
        });
    });
</script>
<script type="text/javascript" src="js-cursos-insert.js"></script>