<div class="submenuAdmin">CURSOS</div>

<div class='addAdmin'><a href='cursos/insertar'>Introducir Curso</a></div>
<!-- Listado de Cursos -->
	<div class='buscador'>
		<P class='t_center'>Cursos introducidos en este momento en el sistema, seleccion antes una convocatoria par verlos:</P><br/>
			<form name='busca_tipo_curso' action='cursos' method='post'>
				<div class='t_center'>
					<select name='idConvocatoria' onchange='this.form.submit();'>
						<option value=''> - Seleccione una convocatoria - </option>
						<?php while ($convocat = mysqli_fetch_assoc($resultado)):?>
							<?php if(isset($post['idConvocatoria']) && $post['idConvocatoria'] == $convocat['idconvocatoria']):?>
								<option value='<?php echo $convocat['idconvocatoria']; ?>' selected><?php echo $convocat['nombre_convocatoria']; ?></option>
							<?php else:?>
								<option value='<?php echo $convocat['idconvocatoria']; ?>'><?php echo $convocat['nombre_convocatoria']; ?></option>
							<?php endif;?>
						<?php endwhile;?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
				<br/>
			</form>
			<form name='busca_tipo_curso' action='cursos' method='post'>
				<div class='t_center'>
					<?php if(isset($cursos)):?>
						<select name='idCurso'>
							<option value=''> - Seleccione un curso - </option>
							<?php while ($curso = mysqli_fetch_assoc($cursos)):?>
								<?php if(isset($post['idCurso']) && $post['idCurso'] == $curso['idcurso']):?>
									<option value='<?php echo $curso['idcurso']; ?>' selected><?php echo $curso['titulo']; ?></option>
								<?php else: ?>
									<option value='<?php echo $curso['idcurso']; ?>'><?php echo $curso['titulo']; ?></option>
								<?php endif; ?>
							<?php endwhile;?>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;
					<?php endif; ?>	
				</div>
				<br/>
				<div class='t_center'>
					<?php if(isset ($post['idConvocatoria'])):?>
						<span class='boton_borrar'><input type='submit' name='boton' value='Actualizar' /></span>
						<span class='boton_borrar'><input id="frmBtnBorrar" type='submit' name='boton' value='Borrar' /></span>
					<?php endif;?>
				</div>
				<?php if(isset ($post['idConvocatoria'])):?>
					<input type='hidden' name='idConvocatoria' value='<?php echo $post['idConvocatoria']?>' />
				<?php endif; ?>
			</form>

	</div>
<?php if(isset($get['boton']) && $get['boton'] == 'Insertar'):?>
<!-- fin Listado de convocatorias -->
	<p class='subtitleAdmin'>INTRODUCIR CURSO</p>
	<div class='formulariosAdmin'>
		<form name='frm_curso' action='' method='post' enctype="multipart/form-data">
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;tulo curso</div>
				<div class='campofrm'><input type='text' name='titulo' size='60'/></div>
				<div class='obligatorio'>(*)</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Convocatoria</div>
				<div class='campofrm'>
					<select name='id_convocatoria'>
						<?php 
						while($convocatoria = mysqli_fetch_assoc($resultadoConvocatoria))
						{
							echo "<option value='".$convocatoria['idconvocatoria']."'>".$convocatoria['nombre_convocatoria']."</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Acciones Formativas</div>
				<div class='campofrm'>
					<select name='idaf'>
						<?php 
							while($registros_af = mysqli_fetch_assoc($resultado_af))
							{
								echo "<option value='".$registros_af['idaccion_formativa']."'>".$registros_af['accion_formativa']."</option>";
							}
						?>
					</select>
				</div>	
			</div>
	
	
	
			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;po de curso</div>
				<div class='campofrm'><?php $mi_curso->select_tipo_curso_noauto($registros,$idtipo_curso); ?></div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm'>T&iacute;po de curso</div>
				<div class='campofrm'><?php //$mi_curso->select_tipo_curso($registros,$idtipo_curso); ?></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Metodolog&iacute;a</div>
				<div class='campofrm'><input type='text' name='metodologia' size='60' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>N&uacute;mero  horas</div>
				<div class='campofrm'><input type='text' name='n_horas' size='60' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha inicio</div>
				<div class='campofrm'><input type='text' name='f_inicio' size='60' value='dd-mm-aaaa'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha finalizaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_fin' size='60' value='dd-mm-aaaa'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Inicio matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='i_matriculacion' size='60' value='dd-mm-aaaa'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fin matriculaci&oacute;n</div>
				<div class='campofrm'><input type='text' name='f_matriculacion' size='60' value='dd-mm-aaaa'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Descripci&oacute;n</div>
				<div class='campofrm'><textarea name='descripcion' class='estilotextarea'></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Gu&iacute;a did&aacute;ctica</div>
				<div class='campofrm'><input type="file" name='guiaDidactica' /></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Imagen gu&iacute;a did&aacute;ctica</div>
				<div class='campofrm'><input type="file" name='guiaDidacticaImagen' /></div>
			</div>
			<div class='filafrm'>
				<div class='obligatorio'>Campos obligatorios (*)</div>
			</div>					
			<div><input type='submit' value='Insertar'></input></div>
		</form>
	</div>
<?php endif; ?>
<?php 
	if(isset($post['boton']) && $post['boton'] == "Actualizar")
	{
		require_once('actualizar.php');
	}
?>

<script type="text/javascript">alertConfirmBtn('¿Quieres eliminar este curso para esta convocatoria?');</script>