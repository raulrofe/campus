<?php

class AdminCursos
{
	public static function generarGuiaDidactica($idcurso)
	{
		require_once(PATH_ROOT.'lib/createpdf/html2pdf.class.php');
		ob_clean();
		
		$objModulos = new Modulos();
		$idUltimoCurso  = $idcurso;

		//Obtengo los datos del curso
		$mi_curso = new Cursos();
		$mi_curso->set_curso($idcurso);
		$registrosCursos = $mi_curso->buscar_curso();
		$registrosCurso = mysqli_fetch_assoc($registrosCursos);
		
		$filename = Texto::clearFilename($registrosCurso['titulo']);
			
		if(file_exists(PATH_ROOT . 'archivos/guias_didacticas/' . $idUltimoCurso . '.pdf'))
		{
			chmod($filename, 0777);
			unlink($filename);
		}
		
		//cargo estilos, cabecera, pie y cuerpo de las paginas para el pdf
		require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/estiloguia.php';
		require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/cabecera_pdf.php';
		require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/pie_pdf.php';
		require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/cuerpo_pdf.php';
			
		$html=
			$estilo.
			'<page backbottom="25mm" backtop="25mm" backleft="15mm" backright="0mm"> 
			    <page_header>'.$cabecera.'</page_header> 
			    <page_footer>'.$pie.'</page_footer>'. 
				$cuerpo.
			'</page>';
			
			$content = ob_get_clean();
			
			try
			{
				$html2pdf = new HTML2PDF();
				$html2pdf->setDefaultFont('Arial');
				$html2pdf->writeHTML($html);
				$html2pdf->Output('../archivos/guias_didacticas/'. $idUltimoCurso .'.pdf','F');
			}
			catch(HTML2PDF_exception $e)
			{
				//echo $e;
			}
			
			ob_end_clean();
	}
}