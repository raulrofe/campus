<?php 

$estilo =
'
<style>
body
{
	font-size:11pt;
	text-align:justify;	
}

.fleft{float:left;}
.fright:{float:right;}

.cabeceraGuia
{
	padding-bottom:5px;
	text-align:left;
	border-bottom:2px dotted #0084c5;
	width:640px;
	margin-left:60px;
	margin-top:43px;
}

.pieGuia
{
	width:500px;
}

.paginasGuia
{
	line-height:20px;
	text-align:justify;
	width:635px;
	margin-top:43px;
}

.tituloNoImagen
{
	font-weight:bold;
	text-align:center;
	font-size:12pt;
}

.tituloConImagen
{
	font-weight:bold;
	font-size:12pt;	
	margin-left:40px;
	line-height:35px;
}

.textoGuia
{
	text-indent:40px;
	text-align:justify;
}

.tituloModuloGuia
{
	color:#0084c5;
	font-weight:bold;
	font-size:12pt;
}

.listadoGuia
{
	margin-left:40px;
}

</style>';

?>
