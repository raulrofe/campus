<?php

ob_clean();

//cargo clase generacion php
// conversion HTML => PDF
require_once(PATH_ROOT.'lib/createpdf/html2pdf.class.php');

//Obtengo los datos del curso
$mi_curso->set_curso($idUltimoCurso);
$registrosCursos = $mi_curso->buscar_curso();
$registrosCurso = mysqli_fetch_assoc($registrosCursos);

//cargo estilos, cabecera, pie y cuerpo de las paginas para el pdf
require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/estiloguia.php';
require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/cabecera_pdf.php';
require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/pie_pdf.php';
require PATH_ROOT. 'admin/modulos/cursos/guiadidactica/cuerpo_pdf.php';

$html=
	$estilo.
	'<page backbottom="20mm" backtop="25mm" backleft="15mm" backright="15mm"> 
	    <page_header>'.$cabecera.'</page_header> 
	    <page_footer>'.$pie.'</page_footer>'. 
		$cuerpo.
	'</page>';

	$content = ob_get_clean();
	
	try
	{
		$html2pdf = new HTML2PDF();
//		$html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML(utf8_decode($html));
		$html2pdf->Output('../archivos/guias_didacticas/'.Texto::clearFilename($registrosCurso['titulo']).'.pdf','F');
	}
	catch(HTML2PDF_exception $e) { echo $e; }

?>
