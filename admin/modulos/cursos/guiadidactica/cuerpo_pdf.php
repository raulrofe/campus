<?php

$registrosModulos = $objModulos->modulos_asignado_curso($idUltimoCurso);

$cuerpo =
	'<div class="paginasGuia">'.
		'<p class="tituloNoImagen">GUIA DID&Aacute;CTICA DEL CURSO NUEVAS TECNOLOG&Iacute;AS APLICADAS A LA EDUCACI&Oacute;N</p>'.
		'<p class="textoGuia">En este documento se incluyen las instrucciones y consejos necesarios para la comprensión del material propuesto, junto con una breve introducción al mismo. Se presenta igualmente el contenido, objetivos, metodología, evaluación y los recursos disponibles en el curso.</p>'.
		'<p class="tituloConImagen">
			<img src="'.URL_BASE.'imagenes/guiadidactica/raya_vertical.jpg">
			PRESENTACI&Oacute;N
		</p>'.
		'<p class="textoGuia">Las Tecnologías de la Información y Comunicación se están implantando de forma vertiginosa en nuestra sociedad. Como bien sabemos, en el sector educativo comienzan a crearse multitud de centros TIC donde los ordenadores ocupan la mayoría de las aulas, además, la formación se abre caminos en Internet creando espacios específicos. Por tanto, los conocimientos y destrezas en este campo constituyen un factor clave para el desarrollo intelectual y profesional de todas las personas. Por este motivo, pensamos que es necesario que los profesionales de la educación se “pongan al día” en todo el amplio espectro que comportan dichos términos y además, por varias razones:</p>'.
		'<p class="textoGuia">En primer lugar porque los centros educativos no pueden dar la espalda a la realidad. Nuestros niños y nuestros jóvenes están en contacto permanente con estas Nuevas Tecnologías que llevan consigo tanto la apertura a nuevos saberes y experiencias positivas y lúdicas como la posibilidad de consecuencias negativas y alienantes.</p>'.
		'<p class="textoGuia">En segundo lugar entendemos que el dominio de estas tecnologías contribuye a la profesionalización del docente, dado el amplio campo de posibilidades didácticas que conlleva.</p>'.
		'<p class="textoGuia">En tercer lugar consideramos que el grado de la penetración tecnológica en una sociedad va a depender del acceso cognoscitivo y experimental que las personas tengan del universo tecnológico y, por tanto, el sistema educativo debe jugar un papel primordial, en la iniciación y en el proceso de familiarización con los medios y el material que la tecnología pone a nuestro alcance.</p>'.
		'<p class="textoGuia">La Ley Orgánica 2/2006, de 3 de mayo, de Educación (LOE) recoge en su preámbulo que, tanto la Unión Europea como la UNESCO, “se ha propuesto mejorar la calidad y la eficacia de los sistemas de educación y de formación, lo que implica mejorar la capacitación de</p>'.
		'<p class="textoGuia">los docentes, desarrollar las aptitudes necesarias para la sociedad del conocimiento, garantizar el acceso de todos a las tecnologías de la información y la comunicación [...]”</p>'.
		'<p class="textoGuia">En esta situación se trata de dar una respuesta adecuada a la dificultad que supone orientarse entre la diversidad de soportes disponibles para el tratamiento de la información y su utilización educativa.</p>'.
		'<p class="textoGuia">Se pretende, en definitiva, contribuir a la formación permanente del profesorado en esta novedosa e imprescindible parcela de su quehacer educativo.</p>'.
		'<p class="tituloConImagen">
			<img src="'.URL_BASE.'imagenes/guiadidactica/raya_vertical.jpg">
			CONTENIDOS Y OBJETIVOS
		</p>';

		while($registrosModulo = mysqli_fetch_assoc($registrosModulos))
		{
			$cuerpo.='<p class="textoGuia">M&oacute;dulo: '.$registrosModulo['nombre'].'</p>';
			$cuerpo.='<p class="listadoGuia">'.nl2br($registrosModulo['contenidos']).'</p>';
			$cuerpo.='<p class="textoGuia">Objetivos:</p>';
			$cuerpo.= '<p class="listadoGuia">';
			
			$c = explode("\n", trim($registrosModulo['objetivos']));
			foreach($c as $n)
			{
				$cuerpo .= '' . preg_replace('/(^|\n)-/', '<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" />', $n) . '<br />';
			}
			$cuerpo.='</p>';
		}	

$cuerpo.= 
		'<p class="tituloConImagen">
			<img src="'.URL_BASE.'imagenes/guiadidactica/raya_vertical.jpg">
			METODOLOG&Iacute;A
		</p>'.
		'<p class="textoGuia">La metodología a seguir durante la realización del curso será eminentemente práctica, centrada en el alumno y en su trabajo práctico que va a desarrollar en el Campus AULA_INTERACTIVA, donde los alumnos van a poder poner en práctica todos los contenidos conceptuales que aprendan.</p>
		<p class="textoGuia">El curso presenta una novedosa metodología de formación e-learning. El Campus AULA_INTERACTIVA es una plataforma virtual de formación que se caracteriza por incorporar las Nuevas Tecnologías de la Información y Comunicación en el proceso de enseñanza-aprendizaje.</p>
		<p class="textoGuia">El Campus AULA_INTERACTIVA permite al alumno interactuar y no sólo mirar, solamente necesita un ordenador con conexión a Internet. El horario del curso será totalmente flexible ofreciendo la posibilidad de que los alumnos avancen a su ritmo y que cada uno elija su horario para la formación.</p>
		<p class="textoGuia">
			El contenido de la acción formativa se encuentra en Internet dentro del Campus AULA_INTERACTIVA, de esta forma, usted podrá seguir la formación desde su domicilio o lugar de trabajo. 
			Para resolver cualquier duda en referencia al contenido del curso contarán con la ayuda de un profesor/tutor, experto en la materia objeto de estudio y responsable del seguimiento de su formación. Se podrá poner en contacto con su tutor a través del correo electrónico del Campus AULA_INTERACTIVA, a través de las tutorías online que se realicen, o bien a través del teléfono en el horario que se establezca, el cual se comunicará en la zona de avisos del Campus AULA_INTERACTIVA.
		</p>
		<p class="textoGuia">Asimismo, ante cualquier duda de tipo administrativo o técnica podrá ponerse en contacto con el personal correspondiente a través del correo electrónico o telefónicamente en el horario de atención al cliente.</p>
		<p class="textoGuia">El contenido del curso está dividido en diferentes temas, el cual podrá visualizar en la pantalla del ordenador, o bien descargar en su ordenador e imprimir. Además, a lo largo del curso se establecerán una serie de actividades como tutorías online, chats y foros de debate para completar la formación recibida.</p>'.
		'<p class="tituloConImagen">
			<img src="'.URL_BASE.'imagenes/guiadidactica/raya_vertical.jpg">
			EVALUACI&Oacute;N
		</p>'.
		'<p class="textoGuia">Cuando considere que ha asimilado los contenidos correspondientes a cada módulo, podrá proceder a la evaluación del mismo. Para ello, tendrá que realizar de forma obligatoria el test de evaluación y dos de los tres trabajos prácticos propuestos por cada módulo. Los trabajos prácticos se deberán enviar al tutor por el correo del Campus AULA_INTERACTIVA.</p>'.
		'<p class="textoGuia">Los test de evaluación de cada módulo están formados por 40 preguntas tipo test. La calificación más alta que se puede obtener es de 10, teniendo en cuenta que por cada respuesta no contestada o errónea se resta 0.5. Dispondrá de dos intentos para realizar el test.</p>'.
		'<p class="textoGuia">Los trabajos consisten en resolver unos ejercicios prácticos que deberá enviar al correo de su tutor para que pueda corregirlos e informarle de la calificación obtenida, así como de las propuestas de mejora si fuera necesario. La puntuación mayor que se puede obtener en cada uno de los trabajos es de 10.</p>'.
		'<p class="textoGuia">
				Por tanto, las tareas obligatorias para superar el curso y tener derecho a la obtención del diploma acreditativo, son las siguientes:
		</p>'.
		'<p class="listadoGuia">
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Realizar los tres exámenes tipo test correspondientes a cada uno de los módulos. (ponderación del 30%)<br />
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Resolver seis trabajos prácticos, es decir dos de los tres trabajos prácticos propuestos por módulo. (ponderación del 60%)<br/>
		</p>
		<p>También se tendrá en cuenta para la calificación final, aunque no es una tarea obligatoria, la participación en el foro (ponderación del 10%).</p>';

$cuerpo.=
		'<p class="tituloConImagen">
			<img src="'.URL_BASE.'imagenes/guiadidactica/raya_vertical.jpg">
			RECURSOS DEL CURSO
		</p>'.
		'<p class="textoGuia">El entorno de aprendizaje en el que se enmarca el curso está constituido por una serie de elementos didácticos que enumeramos a continuación:</p>'.
		'<p class="listadoGuia">
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Guía didáctica del curso: presentación, objetivos, contenido, metodología y evaluación del curso.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Guía de usuario de la plataforma: manual de instrucciones sobre el campus virtual de formación en el que estás inscrito, relativas a la dinámica de trabajo seguida, las utilidades y recursos de nuestra Aula virtual.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Contenido interactivo: versión interactiva de cada uno de los módulos que componen el curso.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Contenido (versión para imprimir): Versión en .pdf de cada uno de los módulos.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Trabajos: ejercicios prácticos o tareas que el alumno debe entregar a lo largo del curso. <br/> 
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Archivador: enlaces a direcciones electrónicas de interés y documentos de interés relacionados con el curso.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Agenda personal: para programar el estudio y la preparación, al cual pueden acceder alumno y tutor.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Correo personal: para comunicarse con compañeros, tutores y demás personal administrativo, y de recursos didácticos e informáticos.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Foro del curso: lugar donde se puede debatir, comentar y conversar en público; de acceso disponible para todos los participantes a un curso. La herramienta Foro del Tema te enlaza directamente con esta utilidad, relacionándola con el tema tratado.<br/> 
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Evaluación: de los contenidos propuestos en el curso.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Calificaciones: Registro de las calificaciones obtenidas en las distintas pruebas de evaluación.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Tutorías online: El alumno contará, durante todo el desarrollo del curso, con la posibilidad de comunicarse con los tutores, para consultar dudas, problemas, compartir opiniones, etc. Para ello se pone a su disposición el correo electrónico, interno del curso, que el tutor atenderá de manera personalizada, y el foro, que permitirá debatir, preguntar y responder sobre temas de distinto interés.<br/>
			<img src="' . URL_BASE . 'imagenes/guiadidactica/puntito.png" alt="" /> Biblioteca específica del curso: normativa fundamental sobre los contenidos del curso.<br/>
		</p>';
$cuerpo.='</div>';
