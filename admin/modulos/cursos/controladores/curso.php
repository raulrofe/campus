<?php

$mi_curso = new Cursos();

$get = Peticion::obtenerGet();

if(isset($get['f']) && $get['f'] == 'listar')
{
	//Buscamos todos los tipos de curso
	$cursos = $mi_curso->all_cursos();

	$loadView = 'curso_listado';
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{

	$mi_modulo = new Modulos();
	$modulos = $mi_modulo->ver_modulos();
	//Cargo modulo contenidos

	mvc::cargarModuloSoporte('contenidos');
	$objContenidos = new AccionFormativa();

	//Cargo modulo scorm
	mvc::cargarModuloSoporte('scorm');
	$objModeloScorm = new ModeloScorm();

	//Cargo modelo adicional de autoevaluaciones
	mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
	$objAutoevaluacionAdmin = new AutoevaluacionAdmin();

	//Cargo modulo temas de foro
	mvc::cargarModuloSoporte('foro');
	$objModelo = new AModeloForo();

	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();

		// var_dump($post);exit;

		if(isset($post['f_inicio'])) $fInicio = Fecha::invertir_fecha($post['f_inicio'], '/', '-');
		if(isset($post['f_fin'])) $fFin = Fecha::invertir_fecha($post['f_fin'], '/', '-');

		if(!empty($post['titulo']) && !empty($post['f_inicio']) && !empty($post['f_fin']) && ($fInicio < $fFin))
		{
			if(count($post['idModulo']) == 1)
			{
				foreach($post['idModulo'] as $idModulo)
				{
					$modulo = $mi_modulo->buscar_modulo($idModulo);
					if($modulo->num_rows == 1)
					{
						$mod = $modulo->fetch_object();
						$codigoAF = '00' . $mod->referencia_modulo;
					}
				}
			}
			else if(count($post['idModulo']) > 1)
			{
				$codigoAF = '00001';
			}

			if(isset($codigoAF))
			{
				//Creo la accion formativa y obtengo el id
				$idAccionFormativa = $objContenidos->insertAF($codigoAF);
			}

			$idCurso = null;
			$iMatriculacion = null;
			$fMatriculacion = null;

			if(isset($post['i_maticulacion'])) $iMatriculacion = Fecha::invertir_fecha($post['i_maticulacion'], '/', '-');
			if(isset($post['f_matriculacion'])) $fMatriculacion = Fecha::invertir_fecha($post['f_matriculacion'], '/', '-');

			$guiaDidactica = 0;
			if(isset($post['guiaDidactica']) && $post['guiaDidactica'] == 'on') $guiaDidactica = 1;

			//Paso 1 -> Inserto los datos del curso
			if($mi_curso->insertDatosCurso($post['titulo'], $post['n_horas'], $fInicio, $fFin, $iMatriculacion, $fMatriculacion,
										   $post['metodologia'], $guiaDidactica, $post['descripcion'], $idAccionFormativa, $post['caf'], $post['cg']))
			{
				$idCurso = $mi_curso->obtenerUltimoIdInsertado();

				if($idCurso && is_numeric($idCurso))
				{
					//Asigno los servicios
					$idServicios = array();
					$idServicios[] = 1;
					$idServicios[] = 2;
					$idServicios[] = 3;
					$idServicios[] = 4;
					$idServicios[] = 5;
					$idServicios[] = 6;
					$idServicios[] = 7;
					$idServicios[] = 8;
					$idServicios[] = 9;
					$idServicios[] = 10;
					$idServicios[] = 11;
					$idServicios[] = 12;
					$idServicios[] = 13;
					$idServicios[] = 14;
					$idServicios[] = 15;
					$idServicios[] = 16;
					$idServicios[] = 17;
					$idServicios[] = 18;
					$idServicios[] = 19;
					$idServicios[] = 20;
					$idServicios[] = 21;
					$idServicios[] = 22;
					$idServicios[] = 23;
					$idServicios[] = 24;
					$idServicios[] = 25;
					$idServicios[] = 27;
					$idServicios[] = 28;
					$idServicios[] = 29;
					$idServicios[] = 30;
					$idServicios[] = 31;

					foreach($idServicios as $idServicio)
					{
						//Asigno los servicios
						$mi_curso->asignarServiciosCurso($idServicio, $idCurso);
					}

					//Creo la recomentacion y la asigno al curso
					if($mi_curso->insertar_agenda($post['titulo'], 'Recomendación automática')){

						//Obtengo el ultimo id de la recomendacion_
						$idRecomendacion = $mi_curso->obtenerUltimoIdInsertado();

						//Actualizo el curso con el id de la recomendacion
						$mi_curso->asignarRecomendacionCurso($idRecomendacion, $idCurso);
					}

					//	Paso 2 -> Asigno los modulos seleccionados al centro TABLA: TEMARIO
					if(isset($post['idModulo']) && is_array($post['idModulo']) && count($post['idModulo']) > 0)
					{
						foreach($post['idModulo'] as $idModulo)
						{
							$mi_curso->asignarModuloCurso($idCurso, $idModulo, $idAccionFormativa);
						}

						//Paso 3 -> Asigno los contenidos, Paso 3.1 -> Temas
						if(isset($post['idTemas']) && is_array($post['idTemas']) && count($post['idTemas']) > 0)
						{
							foreach($post['idTemas'] as $idTema)
							{
								//Obtengo los datos del tema
								$tema = $mi_modulo->getTemerioFilePlantillaById($idTema);
								if($tema->num_rows == 1)
								{
									$tema = mysqli_fetch_object($tema);

									//Inserto el tema
									if($idFichero = $mi_modulo->insertar_fichero($tema->titulo, $tema->descripcion, $tema->prioridad, $idCurso, $tema->idcategoria_archivo))
									{
										$idArchioInsertado = $mi_modulo->obtenerUltimoIdInsertado();

										//Copio el archivo
										if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso))
										{
											mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso);
										}
										if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas"))
										{
											mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas");
										}

										if(file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas"))
										{
											$desgloseArchivo = explode('/', $tema->enlace_archivo);
											$extensionNombre = explode('.', $desgloseArchivo[4]);
											$extension = $extensionNombre[1];
											$pathArchivoInsertado = $idArchioInsertado . "." . $extension;

											chmod(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas", 0777);
											if(copy(PATH_ROOT . $tema->enlace_archivo, PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas/" . $pathArchivoInsertado))
											{
												//Actualizo el fichero en la tabla archivo_temario
												$mi_modulo->updateLinkFileTemario($idFichero, "archivos/cursos/" . $idCurso . "/temas/" . $pathArchivoInsertado);
											}
										}
									}
								}
							}
						}

						//Paso 3.3 -> Trabajos Practicos
						if(isset($post['idTrabajosPracticos']) && is_array($post['idTrabajosPracticos']) && count($post['idTrabajosPracticos']) > 0)
						{
							foreach($post['idTrabajosPracticos'] as $idTrabajoPractico)
							{
								//Obtengo los datos del tema
								$trabajoPractico = $mi_modulo->getTemerioFilePlantillaById($idTrabajoPractico);
								if($trabajoPractico->num_rows == 1)
								{
									$trabajoPractico = mysqli_fetch_object($trabajoPractico);

									//Inserto el tema
									if($idFichero = $mi_modulo->insertar_fichero($trabajoPractico->titulo, $trabajoPractico->descripcion, $trabajoPractico->prioridad, $idCurso, $trabajoPractico->idcategoria_archivo))
									{
										$idArchioInsertado = $mi_modulo->obtenerUltimoIdInsertado();

										//Copio el archivo
										if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso))
										{
											mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso);
										}
										if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos"))
										{
											mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos");
										}

										if(file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos"))
										{
											$desgloseArchivo = explode('/', $trabajoPractico->enlace_archivo);
											$extensionNombre = explode('.', $desgloseArchivo[4]);
											$extension = $extensionNombre[1];
											$pathArchivoInsertado = $idArchioInsertado . "." . $extension;

											chmod(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos", 0777);
											if(copy(PATH_ROOT . $trabajoPractico->enlace_archivo, PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos/" . $pathArchivoInsertado))
											{
												//Actualizo el fichero en la tabla archivo_temario
												$mi_modulo->updateLinkFileTemario($idFichero, "archivos/cursos/" . $idCurso . "/trabajos_practicos/" . $pathArchivoInsertado);

											}
										}
									}
								}
							}
						}

						//Paso 3.4 -> Autoevaluaciones
						if(isset($post['idAutoevaluacionesModulo']) && is_array($post['idAutoevaluacionesModulo']) && count($post['idAutoevaluacionesModulo']) > 0)
						{
							foreach($post['idAutoevaluacionesModulo'] as $idAutoevaluacionModulo)
							{
								$idAutoevaluacionCurso = null;
								$autoevaluacion = $objAutoevaluacionAdmin->autevaluacionesModuloAsignadasPorId($idAutoevaluacionModulo);

								if($autoevaluacion->num_rows == 1)
								{
									$autoevaluacionPlantilla = $autoevaluacion->fetch_object();

									$fechaCreacion = date("Y-m-d");

									//Inserto la informacion en la autoevaluacion
									$idAutoevaluacionCurso = $objAutoevaluacionAdmin->insertar_autoevaluacion($autoevaluacionPlantilla->PTtitulo_autoeval,
															 $autoevaluacionPlantilla->PTdescripcion, $fechaCreacion,
															 $autoevaluacionPlantilla->PTidtipo_autoeval);

									if(isset($idAutoevaluacionCurso) && is_numeric($idAutoevaluacionCurso))
									{
										//Configuramos el test
										if($objAutoevaluacionAdmin->copyConfigTest($autoevaluacionPlantilla->PTpenalizacion_blanco, $autoevaluacionPlantilla->PTpenalizacion_nc,
																 $autoevaluacionPlantilla->PTn_intentos, $autoevaluacionPlantilla->PTnota_minima, $autoevaluacionPlantilla->PTtiempo,
																 $autoevaluacionPlantilla->PTcorreccion, $autoevaluacionPlantilla->PTaleatorias, $idAutoevaluacionCurso, $autoevaluacionPlantilla->PTidmodulo, $idCurso))
										{
											//Buscamos las preguntas
											$preguntasPlantilla = $objAutoevaluacionAdmin->preguntasPlantilla($autoevaluacionPlantilla->PTidautoeval);

											if($preguntasPlantilla->num_rows > 0)
											{
												while($preguntaPlantilla = $preguntasPlantilla->fetch_object())
												{
													$idPregunta = null;
													$idPregunta = $objAutoevaluacionAdmin->copiarPregunta($preguntaPlantilla->PTpregunta, $preguntaPlantilla->PTimagen_pregunta, $idAutoevaluacionCurso);
													if(isset($idPregunta) && is_numeric($idPregunta))
													{
														//Buscamos las respuestas
														$respuestasPlantilla = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($preguntaPlantilla->PTidpreguntas);
														if($respuestasPlantilla->num_rows > 0)
														{
															while($respuestaPlantilla = $respuestasPlantilla->fetch_object())
															{
																	$objAutoevaluacionAdmin->copiarRespuesta($respuestaPlantilla->PTrespuesta,
																							$respuestaPlantilla->PTimagen_respuesta, $respuestaPlantilla->PTcorrecta,
																							$idPregunta);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}

						//Paso 3.5 -> Foros	-------- LO ESTOY ASIGNANDO DE TIRON EN EL MODULO
						if(isset($post['idTemaforos']) && is_array($post['idTemaforos']) && count($post['idTemaforos']) > 0)
						{
							foreach($post['idTemaforos'] as $idTemaForo)
							{
								//Busco el tema de foro
								$foro = $objModelo->obtenerUnTemaPlantilla($idTemaForo);

								if($foro->num_rows == 1)
								{
									$foroPlantilla = $foro->fetch_object();
									//Inserto un tema de foro en la tabla correspondiente

									$objModelo->nuevoTemaCurso($foroPlantilla->titulo, $foroPlantilla->idrrhh,
												$foroPlantilla->idmodulo, $foroPlantilla->es_general, $idCurso);
								}
							}
						}

						//Genero la guia didactica del curso
						if(isset($guiaDidactica) && is_numeric($guiaDidactica) && $guiaDidactica = 1)
						{
							AdminCursos::generarGuiaDidactica($idCurso);
						}
					}
					Url::redirect('cursos');
				}
			}
			else Alerta::mostrarMensajeInfo('errorcurso','Error al insertar curso');
		}
		else
		{
			exit('mal datos obligatorios');
		}
	} // Termina if(isPost()) -> Fin de la insercion del curso

	//Obtengo los modulos y sus contenidos y los guardo en un array para poder mostrarlos luego
	$modulosAndContent = array();
	$modulosAndTask = array();

	if($modulos->num_rows > 0)
	{

		while($modulo = mysqli_fetch_object($modulos))
		{
			//Establezco el id del modulo como variable privada
			$mi_modulo->set_modulo($modulo->idmodulo);

			//Obtengo el temario
			$mi_modulo->set_categoria_archivo('1');
			$archivosTemario = $mi_modulo->archivos_moduloPlantilla();

			$cont = 0;
			while($tema = mysqli_fetch_object($archivosTemario))
			{
				$modulosAndContent[$modulo->idmodulo]['temario'][$cont]['idTema'] = $tema->idarchivo_temario;
				$modulosAndContent[$modulo->idmodulo]['temario'][$cont]['tituloTema'] = $tema->titulo;
				$modulosAndContent[$modulo->idmodulo]['temario'][$cont]['enlaceTema'] = $tema->enlace_archivo;
				$cont++;
			}

			//Obtengo el contenido SCORM del modulo
			$scorms = $objModeloScorm->obtenerScorms($modulo->idmodulo);

			$cont = 0;
			while($scorm = mysqli_fetch_object($scorms))
			{
				$modulosAndContent[$modulo->idmodulo]['scorm'][$cont]['idScorm'] = $scorm->idscorm;
				$modulosAndContent[$modulo->idmodulo]['scorm'][$cont]['tituloScorm'] = $scorm->titulo;
				$modulosAndContent[$modulo->idmodulo]['scorm'][$cont]['carpetaScorm'] = $scorm->idscorm_folder;
				$cont++;
			}


			//Obtengo los trabajos practicos del modulo
			$mi_modulo->set_categoria_archivo('2');
			$archivosTrabajosPracticos = $mi_modulo->archivos_moduloNoBorradosPlantilla();
			$cont = 0;
			while($trabajoPractico = mysqli_fetch_object($archivosTrabajosPracticos))
			{
				$modulosAndTask[$modulo->idmodulo]['trabajoPractico'][$cont]['idTrabajoPractico'] = $trabajoPractico->idarchivo_temario;
				$modulosAndTask[$modulo->idmodulo]['trabajoPractico'][$cont]['tituloTrabajoPractico'] = $trabajoPractico->titulo;
				$modulosAndTask[$modulo->idmodulo]['trabajoPractico'][$cont]['enlaceTrabajoPractico'] = $trabajoPractico->enlace_archivo;
				$cont++;
			}


			//Obtengo las autoevaluaciones del modulo
			$autoevaluaciones = $objAutoevaluacionAdmin->autevaluacionesModuloAsignadas($modulo->idmodulo);
			$cont = 0;
			while($autoevaluacion = mysqli_fetch_object($autoevaluaciones))
			{
				$modulosAndTask[$modulo->idmodulo]['autoevaluacion'][$cont]['idAutoevalModulo'] = $autoevaluacion->PTidautoeval_modulo;
				$modulosAndTask[$modulo->idmodulo]['autoevaluacion'][$cont]['idAutoevaluacion'] = $autoevaluacion->PTidautoeval;
				$modulosAndTask[$modulo->idmodulo]['autoevaluacion'][$cont]['idModulo'] = $autoevaluacion->PTidmodulo;
				$modulosAndTask[$modulo->idmodulo]['autoevaluacion'][$cont]['tituloAutoevaluacion'] = $autoevaluacion->PTtitulo_autoeval;
				$cont++;
			}

			//Obtengo los foros del modulo
			$temasForo = $objModelo->obtenerTemasPorModuloPlantilla($modulo->idmodulo);

			$cont = 0;
			while($temaForo = mysqli_fetch_object($temasForo))
			{
				$modulosAndTask[$modulo->idmodulo]['foro'][$cont]['idForo'] = $temaForo->idforo_temas;
				$modulosAndTask[$modulo->idmodulo]['foro'][$cont]['tituloForo'] = $temaForo->titulo;
				$cont++;
			}
		}
	}

	$modulosSelect = $mi_modulo->ver_modulos();

	$loadView = 'curso_insertar';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{

}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		$mi_curso->set_curso($get['idCurso']);

		$curso = $mi_curso->buscar_curso();
		if($curso->num_rows == 1)
		{
			if($mi_curso->eliminar_curso())
			{
				Alerta::guardarMensajeInfo('Curso eliminado');
			}
		}

		Url::redirect('cursos');
	}
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_curso'));