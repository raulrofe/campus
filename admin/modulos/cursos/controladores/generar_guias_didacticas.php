<?php
exit;
$htmlLog = null;

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

mvc::cargarModuloSoporte('usuario');
$objUsuario = new AModeloUsuario();
$objModulos = new Modulos();

$mi_curso = new Cursos();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();


/* SIN USO */

if(Peticion::isPost())
{
	ob_end_flush();
	
	$htmlLog = '';
	
	header('Content-type: multipart/x-mixed-replace;boundary=endofsection');
	echo "\n--endofsection\n";

	set_time_limit(0);
	ignore_user_abort(true);
	
	$htmlLog .= '<div><b>Generando...</b></div>';
	
	echo "Content-type: text/html\n\n";
	echo $htmlLog;
	echo "--endofsection\n";
	ob_flush();
	flush();

	$contNueva = 0;
	$contExist = 0;
	$post = Peticion::obtenerPost();
	
	if(isset($post['idconv']) && is_numeric($post['idconv']))
	{
		$sobreescribirArchivo = false;
		if(isset($post['sobrescribir']) && $post['sobrescribir'] == 'on')
		{
			$sobreescribirArchivo = true;
		}
		
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($post['idconv']);
		$htmlLog .= '<div>Son ' . $cursos->num_rows . ' cursos<br /><br /></div>';
		
		while($curso = $cursos->fetch_object())
		{
			$filename = Texto::clearFilename($curso->titulo);
			
			if(!file_exists(PATH_ROOT . 'archivos/guias_didacticas/' . $filename . '.pdf') || $sobreescribirArchivo)
			{
				$idUltimoCurso = $curso->idcurso;
				
				require(PATH_ROOT_ADMIN . 'modulos/cursos/guiadidactica/guiapdf.php');
				ob_end_clean();
				
				$htmlLog .= '<div><b>' . $curso->idcurso . ' -</b> ' . Texto::textoPlano($curso->titulo) . '</div>';
				
				echo "Content-type: text/html\n\n";
				echo $htmlLog;
				echo "--endofsection\n";
				ob_flush();
				flush();
				
				$contNueva++;
			}
			else
			{
				$contExist++;
			}
		}
	}
	
	$htmlLog .= '<div><br /><b>Se han generado ' . $contNueva . ' guias y ya existian ' . $contExist . '</b></div>' .
				'<div><br /><a href="generar-guias-didacticas"><b>Volver</b></a>';
	
	echo "Content-type: text/html\n\n";
	echo $htmlLog;
	echo "--endofsection\n";
	ob_flush();
	flush();
	
	//die();
}

if($convocatorias->num_rows > 0)
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'generar_guias_didacticas');
}
else
{
	echo 'Debes crear antes una convocatoria';
}