<?php

require_once(PATH_ROOT."admin/modulos/contenidos/modelos/modelo.php");
require_once(PATH_ROOT."admin/modulos/calificaciones/modelos/modelo.php");
	
require PATH_ROOT . 'configMimes.php';

//cargo clase generacion php
// conversion HTML => PDF
require_once(PATH_ROOT.'lib/createpdf/html2pdf.class.php');

$mi_curso = new Cursos();
$mi_af = new AccionFormativa();
$mi_convocatoria = new Convocatoria();
$objModulos = new Modulos();


$objCalificaciones = new AModeloCalificaciones();
$resultado_CCAutoeval = $objCalificaciones->obtenerConfigNotasAuto();
$resultado_CCScorm = $objCalificaciones->obtenerConfigNotasAuto();
$resultadoAccesos = $objCalificaciones->obtenerConfiguracionNotasAccesos();


$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset($post['idtipo_curso'])) 
{
	$idtipo_curso = $post['idtipo_curso'];
}
else
{
	$idtipo_curso = 6;
}

//print_r($get);

$resultado_af = $mi_af->af();
$registros = $mi_curso->ver_tipos_curso();
$resultado_CC = $mi_curso->obtenerTodasConfigCalificaciones();
$resultadoConvocatoria = $mi_convocatoria->convocatorias();

// Insertamos el curso
if(!empty ($post['titulo']))
{
	if($mi_curso->insertar_curso($post['titulo'],$post['descripcion'],$post['idtipo_curso'],$post['idaf'],$post['id_convocatoria']))
	{

		echo '<pre>';
			print_r($post);
		echo '</pre>';exit;

		$idUltimoCurso = $mi_curso->obtenerUltimoIdInsertado();
		if($mi_convocatoria->volcarConvocatoriaCurso($post['id_convocatoria'], $idUltimoCurso))
		{
			if($mi_af->volcarArchivosCurso($post['idaf'], $idUltimoCurso))
			{
				// pdf de guia didactica
				if(isset($_FILES, $_FILES['guiaDidactica']['name']) && $_FILES['guiaDidactica']['error'] !== FALSE)
				{
					$extension = strtolower(Fichero::obtenerExtension($_FILES['guiaDidactica']['name']));
					
					if(isset($mimes[$extension]) && $extension == 'pdf')
					{
						$nombreGuiaDidactica = $idUltimoCurso . '.' . $extension;
						if(move_uploaded_file($_FILES['guiaDidactica']['tmp_name'], PATH_ROOT . 'archivos/guias_didacticas/' . $nombreGuiaDidactica))
						{
							$mi_convocatoria->insertarGuiaDidactica(
												$nombreGuiaDidactica, Texto::clearFilename($_FILES['guiaDidactica']['name']), $idUltimoCurso);
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('La gu&iacute;a did&aacute;ctica debe ser pdf');
					}
				}
				
				// imagen de portada de guia didactica
				if(isset($_FILES, $_FILES['guiaDidacticaImagen']['name']) && $_FILES['guiaDidacticaImagen']['error'] !== FALSE)
				{
					$extension = strtolower(Fichero::obtenerExtension($_FILES['guiaDidacticaImagen']['name']));
					
					if(isset($mimes[$extension])
						&& ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp' && $extension == 'gif'))
					{						
						$nombreGuiaDidactica = $idUltimoCurso . '.' . $extension;
						if(move_uploaded_file($_FILES['guiaDidacticaImagen']['tmp_name'], PATH_ROOT . 'archivos/guias_didacticas/portadas/' . $nombreGuiaDidactica))
						{
							$mi_convocatoria->insertarNombreImagenGuiaDidactica($nombreGuiaDidactica, $idUltimoCurso);
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('La portada debe ser un jpg, jpeg, png, bmp o gif');
					}
				}
				
				Alerta::guardarMensajeInfo('El curso ha sido creado correctamente');
				if(isset($post['guiaDidactica']) && $post['guiaDidactica'] == 1)
				{
					AdminCursos::generarGuiaDidactica($idUltimoCurso);
				}
				//Url::redirect('cursos/insertar');
			}
		}
	}
	else Alerta::mostrarMensajeInfo('errorcurso','Error al insertar curso');	
}

if(isset($post['boton']) && $post['boton'] == 'Actualizar')
{
	if(isset($post['idCurso']) && is_numeric($post['idCurso']))
	{
		$mi_curso->set_curso($post['idCurso']);
		$res = $mi_curso->buscar_curso();
		$f = mysqli_fetch_assoc($res);
		$f_fin = Fecha::invertir_fecha($f['f_fin'], '-', '/');
		$f_inicio = Fecha::invertir_fecha($f['f_inicio'], '-', '/');
		$i_matriculacion = Fecha::invertir_fecha($f['inicio_matriculacion'], '-', '/');
		$f_matriculacion = Fecha::invertir_fecha($f['fin_matriculacion'], '-', '/');
	}
}

//Buscamos los tipo de cursos
$mi_curso->set_tipo_curso($idtipo_curso);
$registros2 = $mi_curso->ver_tipos_curso();

//Busco las convocatorias
$resultado = $mi_convocatoria->convocatorias();

//Buscamos todos los cursos de una convocatoria
if(isset($post['idConvocatoria']) && is_numeric($post['idConvocatoria']))
{
	$cursos = $mi_curso->cursosConvocatoriaActivos($post['idConvocatoria']);
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_curso'));