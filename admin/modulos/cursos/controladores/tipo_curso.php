<?php

$mi_curso = new Cursos();

//extract(Peticion::obtenerPost());
$get=Peticion::obtenerGet();

//print_r($post);

if(isset($get['f']) && $get['f'] == 'listar')
{
	//Buscamos todos los tipos de curso
	$registros = $mi_curso->ver_tipos_curso();	
	
	$loadView = 'tipo_curso';
}
else if(isset($get['f']) && $get['f'] == 'insertar')
{
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if(!empty ($post['tipo_curso']))
		{
			if($mi_curso->insertar_tipo_curso($post['tipo_curso']))
			{
				Alerta::guardarMensajeInfo('Tipo de curso insertado');
				Url::redirect('cursos/tipo_curso');
			}
		} 
	}
	
	$loadView = 'nuevo_tipo_curso';
}
else if(isset($get['f']) && $get['f'] == 'editar')
{
	if(isset($get['idTipoCurso']) && is_numeric($get['idTipoCurso']))
	{		
		// Establezco el id del tipo de curso
		$mi_curso->set_tipo_curso($get['idTipoCurso']);
		
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(!empty($post['atipo_curso']))
			{
				if($mi_curso->actualizar_tipo_curso($post['atipo_curso']))
				{
					Alerta::guardarMensajeInfo('Tipo de curso actualizado');
					Url::redirect('cursos/tipo_curso');
				}
			}
		}
		
		// Busco el curso
		$tipoCurso = $mi_curso->buscar_tipo_curso();
		$tipoCurso = mysqli_fetch_assoc($tipoCurso);
		
		$loadView = 'actualizar_tipo_curso';
	}	
}
else if(isset($get['f']) && $get['f'] == 'eliminar')
{
	if(isset($get['idTipoCurso']) && is_numeric($get['idTipoCurso']))
	{
		$mi_curso->set_tipo_curso($get['idTipoCurso']);
		
		$tipoCurso = $mi_curso->buscar_tipo_curso();
		if($tipoCurso->num_rows == 1)
		{
			if($mi_curso->eliminar_tipo_curso())
			{
				Alerta::guardarMensajeInfo('Tipo de curso eliminado');
				Url::redirect('cursos/tipo_curso');
			}	
		}		
	}	
}
	
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general_tipo_curso');