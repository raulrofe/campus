<?php
$htmlLog = null;

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

mvc::cargarModuloSoporte('usuario');
$objUsuario = new AModeloUsuario();

mvc::cargarModuloSoporte('contenidos');
$objModulo = new AccionFormativa();

$objCurso = new Cursos();
$objConv = new Convocatoria();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias(true);

if(Peticion::isPost())
{
	set_time_limit(0);
	ignore_user_abort(true);
	
	$post = Peticion::obtenerPost();
	$log = array();
	
	if(isset($post['idconv']) && is_numeric($post['idconv']))
	{
		// si se ha subido un archivo del modo correcto ...
		if(isset($_FILES['archivo']['tmp_name']) && !empty($_FILES['archivo']['tmp_name']))
		{
			// comprobamos la extension del archivo (debe ser CSV)
			if(preg_match('#(.+)\.csv$#i', $_FILES['archivo']['name']))
			{
				// abre el archivo CSV
				if(($handle = fopen($_FILES['archivo']['tmp_name'], "r")) !== false)
				{
					$objModelo = new Cursos();
					
					// para evitar procesar la cabecera del CSV
					$cabecera = true;
					
					// contadores de cursos
					$contSuccess = 0;
					$contTotal = 0;
					
					// se mueve por cada registro del CSV
				    while(($data = fgetcsv($handle, 1000, ";")) !== false)
				    {
				    	// si no es la cabecera del archivo, es decir, no es la primera linea
				    	if(!$cabecera)
				    	{
				    		$nombreAccionFormativa = utf8_encode($data[13]);
				    		$modulos = $data[14];
				    		$accionFormativa = $data[15];
				    		$grupo = $data[16];
				    		$costeAccionFormativa = $data[35];
				    		$tutor1 = $data[36];
				    		$tutor2 = $data[37];
				    		$tutor3 = $data[38];
				    		
				    		$idModulos = explode($modulos, '.');
				    		if(empty($costeAccionFormativa))
				    		{
				    			$costeAccionFormativa = '660.00';
				    		}
				    		
				    		$idRamaCurso = '1';
				    		$idConfiguracionCurso = '1';
				    		
				    		if(!empty($accionFormativa) && is_numeric($accionFormativa))
				    		{
				    			$rowAccionFormativa = $objCurso->obtenerAccionFormativa($accionFormativa);
				    			if($rowAccionFormativa->num_rows == 0)
				    			{
				    				$idAccionFormativa = $objModulo->crear_af($nombreAccionFormativa, $accionFormativa, $costeAccionFormativa, $idModulos);
				    			}
				    			else
				    			{
				    				$rowAccionFormativa = $rowAccionFormativa->fetch_object();
				    				$idAccionFormativa = $rowAccionFormativa->idaccion_formativa;
				    			}	
				    			
				    			if(isset($idAccionFormativa) && is_numeric($idAccionFormativa))
				    			{
				    				$row_config_calf = $objCurso->obtenerConfigCalificaciones($idConfiguracionCurso);
				    				if($row_config_calf->num_rows == 1)
				    				{			    					
				    					$tutores = array();
				    					$tutores[] = $tutor1;
				    					$tutores[] = $tutor2;
				    					$tutores[] = $tutor3;
				    					
				    					if(count($tutores) > 0)
				    					{
				    						$existeCurso = $objCurso->checkCursoExists($nombreAccionFormativa, $post['idconv'], $idAccionFormativa);
				    						if($existeCurso->num_rows > 0)
				    						{
				    							$curso = $existeCurso->fetch_object();
				    							$idCurso = $curso->idcurso;
				    						}
				    						else
				    						{
						    					if($objCurso->insertar_curso($nombreAccionFormativa, '', $idRamaCurso, $idAccionFormativa, $post['idconv']))
							        			{
							        				$idnuevo_curso = $objCurso->obtenerUltimoIdInsertado();
							        				
							        				if(isset($idnuevo_curso) && is_numeric($idnuevo_curso))
							        				{
							        					$contNumeroTutor = 1;
								    					foreach($tutores as $tutor)
								    					{
								    						if(!empty($tutor))
								    						{
								    							$rowRRHH = $objUsuario->obtenerUnRRHHPorNif($tutor);
								    							if($rowRRHH->num_rows == 0)
								    							{
								    								$perfil = 4;
								    								if($objUsuario->insertarNifRRHH($tutor, $perfil))
								    								{
								    									$warningRRHH[] = '<div>Al tutor con cif: ' . $tutor . ' le faltan datos obligatorios</div>';
								    									$idRRHH = $objUsuario->obtenerUltimoIdInsertado();
								    								}	
								    							}	
								    							else
								    							{
								    								$rowRRHH = $rowRRHH->fetch_object();
								    								$idRRHH = $rowRRHH->idrrhh;
								    							}
								    							
								    							$status = 'tutor' . $contNumeroTutor;						    							
								    							$objCurso->asignarCursoTutor($idnuevo_curso, $idRRHH, $status);
								    						}
								    						
								    						$contNumeroTutor++;
								    					}
								    					
								    					//Indicamos que el curso ha sido dado de alta con tutores
								    					$objCurso->changeAlta($idnuevo_curso, '1');

        						        				//cargamos la configuracion de calificaciones para el curso
								        				$rowConfig = $row_config_calf->fetch_object();
								        				if($objCurso->insertar_cc_curso($rowConfig->autoevaluaciones,$rowConfig->trabajos_practicos,$rowConfig->scorm,$rowConfig->trabajo_equipo,$rowConfig->tutoria,$rowConfig->foro,$rowConfig->extra1,$rowConfig->extra2,$rowConfig->nota_coordinador,$idnuevo_curso));
									        		 	else
									        			{
									        				$log[] = 'No se inserto la configuracion de calificaciones para el curso con el titulo ' . Texto::textoPlano($data[0]) . ' por error de sql';
									        			}
			
									        			//cargamos los datos de convocatoria para el curso
									        			if($objConv->volcarConvocatoriaCurso($post['idconv'], $idnuevo_curso))
									        			{
									        				if($objModulo->volcarArchivosCurso($idAccionFormativa, $idnuevo_curso));
									        				else
									        				{
									        					$log[] = 'No se copiaron alguno de los archivos para el curso con el titulo ' . Texto::textoPlano($nombreAccionFormativa) . ' por error de sql';
									        				}
									        			}
								 	        			else
									        			{
									        				$log[] = 'No se inserto los datos de convocatoria para el curso con el titulo ' . Texto::textoPlano($nombreAccionFormativa) . ' por error de sql';
									        			}								    										    	
							        				}							        			
							        			}
					    					}
				    					}				    									    					
				    				}
				    			}
				    		}
				    	}
				    	else
				    	{
				    		// comprueba que la estructura del archivo sea valida
				    		if($data[13] == 'NOMBRE A.F.' && $data[14] == 'MODULOS' && $data[15] == 'A.F.' && $data[16] == 'GRUPO')
				    		{
				    			$cabecera = false;
				    		}
				    		else
				    		{
				    			$log[] = 'La estructura del archivo no es valida';
				    			break; // sale del while	
				    		}
				    	}
				    }
				}
				
				 fclose($handle);
				
				// estadisticas de la carga masiva
				$htmlLog .= '<br /><div>Total cursos nuevos: ' . $contTotal . '</div><div>Total cursos insertados: ' . $contSuccess . '</div><div>Total cursos no insertados: ' . ($contTotal - $contSuccess) . '</div><br />';
			}
			else
			{
				$log[] = 'El archivo debe ser tipo CSV';
			}
		}
		else
		{
			$log[] = 'Selecciona un archivo CSV para hacer la carga masiva';
		}
	}
	else
	{
		$log[] = 'Selecciona una convocatoria';
	}
	
	// errores durante el proceso de la carga
	if(count($log) > 0)
	{
		foreach($log as $item)
		{
			$htmlLog .= '<div>- ' . $item . '</div>';
		}
	}
}

if($convocatorias->num_rows > 0)
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'carga_masiva');
}
else
{
	echo 'Debes crear antes una convocatoria';
}