<?php
$mi_curso = new Cursos();
$mi_usuario = new Usuarios();
$objConvocatoria = new Convocatoria();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();


if(isset($get['f']) && $get['f'] == 'listar')
{
	$registrosCursos = $mi_curso->cursosParaAlta();

	$loadView = 'alta_listado';
}
else if(isset($get['f']) && $get['f'] == 'alta')
{
	if(isset($get['idCurso']) && is_numeric($get['idCurso']))
	{
		//Cargo modulo contenidos
		mvc::cargarModuloSoporte('usuario');
		$userAdmin = new AModeloUsuario();

		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();

			if(isset($post['idCoordinador']) && isset($post['idSupervisor']) && isset($post['idTutores']))
			{
				$stafTecnico = array();
				$status = array();

				// Coordinador
				$stafTecnico[] = $post['idCoordinador'];
				$status[] = "coordinador";

				//Supervisor
				$stafTecnico[] = $post['idSupervisor'];
				$status[] = "supervisor";

				//tutores
				if(is_array($post['idTutores']))
				{
					$cont = 1;
					foreach ($post['idTutores'] as $nifTutor)
					{
						$tutor = $userAdmin->obtenerTutorPorNif($nifTutor);
						if($tutor->num_rows == 1)
						{
							$tutor = mysqli_fetch_object($tutor);

							$stafTecnico[] = $tutor->idrrhh;
							$status[] = "tutor" . $cont;
						}

						$cont++;
					}
				}

				if(count($stafTecnico) == count($status))
				{
					if($mi_curso->eliminarPerfilStaffCurso($get['idCurso'], 'supervisor'))
					{
						if($mi_curso->eliminarPerfilStaffCurso($get['idCurso']))
						{
							for($i=0;$i<=count($stafTecnico)-1;$i++)
							{
								$mi_curso->asignarCursoTutor($get['idCurso'], $stafTecnico[$i], $status[$i]);

								//Asigno permisos por defecto al coordinador del curso
								if($status[$i] == 'coordinador')
								{
									//Cargo el modulo para los servicios
									mvc::cargarModuloSoporte('servicios');
									$objServicios = new AModeloServicios();

									$idrol = array('1','2');
									foreach($idrol as $servicio)
									{
										$checkServicio = $objServicios->obtenerRolesUsuarioServicio($stafTecnico[$i], $get['idCurso'], $servicio);
										if($checkServicio->num_rows == 0)
										{
											$objServicios->asignarRol($stafTecnico[$i], $get['idCurso'], $servicio);
										}
										else
										{
											if($objServicios->eliminarRoles($stafTecnico[$i], $get['idCurso']))
											{
												$objServicios->asignarRol($stafTecnico[$i], $get['idCurso'], $servicio);
											}
										}
									}
								}
							}
						}
					}

					$mi_curso->updateAltaCurso($get['idCurso']);
				}

				Url::redirect('cursos/alta');
			}
			else
			{
				Alerta::guardarMensajeInfo('Todo los perfiles son obligatorios');
			}
		}

		//Establezco el id del curso
		$mi_curso->set_curso($get['idCurso']);

		//Obtengo los datos del curso
		$curso = $mi_curso->buscar_curso();

		//Obtengo los miembros del staff del curso
		$miembrosStaff = array();
		$miembrosStaffTutores = array();
		$staffCurso = $mi_curso->obtenerStaffCurso($get['idCurso']);
		while($staff = mysqli_fetch_object($staffCurso))
		{
			$miembrosStaff[] = $staff->idrrhh;

			if($staff->status != 'coordinador' && $staff->status != 'supervisor')
			{
				$miembroTutor = $userAdmin->obtenerUnRRHH($staff->idrrhh);
				if($miembroTutor->num_rows == 1)
				{
					$rowTutor = mysqli_fetch_object($miembroTutor);
					$miembrosStaffTutores[] = $rowTutor->nif;
				}
			}
		}

		//Obtengo todo los miembrodisponibles separados por perfiles
		$supervisores = $userAdmin->obtenerRRHH('supervisor');
		$coordinadores = $userAdmin->obtenerRRHH('coordinador');
		$tutores = $userAdmin->obtenerRRHH('tutor');
		$tutoresDisponibles = $userAdmin->obtenerRRHH('tutor');

		$loadView = 'alta_nueva';
	}
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_alta_curso'));