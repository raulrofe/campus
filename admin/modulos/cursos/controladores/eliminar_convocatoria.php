<?php
$get = Peticion::obtenerGet();
$objConvocatoria = new Convocatoria();

if(isset($get['idconvocatorias']) and is_numeric($get['idconvocatorias']))
{
	$idconvocatorias = $get['idconvocatorias'];
	if($objConvocatoria->eliminarConvocatoria($idconvocatorias))
	{
		Alerta::guardarMensajeInfo('Convocatoria eliminada');
	}
}