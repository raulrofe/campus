<?php
// NO SE USA?

require_once(PATH_ROOT."admin/modulos/contenidos/modelos/modelo.php");

$mi_curso = new Cursos();
$mi_af = new AccionFormativa();
$mi_convocatoria = new Convocatoria();
$objModulos = new Modulos();

$post = Peticion::obtenerPost();

if(isset ($post['idconv'])) $idconv = $post['idconv'];
if(isset ($post['idcurso'])) $idcurso = $post['idcurso'];


$resultado_af = $mi_af->af();
$resultado_CC = $mi_curso->obtenerTodasConfigCalificaciones();
$resultadoConvocatoria = $mi_convocatoria->convocatorias();

//print_r($_POST);
if(isset($post['idCurso']))
{
	$idcurso = $post['idCurso'];
	$mi_curso->set_curso($idcurso);
}

else
{
	$idcurso = 0;
	$mi_curso->set_curso($idcurso);
	
}

if(!empty ($post['atitulo']))
{
	$curso = $mi_curso->buscar_curso();
	$curso = $curso->fetch_object();
	
	$actualizado = $mi_curso->actualizar_curso($post['atitulo'], $post['adescripcion'], $post['idtipo_curso'], $post['idaf'], $post['ametodologia'], $post['af_inicio'], $post['af_fin'], $post['ai_matriculacion'], $post['af_matriculacion'], $post['an_horas']);
	if($actualizado)
	{
		$filename = PATH_ROOT . 'archivos/guias_didacticas/' . $curso->titulo . '.pdf';
		if(file_exists($filename))
		{
			chmod($filename, 0777);
			unlink($filename);
		}
		$idUltimoCurso = $idcurso;
		if($guiaDidactica == 1)
		{
			require(PATH_ROOT_ADMIN . 'modulos/cursos/guiadidactica/guiapdf.php');
		}
		Alerta::guardarMensajeInfo('Actualizacion completada correctamente');
		Url::reload();
	}
}

if(isset($idconv) && $idconv != 0)
{
	$registros = $mi_curso->cursosConvocatoria($idconv);
}

$registros2 = $mi_curso->ver_tipos_curso();

if(!empty($idcurso))
{
	$res = $mi_curso->buscar_curso();
	$f = mysqli_fetch_assoc($res);
	$f_fin = Fecha::invertir_fecha($f['f_fin'], '-', '/');
	$f_inicio = Fecha::invertir_fecha($f['f_inicio'], '-', '/');
	$i_matriculacion = Fecha::invertir_fecha($f['inicio_matriculacion'], '-', '/');
	$f_matriculacion = Fecha::invertir_fecha($f['fin_matriculacion'], '-', '/');
}

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'general_curso'));