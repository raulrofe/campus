<?php
$mi_curso = new Cursos();

$get = Peticion::obtenerGet();

$mi_modulo = new Modulos();
$modulos = $mi_modulo->ver_modulos();
//Cargo modulo contenidos

mvc::cargarModuloSoporte('contenidos');
$objContenidos = new AccionFormativa();

//Cargo modulo scorm
mvc::cargarModuloSoporte('scorm');
$objModeloScorm = new ModeloScorm();

//Cargo modelo adicional de autoevaluaciones
mvc::cargarModeloAdicional('contenidos', 'modeloautoevaluacion');
$objAutoevaluacionAdmin = new AutoevaluacionAdmin();

//Cargo modulo temas de foro
mvc::cargarModuloSoporte('foro');
$objModelo = new AModeloForo();

//Cargo modelo adicional de autoevaluaciones
mvc::cargarModeloAdicional('usuario');
$objUsuario = new AModeloUsuario();

//Cargo modelo adicional de autoevaluaciones
mvc::cargarModeloAdicional('calificaciones');
$objCalificaciones = new AModeloCalificaciones();

if(!empty($_GET))
{
	//Establezco ID MODULO 
	$mi_modulo->set_modulo($_GET['idModulo']);

	if(!empty($_GET['titulo']) && !empty($_GET['f_inicio']) && !empty($_GET['f_fin']) && ($_GET['f_inicio'] < $_GET['f_fin']))
	{
		$titulo = urldecode($_GET['titulo']);
		$metodologia = urldecode($_GET['metodologia']);

		$modulo = $mi_modulo->buscar_modulo($_GET['idModulo'])->fetch_object();
		if($modulo) {
			$codigoAF = $_GET['caf'];
		}

		if(isset($codigoAF))
		{
			//Creo la accion formativa y obtengo el id
			$idAccionFormativa = $objContenidos->insertAF($codigoAF);
		}

		$idCurso = null;
		$iMatriculacion = null;
		$fMatriculacion = null;

		$guiaDidactica = 0;
		if(isset($_GET['guiaDidactica']) && $_GET['guiaDidactica'] == 'on') $guiaDidactica = 1;

		//Paso 1 -> Inserto los datos del curso
		if($mi_curso->insertDatosCurso($titulo, $_GET['n_horas'], $_GET['f_inicio'], $_GET['f_fin'], $_GET['i_matriculacion'], $_GET['f_matriculacion'], $metodologia, $guiaDidactica, $_GET['descripcion'], $idAccionFormativa, $_GET['caf'], $_GET['cg']))
		{
			$idCurso = $mi_curso->obtenerUltimoIdInsertado();

			if($idCurso && is_numeric($idCurso))
			{
				//Asigno los servicios
				$idServicios = array();
				$idServicios[] = 1;
				$idServicios[] = 2;
				$idServicios[] = 3;
				$idServicios[] = 4;
				$idServicios[] = 5;
				$idServicios[] = 6;
				$idServicios[] = 7;
				$idServicios[] = 8;
				$idServicios[] = 9;
				$idServicios[] = 10;
				$idServicios[] = 11;
				$idServicios[] = 12;
				$idServicios[] = 13;
				$idServicios[] = 14;
				$idServicios[] = 15;
				$idServicios[] = 16;
				$idServicios[] = 17;
				$idServicios[] = 18;
				$idServicios[] = 19;
				$idServicios[] = 20;
				$idServicios[] = 21;
				$idServicios[] = 22;
				$idServicios[] = 23;
				$idServicios[] = 24;
				$idServicios[] = 25;
				$idServicios[] = 27;
				$idServicios[] = 28;
				$idServicios[] = 29;
				$idServicios[] = 30;
				$idServicios[] = 31;

				foreach($idServicios as $idServicio)
				{
					//Asigno los servicios
					$mi_curso->asignarServiciosCurso($idServicio, $idCurso);
				}

				//Creo la recomentacion y la asigno al curso
				if($mi_curso->insertar_agenda($titulo, 'Recomendación automática')){

					//Obtengo el ultimo id de la recomendacion_
					$idRecomendacion = $mi_curso->obtenerUltimoIdInsertado();

					//Actualizo el curso con el id de la recomendacion_
					$mi_curso->asignarRecomendacionCurso($idRecomendacion, $idCurso);
				}

				//	Paso 2 -> Asigno los modulos seleccionados al centro TABLA: TEMARIO
				if(isset($_GET['idModulo'])) {
					$mi_curso->asignarModuloCurso($idCurso, $_GET['idModulo'], $idAccionFormativa);
					
					// Paso 3 -> Asigno los contenidos, Paso 3.1 -> Temas
					// Establezco el tipo de archivo
					$mi_modulo->set_categoria_archivo(1);
					$archivosTemasModulo = $mi_modulo->archivos_moduloPlantilla();
					if($archivosTemasModulo->num_rows > 0) {
						while($archivoTemaModulo = $archivosTemasModulo->fetch_assoc()) {

							//Obtengo los datos del tema
							$tema = $mi_modulo->getTemerioFilePlantillaById($archivoTemaModulo['idarchivo_temario']);
							if($tema->num_rows == 1)
							{
								$tema = mysqli_fetch_object($tema);

								//Inserto el tema
								if($idFichero = $mi_modulo->insertar_fichero($tema->titulo, $tema->descripcion, $tema->prioridad, $idCurso, $tema->idcategoria_archivo))
								{
									$idArchioInsertado = $mi_modulo->obtenerUltimoIdInsertado();

									//Copio el archivo
									if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso))
									{
										mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso);
									}
									if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas"))
									{
										mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas");
									}

									if(file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas"))
									{
										$desgloseArchivo = explode('/', $tema->enlace_archivo);
										$extensionNombre = explode('.', $desgloseArchivo[4]);
										$extension = $extensionNombre[1];
										$pathArchivoInsertado = $idArchioInsertado . "." . $extension;

										chmod(PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas", 0777);
										if(copy(PATH_ROOT . $tema->enlace_archivo, PATH_ROOT . "archivos/cursos/" . $idCurso . "/temas/" . $pathArchivoInsertado))
										{
											//Actualizo el fichero en la tabla archivo_temario
											$mi_modulo->updateLinkFileTemario($idFichero, "archivos/cursos/" . $idCurso . "/temas/" . $pathArchivoInsertado);
										}
									}
								}
							}
						}
					}

					//Paso 3.3 -> Trabajos Practicos
					$mi_modulo->set_categoria_archivo(2);
					$archivosTrabajosPracticosModulo = $mi_modulo->archivos_moduloPlantilla();
					if($archivosTrabajosPracticosModulo->num_rows > 0) {
						while($archivoTrabajoPracticoModulo = $archivosTrabajosPracticosModulo->fetch_assoc()) {
							//Obtengo los datos del tema
							$trabajoPractico = $mi_modulo->getTemerioFilePlantillaById($archivoTrabajoPracticoModulo['idarchivo_temario']);
							if($trabajoPractico->num_rows == 1)
							{
								$trabajoPractico = mysqli_fetch_object($trabajoPractico);

								//Inserto el tema
								if($idFichero = $mi_modulo->insertar_fichero($trabajoPractico->titulo, $trabajoPractico->descripcion, $trabajoPractico->prioridad, $idCurso, $trabajoPractico->idcategoria_archivo))
								{
									$idArchioInsertado = $mi_modulo->obtenerUltimoIdInsertado();

									//Copio el archivo
									if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso))
									{
										mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso);
									}
									if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos"))
									{
										mkdir(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos");
									}

									if(file_exists(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos"))
									{
										$desgloseArchivo = explode('/', $trabajoPractico->enlace_archivo);
										$extensionNombre = explode('.', $desgloseArchivo[4]);
										$extension = $extensionNombre[1];
										$pathArchivoInsertado = $idArchioInsertado . "." . $extension;

										chmod(PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos", 0777);
										if(copy(PATH_ROOT . $trabajoPractico->enlace_archivo, PATH_ROOT . "archivos/cursos/" . $idCurso . "/trabajos_practicos/" . $pathArchivoInsertado))
										{
											//Actualizo el fichero en la tabla archivo_temario
											$mi_modulo->updateLinkFileTemario($idFichero, "archivos/cursos/" . $idCurso . "/trabajos_practicos/" . $pathArchivoInsertado);

										}
									}
								}
							}
						}
					}

					//Paso 3.4 -> Autoevaluaciones
					$autoevaluacionesModulo = $objAutoevaluacionAdmin->autevaluacionesModuloAsignadas($_GET['idModulo']);
					if($autoevaluacionesModulo->num_rows > 0)
					{
						while($autoevaluacionModulo = $autoevaluacionesModulo->fetch_assoc())
						{
							$idAutoevaluacionCurso = null;
							$autoevaluacion = $objAutoevaluacionAdmin->autoevaluacionIDAutoevalIdModulo($autoevaluacionModulo['PTidautoeval'], $_GET['idModulo']);

							if($autoevaluacion->num_rows == 1)
							{
								$autoevaluacionPlantilla = $autoevaluacion->fetch_object();

								$fechaCreacion = date("Y-m-d");

								//Inserto la informacion en la autoevaluacion
								$idAutoevaluacionCurso = $objAutoevaluacionAdmin->insertar_autoevaluacion($autoevaluacionPlantilla->PTtitulo_autoeval,
														 $autoevaluacionPlantilla->PTdescripcion, $fechaCreacion,
														 $autoevaluacionPlantilla->PTidtipo_autoeval);

								if(isset($idAutoevaluacionCurso) && is_numeric($idAutoevaluacionCurso))
								{
									//Configuramos el test
									if($objAutoevaluacionAdmin->copyConfigTest($autoevaluacionPlantilla->PTpenalizacion_blanco, $autoevaluacionPlantilla->PTpenalizacion_nc,
															 $autoevaluacionPlantilla->PTn_intentos, $autoevaluacionPlantilla->PTnota_minima, $autoevaluacionPlantilla->PTtiempo,
															 $autoevaluacionPlantilla->PTcorreccion, $autoevaluacionPlantilla->PTaleatorias, $idAutoevaluacionCurso, $autoevaluacionPlantilla->PTidmodulo, $idCurso))
									{
										//Buscamos las preguntas
										$preguntasPlantilla = $objAutoevaluacionAdmin->preguntasPlantilla($autoevaluacionPlantilla->PTidautoeval);

										if($preguntasPlantilla->num_rows > 0)
										{
											while($preguntaPlantilla = $preguntasPlantilla->fetch_object())
											{
												$idPregunta = null;
												$idPregunta = $objAutoevaluacionAdmin->copiarPregunta($preguntaPlantilla->PTpregunta, $preguntaPlantilla->PTimagen_pregunta, $idAutoevaluacionCurso);
												if(isset($idPregunta) && is_numeric($idPregunta))
												{
													//Buscamos las respuestas
													$respuestasPlantilla = $objAutoevaluacionAdmin->buscar_respuestasPlantilla($preguntaPlantilla->PTidpreguntas);
													if($respuestasPlantilla->num_rows > 0)
													{
														while($respuestaPlantilla = $respuestasPlantilla->fetch_object())
														{
																$objAutoevaluacionAdmin->copiarRespuesta($respuestaPlantilla->PTrespuesta,
																						$respuestaPlantilla->PTimagen_respuesta, $respuestaPlantilla->PTcorrecta,
																						$idPregunta);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					//Paso 3.5 -> Foros	-------- LO ESTOY ASIGNANDO DE TIRON EN EL MODULO
					$forosTemasModulo = $objModelo->obtenerTemasPorModuloPlantilla($_GET['idModulo']);

					if($forosTemasModulo->num_rows > 0)
					{
						while($foroTemaModulo = $forosTemasModulo->fetch_assoc())
						{
							//Busco el tema de foro
							$foro = $objModelo->obtenerUnTemaPlantilla($foroTemaModulo['idforo_temas']);

							if($foro->num_rows == 1)
							{
								$foroPlantilla = $foro->fetch_object();

								//Inserto un tema de foro en la tabla correspondiente
								$objModelo->nuevoTemaCurso($foroPlantilla->titulo, $foroPlantilla->idrrhh,
											$foroPlantilla->idmodulo, $foroPlantilla->es_general, $idCurso);
							}
						}
					}

					//Genero la guia didactica del curso
					if(isset($guiaDidactica) && is_numeric($guiaDidactica) && $guiaDidactica = 1)
					{
						AdminCursos::generarGuiaDidactica($idCurso);
					}

					/*
					*
					* ASIGNO LOS TUTORES
					*
					*/

					// Tutor
					$tutor 			= $objUsuario->obtenerTutorPorEmail(urldecode($_GET['emailCampus']))->fetch_assoc();

					if(empty($tutor)){
						$tutor = $objUsuario->obtenerTutorPorEmail(urldecode('maricielovalenzuela@fundacionaulasmart.org'))->fetch_assoc();
					}

					$stafTecnico[] 	= $tutor['idrrhh'];
					$status[] 		= "tutor";
					// Coordinador - usuario maricielo por defecto
					$stafTecnico[] 	= 65;
					$status[] 		= "coordinador";

					// Supervisor
					$supervisor 	= $objUsuario->obtenerSupervisorPorAlias($_GET['caf'])->fetch_assoc();
					$stafTecnico[] 	= $supervisor['idrrhh'];
					$status[] 		= "supervisor";

					for($i=0; $i<=count($stafTecnico)-1; $i++) {
						$mi_curso->asignarCursoTutor($idCurso, $stafTecnico[$i], $status[$i]);

						//Asigno permisos por defecto al coordinador del curso
						if($status[$i] == 'coordinador') {
							//Cargo el modulo para los servicios
							mvc::cargarModuloSoporte('servicios');
							$objServicios = new AModeloServicios();

							$idrol = array('1','2');
							foreach($idrol as $servicio) {
								$checkServicio = $objServicios->obtenerRolesUsuarioServicio($stafTecnico[$i], $idCurso, $servicio);
								if($checkServicio->num_rows == 0) {

									$objServicios->asignarRol($stafTecnico[$i], $idCurso, $servicio);

								} else {
									if($objServicios->eliminarRoles($stafTecnico[$i], $idCurso)) {

										$objServicios->asignarRol($stafTecnico[$i], $idCurso, $servicio);

									}
								}
							}
						}
					}


					/*
					*
					* ASIGNO LA PONDERACION DE NOTAS
					*
					*/

					if($_GET['caf'] == 00802 || $_GET['caf'] == 00803){
						$ponderacionNotas = array(
							'autoevaluaciones'	=> 30,
							'scorm'				=> 0,
							'trabajos'			=> 50,
							'trabajoEquipo'		=> 0,
							'tutoria'			=> 0,
							'foro'				=> 10,
							'extra1'			=> 10,
							'extra2'			=> 0,
							'coordinador'		=> 100,
						);
					} else {
						$ponderacionNotas = array(
							'autoevaluaciones'	=> 30,
							'scorm'				=> 0,
							'trabajos'			=> 60,
							'trabajoEquipo'		=> 0,
							'tutoria'			=> 0,
							'foro'				=> 10,
							'extra1'			=> 0,
							'extra2'			=> 0,
							'coordinador'		=> 100,							
						);						
					}

					// Actualizo el curso añadiendole la ponderacion de notas
					$objCalificaciones->editarConfigNotasPonderacionParaCurso($idCurso, $ponderacionNotas['autoevaluaciones'], $ponderacionNotas['scorm'], $ponderacionNotas['trabajos'], $ponderacionNotas['trabajoEquipo'], $ponderacionNotas['tutoria'], $ponderacionNotas['foro'], $ponderacionNotas['extra1'], $ponderacionNotas['extra2'], $ponderacionNotas['coordinador']);

					// Actualizo la configuracion de notas minimas
					// 1 Es el numero de mensajes minimo que debe participar en el foro para que le cuente para la nota
					// Numero de trabajos practicos minimos que debe de hacer para superar el 75%
					$objCalificaciones->editarConfigNotasParaCurso($idCurso, 1, ceil($archivosTrabajosPracticosModulo->num_rows/2));

					//Matriculo al alumno
					if(!empty($idCurso)){
						$resultMatricula = $objUsuario->obtenerUnaMatriculaPorAlumnoCurso($_GET['idAlumnoCampus'], $idCurso);
						if($resultMatricula->num_rows == 0) {
							$objUsuario->insertarMatricula($_GET['idAlumnoCampus'], $idCurso);
						} else {
							$rowMatricula = $resultMatricula->fetch_object();
							$objUsuario->reactivarMatricula($rowMatricula->idmatricula);
						}
					}					
				}
				
				header('Content-Type: application/json');
				echo json_encode(array('statusCurso' => 'OK'));
			}
		} else {
			// exit('error al insertar el curso');
		}
	}
	else
	{
		// exit('mal datos obligatorios');
	}
} 