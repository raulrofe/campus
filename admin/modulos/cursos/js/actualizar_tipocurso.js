$("#frmTipoCurso").validate({
		errorElement: "div",
		messages: {
			atipo_curso: {
				required : 'Introduce un tipo de curso'
			}
		},
		rules: {
			atipo_curso : {
				required  : true
			}
		}
	});