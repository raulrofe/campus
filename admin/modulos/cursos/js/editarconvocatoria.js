$("#frmNuevaConvocatoria").validate({
		errorElement: "div",
		messages: {
			titulo: {
				required : 'Introduce un t&iacute;tulo para la convocatoria'
			},
			n_horas: {
				required : 'Introduce un n&uacute;mero de horas'
			},
			f_inicio : {
				required : 'Introduce una fecha de inicio para el curso'
			},
			f_fin : {
				required : 'Introduce una fecha de fin para el curso'
			},
			i_matriculacion : {
				required : 'Introduce una fecha de inicio de matriculaci&oacute;n para el curso'
			},
			f_matriculacion: {
				required : 'Introduce una fecha de fin de matricualci&oacute;n para el curso'
			}
		},
		rules: {
			titulo : {
				required  : true
			},
			n_horas : {
				required  : true,
				digits    : true
			},
			f_inicio : {
				required  : true
			},
			f_fin : {
				required  : true
			},
			i_matriculacion : {
				required  : true
			},
			f_matriculacion : {
				required  : true
			}
		}
	});