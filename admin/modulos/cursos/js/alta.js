function asignarTutor()
{
	var tutorComplete = $('#tutoresAltaCurso').val();
	var tutorSeparate = tutorComplete.split(' - ');
	var nif = tutorSeparate[0];
	
	if(tutorComplete != '' && tutorComplete != undefined)
	{
		if(nif != '' && nif != undefined)
		{
			//Mostramos el registro
			$('tr#tutor_' + nif).removeClass('hide');

			//Creamos el campo input si no existe	
			if ($('#idTutoresAsignarGrupo input[value="' + nif + '"]').length == 0)
			{
				campo = '<input type="hidden" name="idTutores[]" value="' + nif + '"  />';
				$("#idTutoresAsignarGrupo").append(campo);	
			}
			else
			{
				alertMsgInfo('El tutor ya se encuentra asignado');
			}

			$('#tutoresAltaCurso').val('');	
		}
		else
		{
			alert('El tutor no dispone de NIF, completa los datos para el tutor antes de asignarlo');
		}
	}
	else
	{
		alert('Debes indicar un NIF o nombre de tutor para poder asignarlo al curso');
	}
}

function desasignarTutor(nif)
{	
	if ($('#idTutoresAsignarGrupo input[value="' + nif + '"]').length == 1)
	{
		$('#idTutoresAsignarGrupo input[value="' + nif + '"]').remove();
		$('tr#tutor_' + nif).addClass('hide');
	}
	
}