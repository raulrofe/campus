$("#frmTipoCurso").validate({
		errorElement: "div",
		messages: {
			tipo_curso: {
				required : 'Introduce un tipo de curso'
			}
		},
		rules: {
			tipo_curso : {
				required  : true
			}
		}
	});