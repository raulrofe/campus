	$("#frm_datos_personales").validate({
		errorElement: "div",
		messages: {
			anombrec: {
				required: 'Introduce tu nombre',
				minlength : 'El nombre debe contener al menos 4 caracteres'
			},
			aapellidos: {
				required: 'Introduce tus apellidos',
				minlength : 'Los apellidos deben contener al menos 4 caracteres'
			},
			aemail : {
				required  : 'Introduce tu email',
				email     : 'El email no es v&aacute;lido'
			},
			atelefono : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
			},
			atelefono2 : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
			},
			acp : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
			},
			ausuario: {
				required: 'Introduce tu apodo',
				minlength : 'El apodo debe contener al menos 4 caracteres'
			},
			af_nacimiento : {
				date : 'La fecha de nacimiento no es v&aacute;lida',
				minlength : 'La fecha debe tener el formato 00/00/0000'
			}
		},
		rules: {
			anombrec : {
				required  : true,
				minlength : 4
			},
			aapellidos : {
				required  : true,
				minlength : 4
			},
			aemail : {
				required  : true,
				email     : true
			},
			atelefono : {
				digits    : true,
				minlength : 9
			},
			atelefono2 : {
				digits    : true,
				minlength : 9
			},
			acp : {
				digits    : true,
				minlength : 5
			},
			ausuario : {
				required  : true,
				minlength : 4
			},
			af_nacimiento : {
				//date : true,
				minlength : 10
			}
		}
	});