function viewImportContent(element)
{	
	var idModulo = $(element).val();

	if($(element).is(':checked')) 
	{  	
		if ($('.' + idModulo).hasClass('hide'))
		{
			$('.' + idModulo).removeClass('hide');
			$('.' + idModulo + ' input').attr('disabled', false);
		}
	} 
	else 
	{
		if (!$('.' + idModulo).hasClass('hide'))
		{
			$('.' + idModulo).addClass('hide');
		}
	}  
}