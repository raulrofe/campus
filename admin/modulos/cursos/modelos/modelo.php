<?php
 
class Convocatoria extends modeloExtend
{
	public function insertarConvocatoria($post)
	{	
		$sql = "SELECT * from convocatoria where nombre_convocatoria = '".$post['titulo']."'";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			
		$sql = "INSERT into convocatoria 
		(nombre_convocatoria,f_inicio,f_fin,inicio_matriculacion,fin_matriculacion,n_horas,metodologia,codigo_convocatoria,tipo_accion,nivel_formacion,uri,usuario,pass,observaciones,modelo_tecnologia,horapre,horate) 
		VALUES 
		('".$post['titulo']."',
		'".$post['f_inicio']."',
		'".$post['f_fin']."',
		'".$post['i_matriculacion']."',
		'".$post['f_matriculacion']."',
		'".$post['n_horas']."',
		'".$post['metodologia']."',
		'".$post['codConv']."',
		'".$post['tipoAccion']."',
		'".$post['nivelFormacion']."',
		'".$post['accesoTri']."',
		'".$post['usuarioTri']."',
		'".$post['passTri']."',
		'".$post['observaciones']."',
		'".$post['modeloTec']."',
		'".$post['horaPre']."',
		'".$post['horaTel']."')";				
		$resultado = $this->consultaSql($sql);
		return $resultado;
		}
		else Alerta::mostrarMensajeInfo('convocatoriayaexiste','Ya existe una convocatoria con ese nombre');
	}
	
	public function convocatorias()
	{
		$sql = "SELECT * from convocatoria WHERE borrado = 0 ORDER BY idconvocatoria";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function insertarGuiaDidactica($nombreArchivo, $nombreArchivo2, $idCurso)
	{
		$sql = "UPDATE curso SET guia_didactica = '$nombreArchivo', guia_didactica_nombre = '$nombreArchivo2' WHERE idcurso = $idCurso";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function insertarNombreImagenGuiaDidactica($nombreImagen, $idCurso)
	{
		$sql = "UPDATE curso SET guia_didactica_imagen = '$nombreImagen' WHERE idcurso = $idCurso";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function buscar_convocatoria($idconvocatoria)
	{
		$sql = "SELECT * from convocatoria where idconvocatoria = ".$idconvocatoria . ' AND borrado = 0';
		$resultado = $this->consultaSql($sql);
		$fila = mysqli_fetch_assoc($resultado);
		return $fila;
	}
	
	public function actualizarConvocatoria($post)
	{
		$sql = "UPDATE convocatoria" .
		" SET nombre_convocatoria = '".$post['titulo']."'," . 
		" f_inicio = '".$post['f_inicio']."'," . 
		" f_fin = '".$post['f_fin']."'," . 
		" inicio_matriculacion = '".$post['i_matriculacion']."'," . 
		" fin_matriculacion = '".$post['f_matriculacion']."'," . 
		" metodologia = '".$post['metodologia']."'," . 
		" n_horas = '".$post['n_horas']."'," .
		//" codigo_convocatoria = '".$post['codConv']."'," .
		//" tipo_accion = '".$post['tipoAccion']."'," .
		//" nivel_formacion = '".$post['nivelFormacion']."'," .
		//" uri = '".$post['accesoTri']."'," .
		//" usuario = '".$post['usuarioTri']."'," .
		//" pass = '".$post['passTri']."'," .
		" observaciones = '".$post['observaciones']."'," .
		" modelo_tecnologia = '".$post['modeloTec']."'," .
		" horapre = '".$post['horaPre']."'," .
		" horate = '".$post['horaTel']."'" .
		" WHERE idconvocatoria = ".$post['idconvocatorias'];

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarConvocatoria($idconvocatorias)
	{
		$sql = "UPDATE convocatoria SET borrado=1 WHERE idconvocatoria = ".$idconvocatorias;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function volcarConvocatoriaCurso($idconv, $idCurso)
	{
		$rowConvocatoria = $this->buscar_convocatoria($idconv);
		$sql = "UPDATE curso SET ".
		"f_inicio = '".$rowConvocatoria['f_inicio']."' , ".
		"f_fin = '".$rowConvocatoria['f_fin']."' , ".
		"inicio_matriculacion = '".$rowConvocatoria['inicio_matriculacion']."' , ".
		"fin_matriculacion = '".$rowConvocatoria['fin_matriculacion']."' , ".
		"metodologia = '".$rowConvocatoria['metodologia']."' , ".
		"n_horas = '".$rowConvocatoria['n_horas']."' ".
		"where idcurso = ".$idCurso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	
}

?>
