$('select[name="idconvocatoria"]').change(function()	
{
	urlRedirect('servicios/' + $(this).val());
});

$('select[name="idcurso"]').change(function()	
{
	urlRedirect('servicios/' + $('select[name="idconvocatoria"]').val() + '/' + $(this).val());
});

$('select[name="idtutor"]').change(function()	
{
	urlRedirect('servicios/' + $('select[name="idconvocatoria"]').val() + '/' + $('select[name="idcurso"]').val() + '/' + $(this).val());
});