<?php
mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$get = Peticion::obtenerGet();

mvc::cargarModeloAdicional('cursos');
$objConvocatoria = new Convocatoria();

//Busco las convocatorias
$convocatorias = $objConvocatoria->convocatorias();

if(isset($get['idconv']) && is_numeric($get['idconv']))
{
	$unaConvocatoria = $objConvocatoria->buscar_convocatoria($get['idconv']);
	if(!empty($unaConvocatoria))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
		
		if(isset($get['idcurso']) && is_numeric($get['idcurso']))
		{
			mvc::cargarModeloAdicional('usuario');
			$objModeloUsuario = new AModeloUsuario();
			$tutores = $objModeloUsuario->obtenerRRHHCurso($get['idcurso']);
			
			if(isset($get['idtutor']) && is_numeric($get['idtutor']))
			{
				$objModelServicios = new AModeloServicios();
				$roles = $objModelServicios->obtenerRoles();
			
				if(Peticion::isPost())
				{
					$objModelServicios->eliminarRoles($get['idtutor'], $get['idcurso']);
					
					$post = Peticion::obtenerPost();
					if(isset($post['idrol']) && is_array($post['idrol']))
					{
						foreach($post['idrol'] as $item)
						{			
							$objModelServicios->asignarRol($get['idtutor'], $get['idcurso'], $item);
						}
					}
				}
				
				if(!empty($get['idtutor']))
				{
					$rolesUsuarioArray = array();
					$rolesUsuario = $objModelServicios->obtenerRolesPorUsuario($get['idtutor'], $get['idcurso']);
					while($row = $rolesUsuario->fetch_object())
					{
						$rolesUsuarioArray[] = $row->idservicio;
					}
				}
			}
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');