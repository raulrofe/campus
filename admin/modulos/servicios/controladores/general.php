<?php
mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$get = Peticion::obtenerGet();

mvc::cargarModeloAdicional('cursos');
$objConvocatoria = new Convocatoria();

//Busco las convocatorias
$convocatorias = $objConvocatoria->convocatorias();

if(isset($get['idconv']) && is_numeric($get['idconv']))
{
	$unaConvocatoria = $objConvocatoria->buscar_convocatoria($get['idconv']);
	if(!empty($unaConvocatoria))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
		
		if(isset($get['idcurso']) && is_numeric($get['idcurso']))
		{
			$objModelServicios = new AModeloServicios();
			
			$roles = $objModelServicios->obtenerServicios();
		
			if(Peticion::isPost())
			{
				$objModelServicios->eliminarLinksServicioPorCurso($get['idcurso']);
				
				$post = Peticion::obtenerPost();
				if(isset($post['idrol']) && is_array($post['idrol']))
				{
					foreach($post['idrol'] as $item)
					{			
						$objModelServicios->asignarLinkServicio($get['idcurso'], $item);
					}
				}
			}
			
			$rolesArray = array();
			$roles2 = $objModelServicios->obtenerServiciosCurso($get['idcurso']);
			while($row = $roles2->fetch_object())
			{
				$rolesArray[] = $row->idServicio;
			}
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'general');