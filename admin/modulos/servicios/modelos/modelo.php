<?php
class AModeloServicios extends modeloExtend
{	
	public function obtenerRoles()
	{
		$sql = 'SELECT * FROM roles';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRolesPorUsuario($idtutor, $idcurso)
	{
		$sql = 'SELECT * FROM tutores_servicios WHERE idrrhh = ' . $idtutor  . ' AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRolesUsuarioServicio($idtutor, $idcurso, $idServicio)
	{
		$sql = 'SELECT * FROM tutores_servicios WHERE idrrhh = ' . $idtutor  . ' AND idcurso = ' . $idcurso. ' AND idservicio = ' . $idServicio;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function eliminarRoles($idtutor, $idcurso)
	{
		$sql = 'DELETE FROM tutores_servicios WHERE idrrhh = ' . $idtutor  . ' AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function asignarRol($idtutor, $idcurso, $idrol)
	{
		$sql = 'INSERT INTO tutores_servicios (idrrhh, idcurso, idservicio) VALUES (' . $idtutor . ', ' . $idcurso . ', ' . $idrol . ') ';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerServicios()
	{
		$sql = 'SELECT s.idServicio, s.nombre, sg.idGrupo, sg.nombre AS nombreGrupo' .
		' FROM servicios AS s' .
		' LEFT JOIN servicios_grupos AS sg ON s.idGrupo = sg.idGrupo' .
		' ORDER BY sg.idGrupo ASC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerServiciosCurso($idcurso)
	{
		$sql = 'SELECT sl.idServicio, sl.idCurso' .
		' FROM servicios_link AS sl' .
		' WHERE sl.idCurso = ' . $idcurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function eliminarLinksServicioPorCurso($idcurso)
	{
		$sql = 'DELETE FROM servicios_link WHERE idCurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function asignarLinkServicio($idcurso, $idservicio)
	{
		$sql = 'INSERT INTO servicios_link (idServicio, idCurso) VALUES (' . $idservicio . ', ' . $idcurso . ') ';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}