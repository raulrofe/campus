 <div class="titular">
	<h1 class='titleAdmin'>SERVICIOS - GENERAL</h1>
	<img src='../imagenes/pie_titulos.jpg' />
</div>
<br/>

<div class="stContainer" id="tabs">
	<div class="tab">
		<div class="submenuAdmin">SERVICIOS</div>
		<?php if($convocatorias->num_rows > 0):?>
				<form id="frmServiciosConv" action="#" method="post">
				<label>Convocatoria</label>
				<select name="idconvocatoria">
					<option value="0"> - Seleccione una convocatoria - </option>
					<?php while($convocatoria = $convocatorias->fetch_object()):?>
					  	<option value="<?php echo $convocatoria->idconvocatoria?>" <?php if(isset($get['idconv']) && $get['idconv'] == $convocatoria->idconvocatoria) echo 'selected="selected"'?>><?php echo Texto::textoPlano($convocatoria->nombre_convocatoria)?></option>
					<?php endwhile;?>
				</select>
			</form>
		<?php else:?>
			<div><strong>No hay cursos</strong></div>
		<?php endif;?>
		
		<?php if(isset($cursos)):?>
			<br /><br />
			<?php if($cursos->num_rows > 0):?>
				<form id="frmServiciosCurso" action="#" method="post">
					<label>Cursos</label>
					<select name="idcurso">
						<option value="0"> - Seleccione un curso - </option>
						<?php while($curso = $cursos->fetch_object()):?>
						  	<option value="<?php echo $curso->idcurso?>" <?php if(isset($get['idcurso']) && $get['idcurso'] == $curso->idcurso) echo 'selected="selected"'?>><?php echo Texto::textoPlano($curso->titulo)?></option>
						<?php endwhile;?>
					</select>
				</form>
				<div class="clear"></div>
				
				<?php if(isset($roles)):?>
					<?php if($roles->num_rows > 0):?>
						<form id="frmServiciosCurso" action="" method="post">
							<div>
								<?php $roleIdGrupo = 0;?>
								<?php while($role = $roles->fetch_object()):?>
									
									<?php if($roleIdGrupo != $role->idGrupo):?>
										<br />
										<h3><?php echo Texto::textoPlano($role->nombreGrupo)?></h3>
										<?php $roleIdGrupo = $role->idGrupo?>
									<?php endif;?>
									
									<div>
										<input type="checkbox" name="idrol[]" value="<?php echo $role->idServicio?>" <?php if(in_array($role->idServicio, $rolesArray)) echo 'checked="checked"'?> />
										<span><?php echo Texto::textoPlano($role->nombre)?></span>
									</div>
								<?php endwhile;?>
								<br />
								<button type="submit">Guardar</button>
							</div>
						</form>
					<?php endif;?>
				<?php endif;?>
				
			<?php else:?>
				<div><strong>No hay cursos</strong></div>
			<?php endif;?>
		<?php endif;?>
	</div>
</div>

<script type="text/javascript" src="js-servicios-general.js"></script>