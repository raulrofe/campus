 <div class="titular">
	<h1 class='titleAdmin'>SERVICIOS - TUTORES</h1>
	<img src='../imagenes/pie_titulos.jpg' />
</div>
<br/>

<div class="stContainer" id="tabs">
	<div class="tab">
		<div class="submenuAdmin">SERVICIOS</div>
			<?php if($convocatorias->num_rows > 0):?>
					<form id="frmServiciosConv" action="#" method="post">
					<label>Convocatoria</label>
					<select name="idconvocatoria">
						<option value="0"> - Seleccione una convocatoria - </option>
						<?php while($convocatoria = $convocatorias->fetch_object()):?>
						  	<option value="<?php echo $convocatoria->idconvocatoria?>" <?php if(isset($get['idconv']) && $get['idconv'] == $convocatoria->idconvocatoria) echo 'selected="selected"'?>><?php echo Texto::textoPlano($convocatoria->nombre_convocatoria)?></option>
						<?php endwhile;?>
					</select>
				</form>
			<?php else:?>
				<div><strong>No hay cursos</strong></div>
			<?php endif;?>
				
			<?php if(isset($cursos)):?>
			
				<br /><br />
				<?php if($cursos->num_rows > 0):?>
					<form id="frmServiciosCurso" action="#" method="post">
						<label>Cursos</label>
						<select name="idcurso">
							<option value="0"> - Seleccione un curso - </option>
							<?php while($curso = $cursos->fetch_object()):?>
							  	<option value="<?php echo $curso->idcurso?>" <?php if(isset($get['idcurso']) && $get['idcurso'] == $curso->idcurso) echo 'selected="selected"'?>><?php echo Texto::textoPlano($curso->titulo)?></option>
							<?php endwhile;?>
						</select>
					</form>
			
					<?php if(isset($tutores)):?>
						<br /><br />
						<?php if($tutores->num_rows > 0):?>
							<form method="post" action="">
								<div id="serviciosTutores">
									<label>Tutores</label>
									<div>
										<select name="idtutor">
											<option value="0"> - Seleccione un tutor - </option>
											<?php while($tutor = $tutores->fetch_object()):?>
											  	<option value="<?php echo $tutor->idrrhh?>" <?php if(isset($get['idtutor']) && $get['idtutor'] == $tutor->idrrhh) echo 'selected="selected"'?>>
											  		<?php echo Texto::textoPlano($tutor->nombrec)?>
											  	</option>
											<?php endwhile;?>
										</select>
									</div>
								</div>
								<br /><br />
								
								<?php if(isset($roles, $rolesUsuarioArray) && $roles->num_rows > 0):?>
									<div id="serviciosRoles">
										<label>Servicios</label>
										<div class="serviciosBloque">
											<?php while($rol = $roles->fetch_object()):?>
												<div>
													<input type="checkbox" name="idrol[]" value="<?php echo $rol->idrol?>" <?php if(in_array($rol->idrol, $rolesUsuarioArray)) echo 'checked'?>>
													<label><?php echo Texto::textoPlano($rol->nombre)?></label>
												</div>
											<?php endwhile;?>
										</div>
									
										<br /><br />
										<div>
											<input type="submit" value="Guardar" />
										</div>
									</div>
								<?php endif;?>
							</form>
						<?php else:?>
							<div><strong>No hay tutores</strong></div>
						<?php endif;?>
					<?php endif;?>
					
				<?php else:?>
					<div><strong>No hay cursos</strong></div>
				<?php endif;?>
				
			<?php endif;?>
		</div>
	</div>
</div>
	
<script type="text/javascript" src="js-servicios-default.js"></script>