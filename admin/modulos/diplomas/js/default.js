$(document).ready(function()
{
	if($('#adminCertificadoGenerarPDFBuscador').size() == 1)
	{
		$('#adminCertificadoGenerarPDFBuscador input[type="text"]').keyup(function(event)
		{
			var wordSearch = $(this).val();
			
			$('#adminCertificadoGenerarPDFListado').find('li').addClass('hide');
			
			if(wordSearch != "")
			{
				var titleScorm = '';
				var searchMatch = null;
				$('#adminCertificadoGenerarPDFListado').find('li').each(function(index)
				{
					titleScorm = $(this).find('label').attr('data-search-dni');
					//searchMatch = '/^(' + wordSearch + ')(.+)/i';
					//alert(titleScorm);
					wordSearch = LibMatch.escape(wordSearch);
					var regex = new RegExp('^(' + wordSearch + ')(.+)', 'i');
					if(titleScorm.match(regex))
					{
						$(this).removeClass('hide');
					}
				});
			}
			else
			{
				$('#adminCertificadoGenerarPDFListado').find('li').removeClass('hide');
			}
		});
	}
});

function adminCertificadosGenerarPdfSelectCheckbox(elemento)
{
	$('#adminCertificadoGenerarPDFListado').find('li:not(.hide)').find(':checkbox').prop('checked', 'checked');
}
function adminCertificadosGenerarPdfUnselectCheckbox(elemento)
{
	$('#adminCertificadoGenerarPDFListado').find('li:not(.hide)').find(':checkbox').removeProp('checked');
}

function adminCertificadosGenerarPdfGoTo(elemento, oficial)
{
	if(oficial != undefined && oficial)
	{
		var url = 'certificados/generar-pdf/';
	}
	else
	{
		var url = 'certificados-provisionales/generar-pdf/';
	}
	
	urlRedirect(url + $(elemento).val());
}