<style type="text/Css">
<!--
.espacio
{
	margin-bottom:10px;
	background-color: transparent;
}
-->
</style>

<?php 
if($f3['logo_ministerio'] != '1') $tamaño='166';
else $tamaño='66';
?>

<page style="font-size: 12pt;font-family:'Helvetica';background-color: transparent;" backimg='modulos/diplomas/pdf/imagenes/fondo.jpg'>
<div style="margin-top:13mm;margin-left:16mm;margin-right:16mm;background-color: transparent;">
<table>
    <tr>
        <td style='margin-left:10mm;text-align:center;width:<?php echo $tamaño;?>mm;float:left;'>
        	<img src='modulos/diplomas/pdf/imagenes/1_logo.png' alt=''/><br/>
        	<span style='font-size:6.5pt;font-weight:bold;color:#656565;'>Fundación </span><span style='font-size:6.5pt;color:#0B9EC9;'>AULA_</span><span style='font-size:6.5pt;color:#0B9EC9;font-weight:bold;'>SMART</span><br/>
        	<img src='modulos/diplomas/pdf/imagenes/1_puntos.png' alt=''/><br/>
        	<span style='color:#656565;font-size:5.5pt;font-weight:bold;margin-top:-8px;'>FUNDACIÓN PARA EL DESARROLLO E IMPLANTACIÓN<br/> DE LAS NUEVAS TECNOLOGÍAS EN EL SECTOR EDUCATIVO</span>
        </td>
        <?php if($f3['logo_ministerio'] == '1'){?>
        <td style='margin-right:10mm;text-align:center;width:120mm;font-size:5pt;float:right;font-weight:bold;'>
        	<img src='modulos/diplomas/pdf/imagenes/logo_mec.png' alt=''/><br/>
        	<span style='font-size:6pt;margin-top:5px;'>MINISTERIO DE EDUCACIÓN</span><br/><span style='font-size:5pt;margin-top:5px;'>SECRETARÍA DE ESTADO DE EDUCACIÓN Y FORMACIÓN PROFESIONAL</span><br/><span style='font-size:4pt;margin-top:5px;'>DIRECCIÓN GENERAL DE FORMACIÓN PROFESIONAL<BR/>INSTITUTO DE FORMACIÓN DEL PROFESORADO, INVESTIGACIÓN E INNOVACIÓN EDUCATIVA</span>
        </td>
        <?php }?>
    </tr>
</table>
</div>
<div style="margin-top:15mm;margin-left:31mm;width:140mm;background-color: transparent;">
	<div style='text-align:center;font-size:24pt;margin-bottom:45px;'><font face="Book Antiqua">CERTIFICADO DE PARTICIPACIÓN</font></div>
	
<div class='espacio'>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 16mm;">D./Dña.</td>
			<td style="width: 120mm;border-bottom:1px dotted #000;text-align:center;font-weight:bold;"><?php echo utf8_decode(strtoupper($f3['nombre']." ".$f3['apellidos']));?></td>
		</tr>
	</table>
</div>

<div class='espacio'>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 16mm;">con NIF</td>
			<td style="width: 40mm;border-bottom:1px dotted #000;text-align:center;font-weight:bold;"><?php echo $f3['dni'];?></td>
			<td style="width: 80mm;"> que presta sus servicios en la Empresa</td>
		</tr>
	</table>
</div>
	
<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 137mm;border-bottom:1px dotted #000;text-align:center;font-weight:bold;"><?php echo utf8_decode(strtoupper($f3['razon_social']));?></td>
		</tr>
	</table>
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 16mm;">con CIF</td>
			<td style="width: 40mm;border-bottom:1px dotted #000;text-align:center;font-weight:bold;"><?php echo $f3['cif'];?></td>
			<td style="width: 80mm;"> Ha realizado la acción formativa</td>
		</tr>
	</table>	
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 137mm;border-bottom:1px dotted #000;text-align:center;font-weight:bold;"><?php echo utf8_decode(strtoupper($f3['descripcion']));?></td>
		</tr>
	</table>	
</div>
	
<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 35mm;">Código AF/Grupo</td>
			<td style="width: 21mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['idaccion_formativa'];?></td>
			<td style="text-align:center;">/</td>
			<td style="width: 21mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['codigoIG'];?></td>
		</tr>
	</table>	
</div>	

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 35mm;">Durante los días</td>
			<td style="width: 35mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['f_inicio']?></td>
			<td style="text-align:center;"> al </td>
			<td style="width: 35mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['f_fin']?></td>
			<td> , con una </td>
		</tr>
	</table>	
</div>
	
<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td>duración total de</td>
			<td style="width: 20mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['n_horas'];?></td>
			<td> horas.</td>
		</tr>
	</table>	
</div>	

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 50mm;">En la Modalidad formativa</td>
			<td style="width: 86mm;border-bottom:1px dotted #000;text-align:center;"><?php echo utf8_decode($modalidad);?></td>
		</tr>
	</table>	
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 50mm;">Con número de horas: </td>
			<td style="width: 15mm;border-bottom:1px dotted #000;text-align:center;">0</td>
			<td> Jornadas Presenciales</td>
		</tr>
	</table>	
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 50mm;"> </td>
			<td style="width: 15mm;border-bottom:1px dotted #000;text-align:center;">0</td>
			<td> Tutoría Presenciales</td>
		</tr>
	</table>	
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 50mm;"> </td>
			<td style="width: 15mm;border-bottom:1px dotted #000;text-align:center;">0</td>
			<td> Tutoría a distancia</td>
		</tr>
	</table>	
</div>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 50mm;"> </td>
			<td style="width: 15mm;border-bottom:1px dotted #000;text-align:center;"><?php echo $f3['n_horas']?></td>
			<td> Tutoría de Teleformación</td>
		</tr>
	</table>	
</div>
<br/><br/>

<div class='espacio'>	
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td>Contenidos impartidos (ver dorso)</td>
		</tr>
	</table>	
</div>
<br/><br/><br/><br/><br/><br/>
</div>

<div class='espacio' style='margin-left:22mm;'>	
	<table style="width:50mm"	cellspacing="0">
		<tr>
			<td style='font-size:8pt;text-align:center;'>Fecha de expedición</td>
		</tr>
		<tr>
			<td style='font-size:8pt;text-align:center;font-weight:bold;border-bottom:1px solid #000;'><?php echo $f3['fecha_expedicion']?></td>
		</tr>
	</table>	
</div>
<br/>

