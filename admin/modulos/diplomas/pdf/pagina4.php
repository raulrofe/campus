<?php if($f3['recibi'] == 1){?>
<page style="font-size: 11pt;font-family:'Helvetica';">
<div style="margin-top:16mm;margin-left:5mm;width:140mm;">
<table>
    <tr>
        <td style='margin-left:10mm;text-align:center;width:66mm;float:left;'>
        	<img src='modulos/diplomas/pdf/imagenes/1_logo.png' alt=''/><br/>
        	<span style='font-size:6.5pt;font-weight:bold;color:#656565;'>Fundación </span><span style='font-size:6.5pt;color:#0B9EC9;'>AULA_</span><span style='font-size:6.5pt;color:#0B9EC9;font-weight:bold;'>SMART</span><br/>
        	<img src='modulos/diplomas/pdf/imagenes/1_puntos.png' alt=''/><br/>
        	<span style='color:#656565;font-size:5.5pt;font-weight:bold;margin-top:-8px;'>FUNDACIÓN PARA EL DESARROLLO E IMPLANTACIÓN<br/> DE LAS NUEVAS TECNOLOGÍAS EN EL SECTOR EDUCATIVO</span>
        </td>
    </tr>
</table>
<table style="width:350mm;margin-left:570px;margin-top:-110px;"	cellspacing="0">
	<tr>
		<td style='text-align:justify;width:170mm;padding-left:30px;'>
			<img src='modulos/diplomas/pdf/imagenes/logo_tripartita.png' alt='' style='margin-left:-25px;'/><br/>
			<span style="font-size:8.2pt;color:#393c99;font-family:'times New Roman';font-weight:bold;">Fundación Tripartita</span><br/>
			<span style="font-size:4.2pt;padding-top:6px;color:#393c99;margin-top:-5px;font-weight:bold;">PARA LA FORMACION EN EL EMPLEO</span>
		</td>
	</tr>
</table>
	<table style="width:340mm;margin-top:10px;"	cellspacing="0">
		<tr>
			<td style='float:right;text-align:right;width:165mm;'>
				<img src='modulos/diplomas/pdf/imagenes/logo_ue.png' alt='' />
			</td>
			<td style='float:right;text-align:left;padding-top:5px;line-height:1em;padding-top:18px;'>
				<div style='font-size:7pt;font-weight:bold;'>UNIÓN EUROPEA</div>
				<div style='font-size:6pt;font-weight:bold;'>Fondo Social Europeo</div>
				<div style='font-size:5pt;padding-top:3px;font-style: italic;'>El FSE invierte en tu futuro</div>
			</td>
		</tr>
	</table>
</div>
<div style="margin-top:18mm;margin-left:2mm;width:150mm;color:#000066">
	<div style='text-align:center;font-size:14pt;margin-bottom:50px;'><font face="Book Antiqua">RECIBÍ DE CERTIFICADO</font></div>
	
<div class='espacio' style='border:2px solid #bbb;padding:10px;'>
	<table style="width: 100%;"	cellspacing="0">
		<tr>
			<td style="width: 55mm;font-weight:bold;">ENTIDAD ORGANIZADORA: </td>
			<td style="width: 70mm;"><?php echo "FUNDACIÓN AULA_SMART";?></td>
			<td style="width: 10mm;font-weight:bold;">CIF.: </td>
			<td>G-92581651</td>
		</tr>
	</table><br/>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 55mm;font-weight:bold;">CÓDIGO DE AGRUPACIÓN: </td>
			<td style="width: 70mm;"><?php echo "B089078Al";?></td>
		</tr>
	</table>
</div>
<br/><br/>

<div class='espacio' style='border:2px solid #bbb;padding:10px;'>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="font-weight:bold;">DENOMINACIÓN DE LA ACCIÓN FORMATIVA: <?php echo strtoupper(utf8_decode($f3['descripcion']));?></td>
		</tr>
	</table><br/>

	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="width: 25mm;font-weight:bold;">Nº: <?php echo $f3['idaccion_formativa'];?></td>
			<td style="width: 40mm;font-weight:bold;">GRUPO: <?php echo $f3['codigoIG'];?></td>
			<td style="font-weight:bold;">FECHA DE INICIO: </td>
			<td style="width: 25mm;"><?php echo $f3['f_inicio']?></td>
			<td style="font-weight:bold;">FECHA DE FIN: </td>
			<td><?php echo $f3['f_fin']?></td>
		</tr>
	</table><br/>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style="font-weight:bold;">FORMADOR/RESPONSABLE DE FORMACIÓN: Sandra Sicilia Salcedo</td>
		</tr>
	</table>
</div>
<br/><br/>

<div class='espacio' style='border:2px solid #bbb;padding:10px;'>
	<table style="width: 100%"	cellspacing="0">
		<tr>
			<td style='width:190mm;line-height:22px;'>D./Dña. <?php echo utf8_decode(strtoupper($f3['nombre']." ".$f3['apellidos']))?>, con N.I.F. <?php echo $f3['dni']?>, deja constancia de que ha recibido el diploma acreditativo de la acción formativa detallada anteriormente y que ha utilizado horas de trabajo como horas lectivas.</td>
		</tr>
	</table><br/><br/><br/>
	<table style="width: 100%"	cellspacing="0" align="center">
		<tr>
			<td>En </td>
			<td style="border-bottom:1px solid #000066;width:30mm;"></td>
			<td> a </td>
			<td style="border-bottom:1px solid #000066;width:10mm;"></td>
			<td> de </td>
			<td style="border-bottom:1px solid #000066;width:20mm;"></td>
			<td> de <?php echo date("Y");?></td>
		</tr>
	</table><br/><br/><br/>
	<table style="width: 100%"	cellspacing="0" align="center">
		<tr>
			<td>Fdo. </td>
			<td style="border-bottom:1px solid #000066;width:80mm;"></td>
		</tr>
	</table><br/>
</div>

</div>
<br/>	
</page>
<?php }?>