<?php
class AModeloDiplomas extends modeloExtend
{
	public function obtenerDatosDiploma($idAlumno, $idCurso)
	{
		/*$sql = 'SELECT DISTINCT *' .
				' from convocatoria CAF, accion_formativa AF, inicio_grupo IG, diploma D, alumnos A, centros C, curso CR' .
				' where CR.idcurso = ' . $idCurso.
				' and IG.idaccion_formativa = CR.idaccion_formativa ' .
				' and D.idinicio_grupos = IG.idinicio_grupo' .
				' and A.idalumnos = ' . $idAlumno .
				' and D.idalumnos = A.idalumnos' .
				' and A.idcentros = A.idcentros' .
				' order by A.idcentros ASC':
		*/
		
		$sql = 'SELECT * from convocatoria CAF, accion_formativa AF, inicio_grupo IG, diploma D, alumnos A, centros C, curso CR' .
		' where CR.idcurso = ' . $idCurso .
		' and CAF.idconvocatoria = CR.idconvocatoria' .
		' and CR.idaccion_formativa = AF.idaccion_formativa' .
		' and A.idalumnos = ' . $idAlumno .
		' and A.idcentros = A.idcentros' .
		' group by A.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerDatosDiploma2($idAF)
	{
		$sql = 'SELECT m.idmodulo, m.nombre, m.contenidos' .
		' FROM temario AS tm' .
		' LEFT JOIN modulo AS m ON m.idmodulo = tm.idmodulo' .
		' WHERE tm.idaccion_formativa = ' . $idAF;
		
		/*$sql= 'SELECT * from maf MAF, modulo M' . 
				' where MAF.idaccion_formativa = ' . $idAF .
				' and MAF.idmodulo = M.idmodulo';*/
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerDiplomaPorAlumno($idAlumno, $idIniGrupo)
	{
		$sql = 'SELECT * FROM diploma WHERE idalumnos = ' . $idAlumno . ' AND idinicio_grupos = ' . $idIniGrupo;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarDiploma($idAlumno, $inicioGrupo, $recibi, $logoTripartita, $logoMec, $fecha_ini_tripartita, $fecha_fin_tripartita)
	{
		$sql = 'INSERT INTO diploma (idalumnos, idinicio_grupos, recibi, logo_tripartita, logo_ministerio, f_inicio_tripartita, f_fin_tripartita)' .
		' VALUES (' . $idAlumno . ', ' . $inicioGrupo . ', ' . $recibi . ', ' . $logoTripartita .
		', ' . $logoMec . ', "' . $fecha_ini_tripartita . '", "' . $fecha_fin_tripartita . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarDiploma($idDiploma, $recibi, $logoTripartita, $logoMec, $fecha_ini_tripartita, $fecha_fin_tripartita)
	{
		$sql = 'UPDATE diploma SET recibi = ' . $recibi . ', logo_tripartita = ' . $logoTripartita . ', logo_ministerio = ' . $logoMec .
		',f_inicio_tripartita = "' . $fecha_ini_tripartita . '", f_fin_tripartita = "' . $fecha_fin_tripartita . '"' .
		' WHERE iddiploma = ' . $idDiploma;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumno($dni)
	{
		$sql = 'SELECT al.idalumnos, igc.idinicio_grupo, al.dni FROM alumnos AS al' .
		' LEFT JOIN inicio_grupo_centros AS igc ON igc.idcentros = al.idcentros' .
		' WHERE al.dni = "' . $dni . '" AND al.borrado = 0';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}