<?php
set_time_limit(0);
require_once(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');

mvc::cargarModuloSoporte('cursos');
$objConvocatoria = new Convocatoria();

$convocatorias = $objConvocatoria->convocatorias();

$get = Peticion::obtenerGet();
if(isset($get['idconv']) && is_numeric($get['idconv']))
{
	mvc::cargarModuloSoporte('usuario');
	$objUsuario = new AModeloUsuario();
	
	$objModeloDiplomas = new AModeloDiplomas();
	
	$alumnos = $objUsuario->obtenerAlumnoPorConv($get['idconv']);
	
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		if(isset($post['alumnos']) && is_array($post['alumnos']))
		{
			foreach($post['alumnos'] as $idAlumno)
			{
				if(is_numeric($idAlumno))
				{
					$rowAlumno = $objUsuario->obtenerUnAlumno($idAlumno);
					if($rowAlumno->num_rows == 1)
					{
						$rowAlumno = $rowAlumno->fetch_object();
						
						$cursos = $objUsuario->obtenerCursosMatriculadosPorAlumno($idAlumno);
						while($f5 = $cursos->fetch_assoc())
						{
							$resultado5= $objModeloDiplomas->obtenerDatosDiploma($idAlumno, $f5['idcurso']);
							$f3 = mysqli_fetch_assoc($resultado5);
							
							/*if($curso->metodologia == '10')
							{
								$modalidad = 'Teleformación';
							}
							else
							{
								$modalidad = $curso->metodologia;
							}*/
							
							$modalidad = $f5['metodologia'];
							
							$resultado4 = $objModeloDiplomas->obtenerDatosDiploma2($f5['idaccion_formativa']);
							
							 // r_cup_ration du contenu HTML
							ob_start();
							include(dirname(__FILE__) . '/../pdf/pagina1.php');
							$content = ob_get_clean();
								
							ob_start();
							include(dirname(__FILE__) . '/../pdf/pagina2.php');
							$content2 = ob_get_clean();
							
							ob_start();
							include(dirname(__FILE__) . '/../pdf/pagina3.php');
							$content3 = ob_get_clean();
									
							// conversion HTML => PDF
							try
							{
								$html2pdf = new HTML2PDF('P', 'A4', 'fr', false, 'ISO-8859-15');
								
								$html2pdf->setDefaultFont('Arial');
								$html2pdf->writeHTML($content);
								
								$html2pdf->addFont('times');
								$html2pdf->setDefaultFont('times');
								$html2pdf->writeHTML($content2);
								
								$html2pdf->setDefaultFont('Arial');
								$html2pdf->writeHTML($content3);
								
								if(!file_exists(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria']))
								{
									mkdir(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria']);
								}
								if(!file_exists(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria'] . '/' . $f3['cif']))
								{
									mkdir(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria'] . '/' . $f3['cif']);
								}
								
								if(file_exists(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_diploma.pdf'))
								{
									unlink(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_diploma.pdf');
								}
								
								$html2pdf->Output(PATH_ROOT . 'archivos/certificados/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_diploma.pdf', 'F');
								
								
								if($f3['recibi'])
								{
									unset($html2pdf);
									
									ob_start();
									include(dirname(__FILE__) . '/../pdf/pagina4.php');
									$content4 = ob_get_clean();
									
									$html2pdf = new HTML2PDF('P', 'A4', 'fr', false, 'ISO-8859-15');
								
									$html2pdf->setDefaultFont('Arial');
									$html2pdf->writeHTML($content4);
									
									if(!file_exists(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria']))
									{
										mkdir(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria']);
									}
									if(!file_exists(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria'] . '/' . $f3['cif']))
									{
										mkdir(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria'] . '/' . $f3['cif']);
									}
									
									if(file_exists(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_recibi.pdf'))
									{
										unlink(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_recibi.pdf');
									}
									
									$html2pdf->Output(PATH_ROOT . 'archivos/certificados_recibis/' . $f5['nombre_convocatoria'] . '/' . $f3['cif'] . '/' . $rowAlumno->dni . '_recibi.pdf', 'F');
								}
							}
							catch(HTML2PDF_exception $e)
							{
								echo $e;
							}
						}
					}
				}
			}
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'generar_pdf_diplomas');