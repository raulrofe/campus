<?php
$post = Peticion::obtenerPost();
if(Peticion::isPost() && isset($post, $post['idconv']) && is_numeric($post['idconv']))
{
	mvc::cargarModuloSoporte('cursos');
	$objConvocatoria = new Convocatoria();

	set_time_limit(0);
	ignore_user_abort(true);
	
	session_write_close();
	header("Cache-control: private");
	
	ob_end_flush();
	
	require_once PATH_ROOT . 'lib/mail/phpmailer.php';
	require_once PATH_ROOT . 'lib/mail/mail.php';
		
	mvc::cargarModuloSoporte('usuario');
	$objUsuario = new AModeloUsuario();
	
	$objMail = Mail::obtenerInstancia();
	
	// obtenemos los datos de la convocatoria
	$rowConvocatoria = $objConvocatoria->buscar_convocatoria($post['idconv']);
	
	$countError = 0;
	$countBien = 0;
			
	$emailFrom = CONFIG_EMAIL_DIPLOMAS_EMAIL;
	$emailFromName = CONFIG_EMAIL_DIPLOMAS_NAME;
	$emailSubject = "CERTIFICADO DE PARTICIPACION DEL CURSO NUEVAS TECNOLOGIAS APLICADAS A LA EDUCACION - VII EDICIONiiiihh";
	$emailContent = file_get_contents(PATH_ROOT . '/plantillas/email_diploma_provional.html');
	$objMail->addCC(CONFIG_EMAIL_DIPLOMAS_CC_EMAIL, CONFIG_EMAIL_DIPLOMAS_CC_NAME);
	
	// ------------------ //
	
	echo "Obteniendo datos de POP3<br /><br />";
	flush();
	ob_flush();
	
	$mbox = $objMail->connectPop3(CONFIG_EMAIL_DIPLOMAS_CC_HOST, CONFIG_EMAIL_DIPLOMAS_CC_PORT, CONFIG_EMAIL_DIPLOMAS_CC_EMAIL, CONFIG_EMAIL_DIPLOMAS_CC_PASSWORD);
	
	// email a los que no se le enviaran os diplomas
	$emailsNoSend = array();
	$numMsg = 0;
	
	// obtenemos los emails con un asunto especifico, para saber a que email ya se le han enviado los diplomas aetndiendo al asunto (para no enviarselos de nuevos)
	$mailsBox = $objMail->getCurrentBox();
	foreach($mailsBox as $idMsg)
	{
		$mailHeader = $objMail->getHeaderMail($idMsg);
		if(preg_match('/^' . $emailSubject . '$/', $mailHeader->subject))
		{
			$emailsNoSend[] = $mailHeader->to[0]->mailbox . '@' . $mailHeader->to[0]->host;
			
			$numMsg++;
		}
	}
	
	$objMail->closePop3();
	
	echo "Cerrando conexion POP3<br /><br />";
	flush();
	ob_flush();
	
	// ---------- //
	
	// emails a los que se le enviaran los diplomas
	$emailsToSend = array();
	
	$resultado =  $objUsuario->obtenerAlumnos();
	while($alumno = $resultado->fetch_object())
	{
		if(!in_array($alumno->email, $emailsNoSend))
		{
			$emailsToSend[] = $alumno->email;
		}
	}
	//var_dump($emailsToSend);
	foreach($emailsToSend as $itemEmail)
	{
		$objMail->clearAttachments();
		
		$resultado = $objUsuario->obtenerAlumnoPorEmail($itemEmail, $rowConvocatoria['idconvocatoria']);
		if($resultado->num_rows > 0/*== 1*/)
		{
			$alumno = $resultado->fetch_object();
			
			$pathEmailPdf = PATH_ROOT . 'archivos/certificados_provisionales/' . $rowConvocatoria['nombre_convocatoria'] . '/' . $alumno->cif . '/' . $alumno->dni . '_diploma.pdf';
			
			// si no existe el PDF lo creamos
			if(!file_exists($pathEmailPdf))
			{
				/////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\/////////////////////////////////////////
			}
			
			if(file_exists($pathEmailPdf))
			{
				$objMail->addAttachment($pathEmailPdf, 'certificado_participacion_nuevas_tecnologias_aplicadas_a_la_educacion.pdf', 'base64', 'application/pdf');
				
				if(!$objMail->enviar($emailFrom, $alumno->email, $emailFromName, $alumno->nombre . ' ' . $alumno->apellidos, $emailSubject, $emailContent)) 
				{
				  //$error = "Error: " . $mail->ErrorInfo;exit;
				  echo 'No se pudo enviar a ' . $alumno->email;
				  $countError++;
				} 
				else 
				{
				  echo "Mensaje enviado correctamente a " . $alumno->email;
				  $countBien++;
				}
			}
			else
			{
				echo 'No se encontro el pdf de ' . $alumno->email;
			}
		}
		else
		{
			echo 'No se encontro al alumno con el email ' . $itemEmail;
		}
		
		echo "<br /><br />";
		flush();
		ob_flush();
	}
	
	echo '<br>Errores: ' . $countError;
	echo '<br>Bien:' . $countBien;
	
	echo '<br>Enviados: ' . $numMsg;
	echo '<br>No enviados: ' . count($emailsToSend) . '<br>';
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'enviar_diplomas_provisionales');