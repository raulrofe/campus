<?php
$htmlLog = null;

mvc::cargarModuloSoporte('seguimiento');
$objModeloSeguimiento = new AModeloSeguimiento();

$objModeloDiplomas = new AModeloDiplomas();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	$log = array();
	
	if(isset($post['idconv']) && is_numeric($post['idconv']))
	{
		// si se ha subido un archivo del modo correcto ...
		if(isset($_FILES['archivo']['tmp_name']) && !empty($_FILES['archivo']['tmp_name']))
		{
			// comprobamos la extension del archivo (debe ser CSV)
			if(preg_match('#(.+)\.csv$#i', $_FILES['archivo']['name']))
			{
				// abre el archivo CSV
				if(($handle = fopen($_FILES['archivo']['tmp_name'], "r")) !== false)
				{
					
					
					// contadores de cursos
					$contSuccess = 0;
					$contTotal = 0;
					
					// pasamos la cabecera
					fgetcsv($handle, 1000, ";");
					
					// se mueve por cada registro del CSV
				    while(($data = fgetcsv($handle, 1000, ";")) !== false)
				    {
				    	$dni = $data[2];
						$drecibi = $data[12];
						$dlogoTripartita = $data[13];
						$dlogoMec = $data[14];
						
						if($drecibi == 'S')
						{
							$recibi = '1';
						}
						else if($drecibi == 'N')
						{
							$recibi = '0';
						}
						
						if($dlogoTripartita == 'S')
						{
							$logoTripartita = '1';
						}
						else if($dlogoTripartita == 'N')
						{
							$logoTripartita = '0';
						}
						
						if($dlogoMec == 'S')
						{
							$logoMec = '1';
						}
						else if($dlogoMec == 'N')
						{
							$logoMec = '0';
						}
						
						if(isset($recibi, $logoTripartita, $logoMec))
						{
							$rowAlumno = $objModeloDiplomas->obtenerAlumno($dni);
							if($rowAlumno->num_rows == 1)
							{
								//while($rowAlumno = $rowAlumnos->fetch_object());
								//{
									$rowAlumno = $rowAlumnos->fetch_object();
									
									if(isset($rowAlumno->idinicio_grupo))
									{
										$fecha_ini_tripartita = Fecha::invertir_fecha($data[13], '/', '-');
										$fecha_fin_tripartita = Fecha::invertir_fecha($data[14], '/', '-');;
										
										$rowDiploma = $objModeloDiplomas->obtenerDiplomaPorAlumno($rowAlumno->idalumnos, $rowAlumno->idinicio_grupo);
										if($rowDiploma->num_rows == 0)
										{
											if($objModeloDiplomas->insertarDiploma($rowAlumno->idalumnos, $rowAlumno->idinicio_grupo, $recibi, $logoTripartita, $logoMec, $fecha_ini_tripartita, $fecha_fin_tripartita))
											{
												$log[] = 'Se ha insertado el diploma del alumno con DNI ' . $rowAlumno->dni;
											}
											else
											{
												$log[] = 'Hubo un error al insertar el diploma del usuario (SQL)';
											}
										}
										else
										{
											$rowDiploma = $rowDiploma->fetch_object();
											if($objModeloDiplomas->actualizarDiploma($rowDiploma->iddiploma, $recibi, $logoTripartita, $logoMec, $fecha_ini_tripartita, $fecha_fin_tripartita))
											{
												$log[] = 'Se ha actualizado el diploma del alumno con DNI ' . $rowAlumno->dni;
											}
											else
											{
												$log[] = 'Hubo un error al insertar el diploma del usuario (SQL)';
											}
										}
									}
									else
									{
										$log[] = 'El alumno con DNI ' . $dni . ' tiene asignado un inicio de grupo no existente en esta convocatoria';
									}
								//}
							}
							else
							{
								$log[] = 'El alumno con DNI ' . $dni . ' no existe en esta convocatoria';
							}
						}
						else
						{
							$log[] = 'Algun campo no es valido';
						}
					
				    	$contTotal++;
				    }
				}
				
				 fclose($handle);
				
			}
			else
			{
				$log[] = 'El archivo debe ser tipo CSV';
			}
		}
		else
		{
			$log[] = 'Selecciona un archivo CSV para hacer la carga masiva';
		}
	}
	else
	{
		$log[] = 'Selecciona una convocatoria';
	}
	
	// errores durante el proceso de la carga
	if(count($log) > 0)
	{
		foreach($log as $item)
		{
			$htmlLog .= '<div>- ' . $item . '</div>';
		}
	}
}

if($convocatorias->num_rows > 0)
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'diplomas_carga_masiva');
}
else
{
	echo 'Debes crear antes una convocatoria';
}