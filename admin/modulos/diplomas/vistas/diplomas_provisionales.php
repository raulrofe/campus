<div id="admin_usuarios">
	<div class="titular"><h1>Diplomas provisionales - Enviar</h1></div>
	
  	<div class="stContainer" id="tabs">

	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="certificados-provisionales">Enviar certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados-provisionales/generar-pdf">Generar PDFs de certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados">Carga masiva certificados finales</a></div>
	  		<div class="btn_navegacion"><a href="certificados/generar-pdf">Generar PDFs de certificados finales</a></div>
	  	</div>
	  			
	  	<div class="tab">
		  	<form method="post" action="certificados/enviar">
				<ul>
					<li>
						<select name="idconv">
							<option value="">- Selecciona una convocatoria -</option>
							<?php while($convocatoria = $convocatorias->fetch_object()):?>
								<option value="<?php echo $convocatoria->idconvocatoria?>"><?php echo Texto::textoPlano($convocatoria->nombre_convocatoria)?></option>
							<?php endwhile;?>
						</select>
					</li>
					<li>
						<input type="submit" value="Enviar certificados" />
					</li>
				</ul>
			</form>
		</div>
	</div>
</div>