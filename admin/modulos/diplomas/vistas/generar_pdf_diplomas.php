<div id="admin_usuarios">
	<div class="titular"><h1>Diplomas finales - Generar PDFs</h1></div>
	
  	<div class="stContainer" id="tabs">

	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="certificados-provisionales">Enviar certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados-provisionales/generar-pdf">Generar PDFs de certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados">Carga masiva certificados finales</a></div>
	  		<div class="btn_navegacion"><a href="certificados/generar-pdf">Generar PDFs de certificados finales</a></div>
	  	</div>
	  			
	  	<div class="tab">
		  	<form method="post" action="">
				<ul>
					<li>
						<label>Selecciona una convocatoria</label>
						<select name="idconv" onchange="adminCertificadosGenerarPdfGoTo(this, true);">
							<option value="">- Selecciona una convocatoria -</option>
							<?php while($convocatoria = $convocatorias->fetch_object()):?>
								<option value="<?php echo $convocatoria->idconvocatoria?>" <?php if(isset($get['idconv']) && $convocatoria->idconvocatoria == $get['idconv']) echo 'selected="selected"'?>><?php echo Texto::textoPlano($convocatoria->nombre_convocatoria)?></option>
							<?php endwhile;?>
						</select>
					</li>
				</ul>
			</form>
			
			<?php if(isset($alumnos)):?>
				<br />
				<h2>Selecciona a los alumnos</h2>
				<form method="post" action="">
					<ul>
						<li id="adminCertificadoGenerarPDFBuscador">
							<label>Buscador</label>
							<input type="text" />
							<a href="#" onclick="adminCertificadosGenerarPdfSelectCheckbox(this); return false;">Seleccionar todo</a> /
							<a href="#" onclick="adminCertificadosGenerarPdfUnselectCheckbox(this); return false;">Deseleccionar todo</a>
							<div class="clear"></div>
						</li>
						<li>
							<ul id="adminCertificadoGenerarPDFListado">
								<?php while($alumno = $alumnos->fetch_object()):?>
									<li>
										<input id="adminCertificadosGenerarPDF_<?php echo $alumno->idalumnos?>" type="checkbox" name="alumnos[]" value="<?php echo $alumno->idalumnos?>" />
										<label for="adminCertificadosGenerarPDF_<?php echo $alumno->idalumnos?>" data-search-dni="<?php echo $alumno->dni?>"><?php echo $alumno->dni . ' - ' . Texto::textoPlano($alumno->apellidos . ', ' . $alumno->nombre)?></label>
									</li>
								<?php endwhile;?>
							</ul>
						</li>
						<li>
							<input type="submit" value="Generar PDFs" />
						</li>
					</ul>
				</form>
			<?php endif;?>
		</div>
	</div>
</div>