<div id="admin_usuarios">
	<div class="titular"><h1>CURSOS</h1></div>
	
  	<div class="stContainer" id="tabs">
	  	<!-- Navegacion de las pestañas -->
	  	<div class="navegacion">
	  		<div class="btn_navegacion"><a href="certificados-provisionales">Enviar certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados-provisionales/generar-pdf">Generar PDFs de certificados provisionales</a></div>
	  		<div class="btn_navegacion"><a href="certificados">Carga masiva certificados finales</a></div>
	  		<div class="btn_navegacion"><a href="certificados/generar-pdf">Generar PDFs de certificados finales</a></div>
	  	</div>

	  	<div class="tab">
	  		<form method="post" action="" enctype="multipart/form-data">
	  			<ul>
	  				<li>
	  					<select name="idconv">
			  				<option value="">- Selecciona una convocatoria -</option>
				  			<?php while($conv = $convocatorias->fetch_object()):?>
				  				<option value="<?php echo $conv->idconvocatoria?>"><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
				  			<?php endwhile;?>
			  			</select>
	  				</li>
	  				<li><input type="file" name="archivo" /></li>
	  				<li><button type="submit">Cargar</button></li>
	  			</ul>
	  		</form>
	  		<?php echo $htmlLog;?>
	  	</div>
	  </div>
</div>