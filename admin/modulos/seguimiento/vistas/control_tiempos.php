<div id="admin_usuarios">
	<div class="titular">
		<h1>SEGUIMIENTO</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>	
  		<div class="stContainer" id="tabs">	
  		<div class="tab">
  			<div class="submenuAdmin">CONTROL DE TIEMPOS</div>
  			<div class='buscador t_center'>
	  			<select name="idconv" onchange="control_tiempos_cambiar_conv(this); return false;">
	  				<option value="0">- Selecciona una convocatoria -</option>
		  			<?php while($conv = $convocatorias->fetch_object()):?>
		  				<option value="<?php echo $conv->idconvocatoria?>" <?php if($conv->idconvocatoria == $get['idconv']) echo 'selected';?>><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
		  			<?php endwhile;?>
	  			</select>
  				<br/><br/>
	  			<?php if(isset($cursos)):?>
	  				<?php if($cursos->num_rows > 0):?>
	  					<select name="idcurso" onchange="control_tiempos_cambiar_curso(<?php echo $get['idconv']?>, this); return false;">
				  			<option value="0">- Selecciona un curso</option>
				  			<?php while($curso = $cursos->fetch_object()):?>
				  				<option value="<?php echo $curso->idcurso?>" <?php if($curso->idcurso == $get['idcurso']) echo 'selected';?>><?php echo Texto::textoPlano($curso->titulo)?></option>
				  			<?php endwhile;?>
			  			</select>
	  				<?php else:?>
	  					<div>Esta convocatoria no tiene cursos</div>
	  				<?php endif;?>
	  			<?php endif;?>
  			</div>
  			<?php if(isset($alumnos)):?>
  				<div id="informes">
				<br />
				<div>
					<table class="tabla">
						<thead>
							<tr>
								<td>Alumnos</td>
								<td>Primer acceso</td>
								<td>&Uacute;ltimo acceso</td>
								<td>Tiempo total</td>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($alumnos) && $alumnos->num_rows > 0):?>
								<?php while($alumno = $alumnos->fetch_object()):?>
									<tr>
										<td><?php echo Texto::textoPlano($alumno->nombrec)?></td>
										
										<?php if(isset($tiempos[$alumno->idalumnos]['primero'])):?>
											<td class="t_center"><?php echo date('H:i:s', $tiempos[$alumno->idalumnos]['primero'])?></td>
										<?php else:?>
											<td class="t_center">00:00:00</td>
										<?php endif;?>
										
										<?php if(isset($tiempos[$alumno->idalumnos]['ultimo'])):?>
											<td class="t_center"><?php echo date('H:i:s', $tiempos[$alumno->idalumnos]['ultimo'])?></td>
										<?php else:?>
											<td class="t_center">00:00:00</td>
										<?php endif;?>
										
										<?php if(isset($tiempos[$alumno->idalumnos]['suma'])):?>
											<td class="t_center"><?php echo date('H:i:s', $tiempos[$alumno->idalumnos]['suma'])?></td>
										<?php else:?>
											<td class="t_center">00:00:00</td>
										<?php endif;?>
									</tr>
								<?php endwhile;?>
							<?php else:?>
								<tr>
									<td><strong>No hay alumnos en este curso</strong></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							<?php endif;?>
						</tbody>
					</table>
				</div>
			</div>
			<div class='t_center'>
				<input type="button" onclick="imprimir('informes'); return false;" value="Imprimir" />
			</div>
		<?php endif;?>
		</div>
	</div>
</div>