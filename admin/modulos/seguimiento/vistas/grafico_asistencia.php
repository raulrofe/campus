<div id="admin_usuarios">
	<div class="titular">
		<h1>SEGUIMIENTO</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>	
  		<div class="stContainer" id="tabs">
			
  		<div class="tab">
  			<div class="submenuAdmin">INFORME GR&Aacute;FICO DE ASISTENCIA</div>
  			<div class='buscador t_center'>
	  			<select name="idconv" onchange="grafico_asistencia_cambiar_conv(this); return false;">
	  				<option value="0">- Selecciona una convocatoria -</option>
		  			<?php while($conv = $convocatorias->fetch_object()):?>
		  				<option value="<?php echo $conv->idconvocatoria?>" <?php if($conv->idconvocatoria == $get['idconv']) echo 'selected';?>><?php echo Texto::textoPlano($conv->nombre_convocatoria)?></option>
		  			<?php endwhile;?>
	  			</select>
  				<br/><br/>
	  			<?php if(isset($cursos)):?>
	  				<?php if($cursos->num_rows > 0):?>
	  					<select name="idcurso" onchange="grafico_asistencia_cambiar_curso(<?php echo $get['idconv']?>, this); return false;">
				  			<option value="0">- Selecciona un curso</option>
				  			<?php while($item = $cursos->fetch_object()):?>
				  				<option value="<?php echo $item->idcurso?>" <?php if($item->idcurso == $get['idcurso']) echo 'selected';?>><?php echo Texto::textoPlano($item->titulo)?></option>
				  			<?php endwhile;?>
			  			</select>
	  				<?php else:?>
	  					<div>Esta convocatoria no tiene cursos</div>
	  				<?php endif;?>
	  			<?php endif;?>
  			
	  			<?php if(isset($alumnos)):?>
		  			<div id="informes">
						<form id="frmFecha" method="post" action="seguimiento/grafico-asistencia/<?php echo $get['idconv']?>/<?php echo $get['idcurso']?>" class='t_center'>
							<ul>
								<li>
									<select name="idalumno">
										<option value="0">- Todos los alumnos -</option>
										<?php while($alumno = $alumnos->fetch_object()):?>
											<option value="<?php echo $alumno->idalumnos?>"><?php echo Texto::textoPlano($alumno->nombrec)?></option>
										<?php endwhile;?>
									</select>
								</li>
								
								<?php if(isset($curso)):?>
									<li class="fleft">
										<label>Fecha inicio</label>
										<input id="datepicker1" type="text" name="fecha_ini" value="<?php if(isset($post['fecha_ini'])) echo $post['fecha_ini']; else echo date('d/m/Y', strtotime($curso->f_inicio))?>" />
									</li>
									<li class="fleft" style="margin-left: 20px;">
										<label>Fecha fin</label>
										<input id="datepicker2" type="text" name="fecha_fin" value="<?php if(isset($post['fecha_fin'])) echo $post['fecha_fin']; else echo date('d/m/Y', strtotime($curso->f_fin))?>" />
									</li>
									<li class="clear">
										<input type="submit" value="Mostrar gr&aacute;fica" />
									</li>
								<?php endif;?>
							</ul>
						</form>
						
						<div id="chart1"></div>
						
						<script type="text/javascript">
							$(document).ready(function(){
								
								<?php if(isset($data)):?>
									var line1 = [
										<?php foreach($data as $key => $value)
										{
											echo '["' . $key . '", ' . $value . '],';
										}?>
									];
									
									  var plot1 = $.jqplot('chart1', [line1], {
										  series: [{
											lineWidth: 1
									  	  }],
									      axes:{
									        xaxis:{
									          renderer:$.jqplot.DateAxisRenderer,
									          tickOptions:{
									            formatString:'%b&nbsp;%#d'
									          } 
									        },
									        yaxis:{
									          tickOptions:{
									            formatString:'%.0f'
									            }
									        }
									      },
									      highlighter: {
									        show: true,
									        sizeAdjust: 7.5
									      },
									      cursor: {
									        show: false
									      }
									  });
								<?php endif;?>
							});
						</script>	
					</div>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>

<?php if(isset($curso)):?>
	<script type="text/javascript">
		$("#datepicker1").datepicker({
			dateFormat: 'dd/mm/yy',
			firstDay : 1,
			minDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_inicio))?>),
			maxDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_fin))?>),
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		});
		
		$("#datepicker2").datepicker({
			dateFormat: 'dd/mm/yy',
			firstDay : 1,
			minDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_inicio))?>),
			maxDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_fin))?>),
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		});
	</script>
<?php endif;?>