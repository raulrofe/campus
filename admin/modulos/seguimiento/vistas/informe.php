<div id="informes_tabla">
	<div>
		<table class="tabla">
			<thead>
				<tr>
					<td>Alumnos</td>
					<?php foreach($cabeceras as $cabecera):?>
						<?php if(empty($lugaresMostrar) || (!empty($lugaresMostrar) && in_array($cabecera['idlugar'], $lugaresMostrar))):?>
							<td><span><?php echo $cabecera['nombre']?></span></td>
						<?php endif;?>
					<?php endforeach;?>
					<td>Total</td>
				</tr>
			</thead>
			<tbody>
				<?php if($alumnos->num_rows > 0):?>
					<?php while($alumno = $alumnos->fetch_object()):?>
						<tr>
							<td><?php echo Texto::textoPlano($alumno->nombrec)?></td>
							<?php reset($cabeceras)?>
							<?php $totalVisitas = 0;?>
							<?php foreach($cabeceras as $cabecera):?>
								<?php if(empty($lugaresMostrar) || (!empty($lugaresMostrar) && in_array($cabecera['idlugar'], $lugaresMostrar))):?>
									<?php $logsAcceso = $objModeloInforme->obtenerLogAccesosPorAlumno($alumno->idalumnos, $get['idcurso'], $cabecera['idlugar'], $fechaIni, $fechaFin)?>
									<?php if($logsAcceso->num_rows == 1):?>
										<?php $logAcc = $logsAcceso->fetch_object()?>
										<td class="t_center"><?php echo $logAcc->numAccesos?></td>
										<?php $totalVisitas += $logAcc->numAccesos?>
									<?php else:?>
										<td>0</td>
									<?php endif;?>
								<?php endif;?>
							<?php endforeach;?>
							
							<td><?php echo $totalVisitas?></td>
						</tr>
					<?php endwhile;?>
				<?php else:?>
					<tr><td><strong>No hay alumnos en este curso</strong></td></tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	
	<div class="t_right">
		<input type="button" onclick="imprimir('informes'); return false;" value="Imprimir" />
	</div></div>