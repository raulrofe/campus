<div id="admin_usuarios">
	<div class="titular">
		<h1>SEGUIMIENTO</h1>
		<img src='../imagenes/pie_titulos.jpg'/>
	</div>	
	<div class="stContainer" id="tabs">
		<div class="tab">
			<div class="submenuAdmin">Participaci&oacute;n/Asistencia</div>
			<div class='buscador'>
				<div class='t_center'> 
					<select name="idconv" onchange="participacion_asistencia_cambiar_conv(this); return false;">
						<option value="0"> - Seleccione una convocatoria - </option>
						<?php while ($conv = $convocatorias->fetch_object()): ?>
							<option value="<?php echo $conv->idconvocatoria ?>" <?php if ($conv->idconvocatoria == $get['idconv']) echo 'selected'; ?>><?php echo Texto::textoPlano($conv->nombre_convocatoria) ?></option>
						<?php endwhile; ?>
					</select>
				</div>
				<br/>
				<?php if (isset($cursos)): ?>
					<?php if ($cursos->num_rows > 0): ?>
						<div class='t_center'>
							<select name="idcurso" onchange="participacion_asistencia_cambiar_curso(<?php echo $get['idconv'] ?>, this); return false;">
								<option value="0"> - Seleccione un curso - </option>
								<?php while ($curso = $cursos->fetch_object()): ?>
									<option value="<?php echo $curso->idcurso ?>" <?php if ($curso->idcurso == $get['idcurso']) echo 'selected'; ?>><?php echo Texto::textoPlano($curso->titulo) ?></option>
								<?php endwhile; ?>
							</select>
						</div>
					<?php else: ?>
						<div>Esta convocatoria no tiene cursos</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>

			<?php if (isset($alumnos, $lugares)): ?>
				<div id="informes">
					<div style='width:500px;margin:0 auto;'>
						<form id="frm_informe_fecha" method="post" action="seguimiento/participacion-asistencia/<?php echo $get['idconv'] ?>/<?php echo $get['idcurso'] ?>">
							<span class='fleft'>Selecciona un alumno</span>
							<select name="idalumno" class='fleft'>
								<option value=""> - Todos los alumnos - </option>
								<?php while ($alumno = $alumnos->fetch_object()): ?>
									<option value="<?php echo $alumno->idalumnos ?>"><?php echo Texto::textoPlano($alumno->nombrec) ?></option>
								<?php endwhile; ?>
							</select>
							<br/><br/>
							<span class='fleft'>Seleccione un lugar unico</span>
							<select name="idlugar" class='fleft'>
								<option value="">Todos los lugares</option>
								<option value="principal">Lugares principales</option>
								<?php while ($lugar = $lugares->fetch_object()): ?>
									<option value="<?php echo $lugar->idlugar ?>" data-translate-html="lugares.<?php echo $lugar->nombre ?>">
										<?php echo $lugar->nombre ?>
									</option>
								<?php endwhile; ?>
							</select>
							<br/><br/>
							<div>
								<span class='fleft'>Desde<input id="datepicker" type="text" name="fecha_ini" value="<?php echo date('d/m/Y', strtotime($cursoSelect->f_inicio)) ?>" /></span>
							</div>
							<div>
								<span class='fleft'>hasta<input id="datepicker2" type="text" name="fecha_fin" value="<?php echo date('d/m/Y', strtotime($cursoSelect->f_fin)) ?>" /></span>
							</div>
							<div class='clear'></div>
							<input type="submit" value="Ver informe" />
							<div class="clear"></div>
						</form>
					</div>
				</div>

				<?php
				if (isset($pathInformes)) {
					$alumnos = $alumnos2;

					require_once $pathInformes;
				}
				?>

				<script type="text/javascript">
					$("#datepicker").datepicker({
						dateFormat: 'dd/mm/yy',
						firstDay: 1,
						dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
						monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
					});

					$("#datepicker2").datepicker({
						dateFormat: 'dd/mm/yy',
						firstDay: 1,
						dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
						monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
					});
				</script>
<?php endif; ?>
		</div>
	</div>
</div>