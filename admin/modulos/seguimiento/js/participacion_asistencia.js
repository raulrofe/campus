function participacion_asistencia_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/participacion-asistencia/' + idconv);
	}
}

function participacion_asistencia_cambiar_curso(idconv, elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/participacion-asistencia/' + idconv + '/' + idcurso);
	}
}

// ---------------------------- //

function grafico_asistencia_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/grafico-asistencia/' + idconv);
	}
}

function grafico_asistencia_cambiar_curso(idconv, elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/grafico-asistencia/' + idconv + '/' + idcurso);
	}
}

//---------------------------- //

function control_tiempos_cambiar_conv(elemento)
{
	var idconv = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/control-tiempos/' + idconv);
	}
}

function control_tiempos_cambiar_curso(idconv, elemento)
{
	var idcurso = $(elemento).prop('value');
	
	if(idconv != 0)
	{
		urlRedirect('seguimiento/control-tiempos/' + idconv + '/' + idcurso);
	}
}