<?php
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
if($convocatorias->num_rows > 0)
{
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	
	// cuando se ha seleccionado un curso (obtenemos sus alumnos)
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		require_once PATH_ROOT . 'modulos/informe/modelos/modelo.php';
		$objModeloInforme = new ModeloInforme();
		
		$idCurso = $get['idcurso'];

		$alumnos = $objModeloInforme->obtenerAlumnosPorCurso($idCurso, null, false);
		
		$logTiempos = $objModeloInforme->obtenerTiemposAccesosPorAlumno($idCurso);
		
		if($logTiempos->num_rows > 0)
		{
			$tiempos = array();
			
			$sumaTiempos = 0;
			$idAlumno = 0;
			$fecha_salida = 0;
			while($logTiempo = $logTiempos->fetch_object())
			{
				if($idAlumno == 0)
				{
					$idAlumno = $logTiempo->idalumnos;
					$tiempos[$idAlumno]['primero'] = strtotime($logTiempo->fecha_entrada);
				}
				if($logTiempo->idalumnos == $idAlumno)
				{
					$sumaTiempos += strtotime($logTiempo->fecha_salida) - strtotime($logTiempo->fecha_entrada);
				}
				else
				{
					$tiempos[$idAlumno]['suma'] = $sumaTiempos;
					$tiempos[$idAlumno]['ultimo'] = strtotime($logTiempo->fecha_salida);
					
					$idAlumno = $logTiempo->idalumnos;
					$sumaTiempos = 0;
					$sumaTiempos += strtotime($logTiempo->fecha_salida) - strtotime($logTiempo->fecha_entrada);
					$tiempos[$idAlumno]['primero'] = strtotime($logTiempo->fecha_entrada);
				}
		
				$fecha_salida = $logTiempo->fecha_salida;
			}
			$tiempos[$idAlumno]['suma'] = $sumaTiempos;
			$tiempos[$idAlumno]['ultimo'] = strtotime($fecha_salida);
			
			unset($idAlumno, $sumaTiempos, $fecha_salida);
		}
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'control_tiempos');
}