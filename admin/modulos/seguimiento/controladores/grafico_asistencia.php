<?php
$post = Peticion::obtenerPost();
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
if($convocatorias->num_rows > 0)
{
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	
	// cuando se ha seleccionado un curso (obtenemos sus alumnos)
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		$get = Peticion::obtenerGet();
		$post = Peticion::obtenerPost();
		
		require_once PATH_ROOT . 'modulos/informe/modelos/modelo.php';
		$objModeloInforme = new ModeloInforme();
		
		if(isset($get['idcurso']) && is_numeric($get['idcurso']))
		{
			$curso = $objModeloInforme->obtenerCurso($get['idcurso']);
			
			if($curso->num_rows == 1)
			{
				$curso = $curso->fetch_object();
				
				$alumnos = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], null, false);
				
				if(isset($post['fecha_ini'], $post['fecha_fin']))
				{
					if(isset($post['idalumno']) && is_numeric($post['idalumno']) && $post['idalumno'] != 0)
					{
						$alumnosGrafica = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], $post['idalumno'], false);
					}
					else
					{
						$alumnosGrafica = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], null, false);
					}	
							
					// convertimos las fechas a array
					$dateStart = explode('/', $post['fecha_ini']);
					$dateEnd = explode('/', $post['fecha_fin']);
														
					// comprobamos si las fechas son validas
					if((count($dateStart) == 3 && count($dateEnd) == 3) &&
						(checkdate($dateStart[1], $dateStart[0], $dateStart[2]) && checkdate($dateEnd[1], $dateEnd[0], $dateEnd[2])))
					{
						$dateStartCourse = $dateStart[2] . '-' . $dateStart[1] . '-' . $dateStart[0];
						$dateEndCourse = $dateEnd[2] . '-' . $dateEnd[1] . '-' . $dateEnd[0];
							
						$diasCurso = ceil((strtotime($dateEndCourse) - strtotime($dateStartCourse)) / 86400);
							
						$data = array();
						
						while($unAlumnoGrafica = $alumnosGrafica->fetch_object())
						{
							// inserta a un array todos los dias que duro el curso
							for($c = 0; $c <= $diasCurso; $c++)
							{
								$data[date('d-M-y', strtotime($dateStartCourse) + 86400 * $c)] = 0;
							}
							
							// obtenemos los datos de accesos del usuario
							$lugares = $objModeloInforme->obtenerLugares();
							while($lugar = $lugares->fetch_object())
							{
								$logAcceso = $objModeloInforme->obtenerLogAccesosPorAlumno(
									$post['idalumno'],
									$get['idcurso'],
									$lugar->idlugar,
									$dateStart[2] . '-' . $dateStart[1] . '-' . $dateStart[0],
									$dateEnd[2] . '-' . $dateEnd[1] . '-' . $dateEnd[0]
								);
								
								if($logAcceso->num_rows > 0)
								{
									while($item = $logAcceso->fetch_object())
									{
										if(isset($item->fecha_entrada, $data[date('d-M-y', strtotime($item->fecha_entrada))]))
										{
											$data[date('d-M-y', strtotime($item->fecha_entrada))] += $item->numAccesos;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'grafico_asistencia');
}
