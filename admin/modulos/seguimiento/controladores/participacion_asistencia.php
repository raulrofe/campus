<?php
$objModeloSeguimiento = new AModeloSeguimiento();

// obtenemos las convocatorias
$convocatorias = $objModeloSeguimiento->obtenerConvocatorias();
if($convocatorias->num_rows > 0)
{
	// cuando se ha seleccionado una convocatoria (obtenemos ls cursos de esa onvocatoria)
	$get = Peticion::obtenerGet();
	if(isset($get['idconv']) && is_numeric($get['idconv']))
	{
		$cursos = $objModeloSeguimiento->obtenerCursosPorConvocatoria($get['idconv']);
	}
	
	// cuando se ha seleccionado un curso (obtenemos sus alumnos)
	if(isset($get['idcurso']) && is_numeric($get['idcurso']))
	{
		require_once PATH_ROOT . 'modulos/informe/modelos/modelo.php';
		$objModeloInforme = new ModeloInforme();
		
		$cabeceraLugares = $objModeloInforme->obtenerLugares();
		
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			if(isset($post['fecha_ini'], $post['fecha_fin']))
			{
				$fechaIni = Fecha::invertir_fecha($post['fecha_ini'], '/');
				$fechaFin = Fecha::invertir_fecha($post['fecha_fin'], '/');
				
				$cabeceras = array();
				while($cabeceraLugar = $cabeceraLugares->fetch_object())
				{
					$cabeceras[] = array('idlugar' => $cabeceraLugar->idlugar, 'nombre' => $cabeceraLugar->nombre);
				}
				
				$lugaresMostrar = array();
				if(isset($post['idlugar']))
				{
					if(is_numeric($post['idlugar']))
					{
						$lugaresMostrar[] = $post['idlugar'];
					}
					else if($post['idlugar'] == 'principal')
					{
						$lugaresMostrar = array(1, 2, 3, 5);
					}
				}
				
				if(isset($post['idalumno'], $post['idlugar']) && is_numeric($post['idalumno']))
				{
					$alumnos2 = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], $post['idalumno'], false);
				}
				else
				{
					$alumnos2 = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], null, false);
				}
				
				$pathInformes = mvc::obtenerRutaVista(dirname(__FILE__), 'informe');
			}
		}
		
		$cursoSelect = $objModeloInforme->obtenerCurso($get['idcurso']);
		$cursoSelect = $cursoSelect->fetch_object();
			
		$lugares = $objModeloInforme->obtenerLugares();
		
		$alumnos = $objModeloInforme->obtenerAlumnosPorCurso($get['idcurso'], null, false);
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'participacion_asistencia');
}