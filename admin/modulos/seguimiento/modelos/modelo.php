<?php
class AModeloSeguimiento extends modeloExtend
{
	public function obtenerConvocatorias($soloActivos = false)
	{
		$sql = 'SELECT * FROM convocatoria';
		if($soloActivos)
		{
			$sql .= ' WHERE borrado = 0';
		}
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerCursosPorConvocatoria($idconvocatoria)
	{
		$sql = 'SELECT c.idcurso, c.titulo, c.idconvocatoria, c.f_inicio, c.f_fin FROM curso AS c WHERE c.idconvocatoria = ' . $idconvocatoria;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
}