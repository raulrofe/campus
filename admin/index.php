<?php
define('I_EXEC', true);

ob_start();
session_name("campusadmin");
session_start();

date_default_timezone_set('Europe/Madrid');

define('IS_ADMIN', true);

require_once '../config.php';
require_once 'framework.php';

Log::start();

$get = Peticion::obtenerGet();

if(Usuario::is_login() && Usuario::compareProfile('administrador'))
{
	// WEB SIN HTML BASE
	//$get = Peticion::obtenerGet();
	if(isset($get['t']) && $get['t'] == 'simple') {
		$ruta = mvc::obtenerRutaControlador();
		if(isset($ruta))
		{
			mvc::importar($ruta);
		}
	} else {
		if(isset($get['menu']) && $get['menu'] == 'menu_acordeon') {
			require_once 'menu_acordeon.php';
		} else if(isset($get['menu']) && $get['menu'] == 'iframe') {
			require_once 'html.php';
		} else {
			//Conexion a base de datos
			$db = new database();
			$con = $db->conectar();
			
			//Obtenemos el logo para mostrarlo
			$mi_aula = new Aula($con);
			$logotipo = $mi_aula->custom_logo();
			
			require_once 'contenido.php';
		}
	}
} else if(isset($get['m']) && $get['m'] == 'acceso') {
	require_once 'contenido_login.php';
} else if($get['m'] == 'cursos' && $get['c'] == 'curso_web') {
	$ruta = mvc::obtenerRutaControlador();
	
	if(isset($ruta))
	{
		mvc::importar($ruta);
	}
}