<?php
function __autoload($class)
{
	$fileAdmin = PATH_ROOT_ADMIN . 'lib/' . strtolower($class) . '.php';
	$file = PATH_ROOT . 'lib/' . strtolower($class) . '.php';
	
	if(is_file($fileAdmin))
	{
		require_once $fileAdmin;
	}
	else if(is_file($file))
	{
		require_once $file;
	}
	
}

$libs = array('mensajes_emergentes');
foreach($libs as $lib)
{
	$pathAdmin = PATH_ROOT_ADMIN . 'lib/' . $lib . '/';
	$fileBaseAdmin = $pathAdmin . $lib . '.php';
	$fileModeloAdmin = $pathAdmin . 'modelo.php';
	
	$path = PATH_ROOT . 'lib/' . $lib . '/';
	$fileBase = $path . $lib . '.php';
	$fileModelo = $path . 'modelo.php';
	
	if(is_file($pathAdmin) && is_file($fileModeloAdmin))
	{
		require_once $fileBaseAdmin;
		require_once $fileModeloAdmin;
	}
	else
	{
		require_once $fileBase;
		require_once $fileModelo;
	}
}