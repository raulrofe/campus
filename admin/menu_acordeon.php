	        <!-- Left Sidebar -->
            <aside id="page-sidebar" class="nav-collapse collapse">
                <div class="side-scrollable">
                    <!-- Mini Profile -->
                    <div class="mini-profile">
                        <div class="mini-profile-options">
                            <a href="javascript:void(0)" class="badge badge-info loading-on" data-toggle="tooltip" data-placement="right" title="Refresh">
                            	<i class="icon-refresh"></i>
                            </a>
                            <a href="page_ready_shopping_cart.html" class="badge badge-warning" data-toggle="tooltip" data-placement="right" title="6 Products">
                                <i class="glyphicon-shopping_cart"></i>
                            </a>
                            <!-- Modal div is at the bottom of the page before including javascript code, we use .enable-tooltip class for the tooltip because data-toggle is used for modal -->
                            <a href="#modal-user-account" class="badge badge-success enable-tooltip" role="button" data-toggle="modal" data-placement="right" title="Settings">
                                <i class="glyphicon-cogwheel"></i>
                            </a>
                            <a href="salir" class="badge badge-important" data-toggle="tooltip" data-placement="right" title="Salir">
                                <i class="icon-signout"></i>
                            </a>
                        </div>
                        <a href="page_ready_user_profile.html"><img src="img/template/avatar2.jpg" alt="Avatar" class="img-circle"></a>
                    </div>
                    <!-- END Mini Profile -->

                    <!-- Sidebar Tabs -->
                    <div class="sidebar-tabs-con">
                        <div class="tab-content">
                            <div class="tab-pane active" id="side-tab-menu">
                                <!-- Primary Navigation -->
                                <nav id="primary-nav">
                                    <ul>
                                        <li <?php if($get['c'] == 'invitados' || $get['c'] == 'alumnos' || $get['c'] == 'carga-masiva' || $get['c'] == 'rrhh') echo 'class="active"'?>>
                                            <a href="#" class="menu-link" onclick="return false;"><i class="glyphicon-group"></i>Usuarios</a>
                                            <ul>
                                            	<li><a href='usuarios/invitados' <?php if($get['c'] == 'invitados') echo 'class="active"'?>>Invitados</a></li>
												<li><a  href='usuarios/alumnos' <?php if($get['c'] == 'alumnos' || $get['c'] == 'carga-masiva') echo 'class="active"'?>>Alumnos</a></li>
												<li><a  href='usuarios/administradores' <?php if($get['c'] == 'rrhh' && $get['perfil'] == 'administrador') echo 'class="active"'?>>Administradores</a></li>
												<li><a  href='usuarios/supervisores' <?php if($get['c'] == 'rrhh' && $get['perfil'] == 'supervisor') echo 'class="active"'?>>Supervisores</a></li>
												<li><a  href='usuarios/coordinadores' <?php if($get['c'] == 'rrhh' && $get['perfil'] == 'coordinador') echo 'class="active"'?>>Coordinadores</a></li>
												<li><a  href='usuarios/tutores' <?php if($get['c'] == 'rrhh' && $get['perfil'] == 'tutor') echo 'class="active"'?>>Tutores</a></li>
                                            </ul>
                                        </li>
                                        <li <?php if($get['c'] == 'tematica_autoevaluaciones' || $get['c'] == 'tematica_biblioteca') echo 'class="active"'?>>
                                            <a href="#" class="menu-link" onclick="return false;"><i class="glyphicon-table"></i>Tem&aacute;ticas</a>
                                            <ul>
												<li><a  href='tematicas/autoevaluaciones' <?php if($get['c'] == 'tematica_autoevaluaciones') echo 'class="active"'?>>Autoevaluaciones</a></li>
												<li><a  href='tematicas/biblioteca' <?php if($get['c'] == 'tematica_biblioteca') echo 'class="active"'?>>Biblioteca</a></li>                                            
											</ul>
                                        </li>
                                        <li <?php if(($get['c'] == 'content' && $get['m'] == 'contenidos') || $get['c'] == 'ficheros' || ($get['c'] == 'index' && $get['m'] == 'scorm') || $get['c'] == 'ficheros_trabajos' || $get['c'] == 'autoevaluaciones' || $get['c'] == 'asignar-autoevaluacion' || $get['c'] == 'temas') echo 'class="active"'?>>
                                            <a href="#" class="menu-link" onclick="return false;"><i class="halflingicon-file"></i>Contenidos</a>
                                            <ul>
												<li><a  href='contenidos' <?php if($get['c'] == 'content' && $get['m'] == 'contenidos') echo 'class="active"'?>>M&oacute;dulos</a></li>
                                                <li><a  href="contenidos/autoevaluaciones" <?php if($get['c'] == 'autoevaluaciones') echo 'class="active"'?>>Autoevaluaciones</a></li>
							  					<!-- <li><a  href="contenidos/ficheros" <?php //if($get['c'] == 'ficheros') echo 'class="active"'?>>Contenido sin SCORM</a></li> -->
							  					<!-- <li><a  href="contenidos/multimedia" <?php //if($get['c'] == 'index' && $get['m'] != 'welcome') echo 'class="active"'?>>Contenido con SCORM</a></li> -->
							  					<!-- <li><a  href="contenidos/trabajos_practicos" <?php //if($get['c'] == 'ficheros_trabajos') echo 'class="active"'?>>Trabajos pr&aacute;cticos</a></li> -->
							  					<!-- <li><a  href="contenidos/autoevaluaciones/asignar" <?php //if($get['c'] == 'asignar-autoevaluacion') echo 'class="active"'?>>Asignar Autoevaluaciones</a></li> -->
												<!-- <li><a  href="foros" <?php //if($get['c'] == 'temas') echo 'class="active"'?>>temas</a></li> -->
                                            </ul>
                                        </li>
                                        <!-- 
                                        <li <?php //if($get['c'] == 'acciones_formativas' || $get['c'] == 'carga_masiva') echo 'class="active"'?>>
                                            <a href="#" class="menu-link" onclick="return false"><i class="halflingicon-folder-open"></i>Acciones Formativas o cursos</a>
                                            <ul>
												<li><a  href='contenidos/acciones_formativas' <?php //if($get['c'] == 'acciones_formativas') echo 'class="active"' ?>>Acciones formativas</a></li>
												<li><a  href="contenidos/acciones_formativas/carga_masiva" <?php //if($get['c'] == 'carga_masiva') echo 'class="active"' ?>>Carga masiva</a></li>
                                            </ul>
                                        </li>
                                         -->
                                        <li <?php if($get['c'] == 'tipo_curso' || 
                                        			($get['c'] == 'curso' && $get['m'] == 'cursos') || 
                                        			$get['c'] == 'alta' || 
                                        			($get['c'] == 'index' && $get['m'] == 'agenda') || 
                                        			($get['c'] == 'asignar_calificaciones')) echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="halflingicon-book"></i>Acciones formativas</a>
                                            <ul>
                                                <li><a  href='cursos/tipo_curso' <?php if($get['c'] == 'tipo_curso') echo 'class="active"'?>>Tipo curso</a></li>
                                                <li><a  href='cursos' <?php if($get['c'] == 'curso' && $get['m'] == 'cursos') echo 'class="active"'?>>Cursos</a></li>
                                                <li><a  href='cursos/alta' <?php if($get['c'] == 'alta') echo 'class="active"'?>>Alta cursos</a></li>
                                                <li><a  href='calificaciones/asignar' <?php if($get['c'] == 'asignar_calificaciones') echo 'class="active"'?>>Asingnar calificaciones</a></li>
                                                <li><a  href='agenda/recomendaciones' <?php if($get['c'] == 'index' && $get['m'] == 'agenda') echo 'class="active"'?>>Agenda</a></li>
                                            </ul>
                                        </li>
                                        <li <?php if(($get['c'] == 'ponderacion_notas' && $get['f'] != 'asignar') || $get['c'] == 'notas_automaticas' || $get['c'] == 'config_notas') echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="glyphicon-certificate"></i>Calificaciones</a>
                                            <ul>
                                                <li><a  href='calificaciones/ponderacion-notas' <?php if($get['c'] == 'ponderacion_notas' && $get['f'] != 'asignar') echo 'class="active"'?>>Ponderaci&oacute;n de notas</a></li>
                                                <!-- <li><a  href='calificaciones/ponderacion-notas/asignar' <?php //if($get['c'] == 'ponderacion_notas' && $get['f'] == 'asignar') echo 'class="active"'?>>Asignar Ponderaci&oacute;n de notas</a></li> -->
                                               	<li><a  href='calificaciones/configuracion-de-notas' <?php if($get['c'] == 'config_notas' && $get['f'] != 'asignar') echo 'class="active"'?>>Configuraci&oacute;n de notas</a></li>
                                                <!-- <li><a  href='calificaciones/notas-automaticas' <?php //if($get['c'] == 'notas_automaticas') echo 'class="active"'?>>Notas autom&aacute;ticas</a></li> -->
                                                <!-- <li><a  href='calificaciones/configuracion-de-notas/asignar' <?php //if($get['c'] == 'config_notas' && $get['f'] == 'asignar') echo 'class="active"'?>>Asignar Configuraci&oacute;n de notas</a></li> -->
                                            </ul>
                                        </li>
                                        <li <?php if($get['m'] == 'seguimiento') echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="halflingicon-signal"></i>Seguimiento</a>
                                            <ul>
                                                <li><a  href='seguimiento/participacion-asistencia' <?php if($get['c'] == 'participacion_asistencia') echo 'class="active"'?>>Participaci&oacute;n/Asistencia</a></li>
                                                <li><a  href='seguimiento/grafico-asistencia' <?php if($get['c'] == 'grafico_asistencia') echo 'class="active"'?>>Informe gr&aacute;fico de asistencia</a></li>
                                                <li><a  href='seguimiento/control-tiempos' <?php if($get['c'] == 'control_tiempos') echo 'class="active"'?>>Control de tiempos</a></li>
                                            </ul>
                                        </li>
                                        <li <?php if($get['m'] == 'solicitudes') echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="glyphicon-folder_flag"></i>Solicitudes</a>
                                            <ul>
                                                <li><a  href='solicitudes/cambio-datos-personales' <?php if($get['c'] == 'cambio_datos_personales') echo 'class="active"'?>>Cambio datos personales</a></li>
                                            </ul>
                                        </li>
                                        <!-- 
                                        <li>
                                            <a href="#" class="menu-link"><i class="glyphicon-notes"></i>Informes</a>
                                            <ul>
                                                <li><a  href='controlador_admin.php?c=informacion'>Informaci&oacute;n</a></li>
                                                <li><a  href='controlador_admin.php?c=matriculacion'>Matriculaci&oacute;n</a></li>
                                            </ul>
                                        </li>
                                         -->
                                        <li <?php if($get['m'] == 'configuraciones') echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="glyphicon-cogwheels"></i>Configuraci&oacute;n</a>
                                            <ul>
                                                <li><a  href='configuracion/general' <?php if($get['c'] == 'general') echo 'class="active"'?>>Configuraci&oacute;n general</a></li>
                                                <li><a  href='configuracion/grafica' <?php if($get['c'] == 'grafica') echo 'class="active"'?>>Configuraci&oacute;n gr&aacute;fica</a></li>
                                            </ul>
                                        </li>
                                        <li <?php if($get['m'] == 'servicios') echo 'class="active"'?>>
                                            <a href="#" class="menu-link"><i class="glyphicon-certificate"></i>Servicios</a>
                                            <ul>
                                                <li><a  href='servicios/general' <?php if($get['m'] == 'servicios' && $get['c'] == 'general') echo 'class="active"'?>>General</a></li>
                                                <li><a  href='servicios' <?php if($get['m'] == 'servicios' && $get['c'] == 'index') echo 'class="active"'?>>Tutores</a></li>
                                            </ul>
                                        </li>                                        
                                    </ul>
                                </nav>
                                <!-- END Primary Navigation -->
                            </div>
                        </div>
                    </div>
                    <!-- END Sidebar Tabs -->
                </div>
                <!-- END Wrapper for scrolling functionality -->
            </aside>
            <!-- END Left Sidebar -->