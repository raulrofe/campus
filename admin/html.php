<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head profile="http://gmpg.org/xfn/11">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>AulaInteractiva - Administraci&oacute;n</title>     
        <base href="<?php echo URL_BASE_ADMIN?>" />

        <meta name="description" content="FlatApp is a Premium, Responsive and Flat Bootstrap Admin Dashboard Template created by pixelcave and published on Themeforest." />
        <meta name="author" content="Campus aula_interactiva" />
        <meta name="robots" content="noindex,nofollow,noimageindex" />

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="../archivos/config_web/favicon.ico" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57-precomposed.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- The roboto font is included from Google Web Fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic" />

        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="../css/admin_flat/bootstrap.css" />

        <!-- Related styles of various icon packs and javascript plugins -->
        <link rel="stylesheet" href="../css/admin_flat/plugins.css" />

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="../css/admin_flat/main.css" />

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of all the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements (must included last) -->
        <link rel="stylesheet" href="../css/admin_flat/themes.css" />
        <!-- END Stylesheets -->

        <!-- Modernizr (Browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it) -->
        <script src="js/admin_flat/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
	
    <!-- Body -->
    <body>   
		<frameset noresize="" frameborder="no" framespacing="0" border="0" cols="200,*">
			<frame scrolling="auto" noresize="" framespacing="2" frameborder="no" border="0" name="admin_menu" src="index.php?menu=menu_acordeon" style='overflow:hidden'></frame>
			<frame scrolling="auto" noresize="" framespacing="0" frameborder="no" border="0" name="admin_content" src="index.php" style='overflow:auto;overflow-x:hidden;' id="admin_content"></frame>
		</frameset>
    </body>
</html>