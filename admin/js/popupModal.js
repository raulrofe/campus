//window.urlAnchor = null;
//window.historyPosCurrent = 0;

function popup_real_open(name, url, height, width)
{
	window.open(url, name, 'height=' + height + ',width=' + width);
}

function popup_open(name, url, height, width, showIcons)
{
	LibPopupModal.open(name, url, height, width, showIcons);
}

function popup_simple_open(name, url, height, width)
{
	LibPopupModal.open('', name, url, height, width);
}

var LibPopupModal = {};

LibPopupModal.open = function(titleShow, title, url, height, width, showIcons)
{
	// mostrar o ono lo siconos de las cabeceras y el link de abajo del popupModal
	if(showIcons != undefined && showIcons != null && showIcons == false)
	{
		showIcons = false;
	}
	else
	{
		showIcons = true;
	}
	
	var popupId = 'popupModal_' + title;
	
	// calculamos las dimensiones exactas del modalbox segun la pantalla
	var top = 100;
	var heightScreen = $(window).height();
	if(heightScreen <= (height + top + 250))
	{
		height = heightScreen - 250;
		top = 50;
	}

	// si la ventana ya estaba minimizada ...
	if($('#popupBtn_' + popupId).size() > 0)
	{
		$('#popupBtn_' + popupId).remove();
		$('#' + popupId).removeClass('hide');
	}
	
	// si no estaba abierta esta ventana
	if($('#' + popupId).size() == 0)
	{
		if($('#principalNombreCurso').size() == 1)
		{
			var nombreCurso = $('#principalNombreCurso').html();
			var addpuntos = '';
			if(nombreCurso.length > 30)
			{
				addpuntos = '...';
			}
			
			nombreCurso = nombreCurso.substr(0,30) + addpuntos;
		}
		else
		{
			var nombreCurso = '';
		}
		
		var html =
		'<div id="' + popupId + '" class="popupModal" data-url="' + url + '" style="width:' + (width - 6) + 'px; top: ' + top + 'px">' +
			'<div class="popupModalWrapper">' +
				'<div class="popupModalHeader">';
					if(showIcons)
					{
						html +=
						'<div class="popupModalHeaderHistory">' +
							'<a class="popupModalHeaderHistory_back" href="#" onclick="LibPopupModal.historyBack(\'' + popupId + '\'); return false;" title="Ir a atr&aacute;s"><img src="imagenes/popupModal/window_back.png" alt="" /></a>' +
							/*'<a class="popupModalHeaderHistory_forward" href="#" onclick="' + popupId + '_iframe.history.forward(); return false;" title="Ir hacia delante"><img src="imagenes/popupModal/window_forward.png" alt="" /></a>' +*/
						'</div>';
					}
					html += '<h2>' + titleShow + '</h2>' +
							'<div class="popupModalHeaderLoader hide"><img src="imagenes/loaderPopupMini.gif" alt=""></div>';
					if(showIcons)
					{
						html +=
						'<a href="#" class="popupModalHeaderIconReload" onclick="LibPopupModal.reload(\'' + popupId + '\'); return false;" title="Recargar"><img src="imagenes/popupModal/window_reload.png" alt="" /></a>' +
						'<a href="#" onclick="LibPopupModal.minimize(\'' + popupId + '\', \'' + title + '\'); return false;" title="Minimizar"><img src="imagenes/popupModal/window_minimize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_maximize" onclick="LibPopupModal.maximize(\'' + popupId + '\', \'' + title + '\'); return false;" title="Maximizar"><img src="imagenes/popupModal/window_maximize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_restore hide" onclick="LibPopupModal.restore(null, \'' + popupId + '\', ' + height + ', ' + width + '); return false;" title="Restaurar"><img src="imagenes/popupModal/window_restore.png" alt="" /></a>' +
						'<a href="#" onclick="LibPopupModal.close(\'' + popupId + '\'); return false;" title="Cerrar"><img src="imagenes/popupModal/window_close.png" alt="" /></a>';
					}
				html +=
				'</div>' +
				/*'<div class="popupNombreCurso clear"><b>Curso:</b> ' + $('#principalNombreCurso').html() + '</div>' +*/
					'<div id="' + popupId + '_wrapper_aux" style="width:100%;height:' + height + 'px;background:#fff;overflow:auto;">' +
						'<div class="popupModalContent"><img class="popupModalLoader" src="imagenes/popupModalLoader.gif" alt="Cargando" />'+
						'<div id="' + popupId + '_iframe" style="height:100%;" name="' + popupId + '_iframe" class="hide"></div></div>' +
					'</div>';
				if(showIcons)
				{
					html += '<div class="windowPopupCloseBtn"><a href="#" onclick="LibPopupModal.close(\'' + popupId + '\'); return false;">Cerrar ventana</a></div>';
				}
			html += '<iframe id="' + popupId + '_form_iframe" name="' + popupId + '_form_iframe" class="hide"></iframe>' +
				'<ul class="popupHistory hide"></ul>' +
				'</div>' +
		'</div>';
		
		$('body').append(html);

		// a_ade boton de popup
		$('#popupsButtons').append('<div id="popupBtn_' + popupId + '" class="popupButtonFixed"><a href="#" onclick="LibPopupModal.restore(this, \'' + popupId + '\'); return false;" title="">' + titleShow + ' <img src="imagenes/popupModal/window_restore.png" alt="" /></a></div>');

		
		var left = ($(window).width() - $('#' + popupId).width()) / 2;
		$('#' + popupId).css('left', left + 'px');
		
		/*$('#' + popupId).draggable({
			handle : '.popupModalWrapper',
			containment : 'window'
		});*/
		
		$('#' + popupId + '_form_iframe').load(function()
		{
			setTimeout(function()
			{
				var iframeHtml = $('#' + popupId + '_form_iframe').contents().find('body').html();

				if(iframeHtml != null && iframeHtml != '')
				{
					$('#' + popupId + '_iframe').html(iframeHtml);

					// para en envio de formularios dentro del modalbox
					LibPopupModal._repareForms(popupId);

					$('#' + popupId).find('.popupModalHeaderLoader').addClass('hide');
					
					var urlReturn = $('#' + popupId).find('.popupModalUrlCurrent').html();
					
					if($('#' + popupId).attr('data-url') != urlReturn)
					{
						LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
					}
					
					$('#' + popupId).attr('data-url', urlReturn);
				}
			}, 200);
		});
		
		LibPopupModal.indexTop(popupId);
		
		// cuando se cargue el contenido del iframe
		$.ajax({
			url: url,
			success: function(data)
			{
				$('#' + popupId).find('.popupModalLoader').addClass('hide');
				$('#' + popupId + '_iframe').html(data);
				
				$('#' + popupId + '_iframe').removeClass('hide');
				
				var left = ($(window).width() - $('#' + popupId).width()) / 2;
				$('#' + popupId).css('left', left + 'px');
				
				$('#' + popupId).draggable({
					handle : '.popupModalHeader',
					containment : 'document',
					start : function()
					{
						LibPopupModal.indexTop(popupId);
					}
				});
				
				$('#' + popupId).resizable({
					alsoResize  : /*'#' + popupId + ' .popupModalWrapper, */'#' + popupId + '_wrapper_aux',
					minHeight   : 400,
					minWidth    : 400,
					containment : 'document'
					/*resize : function(event, ui)
					{
						var height = ui.size.height - (ui.originalSize.height - ui.position.top);
						$('#' + popupId + '_wrapper_aux').height(height + 'px');
						$('#' + popupId + ' _wrapper_aux').html(height);
					}*/
				});
				
				$('#' + popupId).find('form').live('submit', function()
				{
					$('#' + popupId).find('.popupModalHeaderLoader').removeClass('hide');
				});
	
				LibPopupModal.indexTop(popupId);
	
				// evento de click para z-index
				$('#' + popupId + '_iframe').click(function(){
					LibPopupModal.indexTop(popupId);
				});
				
				LibPopupModal._repareForms(popupId);
				
				// evento al hacer click en un link del modalbox
				$('#' + popupId + '_iframe a').live('click', function()
				{					
					if($(this).attr('data-alert-confirm') == undefined)
					{
						if($('#' + popupId).attr('data-url') != undefined)
						{
							LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
						}
						
						var urlAnchor = $(this).attr('href');
						var targetAnchor = $(this).attr('target');
						
						if(urlAnchor != '' && urlAnchor != '#'
							&& (targetAnchor == undefined || (targetAnchor != undefined && targetAnchor != '_blank')))
						{
							if($(this).attr('data-nofollow') == undefined)
							{
								$('#' + popupId).attr('data-url', urlAnchor);
								//$('#' + popupId).attr('data-url', $(this).attr('href'));
								//$('#' + popupId).attr('data-url-current', urlAnchor);
							}
	
							LibPopupModal._ajaxLoad(popupId, urlAnchor);
						}
	
						if(targetAnchor == undefined || (targetAnchor != undefined && targetAnchor != '_blank'))
						{
							return false;
						}
					}
				});
				
				LibPopupModal.sendLogAccess(popupId);
			},
			error : function()
			{
				$('#' + popupId + '_iframe').html('No se pudo cargar');
			}
		});
		
		// cargamos el contenido en el iframe
		//$('#' + popupId) .find('.popupModalContent iframe').attr('src', url + '?iframe_name=' + popupId);
	}
	// si estaba abierta se recarga el contenido del iframe
	else
	{
		// si la ventana ya estaba minimizada ...
		if($('#popupBtn_' + popupId).size() > 0)
		{
			$('#popupBtn_' + popupId).remove();
			$('#' + popupId).removeClass('hide');
		}
		
		LibPopupModal.indexTop(popupId);
		$('#' + popupId).focus();
	}
};

LibPopupModal.loadPage = function(element, url)
{
	var popupId = $(element).parents('.popupModal').attr('id');
	
	if($('#' + popupId).attr('data-url') != undefined)
	{
		LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
	}
	
	$('#' + popupId).attr('data-url', url);
	//$('#' + popupId).attr('data-url-current', url);

	LibPopupModal._ajaxLoad(popupId, url);
};

LibPopupModal._ajaxLoad = function(popupId, url, reload)
{
	$('#' + popupId + '_iframe').addClass('hide');
	$('#' + popupId).find('.popupModalHeaderLoader').removeClass('hide');
	
	var urlReferer = {};
	var urlCurrent = $('#' + popupId).attr('data-url');
	var getUrlReferer = getParamRequestGet(urlCurrent, 'url-referer');
	
	if(url != urlCurrent)
	{
		if(urlReferer == '')
		{
			urlReferer = {'url-referer' : encodeURIComponent(urlCurrent)};
		}
		else
		{
			urlCurrent = removeParamRequestGet(urlCurrent, 'url-referer');
			
			urlReferer = {'url-referer' : encodeURIComponent(urlCurrent)};
		}
		/*var urlPartSearch = window.location.search;
		if(!urlPartSearch.match(/url\-referer=/))
		{*/
		//}
	}
	
	if(reload != undefined && reload == true)
	{
		urlReferer.refresh = '1';
	}
	
	$.ajax({
		url: url,
		data : urlReferer,
		success: function(data)
		{
			$('#' + popupId).find('.popupModalHeaderLoader').addClass('hide');
			$('#' + popupId + '_iframe').html(data);
			
			$('#' + popupId + '_iframe').removeClass('hide');

			// para en envio de formularios dentro del modalbox
			LibPopupModal._repareForms(popupId);
		},
		error: function()
		{
			$('#' + popupId + '_iframe').html('No se pudo cargar');
		}
	});
};
/*
LibPopupModal.historyForward = function(popupId)
{
	
};
*/

LibPopupModal.historyBack = function(popupId)
{
	var count = $('#' + popupId).find('.popupHistory li').size();
	
	if(count > 0)
	{
		//count = window.historyPosCurrent;
		
		if($('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').size() == 1)
		{
			var url = $('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').html();

			LibPopupModal._ajaxLoad(popupId, url);
			
			//window.historyPosCurrent--;
			
			$('#' + popupId).attr('data-url', url);
			//$('#' + popupId).attr('data-url-current', url);

			//window.urlAnchor = url;
			
			$('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').remove();
		}
	}
};

LibPopupModal.addHistory = function(popupId, url)
{
	var count = $('#' + popupId).find('.popupHistory li').size();
	
	if($('#' + popupId).find('.popupHistory').find('li').last().html() != url)
	{
		$('#' + popupId).find('.popupHistory').append('<li data-pos="' + (count + 1) + '">' + url + '</li>');
	}
};

LibPopupModal._repareForms = function(popupId)
{
	// para en envio de formularios dentro del modalbox
	$('#' + popupId + '_iframe form').each(function(index)
	{
		var formAction = $(this).attr('action');
		//var url = /*$('head base').attr('href') + */ $('#' + popupId).attr('data-url-current');
		var url = $('#' + popupId).find('.popupModalUrlCurrent').html();
		
		if(formAction == undefined || formAction == null || formAction == '' || formAction == '#')
		{
			formAction = /*'action/' + */url;
		}
		else
		{
			//formAction = /*'action/' + */formAction;
		}
		
		$(this).attr('action', formAction);
		$(this).attr('target', popupId + '_form_iframe');

		$(this).append('<div><input type="hidden" name="url-referer" value="' + url + '" /></div>');
		
		//$("input:checkbox, input:radio, textarea, select,").uniform();
		
		$('#' + popupId).attr('data-url', url);
		//window.historyPosCurrent++;
		
		/*$(this).submit(function(e)
		{
			//var htmlOjData = e.target;
			
			//console.log(e);
		});*/
	 });
};

LibPopupModal.reload = function(popupId)
{
	/*$('#' + popupId).find('.popupModalContent iframe').addClass('hide');
	$('#' + popupId).find('.popupModalLoader').removeClass('hide');
	
	// cuando se cargue el contenido del iframe
	$('#' + popupId).find('.popupModalContent iframe').load(function()
	{
		$('#' + popupId).find('.popupModalLoader').addClass('hide');
		
		$(this).removeClass('hide');

		// eliminamos el evento
		$('#' + popupId).find('.popupModalContent iframe').unbind('load');
	});*/
	
	//LibPopupModal._ajaxLoad(popupId, $('#' + popupId).attr('data-url-current'));
	
	LibPopupModal._ajaxLoad(popupId, $('#' + popupId).attr('data-url'), true);

	//document.getElementById(popupId + '_iframe').contentDocument.location.reload();
};

LibPopupModal.maximize = function(popupId)
{
	$('#' + popupId).css('width', $(window).width() + 'px');
	//$('#' + popupId).find('iframe').css('height', ($(window).height() - 130) + 'px');
	
	//var left = ($(window).width() - $('#' + popupId).width()) / 2;
	//$('#' + popupId).css('left', left + 'px');
	
	var heightModal = $(window).height();

	$('#' + popupId + '_wrapper_aux').css({
		width  : '100%',
		height : (heightModal - 60) + 'px'
	});
	
	$('#' + popupId).css({
		top    : '0px',
		left   : '0px', 
		right  : '0px',
		height : (heightModal + 10) + 'px'
	});
	
	$('#' + popupId).resizable({disabled : true}).draggable({disabled : true});

	// cambia los botones
	$('#' + popupId).find('.popupModalHeaderBtn_maximize').addClass('hide');
	$('#' + popupId).find('.popupModalHeaderBtn_restore').removeClass('hide');
	
	//$('#' + popupId).attr('data-height', heightModal);
};

LibPopupModal.minimize = function(popupId, title)
{
	var style = $('#' + popupId).attr('style');
	
	/*var options = {
			to: {
				width: 0,
				height: 0
			},
			origin : [
				'bottom',
				'left'
			]
		};
	
	$('#' + popupId).effect('size', options, 500, function()
		{
			$('#' + popupId).addClass('hide');
			$('#' + popupId).attr('style', style);
		});*/
	$('#' + popupId).addClass('hide');
	$('#' + popupId).attr('style', style);
	//$('#' + popupId).addClass('hide');
	
	// a_ade boton de popup
	//$('#popupsButtons').append('<div id="popupBtn_' + popupId + '" class="popupButtonFixed"><a href="#" onclick="LibPopupModal.restore(this, \'' + popupId + '\'); return false;" title="">' + title.replace(/_/g, ' ') + ' <img src="imagenes/popupModal/window_restore.png" alt="" /></a></div>');
};

LibPopupModal.restore = function(element, popupId, heightDefault, widthDefault)
{
	// restaurar desde minimizado
	if(element != null)
	{
		//$(element).remove();
		$('#' + popupId).removeClass('hide');
	}	
	// restarurar desde maximizado
	else
	{
		$('#' + popupId).css('width', widthDefault + 'px');
		//$('#' + popupId).find('iframe').css('height', heightDefault + 'px');
		
		var left = ($(window).width() - $('#' + popupId).width()) / 2;
		$('#' + popupId).css('left', left + 'px');
	
		$('#' + popupId).css({
			top    : '100px',
			height : heightDefault + 'px'
		});

		$('#' + popupId).resizable({disabled : false}).draggable({disabled : false});
		
		$('#' + popupId + '_wrapper_aux').css({
			width  : '100%',
			height : (heightDefault - 60) + 'px'
		});
		
		// cambia los botones
		$('#' + popupId).find('.popupModalHeaderBtn_restore').addClass('hide');
		$('#' + popupId).find('.popupModalHeaderBtn_maximize').removeClass('hide');
	}
	
	LibPopupModal.indexTop(popupId);
};

LibPopupModal.close = function(popupId)
{
	$('#' + popupId).unbind('draggable').unbind('resizable');
	$('#' + popupId + '_iframe a').die('click');
	$('#' + popupId).find('form').die('submit');
	$('#' + popupId + '_form_iframe').unbind('load');
			
	LibPopupModal.closeLogAccess(popupId);
	//$('#' + popupId).find('iframe').attr('src', '');
	$('#' + popupId).remove();
	
	$('#popupsButtons').find('#popupBtn_' + popupId).remove();
};

LibPopupModal.closeAll = function()
{
	$('.popupModal').each(function(index)
	{
		LibPopupModal.closeLogAccess($(this).attr('id'));
	});
	
	$('.popupModal').remove();
	$('.popupButtonFixed').remove();
};

LibPopupModal.sendLogAccess = function(popupId)
{
	if($('#' + popupId).find('.popupModalModuleName').size() == 1)
	{
		var module = $('#' + popupId).find('.popupModalModuleName').html();
		
		$.ajax({
			url : 'index.php?log_acceso=get&m=' + module
		}).success(function(data)
		{
			$('#' + popupId).attr('data-id-log-access', data);
		});
	}
};

LibPopupModal.closeLogAccess = function(popupId)
{
	$.ajax({
		url : 'index.php?idAcceso=' + $('#' + popupId).attr('data-id-log-access'),
		async : false
	});
};

LibPopupModal.indexTop = function(popupId)
{
	$('.popupModal').css('z-index', 20);
	$('#' + popupId).css('z-index', parseInt($('#' + popupId).css('z-index')) + 1);
};

// al hacer click en una ventana modal (menos iframe)
$('.popupModal').live('click', function()
{
	$('.popupModal').css('z-index', 20);
	$(this).css('z-index', parseInt($(this).css('z-index')) + 1);
});

// al hacer unload en la web
window.onbeforeunload = function()
{
	LibPopupModal.closeAll();
};