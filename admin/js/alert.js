function alertMsg(key, text)
{
	var mensaje = translateString(key, text);

	if($('#mensajeInformacion').size() > 0)
	{
		$('#mensajeInformacion').remove();
	}
	
	var html = '<div id="mensajeInformacion" class="hide">' + mensaje + '</div>';
	$('body').append(html);
	
	var left = ($(window).width() - $('#mensajeInformacion').width()) / 2;
	$('#mensajeInformacion').css('left', left + 'px');
	
	$('#mensajeInformacion').removeClass('hide');
	
	$('#mensajeInformacion').bind('click', function(){
		alertMsgClose();
	});
	
	setTimeout('alertMsgClose()', 5000);
}
function alertMsgClose()
{
	$('#mensajeInformacion').unbind('click');
	$('#mensajeInformacion').slideUp(function(){
		$(this).remove();
	});
}

/* */

function alertConfirm(element, msg, url)
{
	if(confirm(msg))
	{
		/*var id = $(element).attr('id');
		if(id == undefined)
		{
			var newDate = new Date;
			id = 'alertConfimLink_' + newDate.getTime();
		}*/
		
		//url += '?url-referer='  + encodeURIComponent($(element).parents('.popupModal').attr('data-url'));
		//alert(url);
		if($(element).attr('href') != undefined)
		{
			window.location.href = url;
		}
		else
		{
			var objDate = new Date();
			var idAnchor = 'alertConfirmAuxButton_' + objDate.getTime();
			$(element).parent().append('<a class="hide" href="' + $(element).attr('data-url') + '" id="' + idAnchor + '">a</a>');

			setTimeout(function(){
				$('#' + idAnchor).click();
			}, 50);
		}
		
		//window.location.href = url;
		
		return false;
	}
}

function alertConfirmBtn(msg, idBtn)
{
	if(idBtn == undefined || idBtn == null)
	{
		idBtn = 'frmBtnBorrar';
	}
	
	$('#' + idBtn).click(function(e)
	{
		if(!confirm(msg))
		{
			e.preventDefault();
		}
	});
}