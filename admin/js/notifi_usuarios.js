function http(){
    if(typeof window.XMLHttpRequest!='undefined'){
        return new XMLHttpRequest();
    }else{
        try{
            return new ActiveXObject('Microsoft.XMLHTTP');
        }catch(e){
            alert('Su navegador no soporta AJAX');
            return false;
        }
    }
}

function notifi_user_send()
{
	if($('.listado_cursos').size() == 0)
	{
		var H=new http();
		    if(!H)return;
		    H.open('post','notificaciones/usuarios', true);
		    H.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		    H.onreadystatechange=function(){
		        if(H.readyState==4){
		        	notifi_user_callback(H.responseText);
		            H.onreadystatechange=function(){};
		            H.abort();
		            H=null;
		        }
		    }
		    H.send();
	}
	else
	{
		setTimeout(function(){
			notifi_user_send();
		}, 2000);
	}
}

function notifi_user_callback(data)
{
	try
	{
		if(data != undefined && data != null && data != "")
		{
			if($('#notifiUsers').size() == 0) 
			{
				$('body').append(
					'<div id="notifiUsers" class="hide">' + 
						'<div class="fleft">' + data + '</div>' +
						'<div class="fright"><a href="#" onclick="notifi_user_close(); return false;" title="Cerrar notificaci&oacute;n">X</a></div></div>' +
						'<div class=""clear>' +
					'</div>'
				);
			}
			else
			{
				$('#notifiUsers').addClass('hide');
				$('#notifiUsers').find('.fleft').html(data);
			}
			
			$('#notifiUsers').slideDown();;
			
			// posicionamos el mensaje emergente
			$('#notifiUsers').css('left', (($(window).width() - $('#notifiUsers').width()) / 2));
			
			setTimeout(function(){
				notifi_user_close();
			}, 4000);
		}
	}
	catch(e)
	{
		
	}
	
	setTimeout(function(){
		notifi_user_send();
	}, 10000);
}

// cargamos los datos de los usuarios coenctados actualemente como datos de referencia para saber quien se conecta y desconecta
function notifi_user_start()
{
	if($('.listado_cursos').size() == 0)
	{
		$.ajax({
			url : 'notificaciones/usuarios',
			dataType : 'html',
			type : 'GET',
			// si carga los datos satisfactoriamente pasamos a comprobar cada cierto tiempo si alguien entra o sale de algun curso
			success : function()
			{
				setTimeout(function(){
					notifi_user_send();
				}, 1000);
			},
			// si da error intenta volver a realizar la misma operacion
			error : function()
			{
				setTimeout(function(){
					notifi_user_start();
				}, 2000);
			}
		});
	}
	else
	{
		setTimeout(function(){
			notifi_user_start();
		}, 2000);
	}
}

function notifi_user_close()
{
	$('#notifiUsers').slideUp();
}

$(document).ready(function()
{
	notifi_user_start();
});