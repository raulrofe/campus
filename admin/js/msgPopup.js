function msgPopupOpen(json, num)
{
	var title = 'Tiene un mensaje nuevo';
	var content = '';
	var addClass = '';
	for(var cont = 0; cont < num; cont++)
	{
		if(cont > 0)
		{
			addClass = 'hide';
		}
		
		var css_negrita = '';
		if(json[cont]['negrita_letra'] == true)
		{
			css_negrita = 'negrita';
		}
		
		content +=
			'<div id="msgPopup_' + (cont + 1) + '" class="msgPopupContentDinamic ' + addClass + '">' +
				'<div>' +
					'<div class="fleft"><img src="imagenes/mensajes_emergentes/' + json[cont]['url_icono'] + '" alt="" /></div>' +
					'<div class="elementoNombre fright"><p><strong>Escrito por:</strong>' + json[cont]['autor'] + '</p></div>' +
					'<div class="elementoFecha fleft">' + json[cont]['fecha'] + '</div>' +
					'<div class="elementoContenido fright ' + css_negrita + ' " style="font-size:' + json[cont]['tamano_letra'] + 'px; font-family:' + json[cont] ['fuente_letra'] + ';color:#' + json[cont]['color_letra'] + ';"><p>' + json[cont]['mensaje'] + '</div>' +
				'</div>' +
				'<div class="clear"></div>' +
			'</div>';
	}
	
	content += '<div class="clear"></div>';
	
	if(num > 1)
	{
		title = 'Tiene mensajes nuevos';
		content +=
			'<div id="msgPopupButtons">' +
				'<a id="msgPopupButtonPrev" class="hide" href="#" onclick="msgPopupPrev(' + num + ');return false;" title="Anterior mensaje">Anterior</a>' +
				'<a id="msgPopupButtonNext" href="#" onclick="msgPopupNext(' + num + ');return false;" title="Siguiente mensaje">Siguiente</a>' +
				'<span id="msgPopupPage" class="hide">1</span>' +
			'</div>';
	}
	
	var html =
	'<div id="messagePopup">' +
		'<div id="messagePopupAll">' +
			'<div id="messagePopupHeader">' +
				'<h2>' + title + '</h2>' +
				'<a href="#" onclick="msgPopupClose(); return false;" title="">X</a>' +
			'</div>' +
			'<div id="messagePopupContent">' + content + '</div>' +
			'<div id="messagePopupNav"></div>' +
		'</div>' +
	'</div>';
	
	$('body').append(html);
	
	// POSICIONAMOS
	//var top = ($(window).height() - $('#messagePopup').height()) / 2;
	//$('#messagePopup').css('top', top + 'px');
	
	var left = ($(window).width() - $('#messagePopup').width()) / 2;
	$('#messagePopup').css('left', left + 'px');
	
	$('#messagePopup').draggable({
		handle : '#messagePopupHeader'
	});
}

function msgPopupClose()
{
	$('#messagePopup').remove();
}

function msgPopupPrev(maxNum)
{
	var num = parseInt($('#msgPopupPage').html()) - 1;
	
	$('.msgPopupContentDinamic').addClass('hide');
	$('#msgPopup_' + num).removeClass('hide');
	
	$('#msgPopupPage').html(num);
	
	msgPopupButtonsControl(num, maxNum);
}

function msgPopupNext(maxNum)
{
	var num = parseInt($('#msgPopupPage').html()) + 1;
	
	$('.msgPopupContentDinamic').addClass('hide');
	$('#msgPopup_' + num).removeClass('hide');
	
	$('#msgPopupPage').html(num);	
	
	msgPopupButtonsControl(num, maxNum);
}

function msgPopupButtonsControl(num, maxNum)
{
	// MOSTRAR O NO LOS BOTONES DE NAVEGACIOON ENTRE MENSAJES
	if(num <= 1 || num > maxNum)
	{
		$('#msgPopupButtonPrev').addClass('hide');
	}
	else
	{
		$('#msgPopupButtonPrev').removeClass('hide');
	}
	
	if(num >= maxNum)
	{
		$('#msgPopupButtonNext').addClass('hide');
	}
	else
	{
		$('#msgPopupButtonNext').removeClass('hide');
	}
}