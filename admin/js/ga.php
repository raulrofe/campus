<?php
header("Content-type: text/javascript");

require_once '../config.php';
?>

<?php
if(defined('GA_ID'))
{
	$gaId = constant('GA_ID');
	if(!empty($gaId))
	{
?>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-<?php echo GA_ID?>']);
		_gaq.push(['_trackPageview']);
		
		(function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
<?php
		}
 	}
 ?>