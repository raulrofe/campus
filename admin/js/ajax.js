//window.idCurso = null;

function cambiarHash(hash)
{
	if($('#idLogAcceso_menu').size() == 1)
	{
		$.ajax({
			url : 'index.php?idAcceso=' + $('#idLogAcceso_menu').html(),
			async: false
		});
	}
	
	var hashActual = obtenerHash();
	
	if(hash == hashActual)
	{
		cargarPagina();
	}
	else
	{
		window.location.hash = '/' + hash;
	}
}

function cargarPagina()
{
	$('#msgCargaAjax').removeClass('hide');
	
	var hash = obtenerHash();
	
	// cierra los popups en el listado de cursos
	if(hash == '')
	{
		for(var i in window.popup_obj)
		{
			window.popup_obj[i].close();
		}
	}
	
	var method = 'GET';
	//var data = '';
	/*if(window.idCurso != null && isNaN(window.idCurso))
	{
		method = 'POST';
		data = 'idCurso=' + window.idCurso;
	}*/
	
	$.ajax({
		url : 'ajax/' + hash,
		dataType : 'html',
		type : method,
		//data: data,
		success : function(data)
		{
			$('#general').html(data);
		},
		complete : function()
		{
			/*if(hash.match(/^hall\/([0-9]+)/))
			{
				cambiarHash('hall');
			}*/
			
			$('#msgCargaAjax').addClass('hide');
		}
	});
}

function cargarActualHash()
{
	var hash = obtenerHash();
	
	cambiarHash(hash);
}

function obtenerHash()
{
	var hash = window.location.hash.replace(/^#\//, '');
	
	return hash;
}

$(document).ready(function()
{
	eventHash();
});

function eventHash()
{
	if($('#usuario_no_logueado').size() == 0)
	{
		$(window).hashchange(function()
		{
			cargarPagina();
		});
		
		cargarActualHash();
	}
}
