<?php
define('I_EXEC', true);

require_once 'lib/fichero.php';

header("Content-type: text/css");

require_once 'config.php';

$css = array(
	'estilo',
	'msgPopup',
	'elemento',
	'jquery_ui',
	'chat',
	'popupModal',
	'tipsy',
	'editor',
	'countdown',
	'karmicgraphs',
	'fancybox',
	//'ckeditor'
);

if(isset($_GET['admin']) && $_GET['admin'] == 1)
{
	$path = PATH_ROOT_ADMIN;
}
else
{
	$path = PATH_ROOT;
}

$code = null;

if(isset($_GET['m'], $_GET['c']) && preg_match('/^([0-9A-Za-z_\-]+)$/', $_GET['m']) && preg_match('/^([0-9A-Za-z_\-]+)$/', $_GET['c']) )
{
	switch($_GET['t'])
	{
		case 'imprimir':
			$code = file_get_contents(PATH_ROOT . 'css/estilo.css');
			$code = file_get_contents(PATH_ROOT . 'css/imprimir.css');
			$code .= file_get_contents(PATH_ROOT . 'css/imprimir_ocultar_elementos.css');
			break;
		case 'ie7':
			$code = file_get_contents(PATH_ROOT . 'css/ie7.css');
			break;
		default:
			break;
	}
}
else
{
	foreach($css as $item)
	{
		$code .= file_get_contents(PATH_ROOT . 'css/' . $item . '.css');
	}

	$dir = Fichero::obtenerDirectorios($path . 'modulos/');
	foreach($dir as $item)
	{
		$files = Fichero::obtenerFicheros($item . 'css/');
		foreach($files as $file)
		{
			$code .= file_get_contents($file);
		}
	}
}

echo $code;