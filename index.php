<?php
define('I_EXEC', true);
define('IS_ADMIN', false);

ob_start();
session_name("campus");
session_start();
//$_SESSION['perfil'];



setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
date_default_timezone_set('Europe/Madrid');

require_once 'config.php';
require_once 'framework.php';

Log::start();

// para que no accede aqui el admin
if(Usuario::is_login() && Usuario::compareProfile('administrador'))
{
	Url::redirect('admin/');
}

$db = new database();
$con = $db->conectar();

// actualizamos la fecha de ultima conexion del usuario logueado
if(Usuario::is_login() && isset($_SESSION['idcurso']))
{
	$objUsuario = new Usuario();
	$objUsuario->actualizarFechaConexion(Usuario::getIdUser(), Usuario::getIdCurso(), Usuario::getProfile());

	if(!isset($_SESSION['idsesionTime']) && Usuario::compareProfile('alumno')){
		$_SESSION['idsesionTime'] = $objUsuario->fehcaInicioSesion(Usuario::getIdMatriculaDatos(Usuario::getIdUser(), Usuario::getIdCurso()));
	}
}

		// if($_SESSION['idusuario'] == 3976){
		// 	print_r('<pre>');
		// 	print_r($_SESSION);
		// 	print_r('</pre>');
		// 	print_R('<br>');
		// 	exit;
		// }

$get = Peticion::obtenerGet();

// LOG DE ACCESO DE FECHA DE SALIDA
if(isset($_GET['idAcceso']))
{
	ignore_user_abort(true);

	if(is_numeric($get['idAcceso']))
	{
		$objLogAcceso = LogAcceso::obtenerInstancia();
		$objLogAcceso->cerrarlog($get['idAcceso']);
	}
	die();
}

// CREAR LOG DE ACCESO PARA POPUPS
$idCurso = Usuario::getIdCurso();
if(isset($get['log_acceso']) && isset($idCurso))
{
	$objLogAcceso = LogAcceso::obtenerInstancia();
	$objLogAcceso->insertarLog();

	echo LogAcceso::obtenerIdAcceso();
	die();
}

//CIERRO SESSION CON FECHA
if(isset($get['closeIdSession']) && isset($idCurso) && isset($_SESSION['idsesionTime'])){
	$objLogAcceso = LogAcceso::obtenerInstancia();
	$objLogAcceso->cerrarSession($_SESSION['idsesionTime']);
}

// WEB SIN HTML BASE
//$get = Peticion::obtenerGet();
if(isset($get['t']) && $get['t'] == 'simple')
{
	//header("Content-type: text/plain");

	$ruta = mvc::obtenerRutaControlador(false);
	if(isset($ruta))
	{
		mvc::importar($ruta);
	}
	else
	{
		if($get['m'] == 'acceso' && $get['c'] == 'index')
		{
			Url::redirect('salir');
		}
	}
}
// AJAX MENU
else if(isset($get['v']))
{
	require_once 'plantillas/ajax.php';
}
// MENU
//else if(!isset($get['m']) || (isset($get['m']) && ($get['m'] == 'acceso' || $get['m'] == 'menu' || $get['m'] == 'cursos')))
else if(!isset($get['m']))
{
	Url::redirect('');
}
else if(isset($get['m'], $get['c']) && $get['m'] == 'acceso' && $get['c'] == 'index' && Usuario::is_login())
{
	session_unset();
	session_destroy();
	Url::redirect('');
}
else if(isset($get['m']) && ($get['m'] == 'acceso' || $get['m'] == 'menu' || $get['m'] == 'cursos'))
{
	require_once 'plantillas/menu.php';
}
// POPUP
else
{
	//header("Content-type: text/plain");

	require_once 'plantillas/popup.php';
}

mvc::cargarUnloader();

ob_end_flush();