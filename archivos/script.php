<?php
	$servername = 'localhost';
	$username = 'root';
	$password = '';
	$database = 'u75088563_campusaulainteractivainfantil';

	$servername = 'localhost';
	$username = 'user_u75088563';
	$password = 'PBD_u75088563';
	$database = 'u75088563_campusaulainteractivainfantil';

	// Create connection
	$mysqli = new mysqli($servername, $username, $password, $database);

	// Check connection
	if ($mysqli->connect_error) {
		die("Connection failed: " . $mysqli->connect_error);
	}


	// Relaci�n entre el ID original y el nuevo
	$idmatriculas_relacion = array(
		'4281' => '4586',
		'4280' => '4589',
		'4317' => '4584',
		'4295' => '4580',
		'4257' => '4600',
		'4258' => '4599',
		'4274' => '4590',
		'4283' => '4582',
		'4284' => '4585',
		'4287' => '4604',
		'4289' => '4587',
		'4290' => '4605',
		'4291' => '4606',
		'4292' => '4602',
		'4293' => '4591',
		'4294' => '4596',
		'4299' => '4579',
		'4300' => '4592',
		'4301' => '4598',
		'4304' => '4601',
		'4308' => '4588',
		'4319' => '4581',
		'4320' => '4607',
		'4321' => '4597',
		'4322' => '4594',
		'4323' => '4593',
		'4324' => '4595',
		'4325' => '4583',
		'4326' => '4578',
		'4327' => '4603'
	);

	// Lista de IDs originales
	$idmatriculas = array_keys($idmatriculas_relacion);

	//print_r($idmatriculas);

	// Tiempos
	$diaInicio = 5;
	$duracion = 23;
	$fechaActual = date('Y-m-d');
	/*
	$sql = '';
	if ($filas = select($mysqli, 'SELECT * FROM calificaciones2 WHERE idmatricula IN (' . implode(',', $idmatriculas) . ')')) {
		foreach ($filas as $fila) {
			$sql.= 'UPDATE calificaciones SET `fecha` = \'' . $fila['fecha'] . '\', `idmatricula` = ' . $fila['idmatricula'] . ' WHERE `idcalificaciones` = ' . $fila['idcalificaciones'] . ';' . PHP_EOL;
		}
	}
	echo $sql;
	exit;*/

	// Obtenemos la relaci�n de d�as
	$nombreFicheroDias = 'dias.txt';
	$dias = array();

	// Existe el fichero de d�as
	if (is_file($nombreFicheroDias)) {
		// Obtenemos la relaci�n de d�as
		$dias = json_decode(file_get_contents($nombreFicheroDias), true);
	} else {
		foreach($idmatriculas as $idmatricula){
			if ($filas = select($mysqli, 'SELECT DISTINCT DATE(fecha_entrada) as dia FROM logs_acceso WHERE idmatricula=' . $idmatricula)) {
				$dias[$idmatricula] = generarDias($filas, $diaInicio, $duracion);
			}
		}
		// Guardamos la relaci�n de d�as en un fichero
		file_put_contents($nombreFicheroDias, json_encode($dias));
		// echo json_encode($dias) . PHP_EOL . PHP_EOL;
	}

	// print_r($dias);

	// Obtenemos las �ltimas fechas procesadas de cada usuario
	$nombreFicheroUltimas = 'fechasUltimas.txt';
	$fechaUltimaUsuario = array();

	// Existe el fichero de fechas �ltimas
	if (is_file($nombreFicheroUltimas)) {
		// Obtenemos las fechas �ltimas
		$fechaUltimaUsuario = json_decode(file_get_contents($nombreFicheroUltimas), true);
	}
	// print_r($fechaUltimaUsuario);

	// Buscamos la fecha l�mite de cada usuario
	$fechaLimiteUsuario = array();
	// Por cada usuario
	foreach ($dias as $idmatricula => $fechas) {
		// Inicializamos la fecha l�mite
		$fechaLimiteUsuario[$idmatricula] = reset($fechas);

		// Recorremos las fechas
		foreach ($fechas as $diaOrigen => $diaDestino) {
			if ($diaDestino > $fechaActual)
				break;
			$fechaLimiteUsuario[$idmatricula] = $diaOrigen;
		}
	}
	//print_r($fechaLimiteUsuario);
	// return;exit('moinintendo');
	// Recorremos los usuarios
	foreach($idmatriculas as $idmatricula){
		$fechaUltima = (isset($fechaUltimaUsuario[$idmatricula])? $fechaUltimaUsuario[$idmatricula]: null);

		// Tabla "log_tiempo_sesion"
		if ($sesiones = select($mysqli, 'SELECT * FROM log_tiempo_sesion WHERE idmatricula=' . $idmatricula . (is_null($fechaUltima)? '': ' AND DATE(inicio_sesion) > \'' . $fechaUltima . '\'') . ' AND DATE(inicio_sesion) <= \'' . $fechaLimiteUsuario[$idmatricula] . '\'')) {
			$registrosTransformados = transformarRegistrosSesion($sesiones, $idmatriculas_relacion, $dias[$idmatricula]);
			$sql = getSQLUpdate('log_tiempo_sesion', array('idmatricula', 'inicio_sesion', 'fin_sesion'), $registrosTransformados);
			$mysqli->query($sql);
		}

		// Tabla "logs_acceso"
		if ($accesos = select($mysqli, 'SELECT * FROM logs_acceso WHERE idmatricula=' . $idmatricula . (is_null($fechaUltima)? '': ' AND DATE(fecha_entrada) > \'' . $fechaUltima . '\'') . ' AND DATE(fecha_entrada) <= \'' . $fechaLimiteUsuario[$idmatricula] . '\'')) {
			$registrosTransformados = transformarRegistrosAcceso($accesos, $idmatriculas_relacion, $dias[$idmatricula]);
			$sql = getSQLUpdate('logs_acceso', array('idlugar', 'fecha_entrada', 'fecha_salida', 'idmatricula'), $registrosTransformados);
			$mysqli->query($sql);
		}

		// Tabla "calificaciones"
		if ($calificaciones = select($mysqli, 'SELECT *, DATE(fecha) AS dia, TIME(fecha) AS hora FROM calificaciones WHERE idmatricula=' . $idmatricula . (is_null($fechaUltima)? '': ' AND DATE(fecha) > \'' . $fechaUltima . '\'') . ' AND DATE(fecha) <= \'' . $fechaLimiteUsuario[$idmatricula] . '\'')) {
			foreach ($calificaciones as $calificacion) {
				if (isset($dias[$idmatricula][$calificacion['dia']])) {
					$mysqli->query('UPDATE calificaciones SET `fecha` = \'' . $dias[$idmatricula][$calificacion['dia']] . ' ' . $calificacion['hora'] . '\', `idmatricula` = ' . $idmatriculas_relacion[$idmatricula] . ' WHERE `idcalificaciones` = ' . $calificacion['idcalificaciones']);
				}
			}
		}

		// Guardamos la �ltima fecha procesada para el usuario
		$fechaUltimaUsuario[$idmatricula] = $fechaLimiteUsuario[$idmatricula];
	}

	$mysqli->close();

	// Guardamos las fechas �ltimas
	file_put_contents($nombreFicheroUltimas, json_encode($fechaUltimaUsuario));
	// echo json_encode($nombreFicheroUltimas) . PHP_EOL . PHP_EOL;


	function select($mysqli, $consulta) {
		$filas = array();

		if ($resultado = $mysqli->query($consulta)) {
			while ($fila = $resultado->fetch_assoc()) {
				$filas[] = $fila;
			}
		}

		return $filas;
	}

	function generarDias($fechas, $diaInicio, $duracion) {
		$resultado = array();

		$diaFin = $diaInicio + $duracion;
		$cantidad = count($fechas);
		// Hay m�s d�as a generar que disponibles
		if ($cantidad > $duracion)
			$cantidad = $duracion;

		$dias = array();
		for ($i=0; $i<$cantidad; $i++) {
			do {
				$aleatorio = rand($diaInicio, $diaFin);
				// Evitamos los d�as no deseados
				if ($aleatorio >= 24)
					$aleatorio++;
				if ($aleatorio >= 25)
					$aleatorio++;
			} while(in_array($aleatorio, $dias));

			$dias[] = $aleatorio;
		}
		// Ordenamos
		sort($dias);

		// Asignamos los d�as
		foreach ($fechas as $indice => $fecha) {
			$resultado[$fecha['dia']] = '2016-12-' . str_pad(isset($dias[$indice])? $dias[$indice]: $dias[(count($dias)-1)], 2, '0', STR_PAD_LEFT);
		}

		return $resultado;
	}

	function transformarRegistrosSesion($sesiones, $idmatriculas_relacion, $relacionDias) {
		for ($i = 0, $limit = count($sesiones); $i < $limit; $i++) {
			unset($sesiones[$i]['id']);
			$sesiones[$i]['idmatricula'] = $idmatriculas_relacion[$sesiones[$i]['idmatricula']];
			$sesiones[$i]['inicio_sesion'] = setDate($sesiones[$i]['inicio_sesion'], $relacionDias);
			$sesiones[$i]['fin_sesion'] = setDate($sesiones[$i]['fin_sesion'], $relacionDias);
		}
		return $sesiones;
	}

	function transformarRegistrosAcceso($sesiones, $idmatriculas_relacion, $relacionDias) {
		for ($i = 0, $limit = count($sesiones); $i < $limit; $i++) {
			unset($sesiones[$i]['idaccesos']);
			$sesiones[$i]['fecha_entrada'] = setDate($sesiones[$i]['fecha_entrada'], $relacionDias);
			$sesiones[$i]['fecha_salida'] = setDate($sesiones[$i]['fecha_salida'], $relacionDias);
			$sesiones[$i]['idmatricula'] = $idmatriculas_relacion[$sesiones[$i]['idmatricula']];
		}
		return $sesiones;
	}

	function setDate($tiempo, $relacionDias) {
		$fecha = date("Y-m-d", strtotime($tiempo));
		if (!empty($relacionDias[$fecha])) {
			$hora = date("H:i:s", strtotime($tiempo));
			$tiempo = $relacionDias[$fecha] . ' ' . $hora;
		} else {
			$tiempo = '0000-00-00 00:00:00';
		}
		return $tiempo;
	}

	function getSQLUpdate($table, $fields, $registrosTransformados) {
		$sql = "INSERT INTO " . $table . " (" . implode(', ', $fields) . ") VALUES";
		foreach ($registrosTransformados as $registro) {
			$sql.= " ( ";
			$sqlFields = '';
			for ($i = 0, $limit = count($fields); $i < $limit; $i++) {
				$sqlFields.= "'" . $registro[$fields[$i]] . "',";
			}
			$sql .= rtrim($sqlFields, ",");
			$sql.= "),";
		}
		return rtrim($sql, ",");
	}
?>