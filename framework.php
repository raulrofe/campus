<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

function __autoload($class)
{
	$file = PATH_ROOT . 'lib/' . strtolower($class) . '.php';
	
	if(is_file($file))
	{
		require_once $file;
	}
}

$libs = array('log_acceso');
foreach($libs as $lib)
{
	$path = PATH_ROOT . 'lib/' . $lib . '/';
	$fileBase = $path . $lib . '.php';
	$fileModelo = $path . 'modelo.php';
	
	require_once $fileBase;
	require_once $fileModelo;
}
	
if(Usuario::is_login())
{
	$libs = array('mensajes_emergentes');
	foreach($libs as $lib)
	{
		$path = PATH_ROOT . 'lib/' . $lib . '/';
		$fileBase = $path . $lib . '.php';
		$fileModelo = $path . 'modelo.php';
		
		require_once $fileBase;
		require_once $fileModelo;
	}
}