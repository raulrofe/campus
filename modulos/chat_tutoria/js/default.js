LibChatTutoria = {};

LibChatTutoria.http = function(){
    if(typeof window.XMLHttpRequest!='undefined'){
        return new XMLHttpRequest();
    }else{
        try{
            return new ActiveXObject('Microsoft.XMLHTTP');
        }catch(e){
            alert('Su navegador no soporta AJAX');
            return false;
        }
    }
};

LibChatTutoria.chatComprobarSiEstaAbierto = function(H)
{
	if($('#popupModal_chat_tutoria').size() == 0)
	{
	    H.abort();
	    H=null;
	}
	else
	{
		setTimeout(function(){
			LibChatTutoria.chatComprobarSiEstaAbierto(H);
		}, 50);
	}
};

LibChatTutoria.chat_send = function(idLastMsg, ini, callback)
{
	var H=new http();
	    if(!H)return;
	    H.open('post','aula/tutorias/chat/mensajes/obtener/' + idLastMsg + '/' + ini + '/' + window.chat_finTutoriaCerca + '/' + window.idusuario, true);
	    H.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	    H.onreadystatechange=function(){
	        if(H.readyState==4){
	            if(callback)
	                callback(H.responseText);
	            H.onreadystatechange=function(){};
	            H.abort();
	            H=null;
	        };
	    };
	    H.send();

		setTimeout(function(){
			LibChatTutoria.chatComprobarSiEstaAbierto(H);
		}, 50);

		/*$('#chat_tutoria').parents('.popupModal').find('.popupModalHeaderIconReload').unbind('click');
		$('#chat_tutoria').parents('.popupModal').find('.popupModalHeaderIconReload').click(function()
		{
			H.abort();
			H = null;
		});*/
};

window.chat_tutoria_id_last_msg = 0;
window.chat_finTutoriaCerca = 0;
window.idusuario = 0;

/*
if($('#chat_idusuario_objetivo').size() == 1)
{
	window.idusuario = $('#chat_idusuario_objetivo').val();
}
*/

LibChatTutoria.obtenerNuevosMensajes = function(json)
{
	try
	{
		json = jQuery.parseJSON(json);
		if(json != null)
		{
			if(json.finTutoria != undefined && json.finTutoria == 1)
			{
				$('#chat_tutoria').append(
					'<div class="chat_tutoria_disabled"></div>' +
					'<div class="chat_tutoria_disabled_content">' +
						'<div class="negrita">La tutor&iacute;a ha finalizado</div>' +
					'</div>'
				);

				$('.chat_tutoria_privado').each(function()
				{
					$(this).parent().append(
						'<div class="chat_tutoria_disabled"></div>' +
						'<div class="chat_tutoria_disabled_content">' +
							'<div class="negrita">La tutor&iacute;a ha finalizado</div>' +
						'</div>'
					);
				});
			}
			else
			{
				if(json.finTutoriaCerca != undefined)
				{
					$('#chat_tutoria_mensajes').append('<div class="elemento"><i>La tutoria finalizara en 1 minuto</i></div>');
					$('#chat_tutoria_mensajes').scrollTop($('#chat_tutoria_mensajes').prop('scrollHeight'));

					window.chat_finTutoriaCerca = 1;
				}

				if(json.msg != undefined)
				{
					for(var i in json.msg)
					{
						var msgStyle = '';
						if(json.msg[i].idUsuarioDest != null && json.msg[i].nombrecDest)
						{
							msgStyle = ' a ' + json.msg[i].nombrecDest;
						}

						$('#chat_tutoria_mensajes').append('<div class="elemento"><strong>' + json.msg[i].nombre + msgStyle + ':</strong> ' + json.msg[i].contenido + '</div>');
					}
				}

				var idUsuarioSelected = $('#frm_chat_tutoria_destinatarios select').find('option:selected').val();
				$('#frm_chat_tutoria_destinatarios select').html('<option value="">Todos/as</option>');

				$('#chat_tutoria_usuarios').html('');
				if(json.tutor != undefined)
				{
					$('#chat_tutoria_usuarios').append('<h2>Tutores/as</h2>');
					for(var i in json.tutor)
					{
						// selectbox de usuarios para mensajes privados // json.idusuario_completo
						if(json.esAlumno)
						{
							if(json.tutor[i].idusuario != json.idUsuarioLogin)
							{
								var selectedActive = '';
								if(json.tutor[i].idusuario == idUsuarioSelected)
								{
									selectedActive = 'selected="selected"';
								}

								$('#frm_chat_tutoria_destinatarios select').append('<option ' + selectedActive + ' value="' + json.tutor[i].idusuario + '">' + json.tutor[i].nombre + '</option>');
							}
						}


						// solo muestra los enlaces del chat privado a los alumnos
						if(json.esAlumno)
						{
							$('#chat_tutoria_usuarios').append('<div id="chat_tutoria_tutor_t_' + json.tutor[i].idusuario + '"><div></div><a href="#" onclick="popup_open(\'Tutor&iacute;a con ' + json.tutor[i].nombre + '\', \'chat_tutoria_privado_' + json.tutor[i].idusuario + '\', \'aula/tutorias/chat/' + json.tutor[i].idusuario + '\', 400, 600); return false;" title="">' +
									'<img src="imagenes/mini_circle_green.png" alt="" width="7px" /> <span>' + json.tutor[i].nombre + '</span></a></div>');
						}
						else
						{
							$('#chat_tutoria_usuarios').append('<div><img src="imagenes/mini_circle_green.png" alt="" width="7px" /> <span>' + json.tutor[i].nombre + '</span></div>');
						}
					}
				}
				if(json.usuario != undefined)
				{
					$('#chat_tutoria_usuarios').append('<br /><h2>Alumnos/as</h2>');

					var idCursoParaTitulo = 0;
					for(var i in json.usuario)
					{
						if(!json.esAlumno)
						{
							// selectbox de usuarios para mensajes privados // json.idusuario_completo
							if(json.usuario[i].idusuario != json.idUsuarioLogin)
							{
								var selectedActive = '';
								if(json.usuario[i].idusuario == idUsuarioSelected)
								{
									selectedActive = 'selected="selected"';
								}

								$('#frm_chat_tutoria_destinatarios select').append('<option ' + selectedActive + ' value="' + json.usuario[i].idusuario + '">' + json.usuario[i].nombre + '</option>');
							}
						}

						//$('#frm_chat_tutoria_destinatarios select').append('<option ' + selectedActive + ' value="' + json.usuario[i].idusuario + '">' + json.usuario[i].nombre + '</option>');

						// no muestra la cabecera del titulo del curso a los alumnos
						if(!json.esAlumno)
						{
							if(idCursoParaTitulo != json.usuario[i].idcurso)
							{
								$('#chat_tutoria_usuarios').append('<h3>' + json.usuario[i].titulocurso + '</h3>');

								idCursoParaTitulo = json.usuario[i].idcurso;
							}
						}

						// solo muestra los enlaces del chat privado a los tutores
						if(json.esAlumno)
						{
							$('#chat_tutoria_usuarios').append('<div><img src="imagenes/mini_circle_green.png" alt="" width="7px" /> <span>' + json.usuario[i].nombre + '</span></div>');
						}
						else
						{
							$('#chat_tutoria_usuarios').append('<div id="chat_tutoria_alumno_' + json.usuario[i].idusuario + '"><div></div><a href="#" onclick="popup_open(\'Tutor&iacute;a con ' + json.usuario[i].nombre + '\', \'chat_tutoria_privado_' + json.usuario[i].idusuario + '\', \'aula/tutorias/chat/' + json.usuario[i].idusuario + '\', 400, 600); return false;" title="">' +
									'<img src="imagenes/mini_circle_green.png" alt="" width="7px" /> <span>' + json.usuario[i].nombre + '</span></a></div>');
						}
						idtutoria = json.usuario[i].idtutoria;
					}
				}

				if(json != undefined && json.idLastMsg != undefined)
				{
					idLastMsg = json.idLastMsg;
					window.chat_tutoria_id_last_msg = json.idLastMsg;
					$('#chat_tutoria_mensajes').scrollTop($('#chat_tutoria_mensajes').prop('scrollHeight'));
				}
				else
				{
					idLastMsg = window.chat_tutoria_id_last_msg;
				}

				if(json.popup != undefined)
				{
					for(var i in json.popup)
					{
						if(json.esAlumno && json.popup[i].creador == 't')
						{
							$('#chat_tutor_' + json.popup[i].idrrhh).addClass('chat_to_privado');
						}
						else if(!json.esAlumno && json.popup[i].creador == 'a')
						{
							$('#chat_alumno_' + json.popup[i].idalumno).addClass('chat_to_privado');
						}
					}
				}

				setTimeout(function(){
					LibChatTutoria.chat_send(idLastMsg, 0, LibChatTutoria.obtenerNuevosMensajes);
				},10);
			}
		}
	}
	catch(e)
	{

	}
};

LibChatTutoria.chat_enviar = function()
{
	// CKEDITOR.instances['chat_tutoria_input_msg'].updateElement();

	$.ajax({
		url : 'aula/tutorias/chat/mensaje/nuevo',
		type: 'POST',
		data : $('#frm_chat_tutoria').serialize()
	}).success(function(json)
	{
		// CKEDITOR.instances['chat_tutoria_input_msg'].setData('');
		// CKEDITOR.instances['chat_tutoria_input_msg'].updateElement();
	}).complete(function(){
		$('#popupModal_chat_tutoria .popupModalHeaderLoader').addClass('hide');
	});
};

$('#popupModal_chat_tutoria .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled').removeAttr('onclick').attr('onclick', 'return false');

LibChatTutoria.chat_send(0, 1, LibChatTutoria.obtenerNuevosMensajes, 0);