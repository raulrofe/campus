<div id="chat_tutoria">
	<div id="chat_tutoria_mensajes_contenedor">
		<div id="chat_tutoria_mensajes"></div>
	</div>
	<div id="chat_tutoria_usuarios_contenedor">
		<div id="chat_tutoria_usuarios"></div>
	</div>
	<div class="clear"></div>

	<form id="frm_chat_tutoria" name="frm_chat_tutoria"action="" method="post" onsubmit="LibChatTutoria.chat_enviar(); return false;">
		<ul>
			<li id="frm_chat_tutoria_destinatarios" class="t_right">
				<select name="idusuario_objetivo">
					<option value="">Todos/as</option>
				</select>
			</li>
			<li>
				<textarea id="chat_tutoria_input_msg" rows="1" cols="1" name="mensaje"></textarea>
			</li>
			<li><button type="submit">Enviar</button></li>
		</ul>
		<div class="clear"></div>
	</form>

	<div id="chat_horario">Tutor&iacute;a desde las <?php echo date('H:i', strtotime($tutoria->hora_inicio, true))?> hasta las <?php echo date('H:i', strtotime($tutoria->hora_fin, true))?></div>
</div>

<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "#chat_tutoria_input_msg",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
	});

	$('#popupModal_chat_tutoria .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');
</script>

<script type="text/javascript" src="js-chat_tutoria-default.js"></script>