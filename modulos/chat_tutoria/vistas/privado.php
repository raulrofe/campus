<div id="chat_tutoria_privado_<?php echo $get['idusuario'];?>" class="chat_tutoria_privado">
	<h2>Tutor&iacute;a privada con <?php echo Texto::textoPlano($nombreUsuario)?></h2>
	<div class="chat_tutoria_privado_mensajes"></div>
	<div class="clear"></div>
	
	<form id="frm_chat_tutoria_privado_<?php echo $get['idusuario'];?>" action="" method="post" onsubmit="LibChatTutoriaPrivado.chat_enviar('#chat_tutoria_privado_<?php echo $get['idusuario'];?>'); return false;">
		<ul>
			<li>
				<textarea id="chat_tutoria_privado_input_msg_<?php echo $get['idusuario']?>" class="chat_tutoria_privado_input_msg" cols="1" rows="1" name="mensaje"></textarea>
				<input class="chat_tutoria_privado_idusuario_objetivo" type="hidden" name="idusuario_objetivo" value="<?php echo $get['idusuario']?>" />
			</li>
			<li><button type="submit">Enviar</button></li>
		</ul>
		<div class="clear"></div>
	</form>
	
	<div class="chat_horario">Tutor&iacute;a desde las <?php echo date('H:i', strtotime($tutoria->hora_inicio, true))?> hasta las <?php echo date('H:i', strtotime($tutoria->hora_fin, true))?></div>
</div>

<script type="text/javascript" src="js-chat_tutoria-defaultPrivado.js"></script>

<script type="text/javascript">
	$('#popupModal_chat_tutoria_privado_<?php echo $get['idusuario']?> .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');

	LibChatTutoriaPrivado.chat_send(0, 1, LibChatTutoriaPrivado.obtenerNuevosMensajes, '#chat_tutoria_privado_<?php echo $get['idusuario'];?>');
	
	ckeditor_create('chat_tutoria_privado_input_msg_<?php echo $get['idusuario']?>', 60);
</script>