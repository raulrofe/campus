<?php
class ModeloChatTutoria extends modeloExtend
{
	public function nuevoMensaje($idusuario, $contenido, $idtutoria, $idusuario_objetivo)
	{		
		$sql = 'INSERT INTO chat_tutoria (idusuario, mensaje, fecha, idtutoria, idusuario_destinatario)' .
		' VALUES ("' . $idusuario . '", "' . $contenido . '", "' . date('Y-m-d H:i:s') . '", ' . $idtutoria . ',' .
	 	$idusuario_objetivo . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajes($idcurso, $idUsuarioLogin, $idUltimoMsg = null, $idUsuarioDest = null)
	{
		$addQuery = null;
		if(isset($idUltimoMsg))
		{
			$addQuery = ' AND ct.id > ' . $idUltimoMsg;
		}
		
		$addQuery2 = null;
		if(isset($idcurso))
		{
			$addQuery2 = ' AND tc.idcurso = ' . $idcurso;
		}
		
		$addQuery3 = null;
		if(isset($idUsuarioDest))
		{
			$addQuery3 = ' AND ((ct.idusuario = "' . $idUsuarioLogin . '" AND ct.idusuario_destinatario = "' . $idUsuarioDest . '")' .
			' OR (ct.idusuario = "' . $idUsuarioDest . '" AND ct.idusuario_destinatario = "' . $idUsuarioLogin . '"))';
		}
		else
		{
			$addQuery3 = ' AND (ct.idusuario_destinatario IS NULL' .
						' OR (ct.idusuario_destinatario IS NOT NULL AND (ct.idusuario = "' . $idUsuarioLogin . '"' .
						' OR ct.idusuario_destinatario = "' . $idUsuarioLogin . '")))';
		}
		
		/*$sql = 'SELECT c.id, c.mensaje, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec, rh.nombrec AS t_nombrec,' .
		' rh.nombrec AS t_nombrec, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec' .
		' FROM chat_tutoria AS c' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = c.idtutoria' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idtutoria = t.idtutoria' .
		' LEFT JOIN curso AS cr ON cr.idcurso = tc.idcurso' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = c.idusuario' .
		' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = c.idusuario' .
		' LEFT JOIN matricula AS m ON m.idcurso = cr.idcurso' .
		' WHERE (st.idcurso = cr.idcurso OR tc.idcurso = cr.idcurso) AND t.dia_semana = ' . date('N') . ' AND c.fecha >= "' . date('Y-m-d H:i:s', time() - 3600) . '"' . $addQuery . $addQuery2 .
		' GROUP BY c.id';*/
		$sql = 'SELECT ct.id, ct.mensaje, ct.idusuario_destinatario,' .
		' CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec, rh.nombrec AS t_nombrec,' .
		' CONCAT(al2.nombre, " ", al2.apellidos) AS a_nombrec2, rh2.nombrec AS t_nombrec2' .
		' FROM chat_tutoria AS ct' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = ct.idusuario' .
		' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = ct.idtutoria' .
		' LEFT JOIN tutoria_cursos  AS tc ON tc.idtutoria = t.idtutoria' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = ct.idusuario' .
		' LEFT JOIN matricula AS m ON m.idcurso = tc.idcurso' .
		
		' LEFT JOIN rrhh AS rh2 ON CONCAT("t_", rh2.idrrhh) = ct.idusuario_destinatario' .
		' LEFT JOIN alumnos AS al2 ON al2.idalumnos = ct.idusuario_destinatario' .
		
		' WHERE (st.idcurso = tc.idcurso OR m.idcurso = tc.idcurso) AND t.dia_semana = ' . date('N') . ' AND ct.fecha >= "' . date('Y-m-d H:i:s', time() - 3600) . '"' .
		$addQuery . $addQuery2 . $addQuery3 .
		' GROUP BY ct.id';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutoria($idrrhh = null, $idCurso = null)
	{
		$fecha = date('H:i:s');
		
		$addQuery1 = null;
		$addQuery1_2 = null;
		if(isset($idrrhh))
		{
			$addQuery1 = ' LEFT JOIN staff AS st ON st.idcurso = tc.idcurso';
			$addQuery1_2 = ' AND CONCAT("t_", st.idrrhh) = "' . $idrrhh . '"';
		}
		
		$addQuery2 = null;
		if(isset($idCurso))
		{
			$addQuery2 = ' AND tc.idcurso  = ' . $idCurso;
		}
		
		$sql = 'SELECT t.idtutoria, tc.idcurso, t.hora_inicio, t.hora_fin FROM tutoria AS t' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idtutoria = t.idtutoria' .
		$addQuery1 .
		' WHERE t.dia_semana = ' . date('N') . ' AND t.hora_inicio <= "' . $fecha . '" AND t.hora_fin >= "' . $fecha . '"' . $addQuery1_2 . $addQuery2 . ' GROUP BY t.idtutoria';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionAlumno($idUsuario, $idCurso, $fecha)
	{
		$sql = 'UPDATE matricula SET fecha_conexion_tutoria = "' . $fecha . '" WHERE idalumnos = ' . $idUsuario . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionTutor($idUsuario, $fecha)
	{
		$sql = 'UPDATE rrhh SET fecha_conexion = "' . $fecha . '" WHERE idrrhh = ' . $idUsuario;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnosConectados($idCurso = null)
	{
		$addQuery = null;
		if(isset($idCurso))
		{
			$addQuery = ' AND c.idcurso = ' . $idCurso;
		}
		
		$sql = 'SELECT m.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto, c.idcurso, t.idtutoria, c.titulo AS titulocurso' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' LEFT JOIN curso AS c ON c.idcurso = m.idcurso' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idcurso = c.idcurso' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = tc.idtutoria' .
		' WHERE t.dia_semana = ' . date('N') . $addQuery . ' AND m.fecha_conexion_tutoria >= "' . date('Y-m-d H:i:s', time()-15) . '"' .
		' GROUP BY c.idcurso, m.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutoresConectados($idCurso = null)
	{
		$addQuery = null;
		if(isset($idCurso))
		{
			$addQuery = ' AND c.idcurso = ' . $idCurso;
		}
		
		$sql = 'SELECT st.idrrhh, rh.nombrec, rh.foto, c.idcurso, t.idtutoria' .
		' FROM staff AS st' .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = st.idrrhh' .
		' LEFT JOIN curso AS c ON c.idcurso = st.idcurso' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idcurso = c.idcurso' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = tc.idtutoria' .
		' WHERE t.dia_semana = ' . date('N') . $addQuery . ' AND (st.status = "tutor1" OR st.status = "tutor2") AND rh.fecha_conexion  >= "' . date('Y-m-d H:i:s', time()-15) . '"' .
		' GROUP BY st.idrrhh';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnoPorId($idAlumno)
	{
		$sql = 'SELECT al.idalumnos As idusuario, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM alumnos AS al WHERE al.idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutorPorId($idrrhh, $idcurso)
	{
		$sql = 'SELECT rh.idrrhh AS idusuario, rh.nombrec' .
		' FROM rrhh AS rh' .
		' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
		' WHERE CONCAT("t_", rh.idrrhh) = "' . $idrrhh . '" AND st.idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	public function obtenerNumeroChatTutoriaPorAlumno($idUsuario, $idcurso)
	{
		$sql = "SELECT ct.id" .
		" FROM chat_tutoria as ct" .
		" LEFT JOIN tutoria_cursos as tc ON tc.idtutoria = ct.idtutoria" .
		" WHERE ct.idusuario = " . $idUsuario .
		" AND tc.idcurso = " . $idcurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
		
	}
}