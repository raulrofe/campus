<?php
$get = Peticion::obtenerGet();

$idCurso = null;
$idrrhh = null;
if(Usuario::compareProfile('alumno'))
{
	$idCurso = Usuario::getIdCurso();
}
else
{
	$idrrhh = Usuario::getIdUser(true);
}

$objModelo = new ModeloChatTutoria();
$tutoria = $objModelo->obtenerTutoria($idrrhh, $idCurso);
if($tutoria->num_rows > 0)
{
	$tutoria = $tutoria->fetch_object();
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');
}
else
{
	echo 'No hay tutoria';
}