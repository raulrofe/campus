<?php
$get = Peticion::obtenerGet();

if(isset($get['idusuario']))
{
	$objModelo = new ModeloChatTutoria();
	
	$idCurso = null;
	$idrrhh = null;
	$idalumno = null;
	if(Usuario::compareProfile('alumno'))
	{
		$idCurso = Usuario::getIdCurso();
		$idrrhh = $get['idusuario'];
		
		$usuario = $objModelo->obtenerTutorPorId($idrrhh, $idCurso);
		if($usuario->num_rows == 1)
		{
			$usuario = $usuario->fetch_object();
			$nombreUsuario = $usuario->nombrec . ' (tutor/a)';
		}
	}
	else
	{
		$idrrhh = Usuario::getIdUser(true);
		$idalumno = $get['idusuario'];
		
		$usuario = $objModelo->obtenerAlumnoPorId($idalumno);
		if($usuario->num_rows == 1)
		{
			$usuario = $usuario->fetch_object();
			$nombreUsuario = $usuario->nombrec;
		}
	}
	
	$tutoria = $objModelo->obtenerTutoria($idrrhh, $idCurso);
	if($tutoria->num_rows > 0 && isset($nombreUsuario))
	{
		$tutoria = $tutoria->fetch_object();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'privado');
	}
	else
	{
		echo 'No hay tutoria';
	}
}