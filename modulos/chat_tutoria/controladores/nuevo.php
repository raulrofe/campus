<?php
$post = Peticion::obtenerPost();
if(Peticion::isPost() && isset($post['mensaje']) && !empty($post['mensaje']))
{
	$idCurso = null;
	$idrrhh = null;
	if(Usuario::compareProfile('alumno'))
	{
		$idCurso = Usuario::getIdCurso();
	}
	else
	{
		$idrrhh = Usuario::getIdUser(true);
	}

	$objModelo = new ModeloChatTutoria();

	$tutoria = $objModelo->obtenerTutoria($idrrhh, $idCurso);
	if($tutoria->num_rows > 0)
	{
		$tutoria = $tutoria->fetch_object();

		$destinatario = null;

		$idusuario_objetivo = 'NULL';
		if(isset($post['idusuario_objetivo']) && preg_match('/^((t_)?([0-9]+))$/', $post['idusuario_objetivo']) && $post['destinatario'] != Usuario::getIdUser())
		{
			$idusuario_objetivo = '"' . $post['idusuario_objetivo'] . '"';
		}

		$objModelo->nuevoMensaje(Usuario::getIdUser(true), $post['mensaje'], $tutoria->idtutoria, $idusuario_objetivo);
	}
}