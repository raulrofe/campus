<?php
set_time_limit(60);
//ignore_user_abort(true);

$idUsuario = Usuario::getIdUser();
//$idCurso2 = Usuario::getIdCurso();

session_write_close();
header("Cache-control: private");
$vueltas = 0;

ob_end_flush();

$objModelo = new ModeloChatTutoria();

$idrrhh = null;
if(!Usuario::compareProfile('alumno'))
{
	$idrrhh = Usuario::getIdUser();
}

$idCurso = null;
$idrrhh = null;
		
if(Usuario::compareProfile('alumno'))
{
	$idCurso = Usuario::getIdCurso();
}
else
{
	$idrrhh = Usuario::getIdUser(true);
}

// alumnos conectados en un principio que se usa para la base para saber si se conectan nuevos usuarios
$alumnosConectados = $objModelo->obtenerAlumnosConectados($idCurso);
$numAlumnosConectados = $alumnosConectados->num_rows;
unset($alumnosConectados);

$tutoresConectados = $objModelo->obtenerTutoresConectados($idCurso);
$numTutoresConectados = $tutoresConectados->num_rows;
unset($tutoresConectados);

$json = array();

$get = Peticion::obtenerGet();
if(isset($get['idLastMsg'], $get['ini'], $get['fincercaactivado'], $get['idusuario']) && is_numeric($get['idLastMsg'])
	&& is_numeric($get['ini']) && is_numeric($get['fincercaactivado']) && preg_match('/^((t_)?([0-9]+))$/', $get['idusuario']))
{
	$json['idLastMsg'] = $get['idLastMsg'];
	
    while(1)
    {
    	echo "\n";
		@ob_flush();
		flush();
		
		// si se ha desconectado el usuario ...
    	if(connection_status() != 0)
        {
        	if($get['idusuario'] == 0)
			{
	        	if(Usuario::compareProfile('alumno'))
	        	{
	        		$objModelo->actualizarFechaConexionAlumno($idUsuario, $idCurso, (date('Y-m-d H:i:s') - 20));
	        	}
	        	else
	        	{
	        		$objModelo->actualizarFechaConexionTutor($idUsuario, (date('Y-m-d H:i:s') - 20));
	        	}
			}
        	exit;
        }
        
	    // evita que alguien sin acceso a la tutoria acceda a esta, ya sea por "fin de tutoria" o porque no tiene acceso
		$tutoria = $objModelo->obtenerTutoria($idrrhh, $idCurso);
		if($tutoria->num_rows == 0)
		{
			$json['finTutoria'] = 1;
			
			echo json_encode($json);
			@ob_flush();
			flush();
			exit;
		}
		
		// cuando queda 1 minuto para que termine la tutoria
		$tutoria = $tutoria->fetch_object();
		if($get['fincercaactivado'] == 0 && Fecha::restarHoras(date('H:i:s'), $tutoria->hora_fin) < '00:01:00')
		{
        	$json['finTutoriaCerca'] = 1;
		}
		
		if($get['idusuario'] == 0)
		{
        	// actualiza la fecha de conexion y se conoce si el usuario logueado en tutor o alumno
		    if(Usuario::compareProfile('alumno'))
			{
				$objModelo->actualizarFechaConexionAlumno($idUsuario, $idCurso, date('Y-m-d H:i:s'));
				
	    		$json['esAlumno'] = true;
			}
			else
			{
				$objModelo->actualizarFechaConexionTutor($idUsuario, date('Y-m-d H:i:s'));
				
				// para saber el perfil del usuario que envio esta peticion
	    		$json['esAlumno'] = false;
	    	}
		}
        
		// obtiene los alumnos conectados para todos los cursos
    	$alumnosConectados = $objModelo->obtenerAlumnosConectados($idCurso);	

    	// obtiene los alumnos conectados para todos los cursos
    	$tutoresConectados = $objModelo->obtenerTutoresConectados($idCurso);	
    	
    	/*if(Usuario::compareProfile('alumno'))
		{
			$idusuario_privado = 't_' . $get['idusuario'];
		}
		else
		{
			$idusuario_privado = $get['idusuario'];
		}*/
			
		// obtiene los mensajes del chat privado
		if($get['idusuario'] != '0') // el 0 debe ir entrecomillado obligatoriamente
		{
			$mensajes = $objModelo->obtenerMensajes($idCurso, Usuario::getIdUser(true), $get['idLastMsg'], $get['idusuario']);
		}
		// obtiene los mensajes del chat general
		else
		{
    		$mensajes = $objModelo->obtenerMensajes($idCurso, Usuario::getIdUser(true), $get['idLastMsg']);
    		
    		/*$mensajesPrivados = $objModelo->obtenerMensajesEnPrivadoNuevosPopups(Usuario::getIdUser(true), 0);
    		if($mensajesPrivados->num_rows > 0)
    		{
    			$json['popup'] = array();
    			
    			while($item = $mensajesPrivados->fetch_object())
    			{					
    				$json['popup'][] = array('idrrhh' => $item->idrrhh, 'idalumno' => $item->idalumno, 'creador' => $item->creador);
    			}
    		}*/
		}
		
		// guarda los mensajes en formato JSON para luego devolverlos
        if($mensajes->num_rows > 0)
        {
	        while($mensaje = $mensajes->fetch_object())
			{
				$chatMensajeStyle = '';
				
				if(isset($mensaje->t_nombrec))
				{
					$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
				}
				else
				{
					$nombrec = $mensaje->a_nombrec;
				}
				
				$mensajeFormateado = Texto::bbcode($mensaje->mensaje);
				
				if(isset($mensaje->color) && $mensaje->color != 'default')
				{
					$chatMensajeStyle .= 'color:' . $mensaje->color . ';';
				}
				
				if(isset($mensaje->tipo))
				{
					$chatTipos = explode(',', $mensaje->tipo);
					if(!empty($chatTipos) && is_array($chatTipos) && count($chatTipos) > 0)
					{
						foreach($chatTipos as $item)
						{
							if($item == 'italic')
							{
								$chatMensajeStyle .= 'font-style:italic;';
							}
							if($item == 'underline')
							{
								$chatMensajeStyle .= 'text-decoration:underline;';
							}
							if($item == 'bold')
							{
								$chatMensajeStyle .= 'font-weight:bold;';
							}
						}
					}
					$mensajeFormateado = '<span style="' . $chatMensajeStyle . '">' . $mensajeFormateado . '</span>';
				}
				
				$nombrecDest = null;
				
				// datos para cuando el mensaje es privado
				if(isset($mensaje->idusuario_destinatario))
				{
					$msgIdUsuarioDest = $mensaje->idusuario_destinatario;
					
					if(isset($mensaje->a_nombrec2))
					{
						$nombrecDest = $mensaje->a_nombrec2;
					}
					else if(isset($mensaje->t_nombrec2))
					{
						$nombrecDest = $mensaje->t_nombrec2;
					}
				}
				else
				{
					$msgIdUsuarioDest = null;
				}
				
				$json['msg'][] = array('nombre' => Texto::textoPlano($nombrec), 'contenido' => $mensajeFormateado,
										'idUsuarioDest' => $msgIdUsuarioDest, 'nombrecDest' => Texto::textoPlano($nombrecDest));
				$json['idLastMsg'] = $mensaje->id;
			}
        }
        
        if($mensajes->num_rows > 0 || $alumnosConectados->num_rows != $numAlumnosConectados || $tutoresConectados->num_rows != $numTutoresConectados
        	|| $get['ini'] == 1 || isset($json['finTutoriaCerca']))
        {
        	// guarda los tutores en JSON
        	if($tutoresConectados->num_rows > 0)
        	{
	         	while($tutorConectado = $tutoresConectados->fetch_object())
				{
					$json['tutor'][] = array('idusuario' => 't_' . $tutorConectado->idrrhh, 'nombre' => Texto::textoPlano($tutorConectado->nombrec),
											'foto' => URL_BASE . 'imagenes/fotos/' . $tutorConectado->foto);
				}
        	}
			
        	// guarda los alumnos en JSON
        	if($alumnosConectados->num_rows > 0)
	        {
	        	while($alumnoConectado = $alumnosConectados->fetch_object())
				{
					$json['usuario'][] = array('idtutoria' => $alumnoConectado->idtutoria, 'titulocurso' => $alumnoConectado->titulocurso,
											'idusuario' => $alumnoConectado->idalumnos, 'nombre' => Texto::textoPlano($alumnoConectado->nombrec),
											'foto' => URL_BASE . 'imagenes/fotos/' . $alumnoConectado->foto, 'idcurso' => $alumnoConectado->idcurso);
				}
	        }
	        
	        $json['idUsuarioLogin'] = Usuario::getIdUser(true);
	        
        	echo json_encode($json);
			@ob_flush();
			flush();
			exit;
        }

        set_time_limit($vueltas + 20);
        
        if($vueltas < 10)
        {
       		sleep(1);
        }
        else if($vueltas < 20)
        {
        	sleep(5);
        }
        else
        {
        	sleep(10);
        }
    }
}