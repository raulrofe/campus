<div id="tareas_pendientes">
	<div class='subtitle t_center'>
		<img src="imagenes/tareas pendientes/tasks.png"  style="vertical-align:middle;"/>
		<span data-translate-html="tareas_pendiente.titulo">
			TAREAS PENDIENTESa
		</span>
	</div>
	
	<br/>
	
	<div class="tareas_pendientes_bloque">
		<div class="subtitleModulo" data-translate-html="tareas_pendiente.avisos">
			Avisos destacados
		</div>
		<!-- <div class='subtitleModulo'><span>Avisos destacados</span></div> -->
		<div class="tareas_pendientes_bloque_content">
			<?php if($numCorreos > 0):?>
				<p class="avisoTareasPendientes">
					<img src="imagenes/tareas pendientes/mail.png" />
					<a href='#' onClick="parent.popup_open('Correos', 'gestor_correos', 'correos/bandeja_entrada', 750, 900); return false;" data-translate-html="tareas_pendiente.correo_sin_leer">
						Mensajes sin leer del correo
					</a>
					<span class="fright naviso"><?php echo $numCorreos?></span>
				</p>
			<?php endif;?>
			
			<?php if(isset($numeroTotalMensajesForo) && $numeroTotalMensajesForo > 0 && Usuario::compareProfile('tutor')):?>
				<p class="avisoTareasPendientes no-details" id="avisoTareasPendientesForo" details="0">
					<img src="imagenes/tareas pendientes/foro.png" />
					<a href='#' onclick="desgloseCursos('desgloseDudasGenerales', 'avisoTareasPendientesForo');return false" data-translate-html="tareas_pendiente.correo_sin_leer3">
						Mensajes sin leer del foro:
					</a>
					<span class="fright naviso"><?php echo $numeroTotalMensajesForo?></span>
				</p>
				<div id="desgloseDudasGenerales" class="desgloseTable hide">
					<?php for($i=0; $i<=count($mensajesDudasGenerales['tituloCurso'])-1; $i++):?>

						<?php if($mensajesDudasGenerales['numeroMensajes'][$i] > 0):?>

							<div class="rowMensajeForo">
								<div class="fleft">
									<?php echo $mensajesDudasGenerales['tituloCurso'][$i]?>
								</div>

								<div class="fright naviso">
									<?php echo $mensajesDudasGenerales['numeroMensajes'][$i]?>
								</div>
							</div>

							<div class="clear"></div>

						<?php endif; ?>

					<?php endfor; ?>

				</div>
			<?php endif;?>	
				
			<?php if(isset($hayTutorias) && $hayTutorias && Usuario::compareProfile('tutor')):?>
				<p class="avisoTareasPendientes no-details" id="avisoTareasPendientesTutorias" details="0">

					<img src="imagenes/tareas pendientes/tutoria.png" />

					<a href='#' onclick="desgloseCursos('desgloseTutorias', 'avisoTareasPendientesTutorias');return false" data-translate-html="tareascoordinador.tutorias">
						Hoy es d&iacute;a de tutor&iacute;as en los siguientes cursos:
					</a>
				</p>
				<div id="desgloseTutorias" class="desgloseTable hide">
					<?php for($i=0; $i<=count($tutoriaCurso['titulocurso'])-1; $i++):?>
						<div class="rowMensajeForo">
							<div class="fleft">
								<?php echo $tutoriaCurso['titulocurso'][$i]?>	
							</div>

							<div class="fright">
								<span data-translate-html="tareascoordinador.horario">
									Horario:
								</span> 
								<?php 
									echo Fecha::quitarSegundos($tutoriaCurso['horaInicio'][$i]) . ' - ' . Fecha::quitarSegundos($tutoriaCurso['horaFin'][$i]) 
								?>
							</div>
						</div>
						<div class="clear"></div>
					<?php endfor; ?>
				</div>				
			<?php endif;?>
			
			<?php if(isset($numRecomendacionesCoord) && $numRecomendacionesCoord > 0):?>
				<p class="avisoTareasPendientes">

					<img src="imagenes/tareas pendientes/recomendaciones.png" />

					<a href='#' onclick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;" data-translate-html="tareascoordinador.recomendaciones">
						Recomendaciones del coordinador:
					</a>

					<span class="fright naviso">
						<?php echo $numRecomendacionesCoord?>
					</span>
				</p>
			<?php endif;?>
			
			<?php if(isset($numRecomendacionesPers) && $numRecomendacionesPers > 0):?>
				<p class="avisoTareasPendientes">

					<img src="imagenes/tareas pendientes/recomendaciones.png" />

					<a href='#' onclick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;" data-translate-html="tareascoordinador.eventos">
						Eventos personales:
					</a>
					<span class="fright naviso"><?php echo $numRecomendacionesPers?></span>
				</p>
			<?php endif;?>
			
			<?php if(Usuario::compareProfile('tutor')):?>
				<?php if($numCorreos == 0 && $numeroTotalMensajesForo == 0 && $hayTutorias == 0 && $numRecomendacionesCoord == 0 && $numRecomendacionesPers == 0):?>
					<p class="avisoTareasPendientes" data-translate-html="tareascoordinador.no_avisos">
						No tienes avisos
					</p>
				<?php endif; ?>
			<?php elseif(Usuario::compareProfile('coordinador')):?>
				<?php if($numCorreos == 0 && $numRecomendacionesPers == 0):?>
					<p class="avisoTareasPendientes" data-translate-html="tareascoordinador.no_avisos">
						No tienes avisos
					</p>					
				<?php endif; ?>
			<?php endif; ?>
		
		</div>
		
		<div class="tareas_pendientes_bloque">
			<!-- <div class='subtitleModulo'><span>Tareas pendientes</span></div> -->
			<div class="subtitleModulo" data-translate-html="tareascoordinador.administracion">
				Administraci&oacute;n de aportaciones del alumno
			</div>
			<div class="tareas_pendientes_bloque_content">
				<?php if(isset($numeroTotalAportBiblioteca) && $numeroTotalAportBiblioteca > 0 && Usuario::compareProfile('coordinador')):?>
					<p class="avisoTareasPendientes no-details" id="avisoTareasPendientesBiblioteca" details="0">
						<img src="imagenes/tareas pendientes/trabajos_practicos.png" />
							<a id="biblioteca_admin" href='#' class="titulo_tareas_pendientes"
							onClick="desgloseCursos('desgloseBiblioteca', 'avisoTareasPendientesBiblioteca');return false;" data-translate-html="tareascoordinador.aportacionesb">
								Aportaciones pendientes en biblioteca
							</a>
						<span class="fright naviso"><?php echo $numeroTotalAportBiblioteca?></span>
					</p>
					<div id="desgloseBiblioteca" class="desgloseTable hide">
						<?php for($i=0; $i<=count($aportBiblioteca['tituloCurso'])-1; $i++):?>
							<div class="rowMensajeForo">
								<div class="fleft">
									<a href="#" onClick="popup_open('Administrar aportaciones', 'biblioteca_admin', 'biblioteca/admin', 570, 610); return false;">
										<?php echo $aportBiblioteca['tituloCurso'][$i]?>
									</a>
								</div>
								<div class="fright naviso"><?php echo $aportBiblioteca['numero'][$i]?></div>
							</div>
							<div class="clear"></div>
						<?php endfor; ?>
					</div>
				<?php endif;?>
				
				<?php if(isset($numeroTotalAportArchivador) && $numeroTotalAportArchivador > 0 && Usuario::compareProfile('tutor')):?>
					<p class="avisoTareasPendientes no-details" id="avisoTareasPendientesEnlacesInteres" details="0">
						<img src="imagenes/tareas pendientes/trabajos_practicos.png" />
							<a id="biblioteca_admin" href='#' class="titulo_tareas_pendientes"
							onClick="desgloseCursos('desgloseArchivador', 'avisoTareasPendientesEnlacesInteres');return false;" data-translate-html="tareascoordinador.aportacionese">
								Aportaciones pendientes en enlaces de inter&eacute;s
							</a>
						<span class="fright naviso"><?php echo $numeroTotalAportArchivador?></span>
					</p>
					<div id="desgloseArchivador" class="desgloseTable hide">
						<?php for($i=0; $i<=count($aportArchivador['tituloCurso'])-1; $i++):?>
							<div class="rowMensajeForo">
								<div class="fleft">
									<a href="#" onClick="popup_open('Archivador electr&oacute;nico', 'archivador_electronico', 'aula/archivador_electronico/administrar', 550, 800); return false;">
										<?php echo $aportArchivador['tituloCurso'][$i]?>
									</a>
								</div>
								<div class="fright naviso"><?php echo $aportArchivador['numero'][$i]?></div>
							</div>
							<div class="clear"></div>
						<?php endfor; ?>
					</div>
				<?php endif;?>
				
				<?php if(isset($numeroTotalAportAnunciador) && $numeroTotalAportAnunciador > 0 && Usuario::compareProfile('coordinador')):?>
					<p class="avisoTareasPendientes no-details" id="avisoTareasPendientesAnuncios" details="0">
						<img src="imagenes/tareas pendientes/trabajos_practicos.png" />
							<a id="biblioteca_admin" href='#' class="titulo_tareas_pendientes"
							onClick="desgloseCursos('desgloseAnunciador', 'avisoTareasPendientesAnuncios')" data-translate-html="tareascoordinador.aportacionest">
								Aportaciones pendientes en el tabl&oacute;n de anuncios p&uacute;blico
							</a>
						<span class="fright naviso"><?php echo $numeroTotalAportAnunciador?></span>
					</p>
					<div id="desgloseAnunciador" class="desgloseTable hide">
						<?php for($i=0; $i<=count($aportAnunciador['tituloCurso'])-1; $i++):?>
							<div class="rowMensajeForo">
								<div class="fleft">
									<a href="#" onclick="popup_open('Tabl&oacute;n de anuncios', 'anuncios', 'tablon_anuncios/administrar', 600, 600); return false;" title="">
										<?php echo $aportAnunciador['tituloCurso'][$i]?>
									</a>
								</div>
								<div class="fright naviso"><?php echo $aportAnunciador['numero'][$i]?></div>
							</div>
							<div class="clear"></div>
						<?php endfor; ?>
					</div>
				<?php endif;?>
				
				<?php if(Usuario::compareProfile('tutor')):?>
					<?php if($numeroTotalAportArchivador == 0):?>
						<p class="avisoTareasPendientes" data-translate-html="tareascoordinador.no_aportacion">
							No hay aportaciones del alumno
						</p>
					<?php endif; ?>
				<?php elseif(Usuario::compareProfile('coordinador')):?>
					<?php if($numeroTotalAportAnunciador == 0 && $numeroTotalAportBiblioteca == 0):?>
						<p class="avisoTareasPendientes" data-translate-html="tareascoordinador.no_aportacion">
							No hay aportaciones del alumno
						</p>					
					<?php endif; ?>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-tareas_tutor-default.js"></script>