<?php

$idRRHH = Usuario::getIdUser(true);
$idCurso = Usuario::getIdCurso();

$objModelo = new ModeloTareasTutor();

$idconv = Usuario::getIdConvocatoria();
$idUsuario = Usuario::getIdUser();

$aportBiblioteca = array();
$aportArchivador = array();
$aportAnunciador = array();
$mensajesDudasGenerales = array();
$tutoriaCurso = array();

//********************************************** TAREAS PENDIENTES DEL COORDINADOR ***********************************************

if (Usuario::compareProfile('coordinador')) {
	$numeroTotalAportAnunciador = 0;
	$numeroTotalAportBiblioteca = 0;

	//Obtengo todo los curso a los que pertenece al coordinador
	$cursosCoordinador = $objModelo->cursosCoordinador($idconv, $idUsuario);
	if ($cursosCoordinador->num_rows > 0) {
		while ($cursoCoordinador = $cursosCoordinador->fetch_object()) {

			$contadorTablon = $objModelo->contadorAportacionesAnunicosHall($cursoCoordinador->idcurso);
			if ($contadorTablon > 0) {
				$aportAnunciador['tituloCurso'][] = $cursoCoordinador->titulo;
				$aportAnunciador['numero'][] = $contadorTablon;
			}

			$contadorBiblioteca = $objModelo->contadorAportacionesBiblioteca($cursoCoordinador->idcurso);
			if ($contadorBiblioteca > 0) {
				$aportBiblioteca['tituloCurso'][] = $cursoCoordinador->titulo;
				$aportBiblioteca['numero'][] = $contadorBiblioteca;
			}
		}


		// Recuento aportaciones anuncio hall
		$numeroTotalAportAnunciador = 0;
		foreach ($aportAnunciador['numero'] as $numeroAportacionesAnunciador) {
			$numeroTotalAportAnunciador = $numeroTotalAportAnunciador + $numeroAportacionesAnunciador;
		}

		// Recuento aportaciones biblioteca
		$numeroTotalAportBiblioteca = 0;
		foreach ($aportBiblioteca['numero'] as $numeroAportaciones) {
			$numeroTotalAportBiblioteca = $numeroTotalAportBiblioteca + $numeroAportaciones;
		}
	}
}


if (Usuario::compareProfile('tutor')) {
	mvc::cargarModuloSoporte('foro');
	$objForo = new modeloForo();

	$numeroTotalMensajesForo = 0;
	$hayTutorias = 0;
	$numRecomendacionesCoord = 0;

	//Obtengo todo los curso a los que pertenece el tutor
	$cursosTutor = $objModelo->cursosTutor($idconv, $idUsuario);
	if ($cursosTutor->num_rows > 0) {
		while ($curso = $cursosTutor->fetch_object()) {
			$numeroMensajesForoSinLeer = 0;

			$temasLogArray = array();
			$temasLog = $objForo->obtenerForoLog($curso->idcurso, $idRRHH);
			while ($row = $temasLog->fetch_object()) {
				$temasLogArray[$row->idforo_temas] = $row->fechaUltConexion;
			}

			$temas = $objForo->obtenerTemas($curso->idcurso);

			if ($temas->num_rows > 0) {
				while ($tema = $temas->fetch_object()) {
					$fechaLog = null;

					if (isset($temasLogArray[$tema->idforo_temas])) {
						$fechaLog = $temasLogArray[$tema->idforo_temas];
					}
					$numeroMensajesForoSinLeer += $objModelo->contadorMsgStaff($idRRHH, $curso->idcurso, $tema->idforo_temas, $fechaLog);
				}
			}

			$mensajesDudasGenerales['tituloCurso'][] = $curso->titulo;
			$mensajesDudasGenerales['numeroMensajes'][] = $numeroMensajesForoSinLeer;

			//Obtengo las tutorias de los cursos
			$tutorias = $objModelo->esDiaTutoria($curso->idcurso);
			if ($tutorias->num_rows > 0) {
				while ($tutoriaCursoActiva = $tutorias->fetch_object()) {
					$tutoriaCurso['titulocurso'][] = $curso->titulo;
					$tutoriaCurso['horaInicio'][] = $tutoriaCursoActiva->hora_inicio;
					$tutoriaCurso['horaFin'][] = $tutoriaCursoActiva->hora_fin;
				}
			}

			//Aportaciones enlaces de interes
			$contadorArchivador = $objModelo->contadorAportacionesArchivador($curso->idcurso);
			if ($contadorArchivador > 0) {
				$aportArchivador['tituloCurso'][] = $curso->titulo;
				$aportArchivador['numero'][] = $contadorArchivador;
			}
		}


		$numeroTotalMensajesForo = 0;
		$numeroTotalAportArchivador = 0;

		if (count($tutoriaCurso) > 0)
			$hayTutorias = true;
		else
			$hayTutorias = false;

		if (isset($mensajesDudasGenerales['numeroMensajes'])) {
			foreach ($mensajesDudasGenerales['numeroMensajes'] as $numMensajes) {
				$numeroTotalMensajesForo = $numeroTotalMensajesForo + $numMensajes;
			}
		}

		if (isset($aportArchivador['numero'])) {
			foreach ($aportArchivador['numero'] as $numeroAportacionesArchivador) {
				$numeroTotalAportArchivador = $numeroTotalAportArchivador + $numeroAportacionesArchivador;
			}
		}

		//obtener el numero de recomendacione pendientes por el coordinador
		$numRecomendacionesCoord = $objModelo->contadorEventosCoordinadorHoy($idCurso);
	}
}


//obtener el numero de eventos propios de la agenda
$numRecomendacionesPers = $objModelo->contadorEventosHoy($idCurso, Usuario::getIdUser(true));


// correos sin leer
$numCorreos = $objModelo->contadorMensajesSinLeerTutor();


require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');
