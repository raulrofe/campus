<?php

class ModeloTareasTutor extends modeloExtend {

	public function mensajesSinLeerTutor() {

		$idUsuario = Usuario::getIdUser(true);

		$sql = " SELECT *" .
			" FROM correos as C" .
			" LEFT JOIN destinatarios as D ON C.idcorreos = D.idcorreos" .
			" WHERE D.destinatarios = '" . $idUsuario . "'" .
			" AND D.carpeta = 0" .
			" AND D.leido = 0" .
			" AND D.destPapelera = 0" .
			" AND D.destSuprimir = 0";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function cursosTutor($idconv, $idUsuario) {
		$sql = "SELECT c.idcurso, s.idstaff, c.titulo" .
			" FROM curso as c" .
			" LEFT JOIN staff as s ON s.idcurso = c.idcurso" .
			" WHERE c.idconvocatoria = " . $idconv .
			" AND s.idrrhh = " . $idUsuario .
			" GROUP BY c.idcurso";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function cursosCoordinador($idconv, $idUsuario) {
		$sql = "SELECT c.idcurso, s.idstaff, c.titulo" .
			" FROM curso as c" .
			" LEFT JOIN staff as s ON s.idcurso = c.idcurso" .
			" WHERE c.idconvocatoria = " . $idconv .
			" AND s.idrrhh = " . $idUsuario .
			" GROUP BY c.idcurso";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajesSinLeer($idStaff) {
		$sql = "SELECT fm.idforo_mensaje" .
			" FROM foro_mensaje as fm" .
			" LEFT JOIN curso as c ON c.idcurso = fm.idcurso" .
			" LEFT JOIN staff as s ON s.idcurso = c.idcurso" .
			" WHERE s.idstaff = " . $idStaff .
			" AND fecha > (SELECT MAX(las.fecha_entrada)" .
			" FROM logs_acceso_staff as las" .
			" WHERE las.idlugar = 13" .
			" AND las.idstaff = " . $idStaff . ")";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function esDiaTutoria($idCurso) {
		$diaActual = date("N");

		$sql = "SELECT t.idtutoria, t.hora_inicio, t.hora_fin" .
			" FROM tutoria as t" .
			" LEFT JOIN tutoria_cursos as tc ON tc.idtutoria = t.idtutoria" .
			" WHERE t.dia_semana = " . $diaActual .
			" AND tc.idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function aportacionesBiblioteca($idCurso) {
		$sql = "SELECT id_aportacion" .
			" FROM biblioteca" .
			" WHERE estado = 2" .
			" AND idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function aportacionesArchivador($idCurso) {
		$sql = "SELECT idarchivador_electronico" .
			" FROM archivador_electronico_edicion" .
			" WHERE (moderado = 0)" .
			" AND idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function aportacionesAnunicosHall($idCurso) {
		$sql = "SELECT idanuncio" .
			" FROM anuncio_hall_admin" .
			" WHERE moderado = 0 AND idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerEventosCoordinadorHoy($idCurso) {
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT agc.idagenda_coordinador AS idagenda, agc.contenido, agc.fecha' .
			' FROM agenda_coordinador AS agc' .
			' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
			' WHERE agc.id_conv = cr.idconvocatoria AND agc.fecha = "' . $fechaIni . '"' .
			' GROUP BY agc.idagenda_coordinador ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerEventosHoy($idCurso, $idUsuario) {
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha' .
			' FROM agenda AS ag' .
			' WHERE ag.idcurso = ' . $idCurso . ' AND ag.idusuario = "' . $idUsuario . '" AND ag.fecha = "' . $fechaIni . '"' .
			' GROUP BY ag.idagenda ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//	CONTADORES
	public function contadorAportacionesAnunicosHall($idCurso) {
		$sql = "SELECT COUNT(*) as contador" .
			" FROM anuncio_hall_admin" .
			" WHERE moderado = 0 AND idcurso = " . $idCurso;

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

	public function contadorAportacionesBiblioteca($idCurso) {
		$sql = "SELECT COUNT(*) as contador" .
			" FROM biblioteca" .
			" WHERE estado = 2" .
			" AND idcurso = " . $idCurso;

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			if($resultado->contador>0){
			return $resultado->contador;
			}
		}
	}

	public function contadorMsgStaff($idusuario, $idcurso, $idtema, $fecha) {
		$addSql = '';
		if (isset($idusuario)) {
			$addSql = ' AND fm.idusuario != "' . $idusuario . '"';
		}

		$sql = 'SELECT COUNT(*) as contador' .
			' FROM foro_mensaje AS fm' .
			' LEFT JOIN staff AS st ON st.idrrhh = "' . $idusuario . '" AND st.idcurso = ' . $idcurso .
			' LEFT JOIN foro_temas AS ft ON fm.idforo_temas = ft.idforo_temas' .
			' WHERE fm.fecha > "' . $fecha . '"' .
			$addSql .
			' AND ft.idforo_temas = ' . $idtema .
			' AND fm.idcurso = ' . $idcurso .
			' GROUP BY fm.idforo_mensaje';

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

	public function contadorAportacionesArchivador($idCurso) {
		$sql = "SELECT COUNT(*) as contador" .
			" FROM archivador_electronico_edicion" .
			" WHERE moderado = 0" .
			" AND idcurso = " . $idCurso;

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

	public function contadorEventosCoordinadorHoy($idCurso) {
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT COUNT(*) as contador' .
			' FROM agenda_coordinador AS agc' .
			' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
			' WHERE agc.id_conv = cr.idconvocatoria AND agc.fecha = "' . $fechaIni . '"' .
			' GROUP BY agc.idagenda_coordinador ASC';

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

	public function contadorEventosHoy($idCurso, $idUsuario) {
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT COUNT(*) as contador' .
			' FROM agenda AS ag' .
			' WHERE ag.idcurso = ' . $idCurso . ' AND ag.idusuario = "' . $idUsuario . '" AND ag.fecha = "' . $fechaIni . '"' .
			' GROUP BY ag.idagenda ASC';

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

	public function contadorMensajesSinLeerTutor() {
		$idUsuario = Usuario::getIdUser(true);

		$sql = "SELECT COUNT(*) as contador" .
			" FROM correos as C" .
			" LEFT JOIN destinatarios as D ON C.idcorreos = D.idcorreos" .
			" WHERE D.destinatarios = '" . $idUsuario . "'" .
			" AND D.carpeta = 0" .
			" AND D.leido = 0" .
			" AND D.destPapelera = 0" .
			" AND D.destSuprimir = 0";

		$resultados = $this->consultaSql($sql);
		if ($resultado = $resultados->fetch_object()) {
			return $resultado->contador;
		}
	}

}
