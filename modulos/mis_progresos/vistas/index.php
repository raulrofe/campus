<div id="mis_progresos">
	<div>
		<div class="t_center">
			<img src="imagenes/mis_progresos/progress.png" alt="" style="vertical-align:middle;width:32px;height:32px;"/>
			<span data-translate-html="misprogresos.tituloM">
				MIS PROGRESOS
			</span>
		</div>
	</div>
	<br/>

	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>

		<div class="fleft panelTabMenu" id="misProgresos" style="z-index:600">

			<div id="nav1" class="blanco redondearBorde" onclick="changeTab(1)">
				<img src="imagenes/mis_progresos/scorm.png" alt="" />
				<p data-translate-html="misprogresos.scorm.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Contenidos SCORM
				</p>
			</div>

			<div id="nav2" class="redondearBorde" onclick="changeTab(2)">
				<img src="imagenes/mis_progresos/autoevaluaciones.png" alt="" />
				<p data-translate-html="misprogresos.autoevaluaciones.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Autoevaluaciones
				</p>
			</div>

			<div id="nav3" class="redondearBorde" onclick="changeTab(3)">
				<img src="imagenes/mis_progresos/trabajo.png" alt="" />
				<p data-translate-html="misprogresos.trabajos.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Trabajos pr&aacute;cticos
				</p>
			</div>

			<div id="nav4" class="redondearBorde" onclick="changeTab(4)">
				<img src="imagenes/mis_progresos/extra.png" alt="" />
				<p data-translate-html="misprogresos.extra.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Actividades extra
				</p>
			</div>	

			<div id="nav5" class="redondearBorde" onclick="changeTab(5)">
				<img src="imagenes/mis_progresos/grafica.png" alt="" />
				<p data-translate-html="misprogresos.grafica.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Gr&aacute;fica de evoluci&oacute;n
				</p>
			</div>		

			<div id="nav6" class="redondearBorde" onclick="changeTab(6)">
				<img src="imagenes/mis_progresos/notas-final.png" alt="" />
				<p data-translate-html="misprogresos.notafinal.titulo" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
					Nota final
				</p>
			</div>		

		</div>


		<!-- ************************************************************************************************************* -->
		<!-- CONTENIDO DE PESTANAS -->

		<div class='fleft panelTabContent' id="contenidoMisProgresos" style="z-index:300">

			<!-- TAB 1 : SCORM -->
			<div id="tab1">
				<p class="tituloTabs" data-translate-html="misprogresos.scorm.titulo">
					Contenido SCORM
				</p>
				<div>

					<div class="tablaPrgoresos">
						<div>
							<div class="fila1" data-translate-html="misprogresos.scorm.scorm">
								SCORM
							</div>
							<div class="fila2" data-translate-html="misprogresos.scorm.tiempo">
								Tiempo total
							</div>
							<div class="fila3" data-translate-html="misprogresos.scorm.estado">
								Estado
							</div>
						</div>
					</div>
					<div class="clear"></div>

					<div class="fila">
						<?php $numScorms = 0; ?>
						<?php foreach ($modulosArray as $modulo): ?>
							<?php $scormsModulo = $objModeloScorm->obtenerScormModulo($modulo['idmodulo']) ?>
							<?php if ($scormsModulo->num_rows > 0): ?>
								<?php while ($rowScorm = $scormsModulo->fetch_object()): ?>
									<?php if (file_exists(PATH_ROOT . 'archivos/scorm/' . $rowScorm->idscorm . '/sco_' . $rowScorm->idscorm_folder . '/default.html')): ?>
										<?php $numScorms++ ?>
										<?php $scorms = $objModelo->obtenerNotasScorm(Usuario::getIdUser(true), $idCurso, $modulo['idmodulo'], $rowScorm->idscorm); ?>
										<div class="fila">
											<div class="fila1"><?php echo Texto::textoPlano($rowScorm->titulo) ?></div>
											<?php if ($scorms->num_rows > 0): ?>
												<?php $rowScorm = $scorms->fetch_object(); ?>
												<?php $itemScorm = Scorm::obtenerArrayScorm($scorms); ?>
												<div class="fila2"><?php echo $itemScorm['tiempoTotal'] ?></div>
												<div class="fila3">
													<?php
													switch ($itemScorm['estado']) {
														case 'browsed':
															?>
															<span data-translate-html="misprogresos.iniciado">
																Iniciado
															</span>
															<?php
															break;
														case 'completado':
															?>
															<span data-translate-html="misprogresos.finalizado">
																Finalizado
															</span>
															<?php
															break;
														default:
															?>
															<span>-</span>
															<?php
															break;
													}
													?>
												</div>
											<?php else: ?>
												<div class="fila2">-</div>
												<div class="fila3">-</div>
											<?php endif; ?>
										</div>
										<div class="clear"></div>
									<?php endif ?>
								<?php endwhile ?>
							<?php endif ?>
						<?php endforeach; ?>

						<?php if ($numScorms == 0): ?>
							<br />
							<div class="t_center" data-translate-html="misprogresos.noscorm">
								No hay scorms en este curso
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<!-- TAB 2 : AUTOEVALUACIONES -->
			<div id="tab2" class="hide">
				<p class="tituloTabs" data-translate-html="misprogresos.autoevaluaciones.titulo">
					Autoevaluaciones
				</p>
				<div>
					<div class="tablaPrgoresos">
						<div>
							<div class="fila1" data-translate-html="misprogresos.autoevaluaciones.autoevaluacion">
								Autoevaluaci&oacute;n
							</div>
							<div class="fila3" data-translate-html="misprogresos.autoevaluaciones.nota">
								Nota
							</div>
							<div class="fila2" data-translate-html="misprogresos.autoevaluaciones.fecha">
								Fecha realizaci&oacute;n
							</div>
						</div>
					</div>
					<div class="clear"></div>

					<div>
						<?php foreach ($modulosArray as $modulo): ?>
							<?php $autoevalModulo = $objModeloAutoeval->autoeval_modulo($modulo['idmodulo']) ?>
							<?php if ($autoevalModulo->num_rows > 0): ?>
								<?php while ($rowAutoevaluacion = $autoevalModulo->fetch_object()): ?>


									<?php $autoeval = $objModelo->obtenerNotasTest($idUsuario, $idCurso, $modulo['idmodulo'], $rowAutoevaluacion->idautoeval); ?>

									<?php if ($autoeval->num_rows > 0): ?>
										<?php while ($row_autoeval = $autoeval->fetch_object()): ?>
											<div class="fila">
												<div class="fila1"><?php echo Texto::textoPlano($row_autoeval->titulo_autoeval) ?></div>
												<div class="fila3"><?php echo $row_autoeval->nota ?></div>
												<div class="fila2"><?php echo Fecha::obtenerFechaFormateada(date('d-m-Y, H:i', strtotime($row_autoeval->fecha))) ?></div>
											</div>
											<div class="clear"></div>

										<?php endwhile; ?>
									<?php else: ?>									
										<div class="fila">
											<div class="fila1"><?php echo Texto::textoPlano($rowAutoevaluacion->titulo_autoeval) ?></div>
											<div class="fila3">-</div>
											<div class="fila2">-</div>
										</div>
										<div class="clear"></div>	
									<?php endif; ?>
									<div class="clear"></div>



								<?php endwhile ?>
							<?php endif ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<!-- TAB 3 : TRABAJOS PRACTICOS -->
			<div id="tab3" class="hide">
				<p class="tituloTabs" data-translate-html="misprogresos.trabajos.titulo">
					Trabajos pr&aacute;cticos
				</p>
				<div>

					<div class="tablaPrgoresos">
						<div>
							<div class="fila1" data-translate-html="misprogresos.trabajos.modulo">
								M&oacute;dulos
							</div>
							<div class="fila2" data-translate-html="misprogresos.trabajos.fecha">
								Fecha de realizaci&oacute;n
							</div>
							<div class="fila3" data-translate-html="misprogresos.trabajos.nota">
								Nota
							</div>
						</div>
					</div>

					<div>
						<?php $numTrabajosPracticos = 0; ?>

						<?php foreach ($modulosArray as $modulo): ?>
							<?php $trabajosPracticos = $objModelo->obtenerNotasTrabajosPracticosActivos($idUsuario, $idCurso, $modulo['idmodulo']) ?>
							<?php $numTrabajosPracticos = $numTrabajosPracticos + $trabajosPracticos->num_rows; ?>
							<?php if ($trabajosPracticos->num_rows > 0): ?>
								<?php while ($trabajoPract = $trabajosPracticos->fetch_object()): ?>
									<div class="fila">
										<div class="fila1"><?php echo Texto::textoPlano($trabajoPract->titulo) ?></div>
										<?php if (isset($trabajoPract->nota_trabajo)): ?>
											<div class="fila2"><?php echo Fecha::obtenerFechaFormateada(date('d-m-Y', strtotime($trabajoPract->fecha)), false); ?></div>
											<div class="fila3"><?php echo $trabajoPract->nota_trabajo ?></div>
										<?php else: ?>
											<div class="fila2">-</div>
											<div class="fila3">-</div>
										<?php endif; ?>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endforeach; ?>

						<?php if ($numTrabajosPracticos == 0): ?>
							<div class="fila1" data-translate-html="misprogresos.trabajos.no_trabajo">
								No hay trabajos pr&aacute;cticos
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<!--  TAB 4 : ACTIVIDADES EXTRAS -->
			<div id="tab4" class="hide">
				<p class="tituloTabs" data-translate-html="misprogresos.extra.titulo">
					Actividades extra
				</p>
				<div>
					<div>
						<div class="tablaPrgoresos">
							<div>
								<div class="filaExtra1"></div>
								<div class="filaExtra2" data-translate-html="misprogresos.extra.foro">
									Foro
								</div>
								<?php if ($configNotas->extra1 != 0): ?>
									<div class="filaExtra2" data-translate-html="misprogresos.extra.extra1">
										Extra I
									</div>
								<?php endif; ?>
								<?php if ($configNotas->extra2 != 0): ?>
									<div class="filaExtra2" data-translate-html="misprogresos.extra.extra2">
										Extra II
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="fila">
						<div class="filaExtra1">
							<?php echo Texto::textoPlano($curso->titulo) ?>
							<span style="font-size: 0.8em;">
								(
								<span data-translate-html="misprogresos.inicio">
									Inicio:
								</span>
								<?php echo date('d-m-Y', strtotime($curso->f_inicio)) ?>
								-
								<span data-translate-html="misprogresos.fin">
									Fin:
								</span>
								<?php echo date('d-m-Y', strtotime($curso->f_fin)) ?>
								)
							</span>
						</div>
						<div class="filaExtra2"><?php echo $mediaForo ?></div>
						<?php if ($configNotas->extra1 != 0): ?>
							<div class="filaExtra2">-</div>
						<?php endif; ?>
						<?php if ($configNotas->extra2 != 0): ?>
							<div class="filaExtra2">-</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<!-- TAB 5 : GRAFICA DE EVOLUCION -->
			<div id="tab5" class="hide">

				<?php $style = ModeloMisProgresos::chartStyle(round($notaMediaFinal, '1')); ?>

				<p class="tituloTabs" data-translate-html="misprogresos.grafica.titulo">
					Gr&aacute;fica de evoluci&oacute;n
				</p>
				<div class="t_center">
					<h3 class="t_center" data-translate-html="misprogresos.grafica.media_curso">
						Nota media del curso
					</h3>
				</div>
				<div class="imgDegradadoNota"><img src="imagenes/mis_progresos/medidor-evolucion.jpg" alt="medidor evolucion"/></div>
				<div class="groupIndicadorNotaMedia">
					<div id="indicadorNotaMedia" style="margin-left:<?php echo $style['margin'] ?>px">
						<img src="imagenes/mis_progresos/arrow-nota-media.png" alt="" /><br/>
						<p data-translate-html="misprogresos.grafica.media">
							Nota Media
						</p>
						<p class="t_center">(<?php echo number_format($notaMediaFinal, '2', '.', ''); ?>)</p>
					</div>
				</div>
				<br/><br/><br/>


				<div id="graficoNotaMedia">
					<div class="verticalNotes">
						<p>10</p>
						<p>9</p>
						<p>8</p>
						<p>7</p>
						<p>6</p>
						<p>5</p>
						<p>4</p>
						<p>3</p>
						<p>2</p>
						<p>1</p>
						<p>0</p>					
					</div>
					<div class="fleft">
						<img src="imagenes/mis_progresos/arro-graphic.png" alt="" />
					</div>
					<div class="fleft">
						<div class="barraNote" style="background-color:<?php echo $style['background'] ?>;">
							<div class="barraNoteActive" style="height:<?php echo $style['size'] ?>px;"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="horizontalNotes">
						<img src="imagenes/mis_progresos/arrow-horizontal-graphic.png" alt="" />
					</div>
				</div>

				<br/><br/><br/>

				<div class="leyenda">
					<h3 class="t_center" data-translate-html="misprogresos.grafica.leyenda">
						Leyenda
					</h3>
					<br/>
					<div>
						<div class="notaLeyenda">10</div>
						<div class="notaLeyenda">9</div>
						<div class="notaLeyenda">8</div>
						<div class="notaLeyenda">7</div>
						<div class="notaLeyenda">6</div>
						<div class="notaLeyenda">5</div>
						<div class="notaLeyenda">4</div>
						<div class="notaLeyenda">3</div>
						<div class="notaLeyenda">2</div>
						<div class="notaLeyenda2">1</div>
						<div class="notaLeyenda2">0</div>
					</div>
					<div class="clear"></div>
					<div>
						<div class="colorLeyenda10"></div>
						<div class="colorLeyenda9"></div>
						<div class="colorLeyenda8"></div>
						<div class="colorLeyenda7"></div>
						<div class="colorLeyenda6"></div>
						<div class="colorLeyenda5"></div>
						<div class="colorLeyenda4"></div>
						<div class="colorLeyenda3"></div>
						<div class="colorLeyenda2"></div>
						<div class="colorLeyenda01"></div>		

					</div>
				</div>
				<!-- 
				<div id="graficaProgresos">
				<?php
				if (isset($chart)) {
					echo $chart;
				}
				?>
				</div>
								
				<div class="fright cursiva" style="font-size:0.8em;">
					<p><b>AutEv:</b> Media de autoevaluaciones</p>
					<p><b>TrabP:</b> Media de trabajos pr&aacute;cticos</p>
					<p><b>Extr:</b> Media de actividades extras</p>
					<p><b>Total:</b> Media total</p>
				</div>
				<div class="clear"></div>	
				-->

			</div>

			<!--  TAB 6 : NOTA FINAL -->
			<div id="tab6" class="hide">
				<p class="tituloTabs" data-translate-html="misprogresos.notafinal.titulo">
					Nota final
				</p>
				<div>
					<table class="tabla">
						<tr class='filaTabla'>
							<td class="t_center">
								<span data-translate-html="misprogresos.extra.foro">
									Foro
								</span>
								(<?php echo $configNotas->foro ?>%)
							</td>
							<td class="t_center">
								<span data-translate-html="misprogresos.trabajos.titulo">
									Trabajos pr&aacute;cticos 
								</span>
								(<?php echo $configNotas->trabajos_practicos ?>%)
							</td>
							<td class="t_center">
								<span data-translate-html="misprogresos.autoevaluaciones.autoevaluacion">
									Autoevaluaci&oacute;n 
								</span>
								(<?php echo $configNotas->autoevaluaciones ?>%)
							</td>
							<td class="t_center" data-translate-html="misprogresos.grafica.media">
								Nota media
							</td>
							<td class="t_center" data-translate-html="misprogresos.notafinal.completado">
								Completado
							</td>
						</tr>
						<tr>
							<td class="t_center"><?php echo number_format($foroPuntosReduce, 2, '.', ''); ?></td>
							<td class="t_center"><?php echo number_format($notaTPReduce, 2, '.', ''); ?></td>
							<td class="t_center"><?php echo number_format($mediaTestsReduce, 2, '.', ''); ?></td>
							<td class="t_center"><?php echo number_format($notaMediaFinal, 2, '.', ''); ?></td>
							<td class="t_center">
								<?php if (is_float($porcentajeTotal)): ?>
									<?php echo number_format($porcentajeTotal, '2', '.', ''); ?>
								<?php else: ?>
									<?php echo $porcentajeTotal; ?>
								<?php endif; ?>
								%</td>
						</tr>
					</table>
				</div>
			</div>

		</div>

		<!-- ************************************************************************************************************* -->
	</diV>
</div>
<script src="js-mis_progresos-default.js"></script>