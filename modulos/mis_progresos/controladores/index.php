<?php
mvc::cargarModuloSoporte('notas');
$objNotas = new Notas();
$objModeloNotas = new ModeloNotas();

$objModelo = new ModeloMisProgresos();

$idUsuario = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();

mvc::cargarModuloSoporte('scorm');
mvc::cargarModuloSoporte('autoevaluaciones');
mvc::cargarModuloSoporte('calificaciones');

$objTrabajos = new TrabajosPracticos();
$objModeloScorm = new ModeloScorm();
$objModeloAutoeval = new Autoevaluacion();

/***********************************************************************************************************************************
 * OBTENEMOS LOS MODULOS DEL CURSO
 **********************************************************************************************************************************/

$modulos = $objModeloNotas->obtenerModulosPorCurso($idCurso);
$modulosArray = array();
if($modulos->num_rows > 0)
{
	while($modulo = $modulos->fetch_object())
	{
		$modulosArray[] = array('idmodulo' => $modulo->idmodulo, 'nombre' => $modulo->nombre);
	}
}

/***********************************************************************************************************************************
 * OBTENEMOS LA CONFIGURACION DE LAS NOTAS
 **********************************************************************************************************************************/
$configNotas = $objModeloNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

/***********************************************************************************************************************************
 * OBTENEMOS LOS DATOS DEL CURSO
 **********************************************************************************************************************************/
$objModeloCurso = new Cursos();
$objModeloCurso->set_curso(Usuario::getIdCurso());
$curso = $objModeloCurso->buscar_curso();
$curso = $curso->fetch_object();


/***********************************************************************************************************************************
 * OBTENEMOS LOS NUMEROS DE TAREAS DE CADA GRUPOS DE TRABAJO (AUTOEVALUACIONES, TRABAJOS PRACTICOS Y FORO
 **********************************************************************************************************************************/

// Numero temas de foro
$numTemas = $objModeloNotas->obtenerNumTemas($idCurso);
$numTemas = $numTemas->num_rows;

// Numero autoevaluaciones
$numTest = $objModeloNotas->obtenerNumTestCurso($idCurso);
if($numTest->num_rows > 0)
{
	$numTest = $numTest->fetch_object();
	$numTest = $numTest->numTestCurso;
	$numAutoeval = $numTest;
}

// Numero trabajos practicos
$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($idCurso);
if($numTrabajos->num_rows > 0)
{
	$numTrabajos = $numTrabajos->fetch_object();
	$numTrabajos = $numTrabajos->numTrabajosPracticos;
}

/***********************************************************************************************************************************
 * OBTENEMOS LAS NOTAS MEDIAS DE CADA GRUPO DE TRABAJO (AUTOEVALUACIONES, TRABAJOS PRACTICOS Y FORO
 **********************************************************************************************************************************/

// Nota media foros
$foroPuntosReduce = $objNotas->calcularNotaMediaForo($idUsuario, $idCurso, true);
$foroPuntos = $objNotas->calcularNotaMediaForo($idUsuario, $idCurso, false);
$mediaForo = $foroPuntos;

// Nota media auotevaluaciones
$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($idUsuario, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
$mediaTest = $objNotas->calcularNotaMediaAutoeval($idUsuario, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);

// Nota media trabajos practicos
$objNotas->setCurso($idCurso);
$notaTPReduce = $objNotas->calcularMediaTrabajosPract($idUsuario, $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
$notaTP = $objNotas->calcularMediaTrabajosPract($idUsuario, $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);

/***********************************************************************************************************************************
 * OBTENEMOS LOS PORCETAJES DE SUPERACION
 **********************************************************************************************************************************/

// Valores de configuracion par obtener el porcentaje
$tareasObligatoriasCurso = $numTrabajos + $numAutoeval;
$valorTareaObligatoria = 100 / $tareasObligatoriasCurso;
$porcentajeTestTp = 0;
$porcentajeTotal = 0;

// Obtengo los test superados
$idMatricula = Usuario::getIdMatriculaDatos($idUsuario, $idCurso);
$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);
if($numTestAlumno->num_rows > 0)
{
	while($testParticipado = $numTestAlumno->fetch_object())
	{
		if($testParticipado->nota >= 5)
		{
			$porcentajeTestTp += $valorTareaObligatoria; 
		}
	}
}

// Obtengo los trabajos practicos superados
$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($idUsuario, $idCurso);

if($trabajosParticipado->num_rows > 0)
{
	while($tpParticipado = $trabajosParticipado->fetch_object())
	{
		if($tpParticipado->nota_trabajo >= 5)
		{
			$porcentajeTestTp += $valorTareaObligatoria;	
		}
	}
}

//Sumo los porcentajes de autoevaluaciones y trabajos practicos
$porcentajeTotal = $porcentajeTestTp;



/***********************************************************************************************************************************
 * OBTENGO LA NOTA DEL COORDINADOR SI LA TENGO PARA SUSTITUIRLO
 **********************************************************************************************************************************/
$notaCoordinadorAlumno = $objModeloNotas->obtenerUnAlumno($idUsuario, $idCurso);
if($notaCoordinadorAlumno->num_rows > 0)
{
	$notaCoordinadorAlumno = $notaCoordinadorAlumno->fetch_object();
	$nca = 	$notaCoordinadorAlumno->nota_coordinador;		
}

if(isset($nca) && !is_null($nca))
{
	$notaMediaFinal = $nca;	
}
else
{
	$notaMediaFinal = $foroPuntosReduce + $notaTPReduce + $mediaTestsReduce;	
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');