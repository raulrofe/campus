<?php
class ModeloMisProgresos extends modeloExtend
{
	/**
	 * Obtiene las notas de scorm para un alumno de un curso y un modulo especifico (puede devolver varios registros)
	 */
	public function obtenerNotasScorm($idalumno, $idcurso, $idmodulo, $idscorm)
	{
		$sql = 'SELECT * FROM scorm_alumnos AS sa' .
		' LEFT JOIN scormvars AS sv ON sv.SCOInstanceID = sa.SCOInstanceID' .
		' LEFT JOIN scorm AS sc ON sc.idscorm = sa.idscorm' .
		' WHERE sa.idusuario = "' . $idalumno . '"' .
		' AND sa.idcurso = ' . $idcurso . 
		' AND sc.idmodulo = ' . $idmodulo .
		' AND sa.idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene las notas de autoevaluacion para un alumno de un curso y un modulo especifico (puede devolver varios registros)
	 * @param unknown_type $idalumno
	 * @param unknown_type $idcurso
	 * @param unknown_type $idmodulo
	 */
	public function obtenerNotasAutoevaluacion($idalumno, $idcurso, $idmodulo)
	{
		$sql = 'SELECT ae.titulo_autoeval, c.fecha, c.nota' .
		' FROM autoeval AS ae' .
		' LEFT JOIN test AS t ON t.idautoeval = ae.idautoeval' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' LEFT JOIN calificaciones AS c ON c.idmatricula = m.idmatricula' .
		' WHERE t.idmodulo = ' . $idmodulo . 
		' AND ae.idautoeval = c.idautoeval ' .
		' GROUP BY c.idcalificaciones';
		$resultado = $this->consultaSql($sql);
		
		//echo $sql;exit;
		
		return $resultado;
	}
	
	public function obtenerNotasTest($idalumno, $idcurso, $idmodulo, $idtest)
	{
		$sql = 'SELECT ae.titulo_autoeval, c.fecha, c.nota ' .
		' FROM autoeval AS ae' .
		' LEFT JOIN test AS t ON t.idautoeval = ae.idautoeval' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' LEFT JOIN calificaciones AS c ON c.idmatricula = m.idmatricula' .
		' WHERE t.idmodulo = ' . $idmodulo . 
		' AND ae.idautoeval = c.idautoeval ' .
		' AND c.idautoeval = ' . $idtest .
		' GROUP BY c.idcalificaciones';
		$resultado = $this->consultaSql($sql);
		
		//echo $sql;
		
		return $resultado;
	}
	
	/**
	 * Obtiene las notas de trabajos practicos para un alumno de un curso y un modulo especifico (puede devolver varios registros)
	 * @param unknown_type $idalumno
	 * @param unknown_type $idcurso
	 * @param unknown_type $idmodulo
	 */
	public function obtenerNotasTrabajosPracticos($idalumno, $idcurso, $idmodulo)
	{
		$sql = 'SELECT * FROM archivo_temario AS at' .
		' LEFT JOIN calificacion_trabajo_practico AS ctp ON (at.idarchivo_temario = ctp.idarchivo_temario AND ctp.idalumnos = ' . $idalumno . ' AND ctp.idcurso = ' . $idcurso . ')' .
		' WHERE at.idcategoria_archivo = 2 AND at.idmodulo = ' . $idmodulo .
		' GROUP BY at.idarchivo_temario';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNotasTrabajosPracticosActivos($idalumno, $idcurso, $idmodulo)
	{
		$sql = 'SELECT * FROM archivo_temario AS at' .
		' LEFT JOIN calificacion_trabajo_practico AS ctp ON (at.idarchivo_temario = ctp.idarchivo_temario AND ctp.idalumnos = ' . $idalumno . ' AND ctp.idcurso = ' . $idcurso . ')' .
		' WHERE at.idcategoria_archivo = 2' .
		' AND at.idcurso = ' . $idcurso .
		' AND at.activo = 1' .
		' GROUP BY at.idarchivo_temario';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/**
	 * Obtiene las notas de estras (foro, extra 1 y extra 2) para un alumno de un curso y un modulo especifico (puede devolver varios registros)
	 * @param unknown_type $idalumno
	 * @param unknown_type $idcurso
	 */
	public function obtenerNotasExtras($idalumno, $idcurso, $idmodulo)
	{
		$sql = '';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public static function chartStyle($nota)
	{
		$confStyle = array();
			
		$puntuacion = $nota * 10;
		$tam = 143 - $puntuacion;
		
		if($nota == 0)
		{
			$confStyle['background'] =  '#FD0F02';
			$confStyle['size']		 = '143';
			$confStyle['margin']	= '0';
		}
		else if($nota > 0 && $nota < 1)
		{
			$confStyle['background'] =  '#FD0F02';
			$confStyle['size']		 = '137';
			$confStyle['margin']	= '0';			
		}
		else if($nota == 1)
		{
			$confStyle['background'] = '#FD0F02';
			$confStyle['size']			= '127';
			$confStyle['margin']	= '0';			
		}
		else if($nota > 1 && $nota < 2)
		{
			$confStyle['background'] = '#FD6D0D';
			$confStyle['size']			= '123';
			$confStyle['margin']	= '105';			
		}
		else if($nota == 2)
		{
			$confStyle['background'] = '#FD6D0D';
			$confStyle['size']			= '114';
			$confStyle['margin']	= '105';			
		}
		else if($nota > 2 && $nota < 3)
		{
			$confStyle['background'] = '#FD6D0D';
			$confStyle['size']			= '110';
			$confStyle['margin']	= '105';			
		}
		else if($nota == 3)
		{
			$confStyle['background'] = '#FD9F59';
			$confStyle['size']			= '99';
			$confStyle['margin']	= '164';
		}
		else if($nota > 3 && $nota < 4)
		{
			$confStyle['background'] = '#FD9F59';
			$confStyle['size']			= '96';
			$confStyle['margin']	= '164';			
		}
		else if($nota == 4)
		{
			$confStyle['background'] = '#FEC200';
			$confStyle['size']		= '85';
			$confStyle['margin']	= '216';			
		}
		else if($nota > 4 && $nota < 5)
		{
			$confStyle['background'] = '#FEC200';
			$confStyle['size']		= '81';
			$confStyle['margin']	= '216';			
		}
		else if($nota == 5)
		{
			$confStyle['background'] = '#FEF502';
			$confStyle['size']			= '71';
			$confStyle['margin']	= '276';		
		}
		else if($nota > 5 && $nota < 6)
		{
			$confStyle['background'] = '#FEF502';
			$confStyle['size']			= '67';
			$confStyle['margin']	= '276';			
		}
		else if($nota == 6)
		{
			$confStyle['background'] = '#20FF11';
			$confStyle['size']			= '57';
			$confStyle['margin']	= '328';
		}
		else if($nota > 6 && $nota < 7)
		{
			$confStyle['background'] = '#20FF11';
			$confStyle['size']			= '51';
			$confStyle['margin']	= '328';			
		}
		else if($nota == 7)
		{
			$confStyle['background'] = '#00FCFA';
			$confStyle['size']			= '43';
			$confStyle['margin']	= '380';			
		}
		else if($nota > 7 && $nota < 8)
		{
			$confStyle['background'] = '#00FCFA';
			$confStyle['size']			= '38';
			$confStyle['margin']	= '380';				
		}
		else if($nota == 8)
		{
			$confStyle['background'] = '#00B0FB';
			$confStyle['size']			= '29';
			$confStyle['margin']	= '440';			
		}	
		else if($nota > 8 && $nota < 9)
		{
			$confStyle['background'] = '#00B0FB';
			$confStyle['size']			= '24';			
			$confStyle['margin']	= '440';				
		}					
		else if($nota == 9)
		{
			$confStyle['background'] = '#005CFB';
			$confStyle['size']			= '14';
			$confStyle['margin']	= '487';			
		}
		else if($nota > 9 && $nota < 10)
		{
			$confStyle['background'] = '#005CFB';
			$confStyle['size']			= '10';
			$confStyle['margin']	= '487';			
		}
		else if($nota == 10)
		{
			$confStyle['background'] = '#001CBD';
			$confStyle['size']			= '0';
			$confStyle['margin']	= '515';			
		}		
		
		/*
		switch($nota)
		{
			case 0:
				$confStyle['background'] =  '#FD0F02';
				$confStyle['size']		 = '143';
				$confStyle['margin']	= '0';
				break;
				
			case 1:
				$confStyle['background'] = '#FD0F02';
				$confStyle['size']			= '127';
				$confStyle['margin']	= '0';
				break;
				
			case 2:
				$confStyle['background'] = '#FD6D0D';
				$confStyle['size']			= '114';
				$confStyle['margin']	= '105';
				break;
				
			case 3:
				$confStyle['background'] = '#FD9F59';
				$confStyle['size']			= '99';
				$confStyle['margin']	= '164';
				break;
				
			case 4:
				$confStyle['background'] = '#FEC200';
				$confStyle['size']		= '85';
				$confStyle['margin']	= '216';
				break;
				
			case 5:
				$confStyle['background'] = '#FEF502';
				$confStyle['size']			= '71';
				$confStyle['margin']	= '276';
				break;
				
			case 6:
				$confStyle['background'] = '#20FF11';
				$confStyle['size']			= '57';
				$confStyle['margin']	= '328';
				break;
					
			case 7:
				$confStyle['background'] = '#00FCFA';
				$confStyle['size']			= '43';
				$confStyle['margin']	= '380';
				break;
				
			case 8:
				$confStyle['background'] = '#00B0FB';
				$confStyle['size']			= '29';
				$confStyle['margin']	= '440';
				break;
				
			case 9:
				$confStyle['background'] = '#005CFB';
				$confStyle['size']			= '14';
				$confStyle['margin']	= '487';
				break;
					
			case 10:
				$confStyle['background'] = '#001CBD';
				$confStyle['size']			= '0';
				$confStyle['margin']	= '515';
				break;	

			default:
				$confStyle['background'] = '#FD0F02';
				$confStyle['size']			= '143' ;
				$confStyle['margin']	= '0';
				break;	
		}
		*/
		
		return $confStyle;
	}
}