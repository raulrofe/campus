<?php


$idUsuario = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();


if (isset($idCurso) && is_numeric($idCurso)) {

	$objModeloUsuariosConectados = new ModeloNotificaciones();
	mvc::cargarModuloSoporte('usuarios_conectados');
	$objModeloUsuariosConectados = new ModeloUsuariosConectados();


	
	$html = null;
	$conectadosActualmente = array();
	$conectadosAntes = (!empty($_SESSION['conectadosAntes']))?$_SESSION['conectadosAntes']: array();



	// obtiene los alumnos y tutores que se han conectado
	if (Usuario::compareProfile('alumno')) {
		$alumnos = $objModeloUsuariosConectados->obtenerAlumnosConectadosParaAlumnos(Usuario::getIdUser(), Usuario::getIdCurso());
		$tutores = $objModeloUsuariosConectados->obtenerRRHHConectadosParaAlumnos(Usuario::getIdCurso());
	} else {
		$alumnos = $objModeloUsuariosConectados->obtenerAlumnosConectadosParaRRHH(Usuario::getIdUser());
		$tutores = $objModeloUsuariosConectados->obtenerRRHHConectadosParaRRHH(Usuario::getIdUser());
	}




	// recorremos los alumnos actualmente conectados para comprobar cuales son nuevos conectados
	while ($alumno = $alumnos->fetch_object()) {
		$conectadosActualmente[$alumno->idalumnos] = array('id' => $alumno->idalumnos, 'nombrec' => $alumno->nombrec, 'idcurso' => $alumno->idcurso, 'titulocurso' => $alumno->titulocurso);

		if (!isset($conectadosAntes[$alumno->idalumnos])) {
			$html .= '<span data-translate-html="conectados.elalumno">El alumno</span>'
				. ' <b>' . $alumno->nombrec . '</b> '
				. '<span data-translate-html="conectados.haentrado">ha entrado a</span> '
				. '<b><i>' . $alumno->titulocurso . '</i></b><br />';
		}
	}

	

	// recorremos los rrhh actualmente conectados para comprobar cuales son los nuevos conectados
	while ($tutor = $tutores->fetch_object()) {
		$conectadosActualmente['t_' . $tutor->idrrhh] = array('id' => 't_' . $tutor->idrrhh, 'nombrec' => $tutor->nombrec);

		// si no estaba en ningun curso (segun la SESSION) o ha cambiado a otro curso
		if (!isset($conectadosAntes['t_' . $tutor->idrrhh])) {
			$html .= '<b>' . $tutor->nombrec . '</b> '
				. '<span data-translate-html="conectados.haentradocurso">ha entrado al curso</span>'
				. ' <br />';
		} 
	}


	// obtiene a los usuarios que se han desconectado y entrado a un curso
	foreach ($conectadosAntes as $key => $item) {
		$esAlumno = preg_match('/^([0-9]+)$/', $key);
		if(!empty($item['titulocurso'])){
			if ($esAlumno && !isset($conectadosActualmente[$item['id']])) {
				$html .= '<span data-translate-html="conectados.elalumno">El alumno</span> '
					. '<b>' . $item['nombrec'] . '</b> '
					. '<span data-translate-html="conectados.hasalido">ha salido de</span> '
					. '<b><i>' . $item['titulocurso'] . '</i></b><br />';
			} else if (!$esAlumno && !isset($conectadosActualmente['t_' . $item['id']])) {
				$html .= '<b>' . $item['nombrec'] . '</b> '
					. '<span data-translate-html="conectados.hasalido">ha salido de</span> '
					. '<b><i>' . $item['titulocurso'] . '</i></b><br />';
			}
		}
	}

	// carga la nueva lista de usuarios conectados a la session
	$_SESSION['conectadosAntes'] = $conectadosActualmente;

	// devuelve los datos recogidos
	echo json_encode(array('html' => $html));
}
