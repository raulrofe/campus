<?php

$objModelo = new ModeloBlocNotas();

$get = Peticion::obtenerGet();

//var_dump($get);

if(isset($get['idbloc'], $get['estado']) && is_numeric($get['idbloc']))
{

	if($get['estado'] == 'finalizar')
	{
		$estado = 1;
	}
	else if ($get['estado'] == 'pendiente')
	{
		$estado = 0;
	}
	
	if($objModelo->actualizarEstado($get['idbloc'], $estado))
	{
		Alerta::guardarMensajeInfo('tareamodificada','El estado de la tarea ha sido modificado');
	}
	else
	{
		Alerta::guardarMensajeInfo('tareamodificadaerror','Error al modificar el estado de la tarea');
	}
	
	Url::redirect('bloc-de-notas');
}