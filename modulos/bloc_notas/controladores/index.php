<?php

$idUsuario = Usuario::getIdUser();
$objModelo = new ModeloBlocNotas();

$idMatricula = Usuario::getIdMatricula();	
$idPerfil = Usuario::obtenerIdPerfilLogueo();

$get = Peticion::obtenerGet();

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();		
	if(isset($post['mensaje']) && !empty($post['mensaje']))
	{
			$resultado = $objModelo->guardarNotaAlumno($post['idMatricula'], $post['mensaje'], $idPerfil);
			if($resultado)
			{
				Alerta::guardarMensajeInfo('notaguardada','Se ha guardado tu nota');
			}	
	}
}
	
$blocNotasCount = $objModelo->obtenerNotaAlumno($idMatricula, $idPerfil, $numPagina);
$maxElementsPaging = 8;
$maxPaging = $blocNotasCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		
$blocNotas = $objModelo->obtenerNotaAlumno($idMatricula, $idPerfil, $elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');