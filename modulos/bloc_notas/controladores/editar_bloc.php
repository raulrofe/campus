<?php

$idUsuario = Usuario::getIdUser();
$objModelo = new ModeloBlocNotas();

$idMatricula = Usuario::getIdMatricula();	
$idPerfil = Usuario::obtenerIdPerfilLogueo();

$get = Peticion::obtenerGet();

if(isset($get['idbloc']) && is_numeric($get['idbloc']))
{	
	$resultado = $objModelo->obtenerUnaNota($get['idbloc'], $idPerfil);
	$nota = $resultado->fetch_object();
}

if(Peticion::isPost())
{

	$post = Peticion::obtenerPost();
	
	if(isset($post['mensaje']) && !empty($post['mensaje']))
	{
		if(isset($post['idbloc']) && is_numeric($post['idbloc']))
		{
			if($objModelo->editarNota($post['idbloc'], $post['mensaje']))
			{
				Alerta::guardarMensajeInfo('notaeditada','Nota editada');
				Url::redirect('bloc-de-notas');
			}
		}
	}
}

$blocNotas = $objModelo->obtenerNotaAlumno($idMatricula, $idPerfil);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');