<?php

$idUsuario = Usuario::getIdUser();
$objModelo = new ModeloBlocNotas();

$idMatricula = Usuario::getIdMatricula();	
$idPerfil = Usuario::obtenerIdPerfilLogueo();

$get = Peticion::obtenerGet();

if(isset($get['idbloc']) && is_numeric($get['idbloc']))
{	
	if($objModelo->eliminarNota($get['idbloc'], $idPerfil))
	{
		Alerta::guardarMensajeInfo('notaeliminada','Nota eliminada del bloc');
		Url::redirect('bloc-de-notas');
	}
}

$blocNotas = $objModelo->obtenerNotaAlumno($idMatricula, $idPerfil);
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');