<div id="blocNotas">
	<br/>
	<div class='subtitle t_center'>
		<img src="imagenes/boc_notas/notes.png" style="vertical-align:middle;"/>

		<span data-translate-html="notas.titulo">
			BLOC DE NOTAS
		</span>
	</div>
</div>

<div id="blocDeNotas">
	<div id="createNote">
		<form method="post" action="">
			<input type="text" name="mensaje" style="width:80%;float:left;margin-top:3px;margin-right: 5px;" placeholder="Crear nota" data-translate-placeholder="formulario.crear" />
			<input type="submit" value="Guardar" id="btnGuardar" style="width:68px;float:left;margin-top:3px;padding:4px" data-translate-value="formulario.guardar"/>
			<input type="hidden" name="idMatricula" value="<?php echo $idMatricula; ?>" />			
		</form>
	</div>
	
	<div id="optionHeadBlocNotas"></div>

	<div class="clear"></div>
		<?php if($blocNotas->num_rows > 0):?>
			<?php while($bloc = $blocNotas->fetch_object()):?>
				<div id="msjList" class="cursiva">
					<div class="checkBoxBlocNota">
					<?php if($bloc->finalizada == 1):?>
						<a href="bloc-de-notas/tarea/pendiente/<?php echo $bloc->idbloc_notas?>">
							<img src="imagenes/boc_notas/check-enable.png" alt="tarea finalizada" title="Marcar la tarea como pendiente" data-translate-title="notas.pendiente"/>
						</a>
					<?php else :?>
						<a href="bloc-de-notas/tarea/finalizar/<?php echo $bloc->idbloc_notas?>">
							<img src="imagenes/boc_notas/check-disable.png" alt="tarea pendiente" title="Marcar la tarea como finalizada" data-translate-title="notas.finalizada"/>
						</a>
					<?php endif ?>
					</div>
					
					<!-- Nota para mostrar -->
					<div class="fleft viewTask" id="viewTaskblocNotas_<?php echo $bloc->idbloc_notas;?>">
						<span <?php if($bloc->finalizada == 1) {echo "class='completada'";}?> >
							<?php echo Texto::textoFormateado($bloc->nota); ?>	
						</span>

						<br/>
						<span class="fechaBloc fleft">
							<?php echo Fecha::obtenerFechaFormateada($bloc->fecha); ?>
						</span>
					</div>
					
					<!-- nota para editar -->
					<div class="fleft hide oneViewTask" id="EditTaskBlocNotas_<?php echo $bloc->idbloc_notas;?>">
						<form name="frmEditTaskBlocNotas_<?php echo $bloc->idbloc_notas;?>" method="post" action="bloc-de-notas/editar" class="fleft">
							<input class="fleft" type="text" name="mensaje" value="<?php echo Texto::textoFormateado($bloc->nota); ?>" style="width:300px;"/>
							<input type="hidden" value="<?php echo $bloc->idbloc_notas ?>" name="idbloc"/>

							<button class="butonTask">
								<img src="imagenes/boc_notas/edit-task.png" alt="editar tarea" data-translate-titel="notas.editar"/>
							</button>

							<button class="butonTask" onclick="restartNote(<?php echo $bloc->idbloc_notas; ?>);return false;">
								<img src="imagenes/boc_notas/close-task.png" alt="cancelar tarea" data-translate-title="notas.cancelar"/>
							</button>
						</form>	
					</div>
					
					<!-- Opciones del bloc de notas -->
					<div class="fright">
						<span style="font-size:0.9em;">
							<a href="#" onclick="editNote(<?php echo $bloc->idbloc_notas ?>);return false;" rel="tooltip" title="Editar nota" data-translate-title="notas.editar" data-translate-title="notas.editar">
								<img src="imagenes/boc_notas/edit-note.png" alt="Editar nota" />
							</a> 

							<a <?php echo Alerta::alertConfirmOnClick("eliminarnotabloc","¿Deseas eliminar la nota de tu bloc?", "bloc-de-notas/eliminar/" . $bloc->idbloc_notas) ?> href="#" rel="tooltip" title="Eliminar nota" data-translate-title="notas.eliminar">
								<img src="imagenes/boc_notas/delete-note.png" alt="Eliminar nota" />
							</a>
						</span>
					</div>

					<div class="clear"></div>

				</div>	

			<?php endwhile ?>

		<?php else: ?>

			<div id="msjList" class="cursiva" style="padding-left:65px;">
				<span data-translate-html="notas.no_creadas">
					No se han creado anotaciones en el bloc de notas
				</span>
			</div>		
		<?php endif ?>
	
	<div id="option"></div>
	<div id="option"></div>
</div>

<?php echo Bloc_notas::paginar($numPagina, $maxPaging, 'bloc-de-notas/pagina/')?>

<br/>
<script type="text/javascript" src="js-bloc_notas-default.js"></script>