LibBlocNotas = {};

LibBlocNotas.edit = function()
{
	$('#blocNotas_html').addClass('hide');
	$('#blocNotas_textarea').removeClass('hide');
	
	$('#blocNotas_btn1').addClass('hide');
	$('#blocNotas_btn2').removeClass('hide');
};

LibBlocNotas.noEdit = function()
{
	$('#blocNotas_textarea').addClass('hide');
	$('#blocNotas_html').removeClass('hide');
	
	$('#blocNotas_btn2').addClass('hide');
	$('#blocNotas_btn1').removeClass('hide');
};

function addNote()
{
	$('#createNote').removeClass('hide');
}

function editNote(id)
{
	$('.viewTask').removeClass('hide');
	$('.oneViewTask').addClass('hide');
	$('#viewTaskblocNotas_' + id).addClass('hide');
	$('#EditTaskBlocNotas_' + id).removeClass('hide');
}

function restartNote(id)
{
	$('#viewTaskblocNotas_' + id).removeClass('hide');
	$('#EditTaskBlocNotas_' + id).addClass('hide');
}