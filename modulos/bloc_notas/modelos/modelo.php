<?php
class ModeloBlocNotas extends modeloExtend
{
	public function guardarNotaAlumno($idMatricula, $nota, $idPerfil)
	{
		$sql = "insert INTO bloc_notas (idmatricula, nota, idperfil) VALUES ('$idMatricula', '$nota', '$idPerfil')";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNotaAlumno($idMatricula, $idPerfil, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		if(!$idPerfil)
		{
			$idPerfil = 0;
		}
		
		$sql = "SELECT nota, fecha, idbloc_notas, finalizada FROM bloc_notas " .
		" WHERE idmatricula = " . $idMatricula .
		" AND idperfil = " . $idPerfil .
		" ORDER BY fecha DESC" . $addQuery;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function guardarNotaRRHH($idrrhh, $idCurso, $mensaje)
	{
		$sql = 'UPDATE staff SET bloc_notas = "' . $mensaje . '" WHERE idrrhh = ' . $idrrhh . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNotaRRHH($idrrhh, $idCurso)
	{
		$sql = 'SELECT bloc_notas FROM staff WHERE idrrhh = ' . $idrrhh . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarNota($idNota)
	{
		$sql = "DELETE from bloc_notas" .
		" where idbloc_notas = " . $idNota;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerUnaNota($idNota)
	{
		$sql = "SELECT nota, fecha, idbloc_notas" .
		" FROM bloc_notas" .
		" WHERE idbloc_notas = " . $idNota;
		
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function editarNota($idNota, $mensaje)
	{
		
		$sql="UPDATE bloc_notas" .
		" SET nota = '" . $mensaje . "'" .
		" WHERE idbloc_notas = " . $idNota;	
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function actualizarEstado($idbloc, $estado)
	{
		echo $sql = "UPDATE bloc_notas" .
		" SET finalizada = " . $estado .
		" WHERE idbloc_notas = " . $idbloc;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
}