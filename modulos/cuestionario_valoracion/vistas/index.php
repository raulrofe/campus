<div id="mensajes_emergentes">
	<div class='subtitle t_center'><span>Avisos personales</span></div>
	<div class="popupIntro">
		<p>Los avisos personales permiten al alumno establecerse recordatorios</p>
	</div>
	<br />
	<div class='menu_interno'>
		<span class='elemento_menuinterno'>
			<a href="avisos-personales/nuevo" title="">Nuevo aviso personal</a>
		</span>
		<span class='elemento_menuinterno'>
			<a href="avisos-personales/listado" title="">Modificar/borrar aviso personal</a>
		</span>
	</div>
</div>