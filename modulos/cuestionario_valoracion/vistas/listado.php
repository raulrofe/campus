<div id="mensajes_emergentes">
	<div class='subtitle t_center'>
		<img src="imagenes/avisos_personales/ads.png" alt="" style="vertical-align:middle;"/>
		<span>AVISOS PERSONALES</span>
	</div>
	<div class="popupIntro">
		<p class="t_center">Los avisos personales permiten al alumno establecerse recordatorios</p>
	</div>
	
	
	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>	
		<div class="fleft panelTabMenu" id="navAvisosPersonales" style="z-index:600">		
			<div id="nav1" class="redondearBorde">
				<a href="avisos-personales/nuevo" title="Nuevo aviso personal">
					<img src="imagenes/avisos_personales/aviso-personal.png" alt="" />
					<span>Nuevo<br/>aviso personal</span>
				</a>
			</div>
			
			<div id="nav2" class="blanco redondearBorde">
				<a href="avisos-personales/listado" title="listado avisos personales" style="display:block;">
					<img src="imagenes/avisos_personales/edit-aviso-personal.png" alt="" />
					<span style='font-size:0.8em;text-align:center;padding-bottom:10px;'>Modificar/borrar aviso personal</span>
				</a>
			</div>	
		</div>
	</div>
	
	<!-- ************************************************************************************************************* -->
	<!-- CONTENIDO DE PESTANAS -->
	
	<div class='fleft panelTabContent' id="contenidoAvisosPersonales">
				
		<!-- TAB 2 : MODIFICAR/BORRAR AVISOS PERSONALES -->
		<div id="tab2">
			<p class="tituloTabs">Modificar/borrar Avisos personales</p><br/>
			<div>
				<div class="listarElementos">
					<?php if($mensajes->num_rows > 0):?>
						<?php while($mensaje = $mensajes->fetch_object()):?>
							<div class="elemento">
								<div><?php echo $mensaje->mensaje; ?></div>
								<div class="elementoPie">
									<div class="fleft">
										<?php 
											if($mensaje->dias_repeticion == 0) echo date('d/m/Y H:i', strtotime($mensaje->fecha_a_recibir));
											else
											{
												$dias = Fecha::diaSemana($mensaje->dias_repeticion);
												for($i=0; $i<=count($dias)-1; $i++)
												{
													echo $dias[$i];
												}
											} 
										?>
									</div>
									<ul class="elementoListOptions fright" id="optionsAvisosPersonales">
										<li>
											<a rel="tooltip" title="Editar aviso personal" href="avisos-personales/aviso/editar/<?php echo $mensaje->idmensaje_emergente_personal?>"
												title=""><img src="imagenes/options/edit.png" alt="" /></a>
										</li>
										<li>
											<a rel="tooltip" title="Eliminar aviso personal" href="#"
												<?php echo Alerta::alertConfirmOnClick('borraravisopersonal','¿Realmente quiere borrar el aviso personal?',
													'avisos-personales/aviso/borrar/' . $mensaje->idmensaje_emergente_personal);?>
												title=""><img src="imagenes/options/delete.png" alt="" /></a>
										</li>
									</ul>
								</div>
								<div class="clear"></div>
							</div>
						<?php endwhile;?>
						
						<?php echo Paginado::crear($numPagina, $maxPaging, 'avisos-personales/listado/pagina/')?>
						
					<?php else:?>
						<div class="negrita t_center">No se encontraron avisos personales</div>
					<?php endif;?>
				</div>
			</div>
		</div>

	</div>
	
	<!-- *************************************************************************************************************** -->
	
</div>
	