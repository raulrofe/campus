<?php
$objModelo = new ModeloAvisosPersonales();

$get = Peticion::obtenerGet();

if(isset($get['idaviso']) && is_numeric($get['idaviso']))
{
	$rowMensaje = $objModelo->obtenerUnMensajePorUsuario($get['idaviso'], Usuario::getIdUser(true));
	if($rowMensaje->num_rows == 1)
	{
		$objModelo->eliminarMensajePorUsuario($get['idaviso']);
	}
}

Url::redirect('avisos-personales/listado');