<?php
$objModelo = new ModeloAvisosPersonales();

$get = Peticion::obtenerGet();
$idUsuario = Usuario::getIdUser(true);

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}
		
$mensajesCount = $objModelo->obtenerMensajesPorUsuario($idUsuario);
$maxElementsPaging = 5;
$maxPaging = $mensajesCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		
$mensajes = $objModelo->obtenerMensajesPorUsuario($idUsuario, $elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listado');