<?php
$get = Peticion::obtenerGet();

require 'configMimes.php';

$idusuario = Usuario::getIdUser(true);

$mi_modulo = new Modulos();

if(isset($get['id_archivo']) && is_numeric($get['id_archivo']))
{
	$mi_modulo->set_archivo($get['id_archivo']);
	$rowAdjunto = $mi_modulo->buscar_archivo(Usuario::getIdCurso());
	if($rowAdjunto->num_rows > 0)
	{
		$rowAdjunto = $rowAdjunto->fetch_array();
		
		$extension = Fichero::obtenerExtension($rowAdjunto['enlace_archivo']);
		if(file_exists($rowAdjunto['enlace_archivo']) && isset($mimes[$extension]))
		{
			//header("Content-disposition: attachment; filename=" . $rowAdjunto['idarchivo_temario'] . '.' . $extension);
			header("Content-type: application/pdf");
			readfile('./' . $rowAdjunto['enlace_archivo']);
		}
		else
		{
			Url::lanzar404();
		}
	}
	else
	{
		Url::lanzar404();
	}
}
else
{
	Url::lanzar404();
}
