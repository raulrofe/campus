<?php

$idCurso = Usuario::getIdCurso();
$idUsuario = Usuario::getIdUser();

//Para controlar el Contenido SCORM
mvc::cargarModuloSoporte('scorm');
$objModeloScorm = new ModeloScorm();
$scorms = $objModeloScorm->obtenerScorms($idUsuario, $idCurso);

$scormsCurso = array();

if($scorms->num_rows > 0)
{
	$contCurso = 0;
	while($scorm = $scorms->fetch_object())
	{
		//if(file_exists(PATH_ROOT . 'archivos/scorm/' . $scorm->idscorm . '/sco_' . $scorm->idscorm_folder . '/default.html'))
		//{
			$scormsCurso[$contCurso]['idScorm'] = $scorm->idscorm;
			$scormsCurso[$contCurso]['titulo'] = $scorm->titulo;
			$scormsCurso[$contCurso]['descripcion'] = $scorm->descripcion;
			$scormsCurso[$contCurso]['imagenScorm'] = $scorm->imagen_scorm;

			$contCurso++;
		//}
	}
}


$mi_curso = new Cursos();
$mi_modulo = new Modulos();

$mi_curso->set_curso($idCurso);

$registro_curso = $mi_curso->buscar_curso();
$curso = $registro_curso->fetch_object();

$registros_modulos = $mi_modulo->modulos_asignado_curso();

$registros_modulos2 = $mi_modulo->modulos_asignado_curso();

$archivosTemasCurso = $mi_curso->archivosTemarioCurso($idCurso);

$nArchivosTemasCurso = $archivosTemasCurso->num_rows;

$get = Peticion::obtenerGet();

if($get['vista'] == 'listado')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'archivador');
}
else if($get['vista'] == 'grafica')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'archivador_libreria');
}

