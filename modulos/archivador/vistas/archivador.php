<br/>
<?php require DIRNAME(__FILE__) . '/intro_archivador.php'; ?>
<div class="caja_separada" id="vistaClasica">
	<div class="menuVista" style="float:right">
		<div class="vista3" >
			<a href="#" onclick="return false;"><img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.listado" alt="Listado"/></a>
		</div>
		<div class="vista4" >
			<a href="aula/archivador/grafica"><img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.clasica" alt="Vista Clásica"/></a>
		</div>
	</div>
	<div class="clear"></div>
	<br/>

	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuContenidosCurso">
		<div id="navArchiveGuia" class="blanco redondearBorde" onclick="changeTab(1)">
			<img src="imagenes/archivador/user-guide.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="contenidos.guia">
				Gu&iacute;a did&aacute;ctica
			</p>
		</div>
		<div id="navArchiveTemario" class="redondearBorde" onclick="changeTab(2)">
			<img src="imagenes/archivador/temario.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="contenidos.temario">
				Temario (PDF)
			</p>
		</div>
		<div id="navArchiveScorm" class="redondearBorde" onclick="changeTab(3)">
			<img src="imagenes/archivador/multimedia.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="contenidos.scorm">
				Contenido Interactivo (SCORM)
			</p>
		</div>
	</div>

	<!-- CONTENIDO -->
	<div class='fleft panelTabContent' id="contenidoCurso">

		<!--*********************************** TAB 1 GUIAS DE USUARIO ******************************************************** -->

		<div id="archiveGuia">
			<p class="tituloTabs" data-translate-html="contenidos.guia">
				Gu&iacute;a did&aacute;ctica
			</p>
			<div class="listarElementos">
				<div class="elemento">
					<a href="archivos/guias_didacticas/Guiadidactica_id<?php echo $_SESSION['idcurso']; ?>.pdf" target="_blank">
						<span data-translate-html="contenidos.guiacurso">
							1. Gu&iacute;a did&aacute;ctica del curso: "
						</span>
						<?php echo Texto::textoPlano($curso->titulo) ?>"
					</a>
				</div>

				<div class="elemento">
					<a href="archivos/guia-del-usuario.pdf" target="_blank" data-translate-html="contenidos.guiaplataforma">
						2. Gu&iacute;a de usuario de la Plataforma
					</a>
				</div>

			</div>
		</div>

		<!-- ***************************** TAB 2 TEMARIO ************************************************************* -->

		<div id="archiveTemario" class="hide" >
			<p class="tituloTabs" data-translate-html="contenidos.temario">
				Temario (PDF)
			</p>
			<?php
			$numero_de_modulo = 1;
			$totalModulos = mysqli_num_rows($registros_modulos);
			while ($f = mysqli_fetch_assoc($registros_modulos)) {

				if ($totalModulos > 1) {
					echo "<div class='subtitleModulo'>M&oacute;dulo" . $numero_de_modulo . ": " . $f['nombre'] . "</div>";
				} else {
					echo "<div class='subtitleModulo'>" . $f['nombre'] . "</div>";
				}

				//buscamos los archivos dentro de cada modulo
				/*
				  $mi_modulo->set_modulo($f['idmodulo']);
				  $mi_modulo->set_categoria_archivo(1);
				  $registros_archivos = $mi_modulo->archivos_modulo();
				 */

				$numeroArchivosEliminado = 0;
				$nArchivos = $nArchivosTemasCurso;
				if ($nArchivos > 0) {
					echo "<div class='archivosModulo'>";
					while ($f = $archivosTemasCurso->fetch_array()) {
						if (file_exists($f['enlace_archivo'])) {
							$peso_archivo = filesize($f['enlace_archivo']) / 1024;
							$peso_archivo = $peso_archivo / 1024;
							$peso_archivo = round($peso_archivo, 2);
							$fecha = Fecha::invertir_fecha($f['f_creacion']);
							echo "<div class='listado_tabla'>
												<div class='tituloArchivo' style='width:80%;'>";
							//<img src='imagenes/archive_pdf.png' alt='' style='vertical-align:middle;'/>
							echo "<a href='aula/archivador/forzar_descarga/" . $f['idarchivo_temario'] . "' target='_blank'>" . mb_strtoupper($f['titulo']) . "</a></div>
												<div class='botonDescargaArchivo'>
													<a href='aula/archivador/forzar_descarga_muestra/" . $f['idarchivo_temario'] . "' target='_blank' data-translate-html='general.descargar'>
														Descargar
													</a>
												</div>
												<div class='clear'><br/></div>";
							if (!empty($f['descripcion'])) {
								echo "<div style='font-size:0.8em;margin-top:-8px;width:87%;' class='t_justify'>(" . $f['descripcion'] . ")</div>";
							}
							echo "</div>";

							$nArchivos--;
							if ($nArchivos > 0) {
								//echo "<hr/>";
							}
						} else {
							$numeroArchivosEliminado++;
						}

						if ($numeroArchivosEliminado == $nArchivos) {
							//echo "no existe ningun archivo para este m&oacute;dulo";
						}
					}
					echo "</div>";
				} else {
					//echo "No existe ningun archivo para este m&oacute;dulo";
				}
				$numero_de_modulo ++;
			}
			?>
		</div>

		<!-- ***************************** TAB 3 SCORM ************************************************************* -->

		<div id="archiveScorm" class="hide">
			<p class="tituloTabs" data-translate-html="contenidos.scorm">
				Contenido Interactivo (SCORM)
			</p>
			<?php if (count($scormsCurso) > 0): ?>
				<div class="listarElementos">
					<?php $contS = 0; ?>
					<?php foreach ($scormsCurso as $item): ?>
						<div class="elemento">
							<div class="elementoContenido">
								<?php if ($item['idScorm'] == 80): ?>
									<div class='fleft' style='margin-right:15px;'><img src='imagenes/scorm.png'/></div>
									<p style='line-height:27px;'>
										<a href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item['idScorm'] ?>', 1024, 627, false); return false;" title="Abrir este contenido multimedia" data-translate-title="contenidos.abrir">
											<?php echo Texto::textoPlano($item['titulo']) ?></a>
									</p>
								</div>
							<?php else: ?>
								<?php if ($_SESSION['idcurso'] == 1016 && $contS > 2): ?>
									<?php $contS++; ?>
								<?php else: ?>
									<div class='fleft' style='margin-right:15px;'><img src='imagenes/scorm.png'/></div>
									<p style='line-height:27px;'>												
										<a href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item['idScorm'] ?>', 600, 810, false); return false;" title="Abrir este contenido multimedia" data-translate-title="contenidos.abrir">
											<?php echo Texto::textoPlano($item['titulo']) ?></a>	
									</p>
								</div>											
							<?php endif; ?>
						<?php endif; ?>
						<div class="clear"></div>
					</div>
					<?php $contS++; ?>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<div class="t_center" data-translate-html="contenidos.noexisten">
				No existen contenidos con Scorm
			</div>
		<?php endif; ?>
	</div>


</div>
</div>
<script type="text/javascript" src="js-archivador-vista_archivador.js"></script>
