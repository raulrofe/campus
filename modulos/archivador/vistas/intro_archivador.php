	<div class='caja_separada'>
		<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/archivador/archivador.png" alt="" style="vertical-align:middle;"/>
				<span data-translate-html="contenidos.titulo">
					Contenidos
				</span>
			</div>
			<br/>
	 		<p class='t_justify' data-translate-html="contenidos.titulodescripcion">
				El aula virtual cuenta con un archivador electr&oacute;nico donde se encuentran almacenados ficheros directamente relacionados con los contenidos formativos del m&oacute;dulo.
			</p>
			<br/>
			<p class='t_justify' data-translate-html="contenidos.titulodescripcion2">
				Esta secci&oacute;n le ofrece la posibilidad de cargar nuevos ficheros (con extensi&oacute;n m&aacute;xima de 1MB) y eliminar aqu&eacute;llos que han quedado obsoletos.
			</p>
		</div>
		<?php else: ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/archivador/archivador.png" alt="" style="vertical-align:middle;"/>
				<span data-translate-html="contenidos.titulo">
					Contenidos
				</span>
			</div>
			<br/>
	 		<p class='t_center' data-translate-html="contenidos.descripción">
				Aqu&iacute; puede ver y descargar el contenido del curso.
			</p>
			<br/>
		</div>
		<?php endif;?>
	</div>
