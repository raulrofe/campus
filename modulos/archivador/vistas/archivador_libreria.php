<!-- Comienza la vista diseño -->
<div id="archivador">
	<br/>
	<?php require DIRNAME(__FILE__) . '/intro_archivador.php';?>

 	<div class="tituloMenuEnlaceInteres">
 		<div class="subtitleModulo fleft" style="padding:0">
			<?php echo $_SESSION['nombrecurso']; ?>
		</div>
		<div class="menuVista2" style="float:right;">
			<div class="vista1" >
				<a href="aula/archivador/listado">
					<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.listado" alt="Listado"/>
				</a>
			</div>
			<div class="vista2" >
				<a href="#" onclick="return false;">
					<img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.clasica" alt="Vista Clásica"/>
				</a>
			</div>
		</div>
	 	<div class="clear"></div>
 	</div>

	<br/>

	<div id="shelf">

		<!-- *************** SHELF GUIAS ************************ -->
		<div class="shelfGuias">
			<div class="booksCenter">
				<div class="book">
					<a rel="tooltip" href="archivos/guias_didacticas/Guiadidactica_id<?php echo $_SESSION['idcurso']; ?>.pdf" target="_blank" style="display:block"
					   title="Gu&iacute;a did&aacute;ctica" data-translate-title="contenidos.guia">
						<img src="archivos/cursos/1/thumbnail/guia-didactica.png" alt="" />
					</a>
				</div>

				<div class="book">
					<a rel="tooltip" href="archivos/guia-de-usuario.pdf" target="_blank" style="display:block"
					   title="Gu&iacute;a de usuario" data-translate-title="contenidos.listado" data-translate-href="guiausuario.enlace">
						<img src="archivos/thumbnail-guia-del-usuario.png" alt="" />
					</a>
				</div>

			</div>
		</div>

		<!-- *************** SHELF TEMARIO ************************ -->
		<?php $newShelfTemario = 1; ?>
		<?php $finalShelf = 0; ?>
		<?php if($nArchivosTemasCurso > 0):?>
			<?php while($archivoTema = $archivosTemasCurso->fetch_object()):?>

				<?php //var_dump($archivoTema); ?>
				<?php if($newShelfTemario == 1):?>
					<div class="shelfTemario">
						<div class="booksCenter">
				<?php endif; ?>

					<!-- Datos del archivo -->
					<?php
						$peso_archivo = filesize($archivoTema->enlace_archivo)/1024;
						$peso_archivo = $peso_archivo/1024;
						$peso_archivo = round($peso_archivo,2);
						//$fecha = Fecha::invertir_fecha($f['f_creacion']);
					?>

						<div class="book">
							<a rel="tooltip" title="Modulo: <?php echo $archivoTema->nombre; ?> <br/> Titulo: <?php echo $archivoTema->titulo; ?> (<?php echo $peso_archivo; ?> Mb)" href='aula/archivador/forzar_descarga_muestra/<?php echo $archivoTema->idarchivo_temario ?>' target='_blank'>
								<?php if($archivoTema->idarchivo_temario == 7290): ?>
									<img src="archivos/cursos/thumbnail/bloque1_indice.png" alt="" />
								<?php elseif($archivoTema->idarchivo_temario == 7297): ?>
									<img src="archivos/cursos/thumbnail/bloque2_indice.png" alt="" />
								<?php elseif($archivoTema->idarchivo_temario == 7306 || $archivoTema->idarchivo_temario == 7285): ?>
									<img src="archivos/cursos/thumbnail/bibliografia.png" alt="" />
								<?php else: ?>
									<img src="archivos/cursos/thumbnail/tema<?php echo $archivoTema->prioridad ?>.png" alt="" />
								<?php endif; ?>
							</a>
						</div>

					<?php
						$newShelfTemario++;
						$finalShelf++;
					?>

				<?php if($newShelfTemario > 4 || $finalShelf == $nArchivosTemasCurso):?>
						</div>
					</div>
					<?php $newShelfTemario = 1; ?>
				<?php endif; ?>

			<?php endwhile; ?>
		<?php endif;?>

		<!-- *************** SHELF SCORM ************************ -->
		<?php if(count($scormsCurso) > 0):?>

					<?php $newShelfTemario = 1; ?>
					<?php $finalShelf = 0; ?>
					<?php $contS = 0; ?>
					<?php foreach($scormsCurso as $item):?>

						<!-- Parche para mostrar los cursos partidos -->
						<?php if(($_SESSION['idcurso'] == 814 || $_SESSION['idcurso'] == 816 || $_SESSION['idcurso'] == 817 || $_SESSION['idcurso'] == 818) && $contS < 2): ?>
							<?php $contS++;?>
						<?php elseif($_SESSION['idcurso'] == 819 && $contS != 1 && $contS != 6 && $contS != 7): ?>
							<?php $contS++; ?>
						<?php elseif($_SESSION['idcurso'] == 1016 && $contS > 2): ?>
							<?php $contS++; ?>							
						<?php else: ?>
						<!-- Funcionamiento normal de los scorm en modo estanteria -->
							<?php if($newShelfTemario == 1):?>
								<div class="shelfScorm">
									<div class="booksCenter">
							<?php endif; ?>

								<div class="book2">
									<?php if($item['idScorm'] == 44): ?>
										<a rel="tooltip" title="<?php echo $item['titulo']  ?>" href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item['idScorm']?>', 650, 1240, false); return false;" style="display:block">
											<?php if(!file_exists($item['imagenScorm'])): ?>
												<img src="archivos/cursos/thumbnail/scorm.png" alt="" style="width:118px;height:90px;" />
											<?php else: ?>
												<img src="<?php echo $item['imagenScorm'] ?>" alt="" style="width:118px;height:90px;"/>
											<?php endif; ?>
										</a>
									<?php elseif($item['idScorm'] >= 80 || $item['idScorm'] <= 89 ): ?>
										<a rel="tooltip" title="<?php echo $item['titulo'] ?>" href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item['idScorm']?>', 627, 1024, false); return false;" style="display:block">
											<?php if(!file_exists($item['imagenScorm'])): ?>
												<img src="archivos/cursos/thumbnail/scorm.png" alt="" style="width:118px;height:90px;" />
											<?php else: ?>
												<img src="<?php echo $item['imagenScorm'] ?>" alt="" style="width:118px;height:90px;"/>
											<?php endif; ?>
										</a>
									<?php else: ?>
										<a rel="tooltip" title="<?php echo $item['titulo'] ?>" href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item['idScorm']?>', 600, 810, false); return false;" style="display:block">
											<?php if(!file_exists($item['imagenScorm'])): ?>
												<img src="archivos/cursos/thumbnail/scorm.png" alt="" style="width:118px;height:90px;" />
											<?php else: ?>
												<img src="<?php echo $item['imagenScorm'] ?>" alt="" style="width:118px;height:90px;"/>
											<?php endif; ?>
										</a>
									<?php endif; ?>
								</div>

							<?php
								$newShelfTemario++;
								$finalShelf++;
							?>

							<?php if($newShelfTemario > 3 || $finalShelf == $nArchivosTemasCurso):?>
									</div>
								</div>
								<?php $newShelfTemario = 1; ?>
							<?php endif; ?>

							<?php $contS++;?>

							<!-- Para los cursos partidos no mostrar los temas del final -->
							<?php if(($_SESSION['idcurso'] == 792 OR $_SESSION['idcurso'] == 795 OR $_SESSION['idcurso'] == 798 OR $_SESSION['idcurso'] == 799 OR $_SESSION['idcurso'] == 800)
									&& $contS == 2): ?>
								<?php break; ?>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach;?>
				</div>
			</div>
		<?php endif;?>

	</div>
</div>