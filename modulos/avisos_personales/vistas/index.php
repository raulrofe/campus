<div id="mensajes_emergentes">
	<div class='subtitle t_center'>
		<span data-translate-html="personales.titulo">
			Avisos personales
		</span>
	</div>
	<div class="popupIntro">
		<p data-translate-html="personales.descripcion">
			Los avisos personales permiten al alumno establecerse recordatorios
		</p>
	</div>
	<br />
	<div class='menu_interno'>
		<span class='elemento_menuinterno'>
			<a href="avisos-personales/nuevo" data-translate-html="personales.nuevo">
				Nuevo aviso personal
			</a>
		</span>
		<span class='elemento_menuinterno'>
			<a href="avisos-personales/listado" data-translate-html="personales.modificar">
				Modificar/borrar aviso personal
			</a>
		</span>
	</div>
</div>