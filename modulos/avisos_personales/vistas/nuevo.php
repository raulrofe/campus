
<!-- 
<span class="infoPopupModal">
	<a href="#" onclick="viewInfo();return false;">
		<img src="imagenes/info.png" alt="" style="vertical-align:middle;"/>
	</a>
</span>
-->

<div id="mensajes_emergentes">
	<div class='subtitle t_center'>
		<img src="imagenes/avisos_personales/ads.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="personales.tituloM">
			AVISOS PERSONALES
		</span>
	</div>
	<br/>
	<div class="popupIntro">
		<p class="t_center" data-translate-html="personales.descripcion">
			Los avisos personales permiten al alumno establecerse recordatorios
		</p>
	</div>

	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>	
		<div class="fleft panelTabMenu" id="navAvisosPersonales" style="z-index:600">		
			<div id="nav1" class="blanco redondearBorde">
				<a href="avisos-personales/nuevo" title="Nuevo aviso personal">
					<img src="imagenes/avisos_personales/aviso-personal.png" alt="" />
					<span data-translate-html="personales.nuevo">
						Nuevo<br/>aviso personal
					</span>
				</a>
			</div>

			<div id="nav2" class="redondearBorde">
				<a href="avisos-personales/listado" title="listado avisos personales" style="display:block;">
					<img src="imagenes/avisos_personales/edit-aviso-personal.png" alt="" />
					<span data-translate-html="personales.modificar" style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
						Modificar/borrar aviso personal
					</span>
				</a>
			</div>	
		</div>
	</div>

	<!-- ************************************************************************************************************* -->
	<!-- CONTENIDO DE PESTANAS -->

	<div class='fleft panelTabContent' id="contenidoAvisosPersonales">

		<!-- TAB 1 : NUEVO AVISOS PERSONALES -->
		<div id="tab1">
			<p class="tituloTabs" data-translate-html="personales.nuevo">
				Nuevo Aviso personal
			</p>
			<br/>
			<div>
				<form id="frm_mensajes_emergentes" action="" method="post">
					<ul>
						<li id="mensajes_emergentes_iconos">
							<h2 data-translate-html="personales.nuevo">
								Selecciona un icono para el aviso
							</h2>
							<ul>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.alegre" title="Alegre" rel="1">
										<img src="imagenes/mensajes_emergentes/icono_alegre.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.guino" title="Gui&ntilde;o" rel="2">
										<img src="imagenes/mensajes_emergentes/icono_guino.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.sonriente" title="Sonriente" rel="3">
										<img src="imagenes/mensajes_emergentes/icono_sonrisa.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.triste" title="Triste" rel="4">
										<img src="imagenes/mensajes_emergentes/icono_triste.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.amor" title="Amor" rel="5">
										<img src="imagenes/mensajes_emergentes/icono_amor.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.enfadado" title="Enfadado" rel="6">
										<img src="imagenes/mensajes_emergentes/icono_enfadado.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.tiempo" title="Tiempo" rel="7">
										<img src="imagenes/mensajes_emergentes/icono_reloj.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.pregunta" title="Pregunta" rel="8">
										<img src="imagenes/mensajes_emergentes/icono_pregunta.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.informacion" title="Informaci&oacute;n" rel="9">
										<img src="imagenes/mensajes_emergentes/icono_info.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.bien" title="Bien" rel="10">
										<img src="imagenes/mensajes_emergentes/icono_ok.png" alt="" />
									</a>
								</li>
								<li>
									<a href="#" onclick="return false;" data-translate-title="iconos.exclamacion" title="Esclamaci&oacute;n" rel="11">
										<img src="imagenes/mensajes_emergentes/icono_exclamacion.png" alt="" />
									</a>
								</li>
							</ul>
							<input id="mensajes_emergentes_iconos_hidden" type="hidden" name="icono" />
							<div class="clear"></div>
						</li>
						<li>
							<h2 data-translate-html="personales.escribe">
								Escribe el mensaje del aviso
							</h2>
							<textarea id="avisos_personales_textarea" name="mensaje" rows="1" cols="1"></textarea>
						</li>
						<li>
							<h2 data-translate-html="personales.cuando">
								¿A partir de cuando recibir&eacute; el mensaje de aviso?
							</h2>
							<div id="FechaEncabezado" class="hide">
								<p data-translate-html="personales.nonecesario">
									No es necesario seleccionar fecha y hora para esta modalidad de aviso personal
								</p>
							</div>
						</li>
						<li id="frm_mensajes_emergentes_fecha">
							<div class='fleft' id='FechaDia'>
								<span data-translate-html="personales.fecha">
									Fecha
								</span>
								<input id="datepicker" type="text" name="fecha_a_recibir" />
							</div>
						</li>
						<li id="frm_mensajes_emergentes_hora">
							<div id='FechaTiempo'>
								<div class='fleft' style='margin:0 15px;'>
									<span data-translate-html="personales.horas">
										Horas
									</span>
									<select name="hora_a_recibir">
										<?php for ($cont = 0; $cont < 24; $cont++): ?>
											<option value="<?php echo $cont ?>"><?php echo $cont ?></option>
										<?php endfor; ?>
									</select>
								</div>
								<div class='fleft'>
									<span data-translate-html="personales.minutos">
										Minutos
									</span>
									<select name="minutos_a_recibir">
										<?php for ($cont = 0; $cont < 60; $cont++): ?>
											<option value="<?php echo $cont ?>"><?php echo $cont ?></option>
										<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class='clear'></div>
						</li>
						<li>
							<h2 data-translate-html="personales.repetir">
								Repetir el aviso cada...
							</h2>
						</li>
						<li id="frm_mensajes_emergentes_repetir">
							<select name="opcion_repetir" onchange="avisos_personales_seleccionar_repeticion(this);">
								<option value="1" data-translate-html="personales.fecha_elegida">Solo en fecha elegida</option>
								<option value="2" data-translate-html="personales.dia_aviso">Seleccionar d&iacute;a para el aviso</option>
							</select>
							<div id="frm_mensajes_emergentes_repetir_dias" class="hide">
								<ul class='diasAviso'>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_1" data-translate-html="dias.lunes">
											Lunes
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_1" type="checkbox" name="repetir_dias[]" value="1" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_2" data-translate-html="dias.martes">
											Martes
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_2" type="checkbox" name="repetir_dias[]" value="2" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_3" data-translate-html="dias.miercoles">
											Miercoles
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_3" type="checkbox" name="repetir_dias[]" value="3" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_4" data-translate-html="dias.jueves">
											Jueves
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_4" type="checkbox" name="repetir_dias[]" value="4" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_5" data-translate-html="dias.viernes">
											Viernes
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_5" type="checkbox" name="repetir_dias[]" value="5" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_6" data-translate-html="dias.sabado">
											Sabado
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_6" type="checkbox" name="repetir_dias[]" value="6" />
										<div class="clear"></div>
									</li>
									<li>
										<label for="frm_mensajes_emergentes_repetir_dias_7" data-translate-html="dias.domingo">
											Domingo
										</label>
										<input id="frm_mensajes_emergentes_repetir_dias_7" type="checkbox" name="repetir_dias[]" value="7" />
										<div class="clear"></div>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</li>
						<li class="clear t_right">
							<button type="submit" data-translate-html="formulario.enviar">
								Enviar
							</button>
						</li>
					</ul>
					<div class="clear"></div>
				</form>				
			</div>
		</div>
	</div>

	<!-- *************************************************************************************************************** -->

</div>

<script type="text/javascript" src="js-avisos_personales-default.js"></script>

<script type="text/javascript">
								$("#datepicker").datepicker({
									dateFormat: 'dd/mm/yy',
									minDate: new Date(),
									firstDay: 1,
									dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
									monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
								});
</script>

<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist",
	});
</script>