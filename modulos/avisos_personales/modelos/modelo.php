<?php
class ModeloAvisosPersonales extends modeloExtend
{
	public function insertarMensaje($idusuario, $mensaje, $fecha_a_recibir, $icono, $diasRepeticion)
	{
		$sql = 'INSERT INTO avisos_personales (idusuario, mensaje, fecha_creacion, fecha_a_recibir, url_icono, dias_repeticion)' .
		' VALUES ("' . $idusuario . '", "' . $mensaje . '", "' . date('Y-m-d H:i:s') . '", "' . $fecha_a_recibir . '",' .
		' "' . $icono . '", "' . $diasRepeticion . '")';
		$resultado = $this->consultaSql($sql);
				
		return $resultado;
	}

	public function actualizarMensaje($idmensaje, $mensaje, $fecha_a_recibir, $icono, $diasRepeticion)
	{
		$sql = 'UPDATE avisos_personales SET mensaje = "' . $mensaje . '", fecha_a_recibir = "' . $fecha_a_recibir . '",' .
		' url_icono = "' . $icono . '", dias_repeticion = "' . $diasRepeticion . '"' .
		' WHERE idmensaje_emergente_personal = ' . $idmensaje;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajesPorUsuario($idusuario, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = 'SELECT * FROM avisos_personales AS ap WHERE ap.idusuario = "' . $idusuario . '"' . $addQuery;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnMensajePorUsuario($idmensaje, $idusuario)
	{
		$sql = 'SELECT * FROM avisos_personales AS ap WHERE ap.idusuario = "' . $idusuario . '"
				AND ap.idmensaje_emergente_personal = ' . $idmensaje;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarMensajePorUsuario($idmensaje)
	{
		$sql = 'DELETE FROM avisos_personales WHERE idmensaje_emergente_personal = ' . $idmensaje;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}