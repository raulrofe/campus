<?php
$objModelo = new ModeloAvisosPersonales();

$get = Peticion::obtenerGet();

if(isset($get['idaviso']) && is_numeric($get['idaviso']))
{
	$rowMensaje = $objModelo->obtenerUnMensajePorUsuario($get['idaviso'], Usuario::getIdUser(true));
	if($rowMensaje->num_rows == 1)
	{
		$mensaje = $rowMensaje->fetch_object();
		
		$iconosArray = array(
			'icono_alegre.png', 'icono_guino.png', 'icono_sonrisa.png', 'icono_triste.png', 'icono_amor.png', 'icono_enfadado.png',
			'icono_reloj.png', 'icono_pregunta.png', 'icono_info.png', 'icono_ok.png', 'icono_exclamacion.png'
		);
		$iconosArrayFlip = array_flip($iconosArray);
			
		// POST
		if(Peticion::isPost())
		{
			$post = peticion::obtenerPost();
			
			if(!empty($post['mensaje']))
			{
				if(isset($post['mensaje'], $post['icono'], $post['fecha_a_recibir'], $post['hora_a_recibir'], $post['minutos_a_recibir'], $post['opcion_repetir'])
					&& !empty($post['mensaje']) && is_numeric($post['hora_a_recibir']) && is_numeric($post['minutos_a_recibir']))
				{	

					$icono = 'NULL';
					if(isset($post['icono']) && is_numeric($post['icono']) && isset($iconosArray[$post['icono']-1]))
					{
						$icono = $iconosArray[$post['icono']-1];
					}

					// obtenemos los dias donde se ejecutara el aviso
					$diasRepeticion = '0';
					if($post['opcion_repetir'] == 2)
					{
						if(isset($post['repetir_dias']) && is_array($post['repetir_dias']) && count($post['repetir_dias']) > 0)
						{
							$arrayDiasRepeticion = array_unique($post['repetir_dias']);
							$diasRepeticion = '';
							foreach($arrayDiasRepeticion as $rep_dia)
							{
								if($rep_dia > 0 && $rep_dia <= 7)
								{
									$diasRepeticion .= $rep_dia . ',';
								}
							}
							$diasRepeticion = substr($diasRepeticion, 0, strlen($diasRepeticion) - 1);
						}
					}
					
					//validamos la fecha y hora
					if($post['opcion_repetir'] == 1)
					{
						$fechaArray = explode('/', $post['fecha_a_recibir']);
						if(count($fechaArray) == 3 && (checkdate($fechaArray[1], $fechaArray[0], $fechaArray[2]))
							&& ($post['hora_a_recibir'] >= 0 && $post['hora_a_recibir'] < 24) && ($post['minutos_a_recibir'] >= 0 && $post['minutos_a_recibir'] < 60))
						{
							$fecha_a_recibir = $fechaArray[2] . '-' . $fechaArray[1] . '-' . $fechaArray[0] . ' ' . $post['hora_a_recibir'] . ':' . $post['minutos_a_recibir'] . ':00';
								
							$resultado = $objModelo->actualizarMensaje($mensaje->idmensaje_emergente_personal,
								$post['mensaje'], $fecha_a_recibir, $icono, $diasRepeticion
							);
											
							if($resultado)
							{
								Alerta::guardarMensajeInfo('avisoactualizado','Se ha actualizado el aviso');
								Url::redirect('avisos-personales/listado');
							}
						}
						else
						{
							Alerta::mostrarMensajeInfo('fechanovalida','La fecha y/o hora no son v&aacute;lidas');
						}
					}
					
					// Validamos la fecha y hora para dias en que se realiza el aviso
					if($post['opcion_repetir'] == 2)
					{
						if(($post['hora_a_recibir'] >= 0 && $post['hora_a_recibir'] < 24) 
							&& ($post['minutos_a_recibir'] >= 0 && $post['minutos_a_recibir'] < 60))
						{						
							$fecha_a_recibir = '0000-00-00';
							$resultado = $objModelo->actualizarMensaje(
								$mensaje->idmensaje_emergente_personal, $post['mensaje'], $fecha_a_recibir,
								$icono, $diasRepeticion
							);	
						}
						else
						{
							Alerta::mostrarMensajeInfo('horanovalida','La hora no es v&aacute;lidas');	
						}
					}
					
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('debesmensaje','Debes escribir un mensaje');
			}
			
			Url::redirect('avisos-personales/listado');
		}
		else
		{
			$diasRepeticion = array();
			if($mensaje->dias_repeticion != 0)
			{
				$diasRepeticion = explode(',', $mensaje->dias_repeticion);
			}
			
			$mensajeHora = date('H', strtotime($mensaje->fecha_a_recibir));
			$mensajeMinuto = date('i', strtotime($mensaje->fecha_a_recibir));
			
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar');
		}
	}
}