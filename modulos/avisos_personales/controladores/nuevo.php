<?php
$objModelo = new ModeloAvisosPersonales();

if(Usuario::compareProfile(array('tutor', 'alumno', 'coordinador')))
{
	// POST
	if(Peticion::isPost())
	{
		$post = peticion::obtenerPost();
		
		$iconosArray = array(
			'icono_alegre.png', 'icono_guino.png', 'icono_sonrisa.png', 'icono_triste.png', 'icono_amor.png', 'icono_enfadado.png',
			'icono_reloj.png', 'icono_pregunta.png', 'icono_info.png', 'icono_ok.png', 'icono_exclamacion.png'
		);
		
		if(!empty($post['mensaje']))
		{
			if(isset($post['mensaje'], $post['fecha_a_recibir'], $post['hora_a_recibir'], $post['minutos_a_recibir'], $post['opcion_repetir'])
				&& !empty($post['mensaje']) && is_numeric($post['hora_a_recibir']) && is_numeric($post['minutos_a_recibir']))
			{
					
				$icono = 'NULL';
				if(isset($post['icono']) && is_numeric($post['icono']) && isset($iconosArray[$post['icono']-1]))
				{
					$icono = $iconosArray[$post['icono']-1];
				}
					
				// obtenemos los dias donde se ejecutara el aviso
				$diasRepeticion = '0';
				if($post['opcion_repetir'] == 2)
				{
					if(isset($post['repetir_dias']) && is_array($post['repetir_dias']) && count($post['repetir_dias']) > 0)
					{
						// Creamo fechas para la los dia de repeticion (SQL)
						$arrayDiasRepeticion = array_unique($post['repetir_dias']);
						$diasRepeticion = '';
						foreach($arrayDiasRepeticion as $rep_dia)
						{
							if($rep_dia > 0 && $rep_dia <= 7)
							{
								$diasRepeticion .= $rep_dia . ',';
							}
						}
						$diasRepeticion = substr($diasRepeticion, 0, strlen($diasRepeticion) - 1);
					}
				}
				
				//validamos la fecha y hora para solo en dia de comienzo del aviso
				if($post['opcion_repetir'] == 1)
				{
					$fechaArray = explode('/', $post['fecha_a_recibir']);
					if(count($fechaArray) == 3 && (checkdate($fechaArray[1], $fechaArray[0], $fechaArray[2]))
						&& ($post['hora_a_recibir'] >= 0 && $post['hora_a_recibir'] < 24) 
						&& ($post['minutos_a_recibir'] >= 0 && $post['minutos_a_recibir'] < 60))
					{
						$fecha_a_recibir = $fechaArray[2] . '-' . $fechaArray[1] . '-' . $fechaArray[0] . ' ' . $post['hora_a_recibir'] . ':' . $post['minutos_a_recibir'] . ':00';
							
						$resultado = $objModelo->insertarMensaje(
							Usuario::getIdUser(true), $post['mensaje'], $fecha_a_recibir, $icono, $diasRepeticion
						);
										
						if($resultado)
						{
							Alerta::mostrarMensajeInfo('avisoenviado','Se ha enviado el aviso, lo recibir&aacute;s en la fecha y hora que indicastes');
						}
					}
					else
					{
						Alerta::mostrarMensajeInfo('fechanovalida','La fecha y/o hora no son v&aacute;lidas');
					}
				}
				
				// Validamos la fecha y hora para dias en que se realiza el aviso
				if($post['opcion_repetir'] == 2)
				{
					if(($post['hora_a_recibir'] >= 0 && $post['hora_a_recibir'] < 24) 
						&& ($post['minutos_a_recibir'] >= 0 && $post['minutos_a_recibir'] < 60))
					{
						
						//print_r($diasRepeticion);
						
						//echo $post['hora_a_recibir'].":".$post['minutos_a_recibir'];
						
						$fecha_a_recibir = '0000-00-00';
						$resultado = $objModelo->insertarMensaje(
							Usuario::getIdUser(true), $post['mensaje'], $fecha_a_recibir, $icono, $diasRepeticion
						);	
					}
					else
					{
						Alerta::mostrarMensajeInfo('horanovalida','La hora no es v&aacute;lidas');	
					}
				}	
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('debesmensaje','Debes escribir un mensaje');
		}
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevo');
}