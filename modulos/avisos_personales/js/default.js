function avisos_personales_seleccionar_repeticion(elemento)
{
	var tipo = $(elemento).val();
	
	if(tipo == 1)
	{
		$('#frm_mensajes_emergentes_repetir_dias').addClass('hide');
		$('#FechaEncabezado').addClass('hide');
		$('#FechaDia').removeClass('hide');
		$('#FechaTiempo').removeClass('hide');
	}
	else
	{
		$('#frm_mensajes_emergentes_repetir_dias').removeClass('hide');
		$('#FechaEncabezado').removeClass('hide');
		$('#FechaDia').addClass('hide');
		$('#FechaTiempo').addClass('hide');
	}
}

function changeTab(num)
{
	$('#navAvisosPersonales > div').removeClass('blanco');
	$('#contenidoAvisosPersonales > div').addClass('hide');
	
	$('#tab' + num).removeClass('hide');
	$('#nav' + num).addClass('blanco');
}