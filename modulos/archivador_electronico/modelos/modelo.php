<?php 
class ArchivadorElectronico extends modeloExtend{
		
	public function __construct()
	{
		parent::__construct();
	}

	public function insertarArchivoElectronico($titulo, $descripcion, $idUsuario, $idCurso)
	{
		$sql = "INSERT INTO archivador_electronico" .
		" (titulo, descripcion, idusuario, idcurso)" .
		" VALUES ('$titulo', '$descripcion', '$idUsuario', '$idCurso')";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarArchivoAdjuntoElectronico($archivo, $archivo_adjunto, $id)
	{
		if(isset($archivo))
		{
			$sql = "UPDATE archivador_electronico" .
			" SET archivo_adjunto = '$archivo', archivo_adjunto_nombre = '$archivo_adjunto'" .
			" WHERE idarchivador_electronico = ".$id;
		}
		else
		{
			$sql = "UPDATE archivador_electronico" .
			" SET archivo_adjunto = NULL" .
			" WHERE idarchivador_electronico = ".$id;			
		}
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarArchivoAdjuntoElectronicoEdicion($archivo ,$archivo_adjunto, $id)
	{
		if(isset($archivo))
		{
			$sql = "UPDATE archivador_electronico_edicion" .
			" SET archivo_adjunto = '$archivo', archivo_adjunto_nombre = '$archivo_adjunto'" .
			" WHERE idarchivador_electronico_edicion = ".$id;
		}
		else
		{
			$sql = "UPDATE archivador_electronico_edicion" .
			" SET archivo_adjunto = NULL" .
			" WHERE idarchivador_electronico_edicion = ".$id;
		}
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerArchivosElectronicos($idCurso)
	{
		$sql = "SELECT ae.idarchivador_electronico, ae.titulo, ae.descripcion, ae.fecha, ae.archivo_adjunto, ae.idusuario, rh.nombrec, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl" .
		" FROM archivador_electronico as ae" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = ae.idusuario" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ae.idusuario" .
		" WHERE ae.idcurso = " . $idCurso .
		" AND ae.borrado = 0" .
		" ORDER BY ae.idarchivador_electronico DESC";			
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}
	
	public function obtenerArchivosElectronicosPendientes($idCurso)
	{
		$sql = "SELECT ae.idarchivador_electronico_edicion, ae.idarchivador_electronico, ae.titulo, ae.descripcion, ae.fecha, ae.archivo_adjunto, ae.idusuario, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl" .
		" FROM archivador_electronico_edicion as ae" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ae.idusuario" .
		" WHERE ae.moderado = 0 AND ae.idcurso = " . $idCurso .
		" ORDER BY ae.idarchivador_electronico_edicion DESC";			
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}
	
	public function obtenerArchivo($id, $idcurso = null)
	{
		$addSql = null;
		if($idcurso)
		{
			$addSql = ' AND ae.idcurso = ' . $idcurso;
		}
		
		$sql = "SELECT ae.idarchivador_electronico, ae.titulo, ae.descripcion, ae.fecha, ae.archivo_adjunto, ae.archivo_adjunto_nombre, ae.idusuario, 
		rh.nombrec, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl" .
		" FROM archivador_electronico as ae" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = ae.idusuario" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ae.idusuario" .
		" WHERE ae.borrado = 0 AND ae.idarchivador_electronico = " . $id . $addSql;	
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerArchivoEdicion($id)
	{
		$sql = "SELECT ae.idarchivador_electronico_edicion, ae.idarchivador_electronico, ae.titulo, ae.descripcion, ae.fecha, ae.archivo_adjunto, ae.archivo_adjunto_nombre, ae.idcurso, ae.idusuario, 
		ae.eliminar, ae.editar, rh.nombrec, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl" .
		" FROM archivador_electronico_edicion as ae" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = ae.idusuario" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ae.idusuario" .
		" WHERE ae.idarchivador_electronico_edicion = " . $id;	
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function editarArchivoEdicion($idArchivo, $titulo, $descripcion, $edicion)
	{
		if(isset($idArchivo))
		{
			$sql = "INSERT INTO archivador_electronico_edicion " .
			"(idarchivador_electronico, titulo, descripcion, idusuario, idcurso, editar) values" .
			" (" . $idArchivo . ",'" . $titulo . "','" . $descripcion . "', '" . Usuario::getIdUser() . "', " . Usuario::getIdCurso() . ", " . $edicion . ")";
		}
		else
		{
			$sql = "INSERT INTO archivador_electronico_edicion " .
			"(titulo, descripcion, idusuario, idcurso) values" .
			" ('" . $titulo . "','" . $descripcion . "', '" . Usuario::getIdUser() . "', " . Usuario::getIdCurso() . ")";
		}
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function editarArchivo($idArchivo, $titulo, $descripcion)
	{
		$sql = "UPDATE archivador_electronico" .
		" SET titulo = '" . $titulo . "'," .
		" descripcion = '" . $descripcion . "'" .
		" WHERE idarchivador_electronico = " . $idArchivo;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarArchivoAdjunto($id)
	{
		$sql = "UPDATE archivador_electronico SET archivo_adjunto = '' WHERE idarchivador_electronico = ".$id;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function eliminarArchivoEdicion($id)
	{
		$sql = "INSERT INTO archivador_electronico_edicion " .
			"(idarchivador_electronico, idusuario, idcurso, eliminar) values" .
			" (" . $id . ", '" . Usuario::getIdUser() . "', " . Usuario::getIdCurso() . ", 1)";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarArchivo($id)
	{
		$sql = "UPDATE archivador_electronico SET borrado = 1 where idarchivador_electronico = " . $id;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function aceptarPeticion($idArchivo)
	{
		$sql = "UPDATE archivador_electronico_edicion SET moderado = 1 WHERE idarchivador_electronico_edicion = " . $idArchivo;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function rechazarPeticion($idArchivo)
	{
		$sql = "UPDATE archivador_electronico_edicion SET moderado = 1 WHERE idarchivador_electronico_edicion = " . $idArchivo;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}
	
	public function obtenerArcvhivosPorAlumno($idAlumno, $idcurso)
	{
		$sql = "SELECT idarchivador_electronico" .
		" FROM archivador_electronico" .
		" WHERE idusuario = " . $idAlumno .
		" AND idcurso = " . $idcurso .
		" AND borrado = 0";	
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
}
?>