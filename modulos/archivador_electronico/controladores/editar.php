<?php 
$objArchivadorElectronico = new ArchivadorElectronico();

//Archivos pertidos para adjuntar
$extensionesAdjunto = array(
	'jpg',
	'jpeg',
	'png',
	'doc', 
	'docx', 
	'xls', 
	'xlsx', 
	'pdf',
	'ppt',
	'pptx',
	'swf',
	'bmp',
	'tiff',
	'tif',
	'txt',
	'text',
	'html',
	'htm',
	'css',
	'zip',
	'rar'
);
			
$get = Peticion::obtenerGet();

//Valor de peticion
if(Usuario::compareProfile('alumno'))
{
	//echo 'alumno'.$idUsuario = Usuario::getIdUser();	
	$peticion = 3;
}
else
{
	//echo 'tutor'.$idUsuario = Usuario::getIdUser(true);
	$peticion = 1;
}

if(isset($get['idArchivo']) && is_numeric($get['idArchivo']))
{
	$archivo = $objArchivadorElectronico->obtenerArchivo($get['idArchivo']);
	if($archivo->num_rows > 0)
	{
		$dataArchivo = $archivo->fetch_object();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'archivador_editar');
	}
}

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
		
	if(isset($dataArchivo, $post['idArchivo'], $post['titulo'], $post['descripcion']) && is_numeric($post['idArchivo'])
		 && !empty($post['titulo']) && !empty($post['descripcion']))
	{
		if(Usuario::compareProfile('alumno'))
		{
			if($objArchivadorElectronico->editarArchivoEdicion($post['idArchivo'], $post['titulo'], $post['descripcion'], 1))
			{
				$idArchEd = $objArchivadorElectronico->obtenerUltimoIdInsertado();
				
				// eliminamos el archivo actual
				if(isset($_POST['eliminarArchivo']) && $_POST['eliminarArchivo'] == 1)
				{
					if(!empty($dataArchivo->archivo_adjunto))
					{			
						$objArchivadorElectronico->insertarArchivoAdjuntoElectronicoEdicion(null, null, $idArchEd);
					}
				}
				else if(isset($_FILES['archivoelectronico']['tmp_name']) && !empty($_FILES['archivoelectronico']['tmp_name']))
				{
					if ($_FILES['archivoelectronico']['error'] == 0)
					{
						//Compruebo si el tipo de archivo es valido
						if($archivosNoValidos = Fichero::validarTipos($_FILES['archivoelectronico']['name'], $_FILES['archivoelectronico']['type'], $extensionesAdjunto))
						{
							// elimina archivo antiguo
							if(!empty($dataArchivo->archivo_adjunto))
							{
								@chmod($dataArchivo->archivo_adjunto, 0777);
								@unlink($dataArchivo->archivo_adjunto);
							}
							
							$extension =  Texto::obtenerExtension($_FILES['archivoelectronico']['name']);	
							$directorio = 'archivos/archivador_electronico_edicion/';
							move_uploaded_file($_FILES["archivoelectronico"]["tmp_name"],$directorio.$idArchEd.'.'.$extension);
							if(file_exists($directorio.$idArchEd.'.'.$extension))
							{
								$archivoElectronico = $directorio.$idArchEd.'.'.$extension;
								if($objArchivadorElectronico->insertarArchivoAdjuntoElectronicoEdicion($archivoElectronico, Texto::clearFilename($_FILES['archivoelectronico']['name']), $idArchEd))
								{
									Alerta::guardarMensajeInfo('enlacemoderar','El enlace ha sido mandado para moderar');
								}
								else
								{
									Alerta::guardarMensajeInfo('archivonoinsertado','El archivo no pudo ser insertado');
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('errormoverarchivo','Error al mover el archivo');
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('archivonovalido','Archivo no válido');
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('errorguardar','Error al guardar el archivo');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('enlacemandado','El enlace ha sido mandado para moderar');
				}
			}
		}
		else
		{
			if($objArchivadorElectronico->editarArchivo($post['idArchivo'], $post['titulo'], $post['descripcion'], 1))
			{	
				// eliminamos el archivo actual
				if(isset($_POST['eliminarArchivo']) && $_POST['eliminarArchivo'] == 1)
				{
					if(!empty($dataArchivo->archivo_adjunto))
					{							
						if(file_exists(PATH_ROOT . $dataArchivo->archivo_adjunto))
						{
							@chmod(PATH_ROOT . $dataArchivo->archivo_adjunto, 0777);
							unlink(PATH_ROOT . $dataArchivo->archivo_adjunto);	
							
							$objArchivadorElectronico->insertarArchivoAdjuntoElectronico('', '', $post['idArchivo']);		
						}
					}
				}
				else if(isset($_FILES['archivoelectronico']['tmp_name']) && !empty($_FILES['archivoelectronico']['tmp_name']))
				{
					if ($_FILES['archivoelectronico']['error'] == 0)
					{
						//Compruebo si el tipo de archivo es valido
						if($archivosNoValidos = Fichero::validarTipos($_FILES['archivoelectronico']['name'], $_FILES['archivoelectronico']['type'], $extensionesAdjunto))
						{
							// elimina archivo antiguo
							if(!empty($dataArchivo->archivo_adjunto))
							{
								@chmod($dataArchivo->archivo_adjunto, 0777);
								@unlink($dataArchivo->archivo_adjunto);
							}
								
							$extension =  Texto::obtenerExtension($_FILES['archivoelectronico']['name']);	
							$directorio = 'archivos/archivador_electronico/';
							move_uploaded_file($_FILES["archivoelectronico"]["tmp_name"],$directorio.$post['idArchivo'].'.'.$extension);
							if(file_exists($directorio.$post['idArchivo'].'.'.$extension))
							{
								$archivoElectronico = $directorio.$post['idArchivo'].'.'.$extension;
								if($objArchivadorElectronico->insertarArchivoAdjuntoElectronico($archivoElectronico, Texto::clearFilename($_FILES['archivoelectronico']['name']), $post['idArchivo']))
								{
									Alerta::guardarMensajeInfo('enlaceeditado','El enlace ha sido editado');
								}
								else
								{
									Alerta::guardarMensajeInfo('archivonoeditado','El archivo no pudo ser editado');
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('errormoverarchivo','Error al mover el archivo');
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('archivonovalido','Archivo no valido');
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('errorguardar','Error al guardar el archivo');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('enlaceeditado','El enlace ha sido editado');
				}
			}	
			else
			{
				Alerta::guardarMensajeInfo('enlaceeditado','El enlace ha sido editado');
			}
		}
		
		Url::redirect('aula/archivador_electronico');
	}
	
}
