<?php

$objArchivadorElectronico = new ArchivadorElectronico();

$get = Peticion::obtenerGet();

if(isset($get['idArchivo']) && is_numeric($get['idArchivo']))
{
	$row = $objArchivadorElectronico->obtenerArchivo($get['idArchivo']);
		
	if($row->num_rows == 1)
	{
		$row = $row->fetch_object();
		
		if(Usuario::compareProfile('alumno'))
		{
			if($objArchivadorElectronico->eliminarArchivoEdicion($get['idArchivo']))
			{
				Alerta::guardarMensajeInfo('enviadoparaeliminar','El archivo ha sido enviado a moderar para eliminar');
			}
			else
			{
				Alerta::guardarMensajeInfo('noenviadoparaeliminar','El archivo no pudo ser enviado a moderar para eliminar');	
			}
		}
		else
		{
			if($objArchivadorElectronico->eliminarArchivo($get['idArchivo']))
			{
				// elimina archivo antiguo
				if(!empty($row->archivo_adjunto) && file_exists($row->archivo_adjunto))
				{
					@chmod($row->archivo_adjunto, 0777);
					@unlink($row->archivo_adjunto);
				}
				
				Alerta::guardarMensajeInfo('archivoeliminado','El archivo ha sido eliminado');
			}
			else
			{
				Alerta::guardarMensajeInfo('archivonoeliminado','El archivo no pudo ser eliminado');	
			}
		}
	}
}

Url::redirect('aula/archivador_electronico');