<?php
if(!Usuario::compareProfile('tutor'))
{
	Url::lanzar404();
}

mvc::cargarModuloSoporte('tareas_tutor');

$objArchivadorElectronico = new ArchivadorElectronico();
$objTareasTutor = new ModeloTareasTutor();
	
$idconv = Usuario::getIdConvocatoria();
$idUsuario = Usuario::getIdUser();

$post = Peticion::obtenerPost();
	
$cursosTutor = $objTareasTutor->cursosTutor($idconv, $idUsuario);

if($cursosTutor->num_rows > 0)
{
	$nCursos = $cursosTutor->num_rows;
	$archivador = array();
	$cont = 0;
	while($cursoTutor = $cursosTutor->fetch_object())
	{		
		$aportArchivador = $objArchivadorElectronico->obtenerArchivosElectronicosPendientes($cursoTutor->idcurso);
		if($aportArchivador->num_rows > 0)
		{
			$archivador[$cont]['curso'][] = $cursoTutor->titulo;
			while($aport = $aportArchivador->fetch_object())
			{
				$archivador[$cont]['titulo'][] = $aport->titulo;
				$archivador[$cont]['descripcion'][] = $aport->descripcion;
				$archivador[$cont]['fecha'][] = $aport->fecha;
				$archivador[$cont]['archivo'][] = $aport->archivo_adjunto;
				$archivador[$cont]['usuario'][] = $aport->nombrecAl;
				$archivador[$cont]['id'][] = $aport->idarchivador_electronico_edicion;	
			}
			
			$cont++;	
		}
	}	
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'administrar');
