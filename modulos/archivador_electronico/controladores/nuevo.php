<?php
$objArchivadorElectronico = new ArchivadorElectronico();

//Archivos pertidos para adjuntar
$extensionesAdjunto = array(
	'jpg',
	'jpeg',
	'png',
	'doc',
	'docx',
	'xls',
	'xlsx',
	'pdf',
	'ppt',
	'pptx',
	'swf',
	'bmp',
	'tiff',
	'tif',
	'txt',
	'text',
	'html',
	'htm',
	'css',
	'zip',
	'rar',
	'wav',
	'mp3'
);

$get = Peticion::obtenerGet();

//Valor de peticion
if(Usuario::compareProfile('alumno'))
{
	//echo 'alumno'.$idUsuario = Usuario::getIdUser();
	$peticion = 0;
}
else
{
	//echo 'tutor'.$idUsuario = Usuario::getIdUser(true);
	$peticion = 1;
}

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();

	if(isset($post['titulo'], $post['descripcion']) && !empty($post['titulo']) && !empty($post['descripcion']))
	{
		if(Usuario::compareProfile('alumno'))
		{
			if($objArchivadorElectronico->editarArchivoEdicion(null, $post['titulo'], $post['descripcion'], 0))
			{
				$idArchEd = $objArchivadorElectronico->obtenerUltimoIdInsertado();

				if(isset($_FILES['archivoelectronico']['tmp_name']) && !empty($_FILES['archivoelectronico']['tmp_name']))
				{
					if ($_FILES['archivoelectronico']['error'] == 0)
					{
						//Compruebo si el tipo de archivo es valido.
						if($archivosNoValidos = Fichero::validarTipos($_FILES['archivoelectronico']['name'], $_FILES['archivoelectronico']['type'], $extensionesAdjunto)) {

							$extension =  Texto::obtenerExtension($_FILES['archivoelectronico']['name']);
							$directorio = 'archivos/archivador_electronico_edicion/';

							move_uploaded_file($_FILES["archivoelectronico"]["tmp_name"],$directorio.$idArchEd.'.'.$extension);

							if(file_exists($directorio.$idArchEd.'.'.$extension))
							{
								$archivoElectronico = $directorio . $idArchEd . '.' . $extension;
								if($objArchivadorElectronico->insertarArchivoAdjuntoElectronicoEdicion($archivoElectronico, Texto::clearFilename($_FILES['archivoelectronico']['name']), $idArchEd)) {

									Alerta::guardarMensajeInfo('enlacemoderar','El enlace ha sido mandado para moderar');
								}
								else
								{
									Alerta::guardarMensajeInfo('archivonoinsertado','El archivo no pudo ser insertado');
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('errormoverarchivo','Error al mover el archivo');
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('errorguardar','Error al guardar el archivo');
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('enlacemandado','El enlace ha sido mandado para moderar');
					}
				}
			}
		} else {

			if($objArchivadorElectronico->insertarArchivoElectronico($post['titulo'], $post['descripcion'], Usuario::getIdUser(true), Usuario::getIdCurso()))
			{


				$idArch = $objArchivadorElectronico->obtenerUltimoIdInsertado();

				if(isset($_FILES['archivoelectronico']['tmp_name']) && !empty($_FILES['archivoelectronico']['tmp_name']))
				{
					if ($_FILES['archivoelectronico']['error'] == 0)
					{
						//Compruebo si el tipo de archivo es valido
						if($archivosNoValidos = Fichero::validarTipos($_FILES['archivoelectronico']['name'], $_FILES['archivoelectronico']['type'], $extensionesAdjunto))
						{
							$extension =  Texto::obtenerExtension($_FILES['archivoelectronico']['name']);
							$directorio = 'archivos/archivador_electronico/';

							move_uploaded_file($_FILES["archivoelectronico"]["tmp_name"],$directorio.$idArch.'.'.$extension);
							if(file_exists($directorio.$idArch.'.'.$extension))
							{
								$archivoElectronico = $directorio . $idArch . '.' . $extension;
								if($objArchivadorElectronico->insertarArchivoAdjuntoElectronico($archivoElectronico, Texto::clearFilename($_FILES['archivoelectronico']['name']), $idArch))
								{
									Alerta::guardarMensajeInfo('enlacepublicado','El enlace ha sido publicado');
								}
								else
								{
									Alerta::guardarMensajeInfo('archivonoinsertado','El archivo no pudo ser insertado');
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('errormoverarchivo','Error al mover el archivo');
							}
						}
						else
						{
							Alerta::guardarMensajeInfo('errorguardar','Error al guardar el archivo');
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('enlacepublicado','El enlace ha sido publicado');
					}
				}
			}
		}
	}

		Url::redirect('aula/archivador_electronico');
}
	else
	{
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'archivador_nuevo');
	}

