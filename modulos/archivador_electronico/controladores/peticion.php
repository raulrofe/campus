<?php
$get = Peticion::obtenerGet();

//var_dump($get);exit;

$objArchivadorElectronico = new ArchivadorElectronico();

if(isset($get['idArchivado']) && is_numeric($get['idArchivado']))
{
	$row = $objArchivadorElectronico->obtenerArchivoEdicion($get['idArchivado']);
	
	if($row->num_rows == 1)
	{
		$row = $row->fetch_object();
		
		if(isset($get['accion']))
		{
			if($get['accion'] == 'aceptar')
			{
				if($objArchivadorElectronico->aceptarPeticion($get['idArchivado']))	
				{
					// aceptar nuevo enlace
					if($row->eliminar == 0 && $row->editar == 0)
					{
						if($objArchivadorElectronico->insertarArchivoElectronico($row->titulo, $row->descripcion, $row->idusuario, $row->idcurso))
						{
							$idArch = $objArchivadorElectronico->obtenerUltimoIdInsertado();
						
							// movemos el archivo adjunto
							if(!empty($row->archivo_adjunto))
							{
								$extension = Fichero::obtenerExtension($row->archivo_adjunto);
								$directorioNuevo = 'archivos/archivador_electronico/';
								if(file_exists($row->archivo_adjunto))
								{
									$archivoNuevo = $directorioNuevo . $idArch . '.' . $extension;
									
									copy($row->archivo_adjunto, $archivoNuevo);
									@chmod($row->archivo_adjunto, 0777);
									unlink($row->archivo_adjunto);
									
									if(file_exists($archivoNuevo))
									{
										if($objArchivadorElectronico->insertarArchivoAdjuntoElectronico($archivoNuevo, $row->archivo_adjunto_nombre, $idArch))
										{
											Alerta::guardarMensajeInfo('enlaceaprobado','El enlace ha sido aprobado');
										}
										else
										{
											Alerta::guardarMensajeInfo('archivonoaprobado','El archivo no pudo ser aprobado');
										}
									}
								}
							}
							else
							{
								Alerta::guardarMensajeInfo('peticionaceptada','La petición ha sido aceptada');
							}
						}
					}
					// editar enlace
					else if($row->editar == 1)
					{
						if($objArchivadorElectronico->editarArchivo($row->idarchivador_electronico, $row->titulo, $row->descripcion))
						{							
							$extension = Fichero::obtenerExtension($row->archivo_adjunto);
							$directorioNuevo = 'archivos/archivador_electronico/';
											
							// eliminar archivo adjunto actual
							if(!isset($row->archivo_adjunto))
							{
								$row2 = $objArchivadorElectronico->obtenerArchivo($row->idarchivador_electronico);
								if($row2->num_rows == 1)
								{
									$row2 = $row2->fetch_object();
									
									if($objArchivadorElectronico->insertarArchivoAdjuntoElectronico(null, null, $row->idarchivador_electronico))
									{
										if(file_exists($row2->archivo_adjunto))
										{
											@chmod($row2->archivo_adjunto, 0777);
											unlink($row2->archivo_adjunto);
								
											Alerta::guardarMensajeInfo('archivoeditado','El archivo ha sido editado');
										}
									}
								}
							}
							
							// cargar nuevo archivo
							else if(file_exists($row->archivo_adjunto))
							{
								$archivoNuevo = $directorioNuevo . $row->idarchivador_electronico . '.' . $extension;
									
								copy($row->archivo_adjunto, $archivoNuevo);
								@chmod($row->archivo_adjunto, 0777);
								unlink($row->archivo_adjunto);
									
								if(file_exists($archivoNuevo))
								{
									if($objArchivadorElectronico->insertarArchivoAdjuntoElectronico($archivoNuevo, $row->archivo_adjunto_nombre, $row->idarchivador_electronico))
									{
										Alerta::guardarMensajeInfo('enlaceeditado','El enlace ha sido editado');
									}
									else
									{
										Alerta::guardarMensajeInfo('archivonoeditado','El archivo no pudo ser editado');
									}
								}
							}
						}
					}
					// eliminar enlace
					else if($row->eliminar == 1)
					{
						if($objArchivadorElectronico->eliminarArchivo($row->idarchivador_electronico))
						{
							// elimina archivo antiguo
							if(!empty($row->archivo_adjunto) && file_exists($row->archivo_adjunto))
							{
								@chmod($row->archivo_adjunto, 0777);
								@unlink($row->archivo_adjunto);
							}
							
							Alerta::guardarMensajeInfo('La petici&oacute;n ha sido aceptada');
						}
					}
				}
			}
			else if ($get['accion'] == 'denegar')
			{
				if($objArchivadorElectronico->rechazarPeticion($get['idArchivado']))
				{
					Alerta::guardarMensajeInfo('peticionrechazada','La petición ha sido rechazada');	
				}
			}
		}
	}
}

Url::redirect('aula/archivador_electronico/administrar');