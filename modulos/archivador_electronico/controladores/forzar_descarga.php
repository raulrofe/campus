<?php
$get = Peticion::obtenerGet();

require 'configMimes.php';

if(isset($get['idArchivo']) && is_numeric($get['idArchivo']))
{
	$objArchivadorElectronico = new ArchivadorElectronico();
	
	if(isset($get['idArchivo']) && is_numeric($get['idArchivo']))
	{
		$row = $objArchivadorElectronico->obtenerArchivo($get['idArchivo'], Usuario::getIdCurso());
		
		if($row->num_rows == 1)
		{
			$row = $row->fetch_object();
			
			if(!empty($row->archivo_adjunto))
			{
				$extension = Fichero::obtenerExtension($row->archivo_adjunto);
				if(file_exists($row->archivo_adjunto) && isset($mimes[$extension]))
				{
					header("Content-disposition: attachment; filename=" . $row->archivo_adjunto_nombre);
					header("Content-type: application/octet-stream");
					readfile($row->archivo_adjunto);
				}
				else
				{
					Url::lanzar404();
				}
			}
			else
			{
				Url::lanzar404();
			}
		}
		else
		{
			Url::lanzar404();			
		}
	}
	else
	{
		Url::lanzar404();			
	}
}
else
{
	Url::lanzar404();
}