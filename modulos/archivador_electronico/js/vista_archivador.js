function changeTab(num)
{
	$('#menuContenidosCurso > div').removeClass('blanco');
	$('#archiveGuia').addClass('hide');
	$('#archiveTemario').addClass('hide');
	$('#archiveScorm').addClass('hide');
		
	if(num == 1)
	{
		$('#archiveGuia').removeClass('hide');
		$('#navArchiveGuia').addClass('blanco');
	}
	else if(num == 2)
	{
		$('#archiveTemario').removeClass('hide');
		$('#navArchiveTemario').addClass('blanco');
	}
	else if(num == 3)
	{
		$('#archiveScorm').removeClass('hide');
		$('#navArchiveScorm').addClass('blanco');	
	}
}