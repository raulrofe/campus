$("#frmEditarEnlaceInteres").validate({
	errorElement: "div",
	messages: {
		titulo: {
			required: 'Introduce un t&iacute;tulo'
		}
	},
	rules: {
		titulo : {
			required  : true
		}
	}
});
