<br/>
<div id="enlacesInteres">

 	<div class="tituloMenuEnlaceInteres">
 		<div class="subtitleModulo" data-translate-html="archivador.editar">
			Editar enlace de inter&eacute;s
		</div>
 	</div>
	<br/>

	<form id="frmEditarEnlaceInteres" name='' method='post' action='' enctype='multipart/form-data'>
		<div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.titulo">
					T&iacute;tulo
				</div>
				<div class='campofrm'><input type='text' name='titulo' size='65' value='<?php echo $dataArchivo->titulo ?>'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.descripcion">
					Descripci&oacute;n
				</div>
				<div class='campofrm'><textarea id="frmEditarEnlaceInteres_1" name='descripcion' rows='12' cols='49'><?php echo $dataArchivo->descripcion ?></textarea></div>
			</div>
			<?php if(!empty($dataArchivo->archivo_adjunto)):?>
				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="formulario.adjunto">
						Archivo Adjunto
					</div>
					<div class='campofrm'>
						<a href="<?php echo $dataArchivo->archivo_adjunto?>" target="_blank"></a>
						<input type="checkbox" name="eliminarArchivo" value="1">
						<span data-translate-html="formulario.eliminararchivo">
							Eliminar archivo actual
						</span>
					</div>
				</div>
			<?php endif ?>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.archivo">
					Archivo 
				</div>
				<div class='campofrm'>
					<input type='file' name='archivoelectronico' size='30'/>
				</div>
			</div>
		</div>

		<div><input type='submit' data-translate-value="formulario.enviar" value='Enviar' class="width100"/></div>
		<input type="hidden" name="idArchivo" value="<?php echo $dataArchivo->idarchivador_electronico ?>" />
	</form>
</div>

<script type="text/javascript" src="js-archivador_electronico-editar_enlace.js"></script>
<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "#frmEditarEnlaceInteres_1",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
	});
</script>