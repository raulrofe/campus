<!-- Comienza la vista diseño -->
<div id="archivador">

<?php require DIRNAME(__FILE__) . '/intro_archivador_electronico.php';?>

<div class="cabeceraLibrary">
	<div class="titleLibrary"><?php echo $_SESSION['nombrecurso']; ?></div>

	<div class="menuVista2" style="float:right;">
		<div class="vista1" >
			<a href="aula/archivador/listado">
				<img src="imagenes/archivador/vista-clasica.png" data-translate-title="archivador.listado" alt="Listado"/>
			</a>
		</div>
		<div class="vista2" >
			<a href="#" onclick="return false;">
				<img src="imagenes/archivador/vista-nueva.png" data-translate-title="archivador.clasica" alt="Vista Clásica"/>
			</a>
		</div>
	</div>

</div>
<div class="topLibrary"></div>
<div class="clear"></div>

<div>

<div class="menuLibrary">
	<div class="estante">
		<span data-translate-html="archivador.guias">Gu&iacute;as</span>
	</div>
	<div class="estante">
		<span data-translate-html="archivador.temario">Temario Pdf</span>
	</div>
	<div class="estante">
		<span data-translate-html="archivador.scorm">SCORM</span>
	</div>
</div>

	<div class="library">
		<div>

			<div class="book">
				<a class=Ntooltip href="archivos/guias_didacticas/<?php echo Texto::clearFilename($_SESSION['nombrecurso']);?>.pdf" target="_blank" style="display:block">
					 <!-- Gu&iacute;a did&aacute;ctica -->
					<img src="archivos/cursos/1/thumbnail/guia-didactica.png" alt="" />
					<span class="arrow_box">
						<p data-translate-html="archivador.scorm">
							Gu&iacute;a did&aacute;ctica
						</p>
					</span>
				</a>
			</div>
			<!--
			<div class="book">
				<a class=Ntooltip href="archivos/guida-del-usuario.pdf" target="_blank" style="display:block">
					Gu&iacute;a de usuario de la Plataforma
					<span class="arrow_box">
						<p>Gu&iacute;a de usuario de la Plataforma</p>
					</span>
				</a>
			</div>
			 -->
			<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>




		<?php $numero_de_modulo = 1; ?>
		<?php while($f2 = mysqli_fetch_assoc($registros_modulos2)):?>
				<?php //echo strtoupper($f2['nombre']) ?>
				<?php
					$mi_modulo->set_modulo($f2['idmodulo']);
					$mi_modulo->set_categoria_archivo(1);
					$registros_archivos2 = $mi_modulo->archivos_modulo();

					$nArchivos = mysqli_num_rows($registros_archivos2);
				?>
				<div>
					<?php if(mysqli_num_rows($registros_archivos2) > 0):?>
						<?php while ($f = mysqli_fetch_assoc($registros_archivos2)):?>
							<?php if(file_exists($f['enlace_archivo'])):?>
								<?php
								$peso_archivo = filesize($f['enlace_archivo'])/1024;
								$peso_archivo = $peso_archivo/1024;
								$peso_archivo = round($peso_archivo,2);
								$fecha = Fecha::invertir_fecha($f['f_creacion']);
								?>

								<div class="book">
									<!--  <a class=Ntooltip href='http://issuu.com/susanaabc/docs/tutorial_phpwebq?mode=window&backgroundColor=%23222222' target='_blank'> -->
									<a class="Ntooltip" href='aula/archivador/forzar_descarga_muestra/<?php echo $f['idarchivo_temario'] ?>' target='_blank'>
									<!-- <a class="Ntooltip" href='aula/archivador/<?php echo $f['idarchivo_temario'] ?>' target='_blank'>  -->
										<?php //echo strtoupper($f['titulo']); ?>
										<img src="<?php echo $f['imagen_archivo']?>" alt="" />
										<span class="arrow_box">
											<p><?php echo $f['titulo']; ?></p>
											<p><?php echo $peso_archivo ?> Mb</p>
											<?php if(!empty($f['descripcion'])):?>
												<div style='font-size:0.8em;margin-top:-8px;width:87%;' class='t_justify'>(<?php echo $f['descripcion'] ?>)</div>
											<?php endif ?>
										</span>
									</a>
								</div>

							<?php else: ?>
								<!--
								<div class='listado_tabla'>
									<div class='titulo_tabla'><img src='imagenes/archive_pdf.png' alt='' style='vertical-align:middle;'/><?php //echo strtoupper($f['titulo']) ?></div>
									<div class='objeto_tabla'>? Mb</div>
									<div class='clear'><br/></div>
										<?php //if(!empty($f['descripcion'])): ?>
											<div style='font-size:0.8em;margin-top:-8px;width:87%;' class='t_justify'>El archivo fue borrado</div>
										<?php //endif ?>
								</div>
								 -->
							<?php endif ?>
						<?php endwhile ?>
					<?php else: ?>
						<div class="listado_tabla" data-translate-html="archivador.noarchivo">
							No existe ningun archivo para este modulo
						</div>
					<?php endif ?>
					</div>
				<?php $numero_de_modulo ++; ?>
			<?php endwhile ?>

			<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

			<!-- Bucle para mostrar los contenidos con smcorm -->


			<?php if($scorms->num_rows > 0):?>
				<div class="listarElementos">
					<?php while($item = $scorms->fetch_object()):?>
						<?php if(file_exists(PATH_ROOT . 'archivos/scorm/' . $item->idscorm . '/sco_' . $item->idscorm_folder . '/default.html')):?>
							<div class="book2">
								<a class=Ntooltip href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item->idscorm?>', 600, 810, false); return false;" style="display:block">
									<img src="<?php echo $item->imagen_scorm ?>" alt=""/>
									<span class="arrow_box">
										<p data-translate-html="archivador.interactivo">
											Contenido Interactivo
										</p>
										<p><?php echo $item->titulo; ?></p>
										<?php if(!empty($item->descripcion)):?>
											<p style='font-size:0.8em;margin-top:-8px;width:87%;' class='t_justify'>(<?php echo $item->descripcion ?>)</p>
										<?php endif ?>
									</span>
								</a>
							</div>
						<?php endif ?>
					<?php endwhile;?>
				</div>
			<?php else:?>
			<?php endif;?>

	</div>
</div>


</div>
</div>