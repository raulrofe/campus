<br/>
<div id="enlacesInteres">

 	<div class="tituloMenuEnlaceInteres">
 		<div class="subtitleModulo" data-translate-html="archivador.insertar">
			Insertar enlace de inter&eacute;s
		</div>
 	</div>
 	<br/>
	<form id="frmNuevoEnlaceInteres" name='' method='post' action='' enctype='multipart/form-data'>
		<div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.titulo">
					T&iacute;tulo
				</div>
				<div class='campofrm'><input type='text' name='titulo' size='65'/></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.descripcion">
					Descripci&oacute;n
				</div>
				<div class='campofrm'><textarea id="frmNuevoEnlaceInteres_1" name="descripcion" rows='12' cols='49' style="border:0"></textarea></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="formulario.archivo">
					Archivo
				</div>
				<div class='campofrm'>
					<input type='file' name='archivoelectronico' size='30'/>
				</div>
			</div>
			<div class='filafrm'>

			</div>
		</div>

		<div><input type='submit' data-translate-value="formulario.enviar" value='Enviar' class="width100"/></div>
	</form>
</div>

<script type="text/javascript" src="js-archivador_electronico-nuevo_enlace.js"></script>

<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link",
	});
</script>