<div id="enlacesInteres">
	<br/>
	<div class="tituloMenuEnlaceInteres">
		<div class="subtitleModulo fleft" data-translate-html="archivador.aportaciones">
			Admistrar aportaciones Enlaces de inter&eacute;s
		</div>
		<div class="fright menu_interno">
			<span class="elemento_menuinterno">
				<a href="aula/archivador_electronico" onclick="return false;" data-translate-html="archivador.publicadas">
					Publicadas
				</a>
			</span>
			<?php if (Usuario::compareProfile('tutor')): ?>
				<span class="elemento_menuinterno">
					<a href="aula/archivador_electronico/administrar" onclick="return false;" data-translate-html="archivador.administrar">
						Administrar
					</a>
				</span>	
			<?php endif; ?>
		</div>
		<div class="clear"></div>
	</div>

	<br/>

	<?php if (!empty($archivador)): ?>
		<?php foreach ($archivador as $data): ?>
			<?php foreach ($data['curso'] as $curso): ?>
				<div class="tituloAdministrar t_center"><?php echo $curso; ?></div><br/>
				<?php for ($i = 0; $i <= count($data['titulo']) - 1; $i++): ?>
					<div class="elemento">
						<?php if (isset($data['titulo'][$i])): ?>
							<div class="negrita"><?php echo $data['titulo'][$i] ?></div>
							<div><?php echo $data['descripcion'][$i] ?></div>
							<?php if (!empty($data['archivo'][$i])): ?>
								<div>
									<img src="imagenes/correos/clip_adjunto.png" alt="archivo adjunto" />
									<a href="aula/archivador_electronico/admin/forzar_descarga/<?php echo $data['id'][$i] ?>" target="_blank">Archivo Adjunto</a>
								</div>
							<?php endif ?>
						<?php else: ?>
							<div data-translate-html="archivador.peticion">
								Petici&oacute;n de eliminar archivo
							</div>
						<?php endif; ?>
						<div class="elementoPie">
							<div class="elementoFecha negrita"><?php echo Fecha::obtenerFechaFormateada($data['fecha'][$i], true); ?></div>
							<div class="fleft elementoFecha negrita">&nbsp;&nbsp; <?php echo ucwords($data['usuario'][$i]); ?></div>
							<div class="fright">
								<a class="botonAdminEnlaceInteres" href="#" <?php echo Alerta::alertConfirmOnClick('quieresaceptarlo','¿Realmente quieres aceptarlo?', 'aula/archivador_electronico/peticion/aceptar/' . $data['id'][$i]) ?>
								   data-translate-html="formulario.aceptar">
									Aceptar
								</a>
								<a class="botonAdminEnlaceInteres" href="#" <?php echo Alerta::alertConfirmOnClick('quieresdenegarlo','¿Realmente quieres denegarlo?', 'aula/archivador_electronico/peticion/denegar/' . $data['id'][$i]) ?>
								   data-translate-html="formulario.denegar">
									Denegar
								</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				<?php endfor; ?>		
			<?php endforeach; ?>	
		<?php endforeach; ?>
	<?php else: ?>
		<div class="t_center" data-translate-html="archivador.noaportaciones">
			No existen aportaciones de los alumnos para administrar
		</div>
	<?php endif; ?>
</div>

