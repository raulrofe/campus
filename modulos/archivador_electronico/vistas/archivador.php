 <div id="enlacesInteres">
 	 <br/>
	 <?php require DIRNAME(__FILE__) . '/intro_archivador_electronico.php';?>	
 	
 	<div class="tituloMenuEnlaceInteres">
 		<div class="subtitleModulo fleft" data-translate-html="archivador.enlaces">
			Enlaces
		</div>
		 	<?php if(!Usuario::compareProfile('supervisor')):?>
			 	<div class="fright menu_interno">
			 			<span class="elemento_menuinterno">
							<a href="aula/archivador_electronico/nuevo" onclick="return false;"
							    data-translate-html="formulario.nuevo">
								Nuevo
							</a>
						</span>
			 		<?php if(Usuario::compareProfile('tutor')):?>
			 			<span class="elemento_menuinterno">
							<a href="aula/archivador_electronico/administrar" onclick="return false;"
							   data-translate-html="archivador.administrar">
								Administrar
							</a>
						</span>	
			 		<?php endif; ?>
			 	</div> 	
			 <?php endif; ?>
 	</div>

 	
 	<div class="clear"></div>
 	<br/>
 
 
 	<div class="listarElementos">
 		<?php if($archivos->num_rows > 0):?>
 			<?php while($archivo = $archivos->fetch_object()):?>
	 			<div class="elemento">
		 			<p class="elementoTitulo"><?php echo Texto::textoPlano($archivo->titulo);?></p>
		 			<p class="elementoContenido"><?php echo str_replace('<a', '<a style="color:blue;" target="_blank" ', $archivo->descripcion);?></p>
		 			<?php if(!empty($archivo->archivo_adjunto)):?>
		 				<p>
			 				<img src="imagenes/correos/clip_adjunto.png" alt="archivo adjunto" />
			 				<a href="aula/archivador_electronico/forzar_descarga/<?php echo $archivo->idarchivador_electronico?>" target="_blank"
							   data-translate-html="formulario.adjunto">
								Archivo Adjunto
							</a>
		 				</p>
		 			<?php endif ?>
		 			
		 			<div class="elementoPie">
		 				<p class="elementoFecha">
		 					<?php if(!empty($archivo->nombrec)):?>
		 						<?php echo $archivo->nombrec; ?>
		 					<?php else: ?>
		 						<?php echo $archivo->nombrecAl; ?>
		 					<?php endif ?>
		 					(<?php echo Fecha::obtenerFechaFormateada($archivo->fecha, true); ?>)
		 				</p>
		 				<?php if($idUsuario == $archivo->idusuario || !Usuario::compareProfile('alumno')):?>
		 				<p class="fright">
		 					<span class="optionEnlaces">
			 					<a rel="tooltip" title="Editar enlace de inter&eacute;s" href="aula/archivador_electronico/editar/<?php echo $archivo->idarchivador_electronico;?>">
			 						<img src="imagenes/options/edit.png" alt="editar" />
			 					</a>
		 					</span>
		 					<span  class="optionEnlaces">
			 					<a rel="tooltip" href="#" <?php echo Alerta::alertConfirmOnClick('eliminarenlace','¿Realmente quieres eliminar este enlace de interés?', 'aula/archivador_electronico/eliminar/' . $archivo->idarchivador_electronico)?> 
								   data-translate-title="archivador.eliminar" title="Eliminar enlace de inter&eacute;s">
			 						<img src="imagenes/options/delete.png" alt="eliminar" />
			 					</a>
		 					</span>
		 				</p>
		 				<?php endif ?>
		 			</div>
		 			<div class="clear"></div>
		 		</div>	
 			<?php endwhile;?>
 		<?php else:?>
 			<div class="t_center">
				<b data-translate-html="archivador.noenlace">
					No hay enlaces
				</b>
			</div>
 		<?php endif; ?>
 	</div>	
 </div>