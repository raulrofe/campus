<div>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
	<div class="popupIntro">
		<div class='subtitle t_center'>
			<img src="imagenes/enlace_interes/enlaces-interes.png" data-translate-title="archivador.enlaces" alt="Enlaces de interes" style="vertical-align:middle;width:40px;height:40px;margin-right: 5px;"/>
			<span data-translate-html="archivador.titulo">
				Archivador electr&oacute;nico
			</span>
		</div>
		<br/>
 		<p class='t_justify' data-translate-html="archivador.descripcion2">
			El aula virtual cuenta con un archivador electr&oacute;nico donde se encuentran almacenados ficheros directamente relacionados con los contenidos formativos del m&oacute;dulo.
		</p>
		<br/>
		<p class='t_justify' data-translate-html="archivador.descripcion3">
			Esta secci&oacute;n le ofrece la posibilidad de cargar nuevos ficheros (con extensi&oacute;n m&aacute;xima de 1MB) y eliminar aqu&eacute;llos que han quedado obsoletos.
		</p>
	</div>
	<?php else: ?>
	<div class="popupIntro">
		<div class='subtitle t_center'>
			<img src="imagenes/archivador/archivador.png" alt="" style="vertical-align:middle;"/>
			<span data-translate-html="archivador.titulo">
				Archivador electr&oacute;nico
			</span>
		</div>
		<br/>
 		<p class='t_justify' data-translate-html="archivador.descripcion4">
			El archivador electr&oacute;nico contiene material complementario al curso, en concreto se trata de enlaces y ficheros que puede consultar para ampliar sus conocimientos sobre la tem&aacute;tica del curso.
		</p>
		<br/>
		<!-- <p class='t_justify'>Esta secci&oacute;n le ofrece la posibilidad de descargar estos ficheros, para consultar su contenido con mayor detenimiento.</p> -->
	</div>
	<?php endif;?>
</div>
