if($('#alumnoActivo').size() == 1)
{
	$("#frm_datos_personales").validate({
		errorElement: "div",
		messages: {
			anombrec: {
				required: 'Introduce tu nombre',
				minlength : 'El nombre debe contener al menos 3 caracteres'
			},
			aapellidos: {
				required: 'Introduce tus apellidos',
				minlength : 'Los apellidos deben contener al menos 3 caracteres'
			},
			aemail : {
				required  : 'Introduce tu email',
				email     : 'El email no es v&aacute;lido'
			},
			atelefono : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
			},
			atelefono2 : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
			},
			acp : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
			},
			ausuario: {
				required: 'Introduce tu apodo',
				minlength : 'El apodo debe contener al menos 4 caracteres'
			},
			af_nacimiento : {
				date : 'La fecha de nacimiento no es v&aacute;lida',
				minlength : 'La fecha debe tener el formato 00/00/0000'
			}
		},
		rules: {
			anombrec : {
				required  : true,
				minlength : 3
			},
			aapellidos : {
				required  : true,
				minlength : 3
			},
			aemail : {
				required  : true,
				email     : true
			},
			atelefono : {
				digits    : true,
				minlength : 9
			},
			atelefono2 : {
				digits    : true,
				minlength : 9
			},
			acp : {
				digits    : true,
				minlength : 5
			},
			ausuario : {
				required  : true,
				minlength : 3
			},
			af_nacimiento : {
				//date : true,
				minlength : 10
			}
		}
	});
}
else
{
	$("#frm_datos_personales").validate({
		errorElement: "div",
		messages: {
			anombrec: {
				required: 'Introduce tu nombre',
				minlength : 'El nombre debe contener al menos 3 caracteres'
			},
			aemail : {
				required  : 'Introduce tu email',
				email     : 'El email no es v&aacute;lido'
			},
			atelefono : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El tel&eacute;fono debe contener 9 n&uacute;meros'
			},
			acp : {
				digits    : 'S&oacute;lo se admiten numeros',
				minlength : 'El c&oacute;digo postal debe contener 5 d&iacute;gitos'
			},
			aalias: {
				required: 'Introduce tu apodo',
				minlength : 'El apodo debe contener al menos 3 caracteres'
			}
		},
		rules: {
			anombrec : {
				required  : true,
				minlength : 3
			},
			aemail : {
				required  : true,
				email     : true
			},
			atelefono : {
				digits    : true,
				minlength : 9
			},
			acp : {
				digits    : true,
				minlength : 5
			},
			aalias : {
				required  : true,
				minlength : 3
			}
		}
	});
}

$('#frm_datos_personales_imagen input[type="file"]').change(function()
{
	// actual imagen
	var currentSrc = $('#frm_datos_personales_imagen img').attr('src');
	
	// laoder
	$('#frm_datos_personales_imagen img').attr('src', 'imagenes/popupModalLoader.gif');
	
	$('#frm_datos_personales_imagen').attr('target', 'frm_datos_personales_imagen_form_iframe');
	$('#frm_datos_personales_imagen_form_iframe').load(function()
	{
		setTimeout(function()
		{
			var iframeHtml = $('#frm_datos_personales_imagen_form_iframe').contents().find('body').html();
			
			if(iframeHtml != undefined && iframeHtml != null && iframeHtml != '')
			{
				$('#frm_datos_personales_imagen img').attr('src', iframeHtml);
			}
			// si da error, reestablecer img
			else
			{
				$('#frm_datos_personales_imagen img').attr('src', currentSrc);
			}
			
			$('#popupModal_datos_personales').find('.popupModalHeaderLoader').addClass('hide');
			
			$('#frm_datos_personales_imagen_borrar').removeClass('hide');
		}, 200);
	});
	
	$('#frm_datos_personales_imagen').submit();
});

function changeTab(num)
{
	$('#navDatosPersonales > div').removeClass('blanco');
	$('#contenidoDatosPersonales > div').addClass('hide');
	
	$('#tab' + num).removeClass('hide');
	$('#nav' + num).addClass('blanco');
}

$(function()
{
	if($.browser.msie && $.browser.version == '7.0')
	{
		$('.upload input').attr('size', 1);
	}
});