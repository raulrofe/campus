<?php
class ModeloDatosPersonales extends modeloExtend
{
	public function insertarAlumnoPeticion($idalumno, $anombrec, $aapellidos, $adni, $af_nacimiento, $atelefono, $atelefono2, $adireccion, $acp, $alocalidad, $aprovincia, $apais, $ausuario, $alugar_nacimiento, $aemail, $aidsexo)
	{
		$sql = 'INSERT INTO alumnos_peticiones (idalumnos, nombre, apellidos, dni, f_nacimiento, lugar_nacimiento, telefono, telefono_secundario, direccion, cp, poblacion, provincia, pais, email, usuario, idsexo) VALUES' .
		'(' . $idalumno . ', "' . $anombrec . '", "' . $aapellidos . '", "' . $adni . '", "' . $af_nacimiento . '",' .
		'"' . $alugar_nacimiento . '", "' . $atelefono . '",' .
		'"' . $atelefono2 . '", "' . $adireccion . '", "' . $acp . '",' . 
		'"' . $alocalidad . '", "' . $aprovincia . '", "' . $apais . '", "' . $aemail . '",' .
		'"' . $ausuario . '", ' . $aidsexo . ')';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarAlumnoPeticion($idalumno, $anombrec, $aapellidos, $adni, $af_nacimiento, $atelefono, $atelefono2, $adireccion, $acp, $alocalidad, $aprovincia, $apais, $ausuario, $alugar_nacimiento, $aemail, $aidsexo)
	{
		$sql = 'UPDATE alumnos_peticiones SET ' .
		'nombre="' . $anombrec . '", apellidos="' . $aapellidos . '", dni="' . $adni . '", f_nacimiento="' . $af_nacimiento . '",' .
		'lugar_nacimiento="' . $alugar_nacimiento . '", telefono="' . $atelefono . '",' .
		'telefono_secundario="' . $atelefono2 . '", direccion="' . $adireccion . '", cp="' . $acp . '",' . 
		'poblacion="' . $alocalidad . '", provincia="' . $aprovincia . '", pais="' . $apais . '", email="' . $aemail . '",' .
		'usuario="' . $ausuario . '", idsexo = ' . $aidsexo . 
		' where idalumnos=' . $idalumno;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAlumnoPeticion($idAlumno)
	{
		$sql = 'SELECT a.idalumnos, a.foto, ap.apellidos, ap.nombre, ap.dni, ap.f_nacimiento, ap.lugar_nacimiento, ap.idsexo, ap.telefono, ap.telefono_secundario,' .
		' ap.direccion, ap.cp, ap.poblacion, ap.provincia, ap.pais, ap.email, ap.usuario' .
		' FROM alumnos_peticiones AS ap LEFT JOIN alumnos AS a ON a.idalumnos = ap.idalumnos WHERE a.idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarNotifiaciones($idAlumno, $notifiCorreo)
	{
		$sql = 'UPDATE alumnos SET notifi_correo = ' . $notifiCorreo . ' WHERE idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}