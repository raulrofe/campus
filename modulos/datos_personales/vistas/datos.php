<div id="datos_personales">
	<?php if(!Usuario::compareProfile('alumno')):?>
		<div class='subtitle t_center'>
			<img src="imagenes/datos_personales/person-file.png" height="49px" class="imgdatospersonales"/>
			<span data-translate-html="usuario.tab1">
				DATOS PERSONALES
			</span>
		</div>
		<br/>
	<?php else: ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/datos_personales/person-file.png" height="49px" style="vertical-align:middle;"/>
				<span data-translate-html="usuario.tab1">
					DATOS PERSONALES
				</span>
			</div>
			<p class='t_justify' data-translate-html="usuario.descripcion">
				Desde este entorno usted podr&aacute; modificar su configuraci&oacute;n personal y se enviar&aacute; una solicitud al administrador 
				para que se produzcan dichos cambios en la plataforma.
			</p>
		</div>
	<?php endif;?>
	
	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>
		<div class="fleft panelTabMenu" id="navDatosPersonales">
			<div id="nav1" class="blanco redondearBorde" style="margin-right:-3px;">
				<a href="datos-personales">
					<img src="imagenes/datos_personales/datos-personales.png" />
					<span data-translate-html="usuario.tab1">
						Datos personales
					</span>
				</a>
			</div>
	
			<?php if(!Usuario::compareProfile('supervisor')):?>
				<div id="nav2" class="redondearBorde">
					<a href="datos-personales/otros">
						<img src="imagenes/datos_personales/datos-personales-password.png" />
						<span data-translate-html="usuario.tab2">
							contrase&ntilde;a
						</span>
					</a>
				</div>
			<?php endif; ?> 
			
			<?php if(Usuario::compareProfile('alumno')):?>
				<div id="nav3" class="redondearBorde">
					<a href="datos-personales/notificaciones">
						<img src="imagenes/datos_personales/datos-personales-notificacion.png" />
						<span data-translate-html="usuario.tab3">
							Notificaciones
						</span>
					</a>
				</div>	
			<?php endif;?>	
		</div>
	</div>


	
	<!-- ************************************************************************************************************* -->
	<!-- CONTENIDO DE PESTANAS -->
	
	<div class='fleft panelTabContent' id="contenidoDatosPersonales">
	
		<!-- TAB 1 : CAMBIAR DATOS PERSONALES -->
		<div id="tab1">
			<p class="tituloTabs" data-translate-html="usuario.cambiardatos">
				Cambiar datos personales
			</p>
			<div>
				<div class="fleft">
					<form id="frm_datos_personales" action='datos-personales/actualizar' method='post' enctype="multipart/form-data" class='fleft'>
						<div class="camposDatosPersonales">
						<?php if($_SESSION['perfil'] == 'alumno'):?>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.nombre">
									Nombre
								</div>
								<div class='campofrm'>
									<input type='text' name='anombrec' size='60'  maxlength="100" value='<?php echo $f['nombre'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.apellidos">
									Apellidos
								</div>
								<div class='campofrm'>
									<input type='text' name='aapellidos' size='60' maxlength="100" value='<?php echo $f['apellidos'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.email">
									E-mail
								</div>

								<div class='campofrm'>
									<input type='text' name='aemail' size='60' value='<?php echo $f['email'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.dni">
									DNI
								</div>
								<div class='campofrm'>
									<input type='text' name='adni' size='60' maxlength="10" value='<?php echo $f['dni'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.tab1">
									Sexo
								</div>

								<div class='campofrm'>
									<select name="aidsexo">
										<option value="1" data-translate-html="sexo.hombre">
											Hombre
										</option>
										<option value="2" <?php if($f['idsexo'] == 2) echo 'selected="selected"'?> data-translate-html="sexo.mujer">
											Mujer
										</option>
									</select>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.fnacimiento">
									Fecha nacimiento
								</div>

								<div class='campofrm'>
									<input type='text' name='af_nacimiento' size='60' maxlength="10" value='<?php if($f['f_nacimiento'] != '0000-00-00') echo date('d/m/Y', strtotime($f['f_nacimiento']));?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.lnacimiento">
									Lugar nacimiento
								</div>
								<div class='campofrm'>
									<input type='text' name='alugar_nacimiento' size='60' value='<?php echo $f['lugar_nacimiento'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.telefono">
									Tel&eacute;fono
								</div>
								<div class='campofrm'>
									<input type='text' name='atelefono' size='60' maxlength='9' value='<?php if(!empty($f['telefono'])) echo $f['telefono'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.telefono2">
									Tel&eacute;fono secundario
								</div>
								<div class='campofrm'>
									<input type='text' name='atelefono2' size='60' maxlength='9' value='<?php if(!empty($f['telefono_secundario'])) echo $f['telefono_secundario'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.direccion">
									Direcci&oacute;n
								</div>
								<div class='campofrm'>
									<input type='text' name='adireccion' size='60' value='<?php echo $f['direccion'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.cp">
									C&oacute;digo postal
								</div>
								<div class='campofrm'>
									<input type='text' name='acp' size='60' maxlength='5' value='<?php echo $f['cp'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.localidad">
									Localidad
								</div>

								<div class='campofrm'>
									<input type='text' name='alocalidad' size='60' value='<?php echo $f['poblacion'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.provincia">
									Provincia
								</div>
								<div class='campofrm'>
									<input type='text' name='aprovincia' size='60' value='<?php echo $f['provincia'];?>'/>
								</div>
							</div>	

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.pais">
									Pa&iacute;s
								</div>
								<div class='campofrm'>
									<input type='text' name='apais' size='60' value='<?php echo $f['pais'];?>'/>
								</div>
							</div>	

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.usuario">
									Usuario
								</div>
								<div class='campofrm'>
									<input type='text' name='ausuario' size='60' value='<?php echo $f['usuario'];?>'/>
								</div>
							</div>
							
							<span id="alumnoActivo"></span>

						<?php else: ?>	

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.nombrec">
									Nombre completo
								</div>
								<div class='campofrm'>
									<input id='anombrec' type='text' name='anombrec' maxlength='100' size='60' value='<?php echo $f['nombrec'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.email">
									E-mail
								</div>
								<div class='campofrm'>
									<input id='aemail' type='text' name='aemail' maxlength='255' size='60' value='<?php echo $f['email'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.telefono">
									Tel&eacute;fono
								</div>
								<div class='campofrm'>
									<input type='text' name='atelefono' size='60' maxlength='9' value='<?php if(!empty($f['telefono'])) echo $f['telefono'];?>' />
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.direccion">
									Direcci&oacute;n
								</div>
								<div class='campofrm'>
									<input type='text' name='adireccion' size='60' maxlength='255' value='<?php echo $f['direccion'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.cp">
									C&oacute;digo postal
								</div>
								<div class='campofrm'>
									<input type='text' name='acp' size='60' maxlength='5' value='<?php echo $f['cp'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.localidad">
									Localidad
								</div>

								<div class='campofrm'>
									<input type='text' name='alocalidad' size='60' maxlength='255' value='<?php echo $f['localidad'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.provincia">
									Provincia
								</div>
								<div class='campofrm'>
									<input type='text' name='aprovincia' size='60' maxlength='255' value='<?php echo $f['provincia'];?>'/>
								</div>
							</div>

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.pais">
									Pa&iacute;s
								</div>
								<div class='campofrm'>
									<input type='text' name='apais' size='60' maxlength='255' value='<?php echo $f['pais'];?>'/>
								</div>
							</div>		

							<div class='filafrm'>
								<div class='etiquetafrm' data-translate-html="usuario.usuario">
									Usuario
								</div>
								<div class='campofrm'>
									<input type='text' name='aalias' maxlength='30' size='60' value='<?php echo $f['alias'];?>'/>
								</div>
							</div>	

							<?php if(Usuario::compareProfile(array('supervisor'))):?>
								<div class='filafrm'>
									<div class='etiquetafrm' data-translate-html="usuario.password">
										Password
									</div>
									<div class='campofrm'>
										<input type='password' name='password' size='60' value='**********'/>
									</div>
								</div>	
							<?php endif; ?>
							
						<?php endif;?>

					</div>	
						<?php if(!Usuario::compareProfile(array('supervisor'))):?>
							<div class="clear t_left">
								<input type='submit' value='Solicitar cambio de mis datos' data-translate-value="usuario.solicitar"/>
							</div>	
						<?php endif; ?>
					</form>
				</div>
				<?php if(!Usuario::compareProfile(array('supervisor'))):?>

					<div class="fright">

						<form id="frm_datos_personales_imagen" action='datos-personales/imagen' method='post' enctype="multipart/form-data" class='fleft'>

							<div class=''>

								<img src="imagenes/fotos/<?php echo $f['foto']; ?> " style="border:2px dashed #999;padding:20px 27px;width:95px;"/>

								<br/>

								<div class='upload'>
									<div>
											<input type='file' name='imagen' size="30"/>
									</div>
								</div>

								<div id="frm_datos_personales_imagen_borrar" class="<?php if($f['foto'] == 'default.png') echo 'hide'?>">
									<input type="button" 
									<?php echo Alerta::alertConfirmOnClick('eliminarfoto','¿Realmente quieres eliminar tu foto de usuario?', 'datos-personales/foto/eliminar');?> value="Eliminar foto actual" data-translate-value="usuario.eliminarFoto"/>
								</div>
							</div>

							<iframe id="frm_datos_personales_imagen_form_iframe" name="frm_datos_personales_imagen_form_iframe" class="hide"></iframe>

						</form>

					</div>

				<?php endif; ?>

			</div>

		</div>

	</div>

</div>

<div class='burbuja hide'>
	<form id="frm_datos_personales" action='datos-personales/actualizar' method='post' enctype="multipart/form-data">
		<?php if($_SESSION['perfil'] == 'alumno'):?>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.nombre">
					Nombre
				</div>
				<div class='campofrm'>
					<input type='text' name='anombrec' size='60'  maxlength="100" value='<?php echo $f['nombre'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.apellidos">
					Apellidos
				</div>
				<div class='campofrm'>
					<input type='text' name='aapellidos' size='60' maxlength="100" value='<?php echo $f['apellidos'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.email">
					E-mail
				</div>
				<div class='campofrm'>
					<input type='text' name='aemail' size='60' value='<?php echo $f['email'];?>' />
				</div>
			</div>	

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.dni">
					DNI
				</div>
				<div class='campofrm'>
					<input type='text' name='adni' size='60' maxlength="10" value='<?php echo $f['dni'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="sexo.titulo">
					Sexo
				</div>
				<div class='campofrm'>
					<select name="aidsexo">
						<option value="1" data-translate-html="sexo.hombre">Hombre</option>
						<option value="2" <?php if($f['idsexo'] == 2) echo 'selected="selected"'?> data-translate-html="sexo.mujer">Mujer</option>
					</select>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.fnacimiento">
					Fecha nacimiento
				</div>
				<div class='campofrm'>
					<input type='text' name='af_nacimiento' size='60' maxlength="10" value='<?php if($f['f_nacimiento'] != '0000-00-00') echo date('d/m/Y', strtotime($f['f_nacimiento']));?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.lnacimiento">
					Lugar nacimiento
				</div>
				<div class='campofrm'>
					<input type='text' name='alugar_nacimiento' size='60' value='<?php echo $f['lugar_nacimiento'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.telefono">
					Tel&eacute;fono
				</div>
				<div class='campofrm'>
					<input type='text' name='atelefono' size='60' maxlength='9' value='<?php if(!empty($f['telefono'])) echo $f['telefono'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.telefono2">
					Tel&eacute;fono secundario
				</div>
				<div class='campofrm'>
					<input type='text' name='atelefono2' size='60' maxlength='9' value='<?php if(!empty($f['telefono_secundario'])) echo $f['telefono_secundario'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.direccion">
					Direcci&oacute;n
				</div>
				<div class='campofrm'>
					<input type='text' name='adireccion' size='60' value='<?php echo $f['direccion'];?>'/>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.cp">
					C&oacute;digo postal
				</div>
				<div class='campofrm'>
					<input type='text' name='acp' size='60' maxlength='5' value='<?php echo $f['cp'];?>'/>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.localidad">
					Localidad
				</div>
				<div class='campofrm'>
					<input type='text' name='alocalidad' size='60' value='<?php echo $f['poblacion'];?>'/>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.provincia">
					Provincia
				</div>
				<div class='campofrm'>
					<input type='text' name='aprovincia' size='60' value='<?php echo $f['provincia'];?>'/>
				</div>
			</div>	

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.pais">
					Pa&iacute;s
				</div>
				<div class='campofrm'>
					<input type='text' name='apais' size='60' value='<?php echo $f['pais'];?>'/>
				</div>
			</div>		
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.usuario">
					Usuario
				</div>
				<div class='campofrm'>
					<input type='text' name='ausuario' size='60' value='<?php echo $f['usuario'];?>'/>
				</div>
			</div>
			
			<span id="alumnoActivo"></span>

		<?php else: ?>	

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.nombrec">
					Nombre completo
				</div>
				<div class='campofrm'>
					<input id='anombrec' type='text' name='anombrec' maxlength='100' size='60' value='<?php echo $f['nombrec'];?>' />
				</div>
			</div>	

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.email">
					E-mail
				</div>
				<div class='campofrm'>
					<input id='aemail' type='text' name='aemail' maxlength='255' size='60' value='<?php echo $f['email'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.telefono">
					Tel&eacute;fono
				</div>

				<div class='campofrm'>
					<input type='text' name='atelefono' size='60' maxlength='9' value='<?php if(!empty($f['telefono'])) echo $f['telefono'];?>' />
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.direccion">
					Direcci&oacute;n
				</div>
				<div class='campofrm'><input type='text' name='adireccion' size='60' maxlength='255' value='<?php echo $f['direccion'];?>'/></div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.cp">
					C&oacute;digo postal
				</div>
				<div class='campofrm'>
					<input type='text' name='acp' size='60' maxlength='5' value='<?php echo $f['cp'];?>'/>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.localidad">
					Localidad
				</div>
				<div class='campofrm'>
					<input type='text' name='alocalidad' size='60' maxlength='255' value='<?php echo $f['localidad'];?>'/>
				</div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.provincia">
					Provincia
				</div>
				<div class='campofrm'>
					<input type='text' name='aprovincia' size='60' maxlength='255' value='<?php echo $f['provincia'];?>'/>
				</div>
			</div>		

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.pais">
					Pa&iacute;s
				</div>
				<div class='campofrm'>
					<input type='text' name='apais' size='60' maxlength='255' value='<?php echo $f['pais'];?>'/>
				</div>
			</div>	

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="usuario.usuario">
					Usuario
				</div>
				<div class='campofrm'>
					<input type='text' name='aalias' maxlength='30' size='60' value='<?php echo $f['alias'];?>'/>
				</div>
			</div>		
		<?php endif;?>
		
		<div class="clear t_left">
			<input type='submit' value='Solicitar cambio de mis datos' style="margin:10px 0 20px" data-translate-value="usuario.solicitar" />
		</div>	

		<br/>
	</form>
</div>


<script type="text/javascript" src="js-datos_personales-default.js"></script>