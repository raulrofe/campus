<div id="datos_personales">
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class='subtitle t_center'>
			<img src="imagenes/datos_personales/person-file.png" height="49px" style="vertical-align:middle;"/>
			<span data-translate-html="usuario.tab1">
				DATOS PERSONALES
			</span>
		</div>
	<?php else: ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>			
				<img src="imagenes/datos_personales/person-file.png" height="49px" style="vertical-align:middle;"/>
				<span data-translate-html="usuario.tab1">
					DATOS PERSONALES
				</span>
			</div>
			<p class='t_justify' data-translate-html="usuario.descripcion">
				Desde este entorno usted podr&aacute; modificar su configuraci&oacute;n personal y se enviar&aacute; una solicitud al administrador 
				para que se produzcan dichos cambios en la plataforma.
			</p>
		</div>
	<?php endif;?>
	
	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>
		<div class="fleft panelTabMenu" id="navDatosPersonales">
			<div id="nav1" class="redondearBorde">
				<a href="datos-personales">
					<img src="imagenes/datos_personales/datos-personales.png" alt="" />
					<span data-translate-html="usuario.tab1">
						Datos personales
					</span>
				</a>
			</div>
	
			<div id="nav2" class="redondearBorde">
				<a href="datos-personales/otros">
					<img src="imagenes/datos_personales/datos-personales-password.png" alt="" />
					<span data-translate-html="usuario.tab2">
						contrase&ntilde;a
					</span>
				</a>
			</div>
			
			<?php if(Usuario::compareProfile('alumno')):?>
				<div id="nav3" class="blanco redondearBorde" style="margin-right:-3px;">
					<a href="datos-personales/notificaciones">
						<img src="imagenes/datos_personales/datos-personales-notificacion.png" alt="" />
						<span data-translate-html="usuario.tab3">
							Notificaciones
						</span>
					</a>
				</div>	
			<?php endif;?>	
		</div>
	</div>
	<!-- ************************************************************************************************************* -->
	<!-- CONTENIDO DE PESTANAS -->
	
	<div class='fleft panelTabContent' id="contenidoDatosPersonales">	
		<!-- TAB 3 : NOTIFICACIONES -->
		<div id="tab3">

			<p class="tituloTabs" data-translate-html="usuario.tab3">
				Notificaciones
			</p>

			<div>
				<form method="post" action="">
					<ul>
						<li>
							<input id="frmNotificacionCorreo" type="checkbox" name="notificacionCorreo" value="1" <?php if($rowAlumno->notifi_correo) echo 'checked="checked"'?> />
							<label for="frmNotificacionCorreo" data-translate-html="usuario.permitir">
								Permitir envio de email a cuenta externa
							</label>
						</li>
						<li>
							<input type="submit" value="Guardar" data-translate-value="formulario.guardar"/>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>