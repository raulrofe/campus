<div id="datos_personales">
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class='subtitle t_center'>
			<img src="imagenes/datos_personales/person-file.png" height="49px" style="vertical-align:middle;"/>
			<span data-translate-html="usuario.tab1">
				DATOS PERSONALES
			</span>
		</div>
	<?php else: ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>			
				<img src="imagenes/datos_personales/person-file.png" height="49px" style="vertical-align:middle;"/>
				<span data-translate-html="usuario.tab1">
					DATOS PERSONALES
				</span>
			</div>
			<p class='t_justify' data-translate-html="usuario.descripcion">
				Desde este entorno usted podr&aacute; modificar su configuraci&oacute;n personal y se enviar&aacute; una solicitud al administrador 
				para que se produzcan dichos cambios en la plataforma.
			</p>
		</div>
	<?php endif;?>
	
	<!-- ************************************************************************************************************* -->
	<!-- MENU DE ICONOS -->	
	<div>
		<div class="fleft panelTabMenu" id="navDatosPersonales">
			<div id="nav1" class="redondearBorde">
				<a href="datos-personales">
					<img src="imagenes/datos_personales/datos-personales.png" />
					<span data-translate-html="usuario.tab1">
						Datos personales
					</span>
				</a>
			</div>
	
			<div id="nav2" class="blanco redondearBorde" style="margin-right:-3px;">
				<a href="datos-personales/otros">
					<img src="imagenes/datos_personales/datos-personales-password.png" />
					<span data-translate-html="usuario.tab2">
						contrase&ntilde;a
					</span>
				</a>
			</div>
			
			<?php if(Usuario::compareProfile('alumno')):?>
				<div id="nav3" class="redondearBorde">
					<a href="datos-personales/notificaciones">
						<img src="imagenes/datos_personales/datos-personales-notificacion.png" />
						<span data-translate-html="usuario.tab3">
							Notificaciones
						</span>
					</a>
				</div>	
			<?php endif;?>	
		</div>
	</div>
	<!-- ************************************************************************************************************* -->
	<!-- CONTENIDO DE PESTANAS -->
	
	<div class='fleft panelTabContent' id="contenidoDatosPersonales">
		<!-- TAB 3 : NOTIFICACIONES -->
		<div id="tab3">
			<p class="tituloTabs" data-translate-html="usuario.cambiar_pass">
				Cambiar contrase&ntilde;a
			</p>
	
			<form method="post" action="">
				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="usuario.antigua_pass">
						Antigua contrase&ntilde;a
					</div>
					<div class='campofrm'>
						<input type='password' name='pass_old'/>
					</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="usuario.nueva_pass">
						Nueva contrase&ntilde;a
					</div>
					<div class='campofrm'>
						<input type='password' name='pass_new'/>
					</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="usuario.confirmar_pass">
						Confirma nueva contrase&ntilde;a
					</div>
					<div class='campofrm'>
						<input type='password' name='pass_new_repeat'/>
					</div>
				</div>
				
				<div class="clear">
					<input type='submit' value='Cambiar contrase&ntilde;a' data-translate-value="usuario.cambiar_pass" />
				</div>
			</form>
		</div>
	</div>
</div>