<?php
class DatosPersonales
{
	public static function validarDatos($nombreC, $nombreUsuario, $email, $cp, $tlfn, $tlfn2 = null)
	{
		if(self::validarLongitudes($nombreC, $nombreUsuario, $cp, $tlfn, $tlfn2) && self::_validarNombreUsuario($nombreUsuario) && self::_validarEmail($email))
		{
			$valido = true;
		}
		else
		{
			$valido = false;
		}
		
		return $valido;
	}
	
	private static function _validarEmail($email)
	{
		$valido = preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email);
		
		return $valido;
	}
	
	private static function _validarNombreUsuario($nombre)
	{
		$valido = preg_match('#^([a-z0-9_]+)$#i', $nombre);
		
		return $valido;
	}
	
	private static function validarLongitudes($nombreC, $nombreUsuario, $cp, $tlfn, $tlfn2)
	{
		if((strlen($nombreC) >= 4 && strlen($nombreC) <= 255)
			&& (strlen($nombreUsuario) >= 4 && strlen($nombreUsuario) <= 30)
				&& ((strlen($cp)== 0 || strlen($cp)) == 5) && ((strlen($tlfn) == 0 || strlen($tlfn) == 9))
				&& (!isset($tlfn2) || (isset($tlfn2) && ((strlen($tlfn2) == 0 || strlen($tlfn2) == 9)))))
		{
			$valido = true;
		}
		else
		{
			$valido = false;
		}
		
		return $valido;
	}
	
	public static function guardarFoto($imagen, $idUsuario, $idperfil)
	{
		$nuevoNombre = $idUsuario . '_' . Texto::cadenaAleatoria(rand(20, 60));
						
		try
		{
			if($idperfil != 'alumno')
			{
				$nuevoNombre = 't_' . $nuevoNombre;
				
				$objUsuario = new Usuarios();
				$objUsuario->set_usuario($idUsuario);
				$usuario = $objUsuario->buscar_usuario();
			}
			else
			{
				$objUsuario = new Alumnos();
				$objUsuario->set_alumno($idUsuario);
				$usuario = $objUsuario->ver_alumno();
			}
			$usuario = $usuario->fetch_object();
		
			$objThumb = PhpThumbFactory::create($imagen['tmp_name']);
						
			$newFilename = $nuevoNombre . '.' . strtolower($objThumb->getFormat());
			$path = PATH_ROOT . 'imagenes/fotos/';
						
			$objThumb->resize(100, 100);
			$objThumb->save($path . $newFilename);
			
			if($usuario->foto != 'default.png')
			{
				@chmod($path . $usuario->foto, 0777);
				@unlink($path . $usuario->foto);
			}
							
			$objUsuario->actualizar_foto_usuario($newFilename);
		}
		catch(Exception $e) {}
	}
}