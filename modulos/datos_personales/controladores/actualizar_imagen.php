<?php

$imageReturn = '';

if(isset($_FILES['imagen']['tmp_name']) && !empty($_FILES['imagen']['tmp_name']))
{
	if(Fichero::validarTipos($_FILES['imagen']['name'], $_FILES['imagen']['type'], array('jpg', 'jpeg', 'png')))
	{
		require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';
		
		$objThumb = PhpThumbFactory::create($_FILES['imagen']['tmp_name']);
		$objThumb->resize(95);
		$imageReturn = 'data:image/' . strtolower($objThumb->getFormat()) . ';base64,' . base64_encode($objThumb->getImageAsString());
		
		DatosPersonales::guardarFoto($_FILES['imagen'], $_SESSION['idusuario'], $_SESSION['perfil']);
	}
}

echo $imageReturn;