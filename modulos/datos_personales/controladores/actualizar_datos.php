<?php
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	extract(Peticion::obtenerPost());
	
	if($_SESSION['perfil'] == 'alumno')
	{
		if(isset($anombrec, $aapellidos, $af_nacimiento, $atelefono, $atelefono2, $adireccion, $acp, $alocalidad, $aprovincia, $apais, $ausuario, $alugar_nacimiento, $aemail, $aidsexo))
		{
						
			$anombrec = trim($anombrec);
			$aapellidos = trim($aapellidos);
			$af_nacimiento = trim($af_nacimiento);
			$atelefono = trim($atelefono);
			$atelefono2 = trim($atelefono2);
			$adireccion = trim($adireccion);
			$acp = trim($acp);
			$alocalidad = trim($alocalidad);
			$aprovincia = trim($aprovincia);
			$apais = trim($apais);
			$ausuario = trim($ausuario);
			$alugar_nacimiento = trim($alugar_nacimiento);
			$aemail = trim($aemail);
			$adni = trim($adni);
	
			if(!empty($anombrec) && !empty($aapellidos) && !empty($aemail) && !empty($ausuario))
			{
				if(DatosPersonales::validarDatos($anombrec, $ausuario, $aemail, $acp, $atelefono, $atelefono2))
				{
					$objModelo = new ModeloDatosPersonales();
					
					/*$mi_alumno = new alumnos();
					$mi_alumno->set_alumno($_SESSION['idusuario']);
					$alumno = $mi_alumno->ver_alumno();
					$alumno = $alumno->fetch_object();*/
					
					$af_nacimiento = Fecha::invertir_fecha($af_nacimiento, '/');
					
					$alumno = $objModelo->obtenerUnAlumnoPeticion(Usuario::getIdUser());
					
					if($alumno->num_rows == 1)
					{
						$objModelo->actualizarAlumnoPeticion
						(
							Usuario::getIdUser(),
							$anombrec,
							$aapellidos,
							$adni,
							$af_nacimiento,
							$atelefono,
							$atelefono2,
							$adireccion,
							$acp,
							$alocalidad,
							$aprovincia,
							$apais,
							$ausuario,
							$alugar_nacimiento,
							$aemail,
							$aidsexo
						);
					}
					else
					{
						$objModelo->insertarAlumnoPeticion
						(
							Usuario::getIdUser(),
							$anombrec,
							$aapellidos,
							$adni,
							$af_nacimiento,
							$atelefono,
							$atelefono2,
							$adireccion,
							$acp,
							$alocalidad,
							$aprovincia,
							$apais,
							$ausuario,
							$alugar_nacimiento,
							$aemail,
							$aidsexo
						);
					}
					
					
					/*if(isset($_FILES['imagen']['tmp_name']) && !empty($_FILES['imagen']['tmp_name']))
					{
						DatosPersonales::guardarFoto($_FILES['imagen'], $_SESSION['idusuario'], $_SESSION['perfil']);
					}*/
					
					Alerta::guardarMensajeInfo('solicitadocambiodatos','Se han solicitado los cambios de datos');
				}
			}	
		}
	}
	else
	{
		if(isset($anombrec, $aalias, $adireccion, $alocalidad, $aprovincia, $apais, $acp, $atelefono, $aemail))
		{
			$anombrec = trim($anombrec);
			$aalias = trim($aalias);
			$adireccion = trim($adireccion);
			$alocalidad = trim($alocalidad);
			$aprovincia = trim($aprovincia);
			$apais = trim($apais);
			$acp = trim($acp);
			$atelefono = trim($atelefono);
			$aemail = trim($aemail);
			
			$mi_usuario = new usuarios();
			$mi_usuario->set_usuario($_SESSION['idusuario']);
			
			if(!empty($anombrec) && !empty($aalias) && !empty($aemail))
			{
				if(DatosPersonales::validarDatos($anombrec, $aalias, $aemail, $acp, $atelefono))
				{
					$af_nacimiento = Fecha::invertir_fecha($af_nacimiento, '/');
					
					$mi_usuario->actualizar_usuario
					(
						$anombrec,
						$aalias,
						$adireccion,
						$alocalidad,
						$aprovincia,
						$apais,
						$acp,
						$atelefono,
						$aemail
					);
					
					if(isset($_FILES['imagen']['tmp_name']) && !empty($_FILES['imagen']['tmp_name']))
					{
						DatosPersonales::guardarFoto($_FILES['imagen'], $_SESSION['idusuario'], $_SESSION['perfil']);
					}
					
					Alerta::guardarMensajeInfo('datosactualizado','Datos actualizados');
				}
			}
		}
	}
}

Url::redirect('datos-personales', true);

/*
extract($_POST);
if(isset($_GET['v2'])) $v2 = $_GET['v2'];
if(isset($_GET['idautoevaluacion'])) $idautoevaluacion = $_GET['idautoevaluacion'];

$db = new database();
$con = $db->conectar();
*/
/*
$mi_general = new generales($con);
$mi_autoevaluacion = new autoevaluacion($con);
$registros_tematica_autoevaluacion = $mi_autoevaluacion->tematica_autoevaluacion();
$mi_modulo  = new Modulos($con);
$registros_modulos = $mi_modulo->ver_modulos();
*/

/*
if($_SESSION['perfil'] == 'alumnos'){
	$mi_alumno = new alumnos ($con);
	$mi_alumno->set_alumno($_SESSION['idusuario']);
	if(!empty ($anombrec) or ($aapellidos) or ($adni)){$mi_alumno->actualizar_alumno($anombrec,$aapellidos,$adni,$af_nacimiento,$atelefono,$atelefono2,$adireccion,$acp,$alocalidad,$aprovincia,$apais,$ausuario,$apass,$alugar_nacimiento,$aemail);}	
}

else{
	$mi_usuario = new usuarios($_SESSION['perfil'],$con);
	$mi_usuario->set_usuario($_SESSION['idusuario']);
	if(!empty($anombrec)){$mi_usuario->actualizar_usuario($anombrec,$apass,$aalias,$adireccion,$alocalidad,$aprovincia,$apais,$acp,$atelefono,$aemail);}
	$registros = $mi_usuario->buscar_usuario();
	$f = mysqli_fetch_assoc($registros);
}
*/