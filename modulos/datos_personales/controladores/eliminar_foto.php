<?php
$db = new database();
$con = $db->conectar();
		
if($_SESSION['perfil'] == 'alumno')
{
	$mi_usuario = new Alumnos($con);
	$mi_usuario->set_alumno($_SESSION['idusuario']);
			
	$path = PATH_ROOT . 'imagenes/fotos/';
	$usuario = $mi_usuario->ver_alumno();
		
	$usuario = $usuario->fetch_object();
	if($usuario->foto != 'default.png')
	{
		@chmod($path . $usuario->foto, 0777);
		@unlink($path . $usuario->foto);
	
		$mi_usuario->actualizar_foto_usuario('default.png');
	}
}	
else
{
	$mi_usuario = new usuarios($_SESSION['perfil'],$con);
	$mi_usuario->set_usuario($_SESSION['idusuario']);
			
	$path = PATH_ROOT . 'imagenes/fotos/';
	$usuario = $mi_usuario->buscar_usuario();
		
	$usuario = $usuario->fetch_object();
	if($usuario->foto != 'default.png')
	{
		@chmod($path . $usuario->foto, 0777);
		@unlink($path . $usuario->foto);
	
		$mi_usuario->actualizar_foto_usuario('default.png');
	}
}

Url::redirect('datos-personales');