<?php
$db = new database();
$con = $db->conectar();

if($_SESSION['perfil'] == 'alumno')
{
	$mi_alumno = new Alumnos($con);
	
	$mi_alumno->set_alumno($_SESSION['idusuario']);
	$registros = $mi_alumno->ver_alumno();
	
	$objModelo = new ModeloDatosPersonales();
	$registros_peticion = $objModelo->obtenerUnAlumnoPeticion(Usuario::getIdUser());
	if($registros_peticion->num_rows == 1)
	{
		$registros = $registros_peticion;
	}
}
else 
{
	$mi_usuario = new usuarios($_SESSION['perfil'], $con);
	
	$mi_usuario->set_usuario($_SESSION['idusuario']);
	$registros = $mi_usuario->buscar_usuario();
}

$f = $registros->fetch_assoc();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'datos');