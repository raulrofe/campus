<?php
if($_SESSION['perfil'] == 'alumno')
{
	$idAlumno = Usuario::getIdUser();
	
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		// obtenemos las nuevas preferencias enviadas por el usuario
		$notificacionCorreo = '0';
		if(isset($post['notificacionCorreo']) && $post['notificacionCorreo'] == '1')
		{
			$notificacionCorreo = '1';
		}
		
		$objModeloDatosPersonales = new ModeloDatosPersonales();
		// actualizamos las preferencias del usuario en las notificaciones
		if($objModeloDatosPersonales->actualizarNotifiaciones($idAlumno, $notificacionCorreo))
		{
			Alerta::mostrarMensajeInfo('preferenciasguardadas','Se han guardado tus preferencias');
		}
	}
	
	// obtenemos la conexion a la BBDD
	$db = new database();
	$con = $db->conectar();

	// obtenemos al alumno
	$objAlumnos = new Alumnos($con);
	$objAlumnos->set_alumno($idAlumno);
	$rowAlumno = $objAlumnos->ver_alumno();
	$rowAlumno = $rowAlumno->fetch_object();
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'notificaciones');
}
else
{
	Url::lanzar404();
}