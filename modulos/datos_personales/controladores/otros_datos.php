<?php
$db = new database();
$con = $db->conectar();

$mi_usuario = new usuarios($_SESSION['perfil'], $con);
$mi_usuario->set_usuario($_SESSION['idusuario']);

if(Peticion::isPost())
{
	// CAMBIAR IMAGEN DE PERFIL DE USUARIO
	if(isset($_FILES['imagen']['tmp_name']) && !empty($_FILES['imagen']['tmp_name']))
	{
		require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';
		DatosPersonales::guardarFoto($_FILES['imagen'], $_SESSION['idusuario'], $_SESSION['perfil']);
	}
}

if($_SESSION['perfil'] == 'alumno')
{
	$mi_alumno = new Alumnos($con);
	
	$mi_alumno->set_alumno($_SESSION['idusuario']);
	$registros = $mi_alumno->ver_alumno();
}
else 
{
	$registros = $mi_usuario->buscar_usuario();
}

$f = $registros->fetch_assoc();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();

	// CAMBIAR CONTRASE_A
	if(isset($post['pass_old'], $post['pass_new'], $post['pass_new_repeat']))
	{
		if(!empty($post['pass_new']) && !empty($post['pass_new_repeat']))
		{
			// si la contrase_a actual es correcta ...
			if($f['pass'] == md5($post['pass_old']))
			{
				// si la nueva contrase_a esta confirmada ...
				if($post['pass_new'] == $post['pass_new_repeat'])
				{
					if($mi_usuario->actualizarPass($post['pass_new']))
					{
						Alerta::mostrarMensajeInfo('passactualizada',"Se ha actualizado la contrase&ntilde;a");
					}
				}
				else
				{
					Alerta::mostrarMensajeInfo('passnocoincide',"La nueva contrase&ntilde;a no coincide");
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('passincorrecta',"La contrase&ntilde;a actual es incorrecta");
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('passnovacia','La nueva contrase&ntilde;a no puece estar vac&iacute;a');
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'otros_datos');