<?php 
//Obtenemos los mensajes sin leer
$mensajes_sinleer = $mi_correo->mensajes_sin_leer();
$mensajes_sin_leer = mysqli_num_rows($mensajes_sinleer);

//Obtenemos los mensajes sin leer de papelera
$mensajes_sinleer_papelera = $mi_correo->mensajes_sin_leer_papelera();
$mensajesPapeleraNoleidos = mysqli_num_rows($mensajes_sinleer_papelera);

//Obtenemos los mensajes sin leer de la subcarpeta elegida
$mensajes_sinleer_subcarpeta = $mi_correo->mensajes_sin_leer_subcarpeta();
$mensajesSubcarpetaNoleidos = mysqli_num_rows($mensajes_sinleer_subcarpeta);	

$mensajesTodasSubcarpetas = array();
while($msjSubcarpetas = mysqli_fetch_assoc($mensajes_sinleer_subcarpeta))
{
	$mensajesTodasSubcarpetas[] = $msjSubcarpetas['idcurso'];
}

$numMsjSucarpeta = array_count_values($mensajesTodasSubcarpetas);

$mensajesBorrador = $mi_correo->bandeja_borrador();

$numMsjBorrador = mysqli_num_rows($mensajesBorrador);

?>
<div class="popupIntro" style="padding: 0 20px; text-align: center;">
	<div class='subtitle t_center'><span>Correo</span></div>
	<p class='t_justify'>A trav&eacute;s de este servicio de e-mail puede recibir y enviar mensajes al alumnado, al equipo de tutores/as y al profesorado de cada m&oacute;dulo.</p>
</div>

<?php if($get['c'] != 'enviar_mensaje' && $get['c'] != 'reenvio_mensaje'):?>
	<form name='frm_seleccion_correo' method='post' action='' enctype='multipart/form-data'>
<?php endif;?>

<div class='gestor_de_correos' style='border-top:1px solid #eee;'>
	<div class='carpetas_correo' style="height: 103%;">
		<div class='carpetas'>
			<div id="gestor_correo_titulo"><?php echo $carpeta; ?></div>
			
			<div id="gestor_correo_nuevo_mensaje">
				<a href="correos/enviar_mensaje">Mensaje nuevo</a>
			</div>
			<ul class="clear">
				<li class='folder' style="padding-bottom:0;">
					<a id='gestor_correos_bandeja_entrada_icon_menu' href='#' onclick='return false;' title='Mostrar opciones'><img src='imagenes/correos/contextMenu_down.png' alt='' /></a>
					<a href="" onclick="gestorCorreoMostrarOcultarCapertas(); return false;" title=""><img src="imagenes/correo_arrow_folder.png" /></a>
					<img src='imagenes/correos/elementos_recibidos.png' alt='' />
					<a id="gestor_correos_link_mail_into" href='correos/bandeja_entrada'>Bandeja de entrada</a>
					<?php if ($mensajes_sin_leer > 0):?>
						<span class="gestor_correos_mail_count"><?php echo $mensajes_sin_leer;?></span>
					<?php endif;?>
				</li>
				<li id="gestor_correos_ver_idcarpeta" class="<?php if(!isset($_COOKIE['mail_folder_show'])) echo 'hide';?>">
					<div id="gestor_correos_ver_idcarpeta_scroll">
						<ul>
							<?php 
							while($folder = mysqli_fetch_assoc($registros_carpetas))
							{
								if(isset($numMsjSucarpeta[$folder['idcurso']]))
								{
									$numMsjSub = '<span class="gestor_correos_mail_count">' . $numMsjSucarpeta[$folder['idcurso']] . '</span>';
								}
								else
								{
									$numMsjSub = '';
								}

								echo $numMsjSub;
									
								if(isset($verIdcarpeta) && $verIdcarpeta == $folder['idcurso'])
								{
									echo "<li>" .
											"<img src='imagenes/correos/folder.png' alt='' />" .
											"<a data-context-menu='0' href='correos/subcarpeta/".$folder['idcurso']."' title='" . Texto::textoPlano($folder['titulo']) . "' class='tooltip'>" .
												Texto::resume($folder['titulo'], 24) . 
											"</a>" .
											$numMsjSub .
										"</li>";
								}
								else 
								{
									echo "<li><img src='imagenes/correos/folder.png' alt='' />
											<a data-context-menu='0' href='correos/subcarpeta/" . $folder['idcurso'] . "' title='" . Texto::textoPlano($folder['titulo']) . "' >".
												Texto::resume($folder['titulo'], 24) . 
											"</a>" . $numMsjSub . "</li>";
								}
							}
							
							foreach($carpetasPersonalesArray as $carpetaPersonal)
							{
								$numMsjSub = '';
								$numCarpetaPersonal = $mi_correo->bandeja_carpetas_personales_sin_leer($carpetaPersonal->idcarpeta);
								if($numCarpetaPersonal->num_rows > 0)
								{
									$numCarpetaPersonal = $numCarpetaPersonal->num_rows;
									$numMsjSub = '<span class="gestor_correos_mail_count">' . $numCarpetaPersonal . '</span>';
								}
								
								echo "<li><a class='gestor_correos_ver_idcarpeta_icon_menu' href='#' onclick='return false;' title='Mostrar opciones' data-translate-title='correo.opciones'><img src='imagenes/correos/contextMenu_down.png' alt='' /></a>" .
									"<img src='imagenes/correos/folder_user.png' alt='' /><a data-context-menu='1' data-id='" . $carpetaPersonal->idcarpeta . "' href='correos/subcarpeta/personal/" . $carpetaPersonal->idcarpeta . "' title='" . Texto::textoPlano($carpetaPersonal->nombre_carpeta) . "' class='tooltip'>" .
										Texto::resume($carpetaPersonal->nombre_carpeta, 25) . "</a>" . $numMsjSub . "</li>";
							}
							?>
						</ul>
					</div>
				</li>
				<li class='folder'>
					<img src='imagenes/correos/borrador.png' alt='' />
					<a href='correos/borrador'>Borrador</a>
					<?php if($numMsjBorrador > 0):?>
						<span class="gestor_correos_mail_count"><?php echo $numMsjBorrador;?></span>
					<?php endif;?>
				</li>
				<li class='folder'><img src='imagenes/correos/elementos_enviados.png' alt='' /><a href='correos/bandeja_salida'>Elementos enviados</a></li>
				<li class='folder'>
					<img src='imagenes/correos/elementos_eliminados.png' alt='' />
					<a href='correos/papelera'>Elementos eliminados</a>
					<?php if($mensajesPapeleraNoleidos > 0):?>
						<span class="gestor_correos_mail_count"><?php echo $mensajesPapeleraNoleidos;?></span>
					<?php endif;?>
				</li>
			</ul>
		</div>
		<div id="carpetas_correo_bg"></div>
	</div>
	<div class='contenido_correo'>
		<div style='margin-top:-5px;margin-left:-5px;'>
		<div class='subtitle2 fleft'>
			<div class="fleft">
				<?php if($get['c'] == 'bandeja_entrada' || $get['c'] == 'bandeja_salida' || $get['c'] == 'papelera' || $get['c'] == 'detalle_mensaje' || $get['c'] == 'borrador'):?>
					<div class='general_correo fleft'><a href='#' onclick='$("#correo_hacer").val("eliminar");document["frm_seleccion_correo"].submit(); return false;'>Borrar mensajes</a></div>
					
					<div class='general_correo fleft'><a href='#' onclick='$("#correo_hacer").val("noleido");document["frm_seleccion_correo"].submit(); return false;'>No le&iacute;do/Le&iacute;do</a></div>
					<?php if(!isset($get['c2']) && ($get['c'] == 'bandeja_entrada' || $get['c'] == 'detalle_mensaje')):?>
						<div class='general_correo fleft' style="position: relative !important;">
							<a href='#' onclick='correoArchivar(); return false;'>Archivar</a>
							<div id="general_correo_tooltip_menu" class="hide">
								<ul>
									<?php if(!isset($get['ver_idcarpeta'])):?>
										<li><a href="#" onclick="correoArchivarAuto(); return false;" title="">Autoarchivar</a></li>
									<?php endif;?>
									<?php if(isset($get['ver_idcarpeta']) || isset($get['idcarpeta'])
										|| (isset($get['menu']) && $get['menu'] == 'carpeta')):?>
										<li><a href="#" onclick="correoDesarchivar(); return false;" title="">Desarchivar</a></li>
									<?php endif;?>
									
									<?php
									foreach($carpetasPersonalesArray as $carpetaPersonal)
									{
										echo "<li>- <a href='#' onclick='correoArchivarForm(" . $carpetaPersonal->idcarpeta . "); return false;' title='" . Texto::textoPlano($carpetaPersonal->nombre_carpeta) . "' class='tooltip'>" .
												Texto::resume($carpetaPersonal->nombre_carpeta, 25) . "</a></li>";
									}
									?>
								</ul>							
							</div>
						</div>
					<?php endif;?>	
					
					<?php if($get['c'] == 'papelera' || (isset($get['menu']) && $get['menu'] == 'papelera')):?>
						<div class='general_correo fleft'><a href='#' onclick='$("#correo_hacer").val("restaurar");document["frm_seleccion_correo"].submit(); return false;'>Restaurar</a></div>
					<?php endif;?>	
					
					<?php if($get['c'] == 'detalle_mensaje' && isset($detalle_mensaje)):?>
						<?php if(!isset($get['c2'])):?>
							<?php if(!isset($get['menu']) || (isset($get['menu']) && $get['menu'] == 'bandeja_entrada')):?>
								<div class='general_correo fleft' style="position: relative;">
									<a href='correos/respuesta_mensaje/<?php echo $detalle_mensaje['iddestinatarios']; ?>'>Responder</a>
								</div>
							<?php endif;?>
						<?php else:?>
							<div class='general_correo fleft' style="position: relative;">
								<a href='correos/borrador/<?php echo $detalle_mensaje['iddestinatarios'];?>/editar'>Editar borrador</a>
							</div>
						<?php endif;?>
						<div class='general_correo fleft' style="position: relative;">
							<a href='correos/reenvio_mensaje/<?php echo $detalle_mensaje['iddestinatarios']; ?>'>Reenviar</a>
						</div>					
					<?php endif;?>
				<?php endif;?>
			</div>
			<div class="clear"></div>
		</div>
		
		<?php if($get['c'] == 'bandeja_entrada' || $get['c'] == 'bandeja_salida' || $get['c'] == 'papelera' || (isset($get['hacer']) && $get['hacer'] == 'buscar')):?>
			<div class='buscador_correo'>
				<input type='text' name='textBuscar' value='<?php if(isset($get['textBuscar'])) echo Texto::textoPlano($get['textBuscar'])?>'/>
			</div>
		<?php endif;?>
		
		<input id='correo_hacer' type='hidden' name='hacer' value='buscar' />
		<input id='correo_hidden_idcarpeta_personal' type='hidden' name='idcarpeta_personal' value='' />
		</div>
		
		<div class="clear"></div>
		
		<div class='msjs'>
			<?php 
				require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);
			?>
		</div>
	</div>
	<div class="clear"></div>
</div>

<?php if($get['c'] != 'enviar_mensaje' && $get['c'] != 'reenvio_mensaje'):?>
	</form>
<?php endif;?>
	
<br/><br/>
<script type="text/javascript">
	if($('#gestor_correo_listado').size() == 1)
	{
		var heightGestCorreo = parseInt($('#popupModal_gestor_correos_wrapper_aux').height()) + 17;
		$('.carpetas').css('height', heightGestCorreo + 'px');

		$('#popupModal_gestor_correos').resize(function() {
			var heightGestCorreo = parseInt($('#popupModal_gestor_correos_wrapper_aux').height()) + 17;
			$('.carpetas').css('height', heightGestCorreo + 'px');
		});

		$('#popupModal_gestor_correos .popupModalHeaderBtn_maximize').click(function()
		{
			var heightGestCorreo = parseInt($('#popupModal_gestor_correos_wrapper_aux').height()) + 17;
			$('.carpetas').css('height', heightGestCorreo + 'px');
		});
		$('#popupModal_gestor_correos .popupModalHeaderBtn_restore').click(function()
		{
			var heightGestCorreo = parseInt($('#popupModal_gestor_correos_wrapper_aux').height()) + 17;
			$('.carpetas').css('height', heightGestCorreo + 'px');
		});
	}

	$(document).ready(function()
	{
		if($('#gestor_correos_ver_idcarpeta').size() == 1)
		{
			$('#gestor_correos_ver_idcarpeta select').removeClass('hide');
		}
	});

	function correo_cambiar_carpeta(element)
	{
		var idCarpeta = $(element).val();

		LibPopupModal.loadPage(element, 'correos/subcarpeta/' + idCarpeta);
	}
</script>

<script type="text/javascript" src="js-gestor_correos-buscador.js"></script>
<script type="text/javascript" src="js-gestor_correos-default.js"></script>
<script type="text/javascript" src="js-gestor_correos-contextMenu.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		height : "250",
		plugins: "emoticons paste",
		browser_spellcheck : true,	
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons ",
		paste_preprocess : function(pl, o) {
		o.content = o.content.replace(/<\S[^><]*>/g, "");
		}
	});
</script>
