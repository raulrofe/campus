<?php //var_dump($detalle_mensaje); ?>

<form name='frm_respuesta_mensaje' method='post' enctype='multipart/form-data' action=''>
<div class='tituloBloques'>Mensaje</div>
		
	<div id="gestor_correos_botones">
		<ul>
		    <li><button type="submit">Enviar</button></li>
			<li><button type="button" onclick='$("#actionBoton").val("borrador");document["frm_seleccion_correo"].submit(); return false;'>Guardar como borrador</button></li>
		</ul>
		<div class="clear"></div>
	</div>
	
	<div class='api_adjuntar'>
		
		<div style="margin-left: 10px;">
			<br />
			<div>
				Mensaje para: <?php echo Texto::textoPlano($nombreRemitente)?>
			</div>
			<br />
			<div class='fleft etiquetaAsunto'><label>Asunto</label></div>
			<div class='fleft;'>
				<div style='overflow:hidden;'>
					<input type='text' size='90' name='asunto' value='<?php echo $elAsunto;?>'/>
				</div>
			</div>
		</div>
		<div class='clear'></div>
		
		<div>
			<div class='menuMasivoTutorSin'>
				<div class='upload fleft'><input name="archivo[]" class="multi" type="file"/></div>
				<div class='fleft importanciaAlta'>
					<input type='hidden' name='importante' value='0' />
					<span>
						<a href="#" onclick="correoBotonImportancia();return false;">
							<img src='imagenes/correos/important.png' alt='' class='alignVertical'/><label>Importancia alta</label>
						</a>
					</span>
				</div>
			</div>
			<?php if(Usuario::compareProfile(array('tutor'))):?>
				<input type='hidden' name='addSeguimiento' value='0' />
			<?php endif;?>
		</div>
		
		<div class='mensaje_correo clear'>
			<textarea id="" name='mensaje' class='mensaje'>
				<?php echo "<span style='color:#6F6F6F'><br/><br/><br/><br/>______________________________________________________________________<br/><br/>".
				"De: " . $nombreRemitente . "<br/>" .
				"Enviado el: " . Fecha::obtenerFechaFormateada($detalle_mensaje['fecha']) . "<br/>" .
				"Para: " . $nombreDestinatario . "<br/>" .
				"Asunto: " . $detalle_mensaje['asunto'] . "<br/>" .
				$detalle_mensaje['mensajes'] . "<br/></span>"; ?>
			</textarea>
		</div>
	</div>

	<input type='hidden' name='iddestinatario' value='<?php echo $detalle_mensaje['remitente'];?>' />
	<input type='hidden' name='idcorreo' value='<?php echo $detalle_mensaje['idcorreos'];?>' />
	<input type='hidden' name='idCurso' value='<?php echo $detalle_mensaje['idcurso'];?>' />
	<input id='actionBoton' type='hidden' name='actionBoton' value='Enviar' />
</form>

<script type="text/javascript" src="js-gestor_correos-nuevo.js"></script>
<script type="text/javascript">$('.menuMasivoTutorSin input.multi').MultiFile();</script>
