	<div class='encabezado_correo'>
		<div class='asunto_s'><a href='correos/<?php echo $get['c'] ?>/<?php echo $todo2; ?>'>Todo</a></div>
		<div class='asunto_a'>Asunto</div>
		<div class='asunto_d'>De:</div>
		<div class='asunto_f'>Fecha</div>
	</div>
	<div id="gestor_correo_listado">
			<?php 
			if(mysqli_num_rows($registros_correos) != 0)
			{
				while($mensajes = mysqli_fetch_assoc($registros_correos)){
					
				//comprobamos si tiene archivos adjuntos
				$sql_adjuntos = "SELECT * from adjuntos where idcorreos = ".$mensajes['idcorreos'];
				$resultado_adjuntos = mysqli_query($con,$sql_adjuntos);
				$num_adjuntos = mysqli_num_rows($resultado_adjuntos);
				
				$sql_tutor = "SELECT * from correos C, destinatarios D
				where C.idcorreos = ".$mensajes['idcorreos']." and C.idcorreos = D.idcorreos";
				//echo $sql_tutor;
				$resultado_tutor = $mi_correo->consultaSql($sql_tutor);
				$f_tutor = mysqli_fetch_assoc($resultado_tutor);
				$array = Usuario::esAlumno($f_tutor['remitente']);		
	
				
				
				if($array[0] != 'alumno')
				{
					$sql = "SELECT * from rrhh where idrrhh = ".$array[1];
					$result = mysqli_query($con,$sql);
					$fil = mysqli_fetch_assoc($result);
					$de = $fil['nombrec'];
				}
				else 
				{
					$sql = "SELECT * from alumnos where idalumnos = ".$array[1];
					$result = mysqli_query($con,$sql);
					$fil = mysqli_fetch_assoc($result);
					$de = $fil['nombre']." ".$fil['apellidos'];
				}
				
				$fechaFormateada = Fecha::obtenerFechaFormateada($mensajes['fecha']);
				?>
			<div class='mensajes_correo' <?php  if ($mensajes['leido'] == 0) echo "style='font-weight:bold;'"; ?> >
				<div class='asunto_s'><input type='checkbox' name='idmensaje[]' value='<?php echo $mensajes['iddestinatarios'];?>' <?php if ($todo == 'si') echo 'checked'; ?> /></div>
				<div class='asunto_a'>
					<?php 
					if ($mensajes['leido'] == 0){echo "<img src='imagenes/correos/mail_noleido.png' alt='leido' />";}
					else{ echo "<img src='imagenes/correos/mail_leido.png' alt='leido' />";}
					if($num_adjuntos > 0){echo "<img src='imagenes/correos/clip.png' alt='' />";}
					?>
					<a href='correos/detalle_mensaje/<?php echo $mensajes['iddestinatarios'];?>?c2=borrador&menu=borrador'><?php echo Texto::textoPlano($mensajes['asunto']); ?></a>
				</div>
				<div class='asunto_d'><?php echo Texto::textoPlano($de); ?></div>
				<div class='asunto_f'><?php echo $fechaFormateada; ?></div>
				<div class="clear"></div>
			</div>
			<?php 
			echo "<input type='hidden' name='iddestino[]' value='".$mensajes['iddestinatarios']."' />";
				} 
			}
			else
			{
				if(isset($mensaje_buscar))
				{
					echo "<br/><div class='mensaje_correo t_center'>" . $mensaje_buscar . "</div>";
				}
				else if(isset($mensaje_carpetas))
				{
					echo "<br/><div class='mensaje_correo t_center'>" . $mensaje_carpetas . "</div>";	
				}
				else
				{
					echo "<div class='mensajes_correo t_center'>No existen mensajes en borradores</div>";
				}
			}
		?>
		<input type='hidden' name='mandar' value='papelera' />
	</div>
			
			<?php //echo Paginado::crear($numPagina, $maxPaging, 'correos/bandeja_entrada?pagina=', $urlPagingEnd)?>