<div class='encabezado_correo'>
	<div class='asunto_s'><a href='correos/<?php echo $get['c'] ?>/<?php echo $todo2; ?>'>Todo</a></div>
	<div class='asunto_a'>Asunto</div>
	<div class='asunto_d'>De:</div>
	<div class='asunto_f'>Fecha</div>
</div>
<div id="gestor_correo_listado">
<?php 

//Obtenemos el alias del usuario en la sesion
if($_SESSION['perfil'] != 'alumno')
{
	$quienEnviaEmail = 't_'.$_SESSION['idusuario'];
}
else
{
	$quienEnviaEmail = $_SESSION['idusuario'];	
}

if(isset ($papelera['id'])){
	for($i=0;$i<=count($papelera['id'])-1;$i++)
	{	
			//comprobamos si tiene archivos adjuntos
			$sql_adjuntos = "SELECT * from adjuntos where idcorreos = ".$papelera['id'][$i];
			$resultado_adjuntos = mysqli_query($con,$sql_adjuntos);
			$num_adjuntos = mysqli_num_rows($resultado_adjuntos);
		
			$fechaFormateada = Fecha::obtenerFechaFormateada($papelera['fecha'][$i]);
			$sql = "SELECT remitente from destinatarios where idcorreos = ".$papelera['id'][$i]." and fecha = '".$papelera['fecha'][$i]."' GROUP BY fecha";
			$resultado = mysqli_query($con,$sql);
			$los_destinatarios='';
			while($fila = mysqli_fetch_assoc($resultado))
			{
				
				
				
				$dest = explode("_",$fila['remitente']);
				
				if($dest[0] == 't')
				{
					$sql2 = "SELECT * from rrhh where idrrhh = ".$dest[1];
					$resultado2 = mysqli_query($con,$sql2);
					$f2 = mysqli_fetch_assoc($resultado2);
					if($los_destinatarios != $f2['nombrec'])
					{
						$los_destinatarios.= $f2['nombrec'];
					}
				}
				else 
				{
					$sql3 = "SELECT * from alumnos where idalumnos = ".$dest[0];
					$resultado3 = mysqli_query($con,$sql3);
					$f3 = mysqli_fetch_assoc($resultado3);
					$los_destinatarios.= $f3['nombre'].$f3['apellidos'];
				}
			}
	?>
	<div class='mensajes_correo'>
			<div class='asunto_s'><input type='checkbox' name='idmensaje[]' value='<?php echo $papelera['iddestinatario'][$i];?>' <?php if ($todo == 'si') echo 'checked'; ?> /></div>
			<div class='asunto_a'>
				<?php 
				if ($papelera['leido'][$i] == 0 && $quienEnviaEmail != $papelera['de'][$i]){echo "<img src='imagenes/correos/mail_noleido.png' alt='leido' />";}
				else{ echo "<img src='imagenes/correos/mail_leido.png' alt='leido' />";}
				if($papelera['importante'][$i] == 1) {echo "<img src='imagenes/correos/important.png' alt='' />&nbsp;";}
				if($num_adjuntos > 0){echo "<img src='imagenes/correos/clip.png' alt='' />";}
				?>
				<a href='correos/detalle_mensaje/<?php echo $papelera['iddestinatario'][$i];?>/<?php echo $get['c'];?>'>
					<?php 
						if($papelera['leido'][$i] ==  0 && $quienEnviaEmail != $papelera['de'][$i]) 
						{
							echo "<span class='negrita'>". Texto::textoPlano($papelera['asunto'][$i]) . "</span>"; 
						}
						else
						{
							echo Texto::textoPlano($papelera['asunto'][$i]); 	
						}
					?>
				</a>
			</div>
			<div class='asunto_d'>
			<?php echo $los_destinatarios;?>
			</div>
			<div class='asunto_f'><?php echo $fechaFormateada; ?></div>
	</div>
<?php 
	}
}

//else echo "<div class='mensajes_correo>No existen mensajes</di>";

else
{
	if(isset($mensaje_buscar))
	{
		echo "<br/><div class='mensaje_correo t_center'>" . $mensaje_buscar . "</div>";
	}
	else if(isset($mensaje_carpetas))
	{
		echo "<br/><div class='mensaje_correo t_center'>" . $mensaje_carpetas . "</div>";	
	}
	else
	{
		echo "<div class='mensajes_correo t_center'>No existen mensajes en la papelera de reciclaje</div>";
	}
}
?>
</div>

<?php //echo Paginado::crear($numPagina, $maxPaging, 'correos/papelera?pagina=', $urlPagingEnd)?>

