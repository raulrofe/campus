$(document).ready(function()
{
	// sirve para que cuando se envien un correo a toda la convocatoria se deshabiliten el envio al curso y el envio individual por elumnnos
	$('.contenido_correo input:checkbox[name="enviar_a"][value="conv"]').on('click', function()
	{
		$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').removeAttr('checked');
		
		if(!$(this).prop('checked'))
		{
			// habilita el select multiple
			$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').removeAttr('disabled');
			
			$('.selectorCurso select[name="idSelectCurso"]').removeAttr('disabled');
			
			$('.contenido_correo .addSeguimiento input[type="hidden"]').val('0');
		}
		else
		{
			$('#frm_reenvio_1_msg').html('');
			
			// descheckea el otro checkbox
			$('.contenido_correo input:checkbox[name="enviar_a"][value="curso"]').removeAttr('checked');
			
			$('#frm_gestor_reenvio_destinatarios input[name="idSelectCurso[]"]').attr('disabled', 'disabled');
			
			// deshabilita el select multiple y deselecciona sus elementos
			//$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').attr('disabled', 'disabled');
			$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').attr('disabled', 'disabled');
			$('#frm_gestor_reenvio_destinatarios input[data-type="alumno"][name="idalumnos[]"]').attr('checked', 'checked');
			
			$('.selectorCurso select[name="idSelectCurso"]').attr('disabled', 'disabled');
			
			$('.contenido_correo input[name="addSeguimiento"]').val('1');
		}
		
		//$.uniform.update();
		
		if(!$('.contenido_correo input:checkbox[name="enviar_a"][value="curso"]').prop('checked')
			&& !$('.contenido_correo input:checkbox[name="enviar_a"][value="conv"]').prop('checked'))
		{
			$('.contenido_correo input[name="addSeguimiento"]').val('0');
		}
	});
	
	// sirve para que cuando se envien un correo a toda el curso se deshabiliten el envio a la convocatoria y el envio individual por elumnnos
	$('.contenido_correo input:checkbox[name="enviar_a"][value="curso"]').on('click', function()
	{
		$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').removeAttr('checked');
		$('#frm_gestor_reenvio_destinatarios input[name="idalumnos[]"]').removeAttr('disabled');
		
		if(!$(this).prop('checked'))
		{
			$('.contenido_correo input[name="addSeguimiento"]').val('0');
		}
		else
		{
			$('#frm_reenvio_1_msg').html('');
			
			$('.selectorCurso select[name="idSelectCurso"]').removeAttr('disabled');
			
			// descheckea el otro checkbox
			$('.contenido_correo input:checkbox[name="enviar_a"][value="conv"]').removeAttr('checked');

			// deshabilita el select multiple y deselecciona sus elementos
			$('#frm_gestor_reenvio_destinatarios input[data-type="rrhh"][name="idalumnos[]"]').attr('disabled', 'disabled');
			$('#frm_gestor_reenvio_destinatarios input[data-type="alumno"][name="idalumnos[]"]').attr('checked', 'checked');
			
			$('.contenido_correo input[name="addSeguimiento"]').val('1');
		}
		
		//$.uniform.update();
		
		if(!$('.contenido_correo input:checkbox[name="enviar_a"][value="curso"]').prop('checked')
			&& !$('.contenido_correo input:checkbox[name="enviar_a"][value="conv"]').prop('checked'))
		{
			$('.contenido_correo input[name="addSeguimiento"]').val('0');
		}
	});
});

/* --------------- */

$('#frm_reenvio').submit(function(event)
{
	var destinatarios = $(this).find('.menvio select').val();
	if($(this).find('#frm_gestor_reenvio_destinatarios input[type="checkbox"]:checked').val() == null && $(this).find('.menuMasivoTutor input:checked').size() == 0)
	{
		$('#frm_reenvio_1_msg').html('<div class="error">Seleccione un/os destinatario/s</div>');
		
		setTimeout(function()
		{
			$('#popupModal_gestor_correos').find('.popupModalHeaderLoader').addClass('hide');
		},500);
		
		event.preventDefault();
	}
	else
	{
		$('#frm_reenvio_1_msg').html('');
	}
});

$('#frm_reenvio').find('.menvio select').change(function()	
{
	$('#frm_reenvio_1_msg').html('');
});

/* --------------- */

function correoBotonImportancia()
{
	if($('.contenido_correo .importanciaAlta.importanciaAltaActive').size() == 0)
	{
		$('.contenido_correo .importanciaAlta').addClass('importanciaAltaActive');
		$('.contenido_correo .importanciaAlta input[type="hidden"]').val('1');
	}
	else
	{
		$('.contenido_correo .importanciaAlta').removeClass('importanciaAltaActive');
		$('.contenido_correo .importanciaAlta input[type="hidden"]').val('0');
	}
}

function correoEliminarAdjuntoBorrador(elemento)
{
	$(elemento).parent('div').remove();
}