gestorCorreoReedimensionar();
correoCheckboxAllNone();
$('#popupModal_gestor_correos').resize(function()
{
	gestorCorreoReedimensionar();
});

function gestorCorreoReedimensionar()
{
	$('#popupModal_gestor_correos .carpetas_correo').css('width', '280px');
	
	var width = $('#popupModal_gestor_correos').find('.popupModalContent').width() - 280;
	
	$('#popupModal_gestor_correos .contenido_correo').css('width', width + 'px');
	
}

//------------------------//

function correoArchivar()
{
	$('#general_correo_tooltip_menu').removeClass('hide');
	
	$('#general_correo_tooltip_menu').mouseleave(function()
	{
		$('#general_correo_tooltip_menu').addClass('hide');
	});
	//$("#correo_hacer").val("archivar");document["frm_seleccion_correo"].submit();
}

function correoArchivarForm(idCarpeta)
{
	$('#correo_hidden_idcarpeta_personal').val(idCarpeta);
	
	$('#correo_hidden_idcarpeta_personal').parents('form').submit();
}

function correoArchivarAuto()
{
	$("#correo_hacer").val("archivar");
	document["frm_seleccion_correo"].submit();
}

function correoDesarchivar()
{
	$("#correo_hacer").val("desarchivar");
	document["frm_seleccion_correo"].submit();
}

function correoCheckboxAllNone()
{
	$('.encabezado_correo .asunto_s').off('click');
	$('.encabezado_correo .asunto_s').on('click', function()
	{
		$('.mensajes_correo .asunto_s').each(function()
		{
			if($(this).find('input[type="checkbox"]').attr('checked') == undefined)
			{
				$(this).find('input[type="checkbox"]').attr('checked', 'checked');
			}
			else
			{
				$(this).find('input[type="checkbox"]').removeAttr('checked');
			}
		});
			
		return false;
	});
}