$("#frm_reenvio").validate({
		errorElement: "div",
		messages: {
			reenvioAsunto: {
				required : 'Introduce un asunto para el correo'
			},
			reenvioMensaje: {
				required : 'Introduce unmensaje para el correo'
			}
		},
		rules: {
			reenvioAsunto: {
				required  : true
			},
			reenvioMensaje: {
				required  : true
			}
		}
	});