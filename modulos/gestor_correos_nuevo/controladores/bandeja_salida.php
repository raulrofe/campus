<?php
require_once("basic.php");

//print_r($post);

if(!isset($post['mandar']) and isset($post['hacer']) and $post['hacer'] == 'eliminar')
{
	//print_r($post['idmensaje']);
	if(isset($post['idmensaje'])) 
	{
		//Alerta::mostrarMensajeInfo('enviadonoeliminar','Los mensajes enviados no se pueden eliminar');

		if ($mi_correo->enviar_papelera($post['idmensaje'],'salida'))
		{
			Alerta::mostrarMensajeInfo('mensajepapelera','Mensaje enviado a la papelera');
		}		
	}
	else
	{
		Alerta::mostrarMensajeInfo('seleccionamensajeeliminar','Debes seleccionar al menos un mensaje para poder eliminarlo');
	}		
}

/*
if(isset($post['hacer']) and $post['hacer'] == 'archivar')
{
	if(isset($post['idmensaje']))
	{
	$idmensaje = $post['idmensaje'];
		for($i=0;$i<=count($idmensaje)-1;$i++)
		{
			$detalleMensaje = $mi_correo->mostrar_mensaje($idmensaje[$i]);
			$idCarpeta = $detalleMensaje['idcurso'];
			if($perfil != 'alumno') $idDestinatario = 't_'.$_SESSION['idusuario'];
			else $idDestinatario = $_SESSION['idusuario']; 
			$mi_correo->organizar_mail($idCarpeta,$idDestinatario,$idmensaje[$i]);
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('seleccionamensajemover','Debes seleccionar al menos un mensaje para poder moverlo');
	}
}
*/

if(isset($post['hacer']) and $post['hacer'] == 'archivar')
{
	Alerta::mostrarMensajeInfo('enviadonoeliminar','Los mensajes enviados no pueden ser archivados');
	$registros_correos = $mi_correo->mensajes_enviados();
}

else
{
	

	$numPagina = 1;
	if(isset($get['pagina']) && is_numeric($get['pagina']))
	{
		$numPagina = $get['pagina'];
	}
	
	// obtiene los mensajes con paginado
	$registros_correos_count = $mi_correo->mensajes_enviados();
	$maxElementsPaging = 20;
	$maxPaging = $registros_correos_count->num_rows;
	$maxPaging = ceil($maxPaging / $maxElementsPaging);
	
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	
	//$registros_correos = $mi_correo->mensajes_enviados($elementsIni, $maxElementsPaging);
	$registros_correos = $mi_correo->mensajes_enviados();
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
