<?php
require_once("basic.php");

if(isset($post['hacer']) and $post['hacer'] == 'eliminar')
{
	if(isset($post['idmensaje']))
	{
		$mi_correo->enviar_papelera($post['idmensaje'],'borrador');
	}
	else
	{
		Alerta::mostrarMensajeInfo('seleccionamensajeeliminar','Debes seleccionar al menos un mensaje para poder eliminarlo');
	}		
}


	$numPagina = 1;
	if(isset($get['pagina']) && is_numeric($get['pagina']))
	{
		$numPagina = $get['pagina'];
	}
	
	// obtiene los mensajes con paginado
	$registros_correos_count = $mi_correo->bandeja_borrador();
	$maxElementsPaging = 20;
	$maxPaging = $registros_correos_count->num_rows;
	$maxPaging = ceil($maxPaging / $maxElementsPaging);
	
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	
	$registros_correos = $mi_correo->bandeja_borrador();
	//$registros_correos = $mi_correo->bandeja_borrador($elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');