<?php
require_once("basic.php");

//print_r($post);

if(isset($post['hacer']) && $post['hacer'] == 'restaurar')
{
	if(isset($post['idmensaje']) && is_array($post['idmensaje']))
	{
		$mi_correo->restaurarMensaje($post['idmensaje']);
	}
}

if(!isset($post['mandar']) and isset($post['hacer']) and $post['hacer'] == 'eliminar')
{
	if(isset($post['idmensaje'])) $idmensaje = $post['idmensaje'];
	if(isset($idmensaje))
	{
		if ($mi_correo->eliminar_definitivamente($idmensaje))
		{
			Alerta::guardarMensajeInfo('mensajeeliminado','Mensaje eliminado');
			//Url::redirect('correos/papelera');
		}	
	}	
	else
	{
		Alerta::mostrarMensajeInfo('seleccionamensajeeliminar','Debes seleccionar al menos un mensaje para poder eliminarlo');
	}		
}

if(isset($post['hacer']) && $post['hacer'] == 'archivar')
{
	Alerta::mostrarMensajeInfo('papeleranoarchivados','Los mensajes de la papelera no pueden ser archivados');
}

/*
$registros_correos = $mi_correo->mensajes_papelera($numPagina);
$numero_registros = mysqli_num_rows($registros_correos);
$maxElementsPaging = 20;	
$maxPaging = ceil($numero_registros / $maxElementsPaging);
$elementsIni = ($numPagina - 1) * $maxElementsPaging;
*/

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

// obtiene los mensajes con paginado
$registros_correos_count = $mi_correo->mensajes_papelera();
$maxElementsPaging = 20;
$maxPaging = 0;
if(isset($registros_correos_count['id']))
{
	$maxPaging = count($registros_correos_count['id']);
	$maxPaging = ceil($maxPaging / $maxElementsPaging);
}

$elementsIni = ($numPagina - 1) * $maxElementsPaging;

$papelera = $mi_correo->mensajes_papelera();
//$papelera = $mi_correo->mensajes_papelera($elementsIni, $maxElementsPaging);
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');