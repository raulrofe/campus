<?php
require_once("basic.php");

//$objModeloCurso = new Gestor_correos();

if(isset($get['idcorreo']) && is_numeric($get['idcorreo']))
{
	$idcorreo = $get['idcorreo'];
	
	$detalle_mensaje = $mi_correo->mostrar_mensaje($idcorreo);
	if(!empty($detalle_mensaje))
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			$pathAdjuntosBorrador = PATH_ROOT . 'archivos/correo/' . $detalle_mensaje['idcorreos'] . '/';
			
			$adjuntosBorrador = array();
			if(isset($post['adjuntosBorrador']) && is_array($post['adjuntosBorrador']))
			{
				if(!file_exists($pathAdjuntosBorrador))
				{
					mkdir($pathAdjuntosBorrador);	
				}
				
				foreach($post['adjuntosBorrador'] as $idFichero)
				{
					$filaUnAdjunto = $mi_correo->obtenerUnAdjunto($detalle_mensaje['idcorreos'], $idFichero);
					if($filaUnAdjunto->num_rows == 1)
					{
						$filaUnAdjunto = $filaUnAdjunto->fetch_object();
						
						$adjuntosBorrador[] = $filaUnAdjunto->idficheros_adjuntos;
					}
				}
			}
			
			$arrayAdjuntosNuevo = array();
			
			$adjuntosActuales = $mi_correo->obtenerTodosAdjuntos($detalle_mensaje['idcorreos']);
			$n_archivo = $adjuntosActuales->num_rows;
			
			// adjuntos por subida de archivos
			if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]))
			{
				if(!file_exists(PATH_ROOT . "archivos/correo/".$detalle_mensaje['idcorreos']))
				{
					mkdir(PATH_ROOT . "archivos/correo/".$detalle_mensaje['idcorreos']);	
				}
				$directorio = 'archivos/correo/'.$detalle_mensaje['idcorreos'].'/';
				$tamano_total=0;
			
				foreach ($_FILES['archivo']['error'] as $key => $error) 
				{		
					if ($error == UPLOAD_ERR_OK) 
					{
						// deberia ser $archivosValidos
						$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
						if($archivosNoValidos)
						{
							//echo $error_codes[$error];		
							$nombre_fichero = $_FILES["archivo"]["name"][$key];
							$enlace_fichero = $directorio . Texto::quitarTildes($n_archivo . "_" .$nombre_fichero);
						    move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero) or die("Ocurrio un problema al intentar subir el archivo.");
							$sql = "INSERT into ficheros_adjuntos (nombre_fichero,enlace_fichero,idcorreos) VALUES 
							('$nombre_fichero','$enlace_fichero','" . $detalle_mensaje['idcorreos'] . "')";
							
							//echo $sql;
							mysqli_query($con,$sql) or die ("error al insertar los fichero adjuntos");
		
							$id_ultimoarchivo = "SELECT MAX(idficheros_adjuntos) from ficheros_adjuntos";
							$ultimo_archivo = mysqli_query($con,$id_ultimoarchivo);
							$maxfa = mysqli_fetch_row($ultimo_archivo);
		
							$consulta = "INSERT into adjuntos (idcorreos,idficheros_adjuntos) VALUES ('" .$detalle_mensaje['idcorreos'] . "','$maxfa[0]')";
							mysqli_query($con,$consulta) or die ("error al insertar la relacion de correo con ficheros");
							
							$arrayAdjuntosNuevo[] = $maxfa[0];
						}
						else
						{
							Alerta::mostrarMensajeInfo('archivonovalido','El archivo adjunto no es v&aacute;lido');
						}
					}
					$n_archivo++;
				}
			}
			
				
			// adjuntos actuales del correo
			$adjuntosActuales = $mi_correo->obtenerTodosAdjuntos($detalle_mensaje['idcorreos']);
			if($adjuntosActuales->num_rows > 0)
			{
				while($item = $adjuntosActuales->fetch_object())
				{
					if(!in_array($item->idficheros_adjuntos, $arrayAdjuntosNuevo)
						&& !in_array($item->idficheros_adjuntos, $adjuntosBorrador))
					{
						// eliminar de BBDD
						$mi_correo->eliminarAdjunto($detalle_mensaje['idcorreos'], $item->idficheros_adjuntos);
						//@unlink($pathAdjuntosBorrador . $item->nombre_fichero);
					}
				}
			}
			
			//$arrayAdjuntosEliminar = array_diff($arrayAdjuntosAct, $arrayAdjuntosNuevo);
			
			if(isset($post['mensaje'], $post['asunto']) && !empty($post['mensaje']) && !empty($post['asunto']))
			{
				$postImportante = '0';
				if(isset($post['importante']) && $post['importante'] == 1)
				{
					$postImportante = '1';
				}
				
				$mi_correo->actualizarCorreo($detalle_mensaje['idcorreos'], $post['mensaje'], $post['asunto'], $postImportante);
							
				Url::redirect('correos/borrador');
			}
			
			//var_dump($arrayAdjuntosNuevo);
			//var_dump($adjuntosBorrador);
		}
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
	}
}