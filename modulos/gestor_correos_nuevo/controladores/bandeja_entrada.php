<?php
require_once("basic.php");

if(isset($post['hacer']) and $post['hacer'] == 'eliminar')
{
	if(isset($post['idmensaje']))
	{	
		$mi_correo->enviar_papelera($post['idmensaje'],'entrada');
	}
	else
	{
		Alerta::mostrarMensajeInfo('seleccionamensajeeliminar','Debes seleccionar al menos un mensaje para poder eliminarlo');
	}		
}

if(isset($post['hacer']) and $post['hacer'] == 'archivar')
{
	if(isset($post['idmensaje']))
	{
		$idmensaje = $post['idmensaje'];
		for($i=0;$i<=count($idmensaje)-1;$i++)
		{
			$detalleMensaje = $mi_correo->mostrar_mensaje($idmensaje[$i]);
			$idCarpeta = $detalleMensaje['idcurso'];
			if($perfil != 'alumno') $idDestinatario = 't_'.$_SESSION['idusuario'];
			else $idDestinatario = $_SESSION['idusuario']; 
			
			$mi_correo->eliminarCorreoDeCarpetaPersonal($idmensaje[$i]);
			$mi_correo->organizar_mail($idCarpeta,$idDestinatario,$idmensaje[$i]);
		}
		
		Alerta::guardarMensajeInfo('mensajeautoarchivado','El mensaje/s ha sido autoarchivado en la carpeta correspondiente');
		Url::redirect($post['url-referer']);
	}
	else
	{
		Alerta::mostrarMensajeInfo('mensajemoverlo','Debes seleccionar al menos un mensaje para poder moverlo');
	}
}
else if(isset($post['hacer']) and $post['hacer'] == 'desarchivar')
{
	if(isset($post['idmensaje']))
	{
		$idmensaje = $post['idmensaje'];
		for($i=0;$i<=count($idmensaje)-1;$i++)
		{			
			$mi_correo->eliminarCorreoDeCarpetaCurso($idmensaje[$i]);
			$mi_correo->eliminarCorreoDeCarpetaPersonal($idmensaje[$i]);
		}
			
		Alerta::guardarMensajeInfo('mensajedesarchivado','El mensaje/s ha sido desarchivado');
		Url::redirect($post['url-referer']);
	}
	else
	{
		Alerta::mostrarMensajeInfo('debesseleccionarmensaje','Debes seleccionar al menos un mensaje');
	}
}

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

// obtiene los mensajes con paginado
$registros_correos_count = $mi_correo->bandeja_entrada();
$maxElementsPaging = 20;
$maxPaging = $registros_correos_count->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;

$registros_correos = $mi_correo->bandeja_entrada();
//$registros_correos = $mi_correo->bandeja_entrada($elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');