<?php
$db = new database();
$con = $db->conectar();

$mi_correo = new Gestor_correos();
$mi_usuario = new Usuarios();
$mi_alumno = new Alumnos();
$mi_curso = new Cursos();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

$urlPagingEnd = null;
$textBuscarHighlight = '';
if(isset($get['textBuscar']))
{
	$urlPagingEnd = '&textBuscar=' . urlencode($get['textBuscar']);
	$textBuscarHighlight = $get['textBuscar'];
}

//Cargo el controlador de foro para el paginado
mvc::cargarModuloSoporte('foro');

$extensionesEmail = array(
				'cpt',
				'csv',
				'rar',
				'psd',
				'pptx',
				'pdf',
				'ai',
				'eps',
				'ps',
				'odt',
				'xls',
				'ppt',
				'pptx',
				'gtar',
				'gz',
				'gzip',
				'swf',
				'tar',
				'tgz',
				'xhtml',
				'zip',
				'mid',
				'midi',
				'mpga',
				'mp2',
				'mp3',
				'wav',
				'bmp',
				'gif',
				'jpeg',
				'jpg',
				'jpe',
				'png',
				'tiff',
				'tif',
				'css',
				'html',
				'htm',
				'txt',
				'text',
				'xml',
				'xsl',
				'mpeg',
				'mpg',
				'mpe',
				'qt',
				'mov',
				'avi',
				'movie',
				'doc',
				'docx',
				'xlsx',
				'word',
				'xl',
				'3g2',
				'3gp',
				'mp4',
				'm4a',
				'f4v',
				'aac',
				'vlc',
				'wmv',
				'au',
				'ac3',
				'flac',
				'ogg',
				'kmz',
				'kml'
			);

if(isset($post['idSelectCurso']) and !empty($post['idSelectCurso']))
{
	$idSelectCurso = $post['idSelectCurso'];
}
else 
{
	$idSelectCurso = $_SESSION['idcurso'];
}

$perfil = Usuario::getProfile();

	if($perfil != 'alumno')
	{
		 $idusuario = Usuario::getIdUser(true);
		 $resultado = $mi_usuario->buscar_usuario();
		 $datos_usuario = mysqli_fetch_assoc($resultado);
		 $nombre_usuario = $datos_usuario['nombrec']; 
	}
	
	else
	{
		$idusuario = Usuario::getIdUser();
		$mi_alumno->set_alumno($idusuario);
		$resultado = $mi_alumno->ver_alumno();
		$datos_usuario = mysqli_fetch_assoc($resultado);
		$nombre_usuario = $datos_usuario['nombre']." ".$datos_usuario['apellidos']; 
	} 
	
	
if(!empty ($get['todo']))
{
	$todo = $get['todo'];
	if($todo == 'no') $todo2 = 'si';
	else $todo2 = 'no'; 
}
else
{
	$todo2 = 'si';
	$todo='no';
}

// buscamos los usuarios que pertenecen al curso
$staff_curso = $mi_usuario->buscar_usuarios_curso($idSelectCurso);

// clase para los alumnos que pertenecen al curso
$registro_alumnos = $mi_alumno->alumnos_del_curso_activos($idSelectCurso); 

// Obtenemos los mensajes sin leer bandeja de entrada
$mensajes_sinleer = $mi_correo->mensajes_sin_leer();
$mensajes_sin_leer = mysqli_num_rows($mensajes_sinleer);

// Obtenemos los mensajes sin leer papelera
$mensajes_sinleer_papelera = $mi_correo->mensajes_sin_leer_papelera();
$mensajesPapeleraNoleidos = mysqli_num_rows($mensajes_sinleer_papelera);

//Obtenemos los mensajes sin leer de la subcarpeta elegida
$mensajes_sinleer_subcarpeta = $mi_correo->mensajes_sin_leer_subcarpeta();
$mensajesSubcarpetaNoleidos = mysqli_num_rows($mensajes_sinleer_subcarpeta);


//Buscamos los cursos que pertenecen al usuario para crear las carpetas para organizarlas
$registros_carpetas = $mi_curso->cursos_usuario();
$carpetasPersonales = $mi_correo->obtenerCarpetas(Usuario::getidUser(true));

$carpetasPersonalesArray = array();
while($carpetaPersonal = $carpetasPersonales->fetch_object())
{
	$carpetasPersonalesArray[] = $carpetaPersonal;
}

//Mostramos el nombre de la carpeta
$carpeta = ucfirst($get['c']);
$carpeta = str_replace('_',' ',$carpeta);

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

if(isset($post['hacer']) and $post['hacer'] == 'noleido')
{
	if(isset($post['idmensaje']))
	{
		$idDelCorreo = $post['idmensaje'];	
		if($mi_correo->marcarNoleido($idDelCorreo))
		{
			Alerta::mostrarMensajeInfo('accioncompletada','La accion se ha completado correctamente');
		}
	}
	
	else if(isset($get['idcorreo']) && is_numeric($get['idcorreo']))
	{
		$idDelCorreo = $get['idcorreo'];
		if($mi_correo->marcarNoleido(array($idDelCorreo)))
		{
			Alerta::guardarMensajeInfo('accioncompletada','La accion se ha completado correctamente');
			
			switch($get['menu'])
			{
				case 'carpeta':
					if(isset($post['idcarpeta_auto']) & is_numeric($post['idcarpeta_auto']))
					{
						Url::redirect('correos/subcarpeta/' . $post['idcarpeta_auto']);
					}
					break;
				default:
					Url::redirect('correos/' . $get['menu']);
			}
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('marcarmensaje','Debes seleccionar al menos un mensaje para marcarlo como No leído/Leído');
	}		
}

// asigna un correo/os a una carpeta personal
if(isset($post['idcarpeta_personal']) && is_numeric($post['idcarpeta_personal']))
{
	if(isset($post['idmensaje']) && is_array($post['idmensaje']))
	{
		$resultCarpPersonales = $mi_correo->obtenerUnaCarpeta($post['idcarpeta_personal'], $idusuario);
		if($resultCarpPersonales->num_rows > 0)
		{
			foreach($post['idmensaje'] as $item)
			{
				$resultUnCorreo = $mi_correo->mostrar_mensaje($item);
				if(!empty($resultUnCorreo))
				{
					$mi_correo->eliminarCorreoDeCarpetaPersonal($item);

					if($mi_correo->eliminarCorreoDeCarpetaCurso($item) && $mi_correo->asignarCarpetaACorreo($item, $post['idcarpeta_personal']))
					{
						Alerta::guardarMensajeInfo('mensajearchivado','El mensaje ha sido archivado en la carpeta');
					}
				}
			}
		}
	}
	else
	{
		Alerta::guardarMensajeInfo('seleccionamensaje','Selecciona algún mensaje');
	}
}
if(isset($get['ver_idcarpeta']) and !empty($get['ver_idcarpeta']) and empty($get['textBuscar']))
{
	if(isset($post['mandar'], $post['hacer']) && $post['hacer'] == 'eliminar' && $post['mandar'] == 'papelera')
	{
		if($mi_correo->enviar_papelera($post['idmensaje'], 'entrada'))
		{
			Alerta::guardarMensajeInfo('mensajepapelera','EL mensaje ha sido enviado a la papelera');
		}
	}
	$verIdcarpeta = $get['ver_idcarpeta'];
	$registros_correos = $mi_correo->bandeja_carpetas($verIdcarpeta, $numPagina);
	$numero_registros = mysqli_num_rows($registros_correos);
	$maxElementsPaging = 20;	
	$maxPaging = ceil($numero_registros / $maxElementsPaging);
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	$registros_correos = $mi_correo->bandeja_carpetas($verIdcarpeta);
	//$registros_correos = $mi_correo->bandeja_carpetas($verIdcarpeta, $elementsIni, $maxElementsPaging);
	$mensaje_carpetas = 'No existen mensajes archivados en esta carpeta';
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
}	
else if($get['c'] == 'bandeja_entrada' && isset($get['idcarpeta']) and !empty($get['idcarpeta']) and empty($get['textBuscar']))
{
	$rowCarpeta = $mi_correo->obtenerUnaCarpeta($get['idcarpeta'], Usuario::getIdUser(true));
	if($rowCarpeta->num_rows == 1)
	{
		if(isset($post['mandar'], $post['hacer']) && $post['hacer'] == 'eliminar' && $post['mandar'] == 'papelera')
		{
			if($mi_correo->enviar_papelera($post['idmensaje'], 'entrada'))
			{
				Alerta::guardarMensajeInfo('mensajepapelera','El mensaje ha sido enviado a la papelera');
			}
		}
		$verIdcarpeta = $get['idcarpeta'];
		$registros_correos = $mi_correo->bandeja_carpetas_personales($verIdcarpeta, $numPagina);
		$numero_registros = mysqli_num_rows($registros_correos);
		$maxElementsPaging = 20;	
		$maxPaging = ceil($numero_registros / $maxElementsPaging);
		$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		$registros_correos = $mi_correo->bandeja_carpetas_personales($verIdcarpeta);
		//$registros_correos = $mi_correo->bandeja_carpetas_personales($verIdcarpeta, $elementsIni, $maxElementsPaging);
		$mensaje_carpetas = 'No existen mensajes archivados en esta carpeta';
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
	}
}
elseif(!empty($get['textBuscar']))
{

	$textBuscar = '';
	
	if(isset($get['textBuscar'])) $textBuscar = $get['textBuscar'];
	$registros_correos = $mi_correo->textBuscar($textBuscar, $numPagina);

	exit('busca');

	$numero_registros = mysqli_num_rows($registros_correos);
	$maxElementsPaging = 20;	
	$maxPaging = ceil($numero_registros / $maxElementsPaging);
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	$registros_correos = $mi_correo->textBuscar($textBuscar);
	//$registros_correos = $mi_correo->textBuscar($textBuscar, $elementsIni, $maxElementsPaging);
	$mensaje_buscar = 'No se encontraron mensajes para la busqueda realiza';
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
}
else if(!isset($post['mandar']) and isset($post['hacer']) and $post['hacer'] == 'eliminar')
{
	if(isset($post['idmensaje_correo'],$post['origen']) && is_numeric($post['idmensaje_correo']))
	{
		if($post['origen'] != 'correos/papelera')
		{
			if($post['origen'] == 'correos/bandeja_entrada')
			{
				$bandeja_papelera = 'entrada';
				$redirigir = 'correos/bandeja_entrada';
			}
			else
			{
				$bandeja_papelera = 'salida';
				$redirigir = 'correos/bandeja_salida';
			}
			
			if ($mi_correo->enviar_papelera(array($post['idmensaje_correo']), $bandeja_papelera))
			{
				Alerta::guardarMensajeInfo('mensajepapelera','Mensaje enviado a la papelera');
			}	
		}
		else
		{
			$redirigir = 'correos/papelera';
			if ($mi_correo->eliminar_definitivamente($post['idmensaje_correo']))
			{
				Alerta::guardarMensajeInfo('mensajeeliminado','Mensaje eliminado');
			}		
		}	
		
		Url::redirect($post['origen']);
	}	
}
