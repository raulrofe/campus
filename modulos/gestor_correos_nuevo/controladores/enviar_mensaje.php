<?php
require_once("basic.php");
$post = Peticion::obtenerPost();

//var_dump($post);

//Obtengo el valor del id curso
$idusuario = $_SESSION['idusuario'];
if( isset($post['idSelectCurso']) && is_numeric($post['idSelectCurso'])) 
{
	$idcurso = $post['idSelectCurso'];
}
else 
{
	$idcurso = $_SESSION['idcurso'];
}

// Compruebo el perfil para establecer el id del remitente (el que envia el mensaje)
if(Usuario::compareProfile('alumno'))
{
	$remitente = $_SESSION['idusuario'];	
}
else
{
		$remitente = "t_".$_SESSION['idusuario'];
}

//Obtengo los valores  asunto, mensaje y si hay algun alumno seleccionado desde el select multiple
if(Peticion::isPost())
{
	if(isset($post['asunto'], $post['mensaje']))
	{
		$asunto = $post['asunto'];
		$mensaje = $post['mensaje'];
		
		if (isset ($post['idalumnos']))
		{
			$idalumnos = $post['idalumnos'];
		}
		if(isset ($post['enviar_a'])) 
		{
			$enviar_a = $post['enviar_a'];
		}
	}
	
	// Obtengo el valor de importante
	if(isset($post['importante']) and is_numeric($post['importante']))
	{
		$importante = $post['importante'];
	}
	else 
	{
		$importante = 0;	
	}
	
	//obtengo fecha para base de datos
	$fecha = Fecha::fechaActual();
	$fecha_format = Fecha::obtenerFechaFormateada($fecha);

	// Compruebo el perfil para establecer el id del remitente (el que envia el mensaje)
	$remitente = Usuario::getIdUser(true);

}

//Establezco el remitente si existe por gets
if(isset($get['idremitente'])) 
{
	$idremitente = $get['idremitente'];
}

if(isset($post['asunto'], $post['mensaje']) && !empty ($asunto) and !empty($mensaje))
{
	//Insertamos el correo y obtenemo su id
	$mi_correo->insertarCorreo($asunto, $mensaje, $idcurso, $importante);
	$ultimoIdCorreo = $mi_correo->obtenerUltimoIdInsertado();

	if(isset($post['actionBoton']) && $post['actionBoton'] == 'Enviar')
	{
		if(isset ($idalumnos))
		{
			mvc::importar('lib/mail/phpmailer.php');
			mvc::importar('lib/mail/mail.php');
				
			$objMail = Mail::obtenerInstancia();
			$rowEntOrg = $mi_correo->obtenerConfigEntidadOrg();
			$rowEntOrg = $rowEntOrg->fetch_object();
				
			for($i=0;$i<=count($idalumnos)-1;$i++)
			{
				if(isset($cursoAlumno))
				{
					$idcurso = $cursoAlumno[$i];
				}		
				
				$resultadoAlumno = $mi_correo->obtenerUsuarioDestinatario($idalumnos[$i], $idcurso);
				if($resultadoAlumno->num_rows == 1)
				{
					$rowAlumno = mysqli_fetch_assoc($resultadoAlumno);
					if($mi_correo->insertarDestinatario($remitente, $idalumnos[$i], $fecha, $ultimoIdCorreo))
					{
						//Incluimos el mensaje en seguimietno
						if(isset($post['addSeguimiento']) && $post['addSeguimiento'] == 1)
						{
							if(Usuario::checkAlumno($idalumnos[$i]))
							{
								$ultimoIdDestinatario = $mi_correo->obtenerUltimoIdInsertado();
								$idMatricula = Usuario::getIdMatriculaDatos($idalumnos[$i], $idcurso);
								$mi_correo->incluirCorreoSeguimiento($asunto, $fecha, $idMatricula, $ultimoIdDestinatario);	
							}
						}
						
						// envia una notificacion de email
						/*
						if(isset($rowAlumno['notifi_correo']) && $rowAlumno['notifi_correo'] == '1')
						{
							$enviado = $objMail->enviarNotificacion($rowEntOrg->email_notificaciones, 'Campus Aula Interactiva - Aula_SMART', $rowAlumno['email'],
											$rowAlumno['nombre'], Usuario::getNameUser(Usuario::getIdUser(true)), $post['mensaje'], $post['asunto']);
						}
						*/
					}
				}
			} //END FOR
	
			$n_archivo = 0;
			
			if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]) && count($_FILES['archivo']) > 0)
			{
				if(!file_exists(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo))
				{
					mkdir(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo, 0777);	
				}
								
				$directorio = 'archivos/correo/'.$ultimoIdCorreo.'/';
				$tamano_total=0;
			
				foreach ($_FILES['archivo']['error'] as $key => $error) 
				{	
					if ($error == UPLOAD_ERR_OK) 
					{	
						$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
						if($archivosNoValidos)
						{
							//echo $error_codes[$error];		
							$nombre_fichero = $_FILES["archivo"]["name"][$key];
							$enlace_fichero = $directorio . Texto::quitarTildes($n_archivo . "_" .$nombre_fichero);
						    if(move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero))
						    {
						    	$mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoIdCorreo);
								$maxfa = $mi_correo->obtenerUltimoIdInsertado();
								if($mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $maxfa))
								{
									Alerta::mostrarMensajeInfo('archivoadjuntado','El archivo se ha adjuntado correctamente');
								}	
						    	else
								{
									Alerta::mostrarMensajeInfo('archivonovalido','El archivo adjunto no es v&aacute;lido');
								}
						    }
						}
				}
					$n_archivo++;
				} //END FOREACH
			} //END IF EXISTE ARCHIVO
		
			$notificacion = 'Su mensaje se ha enviado correctamente';
			$clave='mensajeenviado';
			
			if(isset($archivosNoValidos) && (!$archivosNoValidos)) 
			{ 
				$notificacion.= ' , algunos archivos no fueron adjuntados por su extension';
				$clave='mensajeenviadopero';
			} 
			Alerta::guardarMensajeInfo($clave, $notificacion);
			Url::redirect('correos/bandeja_entrada');
		} // END POST ACTION ENVIAR
		
		if(!empty($post) && empty ($enviar_a) && empty ($idalumnos))
		{
			Alerta::mostrarMensajeInfo('algundestinatario','Debes seleccionar algun destinatario');
		}
	}

	//Si pulsamos el boton de "Guardar como borrador" en enviar mensaje
	else if(isset($post['actionBoton']) && $post['actionBoton'] == 'borrador')
	{
		$mi_correo->insertarCorreo($post['asunto'], $post['mensaje'], $idcurso, $importante, '1');
		$ultimoIdCorreo = $mi_correo->obtenerUltimoIdInsertado();
		
		$n_archivo = 0;
		
		if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]) && count($_FILES['archivo']) > 0)
		{
			if(!file_exists(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo))
			{
				mkdir(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo);	
			}
			$directorio = 'archivos/correo/'.$ultimoIdCorreo.'/';
			$tamano_total=0;
		
			foreach ($_FILES['archivo']['error'] as $key => $error) 
			{		
				if ($error == UPLOAD_ERR_OK) 
				{	
					$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
					if($_FILES['archivo']['type'][$key] != 'application/octet-stream' && $_FILES['archivo']['type'][$key] != 'application/x-msdownload')
					{
						//echo $error_codes[$error];		
						$nombre_fichero = $_FILES["archivo"]["name"][$key];
						$enlace_fichero = $directorio . Texto::quitarTildes($n_archivo . "_" . $nombre_fichero);
					    move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero) or die("Ocurrio un problema al intentar subir el archivo.");
						$mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoIdCorreo);
						$id_ultimoarchivo = $mi_correo->obtenerUltimoIdInsertado();
		
						$mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $id_ultimoarchivo);
					}
					else
					{
						Alerta::mostrarMensajeInfo('archivonovalido','El archivo adjunto no es v&aacute;lido');
					}
				}
				$n_archivo++;
			} //END FOREACH
		}
			
		//Insertamos el destinatario
		if($mi_correo->insertarDestinatario($remitente, NULL, $fecha, $ultimoIdCorreo))
		{
			Alerta::guardarMensajeInfo('El mensaje ha sido guardado como borrador');

			Url::redirect('correos/bandeja_entrada');
		}
	} //END IF POST ACTION GUARDAR COMO BORRADOR
} //END IF EXISTE ASUNTO Y MENSAJE

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
