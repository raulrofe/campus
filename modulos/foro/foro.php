<?php
class foro
{
	private static $_footerOptions = true;
	
	private function __construct() {}
	
	public static function listarMensajes($mensajes, $ids)
	{
		$modeloForo = new modeloForo();
		
		$html = '';
		$cont=0;
		$nRespuesta = 0;
		while($mensaje = $mensajes->fetch_object())
		{
			$mensajes_respuesta = $modeloForo->obtenerMensajesRespuesta($mensaje->idforo_mensaje);
			$nRespuesta = $mensajes_respuesta->num_rows;
			$cont++;
			//if($mensaje->id_mensaje_respuesta == null)
			//{
			
				while($m = $mensajes_respuesta->fetch_object())
				{
					if(in_array($m->idforo_mensaje, $ids))
					{
						$ids['responseNueva'][] = $m->id_mensaje_respuesta;
					}
					
				}
				
				$mensajes_respuesta = $modeloForo->obtenerMensajesRespuesta($mensaje->idforo_mensaje);
			
				$html .= self::pintarUnMensaje($mensaje, false, $cont, $nRespuesta, $ids);
				
				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta, $ids);
					}
					$html.="</div>";
				}
			//}
		}
		
		return $html;
	}
	
	/*
	public static function listarMensajesForOneMsg($mensajes)
	{
		$modeloForo = new modeloForo();
		
		$html = '';
		$cont=0;
		$nRespuesta = 0;
		while($mensaje = $mensajes->fetch_object())
		{
			$mensajes_respuesta = $modeloForo->obtenerMensajesRespuesta($mensaje->idforo_mensaje);
			$nRespuesta = $mensajes_respuesta->num_rows;
			$cont++;
			if($mensaje->id_mensaje_respuesta == null)
			{
				$html .= self::pintarUnMensaje($mensaje, false, $cont,$nRespuesta);
				
				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta);
					}
					$html.="</div>";
				}
			}
			else
			{
				$html .= self::pintarUnMensaje($mensaje, false, $cont,$nRespuesta);
				
				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta);
					}
					$html.="</div>";
				}
			}
		}
		
		return $html;
	}*/
	
	/**
	 * Mostrar o no las opciones del pie de los mensajes
	 * Enter description here ...
	 * @param unknown_type $value
	 */
	public static function setConfigFooter($value)
	{
		self::$_footerOptions = $value;
	}
	
	public static function pintarUnMensaje($mensaje, $msg_respuesta = false, $cont, $nRespuesta, $ids)
	{
		
		$perfil = Usuario::esAlumno($mensaje->idusuario);
				
		if(Usuario::compareProfile('alumno'))
		{
			$idUsuarioCreador = Usuario::getIdUser();
		}
		else
		{
			$idUsuarioCreador = Usuario::getIdUser(true);
		}
		
			
		$addClass = '';
		$addId = '';
		if($msg_respuesta)
		{
			$addClass =  'un_mensaje_respuesta respuestaForo';
			$addId = "id='ocultarRespuesta". $cont ."'";
			$imgNuevoForo = 'float:left; margin: -10px 0 0 -111px;';
		}
		else
		{
			$addClass = 'beige';
			$imgNuevoForo = 'float:left; margin: -10px 0 0 -111px;';
		}
		
		if(isset($mensaje->t_foto))
		{
			$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
			$foto = $mensaje->t_foto;
		}
		else
		{
			$nombrec = $mensaje->a_nombrec;
			$foto = $mensaje->a_foto;
		}
	
		$html = '<div '. $addId .' class="elemento ' . $addClass . '">' .
		 			'<div class="elementoAvatar"><img src="imagenes/fotos/' . $foto. '" alt="" />' .
					'<br/><br/>' .
					'<span data-translate-html="perfiles.' . $perfil[0] . '">' . $perfil[0] . '/a</span></div>';
		 	
					if(in_array($mensaje->idforo_mensaje, $ids) && $mensaje->idusuario != $idUsuarioCreador)
					{
						$html .= '<img src="imagenes/foro/new-forum.png" alt="" style="' . $imgNuevoForo . '"/>';
					}
			
				$html .= '<div class="elementoAll">' .
							'<div style="padding:10px 15px;">' .
								'<div class="minContentForo">' .
									'<div class="elementoNombreForo fleft"><p>' . Texto::textoFormateado($nombrec) . '</p></div>' .
										'<div class="fechaForo fright">' . Fecha::obtenerFechaFormateada($mensaje->fecha) . '</div>' .
										'<div class="clear"></div>' . 
									'<div class="elementoContenido">' .
										'<p><span class="contenidoRespuesta">' . $mensaje->contenido . '</span></p>' .
									'</div>' .
								'</div>' .
				 				'<div class="elementoPie optionsForo">';
									if(!$msg_respuesta && $nRespuesta > 0)
									{
										$html .= '<span id="detailsRespuestaForo_' . $cont . '" class="textRespuesta fleft no-details" details="0">' .
													'<a rel="tooltip" title="Desplegar respuestas" href="#" onclick="desplegarRespuestas(\'detailsRespuestaForo_\', '.$cont.')" title="Ver respuestas" data-translate-title="seguimiento.desplegar" >' .
														'<span data-translate-html="seguimiento.desplegar">Desplegar respuestas</span> (' . $nRespuesta . ')' .
													'</a>' .
												'</span>';
									}
	
								$html .='<ul class="elementoListOptions fright">';
										if(self::$_footerOptions)
										{
											/*
											if(!$msg_respuesta && $nRespuesta > 0)
											{	
												if(isset($ids['responseNueva']) && is_array($ids['responseNueva']) && count($ids['responseNueva'])> 0)
												{
													if(in_array($mensaje->idforo_mensaje, $ids['responseNueva']) && $mensaje->idusuario != $idUsuarioCreador)
													{
														$classResponse = 'galletaNueva';
													}
													else
													{
														$classResponse = 'galleta';	
													}
												}
												else
												{
													$classResponse = 'galleta';
												}
																	
												$html.= '<li class="' . $classResponse . '">
															<a rel="tooltip" title="Desplegar respuestas" href="#" onclick="desplegarRespuestas('.$cont.')" title="Ver respuestas">
																<span class="textGalleta">' . $nRespuesta .'</span>
																<!-- <span>Respuestas</span> -->
															</a>
														</li>';
											}
											*/
											if(!Usuario::compareProfile('supervisor'))
											{
												$html .= '<li><a data-translate-title="foro.respondermensaje" rel="tooltip" href="aula/foro/tema/mensaje/responder/' . $mensaje->idforo_mensaje . '" title="Responder a este mensaje"><img src="imagenes/options/responder.png" alt="responder" /><span class="alignMiddle"></span></a></li>';	
											}
											
											if(Usuarios::comprobarPermisosLogueo($mensaje->idusuario))
											{
												$html .= '<li><a data-translate-title="foro.editarmensaje" rel="tooltip" href="aula/foro/tema/mensaje/editar/' . $mensaje->idforo_mensaje . '" title="Editar este mensaje"><img src="imagenes/options/edit.png" alt="editar" /><span></span></a></li>' .
												'<li><a data-translate-title="foro.borrarmensaje" rel="tooltip" href="#" ' . Alerta::alertConfirmOnClick('borrarmensaje','¿Deseas borrar el mensaje?', 'aula/foro/tema/mensaje/borrar/' . $mensaje->idforo_mensaje) . ' title="Borrar mensaje"><img src="imagenes/options/delete.png" alt="borrar" /><span></span></a></li>'; //borrar
											}
											else if(Usuario::compareProfile('tutor'))
											{
												$html .= '<li><a data-translate-title="foro.borrarmensaje" rel="tooltip" href="#" ' . Alerta::alertConfirmOnClick('borrarmensaje','¿Deseas borrar el mensaje?', 'aula/foro/tema/mensaje/borrar/' . $mensaje->idforo_mensaje) . ' title="Borrar mensaje"><img src="imagenes/options/delete.png" alt="" /><span></span></a></li>'; //borrar
											}
										}
							$html .= '</ul>' .
								'<div class="clear"></div>' .
								'</div>' .
						'</div>' .
					'</div>' .
					'<div style="position: absolute;top: 8px;left: 99px;"><img src="imagenes/foro/pico-foro.png" alt="" /></div>' .
					'<div class="clear"></div>' .
				'</div>' .
			'<div class="clear"></div>';

		return $html;
	}
}