<div id="foro">
	<div class="subtitle t_center">
		<img src="imagenes/foro/foro.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>
	<br/>
	<?php if(!Usuario::compareProfile(array('alumno', 'supervisor'))):?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as y tutores las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso. 
		</p>
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compa&ntilde;eros/as y no dude en responderles.
		</p>
	</div>
	<br/>
	<?php endif;?>
	<div class="allTemas">
		<div class='titleTemaForo'>
			<div class="headTitleTemaForo fleft" data-translate-html="foro.elige">
				Selecciona un tema del foro
			</div>
			<?php if(!Usuario::compareProfile(array('alumno', 'supervisor'))):?>
				<div class="menu_interno fright" style="margin: 20px 0 0 0;">
					<div class="elemento_menuinterno fright">
						<a href="aula/foro/tema/nuevo" title="Publicar un nuevo tema en el foro" data-translate-html="foro.nuevot">
							Nuevo tema
						</a>
					</div>
				</div>
			<?php endif;?>
			<div class='clear'></div>
		</div>
	</div>
		
	<div id="listarTemasForo" class="listarElementos">
		<?php if($temas->num_rows > 0):?>
			<?php while($tema = $temas->fetch_object()):?>
				<?php
					$fechaLog = null;
					if(isset($temasLogArray[$tema->idforo_temas])) {

						$fechaLog = $temasLogArray[$tema->idforo_temas];
					}
					
					// obtenemos los mensajes
					if(Usuario::compareProfile('alumno')) {

						$newMsg = $objModelo->obtenerNuevosMsg($idusuario, $idcurso, $tema->idforo_temas, $fechaLog);

					} else {

						$newMsg = $objModelo->obtenerNuevosMsgStaff($idusuario, $idcurso, $tema->idforo_temas, $fechaLog);
					}
					
					if($newMsg->num_rows > 0) {

						$newMsg = $newMsg->num_rows;

					} else {
						
						$newMsg = 0;
					}
				?>
				<div class="elemento">
					<div class="elementoTitulo fleft">
						<a href="aula/foro/<?php echo $tema->idforo_temas?>" title="Ir al tema del foro" data-translate-title="foro.ir">
							<?php echo Texto::textoPlano($tema->titulo)?>
							<?php if($newMsg > 0): ?>
								<small style="margin-left: 10px;font-size:0.9em;" class="verde">
									(<?php echo $newMsg ?> 
									<span data-translate-html="foro.n_mensajes">
										mensaje/s nuevo/s
									</span>)
								</small>';
							<?php endif; ?>
						</a>
					</div>
					<div class="clear"></div>
				</div>
			<?php endwhile;?>
		<?php else:?>
			<div class="elementosNoEncontrados">
				<strong data-translate-html="foro.no_tema">
					No hay temas de este curso
				</strong>
			</div>
		<?php endif;?>
	</div>
</div>