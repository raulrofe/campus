<div id="foro">
	<div class="subtitle t_center">
		<img src="imagenes/foro/foro.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>

	<br/>

	<?php if($_SESSION['perfil'] != 'alumno'):?>

	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos del curso.
		</p>
	</div>

	<?php else: ?>

	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		</p>
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compa�eros/as y no dude en responderles. 
		</p>
	</div>
	<br/>
	<?php endif;?>
	<div id="foroEditarMensaje">
		<div class="subtitleForo">
			<div class='subtitleModulo fleft' data-translate-html="foro.edicion">
				Edici&oacute;n de mensaje
			</div>
			<div class="menu_interno">
				<div class='elemento_menuinterno fright'>
					<a href="aula/foro/<?php echo $mensaje->idforo_temas?>" data-translate-title="foro.volverP" title="Volver a los temas del foro" >
						&lt;&lt; 
						<span data-translate-html="foro.volver">
							Volver al tema del foro
						</span>
					</a>
				</div>
			</div>
			<div class='clear'></div>
		</div>
		<br/>
		<form method="post" action="aula/foro/tema/mensaje/editar/<?php echo $get['idMsg']?>">
			<ul>
				<li>
					<textarea id="foroEditarMensaje_1" name="mensaje" rows="1" cols="1">
						<?php echo $mensaje->contenido?>
					</textarea>
				</li>
			</ul>
			<button type="submit" class="width100" data-translate-html="foro.actualizar">
				Actualizar mensaje
			</button>
		</form>
	</div>
	
</div>

<script type="text/javascript">
	$(function() {

		tinymce.remove();

		tinymce.init({
			selector: "textarea",
			menubar: false,
			statusbar: false,
			plugins: ["emoticons"],
			toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
		});
	});
</script>