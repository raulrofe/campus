<div id="foro">
	<div class="elementosNoEncontrados">
		<strong data-translate-html="foro.no_foro">
			El foro del curso al que intentas acceder no existe
		</strong>
	</div>
</div>