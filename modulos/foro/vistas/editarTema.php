<div id="foro">
	<div class="subtitle t_center">
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>

	<?php if($_SESSION['perfil'] != 'alumno'):?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		</p>
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compaņeros/as y no dude en responderles. 
		</p>
	</div>
	<?php endif;?>
	<div>
		<div class='subtitleModulo fleft'>
			<span data-translate-html="foro.editar">
				Editar tema del foro
			</span>
		</div>

		<div id="paginaVolverTop" class='fright' style='margin-top:15px;'>
			<a href="aula/foro/<?php echo $tema->idforo_temas?>" title="Volver al tema del foro" data-translate-html="foro.titulo" data-translate-title="foro.volver">
				Volver al tema del foro
			</a>
		</div>

		<div class='clear'></div>
	</div>

	<form id="foroNuevoTema" action="aula/foro/tema/editar/<?php echo $get['idtema']?>" method="post">
		<ul>
			<li>
				<label for="foroNuevotema_titulo" class="fleft etiqueta" data-translate-html="foro.titulo_tema">
					T&iacute;tulo del tema
				</label>
				<input id="foroNuevotema_titulo" class="fright" type="text" name="titulo" value="<?php echo Texto::textoPlano($tema->titulo)?>" />
				<div class="clear"></div>
			</li>
			<li>
				<label for="foroNuevotema_modulo" class="fleft etiqueta" data-translate-html="foro.modulo_curso">
					M&oacute;dulo del curso
				</label>
				<select id="foroNuevotema_modulo" name="idModulo" class="fright">
					<?php while($modulo = $modulos->fetch_object()):?>
						<option value="<?php echo $modulo->idmodulo?>" <?php if($modulo->idmodulo == $tema->idmodulo) echo 'selected="selected"'?>>
							<?php echo $modulo->nombre?>
						</option>
					<?php endwhile;?>
				</select>
				<div class="clear"></div>
			</li>
			<li>
				<button type="submit" data-translate-html="foro.actualizar_tema">
					Actualizar tema
				</button>
			</li>
		</ul>
	</form>
</div>