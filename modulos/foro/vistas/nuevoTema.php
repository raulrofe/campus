<div id="foro">
		<div class="subtitle t_center">
			<span data-translate-html="foro.titulo">
				Foros
			</span>
		</div>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
	<div class="popupIntro" >
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		</p>
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compaņeros/as y no dude en responderles. 
		</p>
	</div>
	<br/><br/>
	<?php endif;?>
		<div class="subtitleForo">
			<div class='subtitleModulo fleft'>
				<span data-translate-html="foro.nuevo_tema">
					Nuevo tema del foro
				</span>
			</div>
			<div class="menu_interno">
				<div class='elemento_menuinterno fright'>
					<a href="aula/foro" title="Volver a los temas del foro">
						&lt;&lt; 
						<span data-translate-html="foro.volver">
							Volver a los temas del foro
						</span>
					</a>
				</div>
			</div>
			<div class='clear'></div>
		</div>
		<br/>
		<form id="foroNuevoTema" action="aula/foro/tema/nuevo" method="post">
			<ul>
				<li>
					<label for="foroNuevotema_titulo" class="fleft etiqueta" data-translate-html="foro.titulo_tema">
						T&iacute;tulo del tema
					</label>
					<input id="foroNuevotema_titulo" class="boxSizing fright" type="text" name="titulo" />
					<div class="clear"></div>
				</li>
				<li>
					<label for="foroNuevotema_modulo" class="fleft etiqueta" data-translate-html="foro.modulo_curso">
						M&oacute;dulo del curso
					</label>
					<select id="foroNuevotema_modulo" name="idModulo" class="fright">
						<?php while($modulo = $modulos->fetch_object()):?>
							<option value="<?php echo $modulo->idmodulo?>"><?php echo $modulo->nombre?></option>
						<?php endwhile;?>
					</select>
					<div class="clear"></div>
				</li>
			</ul>

			<button class="width100" type="submit" data-translate-html="formulario.crear">
				Crear
			</button>

			<input type="hidden" name="idCurso" value="<?php echo $_SESSION['idcurso'] ?>" />

		</form>
</div>