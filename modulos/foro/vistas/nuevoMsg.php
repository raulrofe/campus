<div id="foro">
	<div class="subtitle t_center">
		<img src="imagenes/foro/foro.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>
	<br/>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compa�eros/as y no dude en responderles. 
		</p>
	</div>
	<br/>
	<?php endif;?>
	<?php if($_SESSION['perfil'] != 'supervisor'):?>
		<div id="foroNuevoMensaje">
			<div class="subtitleForo">

				<div class="subtitleModulo fleft" data-translate-html="foro.nuevo_msj">
					Nuevo mensaje
				</div>

				<div class="menu_interno">
					<div class='elemento_menuinterno fright'>
						<a href="aula/foro/<?php echo $get['idtema']?>" data-translate-title="foro.volver" title="Volver al tema del foro">
							&lt;&lt; 
							<span data-translate-html="foro.volver">
								Volver al tema del foro
							</span>
						</a>
					</div>
				</div>

				<div class="clear"></div>

			</div>

			<br/>

			<form method="post" action="aula/foro/tema/mensaje/nuevo">
				<ul>
					<li>
						<textarea id="foroNuevoMensaje_1" name="mensaje" rows="1" cols="1"></textarea>
						<input type="hidden" name="idTema" value="<?php echo $get['idtema']?>" />
					</li>
					<li>
						<button type="submit" data-translate-html="foro.publicar">
							Publicar
						</button>
					</li>
				</ul>
				<div class="clear"></div>
			</form>
		</div>
	<?php endif;?>
</div>

<script type="text/javascript">
	
	$(function() {

		tinymce.remove();

		tinymce.init({
			selector: "textarea",
			menubar: false,
			statusbar: false,
			plugins: ["emoticons"],
			toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
		});
	});

</script>