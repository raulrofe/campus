<div id="foro">
	<div class="subtitle t_center">
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>

	<?php if($_SESSION['perfil'] != 'alumno'):?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		</p>
		<br/>
		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compaņeros/as y no dude en responderles. 
		</p>
	</div>
	<?php endif;?>
	<?php if($_SESSION['perfil'] != 'supervisor'):?>
		<div id="foroNuevoMensaje">
			<div class="subtitleModulo" data-translate-html="foro.nuevo_msj">
				Nuevo mensaje:
			</div>
			<form method="post" action="aula/foro/tema/mensaje/nuevo">
				<ul>
					<li>
						<textarea id="foroNuevoMensaje_1" name="mensaje" rows="1" cols="1"></textarea>
						<input type="hidden" name="idTema" value="<?php echo $mensaje->idforo_temas?>" />
						<input type="hidden" name="idMsgResp" value="<?php echo $mensaje->idforo_mensaje?>" />
					</li>
					<li>
						<button type="submit" data-translate-html="foro.publicar">
							Publicar
						</button>
					</li>
				</ul>
				<div class="clear"></div>
			</form>
		</div>
		<div id="foroRespuestaMensaje">
			<h2 class='subtitleModulo' data-translate-html="foro.en_respuesta_a">
				En respuesta a:
			</h2>
			<div class="elementoAll">
				<div class="elementoNombre">
					<p>
						<?php echo Texto::textoFormateado($nombrec)?>
					</p>
				</div>
				<div class="elementoContenido">
					<p>
						<?php echo $mensaje->contenido ?>
					</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	<?php endif;?>

	<br/><br/><br/><br/><br/>

	<div id="paginaVolver" class='fleft'>
		<a href="aula/foro/<?php echo $mensaje->idforo_temas?>" title="Volver al tema del foro" data-translate-html="foro.volver" data-translate-title="foro.volver">
			Volver al tema del foro
		</a>
	</div>
</div>

<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		menubar: false,
		statusbar: false,
		plugins: ["emoticons"],
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
	});
</script>