<div id="foro">
	<div class="subtitle t_center">
		<img src="imagenes/foro/foro.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="foro.titulo">
			Foros
		</span>
	</div>
	<br/>
	<?php if(!Usuario::compareProfile(array('alumno', 'supervisor'))):?>
	<div class="popupIntro">
		<p data-translate-html="foro.descripcion3">
			Este espacio es un conjunto de foros donde puede compartir con sus alumnos/as las reflexiones que les suscitan los contenidos 
			del curso.
		</p>
	</div>
	<?php else:?>
	<div class="popupIntro">
		
		<p data-translate-html="foro.descripcion1">
			Este espacio re&uacute;ne diferentes foros de debate donde puede compartir con sus compa&ntilde;eros/as y tutores las reflexiones que le suscitan los contenidos del curso.
		</p>

		<br/>

		<p data-translate-html="foro.descripcion2">
			Consulte los foros que ha creado su tutor/a, lea los mensajes que han enviado sus compa�eros/as y no dude en responderles. 
		</p>

	</div>
	<?php endif;?>
	<div id="foroTemaIntro">
		<div class='descripcionTema'>
			<div class='subtitleModulo'>
				<span data-translate-html="foro.tema">
					Tema:
				</span> 
				<?php echo Texto::textoPlano($tema->titulo)?>
			</div>

			<div class="elementoPie">
				<div class="elementoFecha" style='margin-top:-15px;'>
					<span data-translate-html="foro.creado">
						Creado por:
					</span> 
					<?php echo Texto::textoPlano($tema->nombrec)?> 
					<span data-translate-html="perfiles.tutor">(tutor/a)</span>
				</div>
			</div>
		</div>
		<div class='menuTema'>
			<div class='elementoIntro'>
				<div class="elementoPie">
					<div class="menu_interno">
						<?php if(Usuarios::comprobarPermisosLogueo($tema->idrrhh, $tema->idperfil)):?>
							<div id="elemento_menuinterno" class='fleft' style="margin-top:-18px;">
								<a href="aula/foro/tema/editar/<?php echo $get['idtema']?>" title="Editar este tema" data-translate-html="foro.editar_tema" data-translate-title="foro.editar_tema">
									Editar tema
								</a>
							</div>

							<div id="elemento_menuinterno" class='fleft' style="margin-top:-18px;">
								<a href="#" <?php echo Alerta::alertConfirmOnClick('eliminartema','¿Realmente quieres eliminar este tema del foro para siempre?', 'aula/foro/tema/eliminar/' . $get['idtema'])?> title="Eliminar este tema" data-translate-html="foro.borrar_tema" data-translate-title="foro.borrar_tema">
									Borrar tema
								</a>
							</div>
						<?php endif;?>
						<?php if(!Usuario::compareProfile('supervisor')):?>
							
							<div class='elemento_menuinterno fleft' style="margin-top:-18px;">
								<a href="aula/foro/tema/mensaje/nuevo/<?php echo $get['idtema']?>" data-translate-html="foro.nuevo">
									Escribir mensaje nuevo
								</a>
							</div>

						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="foroMensajes" class="listarElementos">
		<?php if($mensajes->num_rows > 0):?>
			<?php echo foro::listarMensajes($mensajes, $idMensajesForo)?>
		<?php else:?>
			<div class="elementosNoEncontrados">
				<strong data-translate-html="foro.no_mensaje_tema">
					No hay mensajes de este tema
				</strong>
			</div>
		<?php endif;?>
	</div>
	
	<div class="menu_interno">
		<div class='elemento_menuinterno fleft'>
			<a href="aula/foro" data-translate-title="foro.volverP" title="Volver a los temas del foro">
				&lt;&lt; 
				<span data-translate-html="foro.volver">
					Volver a los temas del foro
				</span>
			</a>
		</div>
	</div>
	
	<?php echo Paginado::crear($numPagina, $maxPaging, 'aula/foro/' . $get['idtema'] . '/pagina/')?>

</div>