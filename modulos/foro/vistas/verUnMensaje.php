<div id="foro">
	<div id="foroMensajes" class="listarElementos">
		<?php if($mensajes->num_rows > 0): ?>

			<?php foro::setConfigFooter(false); ?>

			<?php echo foro::listarMensajes($mensajes); ?>

		<?php else: ?>
			<b>
				<span data-translate-html="foro.este_no">
					Este mensaje no existe
				</span>
			</b>
		<?php endif; ?>
	</div>
</div>