<?php
class modeloForo extends modeloExtend
{
	public function nuevoTema($titulo, $idrrhh, $idmodulo)
	{
		$sql = 'INSERT INTO foro_temas (titulo, idrrhh, fecha, idmodulo)' .
		' VALUES ("' . $titulo . '", "' . $idrrhh . '", "' . date('Y-m-d H:i:s') . '", ' . $idmodulo . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoForoLog($idTema, $idUsuario, $idCurso)
	{
		$sql = 'INSERT INTO foros_logs (idTema, idUsuario, idCurso)' .
		' VALUES ("' . $idTema . '", "' . $idUsuario . '", "' . $idCurso . '")';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarForoLog($idTema, $idUsuario, $idCurso)
	{
		$sql = 'UPDATE foros_logs SET fechaUltConexion = "' . date('Y-m-d H:i:s') . '"' .
			' WHERE idTema = ' . $idTema .
			' AND idUsuario = "' . $idUsuario . '"' .
			' AND idCurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnForoLog($idTema, $idUsuario, $idCurso)
	{
		$sql = 'SELECT fechaUltConexion' .
		' FROM foros_logs' .
		' WHERE idTema = ' . $idTema .
		' AND idUsuario = "' . $idUsuario . '"' .
		' AND idCurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerForoLog($idCurso, $idUsuario)
	{

		$sql = 'SELECT ft.idforo_temas, flg.fechaUltConexion' .
		' FROM foro_temas AS ft' .
		' LEFT JOIN foros_logs AS flg ON flg.idTema = ft.idforo_temas' .
		' WHERE flg.idUsuario = "' . $idUsuario . '"' .
		' AND flg.idCurso = ' . $idCurso;

		/*
		$sql = 'SELECT ft.idforo_temas, flg.fechaUltConexion' .
		' FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN foros_logs AS flg ON flg.idTema = ft.idforo_temas AND flg.idUsuario = "' . $idUsuario . '"' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1)' .
		' GROUP BY ft.idforo_temas ORDER BY ft.idforo_temas ASC';
		*/

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerModulos($idCurso)
	{
		/*
		$sql = 'SELECT tm.idmodulo, modulo.nombre FROM temario AS tm' .
		' LEFT JOIN modulo ON modulo.idmodulo = tm.idmodulo' .
		' WHERE tm.idcurso = ' . $idCurso;
		*/
		$sql = 'SELECT md.idmodulo, md.nombre' .
		' FROM curso AS c' .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo ' .
		' WHERE c.idcurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemas($idCurso)
	{
		$fecha = date('Y-m-d');

		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, rrhh.nombrec, c.idcurso, c.titulo AS nombre_modulo' .
		' FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idcurso = c.idcurso' .
		//' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		//' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		//' WHERE ft.idtemario = tm.idtemario' .
		' WHERE (ft.idcurso = tm.idcurso OR ft.es_general = 1)' .// AND c.f_inicio <= "' . $fecha . '" AND c.f_fin <= "' . $fecha . '"' .
		' GROUP BY ft.idforo_temas' .
		' ORDER BY ft.fecha ASC, ft.idforo_temas ASC';
		//LEFT JOIN temario ON temario.idCurso = ' . $idCurso . '

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasAlumnoForo($idAlumno, $idCurso)
	{
		$sql = "SELECT idforo_temas"	.
		" FROM foro_mensaje" .
		" WHERE idcurso = " . $idCurso .
		" AND idusuario = " . $idAlumno .
		" GROUP BY idforo_temas";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasNoGenerales($idCurso)
	{
		$fecha = date('Y-m-d');

		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, rrhh.nombrec, c.idcurso, md.nombre AS nombre_modulo' .
		' FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE (ft.idmodulo = tm.idmodulo AND ft.es_general = 0)' .// AND c.f_inicio <= "' . $fecha . '" AND c.f_fin <= "' . $fecha . '"' .
		' AND ft.idcurso = ' . $idCurso .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha ASC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerNombreCurso($idCurso)
	{
		$sql = 'SELECT titulo FROM curso AS c' .
		' WHERE c.idcurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnTema($idCurso, $idTema)
	{
		//$fecha = date('Y-m-d');

		$sql = 'SELECT ft.idforo_temas, ft.titulo, ft.descripcion, ft.fecha, rh.idrrhh, rh.nombrec, rh.idperfil, c.idcurso, c.titulo AS modulo_nombre FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		//' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo  OR ft.es_general = 1' .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = ft.idrrhh' .
		' WHERE (ft.idcurso = tm.idcurso OR ft.es_general = 1) AND ft.idforo_temas = ' . $idTema .
		' GROUP BY ft.idforo_temas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnTemaPorTitulo($idModulo, $titulo)
	{
		$sql = 'SELECT * FROM foro_temas AS ft' .
		' WHERE (ft.idmodulo = ' . $idModulo . ' OR ft.es_general = 1) AND ft.titulo = "' . $titulo . '"' .
		' GROUP BY ft.idforo_temas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarTema($idTema, $mensaje, $idModulo)
	{
		$sql = 'UPDATE foro_temas SET titulo = "' . $mensaje . '", idmodulo = ' . $idModulo .
		' WHERE foro_temas.idforo_temas = ' . $idTema;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarTema($idTema)
	{
		$sql = 'DELETE FROM foro_temas' .
		' WHERE foro_temas.idforo_temas = ' . $idTema;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajes($idCurso, $idTema, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		$sql = 'SELECT fm.idforo_mensaje, fm.contenido, fm.fecha, fm.idusuario, foro_respuesta.idforo_mensaje AS id_mensaje_respuesta,' .
		' rrhh.foto AS t_foto, rrhh.nombrec AS t_nombrec, alumnos.foto AS a_foto, CONCAT(alumnos.nombre, " ", alumnos.apellidos) AS a_nombrec' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN rrhh ON CONCAT("t_", rrhh.idrrhh) = fm.idusuario' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIN foro_respuesta ON foro_respuesta.idforo_respuesta = fm.idforo_mensaje' .
		//' LEFT JOIN rrhh ON rrhh.idrrhh ON rrhh.idrrhh = fm.idusuario' .
		' WHERE foro_respuesta.idforo_respuesta IS NULL' .
		' AND fm.idforo_temas = ' . $idTema .
		' AND fm.idcurso = ' . $idCurso .
		' GROUP BY fm.idforo_mensaje' .
		' ORDER BY fm.fecha DESC' . $addQuery;
		/*' WHERE ft.idtemario = temario.idCurso';*/

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajesRespuesta($idMsg)
	{
		$sql = 'SELECT fm.idforo_mensaje, fm.contenido, fm.fecha, fm.idusuario, foro_respuesta.idforo_mensaje AS id_mensaje_respuesta,' .
		' rrhh.foto AS t_foto, rrhh.nombrec AS t_nombrec, alumnos.foto AS a_foto, CONCAT(alumnos.nombre, " ", alumnos.apellidos) AS a_nombrec' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN rrhh ON CONCAT("t_", rrhh.idrrhh) = fm.idusuario' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIN foro_respuesta ON foro_respuesta.idforo_respuesta = fm.idforo_mensaje' .
		//' LEFT JOIN rrhh ON rrhh.idrrhh ON rrhh.idrrhh = fm.idusuario' .
		' WHERE foro_respuesta.idforo_respuesta IS NOT NULL AND foro_respuesta.idforo_mensaje = ' . $idMsg .
		' GROUP BY fm.idforo_mensaje ORDER BY fm.fecha ASC';
		/*' WHERE ft.idtemario = temario.idCurso';*/

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnMensaje($idMensaje, $fromParent = true)
	{
		if($fromParent)
		{
			$addSql = 'fr.idforo_mensaje = fm.idforo_mensaje';
		}
		else
		{
			$addSql = 'fr.idforo_respuesta = fm.idforo_mensaje';
		}

		$sql = 'SELECT fm.idforo_mensaje, fm.contenido, fm.fecha, fm.idusuario, fm.idforo_temas, fr.idforo_mensaje AS idforo_mensaje_respondido,' .
		' rrhh.foto AS t_foto, rrhh.nombrec AS t_nombrec, alumnos.foto AS a_foto, CONCAT(alumnos.nombre, " ", alumnos.apellidos) AS a_nombrec' .
		' FROM foro_mensaje AS fm' .
		/*' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .*/
		' LEFT JOIN rrhh ON CONCAT("t_", rrhh.idrrhh) = fm.idusuario' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIN foro_respuesta AS fr ON ' . $addSql .
		' WHERE fm.idforo_mensaje = ' . $idMensaje .
		' GROUP BY fm.idforo_mensaje';
		/*' WHERE ft.idtemario = temario.idCurso';*/

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajesNuevo($idTema, $idCurso, $fechaUltimaConexion)
	{
		$sql = "SELECT idforo_mensaje" .
		" FROM foro_mensaje" .
		" WHERE idforo_temas = " . $idTema .
		" AND idcurso = " . $idCurso .
		" AND fecha > '" . $fechaUltimaConexion . "'";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarMensaje($idMsg, $mensaje)
	{
		$sql = 'UPDATE foro_mensaje SET contenido = "' . $mensaje . '"' .
		' WHERE idforo_mensaje = ' . $idMsg;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoMensaje($idTema, $idUsuario, $mensaje)
	{
		$sql = 'INSERT INTO foro_mensaje (idforo_temas, contenido, idusuario, fecha, idcurso)' .
		' VALUES (' . $idTema . ', "' . $mensaje . '", "' . $idUsuario . '", "' . date('Y-m-d H:i:s') . '", ' . Usuario::getIdCurso() . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoMensajeRespuesta($idMsg, $idMsgRespuesta)
	{
		$sql = 'INSERT INTO foro_respuesta (idforo_mensaje, idforo_respuesta)' .
		' VALUES (' . $idMsg . ', ' . $idMsgRespuesta . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function borrarMensaje($idMsg)
	{
		$sql = 'DELETE FROM foro_mensaje WHERE idforo_mensaje = ' . $idMsg;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarRegistroMsg($idMsg, $idUsuario)
	{
		$sql = 'INSERT INTO foro_msg_reg (idforo_mensaje, idusuario, fecha) VALUES (' . $idMsg . ', "' . $idUsuario . '", "' . date('Y-m-d H:i:s') . '")';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajesPorAlumno($idalumno, $idcurso)
	{
		$sql = 'SELECT fm.idforo_mensaje, fm.contenido, fm.fecha, fm.idusuario, ft.idforo_temas, ft.titulo AS tituloTema,' .
		' alumnos.foto AS a_foto, CONCAT(alumnos.nombre, " ", alumnos.apellidos) AS a_nombrec' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIn foro_temas AS ft ON ft.idforo_temas = fm.idforo_temas' .
		' WHERE fm.idusuario = "' . $idalumno . '" AND fm.idcurso = ' . $idcurso .
		' GROUP BY fm.idforo_mensaje ORDER BY fm.idforo_temas, fm.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTemasParticipadoPorAlumno($idalumno, $idcurso)
	{
		$sql = 'SELECT count(fm.idforo_mensaje) as nTemasParticipados' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIn foro_temas AS ft ON ft.idforo_temas = fm.idforo_temas' .
		' WHERE fm.idusuario = "' . $idalumno . '" AND fm.idcurso = ' . $idcurso .
		' GROUP BY fm.idforo_temas ORDER BY fm.idforo_temas, fm.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajesTemas($idcurso, $idTema)
	{
		$sql = 'SELECT count(fm.idforo_mensaje) as nMensajesTemas' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIn foro_temas AS ft ON ft.idforo_temas = fm.idforo_temas' .
		' WHERE fm.idcurso = ' . $idcurso .
		' AND fm.idforo_temas = ' . $idTema .
		' GROUP BY fm.idforo_temas' .
		' ORDER BY fm.idforo_temas, fm.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMensajesTemasParticipadoPorAlumno($idalumno, $idcurso, $idTema)
	{
		$sql = 'SELECT count(fm.idforo_mensaje) as nMensajesTemasParticipados' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN alumnos ON alumnos.idalumnos = fm.idusuario' .
		' LEFT JOIn foro_temas AS ft ON ft.idforo_temas = fm.idforo_temas' .
		' WHERE fm.idusuario = "' . $idalumno . '"' .
		' AND fm.idcurso = ' . $idcurso .
		' AND fm.idforo_temas = ' . $idTema .
		' GROUP BY fm.idforo_temas' .
		' ORDER BY fm.idforo_temas, fm.fecha DESC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerNuevosMsg($idusuario, $idcurso, $idtema, $fecha)
	{
		$sql = 'SELECT fm.idforo_mensaje' .
		' FROM foro_mensaje AS fm' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idusuario . ' AND m.idcurso = ' . $idcurso .
		' LEFT JOIN foro_temas AS ft ON fm.idforo_temas = ft.idforo_temas' .
		' WHERE fm.fecha > "' . $fecha .
		'" AND fm.idusuario != "'  . $idusuario .
		'" AND ft.idforo_temas = ' . $idtema .
		' AND fm.idcurso = ' . $idcurso .
		' GROUP BY fm.idforo_mensaje';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerNuevosMsgStaff($idusuario, $idcurso, $idtema, $fecha)
	{
		$addSql = '';
		if(isset($idusuario))
		{
			$addSql = ' AND fm.idusuario != "'  . $idusuario . '"';
		}

		$sql = 'SELECT fm.idforo_mensaje FROM foro_mensaje AS fm' .
		' LEFT JOIN staff AS st ON st.idrrhh = "' . $idusuario . '" AND st.idcurso = ' . $idcurso .
		' LEFT JOIN foro_temas AS ft ON fm.idforo_temas = ft.idforo_temas' .
		' WHERE fm.fecha > "' . $fecha . '"' .
		$addSql .
		' AND ft.idforo_temas = ' . $idtema .
		' AND fm.idcurso = ' . $idcurso .
		' GROUP BY fm.idforo_mensaje';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajesSinLeerForo($idCurso)
	{
		/*$sql = "SELECT A.idforo_mensaje, A.idforo_temas from foro_mensaje A
		where A.fecha > (SELECT MAX(LLA.fecha_entrada)
		from logs_acceso LLA, matricula MM
		where LLA.idlugar = 13
		and MM.idalumnos = ".$_SESSION['idusuario']."
		and MM.idcurso = ".$_SESSION['idcurso']."
		and MM.idmatricula = LLA.idmatricula)
		ORDER BY A.idforo_temas";
		*/

		$sql = 'SELECT ft.idforo_temas, flg.fechaUltConexion' .
		' FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' .$idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN foros_logs AS flg ON flg.idTema = ft.idforo_temas AND flg.idUsuario = "' . Usuario::getIdUser(true) . '"' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1)' .
		' GROUP BY ft.idforo_temas ORDER BY ft.idforo_temas ASC';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtUltimoAccesoForo()
	{
		$sql = "SELECT MAX(LLA.fecha_entrada) as fecha
				from logs_acceso LLA, matricula MM
				where LLA.idlugar = 13
				and MM.idalumnos = ".$_SESSION['idusuario']."
				and MM.idcurso = ".$_SESSION['idcurso']."
				and MM.idmatricula = LLA.idmatricula";


		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f['fecha'];
	}

	/*********************************************************************************************************************************************
	METODOS DE FOROS PARA EL CURSO
	*********************************************************************************************************************************************/

	public function obtenerUnTemaPorTituloCurso($idCurso, $titulo)
	{
		$sql = 'SELECT * FROM foro_temas AS ft' .
		' WHERE (ft.idcurso = ' . $idCurso . ' OR ft.es_general = 1) AND ft.titulo = "' . $titulo . '"' .
		' GROUP BY ft.idforo_temas';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function nuevoTemaCurso($titulo, $idusuario, $idModulo, $idCurso, $temaEsGeneral = 0)
	{
		$sql = 'INSERT INTO foro_temas (titulo, idrrhh, fecha, idmodulo, idcurso, es_general)' .
		' VALUES ("' . $titulo . '", "' . $idusuario . '", "' . date('Y-m-d H:i:s') . '", ' . $idModulo . ', ' . $idCurso . ', ' . $temaEsGeneral . ')';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

}