<?php
$idusuario = Usuario::getIdUser(true);
$idcurso = Usuario::getIdCurso();

$objModelo = new modeloForo();

$curso = $objModelo->obtenerNombreCurso($_SESSION['idcurso']);
$curso = $curso->fetch_object();

$temas = $objModelo->obtenerTemas($_SESSION['idcurso']);

$temasLogArray = array();
$temasLog = $objModelo->obtenerForoLog($_SESSION['idcurso'], Usuario::getIdUser(true));
while($row = $temasLog->fetch_object())
{
	$temasLogArray[$row->idforo_temas] = $row->fechaUltConexion;
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'temas');