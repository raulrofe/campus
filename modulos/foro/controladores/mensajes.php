<?php
$get = Peticion::obtenerGet();
$objModelo = new modeloForo();
$idCurso = Usuario::getIdCurso();

// MENSAJES DE TEMATICA
if(isset($get['idtema']) && is_numeric($get['idtema']))
{
	$tema = $objModelo->obtenerUnTema(Usuario::getIdCurso(), $get['idtema']);

	if($tema->num_rows == 1)
	{
		// movemos al primer registro
		$tema = $tema->fetch_object();



		//Usuario::actualizarFechaForo($get['idtema']);
		// Log de acceso para el foro
		$idMensajesForo = array();
		$rowForoLog = $objModelo->obtenerUnForoLog($get['idtema'], Usuario::getIdUser(true), Usuario::getIdCurso());

		if($rowForoLog->num_rows == 0)
		{
			$mensajesForo = $objModelo->obtenerMensajes($idCurso, $get['idtema'], $limitIni = null, $limitEnd = null);
			if($mensajesForo->num_rows > 0)
			{
				while($mensajeForo = $mensajesForo->fetch_object())
				{
					$idMensajesForo[] = $mensajeForo->idforo_mensaje;
				}
			}

			$rowForoLog = $objModelo->nuevoForoLog($get['idtema'], Usuario::getIdUser(true), Usuario::getIdCurso());
		}
		else
		{
			$conexionForo = $rowForoLog->fetch_object();
			$fechaUltimaConexion = $conexionForo->fechaUltConexion;

			$mensajesNuevos = $objModelo->obtenerMensajesNuevo($get['idtema'], $idCurso, $fechaUltimaConexion);
			if($mensajesNuevos->num_rows > 0)
			{
				while($mensajeForo = $mensajesNuevos->fetch_object())
				{
					$idMensajesForo[] = $mensajeForo->idforo_mensaje;
				}
			}

			$rowForoLog = $objModelo->actualizarForoLog($get['idtema'], Usuario::getIdUser(true), Usuario::getIdCurso());
		}

		$numPagina = 1;
		if(isset($get['pagina']) && is_numeric($get['pagina']))
		{
			$numPagina = $get['pagina'];
		}

		$mensajesCount = $objModelo->obtenerMensajes($idCurso, $get['idtema']);
		$maxElementsPaging = 5;
		$maxPaging = $mensajesCount->num_rows;
		$maxPaging = ceil($maxPaging / $maxElementsPaging);

		$elementsIni = ($numPagina - 1) * $maxElementsPaging;

		$mensajes = $objModelo->obtenerMensajes($idCurso, $get['idtema'], $elementsIni, $maxElementsPaging);

		//$mensajesArray = foro::ordenarMensajes($mensajes);

		//var_dump($mensajesArray);die();

		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'mensajes');
	}
	else
	{
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'cursoNoEncontrado');
	}
}