<?php
$get = Peticion::obtenerGet();

$objModelo = new modeloForo();

if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	$mensajes = $objModelo->obtenerUnaConversacion($get['idMsg']);
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'verUnMensaje');
}