<?php

if(!Usuario::compareProfile(array('tutor', 'coordinador', 'alumno')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

$objModelo = new modeloForo();

if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	$mensaje = $objModelo->obtenerUnMensaje($get['idMsg']);
	if($mensaje->num_rows == 1)
	{
		$mensaje = $mensaje->fetch_object();
		if(Usuarios::comprobarPermisosLogueo($mensaje->idusuario) || Usuario::compareProfile('tutor'))
		{
			$borrar = true;
			if(isset($mensaje->idforo_mensaje_respondido))
			{
				$borrar = false;
			}
			
			if($borrar)
			{
				$objModelo->borrarMensaje($get['idMsg']);
			
				Alerta::guardarMensajeInfo('mensajeborrado','Mensaje borrado');
			}
			else
			{
				Alerta::guardarMensajeInfo('mensajenoborradoporrespuestas','No se puede borrar un mensaje que tenga respuestas');
			}
		}
		
		Url::redirect('aula/foro/' . $mensaje->idforo_temas);
	}
}

Url::redirect('aula/foro');