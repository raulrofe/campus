<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();
$objModelo = new modeloForo();

if(isset($get['idtema']) && is_numeric($get['idtema']))
{
	$tema = $objModelo->obtenerUnTema(Usuario::getIdCurso(), $get['idtema']);
	if($tema->num_rows == 1)
	{
		$tema = $tema->fetch_object();
		//if(Usuarios::comprobarPermisosLogueo($tema->idrrhh, $tema->idperfil))
		//{
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$post = Peticion::obtenerPost();
				if(isset($post['titulo'], $post['idModulo']) && is_numeric($post['idModulo']))
				{
					if(!empty($post['titulo']))
					{
						$objModelo->actualizarTema($get['idtema'], $post['titulo'], $post['idModulo']);
						
						Alerta::guardarMensajeInfo('temaeditado','Tema editado correctamente');
						Url::redirect('aula/foro/' . $tema->idforo_temas);
					}
				}
				
				Url::redirect('aula/foro');
			}
			else
			{
				$modulos = $objModelo->obtenerModulos($_SESSION['idcurso']);
		
				require_once dirname(__FILE__) . '/../vistas/editarTema.php';
			}
		//}
	}
}