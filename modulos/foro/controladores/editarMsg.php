<?php

if(!Usuario::compareProfile(array('tutor', 'coordinador', 'alumno')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

$objModelo = new modeloForo();

if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	$mensaje = $objModelo->obtenerUnMensaje($get['idMsg']);
	if($mensaje->num_rows == 1)
	{
		$mensaje = $mensaje->fetch_object();
		if(Usuarios::comprobarPermisosLogueo($mensaje->idusuario))
		{
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$post = Peticion::obtenerPost();
				if(isset($post['mensaje']))
				{
					if(!empty($post['mensaje']))
					{
						$objModelo->actualizarMensaje($get['idMsg'], $post['mensaje']);
						
						Alerta::guardarMensajeInfo('mensajeeditado','Mensaje editado correctamente');
						Url::redirect('aula/foro/' . $mensaje->idforo_temas, true);
					}
				}
				
				Url::redirect('aula/foro', true);
			}
			else
			{
				require_once dirname(__FILE__) . '/../vistas/editarMsg.php';
			}
		}
	}
}