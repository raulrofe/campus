<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$objModelo = new modeloForo();

// POST
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$post = Peticion::obtenerPost();

	if(isset($post['titulo'], $post['idModulo'], $post['idCurso']) && is_numeric($post['idCurso']) && is_numeric($post['idModulo']))
	{
		if(!empty($post['titulo']))
		{
			$rowTema = $objModelo->obtenerUnTemaPorTituloCurso($post['idCurso'], $post['titulo']);
			if($rowTema->num_rows == 0)
			{
				if($objModelo->nuevoTemaCurso($post['titulo'], $_SESSION['idusuario'], $post['idModulo'], $post['idCurso']))
				{
					Alerta::guardarMensajeInfo('temacreado','Se ha creado el nuevo tema');
					
					$idTema = $objModelo->obtenerUltimoIdInsertado();
					Url::redirect('aula/foro/' . $idTema);
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('temayaexiste','Ya existe un tema en ese módulo con el mismo nombre (o es un tema general)');
			}
		}
	}
	
	Url::redirect('aula/foro', true);
}
else
{
	$modulos = $objModelo->obtenerModulos($_SESSION['idcurso']);
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoTema');
}
