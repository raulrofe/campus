<?php
require_once(PATH_ROOT.'modulos/foro/modelos/modelo.php');

$idAlumno = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();

$objForo = new modeloForo();
$numeroMensajesForoSinLeer = 0;

$temasLogArray = array();
$temasLog = $objForo->obtenerForoLog($idCurso, $idAlumno);
while($row = $temasLog->fetch_object())
{
	$temasLogArray[$row->idforo_temas] = $row->fechaUltConexion;
}

$temas = $objForo->obtenerTemas($idCurso);
if($temas->num_rows > 0)
{
	while($tema = $temas->fetch_object())
	{
		$fechaLog = null;
		if(isset($temasLogArray[$tema->idforo_temas]))
		{
			$fechaLog = $temasLogArray[$tema->idforo_temas];
		}
					
		$newMsg = $objForo->obtenerNuevosMsg($idAlumno, $idCurso, $tema->idforo_temas, $fechaLog);
		
		if($newMsg->num_rows > 0)
		{
			$numeroMensajesForoSinLeer += $newMsg->num_rows;
		}
	}
}


//$foros = $objForo->mensajesSinLeerForo(Usuario::getIdCurso());
//$numeroMensajesForoSinLeer = mysqli_num_rows($foros);


