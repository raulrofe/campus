// REDIMENSIONAR
foroReedimensionar();

$('#popupModal_foro').resize(function()
{
	foroReedimensionar();
});

function foroReedimensionar()
{
	var width = $('#popupModal_foro').find('.popupModalContent').width() -143;
	
	// Redimensiono el mensaje
	$('#popupModal_foro .elementoAll').css('width', width + 'px');
	
	// Redimensiono las respuestas
	$('#popupModal_foro .un_mensaje_respuesta .elementoAll').css('width', (width - 101) + 'px');
}

function desplegarRespuestas(id, elemento)
{
	var desglose = $("#" + id + elemento).attr("details");
	
	if(desglose == 0)
	{
		$("#" + id + elemento).removeClass("no-details");
		$("#" + id + elemento).addClass("details");
		var desglose = $("#" + id + elemento).attr("details", "1");
	}
	else if (desglose == 1)
	{
		$("#" + id + elemento).removeClass("details");
		$("#" + id + elemento).addClass("no-details");
		var desglose = $("#" + id + elemento).attr("details", "0");		
	}
	
	$("#mensajesDeRespuesta" + elemento).toggle(200); 
}