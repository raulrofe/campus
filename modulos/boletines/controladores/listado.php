<?php 

$idCurso = Usuario::getIdCurso();

$objModelo = new ModeloInforme();
$curso = $objModelo->obtenerCurso($idCurso);
$curso = $curso->fetch_object();

$alumnos = $objModelo->obtenerAlumnosPorCurso($idCurso);

$lugares = $objModelo->obtenerLugares();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listado');