<?php

error_reporting(0);

require_once "lib/Spreadsheet/PHPExcel.php";
require_once "lib/Spreadsheet/PHPExcel/Reader/Excel2007.php";
require_once "lib/Spreadsheet/PHPExcel/Writer/Excel2007.php";

mvc::importar('lib/mail/phpmailer.php');
mvc::importar('lib/mail/mail.php');

mvc::cargarModuloSoporte('notas');

$objModeloBoletin = new ModeloBoletines();
$objCurso = new Cursos();
$idcurso = Usuario::getIdCurso();

$data = $objCurso->obtenerIGAFCurso($idcurso);
$data = mysqli_fetch_assoc($data);
$accionFormativa = $data['accion_formativa'];
$inicioGrupo = $data['codigoIG'];

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['alumnos'], $post['mensaje']) && is_array($post['alumnos']))
	{
		$objMail = Mail::obtenerInstancia();
		$objNotas = new Notas();
		$objNotas->setCurso($idcurso);
		
		// obtenemos los adtos del tutor logueado
		$rowTutor = $objModeloBoletin->obtenerRRHH(Usuario::getIdUser());
		$rowTutor = $rowTutor->fetch_object();
		
		// las observacion del tutor
		if(empty($post['mensaje']))
		{
			$observacionProfesor = '<i>No hay obsevaciones</i>';
		}
		else
		{
			$observacionProfesor = Texto::textoPlano(addslashes($post['mensaje']));
		}
		
		// obtenemos la plantilla del email y volcamos la observacion del profesor
		$plantillaEmail = file_get_contents(PATH_ROOT . 'plantillas/email_boletin_notas.html');
		$plantillaEmail = str_replace(
			array('{$nombre_curso}', '{$observacion_profesor}', '{$tutor_nombre}', '{$tutor_email}'),
			array(Texto::textoPlano(Usuario::getNameCurso()), $observacionProfesor, Texto::textoPlano($rowTutor->nombrec), Texto::textoPlano($rowTutor->email)),
			$plantillaEmail
		);
		
		// adjuntamos archivos
		//$objExcel = new Spreadsheet_Excel_Writer();
		
		// enviamos a cada alumno un email
		foreach($post['alumnos'] as $idalumno)
		{
			// obtenemos el alumno para este curso
			$rowAlumno = $objModeloBoletin->obtenerUnAlumno($idalumno, $idcurso);
			if($rowAlumno->num_rows == 1)
			{
				$rowAlumno = $rowAlumno->fetch_object();
				
				/*
				// obtenemos la nota final del alumno
				if(isset($rowAlumno->nota_coordinador))
				{
					$notaMediaFinal = round($rowAlumno->nota_coordinador, 2);
				}
				else
				{
					$foroPuntos = $objNotas->calcularNotaMediaForo($idalumno, $idcurso);
					$notaTP = $objNotas->calcularMediaTrabajosPract($idalumno, $idcurso);
				
					$notaMediaFinal = round($foroPuntos + $notaTP + $mediaTests, 2);
				}
				
				// volcamos la nota en la plantiila web
				$contenidoEmail = str_replace('{$notaMediaFinal}', $notaMediaFinal, $plantillaEmail);
				*/
				
				//Librerria phpexcel para pa modificar archivo
				$objReader = new PHPExcel_Reader_Excel2007(); 
				echo $archivoExcel = PATH_ROOT.'archivos/boletin/cuestionariofinal.xlsx';
				$objPHPExcel = $objReader->load($archivoExcel); //cargamos el archivo excel (extensi_n *.xlsx)
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); //objeto de PHPExcel, para escribir en el excel
				$objPHPExcel->getActiveSheet()->setCellValue("F10", $accionFormativa);
				$objPHPExcel->getActiveSheet()->setCellValue("K10", $inicioGrupo);
				$objWriter->save($archivoExcel);//guardamos el archivo excel

				
				$objMail->addAttachment(PATH_ROOT . 'archivos/boletin/cuestionariofinal.xlsx', 'cuestionariofinal.xlsx');
				$objMail->addAttachment(PATH_ROOT . 'archivos/boletin/anexo ficha participantes.doc', 'anexo ficha participantes.doc');
				$objMail->addAttachment(PATH_ROOT . 'archivos/boletin/certificado centro.doc', 'certificado centro.doc');
		
				// mandamos el email
				if($objMail->enviar($rowTutor->email, $rowAlumno->email,Texto::decode($rowTutor->nombrec), Texto::decode($rowAlumno->nombrec), Texto::decode('Entrega del bolet&iacute;n de notas'), $plantillaEmail))
				{
					// sin uso
				}
				
				$objMail->clearAttachments();
			}
		}
		
		Alerta::mostrarMensajeInfo('enviadoboletin','Se ha enviado el bolet&iacute;n a los alumnos seleccionados');
	}
}

$alumnos = $objModeloBoletin->obtenerAlumnosPorCurso($idcurso);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevo');