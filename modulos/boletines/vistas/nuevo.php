<div class="caja_separada">
	<form method="post" action="">
		<ul>
			<li>
				<label>Alumnos</label>
				<select name="alumnos[]" multiple="multiple" size="10">
					<?php while($alumno = $alumnos->fetch_object()):?>
						<option value="<?php echo $alumno->idalumnos?>"><?php echo Texto::textoPlano($alumno->nombrec)?></option>
					<?php endwhile;?>
				</select>
			</li>
			<li>
				<label>Observaciones del profesor/a</label>
				<textarea name="mensaje" rows="1" cols="1"></textarea>
			</li>
			<li>
				<input type="submit" value="Enviar" />
			</li>
		</ul>
	</form>
</div>