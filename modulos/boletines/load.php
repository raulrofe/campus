<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}
?>

<div class='caja_separada'>
	<div class="menu_interno">
		<span class='elemento_menuinterno'><a href="evaluaciones/calificaciones" title="">Calificaciones</a></span>
		<span class='elemento_menuinterno'><a href="evaluaciones/notas" title="">Notas</a></span>
		<span class='elemento_menuinterno'><a href="evaluaciones/informes" title="">Informes</a></span>
		<span class='elemento_menuinterno'><a href="evaluaciones/expedientes" title="">Expedientes</a></span>
		<span class='elemento_menuinterno'><a href="evaluaciones/boletines" title="">Boletines</a></span>
		<div class="clear"></div>
	</div>

<div class='subtitle t_center'><span>Boletines</span></div>
	
</div>

<div id="boletines_menu">
	<div class="menu_tab">
		<ul>
			<li><a href="evaluaciones/boletines" title="">Nuevo bolet&iacute;n</a></li>
			<li><a href="evaluaciones/boletines/listado" title="">Listado de boletines</a>
		</ul>
		<div class="clear"></div>
	</div>
</div>