<?php
class ModeloBoletines extends modeloExtend
{
	public function obtenerAlumnosPorCurso($idCurso)
	{
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM matricula AS m LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos WHERE m.idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAlumno($idAlumnos, $idCurso)
	{
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.email, m.nota_coordinador FROM matricula AS m LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos WHERE m.idcurso = ' . $idCurso . ' AND al.idalumnos = ' . $idAlumnos;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRRHH($idrrhh)
	{
		$sql = 'SELECT * FROM rrhh WHERE idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}