<?php
class ModeloInforme extends modeloExtend
{
	public function obtenerAlumnosPorCurso($idCurso, $idAlumno = null, $activos = true)
	{
		$addQuery = null;
		$addActivos = null;

		if(isset($idAlumno))
		{
			$addQuery = ' AND al.idalumnos = ' . $idAlumno;
		}

		if($activos)
		{
			$addActivos = ' AND m.borrado = 0 AND al.borrado = 0';
		}

		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.nombre, al.apellidos, m.idcurso, m.fecha_conexion_actual' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE m.idcurso = ' . $idCurso .
		$addActivos .
		$addQuery .
		' ORDER BY al.apellidos ASC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerLogAccesosPorAlumno($idAlumno, $idCurso, $idLugar, $fechaIni = null, $fechaFin = null)
	{
		$addQuery = null;
		if(isset($fechaIni, $fechaFin))
		{
			$addQuery = ' AND la.fecha_entrada >= "' . $fechaIni . '" AND la.fecha_salida <= "' . $fechaFin . '"';
		}

		/*$sql = 'SELECT la.idlugar, COUNT(la.idaccesos) AS numAccesos, lg.nombre AS lugar FROM logs_acceso AS la' .
		' LEFT JOIN logs_lugares AS lg ON lg.idlugar = la.idlugar' .
		' WHERE la.idmatricula = (SELECT m.idmatricula FROM matricula AS m WHERE m.idcurso = ' . $idCurso . ' AND m.idalumnos = ' . $idAlumno . ')' .
		' GROUP BY la.idlugar';*/

		$sql = 'SELECT COUNT(la.idaccesos) AS numAccesos, la.fecha_entrada' .
		' FROM logs_acceso AS la' .
		' WHERE la.idlugar = ' . $idLugar .
		' AND la.idmatricula =' .
			' (SELECT m.idmatricula' .
			' FROM matricula AS m' .
			' WHERE m.idcurso = ' . $idCurso .
			' AND m.idalumnos = ' . $idAlumno . ')' .
		$addQuery;

		$sql .= ' GROUP BY la.fecha_entrada';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}


	public function obtenerLugares()
	{
		$sql = 'SELECT idlugar, nombre, aportacion' .
		' FROM logs_lugares' .
		' WHERE idlugar != 19' .
		' ORDER BY prioridad ASC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCurso($idCurso)
	{
		$sql = 'SELECT f_inicio, f_fin FROM curso WHERE idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/****************************************************************************************************************************
	ACCESO DETALLADO POR PARTICIPANTE
	*****************************************************************************************************************************/
	public function obtenerTiemposAccesosPorAlumno($idCurso)
	{
		$sql = 'SELECT m.idalumnos, la.fecha_entrada, la.fecha_salida, m.idmatricula' .
		' FROM logs_acceso AS la' .
		' RIGHT JOIN matricula AS m ON m.idmatricula = la.idmatricula' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE fecha_salida IS NOT NULL AND m.idcurso = ' . $idCurso .
		' GROUP BY la.idaccesos' .
		' ORDER BY la.idmatricula, la.fecha_entrada ASC, la.fecha_salida DESC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAccesosDetalladosParticipante($idMatricula){
		$sql = 'SELECT id, idmatricula, inicio_sesion, fin_sesion' .
		' FROM log_tiempo_sesion' .
		' WHERE idmatricula = ' . $idMatricula .
		' ORDER BY inicio_sesion ASC';

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* NUEVAS FUNCIONES TIEMPOS DE ACCESO GABRI */
	public function obtenerMatriculasCurso($idCurso)	{
		$sql = 'SELECT alumnos.idalumnos, alumnos.nombre, alumnos.apellidos, alumnos.dni, matricula.idmatricula, matricula.fecha_conexion_actual' .
		' FROM matricula' .
		' LEFT JOIN alumnos ON matricula.idalumnos = alumnos.idalumnos' .
		' WHERE idcurso = ' . $idCurso ;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	

	public function obtenerAccesosMatricula($idMatricula)	{
		$sql = 'SELECT fecha_entrada, fecha_salida' .
		' FROM logs_acceso' .
		//' WHERE fecha_salida IS NOT NULL AND idmatricula = ' . $idMatricula .
		' WHERE idmatricula = ' . $idMatricula .
		' ORDER BY fecha_entrada ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerSessionsMatricula($idMatricula)	{
		$sql = 'SELECT inicio_sesion as fecha_entrada, fin_sesion as fecha_salida' .
		' FROM log_tiempo_sesion' .
		//' WHERE fecha_salida IS NOT NULL AND idmatricula = ' . $idMatricula .
		' WHERE idmatricula = ' . $idMatricula .
		' ORDER BY inicio_sesion ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerDatosCursoPorSesion($idCurso) {
		$sql = 'SELECT titulo' .
		' FROM curso' .
		' WHERE idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

	/****************************************************************************************************************************
	INFORME DE TAREAS
	*****************************************************************************************************************************/

	public function getEmpresasByFechasCurso($fechaInicio, $fechaFin, $cif = null, $nif = null, $grupo = null)
	{
		$addQueryEmpresa = '';

		if($cif)
		{
			$addQueryEmpresa .= " AND CT.cif = '" . $cif . "'";
		}

		if($nif)
		{
			$addQueryEmpresa .= " AND AL.dni = '" . $nif . "'";
		}

		if($grupo)
		{
			$addQueryEmpresa .= " AND CS.titulo like '%" . $grupo . "%'";
		}

		$sql = "SELECT M.idmatricula, AL.idalumnos, AL.apellidos, AL.nombre, AL.dni, AL.telefono, AL.email, CT.idcentros, CT.cif, CT.razon_social, CS.idcurso, CS.titulo" .
			   " FROM curso as CS" .
			   " LEFT JOIN matricula as M ON M.idcurso = CS.idcurso" .
			   " LEFT JOIN alumnos as AL ON AL.idalumnos = M.idalumnos" .
			   " LEFT JOIN centros as CT ON CT.idcentros = AL.idcentros" .
			   " WHERE M.borrado = 0 AND CS.f_inicio = '" . $fechaInicio . "' AND CS.f_fin = '" . $fechaFin . "'" . $addQueryEmpresa .
			   " GROUP BY AL.idalumnos" .
			   " ORDER BY CT.cif ASC";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function getInformeParticipante($idMatricula)
	{
		$sql = "SELECT M.idmatricula, AL.idalumnos, AL.apellidos, AL.nombre, AL.dni, AL.telefono, AL.email, CT.idcentros, CT.cif, CT.razon_social, CS.idcurso, CS.titulo" .
			   " FROM matricula as M" .
			   " LEFT JOIN curso as CS ON M.idcurso = CS.idcurso" .
			   " LEFT JOIN alumnos as AL ON AL.idalumnos = M.idalumnos" .
			   " LEFT JOIN centros as CT ON CT.idcentros = AL.idcentros" .
			   " WHERE M.borrado = 0" .
			   " AND M.idmatricula = " . $idMatricula .
			   " GROUP BY AL.idalumnos" .
			   " ORDER BY CT.cif ASC";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function getIdCentroByMatricula($idMatricula)
	{
		$sql = "SELECT CT.idcentros, CT.cif, CT.razon_social, CS.idcurso, CS.titulo, CS.f_inicio, CS.f_fin" .
			   " FROM matricula as M" .
			   " LEFT JOIN curso as CS ON M.idcurso = CS.idcurso" .
			   " LEFT JOIN alumnos as AL ON AL.idalumnos = M.idalumnos" .
			   " LEFT JOIN centros as CT ON CT.idcentros = AL.idcentros" .
			   " WHERE M.borrado = 0" .
			   " AND M.idmatricula = " . $idMatricula .
			   " GROUP BY AL.idalumnos" .
			   " ORDER BY CT.cif ASC";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// CONECTAR A IBERFORM PARA LAS HORAS COFINANCIADAS
	public function horasCofinanciadasIberform($post) {

		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, 'http://www.fundacionaulasmart.org/horas-cofinanciacion/' . $post['nif'] . '/' . $post['codigoIG'] . '/' . $post['accionFormativa']);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);
		curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, false);

		// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($post));

		$buffer = curl_exec($curl_handle);

		curl_close($curl_handle);
		
		if (!empty($buffer)){
			return json_decode($buffer);
		}else{
			return false;
		}
	}
}