	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuIntegrantesCurso">
		<div id="equipoDocente" class="redondearBorde">
			<a href="evaluaciones/calificaciones">
				<img src="imagenes/calificaciones/calificaciones.png" alt="" />
			</a>
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
				<a href="evaluaciones/calificaciones" data-translate-html="evaluaciones.calificaciones">Calificaciones</a>
			</p>
		</div>
		<div id="alumnos" class="redondearBorde">
			<a href="evaluaciones/notas" title="Notas">
				<img src="imagenes/calificaciones/notas.png" alt="" />
			</a>
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
				<a href="evaluaciones/notas" title="Notas" data-translate-html="evaluaciones.notas">Notas</a>
			</p>
		</div>
		<div id="alumnos" class="blanco redondearBorde">
			<a href="evaluaciones/informes" title="Informes">
				<img src="imagenes/calificaciones/informes.png" alt="" />
			</a>
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
				<a href="evaluaciones/informes" title="Informes" data-translate-html="evaluaciones.informes">Informes</a>
			</p>
		</div>
		<div id="alumnos" class="redondearBorde">
			<a href="evaluaciones/expedientes" title="Expedientes">
				<img src="imagenes/calificaciones/expedientes.png" alt="" />
			</a>
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;'>
				<a href="evaluaciones/expedientes" title="Expedientes" data-translate-html="evaluaciones.expedientes">Expedientes</a>
			</p>
		</div>
	</div>
	<!-- Fin menu iconos -->