<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.informes">INFORMES</span>
</div>
<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
		
		<?php require(dirname(__FILE__) . '/submenu.php') ?>
		
		<div id="informes">
			<form id="frm_informe_fecha" method="post" action="evaluaciones/informe/alumno">
				<div class="burbuja">
					<div class="fleft">
						<select name="idalumno">
							<option value="" data-translate-html="evaluaciones.sel_alu">- Todos los alumnos -</option>
							<?php while($alumno = $alumnos->fetch_object()):?>
								<option value="<?php echo $alumno->idalumnos?>"><?php echo $alumno->nombrec?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class="fright">
						<div class="fleft" style="margin:5px 10px 0 0;" data-translate-html="evaluaciones.desde">Desde</div>
						<div class="fleft">
							<input style="width:150px;" id="datepicker" type="text" name="fecha_ini" value="<?php echo date('d/m/Y', strtotime($curso->f_inicio))?>" />
						</div>
					</div>
					<div class="clear"></div>
						
					<br/>
						
					<div class="fleft">
						<select name="idlugar">
							<option value="" data-translate-html="evaluaciones.todoslugares">- Todos los lugares -</option>
							<option value="principal" data-translate-html="evaluaciones.lugaresprincipales">- Lugares principales -</option>
							<?php while($lugar = $lugares->fetch_object()):?>
								<option value="<?php echo $lugar->idlugar?>" data-translate-html="lugares.<?php echo $lugar->nombre?>">
									<?php echo $lugar->nombre?>
								</option>
							<?php endwhile;?>
						</select>
					</div>
					<div class="fright">
						<div class="fleft" style="margin:5px 10px 0 0;" data-translate-html="evaluaciones.hasta">Hasta</div>
						<div class="fleft">
							<input style="width:150px;" id="datepicker2" type="text" name="fecha_fin" value="<?php echo date('d/m/Y', strtotime($curso->f_fin))?>" />
						</div>
					</div>
					<div class="clear"></div>
				</div>

				<div>
					<input type="submit" value="Ver informe de participaci&oacute;n y asistencia" class="width100" data-translate-value="evaluaciones.asistenciai"/>
				</div>

			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});

	$("#datepicker2").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>