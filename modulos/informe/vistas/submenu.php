<?php 
	if(isset($get['c']))
	{
		if($get['c'] == 'index')
		{
			$tituloTabInforme = '<span data-translate-html="evaluaciones.asistencia">Informe de participaci&oacute;n y asistencia</span>';
		}
		else if($get['c'] == 'tiempos')
		{
			$tituloTabInforme = '<span data-translate-html="evaluaciones.informet">Informe de tiempo</span>';
		}
		else if($get['c'] == 'grafica')
		{
			$tituloTabInforme = '<span data-translate-html="evaluaciones.informeg">Informe gr&aacute;fico</span>';
		}
		else if($get['c'] == 'tareas')
		{
			$tituloTabInforme = '<span data-translate-html="evaluaciones.tareas">Tareas</span>';
		}
		else if($get['c'] == 'finalizacion')
		{
			$tituloTabInforme = '<span data-translate-html="evaluaciones.notas">Notas</span>';
		}
	}
	else
	{
		$get['c'] = 'index';
		$tituloTabInforme = 'Informe de participaci&oacute;n y asistencia'; 	
	}
?>

<div class="tituloTabs">
	<div class="tituloEvaluaciones"><?php echo $tituloTabInforme; ?></div>
	
	<div class="fright">
		<div class="menuVista printEvaluation" style="padding:1px;margin-left:5px;">
			<a href="calificaciones/notas/informe" target="_blank" rel="tooltip" title="informe completo del curso" data-translate-title="evaluaciones.icu">
				<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="evaluaciones.vista" />
			</a>
		</div>
	</div>

	<div class="submenuTabs">
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'index') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/informes" rel="tooltip" title="Participaci&oacute;n/Asistencia" data-translate-title="evaluaciones.particpante_asis">
				<img src="imagenes/calificaciones/participacion.png" alt="Participaci&oacute;n/Asistencia" data-translate-title="evaluaciones.particpante_asis"/>
			</a>
		</div>
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'tareas') echo 'activeSubmenu'; ?>" style="border-left:1px solid #CCC;">
			<a href="evaluaciones/informes/tareas" rel="tooltip" title="Tareas" data-translate-title="evaluaciones.tareas">
				<img src="imagenes/calificaciones/tareas.png" alt="Tareas" data-translate-title="evaluaciones.tareas"/>
			</a>
		</div>
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'finalizacion') echo 'activeSubmenu'; ?>" style="border-left:1px solid #CCC;">
			<a href="evaluaciones/informes/finalizacion" rel="tooltip" title="Notas" data-translate-title="evaluaciones.notas">
				<img src="imagenes/calificaciones/finalizacion.png" alt="Informe Notas" data-translate-title="evaluaciones.notas"/>
			</a>
		</div>
		<div class="elementSubmenuTab trabajosPracticos <?php if($get['c'] == 'tiempos') echo 'activeSubmenu'; ?>"style="border-left:1px solid #CCC;border-right:1px solid #CCC;">
			<a href="evaluaciones/informes/tiempos" rel="tooltip" title="Tiempos" data-translate-title="evaluaciones.tiempos">
				<img src="imagenes/calificaciones/tiempos.png" alt="Tiempos" data-translate-title="evaluaciones.tiempos"/>
			</a>
		</div>
		<div class="elementSubmenuTab foros <?php if($get['c'] == 'grafica') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/informes/grafica" rel="tooltip" title="Gr&aacute;fica" data-translate-title="evaluaciones.grafica">
				<img src="imagenes/calificaciones/evolucion.png" alt="Gr&aacute;fica" data-translate-title="evaluaciones.grafica" />
			</a>
		</div>
	</div>
	
	<div class="clear"></div>
</div>
