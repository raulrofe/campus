<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.informes">INFORMES</span>
</div>
<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
		
		<?php require(dirname(__FILE__) . '/submenu.php') ?>
		<div id="informes">

			<form id="frm_informe_fecha" method="post" action="evaluaciones/informes/grafica">
				<div class="burbuja">
					<div class='t_center'>
						<select name="idalumno">
							<option value="" data-translate-html="evaluaciones.sel_alu">- Selecciona un alumno -</option>
							<?php while($alumno = $alumnos->fetch_object()):?>
								<option value="<?php echo $alumno->idalumnos?>" <?php if(isset($alumno->idalumnos, $post['idalumno']) && ($alumno->idalumnos == $post['idalumno'])) echo 'selected="selected"'?>><?php echo $alumno->apellidos . ', ' . $alumno->nombre ?></option>
							<?php endwhile;?>
						</select>
					</div>
					<div class="clear"></div>
					
					<br/>
					
					<div style="width:470px;margin:0 auto;">
						<div class='fleft'>
							<div>
								<span style="width:70px;" data-translate-html="evaluaciones.fechainicio">Fecha inicio</span>
								<input id="datepicker1" type="text" name="fecha_ini" value="<?php if(isset($post['fecha_ini'])) echo $post['fecha_ini']; else echo date('d/m/Y', strtotime($curso->f_inicio))?>" />
							</div>
						</div>	
						<div class='fleft'>	
							<div>
								<span style="width:70px;margin-left:5px;" data-translate-html="evaluaciones.fechafin">Fecha fin</span>
								<input id="datepicker2" type="text" name="fecha_fin" value="<?php if(isset($post['fecha_fin'])) echo $post['fecha_fin']; else echo date('d/m/Y', strtotime($curso->f_fin))?>" />
							</div>
						</div>
					</div>
					<div class="clear"></div>	
				</div>
				
				<input type="submit" value="Mostrar gr&aacute;fica" class="width100" data-translate-value="evaluaciones.mostrarg"/>
			</form>
		
			<div id="graph1"></div>
		
			<?php if(isset($data)):?>
				<script type="text/javascript">
					var data = [
						<?php foreach($data as $key => $value)
						{							
							echo '[' . $value . ',["' . $value . '","' . addslashes($key) . '"]],';
						}?>
					];
					
					$('#graph1').karmicGraph(data,
						{'outLabel' : 'Posa el raton sobre la barra para ver la info',
						'color':'blue',
						'label':'<b>{0} visitas</b> en el dia {1}',
						'type':'flatbars',
						'freeColumn' : 1,
						'width' : 600
					});
				</script>
			<?php endif;?>
		
			<?php
			/*if(isset($chart))
			{
				echo $chart;
			}*/
			?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#datepicker1").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		minDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_inicio))?>),
		maxDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_fin))?>),
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
	
	$("#datepicker2").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		minDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_inicio))?>),
		maxDate: new Date(<?php echo date('Y, m-1, d', strtotime($curso->f_fin))?>),
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>