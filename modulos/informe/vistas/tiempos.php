<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.informes">INFORMES</span>
</div>
<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php'); ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
		
		<?php require(dirname(__FILE__) . '/submenu.php'); ?>
		<div id="informes">
			<div>
				<table class="tabla">
					<thead>
						<tr>
							<td class="t_center" data-translate-html="tiempos.alumnos">Alumnos</td>
							<td class="t_center" data-translate-html="tiempos.primeracceso">Primer acceso</td>
							<td class="t_center" data-translate-html="tiempos.ultimoacceso">&Uacute;ltimo acceso</td>
							<td class="t_center" data-translate-html="tiempos.tiempototal">Tiempo total</td>
							<td class="t_center" data-translate-html="tiempos.cofinanciadas">Cofinanciadas</td>
							<td class="t_center" data-translate-html="tiempos.detalle">Detalle</td>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($tiempos)):?>
							<?php foreach($tiempos as $alumno):?>								
								<tr>
									<!-- NOMBRE Y APELLIDOS -->									
									<td><?php echo Texto::textoPlano($alumno->apellidos . ', ' . $alumno->nombre)?></td>
									
									<!-- PRIMERA CONEXION -->									
									<?php if(!empty($alumno->primero)):?>
										<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', $alumno->primero)); ?></td>
									<?php else:?>
										<td class="t_center">00:00:00</td>
									<?php endif;?>

									<!-- ULTIMA CONEXION -->									
									<?php if(!empty($alumno->ultimo)):?>
										<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', $alumno->ultimo)); ?></td>
									<?php else:?>
										<td class="t_center">00:00:00</td>
									<?php endif;?>
									
									<!-- HORAS TOTALES -->
									<?php if(isset($alumno->suma)):?>
										<td class="t_center">
											<?php echo $alumno->sumaFormateada; ?>
										</td>
									<?php else:?>
										<td class="t_center">00:00:00</td>
									<?php endif;?>

									<!-- HORAS COFINANCIADAS -->
									<?php if(isset($alumno->horaConfinaciacion)):?>
										<td class="t_center">
											<?php echo $alumno->horaConfinaciacion; ?>
										</td>
									<?php else:?>
										<td class="t_center">00:00:00</td>
									<?php endif;?>

									<!-- BOTON PARA VER DETALLE DE CONEXIONES -->
									<td class="t_center">
										<a href="#" onclick="viewEvaluationForo(<?php echo $alumno->idalumnos; ?>);return false" rel="tooltip" title="ver detalle" data-translate-title="tiempos.detalle">
											<img src="imagenes/menu_vistas/view.png">
										</a>
									</td>
								</tr> 


								<!-- DETALLE DE CONEXIONES -->
								<?php $contAcceso 	= 1 ?>

								<?php if(count($alumno->accesos) > 0): ?>
									<!-- CABECERA DETALLE DE CONEXIONES -->
									<tr class="hide detalleForo detalleForoHead" data-student="<?php echo $alumno->idalumnos; ?>" style="background: grey;">
										<td class="t_center" data-translate-html="tiempos.detalle">Número acceso</td>
										<td class="t_center" data-translate-html="tiempos.detalle">Inicio acceso</td>
										<td class="t_center" data-translate-html="tiempos.detalle">Fin acceso</td>
										<td class="t_center" data-translate-html="tiempos.detalle">Tiempo total acceso</td>
										<td class="t_center"></td>
										<td class="t_center"></td>
									</tr>
									<?php foreach($alumno->accesos_reales as $acceso): ?>
										<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>" style="background: #e8e8e8;">
											<td class="t_center"><?php echo $contAcceso ?></td>
											<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', strtotime($acceso['fecha_entrada']))); ?></td>
											<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', strtotime($acceso['fecha_salida']))); ?></td>
											<td class="t_center"><?php echo $acceso['diferenciaFormateada']; ?></td>
											<td></td>
											<td></td>
										</tr>
										<?php $contAcceso++; ?>
									<?php endforeach; ?>
								<?php else: ?>
									<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>" style="background: #e8e8e8;">
										<td colspan="5" class="t_center" data-translate-html="tiempos.no_conex" >Este alumno no tiene ninguna conexión al Campus</td>
									</tr>
								<?php endif; ?>


							<?php endforeach;?>
						<?php else:?>
							<tr><td><strong data-translate-html="tiempos.noalumnocurso">No hay alumnos en este curso</strong></td></tr>
						<?php endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js-calificaciones-detalle_foro.js"></script>