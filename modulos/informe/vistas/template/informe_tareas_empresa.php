<style>

p, table, td{
	margin:0;
	padding:0;
}
b{font-weight: bold;}

/*CLASES GENERALES */
.t_center{text-align:center;}
.t_left{text-align: left;}
.t_right{text-align: right;}
.small{font-size: 12px;}
.fleft{flaot:left;}
.borderBottom{border-bottom:2px solid #AAA;}
.borderRight{border-right:2px solid #AAA;}
.encabezado{
	border:2px solid #AAA;
	width: 100%;
	height: 25px;
	background-color:#EFEFEF;
}

.encabezado .nBox{
	border-right:2px solid #AAA;
	float:left;
	width:25px;
	text-align:center;
	font-weight: bold;
}

.encabezado .titleBox{
	float:left;
	height: 25px;
	width: 752px;
	text-align: center;
	font-weight: bold;
}

.encabezado td{vertical-align: middle;}

.contenido{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	border-bottom:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenido tr td{padding:0;margin:0;}
.contenido td{padding:5px;}

.contenidoList{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenidoList tr td{padding:0;margin:0;}
.contenidoList td{padding:5px;}



.titleInform{
	font-size: 18px;
	font-weight: bold;
	text-align:center;
	margin-bottom:10px;
}

</style>
	<page>

		<br/>
		<div class="t_center"><img src="imagenes/informe/ase.jpg" /></div>
		<br/>
		<div class="titleInform">
			<p>INFORME DE TAREAS OBLIGATORIAS PENDIENTES</p>
		</div>
		<br/>

<!-- 		<div id="entidadOrganizadora">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS DE LA ENTIDAD ORGANIZADORA</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">ENTIDAD ORGANIZADORA</td>
					<td class="borderRight" style="width:150px;">CIF</td>
					<td style="width:150px;">C&Oacute;DIGO ID DE PERFIL</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php //echo $entidad['nombreEntidad']?></td>
					<td class="borderRight" style="width:150px;"><?php //echo $entidad['cifEntidad']?></td>
					<td style="width:150px;"><?php //echo $entidad['codigoEntidad']?></td>
				</tr>
			</table>
		</div>

		<br/> -->

		<div id="empresaParticipante">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS EMPRESA PARTICIPANTE</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:558px;">RAZ&Oacute;N SOCIAL</td>
					<td style="width:150px;">CIF</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:558px;"><?php echo htmlentities($empresaParticipanteRow->razon_social, ENT_QUOTES, 'utf-8') ?></td>
					<td style="width:150px;"><?php echo $empresaParticipanteRow->cif ?></td>
				</tr>
			</table>
		</div>

		<br/>

		<div id="alumnoParticipante">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS PARTICIPANTE</td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" class="contenidoList">
				<tr>
					<td class="borderBottom borderRight" style="width:110px;">Apellidos</td>
					<td class="borderBottom borderRight" style="width:80px;">Nombre</td>
					<td class="borderBottom borderRight t_center" style="width:60px;">NIF</td>
					<td class="borderBottom borderRight t_center" style="width:60px;">AF/Grupo</td>
					<td class="borderBottom borderRight t_center" style="width:80px;">Autoevaluaciones pendientes</td>
					<td class="borderBottom borderRight t_center" style="width:60px;">Trabajos pr&aacute;cticos pendientes</td>
					<td class="borderBottom borderRight t_center" style="width:54px;">Total tareas pendientes</td>
					<td class="borderBottom t_center" style="width:60px;">Porcentaje superado curso</td>
				</tr>
				<?php foreach($informeParticipante as $particpante): ?>
					<tr>
						<td class="borderRight borderBottom " style="width:110px;"><?php echo htmlentities($particpante['apellidos'], ENT_QUOTES, 'utf-8'); ?></td>
						<td class="borderRight borderBottom" style="width:80px;"><?php echo htmlentities($particpante['nombre'], ENT_QUOTES, 'utf-8'); ?></td>
						<td class="borderRight borderBottom t_center" style="width:60px;"><?php echo $particpante['nif']; ?></td>
						<td class="borderRight borderBottom t_center" style="width:60px;"><?php echo $particpante['curso']; ?></td>
						<td class="borderRight borderBottom t_center" style="width:80px;"><?php echo $particpante['autoevalPendientes']; ?></td>
						<td class="borderRight borderBottom t_center" style="width:60px;"><?php echo $particpante['trabajosPendientes']; ?></td>
						<td class="borderRight borderBottom t_center" style="width:54px;"><?php echo $particpante['trabajosPendientes'] + $particpante['autoevalPendientes']; ?></td>
						<td class="borderBottom t_center" style="width:60px;"><?php echo number_format($particpante['porcentajeSuperado'], 2, ',', ' ') ?> %</td>
					</tr>
				<?php endforeach ?>
			</table>
		</div>
	</page>
