<style>

p, table, td{
	margin:0;
	padding:0;	
}
b{font-weight: bold;}

/*CLASES GENERALES */
.t_center{text-align:center;}
.t_left{text-align: left;}
.t_right{text-align: right;}
.small{font-size: 12px;}
.fleft{flaot:left;}
.borderBottom{border-bottom:2px solid #AAA;}
.borderRight{border-right:2px solid #AAA;}
.encabezado{
	border:2px solid #AAA;
	width: 100%;
	height: 25px;
	background-color:#EFEFEF;
}

.encabezado .nBox{
	border-right:2px solid #AAA;
	float:left;
	width:25px;
	text-align:center;
	font-weight: bold;
}

.encabezado .titleBox{
	float:left;
	height: 25px;
	width: 752px;
	text-align: center;
	font-weight: bold;
}

.encabezado td{vertical-align: middle;}

.contenido{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	border-bottom:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenido tr td{padding:0;margin:0;}
.contenido td{padding:5px;}


.titleInform{
	font-size: 18px;
	font-weight: bold;
	text-align:center;
	margin-bottom:10px;
}

</style>
	<page>
	
		<br/>
		<div class="t_center"><img src="imagenes/informe/ase.jpg" /></div>
		<br/>
		<div class="titleInform">
			<p>INFORME DE TAREAS OBLIGATORIAS PENDIENTES</p>
		</div>
		<br/>
		
		<div id="entidadOrganizadora">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS DE LA ENTIDAD ORGANIZADORA</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">ENTIDAD ORGANIZADORA</td>
					<td class="borderRight" style="width:150px;">CIF</td>
					<td style="width:150px;">C&Oacute;DIGO ID DE PERFIL</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php echo $entidad['nombreEntidad']?></td>
					<td class="borderRight" style="width:150px;"><?php echo $entidad['cifEntidad']?></td>
					<td style="width:150px;"><?php echo $entidad['codigoEntidad']?></td>
				</tr>			
			</table>
		</div>
		
		<br/>
		
		<div id="accionFormativa">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS DE LA ACCI&Oacute;N FORMATIVA</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">DENOMINACI&Oacute;N DE LA ACCI&Oacute;N</td>
					<td class="borderRight" style="width:150px;">ACCI&Oacute;N FORMATIVA</td>
					<td style="width:150px;">GRUPO</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php echo htmlentities($informeParticipante['tituloCurso'], ENT_QUOTES, 'utf-8'); ?></td>
					<td class="borderRight" style="width:150px;"><?php echo $codigoAccionFormativa; ?></td>
					<td style="width:150px;"><?php echo str_replace(')', '', $codigoGrupo); ?></td>
				</tr>
			</table>
						
			<table class="contenido" cellpadding="0" cellspacing="0">
				<tr>
					<td class="borderRight" style="width:228px;">FECHA INICIO</td>
					<td class="borderRight" style="width:228px;">FECHA FIN</td>
					<td style="width:228px;">N&ordm; HORAS</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:228px;"><?php echo Fecha::invertir_fecha($curso->f_inicio, '-', '/'); ?></td>
					<td class="borderRight" style="width:228px;"><?php echo Fecha::invertir_fecha($curso->f_fin, '-', '/'); ?></td>
					<td style="width:228px;">100</td>
				</tr>				
			</table>		
		</div>
		
		<br/>

		<div id="empresaParticipante">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS EMPRESA PARTICIPANTE</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:558px;">RAZ&Oacute;N SOCIAL</td>
					<td style="width:150px;">CIF</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:558px;"><?php echo htmlentities($informeParticipante['razonSocial'], ENT_QUOTES, 'utf-8') ?></td>
					<td style="width:150px;"><?php echo $informeParticipante['cif']?></td>
				</tr>			
			</table>
		</div>

		<br/>
	
		<div id="alumnoParticipante">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS PARTICIPANTE</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">Apellidos</td>
					<td class="borderRight" style="width:150px;">Nombre</td>
					<td style="width:150px;">NIF</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php echo htmlentities($informeParticipante['apellidos'], ENT_QUOTES, 'utf-8'); ?></td>
					<td class="borderRight" style="width:150px;"><?php echo htmlentities($informeParticipante['nombre'], ENT_QUOTES, 'utf-8'); ?></td>
					<td style="width:150px;"><?php echo $informeParticipante['nif']; ?></td>
				</tr>
			</table>
		</div>

		<br/>

		<div id="detalleTarea">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">TAREAS PENDIENTES</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:170px;text-align:center;border-bottom:2px solid #AAA;">Autoevaluaciones</td>
					<td class="borderRight" style="width:170px;text-align:center;border-bottom:2px solid #AAA;">Trabajos pr&aacute;cticos</td>
					<td class="borderRight" style="width:170px;text-align:center;border-bottom:2px solid #AAA;">Total tareas pendientes</td>
					<td style="width:150px;text-align:center;border-bottom:2px solid #AAA;">Porcentaje superado</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:168px;text-align:center;"><?php echo $informeParticipante['autoevalPendientes']; ?></td>
					<td class="borderRight" style="width:168px;text-align:center;"><?php echo $informeParticipante['trabajosPendientes']; ?></td>
					<td class="borderRight" style="width:170px;text-align:center;"><?php echo $informeParticipante['trabajosPendientes'] + $informeParticipante['autoevalPendientes']; ?></td>
					<td style="width:150px;text-align:center;"><?php echo number_format($informeParticipante['porcentajeSuperado'], 2, ',', ' ') ?> %</td>
				</tr>
			</table>				
		</div>
	</page>
