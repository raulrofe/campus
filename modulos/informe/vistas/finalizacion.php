<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.informenotas">INFORME NOTAS</span>
</div>
<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
		
		<?php require(dirname(__FILE__) . '/submenu.php') ?>
			
		<div id="tareas">
			<form id="frm_informe_fecha" method="post" action="evaluaciones/informes/finalizacion">
				<div class="burbuja">
					<div>
						<div class="fleft">
							<div class="fleft" style="margin:5px 10px 0 0;" data-translate-html="evaluaciones.fechainicio">
								Fecha inicio
							</div>
							<div class="fleft">
								<input style="width:150px;" id="datepicker" type="text" name="fecha_ini" value="<?php if(isset($post['fecha_ini'])) echo $post['fecha_ini']; else echo date('d/m/Y', strtotime($curso->f_inicio))?>" />
							</div>
						</div>
						<div class="fright">
							<div class="fleft" style="margin:5px 10px 0 0;" data-translate-html="evaluaciones.fechafin">
								Fecha fin
							</div>
							<div class="fleft">
								<input style="width:150px;" id="datepicker2" type="text" name="fecha_fin" value="<?php if(isset($post['fecha_fin'])) echo $post['fecha_fin']; else echo date('d/m/Y', strtotime($curso->f_fin))?>" />
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="fleft" style="width:45%;margin:15px 1% 15px 0">
							<input type="text" name="cifEmpresa" placeholder="Introduzca CIF" style="width:100%;" value="<?php if(isset($post['cifEmpresa'])) echo $post['cifEmpresa'] ?>" data-translate-placeholder="usuario.cif" />
						</div>

						<div class="fright" style="width:45%;margin:15px 0 15px 1%">
							<input type="text" name="nifParticipante" placeholder="Introduzca NIF" style="width:100%;" value="<?php if(isset($post['nifParticipante'])) echo $post['nifParticipante'] ?>" data-translate-placeholder="usuario.dni"/>
						</div>
						<div class="clear"></div>
					</div>	
					<div>
						<div class="fleft" style="width:30%;margin:15px 2% 15px 0">
							<input type="text" name="grupo" placeholder="Introduzca Grupo" style="width:100%;" value="<?php if(isset($post['grupo'])) echo $post['grupo'] ?>" data-translate-placeholder="evaluaciones.grupo" />
						</div>
						<div class="fleft" style="width:30%;margin:15px 2%">
							<input type="text" name="porcentajeInicio" placeholder="Introduzca Porcentaje inicio" style="width:100%;" value="<?php if(isset($post['porcentajeInicio'])) echo $post['porcentajeInicio'] ?>" data-translate-placeholder="evaluaciones.porcentajeinicio" />
						</div>
						<div class="fleft" style="width:30%;margin:15px 0 15px 2%">
							<input type="text" name="porcentajeFin" placeholder="Introduzca Porcentaje fin" style="width:100%;" value="<?php if(isset($post['porcentajeFin'])) echo $post['porcentajeFin'] ?>" data-translate-placeholder="evaluaciones.porcentajefin" />
						</div>
						<div class="clear"></div>
					</div>	
				</div>

				<div>
					<input type="submit" value="Ver informe de tareas" class="width100" data-translate-value="evaluaciones.vertareas"/>
				</div>				
			</form>

			<?php if(isset($informeParticipante)): ?>
					<br/><br/>
					<div class="negrita">
						<div class="fleft">Total de participantes (<?php echo count($informeParticipante) ?>)</div>
						<div class="fright">
							<div class="fleft" style="margin-right:15px;">
								<a href="evaluaciones/informes/tareas/pdf-cif" data-translate-html="evaluaciones.generar">
									Generar Informe por empresa
								</a>
							</div>
							<div class="fleft">
								<a href="#" data-translate-html="evaluaciones.generar2">
									Generear Informe por participante
								</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div style="border-top:1px solid #D5D5D5;margin:10px 0">
							<div style="background-color:#69C6B3;padding:10px 0;border-bottom:1px solid #D5D5D5;border-right:1px solid #D5D5D5;border-left:1px solid #D5D5D5">
								<div class="fleft" style="width:10%;margin:3px;text-align:center" data-translate-html="centro.cif">CIF</div>
								<div class="fleft" style="width:25%;margin:3px 10px;" data-translate-html="centro.razonsocial">RAZ&Oacute;N SOCIAL</div>
								<div class="fleft" style="width:23%;margin:3px;" data-translate-html="centro.apellidos">APELLIDOS</div>
								<div class="fleft" style="width:14%;margin:3px;" data-translate-html="centro.nombre">NOMBRE</div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center" data-translate-html="centro.nif">NIF</div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center;" data-translate-html="centro.afgrupo">AF/GRUPO</div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center;" data-translate-html="centro.sup">% SUP.</div>
								<div class="fleft" style="width:5%;margin:3px;text-align:center;" data-translate-html="centro.detalle">Detalle.</div>
								<div class="clear"></div>
							</div>
						<?php foreach($informeParticipante as $inform): ?>
							<div <?php if(number_format($inform['porcentajeSuperado'], 2, ',', ' ') == '0,00') echo 'class="rojo"'; ?>style="padding:10px 0;border-bottom:1px solid #D5D5D5;border-right:1px solid #D5D5D5;border-left:1px solid #D5D5D5">
								<div class="fleft" style="width:10%;margin:3px;text-align:center"><?php echo $inform['cif'] ?></div>
								<div class="fleft" style="width:25%;margin:3px 10px;">
									<a target="_blank" href="evaluaciones/informes/finalizacion/empresa/<?php echo $inform['idMatricula'] ?>/<?php echo $porcentajeIni ?>/<?php echo $porcentajeFin ?>" rel="tooltip" title="imprimir" data-translate-html="evaluaciones.imprimir">
										<?php echo $inform['razonSocial'] ?>
									</a>
								</div>
								<div class="fleft" style="width:23%;margin:3px;"><?php echo $inform['apellidos'] ?></div>
								<div class="fleft" style="width:14%;margin:3px;"><?php echo $inform['nombre'] ?></div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center"><?php echo $inform['nif'] ?></div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center;"><?php echo $inform['curso'] ?></div>
								<div class="fleft" style="width:6%;margin:3px;text-align:center;"><?php echo number_format($inform['porcentajeSuperado'], 2, ',', ' ') ?></div>
								<div class="fleft" style="width:5%;margin:3px;text-align:center;">
									<a href="#" onclick="viewDetailSuperation(<?php echo $inform['idParticipante'] ?>);return false">
										<img alt="" src="imagenes/menu_vistas/view.png">
									</a>
									<a target="_blank" href="evaluaciones/informes/finalizacion/participante/<?php echo $inform['idMatricula'] ?>" rel="tooltip" title="imprimir">
										<img alt="" src="imagenes/menu_vistas/imprimir.png" width="22px" height="auto">
									</a>								
								</div>
								<div class="clear"></div>

								<!-- Aqui va el detalle de cada participante de manera oculta -->
								<div class="hide detailTareas" id="<?php echo $inform['idParticipante'] ?>" style="color:#000;">
									<div style="background-color:#363636;padding:10px;color:#FFF">
										<div class="fleft" style="width:25%;text-align:center" data-translate-html="usuario.email">Email</div>
										<div class="fleft" style="width:15%;text-align:center" data-translate-html="usuario.telefono">Tel&eacute;fono</div>
										<div class="fleft" style="width:10%;text-align:center" data-translate-html="evaluaciones.amedia">Media Autoevaluaciones</div>
										<div class="fleft" style="width:10%;text-align:center" data-translate-html="evaluaciones.tpmedia">Media Trabajos practicos</div>
										<div class="fleft" style="width:10%;text-align:center" data-translate-html="evaluaciones.fmedia">Media foros</div>
										<div class="fleft" style="width:15%;text-align:center" data-translate-html="evaluaciones.nmedia">Nota media</div>
										<div class="fleft" style="width:15%;text-align:center" data-translate-html="evaluaciones.comentarios">Comentarios</div>
										<div class="clear"></div>
									</div>
									<div style="background-color:#DFDFDF;padding:10px;color:#000">
										<div class="fleft" style="width:25%;text-align:center"><?php echo $inform['email'] ?></div>
										<div class="fleft" style="width:15%;text-align:center"><?php if(isset($inform['telefono'])) echo $inform['telefono']; else echo ' no se indica '?></div>
										<div class="fleft" style="width:10%;text-align:center"><?php echo $inform['mediaAutoevaluaciones'] ?></div>
										<div class="fleft" style="width:10%;text-align:center"><?php echo $inform['mediaTrabajosPracticos'] ?></div>
										<div class="fleft" style="width:10%;text-align:center"><?php echo $inform['mediaForos'] ?></div>
										<div class="fleft" style="width:15%;text-align:center"><?php echo $inform['mediaAutoevaluaciones'] + $inform['mediaTrabajosPracticos'] + $inform['mediaForos'] ?></div>
										<div class="fleft" style="width:15%;text-align:center" data-translate-html="evaluaciones.comentarios">Comentarios</div>
										<div class="clear"></div>	
									</div>						
								</div>

							</div>
						<?php endforeach; ?>
					</div>
			<?php endif; ?>

		</div>
	</div>	
</div>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});

	$("#datepicker2").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});

	function viewDetailSuperation(idAlumno)
	{
		$(".detailTareas").addClass('hide');
		$("#" + idAlumno).removeClass('hide');
	}
</script>