<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.informes">INFORMES</span>
</div>

<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
		
		<?php require(dirname(__FILE__) . '/submenu.php') ?>
			
		<div id="informes">
			<div>
				<table class="tabla">
					<thead>
						<tr>
							<td data-translate-html="integrantes.alumnos">Alumnos</td>
							<?php foreach($cabeceras as $cabecera):?>
							
							<td <?php if($cabecera['aportacion'] == 1) echo 'colspan="2"';?>>
								<span><?php echo $cabecera['nombre']?></span>
							</td>

							<?php endforeach;?>
							<td colspan="2" data-translate-html="evaluaciones.informes" data-translate-html="evaluaciones.total">Total</td>
						</tr>
					</thead>
					<!-- Termina la fila de la cabecera -->
					
					<tbody>
						<?php if($alumnos->num_rows > 0):?>
							<?php while($alumno = $alumnos->fetch_object()):?>
								<?php $totalparticipacion = 0?>
								<tr>
									<td><?php echo Texto::textoPlano($alumno->apellidos . ', ' . $alumno->nombre)?></td>

									<?php reset($cabeceras)?>

									<?php $totalVisitas = 0;?>
									
									<?php foreach($cabeceras as $cabecera):?>
									
										<?php $logsAcceso = $objModelo->obtenerLogAccesosPorAlumno($alumno->idalumnos, $idCurso, $cabecera['idlugar'], $fechaIni, $fechaFin)?>									
												
												<?php 
																						
												if($cabecera['idlugar'] == 5) //Biblioteca
												{
													$numAportacionesBiblioteca = $objModeloBiblioteca->obtenerNumAportacionesPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numAportacionesBiblioteca = $numAportacionesBiblioteca->fetch_object();
													$numParticipacion = $numAportacionesBiblioteca->num;
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 6) //Foro opiniones
												{
													$numParticipacion = $objModeloOpiniones->obtenerNumMensajesPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 7) //Chat cafeteria
												{
													$numParticipacion = $objModeloChat->obtenerNumChatPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 12) //Archivador electronico
												{
													$numParticipacion = $objModeloArchivador->obtenerArcvhivosPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 13) //Foros aula
												{
													$numParticipacion = $objModeloForo->obtenerMensajesPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 16) //Autoevaluaciones
												{
													$numParticipacion = $objModeloAutoeval->obtenerNumAutoevalsPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}
												else if($cabecera['idlugar'] == 20) //Tablon de anuncios publico
												{
													$numParticipacion = $objModeloTablonAnuncios->obtenerNumeroAnunciosAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}	
												else if($cabecera['idlugar'] == 22) //Tablon de anuncios publico
												{
													$numParticipacion = $objModeloChatTutoria->obtenerNumeroChatTutoriaPorAlumno($alumno->idalumnos, $alumno->idcurso);
													$numParticipacion = $numParticipacion->num_rows;
													$totalparticipacion += $numParticipacion;
												}
												
												?>
												
												<?php if($cabecera['aportacion'] == 1):?>											
														<td class="t_center table_celda_informe" style='background-color:#92FF9D'>
															<?php echo $logsAcceso->num_rows; ?>
														</td>
														<td class="t_center table_celda_informe" style='background-color:#53A2FF'>
															<?php echo $numParticipacion?>
														</td>
												<?php else: ?>
														<td class="t_center table_celda_informe" style='background-color:#92FF9D'>
															<?php echo $logsAcceso->num_rows; ?>
														</td>
												<?php endif;?>
																					
													
												<?php
												$totalVisitas += $logsAcceso->num_rows;
												if(isset($numParticipacion))
												{
													unset($numParticipacion);
												}
												?>

									<?php endforeach;?>
									
									
									<td class="t_center table_celda_informe" style='background-color:#92FF9D'><?php echo $totalVisitas?></td>
									<td class="t_center table_celda_informe" style='background-color:#53A2FF'><?php echo $totalparticipacion?></td>
								</tr>
							<?php endwhile;?>
						<?php else:?>
							<tr><td><strong data-translate-html="evaluaciones.noalumnocurso">No hay alumnos en este curso</strong></td></tr>
						<?php endif;?>
					</tbody>
				</table>
			</div>
			
			<br />
				<div class="fleft">
					<div style="overflow:hidden;height: 17px;">
						<span class='fleft' style='width:100px;overflow:hidden;' data-translate-html="evaluaciones.noalumnocurso">Asistencia</span>
						<span class="fleft" style="background:#92FF9D; padding:5px; margin-right:5px; float:fleft;overflow:hidden;">&nbsp;</span>
					</div>
					
					<div class='clear'></div>
					
					<div style="overflow:hidden;height: 17px;">		
						<span class='fleft' style='width:100px;overflow:hidden;' data-translate-html="evaluaciones.noalumnocurso">Participaci&oacute;n</span>
						<span class='fleft' style="background:#53A2FF; padding:5px; margin-right:5px; float:fleft;overflow:hidden;">&nbsp;</span>
					</div>
				</div>
				<div class="fright">
					<div class="menuVista printEvaluation">
						<a href="calificaciones/notas/informe" target="_blank" rel="tooltip" title="imprimir" data-translate-title="evaluaciones.imprimir">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista" />
						</a>
					</div>
				</div>
				
				<div class='clear'></div>
		</div>
	</div>
</div>