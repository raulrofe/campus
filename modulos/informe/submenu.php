<?php 
	if(isset($get['c']))
	{
		if($get['c'] == 'index')
		{
			$tituloTabInforme = 'Informe de participaci&oacute;n y asistencia';
		}
		else if($get['c'] == 'tiempos')
		{
			$tituloTabInforme = 'Informe de tiempo';
		}
		else if($get['c'] == 'grafica')
		{
			$tituloTabInforme = 'Informe gr&aacute;fico';
		}
		else if($get['c'] == 'tareas')
		{
			$tituloTabInforme = 'Tareas';
		}
		else if($get['c'] == 'finalizacion')
		{
			$tituloTabInforme = 'Notas';
		}
	}
	else
	{
		$get['c'] = 'index';
		$tituloTabInforme = 'Informe de participaci&oacute;n y asistencia'; 	
	}
?>

<div class="tituloTabs">
	<div class="tituloEvaluaciones"><?php echo $tituloTabInforme; ?></div>
	
	<div class="fright">
		<div class="menuVista printEvaluation" style="padding:1px;margin-left:5px;">
			<a href="calificaciones/notas/informe" target="_blank" rel="tooltip" title="informe completo del curso">
				<img src="imagenes/menu_vistas/imprimir.png" alt="vista" />
			</a>
		</div>
	</div>

	<div class="submenuTabs">
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'index') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/informes" rel="tooltip" title="Participaci&oacute;n/Asistencia">
				<img src="imagenes/calificaciones/participacion.png" alt="Participaci&oacute;n/Asistencia" />
			</a>
		</div>
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'tareas') echo 'activeSubmenu'; ?>" style="border-left:1px solid #CCC;">
			<a href="evaluaciones/informes/tareas" rel="tooltip" title="Tareas">
				<img src="imagenes/calificaciones/tareas.png" alt="Informe Tareas" />
			</a>
		</div>
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'finalizacion') echo 'activeSubmenu'; ?>" style="border-left:1px solid #CCC;">
			<a href="evaluaciones/informes/finalizacion" rel="tooltip" title="Notas">
				<img src="imagenes/calificaciones/finalizacion.png" alt="Informe Notas" />
			</a>
		</div>
		<div class="elementSubmenuTab trabajosPracticos <?php if($get['c'] == 'tiempos') echo 'activeSubmenu'; ?>"style="border-left:1px solid #CCC;border-right:1px solid #CCC;">
			<a href="evaluaciones/informes/tiempos" rel="tooltip" title="Tiempos">
				<img src="imagenes/calificaciones/tiempos.png" alt="Tiempos" />
			</a>
		</div>
		<div class="elementSubmenuTab foros <?php if($get['c'] == 'grafica') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/informes/grafica" rel="tooltip" title="Gr&aacute;fica">
				<img src="imagenes/calificaciones/evolucion.png" alt="Gr&aacute;fica" />
			</a>
		</div>
	</div>
	
	<div class="clear"></div>
</div>
