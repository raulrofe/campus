<?php

$idCurso = Usuario::getIdCurso();
$post = Peticion::obtenerPost();

$get = Peticion::obtenerGet();

$objModelo = new ModeloInforme();

$js = null;

$curso = $objModelo->obtenerCurso($idCurso);
$curso = $curso->fetch_object();

if(isset($post['idalumno'], $post['fecha_ini'], $post['fecha_fin']) && is_numeric($post['idalumno']))
{
	// convertimos las fechas a array
	$dateStart = explode('/', $post['fecha_ini']);
	$dateEnd = explode('/', $post['fecha_fin']);
	
	// comprobamos si las fechas son validas
	if((count($dateStart) == 3 && count($dateEnd) == 3) &&
		(checkdate($dateStart[1], $dateStart[0], $dateStart[2]) && checkdate($dateEnd[1], $dateEnd[0], $dateEnd[2])))
	{
		$dateStartCourse = $dateStart[2] . '-' . $dateStart[1] . '-' . $dateStart[0];
		$dateEndCourse = $dateEnd[2] . '-' . $dateEnd[1] . '-' . $dateEnd[0];
		
		$diasCurso = ceil((strtotime($dateEndCourse) - strtotime($dateStartCourse)) / 86400);
		
		$data = array();
		
		// inserta a un array todos los dias que duro el curso
		for($c = 0; $c <= $diasCurso; $c++)
		{
			$data[date('d/m/Y', strtotime($dateStartCourse) + 86400 * $c)] = 0;
		}
		
		// obtenemos los datos de accesos del usuario
		$lugares = $objModelo->obtenerLugares();
		while($lugar = $lugares->fetch_object())
		{
			$logAcceso = $objModelo->obtenerLogAccesosPorAlumno(
				$post['idalumno'],
				$idCurso,
				$lugar->idlugar,
				$dateStart[2] . '-' . $dateStart[1] . '-' . $dateStart[0],
				$dateEnd[2] . '-' . $dateEnd[1] . '-' . $dateEnd[0]
			);
			
			if($logAcceso->num_rows > 0)
			{
				while($item = $logAcceso->fetch_object())
				{
					if(isset($item->fecha_entrada, $data[date('d/m/Y', strtotime($item->fecha_entrada))]))
					{
						$data[date('d/m/Y', strtotime($item->fecha_entrada))] += $item->numAccesos;
					}
				}
			}
		}
	}
}
else
{
	Alerta::guardarMensajeInfo('debesseleccionaralumno','Debes seleccionar una alumno para ver su gráfica');
}

$alumnos = $objModelo->obtenerAlumnosPorCurso($idCurso);
	
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'grafica');