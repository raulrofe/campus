<?php
$idCurso = Usuario::getIdCurso();
$post = Peticion::obtenerPost();

if(isset($post['fecha_ini'], $post['fecha_fin']))
{
	$fechaIni = Fecha::invertir_fecha($post['fecha_ini'], '/');
	$fechaFin = Fecha::invertir_fecha($post['fecha_fin'], '/');
	
	mvc::cargarModuloSoporte('foro');
	mvc::cargarModuloSoporte('autoevaluaciones');
	mvc::cargarModuloSoporte('opiniones');
	mvc::cargarModeloAdicional('biblioteca');
	mvc::cargarModeloAdicional('tablon_anuncios');
	mvc::cargarModeloAdicional('archivador_electronico');
	mvc::cargarModeloAdicional('chat');
	mvc::cargarModeloAdicional('chat_tutoria');
	
	$objModeloOpiniones = new ModeloOpiniones();
	$objModeloForo = new modeloForo();
	$objModeloAutoeval = new Autoevaluacion();
	$objModelo = new ModeloInforme();
	$objModeloBiblioteca = new ModeloBiblioteca();
	$objModeloTablonAnuncios = new TablonAnuncios();
	$objModeloArchivador = new ArchivadorElectronico();
	$objModeloChat = new ModeloChat();
	$objModeloChatTutoria = new ModeloChatTutoria(); 
	
	$cabeceraLugares = $objModelo->obtenerLugares();
	$cabeceras = array();
	while($cabeceraLugar = $cabeceraLugares->fetch_object())
	{
		$cabeceras[] = array('idlugar' => $cabeceraLugar->idlugar, 
							 'nombre' => $cabeceraLugar->nombre,
							 'aportacion' => $cabeceraLugar->aportacion);
	}
	
	$lugaresMostrar = array();
	if(isset($post['idlugar']))
	{
		if(is_numeric($post['idlugar']))
		{
			$lugaresMostrar[] = $post['idlugar'];
		}
		else if($post['idlugar'] == 'principal')
		{
			$lugaresMostrar = array(1, 2, 3, 5);
		}
		else if($post['idlugar'] == '')
		{
			foreach($cabeceras as $idLugares)
			{
				$lugaresMostrar[] = $idLugares['idlugar'];
			}
		}
	}
	
	if(isset($post['idalumno'], $post['idlugar']) && is_numeric($post['idalumno']))
	{
		$alumnos = $objModelo->obtenerAlumnosPorCurso(Usuario::getIdCurso(), $post['idalumno']);
	}
	else
	{
		$alumnos = $objModelo->obtenerAlumnosPorCurso(Usuario::getIdCurso());
	}
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'informe');
}
else
{
	Url::redirect('evaluaciones/informes');
}