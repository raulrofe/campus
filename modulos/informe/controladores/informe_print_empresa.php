<?php
//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');

//Libreri curso
$objCurso = new Cursos();

//Libreria para generar el PDF
$html2Pdf = new HTML2PDF();

mvc::cargarModuloSoporte('notas');
$objModeloNotas = new ModeloNotas();
$objNotas = new Notas();

mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$get = Peticion::obtenerGet();

//Obtengo los datos del curso
$idCurso = Usuario::getIdCurso();
$objModelo = new ModeloInforme();
$curso = $objModelo->obtenerCurso($idCurso);
$curso = $curso->fetch_object();

//si existe POST
if(isset($get))
{
	if(isset($get['idMatricula'], $get['porcentajei'], $get['porcentajef']) && is_numeric($get['idMatricula']))
	{
		//Obtengo los participantes
		$empresaParticipante = $objModelo->getIdCentroByMatricula($get['idMatricula']);

		if($empresaParticipante->num_rows == 1)
		{
			$empresaParticipanteRow = $empresaParticipante->fetch_object();
		
			$participantes = $objModelo->getEmpresasByFechasCurso($empresaParticipanteRow->f_inicio, $empresaParticipanteRow->f_fin, $empresaParticipanteRow->cif);
			if($participantes->num_rows > 0)
			{
				//Entidad Organizadora
				$entidadOrganizadora = $objCurso->getEntidadOrganizadora();
				if($entidadOrganizadora->num_rows == 1)
				{
					$entidad = array();		
					$entidadOrganizadora = $entidadOrganizadora->fetch_object();				
					$entidad['nombreEntidad'] 	= $entidadOrganizadora->nombre_entidad;
					$entidad['cifEntidad'] 		= $entidadOrganizadora->cif;
					$entidad['codigoEntidad'] 	= '115-00';
				}

				$informeParticipante = array();
				$cont = 0;

				while($participante = $participantes->fetch_object())
				{
					if(is_numeric($participante->idalumnos))
					{
						$superado = false;

						//Obtengo la configuracion de las notas del curso
						$configNotas = $objModeloNotas->obtenerConfigPorcentajes($participante->idcurso);
						$configNotas = $configNotas->fetch_object();

						//Obtener el numero de test del curso
						$numTest = $objModeloNotas->obtenerNumTestCurso($participante->idcurso);
						if($numTest->num_rows > 0)
						{
							$numTest = $numTest->fetch_object();
							$numTest = $numTest->numTestCurso;
						}

						//Obtener el numero de trabajos practicos de un curso
						$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($participante->idcurso);
						if($numTrabajos->num_rows > 0)
						{
							$numTrabajos = $numTrabajos->fetch_object();
							$numTrabajos = $numTrabajos->numTrabajosPracticos;
						}

						$tareasTotales = $numTest + $numTrabajos;
						$tareasMinimas = ceil($tareasTotales * 0.75);

						//Trabajos practicos realizados
						$numTpParticipado = 0;
						$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($participante->idalumnos, $participante->idcurso);
						while($tpParticipado = $trabajosParticipado->fetch_object())
						{
							if($tpParticipado->nota_trabajo >= 5)
							{
								$numTpParticipado ++;	
							}
						}

						$trabajosPendientes = $numTrabajos - $numTpParticipado;

						$porcentajeTrabajos = ($numTpParticipado * 100) / $numTrabajos;

						//Test realizados
						$idMatricula = Usuario::getIdMatriculaDatos($participante->idalumnos, $participante->idcurso);
						$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);
						$numTestRealizado = 0;
						$testSuspenso = 0;
						if($numTestAlumno->num_rows > 0)
						{
							while($testParticipado = $numTestAlumno->fetch_object())
							{
								if($testParticipado->nota >= 5)
								{
									$numTestRealizado ++; 
								}
								else if(!is_null($testParticipado->nota ) && $testParticipado->nota < 5 && $testParticipado->nota >=0)
								{
									$testSuspenso++;
								}
							}
						}

						$autoevaluacionesPendientes = $numTest - $numTestRealizado;

						// ****************************** NOTAS MEDIAS *******************************

						//Nota media autoevaluaciones
						$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
						$mediaTests = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);

						//Nota media de los trabajos practicos
						$objNotas->setCurso($participante->idcurso);
						$notaTPReduce = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
						$notaTP = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);

						$tareasRealizadas = $numTestRealizado + $numTpParticipado;

						$porcentajeSuperado = $tareasRealizadas * 100 / $tareasTotales;

						if($tareasRealizadas >= $tareasMinimas && $porcentajeSuperado >= 75)
						{
							$trabajosRelizar = 0;
							$autoevalRelizar = 0;
							$superado = true;
						}
						else
						{
							$autoevalRelizar = $autoevaluacionesPendientes;

							if($numTest + $numTpParticipado < $tareasMinimas)
							{
								$trabajosRelizar = $tareasMinimas - $numTest - $numTpParticipado;
							}
							else
							{
								$trabajosRelizar	 = 0;
							}
						}

						if($porcentajeSuperado >= $get['porcentajei'] && $porcentajeSuperado <= $get['porcentajef'])
						{
							$informeParticipante[$cont]['idMatricula']			= $participante->idmatricula;
							$informeParticipante[$cont]['idParticipante']		= $participante->idalumnos;
							$informeParticipante[$cont]['nombre']				= $participante->nombre;
							$informeParticipante[$cont]['apellidos']			= $participante->apellidos;
							$informeParticipante[$cont]['telefono']				= $participante->telefono;
							$informeParticipante[$cont]['email']				= $participante->email;						
							$informeParticipante[$cont]['nif']					= $participante->dni;
							$informeParticipante[$cont]['cif']					= $participante->cif;
							$informeParticipante[$cont]['razonSocial']			= $participante->razon_social;
							$informeParticipante[$cont]['trabajosPendientes']	= $trabajosRelizar;
							$informeParticipante[$cont]['autoevalPendientes']	= $autoevalRelizar;
							$informeParticipante[$cont]['autoevalSuspensas']	= $testSuspenso;
						
							$grupo = explode('(', $participante->titulo);

							$informeParticipante[$cont]['curso']				= str_replace(')', '', $grupo[1]);
							$informeParticipante[$cont]['porcentajeSuperado'] 	= $porcentajeSuperado;

							$cont++;
						}						
					}
				}
			}
		}
	}
}

ob_start();
include(PATH_ROOT . 'modulos/informe/vistas/template/informe_tareas_empresa.php');

$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
	//$html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	
	$html2pdf->Output('informe_notas_participantes' . $empresaParticipanteRow->cif . '.pdf', 'D');
}
catch(HTML2PDF_exception $e) {echo $e;}

//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tareas');