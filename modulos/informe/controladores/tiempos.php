<?php

$idCurso = Usuario::getIdCurso();

$get = Peticion::obtenerGet();

$objModelo 	= new ModeloInforme();
$logTiempos = $objModelo->obtenerTiemposAccesosPorAlumno($idCurso);
$alumnos 	= $objModelo -> obtenerMatriculasCurso($idCurso);

//Obtengo codigo inicio grupo para relacionarlo con Iberform
$curso 		= $objModelo -> obtenerDatosCursoPorSesion($idCurso)->fetch_object();
$tituloDesglosado = explode('/', $curso->titulo);
$codigoIG = str_replace(')', '', $tituloDesglosado[1]);
$accionFormativa = substr($tituloDesglosado[0], -5);

// Funcion auxiliar para ordenar un array por el campo fecha_entrada
function compareFechaEntrada($a, $b){
	if(!empty($a->fecha_entrada) && !empty($b->fecha_entrada)){
	    if (strtotime($a->fecha_entrada) == strtotime($b->fecha_entrada)) {
	        return 0;
	    }
	    return (strtotime($a->fecha_entrada) < strtotime($b->fecha_entrada)) ? -1 : 1;
	}else{
		return 0;
	}
}

// Funcion auxiliar para ordenar un array por el campo fecha_entrada
function secondToFormat($cantidad){
	$seconds = $cantidad;
	$hours = floor($seconds / 3600);
	$seconds -= $hours * 3600;
	$minutes = floor($seconds / 60);
	$seconds -= $minutes * 60;

	$horas = ($hours == 0) ?  '0': $hours; 
	$horas = ($hours >= 10) ? $hours : '0'.$hours; 
	$minutes = ($minutes >= 10) ? $minutes : '0'.$minutes; 
	$seconds = ($seconds >= 10) ? $seconds : '0'.$seconds;
     
  	$str = $horas . ':' . $minutes . ':' . $seconds; 

  	return $str;
}

// Funcion para crear un registro real
function createRegistroReal($InicioReal, $FinReal, $accesosReales){
	$diferencia = strtotime($FinReal) - strtotime($InicioReal);
	$registroReal = array(
		'fecha_entrada' 		=> $InicioReal,
		'fecha_salida' 			=> $FinReal,
		'diferencia' 			=> $diferencia,
		'diferenciaFormateada' 	=> secondToFormat($diferencia)
	);
	array_push ( $accesosReales, $registroReal );

	return $accesosReales;
}

// Funcion para sumar tiempos
function sumaTiempos($InicioReal, $FinReal, $sumaTiempos){
	$diferencia = strtotime($FinReal) - strtotime($InicioReal);
	$sumaTiempos += $diferencia;

	return $sumaTiempos;
}

/////////////////
// INICIO SCRIPT
/////////////////

if($alumnos->num_rows > 0){

	// Reset Variables
	$tiempos = array();
	$idAlumno = 0;
	$fecha_salida = 0;
	$sumaTiempos = 0;


	// Iteracion por cada alumno del curso
	while($alumno = $alumnos->fetch_object())	{

		// Obtenemos el ID para el indice del array
		$idAlumno = $alumno->idalumnos;

		// Traemos los accesos de cada alumno
		$accesos = $objModelo->obtenerAccesosMatricula($alumno->idmatricula);

		// Traemos las sesiones de cada alumno
		$sessions = $objModelo->obtenerSessionsMatricula($alumno->idmatricula);

		// Guardamos los accesos en un array
		$array=array();
		if($accesos->num_rows > 0){
			while($register = $accesos->fetch_object()){
				array_push ( $array, $register );
			}
		}
		if($sessions->num_rows > 0){
			while($register = $sessions->fetch_object()){
				array_push ( $array, $register );
			}
		}

		// Ordenamos el array de accesos y sesiones por fecha de entrada
		usort($array, "compareFechaEntrada");

		// Guardamos ese array en cada alumno
		$alumno->accesos 	= $array;
		$tiempos[$idAlumno] = $alumno;
	}


	// Recorremos los tiempos para sacar las conexiones reales (Por solapamiento de tiempo entre pestañas del navegador)
	foreach($tiempos as $alumno){

		$accesosReales = array();

		$InicioReal=null;
		$FinReal=null;
		$sumaTiempos = 0;


		for( $i=0; $i < count($alumno->accesos); $i++) {

			// Cacheamos el acceso
			$acceso = $alumno->accesos[$i];
			$inicioAcceso = $acceso->fecha_entrada;
			$finAcceso = $acceso->fecha_salida;	
			// Margen de 10 minutos entre fecha_salida de una conexion y la fecha_entrada de la siguiente conexion
			$margen_milisegundos=10*60; // 10 minutos

			// Si es el primer registro
			if($i ==  0){
				$InicioReal = $inicioAcceso;
				$FinReal = $finAcceso;
			}
			// Si es el último registro
			elseif($i == count($alumno->accesos)-1){

				// Si no existe Inicio Real, ponemos el inicio de este ultimo acceso
				if(empty($InicioReal) || $InicioReal == '0000-00-00 00:00:00'){ 
					$InicioReal = $inicioAcceso;		
				}
				// Si no existe Fin Real, vemos si ponemos el fin de este ultimo acceso, o el inciio si el fin esta vacio (Daría un rango real de 0s)
				if(empty($FinReal) || $FinReal == '0000-00-00 00:00:00'){ 
					if(empty($finAcceso) || $finAcceso == '0000-00-00 00:00:00'){ 
						$FinReal = $inicioAcceso;
					}else{
						$FinReal = $finAcceso;
					}
				}
				// Cerramos el rango real
				$accesosReales = createRegistroReal($InicioReal, $FinReal, $accesosReales);
				// Sumamos al contador de tiempo
				$sumaTiempos = sumaTiempos($InicioReal, $FinReal, $sumaTiempos);
				
			}
			// Si no es el primer ni el último registro
			else{
				/*
				if($alumno->idalumnos == 4505){
					print_r("<br>".$inicioAcceso." - " .$finAcceso);
					print_r("<br>".$InicioReal." - " .$FinReal);
				}
				*/
				// Si entra dentro del rango anterior (Se SOLAPA)
				if(strtotime($inicioAcceso) <= (strtotime($FinReal) + $margen_milisegundos)){
					// Si existe "Fin de Acceso" y si el fin del acceso es mayor que el guarado, sobreescribimos
					if(!empty($finAcceso) && (strtotime($finAcceso) > strtotime($FinReal))){
						$FinReal = $finAcceso;
					}

				}
				// Si NO entra dentro del rango anterior (NO se solapa)
				else{
		
					// Si no existe un Fin para ese rango lo obviamos y pasamos al siguiente rango
					if(empty($FinReal) || $FinReal == '0000-00-00 00:00:00'){ 
						$FinReal = $inicioAcceso;
					}
					
					// Si existe un rango real, lo guardamos para empezar con el nuevo rango que no solapa con el anteiorr
					if(!empty($InicioReal) && !empty($FinReal)){
						// Cerramos el rango real
						$accesosReales = createRegistroReal($InicioReal, $FinReal, $accesosReales);
						// Sumamos al contador de tiempo
						$sumaTiempos = sumaTiempos($InicioReal, $FinReal, $sumaTiempos);
					}


					// Ponemos las variables con los nuevos valores, si esta vacio el fin de acceso del acceso obviamos este registro
					if(!empty($finAcceso) && $finAcceso != '0000-00-00 00:00:00'){ 
						$InicioReal = $inicioAcceso;
						$FinReal = $finAcceso; 
					}else{
						$InicioReal = '';
						$FinReal = '';
					}
								
				}
				
			}  
		}


		$alumno->primero = strtotime($alumno->accesos[0]->fecha_entrada);
		$alumno->ultimo = (!empty($alumno->fecha_conexion_actual))? strtotime($alumno->fecha_conexion_actual) : strtotime($alumno->accesos[count($alumno->accesos)]->fecha_salida);
	
		// Filtramos y quitamos los registros de duracion menor de 5s
		$accesosRealesFiltrados = array();
		foreach($accesosReales as $accesosReal){
			if($accesosReal['diferencia'] > 10){
				array_push($accesosRealesFiltrados, $accesosReal);
			}
		}

		$alumno->accesos_reales = $accesosRealesFiltrados;
		$alumno->suma 			= $sumaTiempos;
		$alumno->sumaFormateada = secondToFormat($sumaTiempos);

		//Script para las horas cofinanciadas

		$dataIberform = array(
			'nif'				=> $alumno->dni,
			'codigoIG'			=> $codigoIG,
			'accionFormativa' 	=> $accionFormativa
		);		

		$horarioLaboral = $objModelo->horasCofinanciadasIberform($dataIberform);

		$horaInicioLaboralManana 	= strtotime($horarioLaboral->horariom_inicio);
		$horaFinLaboralManana 		= strtotime($horarioLaboral->horariom_fin);

		$horaInicioLaboralTarde 	= strtotime($horarioLaboral->horariot_inicio);
		$horaFinLaboralTarde 		= strtotime($horarioLaboral->horariot_fin);

		$horaConfinaciacion = 0;

		foreach($accesosRealesFiltrados as $accesoReal) {
			$timeInicio = strtotime($accesoReal['fecha_entrada']);
			$timeFin	= strtotime($accesoReal['fecha_salida']);

			$diaSemanaNumerico 	= date("N", $timeInicio);
			$horaInicio 		= strtotime(date("H:i:s", $timeInicio));
			$horaFin 			= strtotime(date("H:i:s", $timeFin));				

			if(!empty($horarioLaboral->diasLaborales) && in_array($diaSemanaNumerico, $horarioLaboral->diasLaborales)) {

				if(!empty($horarioLaboral->horariom_inicio) && !empty($horarioLaboral->horariom_fin)) {

					$start=0;
					$end=0;
					// MAÑANAS
					if($horaInicio <= $horaInicioLaboralManana && $horaFin >= $horaInicioLaboralManana) {
						$start = $horaInicioLaboralManana;
					}elseif($horaInicio >= $horaInicioLaboralManana && $horaInicio <= $horaFinLaboralManana) {
						$start = $horaInicio;
					}

					if($horaFin <= $horaFinLaboralManana && $horaFin >= $horaInicioLaboralManana) {
						$end = $horaFin;
					}elseif($horaFin >= $horaFinLaboralManana && $horaInicio <= $horaFinLaboralManana) {
						$end = $horaFinLaboralManana;
					}
					if(!empty($end)){
						$horaConfinaciacion = $horaConfinaciacion + ($end - $start);			
					}
				}

				if(!empty($horarioLaboral->horariot_inicio) && !empty($horarioLaboral->horariot_fin)) {
					$start=0;
					$end=0;

					// TARDES
					if($horaInicio <= $horaInicioLaboralTarde && $horaFin >= $horaInicioLaboralTarde) {
						$start = $horaInicioLaboralTarde;
					}elseif($horaInicio >= $horaInicioLaboralTarde && $horaInicio <= $horaFinLaboralTarde) {
						$start = $horaInicio;
					}

					if($horaFin <= $horaFinLaboralTarde && $horaFin >= $horaInicioLaboralTarde) {
						$end = $horaFin;
					}elseif($horaFin >= $horaFinLaboralTarde && $horaInicio <= $horaFinLaboralTarde) {
						$end = $horaFinLaboralTarde;
					}
					if(!empty($end)){
						$horaConfinaciacion = $horaConfinaciacion + ($end - $start);			
					}
				}
			}
		}

		$alumno->horaConfinaciacion = secondToFormat($horaConfinaciacion);

		// if($_SESSION['idusuario'] == 3976){
		// 	print_r('<pre>');
		// 	print_r($_SESSION);
		// 	print_r('</pre>');
		// 	print_R('<br>');
		// }
	}

	/*
	print_r('<pre>');
	print_r($tiempos);
	print_r('</pre>');
	print_R('<br>');	 
	*/
}


require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tiempos');