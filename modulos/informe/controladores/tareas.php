<?php

mvc::cargarModuloSoporte('notas');
$objModeloNotas = new ModeloNotas();
$objNotas = new Notas();

mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$get = Peticion::obtenerGet();

//Obtengo los datos del curso
$idCurso = Usuario::getIdCurso();
$objModelo = new ModeloInforme();
$curso = $objModelo->obtenerCurso($idCurso);
$curso = $curso->fetch_object();

//si existe POST
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();

	if(isset($post['fecha_ini'], $post['fecha_fin']))
	{
		//Busco todos los cursos segun la fecha de inicio y fecha fin
		$fechaIni = Fecha::invertir_fecha($post['fecha_ini'], '/');
		$fechaFin = Fecha::invertir_fecha($post['fecha_fin'], '/');
		$cif = null;
		$nif = null;		
		$grupo = null;
		$porcentajeIni = 0;
		$porcentajeFin = 100;

		//Datos para la busqueda
		if(!empty($post['cifEmpresa']))
		{
			$cif = $post['cifEmpresa'];
		}
		if(!empty($post['nifParticipante']))
		{
			$nif = $post['nifParticipante'];
		}
		if(!empty($post['grupo']))
		{
			$grupo = $post['grupo'];
		}

		//Prcentaje creacion array
		if(!empty($post['porcentajeInicio']))
		{
			$porcentajeIni = $post['porcentajeInicio'];
		}
		if(!empty($post['porcentajeFin']))
		{
			$porcentajeFin = $post['porcentajeFin'];
		}

		$participantes = $objModelo->getEmpresasByFechasCurso($fechaIni, $fechaFin, $cif, $nif, $grupo);
		if($participantes->num_rows > 0)
		{
			$informeParticipante = array();
			$cont = 0;
			$alumnoSuspenso = array();
			while($participante = $participantes->fetch_object())
			{
				if(is_numeric($participante->idalumnos))
				{
					$superado = false;

					//Obtengo la configuracion de las notas del curso
					$configNotas = $objModeloNotas->obtenerConfigPorcentajes($participante->idcurso);
					$configNotas = $configNotas->fetch_object();

					//Obtener el numero de test del curso
					$numTest = $objModeloNotas->obtenerNumTestCurso($participante->idcurso);
					if($numTest->num_rows > 0)
					{
						$numTest = $numTest->fetch_object();
						$numTest = $numTest->numTestCurso;
					}

					//Obtener el numero de trabajos practicos de un curso
					$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($participante->idcurso);
					if($numTrabajos->num_rows > 0)
					{
						$numTrabajos = $numTrabajos->fetch_object();
						$numTrabajos = $numTrabajos->numTrabajosPracticos;
					}

					$tareasTotales = $numTest + $numTrabajos;
					$tareasMinimas = ceil($tareasTotales * 0.75);

					//Trabajos practicos realizados
					$numTpParticipado = 0;
					$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($participante->idalumnos, $participante->idcurso);
					while($tpParticipado = $trabajosParticipado->fetch_object())
					{
						if($tpParticipado->nota_trabajo >= 5)
						{
							$numTpParticipado ++;	
						}
					}

					$trabajosPendientes = $numTrabajos - $numTpParticipado;

					$porcentajeTrabajos = ($numTpParticipado * 100) / $numTrabajos;

					//Test realizados
					$idMatricula = Usuario::getIdMatriculaDatos($participante->idalumnos, $participante->idcurso);
					$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);
					$numTestRealizado = 0;
					$testSuspenso = 0;
					if($numTestAlumno->num_rows > 0)
					{
						while($testParticipado = $numTestAlumno->fetch_object())
						{
							if($testParticipado->nota >= 5)
							{
								$numTestRealizado ++; 
							}
							else if(!is_null($testParticipado->nota ) && $testParticipado->nota < 5 && $testParticipado->nota >=0)
							{
								$testSuspenso++;
							}
						}
					}

					$autoevaluacionesPendientes = $numTest - $numTestRealizado;

					// ****************************** NOTAS MEDIAS *******************************

					//Nota media autoevaluaciones
					$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
					$mediaTests = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);

					//Nota media de los trabajos practicos
					$objNotas->setCurso($participante->idcurso);
					$notaTPReduce = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
					$notaTP = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);

					$tareasRealizadas = $numTestRealizado + $numTpParticipado;

					$porcentajeSuperado = $tareasRealizadas * 100 / $tareasTotales;

					if($tareasRealizadas >= $tareasMinimas && $porcentajeSuperado >= 75)
					{
						$trabajosRelizar = 0;
						$autoevalRelizar = 0;
						$superado = true;
					}
					else
					{
						$autoevalRelizar = $autoevaluacionesPendientes;

						if($numTest + $numTpParticipado < $tareasMinimas)
						{
							$trabajosRelizar = $tareasMinimas - $numTest - $numTpParticipado;
						}
						else
						{
							$trabajosRelizar	 = 0;
						}
					}

					if($porcentajeSuperado >= $porcentajeIni && $porcentajeSuperado <= $porcentajeFin)
					{
						$informeParticipante[$cont]['idMatricula']			= $participante->idmatricula;
						$informeParticipante[$cont]['idParticipante']		= $participante->idalumnos;
						$informeParticipante[$cont]['nombre']				= $participante->nombre;
						$informeParticipante[$cont]['apellidos']			= $participante->apellidos;
						$informeParticipante[$cont]['telefono']				= $participante->telefono;
						$informeParticipante[$cont]['email']				= $participante->email;						
						$informeParticipante[$cont]['nif']					= $participante->dni;
						$informeParticipante[$cont]['cif']					= $participante->cif;
						$informeParticipante[$cont]['razonSocial']			= $participante->razon_social;
						$informeParticipante[$cont]['trabajosPendientes']	= $trabajosRelizar;
						$informeParticipante[$cont]['autoevalPendientes']	= $autoevalRelizar;
						$informeParticipante[$cont]['autoevalSuspensas']	= $testSuspenso;
					
						$grupo = explode('(', $participante->titulo);

						$informeParticipante[$cont]['curso']				= str_replace(')', '', $grupo[1]);
						$informeParticipante[$cont]['porcentajeSuperado'] 	= $porcentajeSuperado;

						if($testSuspenso > 0)
						{
							$alumnoSuspenso[$cont]['alumSusp'] = $participante->idmatricula . ' /-/ ' . $participante->apellidos . ', ' . $participante->nombre . ': ' . $testSuspenso;
						}

						$cont++;
					}						
				}
			}

			/*
			echo '<pre>';
				print_r($alumnoSuspenso);
			echo '</pre>';
			*/
		}
	}
}



require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tareas');