<?php
//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');

//Libreri curso
$objCurso = new Cursos();

//Libreria para generar el PDF
$html2Pdf = new HTML2PDF();

mvc::cargarModuloSoporte('notas');
$objModeloNotas = new ModeloNotas();
$objNotas = new Notas();

mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$get = Peticion::obtenerGet();

//Obtengo los datos del curso
$idCurso = Usuario::getIdCurso();
$objModelo = new ModeloInforme();
$curso = $objModelo->obtenerCurso($idCurso);
$curso = $curso->fetch_object();

//si existe POST
if(isset($get))
{
	if(isset($get['idMatricula']) && is_numeric($get['idMatricula']))
	{
		$participantes = $objModelo->getInformeParticipante($get['idMatricula']);
		if($participantes->num_rows == 1)
		{
			$informeParticipante = array();
			$cont = 0;
			while($participante = $participantes->fetch_object())
			{
				if(is_numeric($participante->idalumnos))
				{
					//Entidad Organizadora
					$entidadOrganizadora = $objCurso->getEntidadOrganizadora();
					if($entidadOrganizadora->num_rows == 1)
					{
						$entidad = array();		
						$entidadOrganizadora = $entidadOrganizadora->fetch_object();				
						$entidad['nombreEntidad'] 	= $entidadOrganizadora->nombre_entidad;
						$entidad['cifEntidad'] 		= $entidadOrganizadora->cif;
						$entidad['codigoEntidad'] 	= '115-00';
					}

					$superado = false;

					//Obtengo la configuracion de las notas del curso
					$configNotas = $objModeloNotas->obtenerConfigPorcentajes($participante->idcurso);
					$configNotas = $configNotas->fetch_object();

					//Obtener el numero de test del curso
					$numTest = $objModeloNotas->obtenerNumTestCurso($participante->idcurso);
					if($numTest->num_rows > 0)
					{
						$numTest = $numTest->fetch_object();
						$numTest = $numTest->numTestCurso;
					}

					//Obtener el numero de trabajos practicos de un curso
					$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($participante->idcurso);
					if($numTrabajos->num_rows > 0)
					{
						$numTrabajos = $numTrabajos->fetch_object();
						$numTrabajos = $numTrabajos->numTrabajosPracticos;
					}

					$tareasTotales = $numTest + $numTrabajos;
					$tareasMinimas = ceil($tareasTotales * 0.75);

					//Trabajos practicos realizados
					$numTpParticipado = 0;
					$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($participante->idalumnos, $participante->idcurso);
					while($tpParticipado = $trabajosParticipado->fetch_object())
					{
						if($tpParticipado->nota_trabajo >= 5)
						{
							$numTpParticipado ++;	
						}
					}

					$trabajosPendientes = $numTrabajos - $numTpParticipado;

					$porcentajeTrabajos = ($numTpParticipado * 100) / $numTrabajos;

					//Test realizados
					
					$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($get['idMatricula']);
					$numTestRealizado = 0;
					if($numTestAlumno->num_rows > 0)
					{
						while($testParticipado = $numTestAlumno->fetch_object())
						{
							if($testParticipado->nota >= 5)
							{
								$numTestRealizado ++; 
							}
						}
					}

					$autoevaluacionesPendientes = $numTest - $numTestRealizado;

					// *********************************************** NOTAS MEDIAS ****************************************************

					//Nota media autoevaluaciones
					$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
					$mediaTests = $objNotas->calcularNotaMediaAutoeval($participante->idalumnos, $participante->idcurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);

					//Nota media de los trabajos practicos
					$objNotas->setCurso($participante->idcurso);
					$notaTPReduce = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
					$notaTP = $objNotas->calcularMediaTrabajosPract($participante->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);

					$tareasRealizadas = $numTestRealizado + $numTpParticipado;

					$porcentajeSuperado = $tareasRealizadas * 100 / $tareasTotales;

					if($tareasRealizadas >= $tareasMinimas && $porcentajeSuperado >= 75)
					{
						$trabajosRelizar = 0;
						$autoevalRelizar = 0;
						$superado = true;
					}
					else
					{
						$autoevalRelizar = $autoevaluacionesPendientes;

						if($numTest + $numTpParticipado < $tareasMinimas)
						{
							$trabajosRelizar = $tareasMinimas - $numTest - $numTpParticipado;
						}
						else
						{
							$trabajosRelizar	 = 0;
						}
					}

					$informeParticipante['idMatricula']			= $participante->idmatricula;
					$informeParticipante['idParticipante']		= $participante->idalumnos;
					$informeParticipante['nombre']				= $participante->nombre;
					$informeParticipante['apellidos']			= $participante->apellidos;
					$informeParticipante['telefono']				= $participante->telefono;
					$informeParticipante['email']				= $participante->email;						
					$informeParticipante['nif']					= $participante->dni;
					$informeParticipante['cif']					= $participante->cif;
					$informeParticipante['razonSocial']			= $participante->razon_social;
					$informeParticipante['trabajosPendientes']	= $trabajosRelizar;
					$informeParticipante['autoevalPendientes']	= $autoevalRelizar;

					$grupo = explode('(', $participante->titulo);

					$informeParticipante['tituloCurso']    		= $participante->titulo;
					$informeParticipante['curso']				= str_replace(')', '', $grupo[1]);

					$codigosCursoSeparate = explode('/', $grupo[1]);
					$codigoAccionFormativa = $codigosCursoSeparate[0];
					$codigoGrupo = $codigosCursoSeparate[1];

					$informeParticipante['porcentajeSuperado'] 	= $porcentajeSuperado;
					$cont++;					
				}
			}
		}
	}
}

ob_start();
include(PATH_ROOT . 'modulos/informe/vistas/template/informe_tareas.php');

$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
	//$html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	
	$html2pdf->Output('informe_tareas_pendientes_' . $informeParticipante['nif'] . '.pdf', 'D');
}
catch(HTML2PDF_exception $e) {echo $e;}

//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tareas');