
<div class="popupIntro">
	<div class='subtitle t_center'>
		<img src="imagenes/publicaciones_educativas/publicaciones-educativas.png" alt="" style="vertical-align:middle"/>
		<span data-translate-html="publicaciones.titulo">
			Publicaciones educativas
		</span>
	</div>
	<br/>
	<p class="t_center" data-translate-html="publicaciones.descripción">
		A continuaci&oacute;n se muestra un listado con los enlaces de revistas educativas
	</p>
	<br/>
</div>
