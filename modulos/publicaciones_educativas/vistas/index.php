<div class='caja_separada'>

	<?php require DIRNAME(__FILE__) . '/intro_publicaciones_educativas.php' ?>
		
	<div class="cabeceraLibrary">
		<div class="titleLibrary" data-translate-html="publicaciones.titulo">
			Publicaciones educativas
		</div>
		
		<div class="menuVista2" style="float:right;">
			<div class="vista1" >
				<a href="publicaciones_educativas/listado">
					<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.clasica" alt="vista clasica"/>
				</a>
			</div>
			<div class="vista2" >
				<a href="#" onclick="return false;">
					<img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.listado" alt="vista"/>
				</a>
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
	<div class="clear"></div>
	
	<div class="menuLibrary">
		<div class="estante">
			<span data-translate-html="publicaciones.revistas">
				Revistas
			</span>
		</div>
	</div>
	<div class="library">
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://rusc.uoc.edu/ojs/index.php/rusc/index' target='_blank'>RUSC (Revista de Universidad y Sociedad del Conocimiento)</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://dim.pangea.org/revistaDIM22/revistanew.htm' target='_blank'>DIM</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://intra.sav.us.es:8080/pixelbit/' target='_blank'>Pixel-Bit (Revista de Medios y Educación)</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://edutec.rediris.es/Revelec2/presentacion.html ' alt='' target='_blank'>Edutec (Revista Electrónica de Tecnología Educativa)</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://educacion2.com/' target='_blank'>Revista Educación 2.0</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://www.educaciontrespuntocero.com/' target='_blank'>Revista Educación 3.0</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://www.comunicacionypedagogia.com/cyp.html' target='_blank'>Revista Comunicación y Pedagogía </a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://www.uclm.es/varios/revistas/docenciaeinvestigacion/' alt='' target='_blank'>Docencia e Investigación</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://www.quadernsdigitals.net/' target='_blank'>Quaderns Digitals</a></div>
		<div class='book' style="background-image:url('imagenes/portada_default.png');font-size:0.9em;font-weight:bold;"><a style="text-align:center" href='http://www.aufop.com/aufop/home/' target='_blank'>Revista Universitaria de Formación del Profesorado</a></div>
	</div>
</div>