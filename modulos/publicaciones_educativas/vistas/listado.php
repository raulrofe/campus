<div class='caja_separada'>

	<?php require DIRNAME(__FILE__) . '/intro_publicaciones_educativas.php' ?>
		
	<div class="cabeceraLibrary listPublications" style="margin:0;border:1px solid #d5d5d5;border-radius:5px;">
		<div class="titleLibrary" data-translate-html="publicaciones.titulo">
			Publicaciones educativas
		</div>
		<div class="menuVista2" style="float:right;">
			<div class="vista3" >
				<a href="#">
					<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.clasica" alt="vista clasica"/>
				</a>
			</div>
			<div class="vista4" >
				<a href="publicaciones_educativas" onclick="return false;">
					<img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.listado" alt="vista"/>
				</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	<div id="revistaEducativaListado">
		<div><a href='http://rusc.uoc.edu/ojs/index.php/rusc/index' target='_blank'>RUSC (Revista de Universidad y Sociedad del Conocimiento)</a></div>
		<div><a href='http://dim.pangea.org/revistaDIM22/revistanew.htm' target='_blank'>DIM</a></div>
		<div><a href='http://intra.sav.us.es:8080/pixelbit/' target='_blank'>Pixel-Bit (Revista de Medios y Educación)</a></div>
		<div><a href='http://edutec.rediris.es/Revelec2/presentacion.html ' alt='' target='_blank'>Edutec (Revista Electrónica de Tecnología Educativa)</a></div>
		<div><a href='http://educacion2.com/' target='_blank'>Revista Educación 2.0</a></div>
		<div><a href='http://www.educaciontrespuntocero.com/' target='_blank'>Revista Educación 3.0</a></div>
		<div><a href='http://www.comunicacionypedagogia.com/cyp.html' target='_blank'>Revista Comunicación y Pedagogía </a></div>
		<div><a href='http://www.uclm.es/varios/revistas/docenciaeinvestigacion/' alt='' target='_blank'>Docencia e Investigación</a></div>
		<div><a href='http://www.quadernsdigitals.net/' target='_blank'>Quaderns Digitals</a></div>
		<div><a href='http://www.aufop.com/aufop/home/' target='_blank'>Revista Universitaria de Formación del Profesorado</a></div>
	</div>
</div>