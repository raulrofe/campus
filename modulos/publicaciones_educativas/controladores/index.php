<?php
$get = Peticion::obtenerGet();

if(isset($get['modo']) && $get['modo'] == 'listado')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listado');
}
else
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');
}