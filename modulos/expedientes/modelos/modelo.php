<?php
class ModeloExpedientes extends modeloExtend
{
	public function obtenerAlumnos($idCurso)
	{
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM alumnos AS al' .
		' LEFT JOIN matricula AS mt ON mt.idalumnos= al.idalumnos WHERE mt.idcurso = ' . $idCurso .
		' GROUP BY al.idalumnos ORDER BY nombrec DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerAlumnosActivos($idCurso)
	{
		$sql = 'SELECT al.idalumnos, CONCAT(al.apellidos, " ", al.nombre) AS nombrec' .
		' FROM alumnos AS al' .
		' LEFT JOIN matricula AS mt ON mt.idalumnos= al.idalumnos' .
		' WHERE mt.idcurso = ' . $idCurso .
		' AND al.borrado = 0' .
		' AND mt.borrado = 0' .
		' GROUP BY al.idalumnos' .
		' ORDER BY al.apellidos ASC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT al.idalumnos, al.nombre, al.apellidos, al.dni, al.f_nacimiento, al.telefono,' .
		' al.telefono_secundario, al.direccion, al.cp, al.poblacion, al.provincia, al.pais, al.email, ct.nombre_centro, al.foto,' .
		' mt.fecha, c.f_inicio, c.f_fin, c.titulo' .
		' FROM alumnos AS al' .
		' LEFT JOIN matricula AS mt ON mt.idcurso = ' . $idCurso .
		' LEFT JOIN curso AS c ON c.idcurso = mt.idcurso' .
		' LEFT JOIN centros AS ct ON ct.idcentros = al.idcentros' .
		' WHERE al.idalumnos = ' . $idAlumno .
		' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}