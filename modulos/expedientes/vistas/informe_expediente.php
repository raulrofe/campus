<style>

p, table, td{
	margin:0;
	padding:0;	
}
b{font-weight: bold;}

/*CLASES GENERALES */
.v_middle{vertical-align: middle;}
.t_center{text-align:center;}
.t_left{text-align: left;}
.t_right{text-align: right;}
.small{font-size: 12px;}
.fleft{flaot:left;}
.borderBottom{border-bottom:2px solid #AAA;}
.borderRight{border-right:2px solid #AAA;}
.encabezado{
	border:2px solid #AAA;
	width: 100%;
	height: 25px;
	background-color:#EFEFEF;
}

.encabezado .nBox{
	border-right:2px solid #AAA;
	float:left;
	width:25px;
	text-align:center;
	font-weight: bold;
}

.encabezado .titleBox{
	float:left;
	height: 25px;
	width: 752px;
	text-align: center;
	font-weight: bold;
}

.encabezado td{vertical-align: middle;}

.contenido{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	border-bottom:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenido tr td{padding:0;margin:0;border-bottom:2px solid #AAA}
.contenido td{padding:5px;}


.titleInform{
	font-size: 18px;
	font-weight: bold;
	text-align:center;
	margin-bottom:10px;
}

</style>

<!-- expediente del alumno seleccionado -->
<page>
	<br/>
	<div class="t_center"><img src="imagenes/informe/ase.jpg" /></div>
	
	<br/>
	
	<div class="titleInform" data-translate-html="evaluaciones.centro">
		EXPEDIENTE ALUMNO
	</div>
	
	<br/>
	
	<div id="entidadOrganizadora">
		<table cellpadding="0" cellspacing="0" class="encabezado">
			<tr>
				<td class="titleBox">
					<?php echo htmlentities(ucwords($alumno->nombre) . ' ' . ucwords($alumno->apellidos), ENT_QUOTES, 'utf-8');?>
				</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0" class="contenido">
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.dni">
					NIF
				</td>
				<td style="width:408px;"><?php echo $alumno->dni;?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.telefono">
					Tel&eacute;fono
				</td>
				<td style="width:408px;"><?php echo $alumno->telefono;?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.telefono2">
					Tel&eacute;fono secudario
				</td>
				<td style="width:408px;"><?php echo $alumno->telefono_secundario;?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.fnacimiento">
					Fecha de nacimiento
				</td>
				<td style="width:408px;"><?php echo Fecha::obtenerFechaFormateada($alumno->f_nacimiento, false);?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.direccion">
					Direcci&oacute;n
				</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->direccion, ENT_QUOTES, 'utf-8');?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.cp">
					C&oacute;digo Postal
				</td>
				<td style="width:408px;"><?php echo $alumno->cp;?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.localidad">
					Poblaci&oacute;n
				</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->poblacion, ENT_QUOTES, 'utf-8');?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.provincia">
					Provincia
				</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->provincia, ENT_QUOTES, 'utf-8');?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.pais">
					Pa&iacute;s
				</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->pais, ENT_QUOTES, 'utf-8');?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="usuario.email">
					Email
				</td>
				<td style="width:408px;"><?php echo $alumno->email;?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="evaluaciones.centro">
					Centro
				</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->nombre_centro, ENT_QUOTES, 'utf-8');?></td>
			</tr>	
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="general.curso">
					Curso
				s</td>
				<td style="width:408px;"><?php echo htmlentities($alumno->titulo, ENT_QUOTES, 'utf-8');?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="evaluaciones.fechainicio">
					Fecha inicio
				</td>
				<td style="width:408px;"><?php echo Fecha::obtenerFechaFormateada($alumno->f_inicio, false);?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="evaluaciones.fechafin">
					Fecha fin
				</td>
				<td style="width:408px;"><?php echo Fecha::obtenerFechaFormateada($alumno->f_fin, false);?></td>
			</tr>
			<tr>
				<td class="borderRight" style="width:300px;" data-translate-html="evaluaciones.fechamat">
					Fecha matriculacion
				</td>
				<td style="width:408px;"><?php echo Fecha::obtenerFechaFormateada($alumno->fecha, false);?></td>
			</tr>
		</table>
	</div>
</page>


