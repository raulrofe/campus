<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.expediente">
		EXPEDIENTES
	</span>
</div>

<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs.php'); ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso">
		
		<div class="tituloTabs">
			<div data-translate-html="evaluaciones.expedientes">
				Expedientes
			</div>
		</div>
		
		<div id="expedientes">
			<div class="headEvaluation" style="overflow:hidden">
				<div class="fleft">
					<form id="frm_expedientes" method="post" action="">
						<select name="idalumno" onchange='this.form.submit()'>
							
							<option value="" data-translate-html="evaluaciones.sel_alu">
								- Selecciona un alumno -
							</option>

							<?php while($alumno = $alumnos->fetch_object()):?>

								<option value="<?php echo $alumno->idalumnos?>">
									<?php echo Texto::textoPlano(ucwords($alumno->nombrec)); ?>
								</option>

							<?php endwhile;?>

						</select>
					</form>
				</div>
				<?php if(isset($alumnoExp) && $alumnoExp->num_rows == 1):?>
						<?php $alumno = $alumnoExp->fetch_object();?>
						
						<div class="fright">
							<div class="menuVista printEvaluation">
								<a href="expedientes/informe/<?php echo $alumno->idalumnos?>" target="_blank" rel="tooltip" title="informe completo del curso" data-translate-title="evaluaciones.informe">
									<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista" />
								</a>
							</div>
						</div>
				<?php endif; ?>
			</div>

			<div class="clear"></div>
			
			<?php if(isset($alumnoExp) && $alumnoExp->num_rows == 1):?>
				<div id="expediente_alumno">
					<div id="generalDataUser">
						<div class="expediente_alumno_foto">
							<img src="imagenes/fotos/<?php echo $alumno->foto?>" alt="" />
						</div>
						<div class="dataUser">
							<div>
								<span class="tagDataUser" data-translate-html="formulario.nombre">
									Nombre: 
								</span>
								<span>
									<?php echo Texto::textoPlano(ucwords($alumno->nombre)) . ' ' . Texto::textoPlano(ucwords($alumno->apellidos))?>
								</span>
							</div>
							<div>
								<span class="tagDataUser" data-translate-html="usuario.dni">
									NIF: 
								</span>
								<span><?php echo Texto::textoPlano($alumno->dni)?></span>
							</div>
							<div>
								<span class="tagDataUser" data-translate-html="usuario.telefono">
									Tel&eacute;fono: 
								</span>
								<span><?php echo Texto::textoPlano($alumno->telefono)?></span>
							</div>
							<div>
								<span class="tagDataUser" data-translate-html="usuario.fnacimiento">
									Fecha nacimiento: 
								</span>
								<span><?php if($alumno->f_nacimiento != '0000-00-00') echo date('d/m/Y', strtotime($alumno->f_nacimiento))?></span>
							</div>
							<div>
								<span class="tagDataUser" data-translate-html="usuario.email">
									E-mail: 
								</span>
								<span><?php echo Texto::textoPlano($alumno->email)?></span>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div id="AdressDataUser">
						<div>
							<span class="tagDataUser" data-translate-html="usuario.direccion">
								Direcci&oacute;n: 
							</span>
							<span><?php echo Texto::textoPlano($alumno->direccion)?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="usuario.cp">
								C&oacute;digo postal: 
							</span>
							<span><?php echo Texto::textoPlano($alumno->cp)?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="usuario.localidad">
								Poblaci&oacute;n: 
							</span>
							<span><?php echo Texto::textoPlano(ucfirst($alumno->poblacion))?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="usuario.provincia">
								Provincia: 
							</span>
							<span><?php echo Texto::textoPlano(ucfirst($alumno->provincia))?> (<?php echo Texto::textoPlano(ucfirst($alumno->pais))?>)</span>
						</div>																					
						<div class="clear"></div>
					</div>
					<div id="administrationDataUser">
						<div>
							<span class="tagDataUser" data-translate-html="evaluaciones.centro">
								Centro: 
							</span>
							<span><?php echo Texto::textoPlano($alumno->nombre_centro)?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="general.curso">
								Curso: 
							</span>
							<span><?php echo Texto::textoPlano($alumno->titulo)?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="evaluaciones.fechacurso">
								Fecha curso: 
							</span>
							<span><?php echo Texto::textoPlano(Fecha::obtenerFechaFormateada($alumno->f_inicio, false))?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="evaluaciones.fechafin">
								Fecha fin: 
							</span>
							<span><?php echo Texto::textoPlano(Fecha::obtenerFechaFormateada($alumno->f_fin, false))?></span>
						</div>
						<div>
							<span class="tagDataUser" data-translate-html="evaluaciones.fechamat">
								Fecha matriculaci&oacute;n: 
							</span>
							<span><?php echo Texto::textoPlano(Fecha::obtenerFechaFormateada($alumno->fecha, false))?></span>
						</div>																						
						<div class="clear"></div>
					</div>
				</div>
			<?php endif;?>
		</div>
	</div>
</div>