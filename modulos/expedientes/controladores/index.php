<?php
$objModelo = new ModeloExpedientes();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	if(isset($post['idalumno']) && is_numeric($post['idalumno']))
	{		
		$alumnoExp = $objModelo->obtenerUnAlumno($post['idalumno'], Usuario::getIdCurso());
	}
}

$get = Peticion::obtenerGet();

if(isset($get['idAlumno']) && is_numeric($get['idAlumno']))
{
	//Cargo la libreria para convertir html en pdf
	require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');
	
	//Cargo los modelos basicos
	$html2Pdf = new HTML2PDF();
	
	$alumnoExp = $objModelo->obtenerUnAlumno($get['idAlumno'], Usuario::getIdCurso());	
	
	if($alumnoExp->num_rows > 0)
	{
		$alumno = $alumnoExp->fetch_object();
		
		// Una vez terminado el proceso de creacion del array para generar el pdf nos ponemos a generarlo
		// Recuperacion del contenido HTML
		ob_start();
		include(PATH_ROOT . 'modulos/expedientes/vistas/informe_expediente.php');
		$content = ob_get_clean();
		
		// conversion HTML => PDF
		try
		{
			$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
			//$html2pdf->setModeDebug();
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML($content);
			
			$html2pdf->Output();
		}
		catch(HTML2PDF_exception $e) {echo $e;}
	}
}


if(!isset($get['idAlumno']))
{
	$alumnos = $objModelo->obtenerAlumnosActivos(Usuario::getIdCurso());
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');	
}

