$("#tablonAnuncioAulaNuevoMensaje").validate({
	errorElement: "div",
	messages: {
		titulo: {
			required: 'Introduce un t&iacute;tulo'
		},
		descripcion: {
			required: 'Introduce una descripci&oacute;n'
		}
	},
	rules: {
		titulo : {
			required  : true
		},
		descripcion: {
			required: true
		}
	}
});
