<?php 
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$mi_anuncio = new TablonAnuncio();

$get = Peticion::obtenerGet();
extract(Peticion::obtenerPost());

if(isset($get['idanuncio']) && is_numeric($get['idanuncio']))
{
	$mi_anuncio->set_anuncio();
	
	$registros_anuncio = $mi_anuncio->buscar_anuncio();
	if(isset($registros_anuncio))
	{
		if(isset($get['opcion']) && $get['opcion'] = 'eliminar_adjunto')
		{
			if($mi_anuncio->eliminarAdjunto($get['idanuncio']))
			{
				if(file_exists(PATH_ROOT . $registros_anuncio['archivo_tablon']))
				{
					chmod(PATH_ROOT . $registros_anuncio['archivo_tablon'], 0777);
					unlink(PATH_ROOT . $registros_anuncio['archivo_tablon']);
				}
				
				Url::redirect('aula/tablon_anuncio');
			}
		}
		else 
		{
			if(!empty($titulo) and ($descripcion)){
				if($mi_anuncio->modificar_anuncio($titulo,$descripcion))
				{
					Alerta::guardarMensajeInfo('anuncioactualizado','Anuncio actualizado');
					Url::redirect('aula/tablon_anuncio');	
				}
			}
		}
		
		$registros_anuncio = $mi_anuncio->buscar_anuncio();
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'anuncios');
	}
}