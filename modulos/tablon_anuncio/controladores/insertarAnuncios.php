<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

$mi_anuncio = new TablonAnuncio();
$obj = new modeloExtend();

extract(Peticion::obtenerPost());

$mi_anuncio->buscar_staff();

if(!empty($titulo) and ($descripcion)){
	if($mi_anuncio->insertar_anuncio($titulo,$descripcion))
	{
		Alerta::guardarMensajeInfo('anuncioinsertado','Anuncio insertado');
		Url::redirect('aula/tablon_anuncio');	
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'anuncios');
