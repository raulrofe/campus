<?php
$get = Peticion::obtenerGet();

require 'configMimes.php';

$idusuario = Usuario::getIdUser(true);

$modeloTablonAnuncio = new TablonAnuncio();

/*
$objUsuarios = new Usuarios();
$rowUsuario = $objUsuarios->buscar_usuario_curso(Usuario::getIdUser(), Usuario::getIdCurso());
if($rowUsuario->num_rows == 1)
{*/
	$modeloTablonAnuncio->set_anuncio($get['idanuncio']);
	$rowAdjunto = $modeloTablonAnuncio->buscar_anuncio();
	//$rowAdjunto = $modeloTablonAnuncio->buscar_anuncio(Usuario::getIdUser(true), Usuario::getIdCurso());
	if(!empty($rowAdjunto))
	{
		if(isset($rowAdjunto['idcurso']) && $rowAdjunto['idcurso'] == Usuario::getIdCurso() && !empty($rowAdjunto['archivo_tablon']))
		{
			$extension = Fichero::obtenerExtension($rowAdjunto['archivo_tablon']);
			if(file_exists($rowAdjunto['archivo_tablon']) && isset($mimes[$extension]))
			{
				header("Content-disposition: attachment; filename=" . $rowAdjunto['nombre_archivo']);
				header("Content-type: application/octet-stream");
				readfile($rowAdjunto['archivo_tablon']);
			}
			else
			{
				Url::lanzar404();
			}
		}
		else
		{
			Url::lanzar404();
		}
	}
	else
	{
		Url::lanzar404();
	}
/*
}
else
{
	Url::lanzar404();
}
*/