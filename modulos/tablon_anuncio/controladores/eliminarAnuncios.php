<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$mi_anuncio = new TablonAnuncio();

$mi_anuncio->set_anuncio();
if($mi_anuncio->eliminar_anuncio())
{
	Alerta::guardarMensajeInfo('anuncioeliminado','Anuncio eliminado');
}
else
{
	Alerta::guardarMensajeInfo('anuncionoeliminar','El anuncio no se ha podido eliminar');
}

Url::redirect('aula/tablon_anuncio');

