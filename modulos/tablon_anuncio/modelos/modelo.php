<?php 
class TablonAnuncio extends modeloExtend{
	
	private $idstaff;
	private $idanuncio;
	
	//Archivos pertidos para adjuntar
	private $extensionesAdjunto = array(
		'jpg',
		'jpeg',
		'png',
		'doc', 
		'docx', 
		'xls', 
		'xlsx', 
		'pdf',
		'ppt',
		'pptx',
		'swf',
		'bmp',
		'tiff',
		'tif',
		'txt',
		'text',
		'html',
		'htm',
		'css',
		'zip',
		'rar'
	);
	
	public function __construct()
	{
		parent::__construct();
	}

	public function set_anuncio()
	{
		$get = Peticion::obtenerGet();
		
		if(isset($get['idanuncio']) && is_numeric( $get['idanuncio']))
		{
			$this->idanuncio = $get['idanuncio'];
		}
		else
		{
			exit;
			Url::lanzar404();
		}
	}
	
	public function insertar_anuncio($titulo,$descripcion)
	{
		$fecha = date("Y-m-d H:i:s");
		$sql = "INSERT into anuncio (titulo_anuncio,descripcion_anuncio,fecha,idstaff)
		VALUES ('$titulo','$descripcion','$fecha','$this->idstaff')";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{	
			if ($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					$extension =  Texto::obtenerExtension($_FILES['archivotablon']['name']);	
					$directorio = 'archivos/tablon_anuncios/';
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$id.'.'.$extension) or die("Ocurrio un problema al intentar subir el archivo.");
					if(file_exists($directorio.$id.'.'.$extension))
					{
						$archivoTablon = $directorio.$id.'.'.$extension;
						$nombreArchivo = Texto::clearFilename($_FILES['archivotablon']['name']);
						$sql = "UPDATE anuncio SET archivo_tablon = '$archivoTablon', nombre_archivo = '$nombreArchivo' where idanuncio = ".$id;
						$restultado = $this->consultaSql($sql);
					}
					else echo "Error al mover el archivo";
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}
			}
			//else echo "Error al subir el archivo";			
		}
		return $resultado;
	}
	
	public function eliminarAdjunto($idanuncio)
	{
		$sql = 'UPDATE anuncio SET archivo_tablon = "" WHERE idanuncio = ' . $idanuncio;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function modificar_anuncio($titulo,$descripcion){
		$fecha = date("Y-m-d");
		$sql = "UPDATE anuncio SET titulo_anuncio = '$titulo' , descripcion_anuncio = '$descripcion'
		where idanuncio = ".$this->idanuncio;
		//echo $sql;
		$resultado2 = $this->consultaSql($sql);
		
		$sql = "SELECT * from anuncio where idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		$registroArchivo = mysqli_fetch_assoc($resultado);
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{
			if($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					if($registroArchivo['archivo_tablon'] != NULL && file_exists($registroArchivo['archivo_tablon'] ))
					{
						chmod($registroArchivo['archivo_tablon'] , 0777);
						unlink($registroArchivo['archivo_tablon']);	
					}
					
					$extension = Texto::obtenerExtension($_FILES['archivotablon']['name']);
					$directorio = 'archivos/tablon_anuncios/';
					$nombreArchivoFisico = $this->idanuncio.'.'.$extension;
					$archivoTablon = $directorio.$nombreArchivoFisico;
					
					$nombreArchivo = Texto::clearFilename($_FILES['archivotablon']['name']);
					
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$nombreArchivoFisico) or die("Ocurrio un problema al intentar subir el archivo.");	
					
					$sql = "UPDATE anuncio SET archivo_tablon = '$archivoTablon', nombre_archivo = '$nombreArchivo' where idanuncio = ".$this->idanuncio;
					$this->consultaSql($sql);
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}				
			}
		}
		
		return $resultado2;
	}
	
	public function ver_anuncios($limitIni = null, $limitEnd = null){
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		$sql = "SELECT * from rrhh RH, staff S, anuncio A
		where S.idcurso = ".$_SESSION['idcurso']." 
		AND A.borrado = 0
		and S.idstaff = A.idstaff 
		and S.idrrhh = RH.idrrhh
		ORDER BY A.idanuncio DESC" . $addQuery;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function buscar_anuncio(){
	//public function buscar_anuncio($idusuario = NULL,  $idcurso = NULL){
		/*$addQuery = null;
		if(isset($idusuario, $idcurso))
		{
			$addQuery = ' AND idstaff = (SELECT idstaff FROM staff WHERE CONCAT("t_", idrrhh) = "' . $idusuario . '" AND idcurso = ' . $idcurso . ' LIMIT 1)';
			//$addQuery = ' OR idstaff = (SELECT idstaff FROM staff WHERE CONCAT("t_", idrrhh) = "' . $idusuario . '" AND idcurso = ' . $idcurso . ' LIMIT 1))';
		}*/
		
		$sql = "SELECT * from anuncio a
		LEFT JOIN staff s ON s.idstaff = a.idstaff AND s.idcurso = " . Usuario::getIdCurso() .
		" where a.borrado = 0 AND idanuncio = ".$this->idanuncio ;// . $addQuery;
		//echo $sql;
		//$resultado = mysqli_query($this->con,$sql);
		$resultado = $this->consultaSql($sql);
		return mysqli_fetch_assoc($resultado);
	}
	
	public function eliminar_anuncio(){
		$sql="SELECT * from anuncio where borrado = 0 AND idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		$registroArchivo = mysqli_fetch_assoc($resultado);
		
		
		if($registroArchivo['archivo_tablon'] != NULL && file_exists($registroArchivo['archivo_tablon']))
		{
			chmod($registroArchivo['archivo_tablon'], 0777);
			unlink($registroArchivo['archivo_tablon']);
		}
		
		$sql = "UPDATE anuncio SET borrado = 1 where idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Obtenemos el id del staff (relacion curso - tutor)
	public function buscar_staff(){
		$sql = "SELECT * from staff where idcurso = ".$_SESSION['idcurso']." 
		and idrrhh = ".$_SESSION['idusuario'];
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->idstaff = $f['idstaff'];
		return $this->idstaff;
	}
	
	public function numero_anuncios()
	{
		$sql = "SELECT * from anuncio A
		where A.fecha > (SELECT MAX(LLA.fecha_entrada) 
		from logs_acceso LLA, matricula MM 
		where LLA.idlugar = 4
		AND A.borrado = 0
		and MM.idalumnos = ".$_SESSION['idusuario']." 
		and MM.idcurso = ".$_SESSION['idcurso']." 
		and MM.idmatricula = LLA.idmatricula)";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}
?>