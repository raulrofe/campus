<div>
	<div id="anuncios">
	
		<?php if($_SESSION['perfil'] != 'alumno'){require_once mvc::obtenerRutaVista(dirname(__FILE__), 'menu_tutores');} ?>
		<div class='subtitleModulo'>
			<span data-translate-html="tablon_anuncios.anuncios">
				Anuncios
			</span>
		</div>
		<div class='clear'></div>
		<div>
			<?php
				if(mysqli_num_rows($registros_anuncios) > 0)
				{ 
					while($f = mysqli_fetch_assoc($registros_anuncios))
					{
						$fecha = Fecha::obtenerFechaFormateada($f['fecha']);				
						echo "<div class='elemento'>
								 <div class='elementoTitulo'>".Texto::textoPlano($f['titulo_anuncio'])."</div>
								 <div class='elementoContenido'>" . $f['descripcion_anuncio'] . "</div>";
								 
							if(!empty($f['archivo_tablon']))
							{
								echo "<div class='elementoContenido'><img src='imagenes/correos/clip.png' alt='' /><a href='tablon_anuncio/forzar_descarga/" . $f['idanuncio'] ."' target='_blank'>Archivo adjunto</a></div>";
							}
							echo "<div class='elementoPie'>
								 	<div class='elementoFecha'>De: ".Texto::textoPlano($f['nombrec'])." (".$fecha.")</div>	";	
		
									if($_SESSION['perfil'] == 'tutor' or $_SESSION['perfil'] == 'coordinador')
									{ 
										echo "<ul class='elementoListOptions fright hide'>
											   <li><a data-translate-title='foro.editarmensaje' title='Editar este mensaje' href='aula/tablon_anuncio/editar/".$f['idanuncio']."' rel='tooltip'><img src='imagenes/options/edit.png' alt='' /></a></li>
											   <li><a data-translate-title='foro.borrarmensaje' title='Borrar mensaje' " . Alerta::alertConfirmOnClick("borrarmensajetablon", "¿Deseas borrar el mensaje del tablón de anuncios?", "aula/tablon_anuncio/eliminar/".$f['idanuncio']) . " href='#' rel='tooltip'><img src='imagenes/options/delete.png' alt=''/></a></li>
										      </ul>";
									}
						   echo "</div>";
						echo "<div class='clear'></div></div>";
					}
		
					echo Paginado::crear($numPagina, $maxPaging, 'aula/tablon_anuncio/pagina/');
				}
				else 
				{
					echo "<div class='burbuja t_center' data-translate-html='tablon_anuncios.vacio'>";
						echo "El tabl&oacute;n de anuncios del aula se encuentra vac&iacute;o en estos momentos";
					echo "</div>";
				}
			?>
		</div>
	</div>
</div>

