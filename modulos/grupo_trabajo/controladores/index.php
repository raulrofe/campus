<?php
    // Specify WebEx site and port
    $XML_SITE="campusaulas.webex.com";
    $XML_PORT="80";

    // Set calling user information
    $d["UID"] = "rrodriguez"; // WebEx username
    $d["PWD"] = "1Q2w3E4rtY"; // WebEx password
    $d["SID"] = "954902"; //Demo Site SiteID
    $d["PID"] = "yGMZ-HeiTf7-tYP7N2m3Ew"; //Demo Site PartnerID
    $d["fechaSesion"] = date('d/m/YY');

    // Build XML request document
    $d["XML"]="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $d["XML"].="<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
        $d["XML"].="xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\">";
            $d["XML"].="<header>";
                $d["XML"].="<securityContext>";
                    $d["XML"].="<webExID>{$d["UID"]}</webExID>";
                    $d["XML"].="<password>{$d["PWD"]}</password>";
                    $d["XML"].="<siteID>{$d["SID"]}</siteID>";
                    $d["XML"].="<partnerID>{$d["PID"]}</partnerID>";
                    $d["XML"].="<email>raulrodriguez@fundacionaulasmart.org</email>";
                $d["XML"].="</securityContext>";
                $d["XML"].="</header>";
                $d["XML"].="<body>";
                    $d["XML"].="<bodyContent xsi:type=\"java:com.webex.service.binding.training.LstsummaryTrainingSession\">";
                        $d["XML"].="<listControl>";
                            $d["XML"].="<startFrom>1</startFrom>";
                            $d["XML"].="<maximumNum>5</maximumNum>";
                            $d["XML"].="<listMethod>OR</listMethod>";
                        $d["XML"].="</listControl>";
                        $d["XML"].="<order>";
                            $d["XML"].="<orderBy>STARTTIME</orderBy>";
                            $d["XML"].="<orderAD>DESC</orderAD>";
                        $d["XML"].="</order>";
                        $d["XML"].="<dateScope>";
                            $d["XML"].="<startDateStart>03/28/2016 00:00:00</startDateStart>";
                            $d["XML"].="<timeZoneID>45</timeZoneID>";
                        $d["XML"].="</dateScope>";
                    $d["XML"].="</bodyContent>";
                 $d["XML"].="</body>";
        $d["XML"].="</serv:message>";

    $URL = "https://{$XML_SITE}/WBXService/preview/XMLService";
    $Result = postIt($d,$URL,$XML_PORT);

    $p = xml_parser_create();
    xml_parse_into_struct($p, $Result, $vals, $index);
    xml_parser_free($p);

    echo '<pre>';
     print_r($vals);
    echo '</pre>';
    exit;

    $sesiones = array();

    foreach($index['TRAIN:SESSIONKEY'] as $key => $elemento){
        $sesiones[$key]['idSesion'] = $vals[$elemento]['value'];
    }

    foreach($index['TRAIN:CONFNAME'] as $key => $elemento){
        $sesiones[$key]['nombreSesion'] = $vals[$elemento]['value'];
    }

    foreach($index['TRAIN:HOSTWEBEXID'] as $key => $elemento){
        $sesiones[$key]['hostSesion'] = $vals[$elemento]['value'];
    }

    foreach($index['TRAIN:STARTDATE'] as $key => $elemento){
        $sesiones[$key]['fechaInicio'] = $vals[$elemento]['value'];
    }


    foreach($index['TRAIN:DURATION'] as $key => $elemento){
        $sesiones[$key]['duracion'] = $vals[$elemento]['value'];
    }

	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'listado');

    //
    //  postIt()
    //
    //  POSTs the XML action document and calling user variables
    //  to the specified WebEx XML Server and receives an XML response document
    //
    function postIt($DataStream, $URL, $Port)
    {
        //  Strip http:// from the URL if present
        $URL = ereg_replace("^https://", "", $URL);

        //  Separate into Host and URI
        $Host = substr($URL, 0, strpos($URL, "/"));
        $URI = strstr($URL, "/");

        //  Form the request body
        $reqBody = "";
        while (list($key, $val) = each($DataStream)) {
            if ($reqBody) $reqBody.= "&";
            $reqBody.= $key."=".urlencode($val);
        }
        $ContentLength = strlen($reqBody);

        $xml = $DataStream['XML'];

        //  Generate the request header
        global $Debug_Mode;
        $Debug_Mode = 1;
        $URL = $Host;
        $fp = fsockopen($URL,80,$errno,$errstr);
        $Post =  "POST /WBXService/XMLService HTTPS/1.0\n";
        $Post .= "Host: $URL\n";
        $Post .= "Content-Type: application/xml\n";
        $Post .= "Content-Length: ".strlen($xml)."\n\n";
        $Post .= "$xml\n";

        // if($Debug_Mode){
        //   echo "<hr>XML Sent:<br><textarea cols=\"50\" rows=\"25\">".htmlspecialchars($xml)."</textarea><hr>";
        // }
        if($fp){
            fwrite($fp,$Post);
            $response = "";
            while (!feof($fp)) {
                $response .= fgets($fp, 1024);
            }
            if($Debug_Mode){
                $xmlResponse = explode('Connection: close', $response);
                $response = trim($xmlResponse[1]);
            }

            return $response;
        }
        else{
            echo "$errstr ($errno)<br />\n";
            return false;
        }
    }

