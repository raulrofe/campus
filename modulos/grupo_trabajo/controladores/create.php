<?php

if(Peticion::isPost())
{
    $post = Peticion::obtenerPost();

    list($dia, $mes, $año) = explode('/', $post['fechaSesion;']);
    $fechaInicio = $mes . '/' . $dia . '/' . $año . ' ' . $post['hora_inicio'] . ':' . $post['minuto_inicio'];

    // Specify WebEx site and port
    $XML_SITE="campusaulas.webex.com";
    $XML_PORT="80";

    // Set calling user information
    $d["UID"] = "rrodriguez"; // WebEx username
    $d["PWD"] = "1Q2w3E4rtY"; // WebEx password
    $d["SID"] = "954902"; //Demo Site SiteID
    $d["PID"] = "yGMZ-HeiTf7-tYP7N2m3Ew"; //Demo Site PartnerID

    // Build XML request document
    $d["XML"]="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
        $d["XML"].="<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
        $d["XML"].="xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\">";
            $d["XML"].="<header>";
                $d["XML"].="<securityContext>";
                    $d["XML"].="<webExID>{$d["UID"]}</webExID>";
                    $d["XML"].="<password>{$d["PWD"]}</password>";
                    $d["XML"].="<siteID>{$d["SID"]}</siteID>";
                    $d["XML"].="<partnerID>{$d["PID"]}</partnerID>";
                    $d["XML"].="<email>raulrodriguez@fundacionaulasmart.org</email>";
                $d["XML"].="</securityContext>";
                $d["XML"].="</header>";
                $d["XML"].="<body>";
                    $d["XML"].="<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">";
                        $d["XML"].="<accessControl>";
                            $d["XML"].="<listing>PUBLIC</listing>";
                            $d["XML"].="<sessionPassword>111111</sessionPassword>";
                            $d["XML"].="<listMethod>OR</listMethod>";
                        $d["XML"].="</accessControl>";
                        $d["XML"].="<schedule>";
                            $d["XML"].="<startDate>" . $fechaInicio . "</startDate>";
                            $d["XML"].="<timeZone>GMT +2:00, Madrid</timeZone>";
                            $d["XML"].="<duration>60</duration>";
                            $d["XML"].="<timeZoneID>60</timeZoneID>";
                            $d["XML"].="<openTime>60</openTime>";
                        $d["XML"].="</schedule>";
                        $d["XML"].="<metaData>";
                            $d["XML"].="<confName>" . $post['nombreSesion'] . "</confName>";
                            $d["XML"].="<agenda>Agenda sesion</agenda>";
                            $d["XML"].="<description>Seseón de curso</description>";
                            $d["XML"].="<greeting>Bienvenido/a a la sesión de entrenamiento programada por AULA SMART</greeting>";
                            $d["XML"].="<location>Online</location>";
                            $d["XML"].="<invitation>Invitación</invitation>";
                        $d["XML"].="</metaData>";
                        $d["XML"].="<enableOptions>";
                            $d["XML"].="<attendeeList>true</attendeeList>";
                            $d["XML"].="<javaClient>false</javaClient>";
                            $d["XML"].="<nativeClient>true</nativeClient>";
                            $d["XML"].="<chat>true</chat>";
                            $d["XML"].="<poll>true</poll>";
                            $d["XML"].="<audioVideo>true</audioVideo>";
                            $d["XML"].="<fileShare>true</fileShare>";
                            $d["XML"].="<presentation>false</presentation>";
                            $d["XML"].="<applicationShare>true</applicationShare>";
                            $d["XML"].="<desktopShare>true</desktopShare>";
                            $d["XML"].="<webTour>true</webTour>";
                            $d["XML"].="<trainingSessionRecord>true</trainingSessionRecord>";
                            $d["XML"].="<annotation>true</annotation>";
                            $d["XML"].="<importDocument>true</importDocument>";
                            $d["XML"].="<saveDocument>true</saveDocument>";
                            $d["XML"].="<printDocument>true</printDocument>";
                            $d["XML"].="<pointer>true</pointer>";
                            $d["XML"].="<switchPage>true</switchPage>";
                            $d["XML"].="<fullScreen>true</fullScreen>";
                            $d["XML"].="<thumbnail>true</thumbnail>";
                            $d["XML"].="<zoom>true</zoom>";
                            $d["XML"].="<copyPage>true</copyPage>";
                            $d["XML"].="<rcAppShare>true</rcAppShare>";
                            $d["XML"].="<rcDesktopShare>true</rcDesktopShare>";
                            $d["XML"].="<rcWebTour>true</rcWebTour>";
                            $d["XML"].="<attendeeRecordTrainingSession>true</attendeeRecordTrainingSession>";
                            $d["XML"].="<voip>false</voip>";
                            $d["XML"].="<faxIntoTrainingSession>false</faxIntoTrainingSession>";
                            $d["XML"].="<autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
                        $d["XML"].="</enableOptions>";
                        $d["XML"].="<telephony>";
                            $d["XML"].="<telephonySupport>NONE</telephonySupport>";
                            $d["XML"].="<numPhoneLines>4</numPhoneLines>";
                            $d["XML"].="<extTelephonyURL>String</extTelephonyURL>";
                            $d["XML"].="<extTelephonyDescription>String</extTelephonyDescription>";
                            $d["XML"].="<enableTSP>false</enableTSP>";
                            $d["XML"].="<tspAccountIndex>1</tspAccountIndex>";
                        $d["XML"].="</telephony>";
                        $d["XML"].="<tracking>";
                        $d["XML"].="</tracking>";
                        $d["XML"].="<repeat>";
                            $d["XML"].="<repeatType>RECURRING_SINGLE</repeatType>";
                            $d["XML"].="<dayInWeek><day>SUNDAY</day></dayInWeek>";
                            $d["XML"].="<endAfter>5</endAfter>";
                            $d["XML"].="<occurenceType>WEEKLY</occurenceType>";
                            $d["XML"].="<interval>5</interval>";
                            $d["XML"].="<dayInMonth>1</dayInMonth>";
                            $d["XML"].="<weekInMonth>1</weekInMonth>";
                            $d["XML"].="<monthInYear>1</monthInYear>";
                            $d["XML"].="<dayInYear>1</dayInYear>";
                        $d["XML"].="</repeat>";
                        $d["XML"].="<remind>";
                            $d["XML"].="<enableReminder>false</enableReminder>";
                            $d["XML"].="<emails><email></email></emails>";
                            $d["XML"].="<sendEmail>false</sendEmail>";
                            $d["XML"].="<mobile>String</mobile>";
                            $d["XML"].="<sendMobile>false</sendMobile>";
                            $d["XML"].="<daysAhead>1</daysAhead>";
                            $d["XML"].="<hoursAhead>1</hoursAhead>";
                            $d["XML"].="<minutesAhead>1</minutesAhead>";
                        $d["XML"].="</remind>";
                        $d["XML"].="<presenters>";
                        	$d["XML"].="<participants>";
                        		$d["XML"].="<participant>";
                        			$d["XML"].="<person>";
                        				$d["XML"].="<name>Raul Rodriguez</name>";
                        				$d["XML"].="<email>raulrodriguez@fundacionaulasmart.org</email>";
                        				$d["XML"].="<type>MEMBER</type>";
                        			$d["XML"].="</person>";
                        			$d["XML"].="<role>HOST</role>";
                        		$d["XML"].="</participant>";
                        	$d["XML"].="</participants>";
                        $d["XML"].="</presenters>";
                        $d["XML"].="<attendees>";
                        	$d["XML"].="<participants>";
                        		$d["XML"].="<participant>";
                        			$d["XML"].="<person>";
                        				$d["XML"].="<name>Aurora Alarcon</name>";
                        				$d["XML"].="<email>auroraalarcon@fundacionaulasmart.org</email>";
                        				$d["XML"].="<type>MEMBER</type>";
                        			$d["XML"].="</person>";
                        			$d["XML"].="<role>HOST</role>";
                        		$d["XML"].="</participant>";
                                $d["XML"].="<participant>";
                                    $d["XML"].="<person>";
                                        $d["XML"].="<name>Laura Jimenez</name>";
                                        $d["XML"].="<email>laurajimenez@ibericasur.com</email>";
                                        $d["XML"].="<type>MEMBER</type>";
                                    $d["XML"].="</person>";
                                    $d["XML"].="<role>HOST</role>";
                                $d["XML"].="</participant>";
                                $d["XML"].="<participant>";
                                    $d["XML"].="<person>";
                                        $d["XML"].="<name>Antonio Rodriguez</name>";
                                        $d["XML"].="<email>raulrodriguez@ibericasur.com</email>";
                                        $d["XML"].="<type>MEMBER</type>";
                                    $d["XML"].="</person>";
                                    $d["XML"].="<role>HOST</role>";
                                $d["XML"].="</participant>";
                        	$d["XML"].="</participants>";
                        $d["XML"].="</attendees>";
                        $d["XML"].="<attendeeOptions>";
                            $d["XML"].="<request>false</request>";
                            $d["XML"].="<registration>false</registration>";
                            $d["XML"].="<auto>true</auto>";
                            $d["XML"].="<registrationPWD>false</registrationPWD>";
                            $d["XML"].="<maxRegistrations>25</maxRegistrations>";
                            $d["XML"].="<registrationCloseDate>04/10/2016 00:00:00</registrationCloseDate>";
                            $d["XML"].="<emailInvitations>true</emailInvitations>";
                        $d["XML"].="</attendeeOptions>";
                    $d["XML"].="</bodyContent>";
                 $d["XML"].="</body>";
        $d["XML"].="</serv:message>";

        $URL = "https://{$XML_SITE}/WBXService/preview/XMLService";
        $Result = postIt($d,$URL,$XML_PORT);

        exit('despues de respuesta');

        echo '<pre>';
            print_r(htmlspecialchars($Result));
        echo '</pre>';
        exit;

        Url::redirect('aula/grupo-trabajo');

    //
    //  postIt()
    //
    //  POSTs the XML action document and calling user variables
    //  to the specified WebEx XML Server and receives an XML response document
    //
    function postIt($DataStream, $URL, $Port)
    {
        //  Strip http:// from the URL if present
        $URL = ereg_replace("^https://", "", $URL);

        //  Separate into Host and URI
        $Host = substr($URL, 0, strpos($URL, "/"));
        $URI = strstr($URL, "/");

        //  Form the request body
        $reqBody = "";
        while (list($key, $val) = each($DataStream)) {
            if ($reqBody) $reqBody.= "&";
            $reqBody.= $key."=".urlencode($val);
        }
        $ContentLength = strlen($reqBody);

        $xml = $DataStream['XML'];

        //  Generate the request header
        global $Debug_Mode;
        $Debug_Mode = 1;
        $URL = $Host;
        $fp = fsockopen($URL,80,$errno,$errstr);
        $Post =  "POST /WBXService/XMLService HTTPS/1.0\n";
        $Post .= "Host: $URL\n";
        $Post .= "Content-Type: application/xml\n";
        $Post .= "Content-Length: ".strlen($xml)."\n\n";
        $Post .= "$xml\n";

        // if($Debug_Mode){
        //   echo "<hr>XML Sent:<br><textarea cols=\"50\" rows=\"25\">".htmlspecialchars($xml)."</textarea><hr>";
        // }
        if($fp){
            fwrite($fp,$Post);
            $response = "";
            while (!feof($fp)) {
                $response .= fgets($fp, 1024);
            }
            if($Debug_Mode){
                $xmlResponse = explode('Connection: close', $response);
                $response = trim($xmlResponse[1]);
            }

            return $response;
        }
        else{
            echo "$errstr ($errno)<br />\n";
            return false;
        }
    }
}


mvc::cargarModuloSoporte('tutorias');
$mi_tutoria = new Tutoria();
$alumnos = $mi_tutoria->obtenerAlumnosCurso(Usuario::getIdCurso());
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'create');
