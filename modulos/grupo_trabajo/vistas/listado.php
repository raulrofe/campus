<div class='caja_separada'>
	<?php if(!Usuario::compareProfile(array('supervisor', 'alumno'))): ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/archivador/archivador.png" alt="" style="vertical-align:middle;"/>
				<span>GRUPOS DE TRABAJOS / VIDEOCONFERENCIAS</span>
			</div>
			<br/>
	 		<p class='t_center'>En este espacio podra disfrutar de videoconferencias y trabajos para compartir en grupo.</p><br/>
		</div>
		<div class='menu_interno'>
			<span class='elemento_menuinterno'><a href='aula/grupo-trabajo/crearsesion'>Crear Sesiones de videoconferencias</a></span>
		</div>
	<?php else: ?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/archivador/archivador.png" alt="" style="vertical-align:middle;"/>
				<span>GRUPOS DE TRABAJOS / VIDEOCONFERENCIAS</span>
			</div>
			<br/>
	 		<p class='t_center'>En este espacio podra disfrutar de videoconferencias y trabajos para compartir en grupo.</p><br/>
		</div>
	<?php endif;?>
</div>

<div class='caja_separada'>

	<?php if(!empty($sesiones)): ?>

		<p class="t_center">Sesiones disponibles</p><br/><br/>

		<table>
			<tr>
				<td style="padding: 10px 20px;">Sesi&oacute;n videoconferencia</td>
				<td style="padding: 10px 20px" class="t_center">Fecha Inicio</td>
				<td style="padding: 10px 20px" class="t_center">Duraci&oacute;n</td>
				<td style="padding: 10px 20px" class="t_center">Estado</td>
			</tr>
			<?php foreach($sesiones as $sesion): ?>
				<tr>
					<?php if($sesion['hostSesion'] == 'rrodriguez'): ?>
						<td style="padding: 10px 20px">
							<?php if(date("m/d/Y H:i:s") < $sesion['fechaInicio']): ?>
								<a href="aula/grupo-trabajo/sesion/<?php echo $sesion['idSesion'] ?>"><?php echo $sesion['nombreSesion'] ?></a>
							<?php else: ?>
								<?php echo $sesion['nombreSesion'] ?>
							<?php endif; ?>
						</td>
						<td style="padding: 10px 20px" class="t_center"><?php echo date("d/m/Y H:i:s", strtotime($sesion['fechaInicio'])) ?></td>
						<td style="padding: 10px 20px" class="t_center"><?php echo $sesion['duracion'] ?> minutos</td>
						<?php if(date("m/d/Y H:i:s") < $sesion['fechaInicio']): ?>
							<td class="t_center">En propreso</td>
						<?php else: ?>
							<td class="t_center">Finalizada</td>
						<?php endif; ?>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</table>

	<?php endif; ?>
</div>
