<div id="frm-grupo-trabajo" <div id="tutoria" class='caja_frm'>
	<div class='subtitleModulo'><span>Nueva tutor&iacute;a</span></div>
	<div class='caja_frm'>
		<br/>
		<form id="frmInsertTrainingSession" name='' method='post' action=''>
			<div class='filafrm'>
				<div class='etiquetafrm'>Nombre Sesi&oacute;n</div>
				<div class='campofrm'>
					<input type='text' name='nombreSesion' value=""/>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Fecha Sesi&oacute;n</div>
				<div class='campofrm'>
					<input type='text' name='fechaSesion' value="dd/mm/YYYY"/>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Hora Inicio:</div>
				<div class='campofrm'>
					<select name='hora_inicio'><?php $mi_tutoria->horas();?></select>
					<span> : </span>
					<select name='minuto_inicio'><?php $mi_tutoria->minutos();?></select>
				</div>
				<div class='clear'></div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm'>Hora Fin:</div>
				<div class='campofrm'>
					<select name='hora_fin'><?php $mi_tutoria->horas();?></select>
					<span> : </span>
					<select name='minuto_fin'><?php $mi_tutoria->minutos();?></select>
				</div>
				<div class='clear'></div>
			</div>
			<div id="frmInsertTutoriaAlumnos" class='filafrm'>
				<div class='etiquetafrm'>Participantes</div>
				<div class='campofrm'>
					<?php if($alumnos->num_rows > 0):?>
						<?php while($alumno = $alumnos->fetch_object()):?>
							<input type="checkbox" value="<?php echo $alumno->idalumnos;?>"><?php echo Texto::textoPlano($alumno->nombrec);?><br/>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
			<div><input type='submit' value='Guardar' class="width100"/></div>
		</form>
	</div>
</div>