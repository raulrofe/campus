<?php
class ModeloChat extends modeloExtend
{
	public function nuevoMensaje($idUsuario, $idCurso, $contenido, $destinatario)
	{
		if(isset($destinatario))
		{
			$destinatario = '"' . $destinatario . '"';
		}
		else
		{
			$destinatario = 'NULL';
		}
		
		$sql = 'INSERT INTO chat (idusuario, mensaje, idcurso, destinatario) VALUES ("' . $idUsuario . '", "' . $contenido .
				'", ' . $idCurso . ', ' . $destinatario . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajes($idUsuario, $idCurso, $idUltimoMsg = null, $idUsuarioPrivado = null)
	{
		$addQuery = null;
		if(isset($idUltimoMsg))
		{
			$addQuery = ' AND c.id > ' . $idUltimoMsg;
		}
		
		$addQuery2 = null;
		if(!empty($idUsuarioPrivado))
		{
			$addQuery2 = ' AND ((c.idusuario = "' . $idUsuario . '" AND c.destinatario = "' . $idUsuarioPrivado . '")' .
						' OR (c.idusuario = "' . $idUsuarioPrivado . '" AND c.destinatario = "' . $idUsuario . '"))';
		}
		else
		{
			$addQuery2 = ' AND (c.destinatario IS NULL OR (c.destinatario = "' . $idUsuario .
							'" OR c.idusuario = "' . $idUsuario . '"))';
		}
		
		$sql = 'SELECT c.id, c.mensaje, c.destinatario, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec, rh.nombrec AS t_nombrec,' .
		' rh2.nombrec AS t_nombrec2, CONCAT(al2.nombre, " ", al2.apellidos) AS a_nombrec2' .
		' FROM chat AS c' .
		
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = c.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = c.idusuario' .
		
		' LEFT JOIN rrhh AS rh2 ON CONCAT("t_", rh2.idrrhh) = c.destinatario' .
		' LEFT JOIN alumnos AS al2 ON al2.idalumnos = c.destinatario' .
		
		' WHERE c.idcurso = ' . $idCurso . ' AND c.fecha >= "' . $_SESSION['usuario_fecha_actual_acceso'] . '"' . $addQuery .
		$addQuery2 .
		' GROUP BY c.id ORDER BY c.id ASC LIMIT 30';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionAlumno($idUsuario, $idCurso, $fecha)
	{
		$sql = 'UPDATE matricula SET fecha_conexion = "' . $fecha . '" WHERE idalumnos = ' . $idUsuario . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionTutor($idUsuario, $idCurso, $fecha)
	{
		$sql = 'UPDATE staff SET fecha_conexion = "' . $fecha . '" WHERE idrrhh = ' . $idUsuario . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnosConectados($idCurso)
	{
		$sql = 'SELECT m.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE m.idcurso = ' . $idCurso . ' AND m.fecha_conexion >= "' . date('Y-m-d H:i:s', time()-12) . '"' .
		' GROUP BY m.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	public function obtenerTutoresConectados($idCurso)
	{
		$sql = 'SELECT rh.idrrhh, rh.foto, rh.nombrec' .
		' FROM staff AS st' .
		' LEFT JOIN rrhh AS rh ON st.idrrhh =  rh.idrrhh' .
		' WHERE st.idcurso = ' . $idCurso . ' AND st.fecha_conexion >= "' . date('Y-m-d H:i:s', time()-12) . '"' .
		' GROUP BY st.idrrhh';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumChatPorAlumno($idUsuario, $idCurso)
	{
		$sql = "SELECT id" .
		" FROM chat" .
		" WHERE idusuario = " . $idUsuario . 
		" AND idcurso = " . $idCurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}