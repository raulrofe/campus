<div id="chat">

	<div id="chat_mensajes_container">
		<div id="chat_mensajes_titulo" data-translate-html="espacios.cafeteria">
			Cafeter&iacute;a
		</div>
		<div id="chat_mensajes"></div>
	</div>
	<div id="chat_usuarios_container">
		<div id="chat_usuarios_titulo" data-translate-html="general.usuarios">
			Usuarios
		</div>
		<div id="chat_usuarios"></div>
	</div>
	<div class="clear"></div>

	<div id="frm_chat_titulo2" data-translate-html="tertulia.mensaje">
		Escriba su mensaje
	</div>

	<form id="frm_chat" name="frm_chat" action="" method="post" onsubmit="LibChatTertulia.chat_enviar(); return false;">
		<ul>
			<li id="frm_chat_destinatarios">
				<select name="destinatario">
					<option value="0" data-translate-html="tertulia.todos">
						Todosdsd
					</option>
				</select>
			</li>

			<li id="chat_input_msg_li" class="clear">
				<textarea id="chat_input_msg" rows="1" cols="1" name="mensaje"></textarea>
			</li>

			<li>
				<button type="submit" data-translate-html="formulario.enviar">
					Enviar
				</button>
			</li>
		</ul>
		<div class="clear"></div>
	</form>
</div>

<script type="text/javascript" src="js-chat-default.js"></script>

<script type="text/javascript">
	tinymce.init({
		selector: "#chat_input_msg",
		menubar: false,
		statusbar: false,
		plugins: ["emoticons"],
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
		height : "250"
	});
</script>
