<div id="chat_tertulia_privado_<?php echo $get['idusuario'];?>" class="chat_tertulia_privado">

	<div class="chat_tertulia_privado_titulo">Tertulia privada con <?php echo Texto::textoPlano($rowUsuario->nombrec)?></div>
	
	<div class="chat_tertulia_privado_mensajes"></div>
	<div class="clear"></div>
	
	<form class="frm_chat_tertulia_privado" action="" method="post" onsubmit="LibChatTertuliaPrivado.chat_enviar('#chat_tertulia_privado_<?php echo $get['idusuario'];?>'); return false;">
		<ul>
			<li>
				<textarea id="chat_tertulia_privado_input_msg_<?php echo $get['idusuario'];?>" class="chat_tertulia_privado_input_msg" rows="1" cols="1" name="mensaje"></textarea>
				<input class="chat_tertulia_privado_idusuario_objetivo" type="hidden" name="destinatario" value="<?php echo $get['idusuario']?>" />
			</li>
			<li><button type="submit" data-translate-html="formulario.enviar">Enviar</button></li>
		</ul>
		<div class="clear"></div>
	</form>
</div>

<script type="text/javascript" src="js-chat-defaultPrivado.js"></script>

<script type="text/javascript">
	$('#popupModal_chat_tertulia_privado_<?php echo $get['idusuario']?> .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');

	LibChatTertuliaPrivado.obtenerNuevosMensajes(-1, '<?php echo $get['idusuario']?>');
</script>

<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		menubar: false,
		statusbar: false,
		plugins: ["emoticons"],
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
		height : "250"
	});
</script>