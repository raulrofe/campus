var LibChatTertuliaPrivado = {};

LibChatTertulia.LibChatTertuliaPrivado = 0;

LibChatTertuliaPrivado.http = function() {
    if(typeof window.XMLHttpRequest!='undefined'){
        return new XMLHttpRequest();
    }else{
        try{
            return new ActiveXObject('Microsoft.XMLHTTP');
        }catch(e){
            alert('Su navegador no soporta AJAX');
            return false;
        }
    }
    
	$(element).parents('.popupModal').find('.popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled').removeAttr('onclick').attr('onclick', 'return false');
};

LibChatTertuliaPrivado.obtenerNuevosMensajes = function(idLastMsg, idUsuario)
{
	var H=new LibChatTertuliaPrivado.http();
	    if(!H)return;
	    H.open('post','chat/mensajes/obtener/' + idLastMsg + '/' + idUsuario, true);
	    H.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	    H.onreadystatechange=function(){
	        if(H.readyState==4){
	        	LibChatTertuliaPrivado.obtenerNuevosMensajesCallback(H.responseText, idUsuario);
	            H.onreadystatechange=function(){};
	            H.abort();
	            H=null;
	        }
	    };
	    H.send();

		setTimeout(function(){
			LibChatTertuliaPrivado.chatComprobarSiEstaAbierto(H, idUsuario);
		}, 50);
};

LibChatTertuliaPrivado.chatComprobarSiEstaAbierto = function(H, idUsuario)
{
	if($('#popupModal_chat_tertulia_privado_' + idUsuario).size() == 0)
	{
	    H.abort();
	}
	else
	{
		setTimeout(function(){
			LibChatTertuliaPrivado.chatComprobarSiEstaAbierto(H, idUsuario);
		}, 50);
	}
};

LibChatTertuliaPrivado.obtenerNuevosMensajesCallback = function(json, idUsuario)
{
	try
	{
		json = jQuery.parseJSON(json);
		
		var idLastMsg = 0;
		if(json != undefined)
		{
			if(json.msg != undefined)
			{
				var chatMensajetNombre = '';
				for(var i in json.msg)
				{
					$('#chat_tertulia_privado_' + idUsuario).find('.chat_tertulia_privado_mensajes')
						.append('<div class="elemento"><strong>' + json.msg[i].nombre + ':</strong> ' + json.msg[i].contenido + '</div>');
				}
			}
			
			if(json.idLastMsg != undefined)
			{
				idLastMsg = json.idLastMsg;
				LibChatTertulia.LibChatTertuliaPrivado = json.idLastMsg;
				
				$('#chat_tertulia_privado_' + idUsuario).find('.chat_tertulia_privado_mensajes').scrollTop(
					$('#chat_tertulia_privado_' + idUsuario).find('.chat_tertulia_privado_mensajes').prop('scrollHeight')
				);
			}
			else
			{

				idLastMsg = LibChatTertulia.LibChatTertuliaPrivado;
			}
		
			setTimeout(function(){
				LibChatTertuliaPrivado.obtenerNuevosMensajes(idLastMsg, idUsuario);
			}, 10);
		}
	}
	catch(e)
	{
		
	}
};

LibChatTertuliaPrivado.chat_enviar = function(elemento)
{
	var id = $(elemento).find('.chat_tertulia_privado_input_msg').attr('id');
	
	CKEDITOR.instances[id].updateElement();
	
	$.ajax({
		url : 'chat/mensaje/nuevo',
		type: 'POST',
		data : $(elemento).find('form').serialize()
	}).success(function(json)
	{
		CKEDITOR.instances[id].setData('');
		CKEDITOR.instances[id].updateElement();
	}).complete(function(){
		$(elemento).parents('.popupModal').find('.popupModalHeaderLoader').addClass('hide');
	});
};

$('.chat_tertulia_privado').parents('.popupModal').find('.popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled').removeAttr('onclick').attr('onclick', 'return false');

//$('#popupModal_chat .popupModalHeaderIconReload').remove();