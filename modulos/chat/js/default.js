var LibChatTertulia = {};

LibChatTertulia.idLastMsg = 0;

LibChatTertulia.http = function(){
    if(typeof window.XMLHttpRequest!='undefined'){
        return new XMLHttpRequest();
    }else{
        try{
            return new ActiveXObject('Microsoft.XMLHTTP');
        }catch(e){
            alert('Su navegador no soporta AJAX');
            return false;
        }
    }
};

LibChatTertulia.obtenerNuevosMensajes = function(idLastMsg)
{
	var H=new LibChatTertulia.http();
	    if(!H)return;
	    H.open('post','chat/mensajes/obtener/' + idLastMsg, true);
	    H.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	    H.onreadystatechange=function(){
	        if(H.readyState==4){
	        	LibChatTertulia.obtenerNuevosMensajesCallback(H.responseText);
	            H.onreadystatechange=function(){};
	            H.abort();
	            H=null;
	        }
	    };
	    H.send();

		setTimeout(function(){
			LibChatTertulia.chatComprobarSiEstaAbierto(H);
		}, 50);
};

LibChatTertulia.chatComprobarSiEstaAbierto = function(H)
{
	if($('#popupModal_chat').size() == 0)
	{
	    H.abort();
	}
	else
	{
		setTimeout(function(){
			LibChatTertulia.chatComprobarSiEstaAbierto(H);
		}, 50);
	}
};

LibChatTertulia.obtenerNuevosMensajesCallback = function(json)
{
	try
	{
		json = jQuery.parseJSON(json);
		
		var idLastMsg = 0;
		if(json != undefined)
		{
			if(json.msg != undefined)
			{
				var chatMensajetNombre = '';
				for(var i in json.msg)
				{
					chatMensajetNombre = json.msg[i].nombre;
					if(json.msg[i].privado != undefined && json.msg[i].privado != null)
					{
						chatMensajetNombre += ' dice a ' + json.msg[i].privado + '</span>';
					}
					
					$('#chat_mensajes').append('<div class="elemento"><strong>' + chatMensajetNombre + ':</strong> ' + json.msg[i].contenido + '</div>');
				}
			}
			
			if(json.usuario != undefined)
			{
				var idUsuarioSelected = $('#frm_chat_destinatarios select').find('option:selected').val();
				
				$('#chat_usuarios').html('');
				$('#frm_chat_destinatarios select').html('<option value="0" data-translate-html="tertulia.todos">Todos</option>');
				for(var i in json.usuario)
				{
					if(json.usuario[i].idusuario != json.idusuario_completo)
					{
						$('#chat_usuarios').append('<div><img src="imagenes/mini_circle_green.png" alt="" width="7px" /> ' +
							'<a href="#" onclick="popup_open(\'Tertulia privada con ' + json.usuario[i].nombre + '\', \'chat_tertulia_privado_' +
								json.usuario[i].idusuario + '\', \'chat/' + json.usuario[i].idusuario +
								'\', 430, 550); return false;" title="">' +
							json.usuario[i].nombre + '</a></div>');
					}
					else
					{
						$('#chat_usuarios').append('<div><img src="imagenes/mini_circle_green.png" alt="" width="7px" /> ' +
							json.usuario[i].nombre + '</div>');
					}
					
					// selectbox de usuarios para mensajes privados // json.idusuario_completo
					if(json.usuario[i].idusuario != json.idusuario_completo)
					{
						var selectedActive = '';
						if(json.usuario[i].idusuario == idUsuarioSelected)
						{
							selectedActive = 'selected="selected"';
						}
						
						$('#frm_chat_destinatarios select').append('<option ' + selectedActive + ' value="' + json.usuario[i].idusuario + '">' + json.usuario[i].nombre + '</option>');
					}
				}
			}
			
			if(json.idLastMsg != undefined)
			{
				idLastMsg = json.idLastMsg;
				LibChatTertulia.idLastMsg = idLastMsg;
				$('#chat_mensajes').scrollTop($('#chat_mensajes').prop('scrollHeight'));
			}
			else
			{
				idLastMsg = LibChatTertulia.idLastMsg;
			}
		
			setTimeout(function(){
				LibChatTertulia.obtenerNuevosMensajes(idLastMsg);
			}, 10);
		}
	}
	catch(e)
	{
		
	}
};

LibChatTertulia.chat_enviar = function()
{
	CKEDITOR.instances['chat_input_msg'].updateElement();
	
	$.ajax({
		url : 'chat/mensaje/nuevo',
		type: 'POST',
		data : $('#frm_chat').serialize()
	}).success(function(json)
	{
		CKEDITOR.instances['chat_input_msg'].setData('');
		CKEDITOR.instances['chat_input_msg'].updateElement();
		
	}).complete(function(){
		$('#popupModal_chat .popupModalHeaderLoader').addClass('hide');
	});
};

$('#popupModal_chat .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled').removeAttr('onclick').attr('onclick', 'return false');

LibChatTertulia.obtenerNuevosMensajes(-1);