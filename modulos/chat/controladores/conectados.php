<?php
/* ------------------------ //
 * SIN USO?
 --------------------------*/


set_time_limit(0);
session_write_close();
header("Cache-control: private");
$vueltas = 0;
ignore_user_abort(1);

$objModelo = new ModeloChat();

$get = Peticion::obtenerGet();
if(isset($get['listadoUsuarios']))
{
    while(1)
    {
    	@ob_flush();
        flush();
        
        if(connection_status() != 0)
        {
           //	echo 'salida del chat';
           	exit;
        }
        
        $listUsuarios = explode(',', $get['listadoUsuarios']);
        //print_r($listUsuarios);
    	$usuariosConectados = $objModelo->obtenerUsuariosConectados(Usuario::getIdCurso());
        
        if($usuariosConectados->num_rows > 0)
        {
        	$json = array();
	        while($usuarioConectado = $usuariosConectados->fetch_object())
			{
				$json['usuario'][] = array('idusuario' => $usuarioConectado->idalumnos);
			}
			echo json_encode($json);
			@ob_flush();
			flush();
			break;
        }
        
        sleep(5);
    }
}