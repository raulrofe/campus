<?php
$get = Peticion::obtenerGet();

if(isset($get['idusuario']))
{
	$objUsuarios = new Usuarios();
	$rowUsuario = $objUsuarios->buscar_usuario_auto($get['idusuario']);
	
	if($rowUsuario->num_rows == 1)
	{
		$rowUsuario = $rowUsuario->fetch_object();
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'privado');
	}
}