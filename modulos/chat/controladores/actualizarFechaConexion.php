<?php
set_time_limit(0);
header("Cache-control: private");
ignore_user_abort(1);

$idUsuario = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();

session_write_close();

$objModelo = new ModeloChat();
while(1)
{
	echo "\n";
	@ob_flush();
	flush();
	
	if(connection_status() != 0)
	{
		//	echo 'salida del chat';
		die();
	}
	
	if(Usuario::compareProfile('alumno'))
	{
		$objModelo->actualizarFechaConexionAlumno($idUsuario, $idCurso);
	}
	else
	{
		$objModelo->actualizarFechaConexionTutor($idUsuario, $idCurso);
	}
	
	sleep(10);
}