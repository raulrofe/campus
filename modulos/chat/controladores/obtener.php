<?php
set_time_limit(60);
//ignore_user_abort(true);

$idUsuario = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();
$get = Peticion::obtenerGet();

$idUsuarioPrivado = null;
if(isset($get['idusuario']))
{
	$idUsuarioPrivado = $get['idusuario'];
}

session_write_close();
header("Cache-control: private");
$vueltas = 0;

ob_end_flush();

$objModelo = new ModeloChat();

if(isset($get['idLastMsg']) && is_numeric($get['idLastMsg']))
{	
	// numero de tutores conectados
	if($get['idLastMsg'] == -1)
	{
		$numAlumnos = 0;
		$numTutores = 0;
		$json['idLastMsg'] = 0;
	}
	else
	{
		// numero de alumnos conectados
		$numAlumnos = $objModelo->obtenerAlumnosConectados($idCurso);
		$numAlumnos = $numAlumnos->num_rows;
	
		$tutoresConectados = $objModelo->obtenerTutoresConectados($idCurso);
		$numTutores = $tutoresConectados->num_rows;
	}
	
    while(1)
    {
    	echo "\n";
    	@ob_flush();
    	flush();
    
    	if(connection_status() != 0)
        {
           	//	echo 'salida del chat';
           	if(Usuario::compareProfile('alumno'))
			{
				$objModelo->actualizarFechaConexionAlumno($idUsuario, $idCurso, date('Y-m-d H:i:s', time() - 20));
			}
			else
			{
				$objModelo->actualizarFechaConexionTutor($idUsuario, $idCurso, date('Y-m-d H:i:s', time() - 20));
			}
        	die();
        }
        
	    if(Usuario::compareProfile('alumno'))
		{
			$objModelo->actualizarFechaConexionAlumno($idUsuario, $idCurso, date('Y-m-d H:i:s'));
		}
		else
		{
			$objModelo->actualizarFechaConexionTutor($idUsuario, $idCurso, date('Y-m-d H:i:s'));
		}
        
    	$mensajes = $objModelo->obtenerMensajes(Usuario::getIdUser(true), $idCurso, $get['idLastMsg'], $idUsuarioPrivado);
        if($mensajes->num_rows > 0)
        {
        	$json = array();
	        while($mensaje = $mensajes->fetch_object())
			{
				if(isset($mensaje->t_nombrec))
				{
					$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
				}
				else
				{
					$nombrec = $mensaje->a_nombrec;
				}
				
				$contenidoMensaje = Texto::toEmoticons(Texto::bbcode($mensaje->mensaje));
				
				$msgPrivado = null;
				if(isset($mensaje->destinatario))
				{
					if(isset($mensaje->t_nombrec2))
					{
						$msgPrivado = $mensaje->t_nombrec2;
					}
					else
					{
						$msgPrivado = $mensaje->a_nombrec2;
					}
				}
				$contenidoMensaje = '<span>' . $contenidoMensaje . '</span>';
				
				$json['msg'][] = array('nombre' => $nombrec, 'contenido' => $contenidoMensaje, 'privado' => $msgPrivado);
				$json['idLastMsg'] = $mensaje->id;
			}
			
        }
        
        // obtiene los alumnos y tutores conectados
    	$alumnosConectados = $objModelo->obtenerAlumnosConectados($idCurso);
   	 	$tutoresConectados = $objModelo->obtenerTutoresConectados($idCurso);
        
        if($mensajes->num_rows > 0 || $alumnosConectados->num_rows != $numAlumnos || $tutoresConectados->num_rows != $numTutores)
        {
        	// pinta los alumnos conectados
        	if($alumnosConectados->num_rows > 0)
	        {
	        	while($alumnoConectado = $alumnosConectados->fetch_object())
				{
					$json['usuario'][] = array('idusuario' => $alumnoConectado->idalumnos, 'nombre' => $alumnoConectado->nombrec/*, 'foto' => URL_BASE . 'imagenes/fotos/' . $alumnoConectado->foto*/);
				}
	        }
	        
	        // pinta los tutores conectados
	        if($tutoresConectados->num_rows > 0)
	        {
	        	while($tutorConectado = $tutoresConectados->fetch_object())
				{
					$json['usuario'][] = array('idusuario' => 't_' . $tutorConectado->idrrhh, 'nombre' => $tutorConectado->nombrec/*, 'foto' => URL_BASE . 'imagenes/fotos/' . $tutorConectado->foto*/);
				}
	        }
	        
	        $json['idusuario'] = $idUsuario;
	        $json['idusuario_completo'] = Usuario::getIdUser(true);
	        
        	echo json_encode($json);
			@ob_flush();
			flush();
			exit;
        }

     	set_time_limit($vueltas + 20);
        
        if($vueltas < 10)
        {
       		sleep(1);
        }
        else if($vueltas < 20)
        {
        	sleep(5);
        }
        else
        {
        	sleep(10);
        }
    }
}