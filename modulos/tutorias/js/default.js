/*function tutoria_go_to_asignado()
{
	$("#tutoria_cursos_no_asignados option:selected").each(function ()
	{
		if($('#tutoria_cursos_asignados option[value="' + $(this).val() + '"]').size() == 0)
		{
			$('#tutoria_cursos_asignados').append('<option value="' + $(this).val() + '">' + $(this).text() + '</option>');
			
			$('#tutoria_cursos_no_asignados option[value="' + $(this).val() + '"]').remove();
		}
	});
}

function tutoria_go_to_no_asignado()
{
	$("#tutoria_cursos_asignados option:selected").each(function ()
	{
		if($('#tutoria_cursos_no_asignados option[value="' + $(this).val() + '"]').size() == 0)
		{
			$('#tutoria_cursos_no_asignados').append('<option value="' + $(this).val() + '">' + $(this).text() + '</option>');
			
			$('#tutoria_cursos_asignados option[value="' + $(this).val() + '"]').remove();
		}
	});
}

$(document).ready(function()
{
	$('#frm_tutoria_asignar_cursos').submit(function()
	{
		$('#tutoria_cursos_asignados option').attr('selected', 'selected');
		$('#tutoria_cursos_no_asignados option').attr('selected', 'selected');
	});
});*/

var ModuloTutoria = {};

ModuloTutoria.seleccionarTodo = function()
{
	$('#frm_tutoria_asignar_cursos_scroll input[type="checkbox"]').each(function(key, value)
	{
		if($(this).attr('checked') != 'checked')
		{
			$(this).attr('checked', 'checked');
		}
	});
};

ModuloTutoria.deseleccionarTodo = function()
{
	$('#frm_tutoria_asignar_cursos_scroll input[type="checkbox"]').each(function(key, value)
	{
		if($(this).attr('checked') == 'checked')
		{
			$(this).removeAttr('checked');
		}
	});
};