<?php

$get = Peticion::obtenerGet();

$mi_tutoria = new Tutoria();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	extract($post);

	$mi_tutoria->insertar_speaking($post['diaSemana'], $post['horaInicio'],  $post['tema'], Usuario::getIdUser(), Usuario::getIdCurso());
}

if(isset($get['todas']) && $get['todas'] == 1)
{
	$registros_tutorias = $mi_tutoria->ver_tutorias();
}
else if(isset($get['privadas']) && $get['privadas'] == 1)
{
	$registros_tutorias = $mi_tutoria->ver_tutorias_privadas();
}
else
{
	$registros_tutorias = $mi_tutoria->obtenerTodasTutoriasCurso(Usuario::getIdCurso());
}

$diasemana = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
$dianumero = date("N");
$hora_actual = date("H:i");

$dia_semana = $diasemana[$dianumero];

$ct_idrrhh = null;
$ct_idcurso = null;

// para saber si hay alguna tutoria abierta
mvc::cargarModuloSoporte('chat_tutoria');
$objModeloChatTutoria = new ModeloChatTutoria();
if(Usuario::compareProfile('alumno'))
{
	$ct_idcurso = Usuario::getIdCurso();
}
else
{
	$ct_idrrhh = Usuario::getIdUser(true);
}
$tutoria = $objModeloChatTutoria->obtenerTutoria($ct_idrrhh, $ct_idcurso);

//Saber si hay speakings
$speakingsUsuario = $mi_tutoria->obtenerSpeakingsPorUsuario(Usuario::getIdCurso(), Usuario::getIdUser());

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');