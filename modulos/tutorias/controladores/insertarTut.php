<?php 
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

$mi_tutoria = new Tutoria();
//$mi_general = new Generales();
//$mi_tutoria = new Tutoria();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	extract($post);
	
	$minutos_validos = array(0, 10, 20, 30, 40, 50);
	
	if(isset($dia_semanaf, $hora_inicio, $hora_fin, $minuto_inicio, $minuto_fin)
		&& (is_numeric($hora_inicio) && is_numeric($hora_fin) && is_numeric($minuto_inicio) && is_numeric($minuto_fin))
		&& ($hora_inicio >= 0 && $hora_inicio <= 23) && ($hora_fin >= 0 && $hora_fin <= 23)
		&& in_array($minuto_inicio, $minutos_validos) && in_array($minuto_fin, $minutos_validos))
	{
		// tutoria privada
		if(isset($post['tutoria_privada']) && $post['tutoria_privada'] == 1
			&& isset($post['idalumno']) && is_numeric($post['idalumno']))
		{
			if($mi_tutoria->insertar_tutoria_privada($post['idalumno'], $dia_semanaf,$hora_inicio,$hora_fin,$minuto_inicio,$minuto_fin))
			{
				Alerta::guardarMensajeInfo('tutoriacreadaprivada','Se ha añadido la tutoría privada');
				Url::redirect('aula/tutorias/privadas');
			}
		}
		// tutoria general
		else
		{
			if($mi_tutoria->insertar_tutoriag($dia_semanaf,$hora_inicio,$hora_fin,$minuto_inicio,$minuto_fin))
			{		
				Alerta::guardarMensajeInfo('tutoriacreada','Se ha añadido la tutoría. Ahora puedes asignar los cursos.');
				Url::redirect('aula/tutorias/todas');
			}
		}
	}
}

$alumnos = $mi_tutoria->obtenerAlumnosCurso(Usuario::getIdCurso());

$registros_tutorias = $mi_tutoria->ver_tutorias();
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');