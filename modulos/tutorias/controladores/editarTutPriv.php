<?php 
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

if(isset($get['idtutoria']) && is_numeric($get['idtutoria']))
{
	$mi_tutoria = new Tutoria();
	
	$tutoria = $mi_tutoria->obtenerTutoriaPrivada($get['idtutoria']);
	
	if($tutoria->num_rows == 1)
	{
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			
			$minutos_validos = array(0, 10, 20, 30, 40, 50);
	
			if(isset($post['idalumno'], $get['idtutoria'], $post['dia_semanaf'], $post['hora_inicio'], $post['hora_fin'], $post['minuto_inicio'], $post['minuto_fin'])
				&& (is_numeric($get['idtutoria']) && is_numeric($post['dia_semanaf']) && is_numeric($post['idalumno'])
				&& is_numeric($post['hora_inicio']) && is_numeric($post['hora_fin'])
				&& is_numeric($post['minuto_inicio']) && is_numeric($post['minuto_fin']))
				&& ($post['dia_semanaf'] > 0 && $post['dia_semanaf'] <= 7) && ($post['hora_inicio'] >= 0 && $post['hora_inicio'] <= 23)
				&& ($post['hora_fin'] >= 0 && $post['hora_fin'] <= 23)
				&& in_array($post['minuto_inicio'], $minutos_validos) && in_array($post['minuto_fin'], $minutos_validos))
			{
				$hora_inicio = $post['hora_inicio'] . ":" . $post['minuto_inicio'] . ":00";
				$hora_fin = $post['hora_fin'] . ":" . $post['minuto_fin'] . ":00";
				
				if($hora_inicio < $hora_fin)
				{
					if($mi_tutoria->actualizar_tutoria_priv($get['idtutoria'], $post['idalumno'], $post['dia_semanaf'], $hora_inicio, $hora_fin))
					{
						Alerta::guardarMensajeInfo('tutoriaactualizadaprivada','Se ha actualizado la tutor&iacute;a privada');
						
						Url::redirect('aula/tutorias/privadas');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('fechafinmayor','La fecha de fin debe ser mayor que la de inicio');
				}
			}
		}

		$alumnos = $mi_tutoria->obtenerAlumnosCurso(Usuario::getIdCurso());
		
		$tutoria = $tutoria->fetch_object();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');
	}
}