<?php

$get = Peticion::obtenerGet();

$mi_tutoria = new Tutoria();

$speakingsConcertados = $mi_tutoria->obtenerSpeakingsPorDia(Usuario::getIdCurso(), $get['dia']);

$horasOcupadas = array();

$htmlContent = "<select name='horaInicio'>" ;
	$htmlContent .= "<option value='14:00'>14:00</option>" ;
	$htmlContent .= "<option value='14:15'>14:15</option>" ;
	$htmlContent .= "<option value='14:30'>14:30</option>" ;
	$htmlContent .= "<option value='14:45'>14:45</option>" ;
	$htmlContent .= "<option value='15:00'>15:00</option>" ;
	$htmlContent .= "<option value='15:15'>15:15</option>" ;
	$htmlContent .= "<option value='15:30'>15:30</option>" ;
	$htmlContent .= "<option value='15:45'>15:45</option>" ;
$htmlContent .= "</select>" ;

if($speakingsConcertados->num_rows > 0 && $speakingsConcertados->num_rows < 8){

	$htmlContent = "<select name='horaInicio'>" ;

	while($f = $speakingsConcertados->fetch_object()) {
		$horasOcupadas[] = $f->hora;
	}

	if(!in_array('14:00', $horasOcupadas)){
		$htmlContent .= "<option value='14:00'>14:00</option>" ;
	}
	if(!in_array('14:15', $horasOcupadas)){
		$htmlContent .= "<option value='14:15'>14:15</option>" ;
	}
	if(!in_array('14:30', $horasOcupadas)){
		$htmlContent .= "<option value='14:30'>14:30</option>" ;
	}
	if(!in_array('14:45', $horasOcupadas)){
		$htmlContent .= "<option value='14:45'>14:45</option>" ;
	}
	if(!in_array('15:00', $horasOcupadas)){
		$htmlContent .= "<option value='15:00'>15:00</option>" ;
	}
	if(!in_array('15:15', $horasOcupadas)){
		$htmlContent .= "<option value='15:15'>15:15</option>" ;
	}
	if(!in_array('15:30', $horasOcupadas)){
		$htmlContent .= "<option value='15:30'>15:30</option>" ;
	}
	if(!in_array('15:45', $horasOcupadas)){
		$htmlContent .= "<option value='15:45'>15:45</option>" ;
	}

	$htmlContent .= "</select>" ;

}
else {
	if($speakingsConcertados->num_rows == 8){
		$htmlContent = "<p>No hay horas disponibles para este día</p>";
	}
}

echo $htmlContent;

