<?php 
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

if(isset($get['idtutoria']) && is_numeric($get['idtutoria']))
{
	$mi_tutoria = new Tutoria();
	
	$tutoria = $mi_tutoria->obtenerTutoriaPrivada($get['idtutoria']);
	
	if($tutoria->num_rows == 1)
	{
		if($mi_tutoria->eliminar_tutoria_priv($get['idtutoria']))
		{
			Alerta::guardarMensajeInfo('tutoriaborradaprivada','Se ha borrado la tutor&iacute;a privada');
		}
	}
}

Url::redirect('aula/tutorias/privadas');