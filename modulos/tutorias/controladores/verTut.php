<?php
$get = Peticion::obtenerGet();

//Array temas
$temaIngles[1] = 'Unit 1 - Describe what you do every morning';
$temaIngles[2] = 'Unit 1 - Advantages and disadvantages of living in a big city';
$temaIngles[3] = 'Unit 1 - Do and don’t ……at work';
$temaIngles[4] = 'Unit 2 - What was school like 20 years ago? Discuss';
$temaIngles[5] = 'Unit 2 - Where were you when ……? Tell me about something famous that happened and what you were doing at that time';
$temaIngles[6] = 'Unit 3 - Tell me about your career (e.g. I have been teaching for 10 years)';
$temaIngles[7] = 'Unit 3 - Tell me about your bucket list. What are your ideas? (use I have never….)';
$temaIngles[8] = 'Unit 3 - What are your reasons to learn English?';
$temaIngles[9] = 'Unit 4 - Can you live without the Internet? Why/Why not?';
$temaIngles[10] = 'Unit 4 - Advantages and disadvantages of technology.';
$temaIngles[11] = 'Unit 4 - Are you good at lodging a complaint? Describe a situation where you bought something and you were not satisfied with it.';
$temaIngles[12] = 'Unit 5 - Do you recycle? Do you find some problems when recycling in your city?';
$temaIngles[13] = 'Unit 5 - Do you use public transport? How do you go to work?';
$temaIngles[14] = 'Unit 6 - Tell me about your last holiday. Did you go somewhere interesting?';
$temaIngles[15] = 'Unit 6 - Which place would you like to visit someday? Why?';
$temaIngles[16] = 'Unit 7 - What do you do to be healthy? Do you eat well? Do you play/do any sports?';
$temaIngles[17] = 'Unit 7 - What can we do to teach children to eat well? ';
$temaIngles[18] = 'Unit 8 - Do you have any friendship you have kept up for ages?';
$temaIngles[19] = 'Unit 8 - Does love at first sight exist? Why? Why not?';
$temaIngles[20] = 'Unit 9 - What is the last movie you have seen/ book you have read/ CD you have bought? Did you like it? Why? Why not?';
$temaIngles[21] = 'Unit 9 - What do you think about modern art?';
$temaIngles[22] = 'Unit 10 - Have you ever driven in a foreign country? What was it like? Did you mind doing it?';
$temaIngles[23] = 'Unit 10 - Are you afraid of flying? Which is your favourite means of transport?';

$mi_tutoria = new Tutoria();

if(isset($get['todas']) && $get['todas'] == 1)
{
	$registros_tutorias = $mi_tutoria->ver_tutorias();
}
else if(isset($get['privadas']) && $get['privadas'] == 1)
{
	$registros_tutorias = $mi_tutoria->ver_tutorias_privadas();
}
else
{
	$registros_tutorias = $mi_tutoria->obtenerTodasTutoriasCurso(Usuario::getIdCurso());
}

$diasemana = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
$dianumero = date("N");
$hora_actual = date("H:i");

$dia_semana = $diasemana[$dianumero];

$ct_idrrhh = null;
$ct_idcurso = null;

// para saber si hay alguna tutoria abierta
mvc::cargarModuloSoporte('chat_tutoria');
$objModeloChatTutoria = new ModeloChatTutoria();
if(Usuario::compareProfile('alumno')){
	$ct_idcurso = Usuario::getIdCurso();
} else {
	$ct_idrrhh = Usuario::getIdUser(true);
}

$tutoria = $objModeloChatTutoria->obtenerTutoria($ct_idrrhh, $ct_idcurso);

//Saber si hay speakings
if(Usuario::compareProfile(array('tutor', 'coordinador', 'supervisor'))){
	$speakingsUsuario = $mi_tutoria->obtenerSpeakings(Usuario::getIdCurso());
} else {
	$speakingsUsuario = $mi_tutoria->obtenerSpeakingsPorUsuario(Usuario::getIdCurso(), Usuario::getIdUser());
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');