<?php 
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

if(isset($get['idtutoria']) && is_numeric($get['idtutoria']))
{
	$idCursoLogin = Usuario::getIdCurso();
	
	$mi_tutoria = new Tutoria();
	
	$tutoria = $mi_tutoria->obtenerTutoria($get['idtutoria']);
	
	if($tutoria->num_rows == 1)
	{
		$diasemana = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
		
		// POST //
		if(Peticion::isPost())
		{
			$mi_tutoria->eliminar_todos_cursos_asignados($get['idtutoria']);
				
			$post = Peticion::obtenerPost();
			if(isset($post['cursos']) && is_array($post['cursos']))
			{
				foreach($post['cursos'] as $idcurso)
				{
					if(is_numeric($idcurso))
					{
						$tutoria_curso = $mi_tutoria->obtenerTutoriaCurso($get['idtutoria'], $idcurso);
						if($tutoria_curso->num_rows == 0)
						{
							$mi_tutoria->asignar_curso($get['idtutoria'], $idcurso);
						}
					}
				}
			}
			/*
			if(isset($post['cursos_no_asignados']) && is_array($post['cursos_no_asignados']))
			{
				foreach($post['cursos_no_asignados'] as $idcurso)
				{
					if(is_numeric($idcurso))
					{
						$tutoria_curso = $mi_tutoria->obtenerTutoriaCurso($get['idtutoria'], $idcurso);
						if($tutoria_curso->num_rows == 1)
						{
							$mi_tutoria->eliminar_tutoria_curso($get['idtutoria'], $idcurso);
						}
					}
				}
			}*/
			
			Alerta::mostrarMensajeInfo('cursosasignados','Se han asignados los cursos');
		}
		// END POST //
		
		//$cursos_asignados = array();
		$cursos_asignados_solo_id = array();
		$row_cursos_asignados = $mi_tutoria->obtenerCursosAsignados($get['idtutoria']);
		while($curso = $row_cursos_asignados->fetch_object())
		{
			//$cursos_asignados[] = array('idcurso' => $curso->idcurso, 'titulo' => $curso->titulo);
			$cursos_asignados_solo_id[] = $curso->idcurso;
		}
		
		//$cursos_no_asignados = array();
		$row_cursos = $mi_tutoria->obtenerCursos();
		/*while($curso = $row_cursos->fetch_object())
		{
			if(!in_array($curso->idcurso, $cursos_asignados_solo_id))
			{
				$cursos_no_asignados[] = array('idcurso' => $curso->idcurso, 'titulo' => $curso->titulo);
			}
		}*/
		//$cursos_no_asignados = array_diff($cursos_no_asignados, $cursos_asignados);
		
		$tutoria = $tutoria->fetch_object();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'tutorias');
	}
}