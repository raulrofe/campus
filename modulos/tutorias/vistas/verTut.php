<div id="tutoria" class='caja_frm'>
	<div class='subtitleModulo' data-translate-html="tutoria.horarios">
		Horario de tutorias
	</div>

	<div>
		<?php if(mysqli_num_rows($registros_tutorias) > 0):?>

			<?php while($f = mysqli_fetch_assoc($registros_tutorias)):?>
				<div class='elemento'>
					<div class='fleft'>
						<img src='imagenes/tutoria.png' class='fleft'/>
						<div class='horaTutoria'>
							<?php echo $diasemana[$f['dia_semana'] - 1]?>: <?php echo $f['hora_inicio']." - ".$f['hora_fin']?>
						</div>
					</div>

					<?php if(Usuario::compareProfile(array('tutor', 'coordinador'))):?>
						<div class="elementoPie">
							<ul class="fright elementoListOptions">
								<li>
									<a href="aula/tutorias/asignar_cursos/<?php echo $f['idtutoria']?>">
										<img src='imagenes/options/asignar-cursos.png' rel='tooltip' title="Asignar a cursos" data-translate-title="tutoria.asignar_a"/>
									</a>
								</li>

								<li>
									<a href="aula/tutorias/editar/<?php echo $f['idtutoria']?>" rel="tooltip" title="Editar tutor&iacute;a" data-translate-title="tutoria.eliminar">
										<img src='imagenes/options/edit.png' />
									</a>
								</li>

								<li>
									<a href="#" <?php echo Alerta::alertConfirmOnClick('eliminartutoria', '¿Deseas eliminar esta tutoría?', 'aula/tutorias/borrar/' . $f['idtutoria']);?>>
										
										<img src='imagenes/options/delete.png' alt='' rel='tooltip' title='Eliminar tutor&iacute;a' data-translate-html="tutoria.eliminar"/>

									</a>
								</li>
							</ul>

							<div class="clear"></div>

						</div>

					<?php endif;?>

					<div class="clear"></div>

				</div>
			<?php endwhile;?>
		<?php else:?>
			
			<p data-translate-html="tutoria.no_tutorias">
				No existen tutor&iacute;as
			</p>

		<?php endif;?>

		<?php if($tutoria->num_rows > 0 && !Usuario::compareProfile('supervisor')):?>
			
			<br />
			<div class='tutoriaAbierta'>
				<a href='#' onClick="parent.popup_open('Tutor&iacute;a online', 'chat_tutoria', 'aula/tutorias/chat', 430, 700); return false;" data-translate-html="tutoria.es_tutoria">
					Es hora de tutoria, puedes entrar desde aqu&iacute;
				</a>
			</div>

		<?php endif;?>
	</div>

	<?php if(Usuario::getIdCurso() == 646 OR Usuario::getIdCurso() == 682 OR Usuario::getIdCurso() == 633 OR Usuario::getIdCurso() == 681
			OR Usuario::getIdCurso() == 718 OR Usuario::getIdCurso() == 719 OR Usuario::getIdCurso() == 759 OR Usuario::getIdCurso() == 746
			OR Usuario::getIdCurso() == 783 OR Usuario::getIdCurso() == 760): ?>
		<?php if(!Usuario::compareProfile(array('tutor', 'coordinador', 'supervisor'))): ?>
			<div id="anuncios">
				<div class='subtitleModulo'>
					<span data-translate-html="tutoria.hora_speaking">
						Hora Speakings disponibles
					</span>
				</div>

				<div class='caja_frm'>
					<br/>
					<form id="frmInsertSpeaking" name='frmInsertSpeaking' method='post' action='aula/tutorias/solicitar-speaking'>
						<div class='filafrm'>
							<div class='etiquetafrm' data-translate-html="tutoria.dia_semana">
								D&iacute;a de la semana
							</div>
							<div class='campofrm'>
								<select name="diaSemana">
									<option value="2016-10-25">25/10/2016</option>
									<option value="2016-11-01">01/11/2016</option>
									<option value="2016-11-08">08/11/2016</option>
									<option value="2016-11-15">15/11/2016</option>
									<option value="2016-11-22">22/11/2016</option>
									<?php if(Usuario::getIdCurso() >= 681): ?>
										<option value="2016-11-29">29/11/2016</option>
										<option value="2016-12-01">01/12/2016</option>
										<option value="2016-12-08">08/12/2016</option>
									<?php elseif(Usuario::getIdCurso() >= 718): ?>
										<option value="2016-12-13">13/12/2016</option>
										<option value="2016-12-15">15/12/2016</option>
										<option value="2016-12-20">20/12/2016</option>
									<?php endif; ?>
								</select>
							</div>
						</div>

						<div class='filafrm'>
							
							<div class='etiquetafrm' data-translate-html="tutoria.horai">
								Hora Inicio:
							</div>

							<div class='campofrm' id="selectHoras">
								<select name='horaInicio'>
									<option value="14:00">14:00</option>
									<option value="14:15">14:15</option>
									<option value="14:30">14:30</option>
									<option value="14:45">14:45</option>
									<option value="15:00">15:00</option>
									<option value="15:15">15:15</option>
									<option value="15:30">15:30</option>
									<option value="15:45">15:45</option>
								</select>
							</div>
							<div class='clear'></div>
						</div>

						<div class='filafrm'>
							<div class='etiquetafrm' data-translate-html="tutoria.elige_tema">
								Elige tema
							</div>

							<div class='campofrm'>
								<select name='tema'>
									<option value="1">Unit 1 - Describe what you do every morning.</option>
									<option value="2">Unit 1 - Advantages and disadvantages of living in a big city.</option>
									<option value="3">Unit 1 - Do and don’t ……at work.</option>
									<option value="4">Unit 2 - What was school like 20 years ago? Discuss.</option>
									<option value="5">Unit 2 - Where were you when ……? Tell me about something famous that happened and what you were doing at that time.</option>
									<option value="6">Unit 3 - Tell me about your career (e.g. I have been teaching for 10 years).</option>
									<option value="7">Unit 3 - Tell me about your bucket list. What are your ideas? (use I have never….)</option>
									<option value="8">Unit 3 - What are your reasons to learn English?</option>
									<option value="9">Unit 4 - Can you live without the Internet? Why/Why not?</option>
									<option value="10">Unit 4 - Advantages and disadvantages of technology.</option>
									<option value="11">Unit 4 - Are you good at lodging a complaint? Describe a situation where you bought something and you were not satisfied with it.</option>
									<option value="12">Unit 5 - Do you recycle? Do you find some problems when recycling in your city?</option>
									<option value="13">Unit 5 - Do you use public transport? How do you go to work?</option>
									<option value="14">Unit 6 - Tell me about your last holiday. Did you go somewhere interesting?</option>
									<option value="15">Unit 6 - Which place would you like to visit someday? Why?</option>
									<option value="16">Unit 7 - What do you do to be healthy? Do you eat well? Do you play/do any sports?</option>
									<option value="17">Unit 7 - What can we do to teach children to eat well? </option>
									<option value="18">Unit 8 - Do you have any friendship you have kept up for ages?</option>
									<option value="19">Unit 8 - Does love at first sight exist? Why? Why not?</option>
									<option value="20">Unit 9 - What is the last movie you have seen/ book you have read/ CD you have bought? Did you like it? Why? Why not?</option>
									<option value="21">Unit 9 - What do you think about modern art?</option>
									<option value="22">Unit 10 - Have you ever driven in a foreign country? What was it like? Did you mind doing it?</option>
									<option value="23">Unit 10 - Are you afraid of flying? Which is your favourite means of transport?</option>
								</select>
							</div>
							<div class='clear'></div>
						</div>
						<div><input type='submit' value='Guardar' class="width100"/></div>
					</form>
				</div>
			</div>

			<div>
				<?php if(mysqli_num_rows($speakingsUsuario) > 0):?>

					<?php while($speak = mysqli_fetch_object($speakingsUsuario)):?>
						<div class='elemento'>
								<div class='fleft'>
									<img src='imagenes/tutoria.png' alt='' class='fleft'/>
									<div class='horaTutoria'><?php echo 'Tiene una cita concertada para el speaking el ' . date('d/m/Y', strtotime($speak->dia)) . ' a las ' . $speak->hora ?></div>
								</div>
								<div class="clear"></div>
							</div>
					<?php endwhile;?>
				<?php else:?>
					<p data-translate-html="tutoria.no_speaking">
						No existen speaking concertados
					</p>
				<?php endif;?>
			</div>
		<?php else: ?>
			<div class='subtitleModulo' data-translate-html="tutoria.speaking_alumnos">
				Speakings alumnos
			</div>

			<div>
				<?php if(mysqli_num_rows($speakingsUsuario) > 0):?>

					<?php while($speak = mysqli_fetch_object($speakingsUsuario)):?>
						<div class='elemento'>
								<div class='fleft'>
									<img src='imagenes/tutoria.png' class='fleft'/>
									<div class='horaTutoria'>
										<p><?php echo 'Nombre: ' . $speak->nombre . ' ' . $speak->apellidos ?></p>
										<p><?php echo 'tiene una cita concertada para el speaking el ' . date('d/m/Y', strtotime($speak->dia)) . ' a las ' . $speak->hora ?></p>
										<p><?php echo 'Tema: ' . $speak->titulo ?></p>
										<p><?php echo 'Curso: ' . $temaIngles[$speak->tema] ?></p>
									</div>
								</div>
								<div class="clear"></div>
							</div>
					<?php endwhile;?>
				<?php else:?>
					<p data-translate-html="tutoria.no_speaking">
						No existen speaking concertados
					</p>
				<?php endif;?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>

<script type="text/javascript">
	// Devolvemos la petición ajax
	$("select[name=diaSemana]").on("change", function(){
		var dia = $(this).val();
		getHorasSpeaking(dia);
	});

	getHorasSpeaking('2016-10-25');

	function getHorasSpeaking(dia){
		$.ajax({
			type: 'GET',
			url: 'http://campusaulainteractiva.aulasmart.net/obtenerspeakings/' + dia,
			dataType: 'text',
		})
		.done(function (data) {
			$('#selectHoras').html(data);
		})
		.fail(function (data) {
			console.log(data);
		})
	}

</script>
