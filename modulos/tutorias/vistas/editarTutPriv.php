<div id='tutoria'>
	<div class='subtitleModulo'>
		<span data-translate-html="tutoria.editar_privada">
			Editar Tutor&iacute;a privada
		</span>
	</div>

	<br/>

	<form id="frmEditTutoria" name='' method='post' action=''>

		<div class='filafrm'>

			<div class='etiquetafrm' data-translate-html="tutoria.dia_semana">
				D&iacute;a de la semana
			</div>

			<div class='campofrm'>
				<?php $mi_tutoria->dia_semana($tutoria->dia_semana);?>
			</div>
		</div>

		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="tutoria.horai">
				Hora Inicio:
			</div>

			<div class='campofrm'>
				<select name='hora_inicio'>
					<?php $mi_tutoria->horas(date('H', strtotime($tutoria->hora_inicio)));?>
				</select>

				<span> : </span>

				<select name='minuto_inicio'>
					<?php $mi_tutoria->minutos(date('i', strtotime($tutoria->hora_inicio)));?>
				</select>
			</div>

			<div class='clear'></div>

		</div>

		<div class='filafrm'>

			<div class='etiquetafrm' data-translate-html="tutoria.horaf">
				Hora Fin:
			</div>

			<div class='campofrm'>
				<select name='hora_fin'>
					<?php $mi_tutoria->horas(date('H', strtotime($tutoria->hora_fin)));?>
				</select>

				<span> : </span>

				<select name='minuto_fin'>
					<?php $mi_tutoria->minutos(date('i', strtotime($tutoria->hora_fin)));?>
				</select>
			</div>

			<div class='clear'></div>

		</div>

		<div class='filafrm'>

			<div class='etiquetafrm' data-translate-html="general.alumno">
				Alumno
			</div>

			<div class='campofrm'>
				<select name="idalumno">
					<option value="0" data-translate-html="tutoria.sel_alumno">- Selecciona un alumno -</option>
					<?php if($alumnos->num_rows > 0):?>
						<?php while($alumno = $alumnos->fetch_object()):?>
							<option value="<?php echo $alumno->idalumnos;?>"
								<?php if($tutoria->idalumnos == $alumno->idalumnos) echo 'selected="selected"';?>>
								<?php echo Texto::textoPlano($alumno->nombrec);?>
							</option>
						<?php endwhile;?>
					<?php endif;?>
				</select>
			</div>
		</div>
		<div><input type='submit' value='Editar' class="width100" data-translate-value="general.editar"/></div>
	</form>
</div>