<div id="tutoria" class='caja_frm'>

	<div class='subtitleModulo' data-translate-html="tutoria.horarios_priv">
		Horario de tutorias privadas
	</div>

	<div>
	<?php if(mysqli_num_rows($registros_tutorias) > 0):?>
		<?php while($f = mysqli_fetch_assoc($registros_tutorias)):?>
			<div class='elemento'>

				<div class='fleft'>
					<img src='imagenes/tutoria.png' class='fleft'/>
					<div class='horaTutoria'>
						<?php echo Texto::textoPlano($f['nombrec'])?> - <?php echo $diasemana[$f['dia_semana'] - 1]?>: <?php echo $f['hora_inicio']." - ".$f['hora_fin']?>
					</div>
				</div>

				<?php if(Usuario::compareProfile(array('tutor', 'coordinador'))):?>
					<div class="elementoPie">
						<ul class="fright elementoListOptions">
							<li>
								<a rel="tooltip" title="Editar tutor&iacute;a privada" href="aula/tutorias/privada/editar/<?php echo $f['idtutoria_priv']?>" data-translate-title="tutoria.editar_privada">
									<img src='imagenes/options/edit.png' alt='' />
								</a>
							</li>

							<li>
								<a rel="tooltip" title="Eliminar tutor&iacute;a privada" href="#" <?php echo Alerta::alertConfirmOnClick('eliminartutoria', '¿Deseas eliminar esta tutoría?', 'aula/tutorias/privada/borrar/' . $f['idtutoria_priv']);?> data-translate-title="tutoria.eliminar_priv">
										<img src='imagenes/options/delete.png' />
								</a>
							</li>						
						</ul>
						<div class="clear"></div>
					</div>
				<?php endif;?>
				<div class="clear"></div>
			</div>
		<?php endwhile;?>
	<?php else:?>

		<p class="t_center" data-translate-html="tutoria.no_tutorias_priv">
			No existen tutor&iacute;as privadas
		</p>

	<?php endif;?>
		
		<?php if($tutoria->num_rows > 0):?>
			<br />
				<div class='tutoriaAbierta'>
					<a href='#' onClick="parent.popup_open('Tutor&iacute;a online', 'chat_tutoria', 'aula/tutorias/chat', 430, 700); return false;" data-translate-html="tutoria.es_tutoria">
						Es hora de tutoria, puedes entrar desde aqu&iacute;
					</a>
				</div>
		<?php endif;?>
	</div>
</div>	
