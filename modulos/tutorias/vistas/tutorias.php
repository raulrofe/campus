<div class='caja_separada'>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class="popupIntro">
			<!-- <h2>TUTOR&Iacute;AS</h2> -->
			<div class='subtitle t_center'>
				<img src="imagenes/tutorias/tutorias.png" style="vertical-align:middle;"/>
				<span data-translate-html="tutoria.titulo">
					Tutor&iacute;as
				</span>
			</div>

			<br/>

			<p class='t_justify' data-translate-html="tutoria.descripcion1">
				Las tutor&iacute;as telem&aacute;ticas le permiten establecer una l&iacute;nea de comunicaci&oacute;n directa con el
				alumnado y la posibilidad de ofrecerle una respuesta r&aacute;pida a sus dudas acerca de los contenidos del curso.
			</p>

			<br/>

			<p class='t_justify' data-translate-html="tutoria.descripcion3">

				Para prestar un servicio de tutor&iacute;a eficaz, as&iacute; como para aumentar las posibilidades de comunicaci&oacute;n con el alumnado, cuenta con dos modalidades de tutor&iacute;a: general y privada.

			</p>
		</div>
	<?php else:?>
		<div class="popupIntro">

			<div class='subtitle t_center'>
				<img src="imagenes/tutorias/tutorias.png" style="vertical-align:middle;"/>
				<span data-translate-html="tutoria.titulo">Tutor&iacute;as</span>
			</div>

			<br/>

			<p class='t_justify' data-translate-html="tutoria.descripcion1">
				Las tutor&iacute;as telem&aacute;ticas le permiten establecer una l&iacute;nea de comunicaci&oacute;n directa con su tutor/a y obtener una respuesta r&aacute;pida a sus dudas.
			</p>

			<br/>

			<p class='t_justify' data-translate-html="tutoria.descripcion2">
				Consulte el horario que ha establecido su tutor/a para celebrar la tutor&iacute;a general del curso o compruebe que su cita de tutor&iacute;a privada ha sido reservada en la fecha en que usted la solicit&oacute;. Posteriormente acceda al chat en el que se celebran las tutor&iacute;as y participe activamente en la sesi&oacute;n.
			</p>
		</div>
	<?php endif;?>

	<?php
		//Muestro el menu de administracion cuando corresponda
		if(!Usuario::compareProfile(array('supervisor', 'alumno')))
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'menu_tutores');
		}

		//Muestro la vista correspondiente
		if($get['c'] == 'verTut' && isset($get['privadas']) && $get['privadas'] == 1)
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'verTutPriv');
		}
		else if($get['c'] == 'verTut' OR $get['c'] == 'solicitar_speaking')
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'verTut');
		}
		else if($get['c'] == 'insertarTut')
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'insertarTut');
		}
		else if($get['c'] == 'editarTut')
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editarTut');
		}
		else if($get['c'] == 'editarTutPriv')
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editarTutPriv');
		}
		else if($get['c'] == 'asignar_cursos')
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'asignar_cursos');
		}
	?>
</div>