<div class='menu_interno'>
	<span class='elemento_menuinterno'>
		<a href='aula/tutorias' data-translate-html="tutoria.menu.btn1">
			Listado tutor&iacute;as (curso)
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href='aula/tutorias/todas' data-translate-html="tutoria.menu.btn2">
			Tutor&iacute;as generales
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href='aula/tutorias/privadas' data-translate-html="tutoria.menu.btn3">
			Tutor&iacute;as privadas
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href='aula/tutorias/crear' data-translate-html="tutoria.menu.btn4">
			Crear horario tutor&iacute;a
		</a>
	</span>

	<?php if(Usuario::getIdCurso() == 679): ?>
		<span class='elemento_menuinterno'>
			<a href='aula/tutorias/speaking' data-translate-html="tutoria.menu.speaking">
				Speaking
			</a>
		</span>
	<?php endif; ?>
</div>