<div id="anuncios">
	<div class='subtitleModulo'>
		<span data-translate-html="tutoria.nueva">
			Nueva tutor&iacute;a
		</span>
	</div>

	<div class='caja_frm'>
		<br/>
		<form id="frmInsertTutoria" name='' method='post' action=''>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="tutoria.dia_semana">
					D&iacute;a de la semana
				</div>

				<div class='campofrm'>
					<span>
						<input type='checkbox' name='dia_semanaf[]' value='1' />
						&nbsp;
						<span data-translate-html="dias.lunes">
							Lunes
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='2' />
						&nbsp;
						<span data-translate-html="dias.martes">
							Martes
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='3' />
						&nbsp;
						<span data-translate-html="dias.miercoles">
							Mi&eacute;rcoles
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='4' />
						&nbsp;
						<span data-translate-html="dias.jueves">
							Jueves
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='5' />
						&nbsp;
						<span data-translate-html="dias.viernes">
							Viernes
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='6' />
						&nbsp;
						<span data-translate-html="dias.sabado">
							S&aacute;bado
						</span>
					</span>

					<span>
						<input type='checkbox' name='dia_semanaf[]' value='7' />
						&nbsp;
						<span data-translate-html="dias.domingo">
							domingo
						</span>
					</span>	

				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="tutoria.horai">
					Hora Inicio:
				</div>

				<div class='campofrm'>
					<select name='hora_inicio'>
						<?php $mi_tutoria->horas();?>	
					</select>

					<span> : </span>

					<select name='minuto_inicio'>
						<?php $mi_tutoria->minutos();?>
					</select>
				</div>

				<div class='clear'></div>

			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="tutoria.horaf">
					Hora Fin:
				</div>

				<div class='campofrm'>
					<select name='hora_fin'>
						<?php $mi_tutoria->horas();?>
					</select>
					
					<span> : </span>

					<select name='minuto_fin'>
						<?php $mi_tutoria->minutos();?>
					</select>
				</div>

				<div class='clear'></div>

			</div>

			<div class='filafrm'>

				<div class='etiquetafrm' data-translate-html="tutoria.privada">
					Tutor&iacute;a privada
				</div>

				<div class='campofrm'>
					<input type="checkbox" name="tutoria_privada" value="1" />
				</div>
			</div>

			<div id="frmInsertTutoriaAlumnos" class='filafrm hide'>

				<div class='etiquetafrm' data-translate-html="general.alumno">
					Alumno
				</div>

				<div class='campofrm'>
					<select name="idalumno">
						<option value="0" data-translate-html="tutoria.sel_alumno">- Selecciona un alumno -</option>

						<?php if($alumnos->num_rows > 0):?>
							<?php while($alumno = $alumnos->fetch_object()):?>
								<option value="<?php echo $alumno->idalumnos;?>">
									<?php echo Texto::textoPlano($alumno->nombrec);?>
								</option>
							<?php endwhile;?>
						<?php endif;?>

					</select>
				</div>

			</div>

			<div>
				<input type='submit' value='Guardar' class="width100" data-translate-html="formulario.guardar"/>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('#frmInsertTutoria input[name="tutoria_privada"]').change(function()
	{
		if($(this).attr('checked') != undefined)
		{
			$('#frmInsertTutoriaAlumnos').removeClass('hide');
		}
		else
		{
			$('#frmInsertTutoriaAlumnos').addClass('hide');
		}
	})
</script>