 <div id="tutoria" class='caja_frm'>
	<div class='subtitleModulo'>

		<span data-translate-html="tutoria.asignar_curso">
			Asignar cursos:
		</span> 

		<?php echo $diasemana[$tutoria->dia_semana - 1];?>, <?php echo date('H:i', strtotime($tutoria->hora_inicio, true))?>

		<span data-translate-html="general.hasta">
			hasta
		</span> 

		<?php echo date('H:i', strtotime($tutoria->hora_fin, true))?>
	</div>
	
	<?php if($row_cursos->num_rows > 0):?>
		<form id="frm_tutoria_asignar_cursos" method="post" action="">
			<div id="frm_tutoria_asignar_cursos_options" class="fright">
				<ul>
					<li>
						<a href="#" onclick="ModuloTutoria.seleccionarTodo();" title="" data-translate-html="general.seleccionar">
							Seleccionar todo
						</a>
					</li>

					<li>/</li>

					<li>
						<a href="#" onclick="ModuloTutoria.deseleccionarTodo();" title="" data-translate-html="general.deseleccionar">
							Deseleccionar todo
						</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			
			<div id="frm_tutoria_asignar_cursos_scroll">
				<ul>
					<?php while($curso = $row_cursos->fetch_object()):?>
						<li>
							<?php if(in_array($curso->idcurso, $cursos_asignados_solo_id)):?>
								<input id="tutoria_cursos_<?php echo $curso->idcurso?>"
									type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>"
									value="1" checked="checked" />
							<?php else:?>
								<input id="tutoria_cursos_<?php echo $curso->idcurso?>"
									type="checkbox" name="cursos[]" value="<?php echo $curso->idcurso?>"
									value="1" />
							<?php endif;?>
							<label for="tutoria_cursos_<?php echo $curso->idcurso?>">
								<?php echo Texto::textoPlano($curso->titulo)?></label>
						</li>
					<?php endwhile;?>
				</ul>
			</div>
			<ul>
				<li class="t_right clear">
					<button type="submit" data-translate-html="tutoria.asignar_curso">
						Asignar cursos
					</button>
				</li>
			</ul>
		</form>
	<?php else:?>
		<strong data-translate-html="tutoria.no_cursos">
			No hay cursos a los que asignar
		</strong>
	<?php endif;?>
	
	<div id="paginaVolver" class='fleft'>
		<a href="aula/tutorias" title="" data-translate-html="general.volver">
			Volver
		</a>
	</div>
</div>