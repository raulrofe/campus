<div class='caja_separada'>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class="popupIntro">
			<!-- <h2>TUTOR&Iacute;AS</h2> -->
			<div class='subtitle t_center'>
				<img src="imagenes/tutorias/tutorias.png" alt="" style="vertical-align:middle;"/>
				<span>Speaking</span>
			</div>
			<br/>
			<p class='t_justify'>
				El Speaking le permiten establecer una l&iacute;nea de comunicaci&oacute;n directa con el tutor para que pueda ser evluado sobre su nivel hablado.
			</p>
			<br/>
			<p class='t_justify'>
				Seleccione una fecha y hora disponible (La duración del Speakin es de 15 minutos)
			</p>
			<br/>
		</div>
	<?php else:?>
		<div class="popupIntro">
			<!-- <h2>TUTOR&Iacute;AS</h2> -->
			<div class='subtitle t_center'>
				<img src="imagenes/tutorias/tutorias.png" alt="" style="vertical-align:middle;"/>
				<span>Speaking</span>
			</div>
			<br/>
			<p class='t_justify'>
				El Speaking le permiten establecer una l&iacute;nea de comunicaci&oacute;n directa con el tu tutor para que pueda ser evluado sobre su nivel hablado.
			</p>
			<br/>
			<p class='t_justify'>
				Seleccione una fecha y hora disponible (La duración del Speakin es de 15 minutos)
			</p>
			<br/>
		</div>
	<?php endif;?>

	<div id="anuncios">
		<div class='subtitleModulo'><span>Horas Speaking disponibles</span></div>
		<div class='caja_frm'>
			<br/>
			<form id="frmInsertSpeaking" name='frmInsertSpeaking' method='post' action='aula/tutorias/solicitar-speaking'>
				<div class='filafrm'>
					<div class='etiquetafrm'>D&iacute;a de la semana</div>
					<div class='campofrm'>
						<select name="diaSemana">
							<option value="2016-10-18">18/10/2016</option>
							<option value="2016-10-25">25/10/2016</option>
							<option value="2016-11-01">01/11/2016</option>
							<option value="2016-11-08">08/11/2016</option>
							<option value="2016-11-15">15/11/2016</option>
							<option value="2016-11-22">22/11/2016</option>
						</select>
					</div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Hora Inicio:</div>
					<div class='campofrm' id="selectHoras">
						<select name='horaInicio'>
							<option value="14:00">14:00</option>
							<option value="14:15">14:15</option>
							<option value="14:30">14:30</option>
							<option value="14:45">14:45</option>
							<option value="15:00">15:00</option>
							<option value="15:15">15:15</option>
							<option value="15:30">15:30</option>
							<option value="15:45">15:45</option>
						</select>
					</div>
					<div class='clear'></div>
				</div>
				<div class='filafrm'>
					<div class='etiquetafrm'>Elige tema </div>
					<div class='campofrm'>
						<select name='tema'>
							<option value="1">Unit 1 - Describe what you do every morning.</option>
							<option value="2">Unit 1 - Advantages and disadvantages of living in a big city.</option>
							<option value="3">Unit 1 - Do and don’t ……at work.</option>
							<option value="4">Unit 2 - What was school like 20 years ago? Discuss.</option>
							<option value="5">Unit 2 - Where were you when ……? Tell me about something famous that happened and what you were doing at that time.</option>
							<option value="6">Unit 3 - Tell me about your career (e.g. I have been teaching for 10 years).</option>
							<option value="7">Unit 3 - Tell me about your bucket list. What are your ideas? (use I have never….)</option>
							<option value="8">Unit 3 - What are your reasons to learn English?</option>
							<option value="9">Unit 4 - Can you live without the Internet? Why/Why not?</option>
							<option value="10">Unit 4 - Advantages and disadvantages of technology.</option>
							<option value="11">Unit 4 - Are you good at lodging a complaint? Describe a situation where you bought something and you were not satisfied with it.</option>
							<option value="12">Unit 5 - Do you recycle? Do you find some problems when recycling in your city?</option>
							<option value="13">Unit 5 - Do you use public transport? How do you go to work?</option>
							<option value="14">Unit 6 - Tell me about your last holiday. Did you go somewhere interesting?</option>
							<option value="15">Unit 6 - Which place would you like to visit someday? Why?</option>
							<option value="16">Unit 7 - What do you do to be healthy? Do you eat well? Do you play/do any sports?</option>
							<option value="17">Unit 7 - What can we do to teach children to eat well? </option>
							<option value="18">Unit 8 - Do you have any friendship you have kept up for ages?</option>
							<option value="19">Unit 8 - Does love at first sight exist? Why? Why not?</option>
							<option value="20">Unit 9 - What is the last movie you have seen/ book you have read/ CD you have bought? Did you like it? Why? Why not?</option>
							<option value="21">Unit 9 - What do you think about modern art?</option>
							<option value="22">Unit 10 - Have you ever driven in a foreign country? What was it like? Did you mind doing it?</option>
							<option value="23">Unit 10 - Are you afraid of flying? Which is your favourite means of transport?</option>
						</select>
					</div>
					<div class='clear'></div>
				</div>
				<div><input type='submit' value='Guardar' class="width100"/></div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	// Devolvemos la petición ajax
	$("select[name=diaSemana]").on("change", function(){
		var dia = $(this).val();
		getHorasSpeaking(dia);
	});

	getHorasSpeaking('2016-10-18');

	function getHorasSpeaking(dia){
		$.ajax({
			type: 'GET',
			url: 'http://campusaulainteractiva.aulasmart.net/obtenerspeakings/' + dia,
			dataType: 'text',
		})
		.done(function (data) {
			$('#selectHoras').html(data);
		})
		.fail(function (data) {
			console.log(data);
		})
	}

</script>