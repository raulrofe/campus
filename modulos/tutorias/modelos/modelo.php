<?php
class Tutoria extends modeloExtend {


	public function __construct()
	{
		parent::__construct();

		// lo que sea
	}

	public function set_tutoria()
	{
		$get = Peticion::obtenerGet();

		if(isset($get['idtutoria']) && is_numeric($get['idtutoria']))
		{
			$this->idtutoria = $get['idtutoria'];
		}
	}

	public function dia_semana($dia_semana = 1){

		$selected1 = null;
		$selected2 = null;
		$selected3 = null;
		$selected4 = null;
		$selected5 = null;
		$selected6 = null;
		$selected7 = null;
		switch($dia_semana)
		{
			case 2:
				$selected2 = 'selected';
				break;
			case 3:
				$selected3 = 'selected';
				break;
			case 4:
				$selected4 = 'selected';
				break;
			case 5:
				$selected5 = 'selected';
				break;
			case 6:
				$selected6 = 'selected';
				break;
			case 7:
				$selected7 = 'selected';
				break;
			default:
				$selected1 = 'selected';
				break;
		}

		echo "<select name='dia_semanaf'>
				<option value='1' $selected1>Lunes</option>
				<option value='2' $selected2>Martes</option>
				<option value='3' $selected3>Mi&eacute;rcoles</option>
				<option value='4' $selected4>Jueves</option>
				<option value='5' $selected5>Viernes</option>
				<option value='6' $selected6>S&aacute;bado</option>
				<option value='7' $selected7>Domingo</option>
			 </select>";
	}

	public function horas($hora = 0){

		$hora_selected = array_fill(0, 24, 0);
		$hora_selected[intval($hora)] = 'selected';

		echo "<option value='00' $hora_selected[0]>00</option>
			<option value='01' $hora_selected[1]>01</option>
			<option value='02' $hora_selected[2]>02</option>
			<option value='03' $hora_selected[3]>03</option>
			<option value='04' $hora_selected[4]>04</option>
			<option value='05' $hora_selected[5]>05</option>
			<option value='06' $hora_selected[6]>06</option>
			<option value='07' $hora_selected[7]>07</option>
			<option value='08' $hora_selected[8]>08</option>
			<option value='09' $hora_selected[9]>09</option>
			<option value='10' $hora_selected[10]>10</option>
			<option value='11' $hora_selected[11]>11</option>
			<option value='12' $hora_selected[12]>12</option>
			<option value='13' $hora_selected[13]>13</option>
			<option value='14' $hora_selected[14]>14</option>
			<option value='15' $hora_selected[15]>15</option>
			<option value='16' $hora_selected[16]>16</option>
			<option value='17' $hora_selected[17]>17</option>
			<option value='18' $hora_selected[18]>18</option>
			<option value='19' $hora_selected[19]>19</option>
			<option value='20' $hora_selected[20]>20</option>
			<option value='21' $hora_selected[21]>21</option>
			<option value='22' $hora_selected[22]>22</option>
			<option value='23' $hora_selected[23]>23</option>";
	}

	public function minutos($minutos = 0){

		$minutos_selected = array(0 => null, 10 => null, 20 => null, 30 => null, 40 => null, 50 => null);
		$minutos_selected[intval($minutos) - 1] = 'selected';

		echo "<option value='00' $minutos_selected[0]>00</option>
			<option value='10' $minutos_selected[10]>10</option>
			<option value='20' $minutos_selected[20]>20</option>
			<option value='30' $minutos_selected[30]>30</option>
			<option value='40' $minutos_selected[40]>40</option>
			<option value='50' $minutos_selected[50]>50</option>";
	}

	public function ver_tutorias(){
		$sql = "SELECT t.idtutoria, t.dia_semana, t.hora_inicio, t.hora_fin from tutoria AS t" .
		" LEFT JOIN tutoria_cursos AS tc ON tc.idtutoria = t.idtutoria" .
		" WHERE 1=1 OR tc.idcurso = " . Usuario::getIdCurso() . " GROUP BY t.idtutoria";
		$resultado = $this->consultaSql($sql);
		//$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}

	public function ver_tutorias_privadas(){
		$sql = "SELECT tp.idtutoria_priv, tp.dia_semana, tp.hora_inicio, tp.hora_fin, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec from tutoria_privada AS tp" .
		" LEFT JOIN alumnos AS a ON a.idalumnos = tp.idalumnos" .
		" GROUP BY tp.idtutoria_priv";
		$resultado = $this->consultaSql($sql);
		//$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}

	public function obtenerTutoriaPrivada($idTutoriaPriv){
		$sql = "SELECT tp.idtutoria_priv, tp.dia_semana, tp.hora_inicio, tp.hora_fin, a.idalumnos, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec from tutoria_privada AS tp" .
		" LEFT JOIN alumnos AS a ON a.idalumnos = tp.idalumnos" .
		" WHERE idtutoria_priv = " . $idTutoriaPriv .
		" GROUP BY tp.idtutoria_priv";
		$resultado = $this->consultaSql($sql);
		//$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}
	/*
		public function eliminar_tutoria(){
		$sql = "DELETE from tutoria where idtutoria = ".$this->idtutoria;
		$resultado = $this->consultaSql($sql);
		//echo $sql;
		//mysqli_query($this->con,$sql);
	}
	*/

	public function obtenerTutoria($idTutoria)
	{
		$sql = 'SELECT * FROM tutoria As t WHERE t.idtutoria = ' . $idTutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCursosAsignados($idtutoria)
	{
		$sql = 'SELECT c.idcurso, c.titulo FROM tutoria_cursos AS tc' .
		' LEFT JOIN curso AS c ON c.idcurso = tc.idcurso' .
		' WHERE tc.idtutoria = ' . $idtutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCursos()
	{
		$sql = 'SELECT c.idcurso, c.titulo FROM curso AS c';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function asignar_curso($idtutoria, $idcurso)
	{
		$sql = 'INSERT INTO tutoria_cursos (idtutoria, idcurso) VALUES (' . $idtutoria . ', ' . $idcurso . ')';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminar_tutoria_curso($idtutoria, $idcurso)
	{
		$sql = 'DELETE FROM tutoria_cursos WHERE idcurso = ' . $idcurso . ' AND idtutoria = ' . $idtutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminar_todos_cursos_asignados($idtutoria)
	{
		$sql = 'DELETE FROM tutoria_cursos WHERE idtutoria = ' . $idtutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminar_tutoria($idtutoria)
	{
		$sql = 'DELETE FROM tutoria WHERE idtutoria = ' . $idtutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminar_tutoria_priv($idtutoria)
	{
		$sql = 'DELETE FROM tutoria_privada WHERE idtutoria_priv = ' . $idtutoria;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTutoriaCurso($idtutoria, $idcurso)
	{
		$sql = 'SELECT * FROM tutoria_cursos WHERE idtutoria = ' . $idtutoria . ' AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTodasTutoriasCurso($idcurso)
	{
		$sql = "SELECT t.idtutoria, t.dia_semana, t.hora_inicio, t.hora_fin from tutoria AS t
		LEFT JOIN tutoria_cursos AS tc ON tc.idtutoria = t.idtutoria WHERE tc.idcurso = " . Usuario::getIdCurso();
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizar_tutoria($id_tutoria, $dia_semana, $hora_inicio, $hora_fin)
	{
		$sql = 'SELECT idtutoria from tutoria where dia_semana = "'.$dia_semana.'" and hora_inicio = "'.$hora_inicio.'" and hora_fin = "'.$hora_fin.'"';
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = 'UPDATE tutoria SET dia_semana="' . $dia_semana . '", hora_inicio="' . $hora_inicio . '", hora_fin="' . $hora_fin . '"' .
					' WHERE idtutoria=' . $id_tutoria;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
		else
		{
			Alerta::mostrarMensajeInfo('yaexistetutoria','Ya existe una tutoria igual');
		}
	}

	public function actualizar_tutoria_priv($id_tutoria, $idalumno, $dia_semana, $hora_inicio, $hora_fin)
	{
		$sql = 'SELECT idtutoria_priv from tutoria_privada where idalumnos="' . $idalumno . '" AND dia_semana = "'.$dia_semana.'" and hora_inicio = "'.$hora_inicio.'" and hora_fin = "'.$hora_fin.'"';
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = 'UPDATE tutoria_privada SET idalumnos=' .$idalumno . ', dia_semana="' . $dia_semana . '", hora_inicio="' . $hora_inicio . '", hora_fin="' . $hora_fin . '"' .
					' WHERE idtutoria_priv=' . $id_tutoria;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
		else
		{
			Alerta::mostrarMensajeInfo('yaexistetutoria','Ya existe una tutoria igual');
		}
	}

	public function insertar_tutoriag($dia_semanaf,$hora_inicio,$hora_fin,$minuto_inicio,$minuto_fin){
		$bien = 0;
		$return = false;

		// Compromabamos si la hora de inicio es anterior a la hora de fin
		$hora_inicio = $hora_inicio.":".$minuto_inicio.":00";
		$hora_fin = $hora_fin.":".$minuto_fin.":00";
		if($hora_inicio < $hora_fin)
		{
			$numeroDias = count($dia_semanaf);

			for($i=0;$i<=$numeroDias-1;$i++)
			{
				if(is_numeric($dia_semanaf[$i]) && $dia_semanaf[$i]>0 && $dia_semanaf[$i]<=7)
				{
					$resultado = $this->obtenerTutoriaPorFecha($dia_semanaf[$i], $hora_inicio, $hora_fin);
					if(mysqli_num_rows($resultado) == 0)
					{
						$sql = "INSERT into tutoria (dia_semana,hora_inicio,hora_fin)
						VALUES ('".$dia_semanaf[$i]."','$hora_inicio','$hora_fin')";
						if($this->consultaSql($sql))
						{
							$bien++;
						}
					}
					else
					{
						Alerta::guardarMensajeInfo('tutoriaexiste','Ya existe una tutoría con esta misma fecha y horario');
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('dianovalido','El día de la semana no es válido');
				}
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('horafinmayor','La hora de inicio de la tutoría deber ser anterior a la hora de fin');
		}

		if($bien == $numeroDias)
		{
			$return = true;
		}
		else
		{
			$return = false;
		}
		return $return;
	}

	public function insertar_tutoria_privada($idalumnos,$dia_semanaf,$hora_inicio,$hora_fin,$minuto_inicio,$minuto_fin){
		$bien = 0;
		$return = false;

		// Compromabamos si la hora de inicio es anterior a la hora de fin
		$hora_inicio = $hora_inicio.":".$minuto_inicio.":00";
		$hora_fin = $hora_fin.":".$minuto_fin.":00";
		if($hora_inicio < $hora_fin)
		{
			$numeroDias = count($dia_semanaf);

			for($i=0;$i<=$numeroDias-1;$i++)
			{
				if(is_numeric($dia_semanaf[$i]) && $dia_semanaf[$i]>0 && $dia_semanaf[$i]<=7)
				{
					$sql = "INSERT into tutoria_privada (idalumnos, dia_semana,hora_inicio,hora_fin)
						VALUES ($idalumnos, '".$dia_semanaf[$i]."','$hora_inicio','$hora_fin')";

					if($this->consultaSql($sql))
					{
						$bien++;
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('dianovalido','El día de la semana no es válido');
				}
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('horafinmayor','La hora de inicio de la tutor&iacute;a deber ser anterior a la hora de fin');
		}

		if($bien == $numeroDias)
		{
			$return = true;
		}
		else
		{
			$return = false;
		}
		return $return;
	}

	private function obtenerTutoriaPorFecha($dia_semana, $hora_inicio, $hora_fin)
	{
		$sql = "SELECT * from tutoria AS t LEFT JOIN tutoria_cursos As tc ON tc.idtutoria = t.idtutoria where
			dia_semana = '".$dia_semana."'
			and hora_inicio = '".$hora_inicio."'
			and hora_fin = '".$hora_fin."'";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnosCurso($idcurso)
	{
		$sql = "SELECT a.idalumnos, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec FROM alumnos AS a
		LEFT JOIN matricula AS m ON m.idalumnos = a.idalumnos
		WHERE m.idcurso = ".$idcurso." AND a.borrado = 0
		GROUP BY a.idalumnos";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/********************
	SPEAKING
	********************/


	public function obtenerSpeakings($idcurso)
	{
		$sql = "SELECT * FROM speaking  as spk " .
			   " LEFT JOIN alumnos as al ON al.idalumnos = spk.idusuario " .
			   " LEFT JOIN curso as cu ON cu.idcurso = spk.idcurso " .
			   " WHERE spk.idcurso = " . $idcurso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerSpeakingsPorDia($idcurso, $dia)
	{
		$sql = "SELECT * FROM speaking WHERE idcurso = '" . $idcurso . "' AND dia = '" . $dia . "'";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerSpeakingsPorUsuario($idcurso, $idusuario)
	{
		$sql = "SELECT * FROM speaking WHERE idcurso = '" . $idcurso. "' AND idusuario = '" . $idusuario . "'";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function insertar_speaking($diaSemana, $horaInicio,  $tema, $idUsuario, $idCurso){
		$sql = "INSERT into speaking (dia, hora, tema, idusuario, idcurso)
				VALUES ('" . $diaSemana . "', '" . $horaInicio . "', '" . $tema . "', '" . $idUsuario . "', '" . $idCurso . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
}
?>