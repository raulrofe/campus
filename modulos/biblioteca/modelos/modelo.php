<?php
class ModeloBiblioteca extends modeloExtend
{
	public function insertarAportacion($idUsuario, $idCurso, $titulo, $idTematica, $descripcion, $etiquetas, $idTipoAportacion)
	{
		$sql = 'INSERT INTO biblioteca (idusuario, idcurso, titulo, id_tematica, descripcion, etiquetas, id_tipo_aportacion)' .
		' VALUES ("' . $idUsuario . '", ' . $idCurso . ', "' . $titulo . '", ' . $idTematica . ', "' . $descripcion . '", "' . $etiquetas . '", ' . $idTipoAportacion . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarFichero($idAportacion, $nombreFichero)
	{
		$sql = 'INSERT INTO biblioteca_ficheros (id_aportacion, nombre_fichero) VALUES (' . $idAportacion . ', "' . $nombreFichero . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	public function insertarAutor($idAportacion, $autor)
	{
		$sql = 'INSERT INTO biblioteca_autor (id_aportacion, autor) VALUES (' . $idAportacion . ', "' . $autor . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function mostrarTodo()
	{
		$sql = 'SELECT b.id_aportacion, b.id_tipo_aportacion, b.titulo, ba.autor, b.id_tematica, bt.nombre AS nombreTematica, b.etiquetas, b.descripcion, bf.nombre_fichero, bt.idtematica, 
			rh.nombrec AS nombreUsuarioRH, CONCAT(al.nombre, " ", al.apellidos) AS nombreUsuarioAL, c.titulo AS tituloCurso FROM biblioteca AS b' .
		' LEFT JOIN biblioteca_autor AS ba ON ba.id_aportacion = b.id_aportacion' .
		' LEFT JOIN biblioteca_tematica AS bt ON bt.idtematica = b.id_tematica' .
		' LEFT JOIN biblioteca_ficheros AS bf ON bf.id_aportacion = b.id_aportacion' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = b.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = b.idusuario' .
		' LEFT JOIN curso AS c ON c.idcurso = b.idcurso' .
		' WHERE b.estado = 1';
		//' WHERE (b.titulo LIKE "%' . $titulo . '%" OR ba.autor LIKE "%' . $autor . '%"' .
		//' OR (b.etiquetas LIKE "' . $tag . ',%" OR b.etiquetas LIKE "%,' . $tag . ',%" OR b.etiquetas LIKE "%,' . $tag . '"' .
		//	' OR b.etiquetas LIKE "' . $tag . '"))' .
		//' AND (b.id_tematica = ' . $idTematica . ' AND b.estado = 1)';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTematicas()
	{
		$sql = 'SELECT * FROM biblioteca_tematica';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnaTematica($idtematica)
	{
		$sql = 'SELECT * FROM biblioteca_tematica WHERE idtematica = ' . $idtematica;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAportacionesPendientes()
	{
		$sql = 'SELECT b.id_aportacion, b.titulo, b.id_tipo_aportacion, ba.autor, b.id_tematica,
			bt.nombre AS nombreTematica, b.etiquetas, b.descripcion, bf.nombre_fichero
			FROM biblioteca AS b' .
		' LEFT JOIN biblioteca_autor AS ba ON ba.id_aportacion = b.id_aportacion' .
		' LEFT JOIN biblioteca_tematica AS bt ON bt.idtematica = b.id_tematica' .
		' LEFT JOIN biblioteca_ficheros AS bf ON bf.id_aportacion = b.id_aportacion' .
		' WHERE estado = 2';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumAportacionesPendientes()
	{
		$sql = 'SELECT COUNT(id_aportacion) AS num FROM biblioteca' .
		' WHERE estado = 2';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumAportacionesPorAlumno($idAlumnos, $idCurso)
	{
		$sql = 'SELECT COUNT(id_aportacion) AS num FROM biblioteca' .
		' WHERE idusuario="' . $idAlumnos . '" AND idcurso=' . $idCurso . ' AND estado = 1';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnaAportacionPendientes($idaportacion, $estado)
	{
		if(is_array($estado))
		{
			$addQuery = '';
			foreach($estado as $key => $item)
			{
				if($key > 0)
				{
					$addQuery .= ' OR ';
				}
				
				$addQuery .= 'b.estado = ' . $item;
			}
		}
		else
		{
			$addQuery = 'b.estado = ' . $estado;
		}
		
		$sql = 'SELECT b.id_aportacion, b.titulo, ba.autor, b.id_tematica, bt.nombre AS nombreTematica, b.etiquetas, b.descripcion, bf.nombre_fichero FROM biblioteca AS b' .
		' LEFT JOIN biblioteca_autor AS ba ON ba.id_aportacion = b.id_aportacion' .
		' LEFT JOIN biblioteca_tematica AS bt ON bt.idtematica = b.id_tematica' .
		' LEFT JOIN biblioteca_ficheros AS bf ON bf.id_aportacion = b.id_aportacion' .
		' WHERE (' . $addQuery . ') AND b.id_aportacion = ' . $idaportacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function revisarAportacionPendientes($idaportacion, $estado)
	{
		$sql = 'UPDATE biblioteca SET estado = ' . $estado . ' WHERE estado = 2 AND id_aportacion = ' . $idaportacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}