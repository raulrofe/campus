<?php
$get = Peticion::obtenerGet();

require 'configMimes.php';

if(Usuario::compareProfile(array('tutor', 'coordinador')))
{
	$estadoAportacion = array(1, 2);
}
else
{
	$estadoAportacion = 1;
}

$idusuario = Usuario::getIdUser(true);

if(isset($get['idaportacion']) && is_numeric($get['idaportacion']))
{
	$modeloBiblioteca = new ModeloBiblioteca();

	$rowAdjunto = $modeloBiblioteca->obtenerUnaAportacionPendientes($get['idaportacion'], $estadoAportacion);
	if($rowAdjunto->num_rows == 1)
	{
		$rowAdjunto = $rowAdjunto->fetch_object();

		if(isset($rowAdjunto->nombre_fichero))
		{
			$filename = 'archivos/biblioteca/' . $rowAdjunto->nombre_fichero;

			$extension = Fichero::obtenerExtension($rowAdjunto->nombre_fichero);
			if(file_exists(utf8_decode($filename)) && isset($mimes[$extension]))
			{
				header("Content-disposition: attachment; filename=" . $rowAdjunto->nombre_fichero);
				header("Content-type: " . $mimes[$extension]);
				readfile($filename);
			}
			else
			{
				Url::lanzar404();
			}
		}
		else
		{
			Url::lanzar404();
		}
	}
	else
	{
		Url::lanzar404();
	}
}
else
{
	Url::lanzar404();
}