<?php

$objModelo = new ModeloBiblioteca();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['titulo'], $post['idTematica'], $post['descripcion'], $post['tags'], $_FILES['archivo']['tmp_name'])
		&& !empty($_FILES['archivo']['tmp_name']) && is_uploaded_file($_FILES['archivo']['tmp_name']) && is_numeric($post['idTematica'])
		&& Fichero::validarTipos($_FILES['archivo']['name'], $_FILES['archivo']['type'], array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'odt')))
	{
		if(!empty($post['titulo']))
		{
			// si existe la tematica...
			$rowTematica = $objModelo->obtenerUnaTematica($post['idTematica']);
			if($rowTematica->num_rows == 1)
			{
				$objModelo->insertarAportacion(Usuario::getIdUser(true), Usuario::getIdCurso(), $post['titulo'], $post['idTematica'], $post['descripcion'], $post['tags'], 1);
						
				$nombreArchivo = $objModelo->obtenerUltimoIdInsertado() . '-' . $_FILES['archivo']['name'];
					
				if(move_uploaded_file($_FILES['archivo']['tmp_name'], PATH_ROOT . 'archivos/biblioteca/' . $nombreArchivo))
				{
					$idAportacion = $objModelo->obtenerUltimoIdInsertado();
					if(Usuario::compareProfile(array('tutor', 'coordinador')))
					{
						$resultado = $objModelo->revisarAportacionPendientes($idAportacion, 1);
					}
					
					$objModelo->insertarFichero($idAportacion, $nombreArchivo);
					
					Alerta::guardarMensajeInfo('aportacionmandada','Se ha mandado la aportación');
				}
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('debestitulo','Debes poner un t&iacute;tulo');
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('debesfichero','Debes adjuntar un archivo v&aacute;lido');
	}
}

$tematicas = $objModelo->obtenerTematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevo_fichero');