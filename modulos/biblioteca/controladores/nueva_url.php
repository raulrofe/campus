<?php

$objModelo = new ModeloBiblioteca();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['url'], $post['idTematica'], $post['descripcion'], $post['tags'])
		&& is_numeric($post['idTematica']))
	{
		if(!empty($post['url']))
		{
			if(Url::isValid($post['url']))
			{
				// si existe la tematica...
				$rowTematica = $objModelo->obtenerUnaTematica($post['idTematica']);
				if($rowTematica->num_rows == 1)
				{
					if($objModelo->insertarAportacion(Usuario::getIdUser(true), Usuario::getIdCurso(), $post['url'], $post['idTematica'], $post['descripcion'], $post['tags'], 3))
					{
						if(Usuario::compareProfile(array('tutor', 'coordinador')))
						{
							$resultado = $objModelo->revisarAportacionPendientes($objModelo->obtenerUltimoIdInsertado(), 1);
						}
						
						Alerta::mostrarMensajeInfo('enviadoaportacion',"Se ha enviado la aportaci&oacute;n");
					}
					
				}
			}
			else
			{
				Alerta::mostrarMensajeInfo('webnovalida','La direcci&oacute;n web no es v&aacute;lida');
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('debesweb','Debes introducir una direcci&oacute;n web');
		}
	}
}

$tematicas = $objModelo->obtenerTematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_url');