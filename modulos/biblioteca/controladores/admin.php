<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();
$objModelo = new ModeloBiblioteca();

if(isset($get['option'], $get['idaportacion'], $get['tipo']) && $get['option'] == 'revision'
	&& is_numeric($get['tipo']) && is_numeric($get['idaportacion']))
{
	$rowAportacion = $objModelo->obtenerUnaAportacionPendientes($get['idaportacion'], 2);
	if($rowAportacion->num_rows == 1)
	{
		switch($get['tipo'])
		{
			case 1:
				$resultado = $objModelo->revisarAportacionPendientes($get['idaportacion'], 1);
				$mensajeAlerta = 'Se ha aprobado la aportaci&oacute;n';
				$clave = 'aprobadoaportacion';
				break;
			case 2:
				$resultado = $objModelo->revisarAportacionPendientes($get['idaportacion'], 0);
				$mensajeAlerta = 'Se ha rechazado la aportaci&oacute;n';
				$clave = 'rechazadoaportacion';
				break;
			default:
				$clave = '';
				break;
		}
		
		if(isset($resultado) && $resultado)
		{
			Alerta::mostrarMensajeInfo($clave,$mensajeAlerta);
		}
	}
}

$aportaciones = $objModelo->obtenerAportacionesPendientes();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'admin');