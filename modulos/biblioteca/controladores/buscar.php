<?php
$objModelo = new ModeloBiblioteca();

/*
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['tag'], $post['idTematica'], $post['titulo'], $post['autor'])
		&& is_numeric($post['idTematica']))
	{
		$busqueda = $objModelo->buscar($post['tag'], $post['idTematica'], $post['titulo'], $post['autor']);
	}
}
*/

$busqueda = $objModelo->mostrarTodo();

$tematicas = $objModelo->obtenerTematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'buscar');