<?php

$objModelo = new ModeloBiblioteca();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['titulo_agno'], $post['autor'], $post['idTematica'], $post['descripcion'], $post['tags'])
		&& is_numeric($post['idTematica']))
	{
		if(!empty($post['titulo_agno']))
		{
			// si existe la tematica...
			$rowTematica = $objModelo->obtenerUnaTematica($post['idTematica']);
			if($rowTematica->num_rows == 1)
			{
				if($objModelo->insertarAportacion(Usuario::getIdUser(true), Usuario::getIdCurso(), $post['titulo_agno'], $post['idTematica'], $post['descripcion'], $post['tags'], 2))
				{
					if($objModelo->insertarAutor($objModelo->obtenerUltimoIdInsertado(), $post['autor']))
					{
					if(Usuario::compareProfile(array('tutor', 'coordinador')))
						{
							$resultado = $objModelo->revisarAportacionPendientes($objModelo->obtenerUltimoIdInsertado(), 1);
						}
						
						Alerta::mostrarMensajeInfo('enviadoaportacion',"Se ha enviado la aportaci&oacute;n");
					}
				}
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('introducetituloano','Introduce un t&iacute;tulo y su a&ntilde;o');
		}
	}
}

$tematicas = $objModelo->obtenerTematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_referencia');