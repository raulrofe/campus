<?php

$objModelo = new ModeloBiblioteca();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['termino'], $post['idTematica'], $post['descripcion'], $post['tags'])
		&& is_numeric($post['idTematica']))
	{
		if(!empty($post['termino']))
		{
			// si existe la tematica...
			$rowTematica = $objModelo->obtenerUnaTematica($post['idTematica']);
			if($rowTematica->num_rows == 1)
			{
				if($objModelo->insertarAportacion(Usuario::getIdUser(true), Usuario::getIdCurso(), $post['termino'], $post['idTematica'], $post['descripcion'], $post['tags'], 4))
				{
					if(Usuario::compareProfile(array('tutor', 'coordinador')))
					{
						$resultado = $objModelo->revisarAportacionPendientes($objModelo->obtenerUltimoIdInsertado(), 1);
					}
					
					Alerta::mostrarMensajeInfo('enviadoaportacion',"Se ha enviado la aportaci&oacute;n");
				}
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('introducetermino','Introduce un t&eacute;rmino a la definici&oacute;n');
		}
	}
}

$tematicas = $objModelo->obtenerTematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_definicion');