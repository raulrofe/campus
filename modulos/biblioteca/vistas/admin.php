<div id="biblioteca_admin_aportaciones">

	<div class="subtitle t_center"><span>Biblioteca - Administrador</span></div>
	
	<div>
		<?php if($aportaciones->num_rows > 0):?>
			<?php while($aportacion = $aportaciones->fetch_object()):?>
				<div class="elemento bibliotecaElemento">
					<div class="bibliotecaElementoTitulo">
						<?php echo Texto::textoPlano($aportacion->titulo);?>
						<?php if(isset($aportacion->autor)):?>
							<span class="bibliotecaElementoAutor">- <?php echo Texto::textoPlano($aportacion->autor);?></span>
						<?php endif;?>
					</div>
					<div class="bibliotecaElementoDescripcion"><?php echo Texto::textoFormateado($aportacion->descripcion);?></div>
					<?php if(isset($aportacion->nombre_fichero)):?>
						<div class="bibliotecaElementoFichero">
							<a href="biblioteca/forzar_descarga/<?php echo $aportacion->id_aportacion?>" target="_blank">Decargar archivo</a>
						</div>
					<?php endif;?>
					<div class="fleft">
						<ul class="bibliotecaMenu">
							<li>
								<a href="biblioteca/admin/revisar/1/<?php echo $aportacion->id_aportacion;?>" title="">
									Aprobar
								</a>
							</li>
							<li>-</li>
							<li>
								<a href="#" <?php echo Alerta::alertConfirmOnClick(
											'rechazaraportacion',
											'¿Realmente quiere rechazar esta aportación?',
											'biblioteca/admin/revisar/2/' . $aportacion->id_aportacion)?>
									title="">
										Rechazar
								</a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
					
					<div class="bibliotecaTipo fright">
						<?php if($aportacion->id_tipo_aportacion == 1):?>
							<img src="imagenes/biblioteca/doc.png" title="Fichero" alt="" />
						<?php elseif($aportacion->id_tipo_aportacion == 2):?>
							<img src="imagenes/biblioteca/book.png" title="Referencia bibliogr&aacute;fica" alt="" />
						<?php elseif($aportacion->id_tipo_aportacion == 3):?>
							<img src="imagenes/biblioteca/link.png" title="Direcci&oacute;n web" alt="" />
						<?php elseif($aportacion->id_tipo_aportacion == 4):?>
							<img src="imagenes/biblioteca/question.png" title="Definici&oacute;n" alt="" />
						<?php endif;?>
					</div>
					<div class="fright">
						<?php echo Texto::textoPlano($aportacion->etiquetas);?>
						<b><?php echo Texto::textoPlano($aportacion->nombreTematica);?></b>
					</div>
					<div class="clear"></div>
				</div>
			<?php endwhile;?>
		<?php else: ?>
			<p class='caja_separada burbuja t_center'>No existen nuevas aportaciones</p>
		<?php endif;?>
	</div>
</div>