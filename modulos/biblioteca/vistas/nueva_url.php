<div id="biblioteca_nueva_aportacion">
	<div id="biblioteca_nueva_aportacion_menu" class="menu_interno">

		<div class="subtitle t_center">
			<span data-translate-html="biblioteca.sugerirurl">
				Biblioteca - Sugerir aportaci&oacute;n - Web
			</span>
		</div>

		<div class="popupIntro">
			<p class='t_justify' data-translate-html="biblioteca.sugerirparrafo">
				La biblioteca almacena referencias bibliogr&aacute;ficas, direcciones web, ficheros electr&oacute;nicos y
				definiciones de t&eacute;rminos que son introducidas por el Coordinador/ra en base a las sugerencias aportadas
				por tutes/as y alumnos/as.
			</p>
		</div>
		<div class='menu_interno'>
			<span class='elemento_menuinterno'>
				<a class="menu_interno_activo" href="biblioteca/nuevo/fichero" data-translate-html="biblioteca.fichero">
					Fichero
				</a>
			</span>
			<span class='elemento_menuinterno'>
				<a href="biblioteca/nuevo/referencia" data-translate-html="biblioteca.referencia">
					Referencia bibliogr&aacute;fica
				</a>
			</span>
			<span class='elemento_menuinterno'>
				<a href="biblioteca/nuevo/url" data-translate-html="biblioteca.direccion">
					Direcci&oacute;n web
				</a>
			</span>
			<span class='elemento_menuinterno'>
				<a href="biblioteca/nuevo/definicion" data-translate-html="biblioteca.definicion">
					Definici&oacute;n
				</a>
			</span>
		</div>
		<div class="clear"></div>
	</div>

	<br />

	<form id="frm_biblioteca_nueva_aportacion_tab_url" method="post" action="">
		<ul>
			<li>
				<div class="subtitleModulo" data-translate-html="biblioteca.web.nuevo">
					Nueva direcci&oacute;n web
				</div>
			</li>
			<li>
				<label for="frm_biblioteca_nueva_aportacion_tab_url_web" data-translate-html="biblioteca.web.direccion">
					Direcci&oacute;n web
				</label>
				<input type="text" name="url" />
				<div class="clear"></div>
			</li>
			<li>
				<label for="frm_biblioteca_nueva_aportacion_tab_url_tematica" data-translate-html="biblioteca.tematica">
					Selecciona una tem&aacute;tica
				</label>
				<select id="frm_biblioteca_nueva_aportacion_tab_url_tematica" name="idTematica">
					<?php if ($tematicas->num_rows > 0): ?>
						<?php while ($tematica = $tematicas->fetch_object()): ?>
							<option data-translate-html="biblioteca.selector.<?php echo $tematica->idtematica ?>" value="<?php echo $tematica->idtematica ?>"><?php echo $tematica->nombre ?></option>
						<?php endwhile; ?>
					<?php endif; ?>
				</select>
				<div class="clear"></div>
			</li>
			<li>
				<label class="frm_biblioteca_nueva_aportacion_label_no_format" for="frm_biblioteca_nueva_aportacion_tab_url_description"
					   data-translate-html="biblioteca.redacte">
					Redacte una breve descripci&oacute;n del contenido:
				</label>
				<textarea id="frm_biblioteca_nueva_aportacion_tab_url_descripcion" name="descripcion" rows="1" cols="1"></textarea>
				<span data-translate-html="biblioteca.max">
					* M&aacute;ximo 400 caracteres
				</span>
				<div class="clear"></div>
			</li>
			<li>
				<label class="frm_biblioteca_nueva_aportacion_label_no_format" for="frm_biblioteca_nueva_aportacion_tab_url_tags"
					   data-translate-html="biblioteca.ref">
					Asigne una o varias palabras clave que hagan referencia a la materia sobre la que trata la web
				</label>
				<input id="frm_biblioteca_nueva_aportacion_tab_url_tags" type="text" name="tags" value="" />
				<div class="clear"></div>
			</li>
			<li>
				<button type="submit" data-translate-html="formulario.enviar">
					Enviar
				</button>
			</li>
		</ul>
	</form>
</div>