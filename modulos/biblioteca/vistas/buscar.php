<div id="biblioteca_busqueda">

	<div class="subtitle t_center">
		<span data-translate-html="biblioteca.subtitulo">B&uacute;squeda en el cat&aacute;logo de la biblioteca</span>
	</div>
	<br/>

	<div class="popupIntro">
		<p class='t_justify' data-translate-html="biblioteca.descripcion">
			Puede hacer una b&uacute;squeda por palabra clave, &aacute;reas tem&aacute;ticas, t&iacute;tulo o autor tanto de forma individual como de forma conjunta:
		</p>
	</div>
	<br /><br />

	<div id="listadoBiblioteca">
		<form id="frm_biblioteca_buscar" method="post" action="">
			<ul>
				<li>
					<label data-translate-html="biblioteca.buscar">Buscar por</label>
					<input type="text" name="buscador" value="" />
					<div class="clear"></div>
				</li>
				<li>
					<label data-translate-html="biblioteca.tematica">
						Selecciona una tem&aacute;tica
					</label>
					<select name="idTematica">
						<?php if($tematicas->num_rows > 0):?>
							<?php while($tematica = $tematicas->fetch_object()):?>
								<?php if($tematica->idtematica == $_SESSION['idcurso']): ?>
									<option data-translate-html="biblioteca.selector.<?php echo $tematica->idtematica ?>" value="<?php echo $tematica->idtematica ?>" selected="selected"><?php echo $tematica->nombre?></option>
								<?php else: ?>
									<option data-translate-html="biblioteca.selector.<?php echo $tematica->idtematica ?>" value="<?php echo $tematica->idtematica ?>"><?php echo $tematica->nombre?></option>
								<?php endif; ?>
							<?php endwhile;?>
						<?php endif;?>
					</select>
					<div class="clear"></div>
				</li>
			</ul>
			<div class="clear"></div>
		</form>

		<?php if(isset($busqueda)):?>
			<br /><br />
			<div class="subtitleModulo" data-translate-html="biblioteca.resultados">
				Resultados de la b&uacute;squeda
			</div>
			<?php if($busqueda->num_rows > 0):?>
				<?php while($item = $busqueda->fetch_object()):?>

					<div class="elemento bibliotecaElemento" data-tematica="<?php echo $item->idtematica?>">
						<div class="bibliotecaElementoTitulo">
							<?php echo Texto::textoPlano($item->titulo);?>
							<?php if(isset($item->autor)):?>
								- <span class="bibliotecaElementoAutor"><?php echo Texto::textoPlano($item->autor);?></span>
							<?php endif;?>
						</div>
						<div class="bibliotecaElementoDescripcion"><?php echo Texto::textoFormateado($item->descripcion);?></div>

						<?php if($item->id_tipo_aportacion == 1):?>
							<div class="bibliotecaElementoFichero">
								<a href="biblioteca/forzar_descarga/<?php echo $item->id_aportacion?>" rel="tooltip" title="Archivo: <?php echo Texto::textoPlano($item->nombre_fichero)?>" target="_blank">Descargar archivo</a>
							</div>
						<?php elseif($item->id_tipo_aportacion == 3):?>
							<div class="bibliotecaElementoFichero">
								<a href="<?php echo $item->titulo?>" target="_blank" data-translate-html="biblioteca.enlace">Ir al enlace web</a>
							</div>
						<?php endif;?>

						<div class="fleft bibliotecaElementoEtiqueta" style="margin-top:2px;">
							<b data-translate-html="biblioteca.etiquetas">Etiquetas claves:</b> 
							<span><?php echo Texto::textoPlano($item->etiquetas);?></span>
						</div>

						<div class="bibliotecaTipo fright">
							<?php if($item->id_tipo_aportacion == 1):?>
								<img src="imagenes/biblioteca/doc.png" rel="tooltip" data-translate-title="biblioteca.fichero" title="Fichero" alt="" />
							<?php elseif($item->id_tipo_aportacion == 2):?>
								<img src="imagenes/biblioteca/book.png" rel="tooltip" data-translate-title="biblioteca.referencia" title="Referencia bibliogr&aacute;fica" alt="" />
							<?php elseif($item->id_tipo_aportacion == 3):?>
								<img src="imagenes/biblioteca/link.png" rel="tooltip" data-translate-title="biblioteca.direccion" title="Direcci&oacute;n web" alt="" />
							<?php elseif($item->id_tipo_aportacion == 4):?>
								<img src="imagenes/biblioteca/question.png" rel="tooltip" data-translate-title="biblioteca.definicion" title="Definici&oacute;n" alt="" />
							<?php endif;?>
						</div>
						<div class="fright"><?php echo Texto::textoPlano($item->nombreTematica);?></div>
						<div class="clear"></div>

						<div class="bibliotecaTipo2">
							<div class="fleft">
								<?php
									if(isset($item->nombreUsuarioAL))
									{
										echo Texto::textoPlano($item->nombreUsuarioAL);
									}
									else
									{
										echo Texto::textoPlano($item->nombreUsuarioRH);
									}
								?>
							</div>
							<div class="fright"><?php echo Texto::resume($item->tituloCurso, 40)?></div>
							<div class="clear"></div>
						</div>
					</div>
				<?php endwhile;?>
			<?php else:?>
				<div class="t_center bold" data-translate-html="biblioteca.nocoincidencia">
					No se encontraron coincidencias en la b&uacute;squeda
				</div>
			<?php endif;?>
		<?php endif;?>
	</div>
</div>

<script type="text/javascript" src="js-biblioteca-default.js"></script>