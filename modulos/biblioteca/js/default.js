$(document).ready(function()
{	
	var idTematica = $('#listadoBiblioteca select[name="idTematica"]').val();
	$('#listadoBiblioteca .bibliotecaElemento[data-tematica!="' + idTematica + '"]').addClass('hide');
	
	$('#listadoBiblioteca input[name="buscador"]').keyup(function(event)
	{
		bibliotecaBuscar();
	});
	
	$('#listadoBiblioteca select[name="idTematica"]').change(function(event)
	{
		bibliotecaBuscar();
	});
});

// BUSCADOR DE CURSOS INACTIVOS POR CONVOCATORIA
function bibliotecaBuscar()
{
	var wordSearch = $('#listadoBiblioteca input[name="buscador"]').val();
	var idTematica = $('#listadoBiblioteca select[name="idTematica"]').val();
	
	$('#listadoBiblioteca .bibliotecaElemento').addClass('hide');

	// remove subrayado
	$('#listadoBiblioteca .bibliotecaElemento .subrayadoAmarillo').each(function(index)
	{
		$(this).replaceWith($(this).html());
	});
	
	if(wordSearch != "")
	{
		var inSearch = '';
		$('#listadoBiblioteca .bibliotecaElemento[data-tematica="' + idTematica + '"]').each(function(index)
		{
			inSearch = $(this).find('.bibliotecaElementoTitulo').text()
						+ ' ' + $(this).find('.bibliotecaElementoDescripcion').text()
						+ ' ' + $(this).find('.bibliotecaElementoEtiqueta span').text();
			
			if(inSearch != undefined)
			{
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('(' + wordSearch + ')', 'ig');
				if(inSearch.match(regex))
				{
					$(this).removeClass('hide');
					
					// subraya
					var regex2 = new RegExp('(' + wordSearch + ')', 'ig');
					
					var tituloText = $(this).find('.bibliotecaElementoTitulo').text();
					$(this).find('.bibliotecaElementoTitulo').html(
						tituloText.replace(regex2, '<span class="subrayadoAmarillo">$1</span>')
					);
					
					var descpText = $(this).find('.bibliotecaElementoDescripcion').text();
					$(this).find('.bibliotecaElementoDescripcion').html(
						descpText.replace(regex2, '<span class="subrayadoAmarillo">$1</span>')
					);
					
					var etiquetaText = $(this).find('.bibliotecaElementoEtiqueta span').text();
					$(this).find('.bibliotecaElementoEtiqueta span').html(
						etiquetaText.replace(regex2, '<span class="subrayadoAmarillo">$1</span>')
					);
				}
			}
		});
	}
	else
	{
		$('#listadoBiblioteca .bibliotecaElemento[data-tematica="' + idTematica + '"]').removeClass('hide');
	}
}