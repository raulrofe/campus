<div class='caja_separada'>
	<div class='subtitle'>
		<span data-translate-html="gestioncurso.archivos">
			Actualizar Fichero
		</span>
	</div>
	<div class='updt'>
		<form name='frm_modulo' action='' method='post' enctype='multipart/form-data'>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="gestioncurso.titulo">
					T&iacute;tulo
				</div>
				<div class='campofrm'>
					<input type='text' name='titulo' size='60' value='<?php echo $f['titulo']; ?>' />
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="gestioncurso.prioridad">
					Prioridad
				</div>
				<div class='campofrm'>
					<input type='text' name='prioridad' size='1' value='<?php echo $f['prioridad']; ?>' />
				</div>
			</div>	
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="gestioncurso.descripcion">
					Descripci&oacute;n
				</div>
				<div class='campofrm'>
					<textarea name='descripcion' class='estilotextarea'>
						<?php echo $f['descripcion']; ?>
					</textarea>
				</div>
			</div>
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="gestioncurso.cambiararchivo">
					Cambiar archivo
				</div>

				<div class='campofrm'>
						<input type='file' name='archivo' size='45'/>
				</div>
			</div>

			<div class='campofrm filafrm'>
				<input type='submit' value='Actualizar' data-translate-value="general.actualizar" />
			</div>				

			<input type='hidden' name='idarch' value='<?php echo $f['idarchivo_temario']; ?>' />
		</form>
	</div>	
</div>
