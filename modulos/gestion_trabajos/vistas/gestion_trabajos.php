<div id="autoevaluaciones">
	<div class="t_center">
		<img src="imagenes/trabajos_practicos/trabajos-practicos.png" alt="" class="imageIntro"/>
		<span data-translate-html="gestioncurso.trabajos">
			TRABAJOS PR&Aacute;CTICOS
		</span>
	</div>
	<br/><br/>
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuGestionContenidos">
		<div id="menuTrabajosPracticos" class="blanco redondearBorde" onclick="changeTab(1)">
			<img src="imagenes/trabajos_practicos/trabajos-practicos2.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="gestioncurso.trabajos">
				Trabajos Pr&aacute;cticos
			</p>
		</div>
		<div id="menuAutoevaluaciones" class="redondearBorde" onclick="changeTab(2)">
			<img src="imagenes/mis_progresos/autoevaluaciones.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="gestioncurso.autoevaluaciones">
				Autoevaluaciones
			</p>
		</div>
	</div>
	<!-- Fin menu iconos -->
	
	<!-- Contenido tabs -->
		
	<div class='fleft panelTabContent' id="contenidoAutoevaluaciones">
		<div class="popupIntro">
			<p class='t_justify' data-translate-html="gestioncurso.descripcioninfo">
				Puede introducir nuevas aportaciones y, accediendo a su contenido, realizar modificaciones o eliminar aqu&eacute;llas que han quedado obsoletas para este curso.
			</p> 
		</div>
		
		<!-- este es un buscador fijo -->
			<div class='filafrm'>
				<form name='busca_tipo_curso' action='gestion_trabajos' method='post'>
					<div class='etiquetafrm' data-translate-html="gestioncurso.elige">
						<span data-translate-html="gestioncurso.elige">
							Elije un  m&oacute;dulo del curso:
						</span>
						 &nbsp;&nbsp;
					</div>
					<div class='campofrm'>
						<select name="idmodulo" onchange="this.form.submit()">
							<option value="0" data-translate-html="gestioncurso.seleccione">
								- Seleccione un m&oacute;dulo -
							</option>

							<?php while(!empty($moduloCurso) && $moduloCurso = $modulosCurso->fetch_object()):?>
								<?php if($post['idmodulo'] == $moduloCurso->idmodulo):?>
									<option value="<?php echo $moduloCurso->idmodulo ?>" selected ><?php echo $moduloCurso->nombre ?></option>
								<?php else:?>
									<option value="<?php echo $moduloCurso->idmodulo ?>"><?php echo $moduloCurso->nombre ?></option>
								<?php endif ?>
							<?php endwhile ?>
						</select>
					</div>
				</form>
			</div>
			<br/>
		<!-- fin buscador fijo -->
		<?php if($idmodulo !=0):?>
			<div class='updt'>
				<form name='frm_modulo' action='' method='post' enctype='multipart/form-data'>
					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="gestioncurso.titulo">
							T&iacute;tulo
						</div>
						<div class='campofrm'>
							<input type='text' name='titulo' size='60'/>
						</div>
						<div class='obligatorio'>(*)</div>
					</div>
					<!-- 
					<div class='filafrm'>
						<div class='etiquetafrm'>Categor&iacute;a<a></a>a archivo</div>
						<div class='campofrm'><?php //$mi_modulo->select_categoria_archivos($idcategoria_archivos);?></div>
					</div>	
					 -->
					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="gestioncurso.prioridad">
							Prioridad
						</div>

						<div class='campofrm'>
							<input type='text' name='prioridad' size='1' />
						</div>

					</div>	

					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="gestioncurso.descripcion">
							Descripci&oacute;n
						</div>
						<div class='campofrm'>
							<textarea name='descripcion' class='estilotextarea2'></textarea>
						</div>
					</div>

					<div class='filafrm'>
						<div class='etiquetafrm'>
							<span  data-translate-html="gestioncurso.subir">
								Subir archivo
							</span>
							(*)
						</div>

						<div class='campofrm'>
								<input type='file' name='archivo' size='45'/>
						</div>

					</div>

					<div class='campofrm filafrm'>
						<div class='obligatorio'>
							<span data-translate-html="gestioncurso.obligatorio">
								Campos obligatorios
							</span> 
							(*)
						</div>
					</div>

					<div class='campofrm filafrm'>
						<input type='submit' value='Insertar' data-translate-value="gestioncurso.insertar"></input>
					</div>

					<input type='hidden' name='idmodulo' value='<?php echo $idmodulo; ?>' />
				</form>
			</div>
						
		<!-- simulaci_n de tabla -->
			<div class='view'>
				<div class='header'>
					<div class='itemg1' data-translate-html="gestioncurso.titulo">
						Titulo
					</div>

					<div class='itemp' data-translate-html="gestioncurso.prioridad">
						Prioridad
					</div>

					<div class='itemp' data-translate-html="gestioncurso.tema">
						Tama&ntilde;o
					</div>

					<div class='itemp' data-translate-html="general.editar">
						Editar
					</div>

					<div class='itemp' data-translate-html="gestioncurso.fecha">
						Fecha
					</div>

					<div class='itemp' data-translate-html="gestioncurso.vista">
						Vista
					</div>
				</div>
				<div class='boder'>
				<?php 
					$peso_total = 0;
					if($n_archivos > 0)
					{
						while ($f = mysqli_fetch_assoc($resultado))
						{	
							if(file_exists($f['enlace_archivo']))
							{
								$peso_archivo = filesize($f['enlace_archivo']);
								$peso_archivo = $peso_archivo/1024;
								$peso_archivo = $peso_archivo/1024;
								$peso_archivo = round($peso_archivo,2);
								$peso_total = $peso_total + $peso_archivo;
					?>
								<div class='bod_cont'>
									<div class='itemg1'>
										<a href='<?php echo $f['enlace_archivo'];?>' target='_blank'>
											<?php echo $f['titulo']?>
										</a>
									</div>

									<div class='itemp'>
										<?php echo $f['prioridad']?>
									</div>

									<div class='itemp'>
										<?php echo $peso_archivo;?> Mb
									</div>

									<div class='itemp'>
										<a href='gestion_trabajos/actualizar/<?php echo $f['idarchivo_temario'];?>/<?php echo $idmodulo;?>'>
											<img src='admin/imagenes/editar2.png' alt=''/>
										</a>
									</div>

									<div class='itemp'>
										<?php echo $f['f_creacion'] = Fecha::invertir_fecha($f['f_creacion'],'-','/');?>
									</div>	

									<div class='itemp'>
										<?php 
										if($f['activo'] == 1){echo "<a href='gestion_trabajos/activar/".$f['idarchivo_temario']."/".$idmodulo."'><img src='imagenes/check_ok.png' alt='visible' data-translate-title='gestioncurso.visible'/></a>";}
										else{echo "<a href='gestion_trabajos/activar/".$f['idarchivo_temario']."/".$idmodulo."'><img src='imagenes/check_fail.png' alt='no visible' data-translate-title='gestioncurso.no_visible'/></a>";}
										?>
									</div>	
								</div>
					<?php
							}
							else
							{
								
					?>
								<div class='bod_cont'>
									<div class='itemg1'><?php echo $f['titulo']?></div>
									<div class='itemp'><?php echo $f['prioridad']?></div>
									<div class='itemp'>? Mb</div>
									<div class='itemp'><a href='gestion_trabajos/actualizar/<?php echo $f['idarchivo_temario'];?>/<?php echo $idmodulo;?>'><img src='admin/imagenes/editar2.png' alt=''/></a></div>
									<div class='itemp'><?php echo $f['f_creacion'] = Fecha::invertir_fecha($f['f_creacion'],'-','/');?></div>
									<div class='itemp'>
										<?php 
										if($f['activo'] == 1){echo "<a href='gestion_trabajos/activar/".$f['idarchivo_temario']."/".$idmodulo."'><img src='imagenes/check_ok.png' alt='visible' /></a>";}
										else{echo "<a href='gestion_trabajos/activar/".$f['idarchivo_temario']."/".$idmodulo."'><img src='imagenes/check_fail.png' alt='' /></a>";}
										?>
									</div>	
								</div>
					<?php 
							}
						}
					}
					else
					{
						echo "<span data-translate-html='gestioncurso.no_trabajos'>No existen trabajos pr&aacute;cticos</span>";
					}
					?>		
				</div>
					<div class='foot'>
						<p>
							<?php echo $n_archivos?> 
							<span data-translate-html="gestioncurso.archivos">Archivos</span> | 
							<?php echo $peso_total;?> 
							<span data-translate-html="gestioncurso.mb">Mb</span>
						</p>
					</div>
			</div>
		<?php endif;?>
	</div>
</div>