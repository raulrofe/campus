<?php

$mi_modulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($get['idmodulo'])) $idmodulo = $get['idmodulo'];
if(isset ($post['idmodulo'])) $idmodulo = $post['idmodulo'];
if(isset ($get['idarch'])) $idarch = $get['idarch'];

$mi_modulo->set_modulo($idmodulo);
$mi_modulo->set_archivo($idarch);
	
if(isset($post['titulo'], $post['descripcion'], $post['prioridad']) && is_numeric($post['prioridad']))
{
	if(!empty($post['titulo']))
	{
		if($mi_modulo->actualizar_archivo($post['titulo'],$post['descripcion'], $post['prioridad']))
		{
			Alerta::guardarMensajeInfo('archivoactualizado','Archivo actualizado');
			Url::redirect('gestion_trabajos/'.$idmodulo);
		}
	}
	else
	{
		Alerta::mostrarMensajeInfo('titulobligatorio','Debes poner un título');
	}
}
	
$resultado = $mi_modulo->buscar_archivo();
$f = mysqli_fetch_assoc($resultado);

require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);

