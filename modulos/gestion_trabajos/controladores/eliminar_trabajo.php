<?php

$mi_modulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($get['idmodulo'])) $idmodulo = $get['idmodulo'];
if(isset ($post['idmodulo'])) $idmodulo = $post['idmodulo'];
if(isset ($get['idarch'])) $idarch = $get['idarch'];

$mi_modulo->set_modulo($idmodulo);
$mi_modulo->set_archivo($idarch);


$resultado = $mi_modulo->buscar_archivo();

$eliminado = $mi_modulo->eliminar_archivo($resultado);
if($eliminado)
{
	Alerta::guardarMensajeInfo('archivoeliminado','El archivo se ha eliminado correctamente');
	Url::redirect('gestion_trabajos/'.$idmodulo);
}

Url::redirect('contenidos/ficheros/'.$idmodulo);
