<?php

$mi_modulo = new Modulos();

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset ($get['idmodulo'])) $idmodulo = $get['idmodulo'];
if(isset ($post['idmodulo'])) $idmodulo = $post['idmodulo'];

if(!isset ($idmodulo))
{
	$idmodulo = '0';
}

if(isset($idmodulo))
{
	$mi_modulo->set_modulo($idmodulo);
}

$registros = $mi_modulo->ver_modulos();
	
if(isset($post['titulo']) and !empty($post['titulo']))
{
/*	if(!empty ($_FILES['archivo']['tmp_name']))
	{
		$mi_modulo->insertar_fichero($titulo,$descripcion,$prioridad,2);
		$mi_modulo->ultimo_registro();*/
		if($mi_modulo->guardar_ficheroCurso($post['titulo'], $post['descripcion'], $_SESSION['idcurso'], $post['prioridad'], 2))
		{
			Alerta::mostrarMensajeInfo('trabajoinsertado','Trabajo insertado');
	//	}
	}
	//else Alerta::mostrarMensajeInfo('noexistefichero','No existe fichero');
}
else if(isset($post['titulo']) and empty($post['titulo']))
{
	Alerta::mostrarMensajeInfo('noexistetitulo','No existe titulo');
}

if(isset ($idmodulo))
{
	$mi_modulo->set_categoria_archivo('2');
	$resultado = $mi_modulo->archivos_modulo();
	$n_archivos = mysqli_num_rows($resultado);
	$modulosCurso = $mi_modulo->modulos_asignado_curso();
}

if(!isset($post['idcategoria_archivos'])) $idcategoria_archivos = 2;

require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);