<div id="curriculum">
	<form id="frmCurriculum" method="post" action="">
		<div class='subtitle'><span>Datos personales</span></div>
		<div id="frmCvDatosPersonales" class="fleft">
			<ul>
				<li>
					<label>Nombre</label>
					<input type="text" name="nombre" />
				</li>
				<li>
					<label>NIF <small>(sin guiones)</small></label>
					<input type="text" name="nif" />
				</li>
				<li>
					<label>Direcci&oacute;n</label>
					<input type="text" name="direccion" />
				</li>
				<li>
					<label>Localidad</label>
					<input type="text" name="localidad" />
				</li>
				<li>
					<label>Tel&eacute;fono </label>
					<input type="text" name="telefono" />
				</li>
				<li>
					<label>Correo electr&oacute;nico</label>
					<input type="text" name="email" />
				</li>
				<li>
					<label>N&deg; de hijos</label>
					<input type="text" name="hijos" />
				</li>
				<li>
					<label>Fotograf&iacute;a</label>
					<input type="text" name="foto" />
				</li>
			</ul>
		</div>
		<div id="frmCvDatosPersonales2" class="fleft">
			<ul>
				<li>
					<label>Apellidos</label>
					<input type="text" name="apellidos" />
				</li>
				<li>
					<label>Sexo</label>
					<input type="text" name="sexo" />
				</li>
				<li>
					<label>C&oacute;digo Postal</label>
					<input type="text" name="codigoPostal" />
				</li>
				<li>
					<label>Provincia</label>
					<input type="text" name="provincia" />
				</li>
				<li>
					<label>Tel&eacute;fono M&oacute;vil</label>
					<input type="text" name="movil" />
				</li>
				<li>
					<label>Estado Civil</label>
					<select name='estadoCivil'>
						<option value='soltero'>Soltero/a</option>
						<option value='casado'>Casado/a</option>
						<option value='viudo'>Viudo/a</option>
						<option value='separado'>Separado/a</option>
						<option value='divorciado'>Divorciado/a</option>
					</select>
				</li>
				<li>
					<label>Fecha de nacimiento</label>
					<input type="text" name="fechaNacimiento" />
				</li>
			</ul>
		</div>
		<div class="clear"></div>
	</form>

	<div class='subtitle'><span>Objetivos Personales</span></div>

</div>





<div id="ayuda_faq">
	<div id="ayuda_faq_busqueda">
		<div class="fleft">
			<form method="post" action="">
				<div>
					<table align="center" cellpadding="3" cellspacing="0" border="0">

						<tr><td colspan="2" align="center" class="subtitulo"><hr>Datos Personales<hr></td></tr>
						<tr><td colspan="2" align="center">&nbsp;</td></tr>
						<tr><td colspan="2" align="center">
								<table cellpadding="2" cellspacing="0" border="0" align="center" width="100%">
									<tr valign="middle">
										<td align="right">Nombre (*):</td><td><input type="text" name="nombre" value="" size="35" maxlength="100"></td>
										<td align="right">Apellidos (*):</td><td><input type="text" name="apellidos" value="" size="35" maxlength="200"></td>
									</tr>
									<tr valign="middle">

										<td align="right">N.I.F.<br>(sin espacios ni guiones) (*):</td><td><input type="text" name="nif" value="" size="12" maxlength="50"></td>
										<td align="right">Sexo(*):</td><td><select name="sexo">
												<option value="Hombre" >Hombre</option>
												<option value="Mujer" selected>Mujer</option></select></td>
									</tr>
									<tr valign="middle">

										<td align="right">Direcci&oacute;n (*):</td><td><input type="text" name="direccion" value="" size="35" maxlength="200"></td>
										<td align="right">C&oacute;digo Postal(*):</td><td><input type="text" name="cp" value="" size="10" maxlength="50"></td>
									</tr>
									<tr valign="middle">
										<td align="right">Localidad (*):</td><td><input type="text" name="localidad" value="" size="35" maxlength="100"></td>
										<td align="right">Provincia (*):</td><td><input type="text" name="provincia" value="" size="35" maxlength="100"></td>

									</tr>

									<tr valign="middle">
										<td align="right">Tel&eacute;fono (*):</td><td><input type="text" name="telefono" value="" size="15" maxlength="20"></td>
										<td align="right">Tel&eacute;fono M&oacute;vil:</td><td><input type="text" name="telefono_movil" value="" size="15" maxlength="20"></td>
									</tr>
									<tr valign="middle">
										<td align="right">Correo Electr&oacute;nico (*):</td><td><input type="text" name="email" value="" size="35" maxlength="50"></td>
										<td align="right">Estado Civil:</td><td><select name="estado_civil">

												<option value="Soltero" >Soltero/a</option>
												<option value="Casado" >Casado/a</option>
												<option value="Viudo" >Viudo/a</option>
												<option value="Separado" >Separado/a</option>
												<option value="Divorciado" >Divorciado/a</option>
											</select>

										</td>
									</tr>
									<tr valign="middle">
										<td align="right">N&uacute;mero de hijos:</td><td><input type="text" name="hijos" value="" size="10" maxlength="50"></td>
										<td align="right">Fecha de Nacimiento (*):</td><td><input type="text" name="fnacimiento" value="" size="15" maxlength="10"></td>
									</tr>
									<tr valign="top"><td align="right">Fotograf&iacute;a:</td>

										<td colspan="3" >
											<div>
													<input type='file' name='fotografia'/>
											</div>
										</td>
									</tr>
									<tr valign="middle"><td align="right" colspan="2">¿Desea recibir informaci&oacute;n de PLATAFORMA E-LEARNING?</td><td  colspan="2"><input type="radio" name="info" value="0"  checked>NO&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="info" value="1"  checked>SI</td></tr>
									<tr valign="middle"><td align="left" colspan="2"><b>(*) Campo obligatorio</b></td></tr>

								</table><br>&nbsp;


							</td></tr>
						<tr><td colspan="2" align="center" class="subtitulo"><hr>Objetivos Personales<hr></td></tr>
						<tr><td colspan="2" align="center">&nbsp;</td></tr>

						<tr><td colspan="2" align="center">
								<table cellpadding="2" cellspacing="0" border="0" align="center" width="100%">
									<tr>
										<td align="right">Desear&iacute;a trabajar en ...</td>
										<td><select name="deseo_trabajar">
												<option value="Nacional" >Nacional</option>
												<option value="Europa" >Europa</option>
												<option value="America" >Am&eacute;rica</option>

												<option value="Africa" >&Aacute;frica</option>
												<option value="En cualquier sitio" >En cualquier sitio</option>
											</select>
										</td>
										<td align="right">Tipo de contrato que busca: </td>
										<td><select name="tipo_contrato">
												<option value="Fijo" >Fijo</option>

												<option value="Indefinido" >Indefinido</option>
												<option value="Por obra y servicio" >Por obra y servicio</option>
												<option value="Cualquier tipo" >Cualquier tipo</option>
											</select>
										</td>	
									</tr>
									<tr>
										<td align="right">Sector en el que se quiere trabajar:</td><td><input type="text" name="sector" value="" size="25" maxlength="50"></td>

										<td align="right">Remuneraci&oacute;n m&iacute;nima aceptable:</td>
										<td><select name="remuneracion_minima">
												<option value="Hasta 8000 Euros" >Hasta 8.000 Euros</option>
												<option value="Entre 8.000 y 11.000 Euros" >Entre 8.000 y 11.000 Euros</option>
												<option value="Entre 11.000 y 14.000 Euros" >Entre 11.000 y 14.000 Euros</option>
												<option value="Entre 14.000 y 18.000 Euros" >Entre 14.000 y 18.000 Euros</option>

												<option value="Entre 18.000 y 21.000 Euros" >Entre 18.000 y 21.000 Euros</option>
												<option value="Entre 21.000 y 25.000 Euros" >Entre 21.000 y 25.000 Euros</option>
												<option value="Mas de 25.000 Euros" >M&aacute;s de 25.000 Euros</option>
											</select>
										</td>
									</tr>
									<tr>

										<td align="right">Movilidad geogr&aacute;fica:</td>
										<td>
											<select name="movilidad">
												<option value="Si" >Si</option>
												<option value="No" >No</option>
											</select></td>
										<td align="right">Comentarios sobre la movilidad geogr&aacute;fica:</td>

										<td><textarea name="movilidad_comentarios" rows=5 cols="30"></textarea></td>
									</tr>
									<tr>
									<tr>
										<td align="right">Permiso de conducir:</td><td><input type="radio" name="permiso_conducir" value="0"  checked>NO&nbsp;<input type="radio" name="permiso_conducir" value="1" >SI</td>
										<td align="right">Veh&iacute;culo propio:</td><td><input type="radio" name="vehiculo" value="0"  checked>NO&nbsp;<input type="radio" name="vehiculo" value="1" >SI</td>

									</tr>
									<tr>
										<td colspan="2" align="right">Disponibilidad para viajar:</td>
										<td colspan="2"><input type="radio" name="viajar" value="0"  checked>NO&nbsp;&nbsp;&nbsp;<input type="radio" name="viajar" value="1" >SI</td>
									</tr>
								</table><br>&nbsp;
							</td></tr>
						<tr><td colspan="2" align="center" class="subtitulo"><hr>Formaci&oacute;n<hr></td></tr>

						<tr><td colspan="2" align="center">&nbsp;</td></tr>
						<tr><td colspan="2" align="center">
								<table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">
									<tr><td colspan="4" align="center"><b>Titulaci&oacute;n oficial: </b></td></tr>
									<tr><td colspan="2" align="right" >T&iacute;tulo: <input type="text" size="35" name="tit_nombre1" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2" >Fecha finalizaci&oacute;n estudios:<input type="text" name="tit_fecha1" value=""></td></tr>
									<tr><td colspan="2" align="right">T&iacute;tulo: <input type="text" size="35" name="tit_nombre2" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2">Fecha finalizaci&oacute;n estudios:<input type="text" name="tit_fecha2" value=""></td></tr>
									<tr><td colspan="2" align="right">T&iacute;tulo: <input type="text" size="35" name="tit_nombre3" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2">Fecha finalizaci&oacute;n estudios:<input type="text" name="tit_fecha3" value=""></td></tr>

									<tr><td colspan="4" align="center"><br><b>Formaci&oacute;n Complementaria:</b></td></tr>
									<tr><td colspan="2" align="right">T&iacute;tulo: <input type="text" size="35" name="formacion_nombre1" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2">Fecha finalizaci&oacute;n estudios:<input type="text" name="formacion_fecha1" value=""></td></tr>
									<tr><td colspan="2" align="right">T&iacute;tulo: <input type="text" size="35" name="formacion_nombre2" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2">Fecha finalizaci&oacute;n estudios:<input type="text" name="formacion_fecha2" value=""></td></tr>
									<tr><td colspan="2" align="right">T&iacute;tulo: <input type="text" size="35" name="formacion_nombre3" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan="2">Fecha finalizaci&oacute;n estudios:<input type="text" name="formacion_fecha3" value=""></td></tr>
									<tr><td colspan="4" align="center"><br><b>Otra formaci&oacute;n: </b></td></tr>

									<tr><td colspan="4" align="center"><textarea name="formacion_otra" wrap="auto" rows="3" cols="60"></textarea></td></tr>
									<tr><td colspan="4" align="center"><br><b>Idiomas: </b></td></tr>
									<tr>
										<td align="right">Ingl&eacute;s:</td>
										<td><select name="idioma_ingles">
												<option value="No lo habla, ni lo lee" >No lo habla, ni lo lee</option>
												<option value="Nativo" >Nativo</option>

												<option value="Muy Bien" >Muy Bien</option>
												<option value="Bien" >Bien</option>
												<option value="Regular" >Regular</option>
												<option value="No lo habla, pero lee y escribe" >No lo habla, pero lee y escribe</option>
												<option value="Muy bajo" >Muy bajo</option>
											</select></td>

										<td align="right">Franc&eacute;s:</td>
										<td><select name="idioma_frances">
												<option value="No lo habla, ni lo lee" >No lo habla, ni lo lee</option>
												<option value="Nativo" >Nativo</option>
												<option value="Muy Bien" >Muy Bien</option>
												<option value="Bien" >Bien</option>

												<option value="Regular" >Regular</option>
												<option value="No lo habla, pero lee y escribe" >No lo habla, pero lee y escribe</option>
												<option value="Muy bajo" >Muy bajo</option>
											</select></td>
									</tr>
									<tr>
										<td align="right">Alem&aacute;n:</td>

										<td><select name="idioma_aleman">
												<option value="No lo habla, ni lo lee" >No lo habla, ni lo lee</option>
												<option value="Nativo" >Nativo</option>
												<option value="Muy Bien" >Muy Bien</option>
												<option value="Bien" >Bien</option>
												<option value="Regular" >Regular</option>

												<option value="No lo habla, pero lee y escribe" >No lo habla, pero lee y escribe</option>
												<option value="Muy bajo" >Muy bajo</option>
											</select></td>
										<td align="right">Portugu&eacute;s:</td>
										<td><select name="idioma_portugues">
												<option value="No lo habla, ni lo lee" >No lo habla, ni lo lee</option>
												<option value="Nativo" >Nativo</option>

												<option value="Muy Bien" >Muy Bien</option>
												<option value="Bien" >Bien</option>
												<option value="Regular" >Regular</option>
												<option value="No lo habla, pero lee y escribe" >No lo habla, pero lee y escribe</option>
												<option value="Muy bajo" >Muy bajo</option>
											</select></td>

									</tr>
									<tr><td colspan="4" align="center"><br>Otros idiomas:</td></tr>
									<tr><td colspan="4" align="center"><textarea name="idioma_otro" wrap="auto" rows="3" cols="60"></textarea></td></tr>
									<tr><td colspan="4" align="center"><br><b>Publicaciones: </b></td></tr>
									<tr><td colspan="4" align="center"><textarea name="publicaciones" wrap="auto" rows="5" cols="60"></textarea></td></tr>
								</table><br>&nbsp;
							</td></tr>

						<tr><td colspan="2" align="center" class="subtitulo"><hr>Experiencia Personal<hr></td></tr>
						<tr><td colspan="2" align="center">&nbsp;</td></tr>
						<tr><td colspan="2" align="center">
								<table cellpadding="2" cellspacing="0" border="0" align="center" width="100%">
									<tr><td  colspan="2" align="right">Situaci&oacute;n laboral actual:</td>
										<td colspan="2"><select name="situacion">
												<option value="Desempleado"  selected>Desempleado</option>
												<option value="Empleado" >Empleado</option>

											</select></td></tr>
									<tr><td colspan="4" align="center"><br><b>EMPRESA 1</b></td></tr>
									<tr>
										<td align="right">Nombre de la empresa:</td><td><input type="text" name="empresa_nombre1" value="" size="20" maxlength="200"></td>
										<td align="right">Cargo:</td><td><input type="text" name="empresa_cargo1" value="" size="20" maxlength="100"></td>

									</tr>
									<tr>

										<td align="right">N&uacute;mero de personas a su cargo:</td><td><input type="text" name="empresa_npersonas1" value="" size="10" maxlength="50"></td>
										<td align="right">Funciones:</td><td><input type="text" name="empresa_funciones1" value="" size="20" maxlength="200"></td>

									</tr>
									<tr>
										<td align="right">Fecha inicio:</td><td><input type="text" name="empresa_fechainicio1" value="" size="10" maxlength="10"></td>
										<td align="right">Fecha finalizaci&oacute;n:</td><td><input type="text" name="empresa_fechafin1" value="" size="10" maxlength="10"></td>
									</tr>

									<tr>
										<td align="right">Persona de referencia:</td><td><input type="text" name="empresa_referencia1" value=""></td>
										<td align="right">Comentarios adicionales:</td><td ><input type="text" name="empresa_comentarios1" value=""  size="30" maxlength="500"></td>
									</tr>
									<tr>
										<td colspan="4" align="center"><br><b>EMPRESA 2</b></td></tr>
									<tr>

										<td align="right">Nombre de la empresa:</td><td><input type="text" name="empresa_nombre2" value="" size="20" maxlength="200"></td>
										<td align="right">Cargo:</td><td><input type="text" name="empresa_cargo2" value="" size="20" maxlength="100"></td>
									</tr>
									<tr>
										<td align="right">N&uacute;mero de personas a su cargo:</td><td><input type="text" name="empresa_npersonas2" value="" size="10" maxlength="50"></td>
										<td align="right">Funciones:</td><td><input type="text" name="empresa_funciones2" value="" size="20" maxlength="200"></td>
									</tr>

									<tr>
										<td align="right">Fecha inicio:</td><td><input type="text" name="empresa_fechainicio2" value="" size="10" maxlength="10"></td>
										<td align="right">Fecha finalizaci&oacute;n:</td><td><input type="text" name="empresa_fechafin2" value="" size="10" maxlength="10"></td>
									</tr>
									<tr>
										<td align="right">Persona de referencia:</td><td><input type="text" name="empresa_referencia2" value=""></td>
										<td align="right">Comentarios adicionales:</td><td><input type="text" name="empresa_comentarios2" value=""  size="30" maxlength="500"></td>
									</tr>
									<tr><td colspan="4" align="center"><br><b>EMPRESA 3</b></td></tr>
									<tr>
										<td align="right">Nombre de la empresa:</td><td><input type="text" name="empresa_nombre3" value="" size="20" maxlength="200"></td>
										<td align="right">Cargo:</td><td><input type="text" name="empresa_cargo3" value="" size="20" maxlength="100"></td>
									</tr>
									<tr>

										<td align="right">N&uacute;mero de personas a su cargo:</td><td><input type="text" name="empresa_npersonas3" value="" size="10" maxlength="50"></td>
										<td align="right">Funciones:</td><td><input type="text" name="empresa_funciones3" value="" size="20" maxlength="200"></td>
									</tr>
									<tr>
										<td align="right">Fecha inicio:</td><td><input type="text" name="empresa_fechainicio3" value="" size="10" maxlength="10"></td>
										<td align="right">Fecha finalizaci&oacute;n:</td><td><input type="text" name="empresa_fechafin3" value="" size="10" maxlength="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									</tr>

									<tr>
										<td align="right">Persona de referencia:</td><td><input type="text" name="empresa_referencia3" value=""></td>
										<td align="right">Comentarios adicionales:</td><td><input type="text" name="empresa_comentarios3" value=""  size="30" maxlength="500"></td>
									</tr>
									<tr><td colspan="4" align="center"><br><b>Otras experiencias profesionales: </b></td></tr>
									<tr><td colspan="4" align="center"><textarea name="empresa_otros" cols="60" wrap="auto" rows="5"></textarea></td></tr>
									<tr><td align="right">¿Ha estado alguna vez en desempleo?</td>

										<td><select name="desempleo"><option value="0"  selected>No</option><option value="1" >Si</option></select></td>
										<td align="right">En caso afirmativo, ¿Cu&aacute;nto tiempo tard&oacute; en encontrar trabajo?:</td><td><input type="text" name="tiempo_desempleado" value="" size="20" maxlength="50"></td></tr>
									<tr><td align="center" colspan="4"><br><b>Perfil Profesional:</b></td></tr>
									<tr><td align="center" colspan="4"><textarea name="perfil_profesional" cols="60" wrap="auto" rows="5"></textarea></td></tr>

								</table><br>&nbsp;
							</td></tr>
						<tr><td colspan="2" align="center" class="subtitulo"><hr>Otros datos<hr></td></tr>

						<tr><td colspan="2" align="center">&nbsp;</td></tr>
						<tr><td colspan="2" align="center">
								<table cellpadding="2" cellspacing="0" border="0" align="center" width="100%">

									<tr><td align="center" colspan="2"><b>Fortalezas personales:</b></td></tr>
									<tr><td align="center" colspan="2"><textarea name="fortalezas" cols="60" wrap="auto" rows="5"></textarea></td></tr>
									<tr><td align="center" colspan="2"><b>Aficiones:</b></td></tr>
									<tr><td align="center" colspan="2"><textarea name="aficiones" cols="60" wrap="auto" rows="5"></textarea></td></tr>

								</table><br>&nbsp;
							</td></tr>
						<tr><td colspan="2" align="center" class="subtitulo"><br><input type="submit" value="guardar"></td></tr>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>


