<?php
class Scorm
{
	public static function obtenerArrayScorm($rows)
	{
		$arrayScorm = array('tiempoTotal' => '-', 'estado' => '-');
		
		while($item = $rows->fetch_object())
		{
			switch($item->varName)
			{
				case 'cmi.core.total_time':
					$arrayScorm['tiempoTotal'] = $item->varValue;
					break;
				case 'cmi.core.lesson_status':
					if($item->varValue == 'completed')
					{
						$estadoScorm = 'completado';
					}
					else
					{
						$estadoScorm = $item->varValue;
					}
					
					$arrayScorm['estado'] = $estadoScorm;
					break;
				default:
					break;
			}
		}
		
		return $arrayScorm;
	}
}