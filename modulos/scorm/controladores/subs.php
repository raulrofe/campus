<?php

/*

VS SCORM 1.2 RTE - subs.php
Rev 2009-11-30-01
Copyright (C) 2009, Addison Robson LLC

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor,
Boston, MA 02110-1301, USA.

*/

// ------------------------------------------------------------------------------------
// Database-specific code
// ------------------------------------------------------------------------------------

function dbConnect() {

	// database login details
	global $dbname;
	global $dbhost;
	global $dbuser;
	global $dbpass;

	// link
	global $link;

	// connect to the database
	$objDatabase = new Database();
	$link = $objDatabase->conectar();

}

function readElement($VarName) {

	global $link;
	global $SCOInstanceID;

	$safeVarName = mysqli_real_escape_string($link, $VarName);
	$result = mysqli_query($link, "select VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName'))");
	list($value) = mysqli_fetch_row($result);

	return $value;
}

function writeElement($VarName,$VarValue) {

	global $link;
	global $SCOInstanceID;

	$safeVarName = mysqli_real_escape_string($link, $VarName);
	$safeVarValue = mysqli_real_escape_string($link, $VarValue);
	mysqli_query($link, "update scormvars set VarValue='$safeVarValue' where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName'))");

	return;

}

function initializeElement($VarName,$VarValue) {

	global $link;
	global $SCOInstanceID;

	// make safe for the database
	$safeVarName = mysqli_real_escape_string($link, $VarName);
	$safeVarValue = mysqli_real_escape_string($link, $VarValue);

	// look for pre-existing values
	$result = mysqli_query($link, "select VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName'))");

	// if nothing found ...
	if (! mysqli_num_rows($result)) {
		mysqli_query($link, "insert into scormvars (SCOInstanceID,VarName,VarValue) values ($SCOInstanceID,'$safeVarName','$safeVarValue')");
	}

}

function initializeSCO() {

	global $link;
	global $SCOInstanceID;

	//echo "select count(VarName) from scormvars where (SCOInstanceID=$SCOInstanceID)"; exit;
	// has the SCO previously been initialized?
	$result = mysqli_query($link, "select count(VarName) from scormvars where (SCOInstanceID=$SCOInstanceID)");
	list($count) = mysqli_fetch_row($result);

	// not yet initialized - initialize all elements
	if (! $count) {

		// elements that tell the SCO which other elements are supported by this API
		initializeElement('cmi.core._children','student_id,student_name,lesson_location,credit,lesson_status,entry,score,total_time,exit,session_time');
		initializeElement('cmi.core.score._children','raw');

		// student information
		initializeElement('cmi.core.student_name',getFromLMS('cmi.core.student_name'));
		initializeElement('cmi.core.student_id',getFromLMS('cmi.core.student_id'));

		// test score
		initializeElement('cmi.core.score.raw','');
		initializeElement('adlcp:masteryscore',getFromLMS('adlcp:masteryscore'));

		// SCO launch and suspend data
		initializeElement('cmi.launch_data',getFromLMS('cmi.launch_data'));
		initializeElement('cmi.suspend_data','');

		// progress and completion tracking
		initializeElement('cmi.core.lesson_location','');
		initializeElement('cmi.core.credit','credit');
		initializeElement('cmi.core.lesson_status','browsed');
		initializeElement('cmi.core.entry','ab-initio');
		initializeElement('cmi.core.exit','');

		// seat time
		initializeElement('cmi.core.total_time','0000:00:00');
		initializeElement('cmi.core.session_time','');
		initializeElement('cmi.student_data.max_time_allowed','0024:00:00');
	}

	// new session so clear pre-existing session time
	writeElement('cmi.core.session_time','');

	// create the javascript code that will be used to set up the javascript cache,
	$initializeCache = "var cache = new Object();\n";

	$result = mysqli_query($link, "select VarName,VarValue from scormvars where (SCOInstanceID=$SCOInstanceID)");
	while (list($varname,$varvalue) = mysqli_fetch_row($result)) {

		// make the value safe by escaping quotes and special characters
		$jvarvalue = addslashes($varvalue);

		// javascript to set the initial cache value
		$initializeCache .= "cache['$varname'] = '$jvarvalue';\n";

	}

	// return javascript for cache initialization to the calling program
	return $initializeCache;

}

// ------------------------------------------------------------------------------------
// LMS-specific code
// ------------------------------------------------------------------------------------
function setInLMS($varname,$varvalue) {
	return "OK";
}

function getFromLMS($varname) {

	switch ($varname) {

		case 'cmi.core.student_name':
			$objUsuarios = new Usuarios();
			$objUsuarios->set_usuario(Usuario::getIdUser());
			$usuario = $objUsuarios->buscar_usuario();
			$usuario = $usuario->fetch_object();
			if(Usuario::compareProfile('alumno'))
			{
				$varvalue = $usuario->nombre . ' ' . $usuario->apellidos;
			}
			else
			{
				$varvalue = $usuario->nombrec;
			}
			$varvalue =  ucwords(Texto::quitarTildes($varvalue, ' '));
			break;

		case 'cmi.core.student_id':
			$varvalue = Usuario::getIdUser(true);
			break;

		case 'adlcp:masteryscore':
			$varvalue = 0;
			break;

		case 'cmi.launch_data':
			$varvalue = "";
			break;

		default:
			$varvalue = '';

	}

	return $varvalue;

}

?>