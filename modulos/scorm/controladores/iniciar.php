<?php
$objModeloScorm = new ModeloScorm();
$get = Peticion::obtenerGet();
$SCOInstanceID = null;

if(isset($get['idscorm']) && is_numeric($get['idscorm']))
{
	// comprobamos que el scorm exista
	$rowScorm = $objModeloScorm->obtenerUnScorm($get['idscorm']);
	
	if($rowScorm->num_rows == 1)
	{
		$rowScorm = $rowScorm->fetch_object();

		// si no vine por GET el SCOInstanceID lo creamos
		if(!isset($get['SCOInstanceID']) || (isset($get['SCOInstanceID']) && empty($get['SCOInstanceID'])))
		{
			// si ya tiene una instancia de SCOInstanceID pues la asignamos SCOInstanceID
			$alumnoScorm = $objModeloScorm->obtenerIdInstanceScormFromAlumno($get['idscorm'], Usuario::getIdUser(true), Usuario::getIdCurso());
			if($alumnoScorm->num_rows == 1)
			{
				$alumnoScorm = $alumnoScorm->fetch_object();
				$SCOInstanceID = $alumnoScorm->SCOInstanceID;
			}
			// creamos una instancia de scorm de SCOInstanceID para ese usuario en este curso
			else
			{
				if($objModeloScorm->insertarScormAlumno($get['idscorm'], Usuario::getIdUser(true), Usuario::getIdCurso()))
				{
					$SCOInstanceID = $objModeloScorm->obtenerUltimoIdInsertado();
				}
			}

			// REDIRIGIMOS A LA MISMO URL PERO CON EL IDINSTANCIA DEL SCORM
			Url::redirect('contenido-multimedia/curso/' . $get['idscorm'] . '/' . $SCOInstanceID);
		}
		else if(isset($get['SCOInstanceID']) && is_numeric($get['SCOInstanceID']))
		{
			$SCOInstanceID = $get['SCOInstanceID'];
		}

		if(isset($SCOInstanceID))
		{

			if(Usuario::getIdCurso() == 51) {
				exit($SCOInstanceID	);
			}

			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'iniciar');
		}
	}
}