<div id="contenido_multimedia">
	<?php require DIRNAME(__FILE__) . '/intro_scorm.php'?>

	<?php if($scorms->num_rows > 0):?>
		<div class="listarElementos">
			<?php while($item = $scorms->fetch_object()):?>
					<?php if(file_exists(PATH_ROOT . 'archivos/scorm/' . $item->idscorm . '/sco_' . $item->idscorm_folder . '/default.html')):?>
						<div class="elemento">
							<div class="elementoContenido">
								<div class='fleft' style='margin-right:15px;'><img src='imagenes/scorm.png' alt='' /></div>
								<p style='line-height:27px;'><a href="#" onclick="parent.popup_real_open('scorm', 'contenido-multimedia/curso/<?php echo $item->idscorm?>', 600, 810, false); return false;" title="Abrir este contenido multimedia"><?php echo Texto::textoPlano($item->titulo)?></a></p>
							</div>
						<div class="clear"></div>
						</div>
					<?php else:?>
						<!-- <div>No se encontro <b><?php //echo Texto::textoPlano($item->titulo)?></b></div> -->
					<?php endif;?>
			<?php endwhile;?>
		</div>
	<?php else:?>
	<?php endif;?>
</div>
