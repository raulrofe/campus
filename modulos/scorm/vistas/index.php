 <div id="enlacesInteres">
 	
 	<br/>
 	<div class="subtitle t_center">
 		<img src="imagenes/enlace_interes/enlaces-interes.png" alt="enlaces de interes" style="vertical-align:middle;width:40px;height40px;margin-right: 5px;"/>
 		<span>Enlaces de inter&eacute;s</span>
 	</div>
 	<br/>
 
	<div class="listarElementos">
	
		<div class="elemento">
			<p><b>Nombre: Centro Aragonés de Recursos para la Educación Intercultural (CAREI)</b></p>
			<p><b>Descripción:</b> Página web que contiene recursos, materiales, actividades para el aula, presentaciones audiovisuales de distintos países, experiencias, formación y enlaces.</p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.carei.es/portada.php" target="_blank">http://www.carei.es/portada.php</a> </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Centro de Animación y Documentación Intercultural (CADI) </b></p>
			<p><b>Descripción:</b> Se trata de la página web del centro de recursos y de apoyo a educadores CADI (Región de Murcia). Incluye extensa bibliografía, materiales, enlaces, noticias y formación. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://cadi.murciadiversidad.org/index.php?op2=Salir" target="_blank">http://cadi.murciadiversidad.org/index.php?op2=Salir</a>  </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Centro de Recursos de Educación Interculturalidad (CREI) </b></p>
			<p><b>Descripción:</b> Esta página web ofrece una selección de recursos didácticos para Educación Primaria y Secundaria, manuales teóricos, páginas web, recursos multimedia, etc. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://crei.centros.educa.jcyl.es/sitio/index.cgi#" target="_blank">http://crei.centros.educa.jcyl.es/sitio/index.cgi#</a> </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Cuaderno multicultural</b></p>
			<p><b>Descripción:</b> Se trata de un espacio web en el que podremos encontrar multitud de recursos para la educación intercultural: orientación para el docente, mariales didácticos, dinámicas y juegos, etc. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.cuadernointercultural.com/" target="_blank">http://www.cuadernointercultural.com/</a>  </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Intercultur@net </b></p>
			<p><b>Descripción:</b> En esta página se promueve la interculturalidad a través de Internet, ofreciendo entre otras cosas Formación (a distancia y en grupos de trabajo), y Recursos (propios y externos). </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://ntic.educacion.es/w3//interculturanet/recurss1.htm" target="_blank">http://ntic.educacion.es/w3//interculturanet/recurss1.htm</a>  </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Interculturalidad ITE </b></p>
			<p><b>Descripción:</b> Sección del Instituto de Tecnologías Educativas dedicado a las Interculturalidad a partir del cual podremos acceder a materiales y recursos relevantes para trabajar este aspecto </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://ntic.educacion.es/w3//recursos2/interculturalidad/index.html" target="_blank">http://ntic.educacion.es/w3//recursos2/interculturalidad/index.html</a>  </p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Portal Aula Intercultural </b></p>
			<p><b>Descripción:</b> :Ofrece la traducción de materias en otros idiomas, incide sobre el papel de los medios de comunicación y sobre la interculturalidad en el estado español. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.aulaintercultural.org/" target="_blank">http://www.aulaintercultural.org/</a></p>
		</div>
		
		<div class="elemento">
			<p><b>Nombre: Portal Multiculturalidad Junta de Andalucía </b></p>
			<p><b>Descripción:</b> Se trata de un espacio de intercambio de información, conocimientos y experiencias entre voluntarios, especialistas y profesionales, el cual se ha dotado de contenido, útil y dinámico, para seguir formándose y adquiriendo conocimientos y experiencias multiculturales. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b><a href="http://multiculturalidad.org/" target="_blank">http://multiculturalidad.org/</a> </p>
		</div>
		
		<!-- 
		<div class="elemento">
			<p><b>Nombre: Programa Norte Sur: Un viaje de ida y vuelta </b></p>
			<p><b>Descripción:</b> Esta página web contiene materiales didácticos de interculturalidad, comercio justo y género, dirigido a centros educativos </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.nortesur.org/" target="_blank">http://www.nortesur.org/</a></p>
		</div>
		 	

		<div class="elemento">
			<p><b>Nombre: Proyecto Europeo la Maleta Intercultural </b></p>
			<p><b>Descripción:</b> El conjunto de materiales que se presentan en este web es el resultado de tres años de trabajo dentro del proyecto europeo titulado La maleta intercultural. En él han participado varias instituciones de formación de distintos países comunitarios, y entre otras cosas podremos acceder y descargarnos una serie de Unidades Didácticas interculturales muy útiles para nuestra práctica docente. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.sebyc.com/iesrch/intercultural" target="_blank">http://www.sebyc.com/iesrch/intercultural</a> </p>
		</div>	
		-->
		
		<div class="elemento">
			<p><b>Nombre: Red de Educación Intercultural </b></p>
			<p><b>Descripción:</b> Es un proyecto dirigido a fortalecer la formación del profesorado sobre diversidad cultural y estrategias para promover la interculturalidad en las aulas. </p>
			<p><b>Idioma:</b> Español</p>
			<p><b>Enlace:</b> <a href="http://www.fundacionfide.org/red/" target="_blank">http://www.fundacionfide.org/red/</a> </p>
		</div>		
	</div>
 </div>
 
 <br/>
 