<?php
class ModeloScorm extends modeloExtend
{
	public function obtenerScorms($idusuario, $idcurso)
	{
		$sql = 'SELECT sc.titulo, sc.idscorm, sc.idscorm_folder, sc.imagen_scorm, sc.descripcion' .
		' FROM scorm AS sc' .
		' LEFT JOIN temario AS tm ON tm.idmodulo = sc.idmodulo' .
		' LEFT JOIN curso AS c ON c.idaccion_formativa = tm.idaccion_formativa' .
		' LEFT JOIN matricula AS m ON m.idcurso = c.idcurso' .
		' LEFT JOIN staff AS st ON st.idcurso = c.idcurso' .
		' WHERE (m.idcurso = ' . $idcurso . ' AND m.idalumnos = ' . $idusuario . ') OR (st.idcurso = ' . $idcurso . ' AND st.idrrhh = ' . $idusuario . ')' .
		' GROUP BY sc.idscorm';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerIdInstanceScormFromAlumno($idscorm, $idusuario, $idcurso)
	{
		$sql = 'SELECT * FROM scorm_alumnos WHERE idscorm = ' . $idscorm . ' AND idusuario = "' . $idusuario . '" AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnScorm($idscorm)
	{
		$sql = 'SELECT * FROM scorm WHERE idscorm = ' . $idscorm;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarScormAlumno($idscorm, $idusuario, $idcurso)
	{
		$sql = 'INSERT INTO scorm_alumnos (idscorm, idusuario, idcurso)' .
		' VALUES (' . $idscorm . ', "' . $idusuario . '", ' . $idcurso . ')';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerScormModulo($idModulo)
	{
		$sql = 'SELECT idscorm, titulo, idscorm_folder FROM scorm WHERE idmodulo = ' . $idModulo;
		$resultado = $this->consultaSql($sql);

		//echo $sql;

		return $resultado;
	}

	public function obtenerVariablesUsuario($instanciaScorm)
	{
		$sql = 'SELECT * FROM scormvars WHERE SCOInstanceID = ' . $instanciaScorm;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Eliminar instancia

	public function eliminarInstanciaScorm($instanciaScorm) {
		
		$sql = 'DELETE FROM scorm_alumnos WHERE SCOInstanceID = ' . $instanciaScorm;

		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

}