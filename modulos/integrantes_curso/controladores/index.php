<?php
$objModelo = new ModeloIntegrantesCurso();

$profesores = $objModelo->obtenerProfesores(Usuario::getIdCurso());

$alumnos = $objModelo->obtenerAlumnosActivos(Usuario::getIdCurso());


require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');