<?php
$objModelo = new ModeloIntegrantesCurso();

$get = Peticion::obtenerGet();
if(isset($get['idrrhh']) && is_numeric($get['idrrhh']))
{
	$rowData = $objModelo->obtenerUnProfesor($get['idrrhh']);
}
else if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	$rowData = $objModelo->obtenerUnAlumno($get['idalumno']);
}

if(isset($rowData) && $rowData->num_rows == 1)
{
	$rowData = $rowData->fetch_object();
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'verDatos');
}