<?php

class ModeloIntegrantesCurso extends modeloExtend {

	public function obtenerAlumnos($idCurso) {
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto, al.idsexo FROM alumnos AS al' .
			' LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos' .
			' WHERE mt.idcurso = ' . $idCurso .
			' GROUP BY al.idalumnos ORDER BY nombrec DESC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnosActivos($idCurso) {
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto, al.idsexo, c.titulo AS titulocurso' .
			' FROM alumnos AS al' .
			' LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos' .
			' LEFT JOIN curso AS c ON c.idcurso = mt.idcurso' . 
			' WHERE mt.idcurso = ' . $idCurso .
			' AND al.borrado = 0' .
			' AND mt.borrado = 0' .
			' GROUP BY al.idalumnos ORDER BY nombre ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerProfesores($idCurso) {
		$sql = 'SELECT rh.idrrhh, rh.nombrec, rh.foto, rh.idperfil, rh.cv, p.perfil_mostrar '
			. 'FROM rrhh AS rh' .
			' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
			' LEFT JOIN perfil AS p ON rh.idperfil = p.idperfil' .
			' WHERE st.idcurso = ' . $idCurso .
			' and (rh.idperfil = 3 OR rh.idperfil = 4 OR rh.idperfil = 5)' .
			' GROUP BY rh.idrrhh ORDER BY rh.nombrec ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnAlumno($idAlumno) {
		$sql = 'SELECT CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto, al.idsexo FROM alumnos AS al' .
			' LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos' .
			' WHERE mt.idalumnos = ' . $idAlumno .
			' GROUP BY al.idalumnos ORDER BY nombrec DESC LIMIT 1';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnProfesor($idTutor) {
		$sql = 'SELECT rh.nombrec, rh.foto, rh.idperfil, rh.email FROM rrhh AS rh' .
			' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
			' WHERE st.idrrhh = ' . $idTutor .
			' and (rh.idperfil = 3 OR rh.idperfil = 4 OR rh.idperfil = 5)' .
			' GROUP BY rh.idrrhh ORDER BY rh.nombrec DESC LIMIT 1';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

}
