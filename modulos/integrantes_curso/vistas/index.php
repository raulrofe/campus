<div id="integrantes_curso">
	<div class="subtitle t_center">
		<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
		<span data-translate-html="integrantes.titulo">INTEGRANTES DEL CURSO</span>
	</div>
	<br/><br/>
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuIntegrantesCurso">
		<div id="equipoDocente" class="blanco redondearBorde" onclick="changeTab(1)">
			<img src="imagenes/integrantes_curso/tutores.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="integrantes.docentes">Equipo docente</p>
		</div>
		<div id="alumnos" class="redondearBorde" onclick="changeTab(2)">
			<img src="imagenes/integrantes_curso/alumnos.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="integrantes.alumnos">Alumnos</p>
		</div>
	</div>
	<!-- Fin menu iconos -->

	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso">

		<!--*********************************** TAB 1DOCENTE ******************************************************** -->

		<div id="docentes">
			<div class="tituloTabs">
				<span data-translate-html="integrantes.docentes"> 
					Equipo docente
				</span> 

				(<?php echo $profesores->num_rows ?>)
			</div>

				<?php if($profesores->num_rows > 0):?>

					<?php while($profesor = $profesores->fetch_object()):?>

						<div class="listMembers">

							<div class="elementoAvatar">

								<?php if(file_exists('imagenes/fotos/' . $profesor->foto)):?>

									<img src="imagenes/fotos/<?php echo $profesor->foto?>"/>

								<?php else:?>

									<img src="imagenes/fotos/default.png"/>

								<?php endif?>
							</div>

							<div class="elementoNombre" >
								<?php echo Texto::textoPlano($profesor->nombrec)?>
								<div>
									<?php if($profesor->idperfil == 4 || $profesor->idperfil == 5):?>
										<span data-translate-html="perfiles.tutor">(Tutor/a)</span>
									<?php else:?>
										<span data-translate-html="perfiles.coordinador">(Coordinador/a)</span>
									<?php endif;?>
								</div>
							</div>

							<?php if(!is_null($profesor->cv)): ?>
								<div class="elementoPie">
									<ul class="elementoListOptions fright">
										<li>
											<a href="<?php echo $profesor->cv ?>" target="_blank" data-translate-html="integrantes.ver_curriculum">
												Ver curriculum vitae
											</a>
										</li>
									</ul>
								</div>
							<?php endif; ?>

							<div class="clear"></div>

						</div>
					<?php endwhile;?>
				<?php else:?>
					<div class="elementosNoEncontrados">
						<strong>
							<span data-translate-html="integrantes.no_docentes">
								No hay profesores/coordinadores
							</span>
						</strong>
					</div>
				<?php endif;?>
		</div>

		<!-- ***************************** TAB 2 ALUMNOS ************************************************************* -->

		<div id="alumnado" class="hide" >
			<div class="tituloTabs">
				<span data-translate-html="integrantes.alumnos">
					Alumnos
				</span> 

				(<?php echo $alumnos->num_rows ?>)
			</div>

			<?php if($alumnos->num_rows > 0):?>

				<?php while($alumno = $alumnos->fetch_object()):?>

					<div class="listMembers" >

						<div class="elementoAvatar fleft">

							<?php if(file_exists('imagenes/fotos/' . $alumno->foto)):?>

								<img src="imagenes/fotos/<?php echo $alumno->foto?>"/>

							<?php else:?>

								<img src="imagenes/fotos/default.png"/>

							<?php endif ?>

						</div>

						<div>
							<div class="elementoNombre fleft">
								<?php echo Texto::textoPlano(ucwords($alumno->nombrec))?>
							</div>

							<div class='clear'></div>
						</div>
					</div>

					<div class="clear"></div>

				<?php endwhile;?>

			<?php else:?>

				<div class="elementosNoEncontrados">
					<strong>
						<span data-translate-html="integrantes.no_alumnos">
							No hay alumnos
						</span>
					</strong>
				</div>

			<?php endif;?>
		</div>

		<!-- Fin de programas utiles -->

	</div>
	<div class='clear'></div>

</div>


<script src="js-integrantes_curso-default.js"></script>