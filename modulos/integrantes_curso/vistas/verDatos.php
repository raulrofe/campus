<div id="integrantes_curso" style="background-color:#EFEFEF;padding:20px;">
	<div class='subtitleModulo t_center'>
		<span data-translate-html="usuario.tab1">
			Datos personales
		</span> 
		<span data-translate-html="general.de">
			de:
		</span>
		<br/> 
		<?php echo strtoupper(Texto::textoPlano($rowData->nombrec));?>
	</div>

	<div>
		<div class="clear"></div>

		<img src="imagenes/fotos/<?php echo $rowData->foto?>" alt="" style="margin: 0 0 0 45%;"/>
	</div>

	<br/>

	<div class="t_center" style="font-size:1em;width:300px;margin:0 auto;">
		<div>
			<div class='fleft negrita' style="width:90px;">
				<span data-translate-html="usuario.nombre">
					Nombre:
				</span>
				&nbsp;
			</div>
			<div class="fleft">
				&nbsp;<?php echo $rowData->nombrec;?>	
			</div>
		</div>

		<br/>

		<?php if(isset($rowData->idperfil)):?>
			<br/>
			<div>
				<div class='fleft negrita' style="width:90px;">
					<span data-translate-html="perfiles.funcion">
						Funci&oacute;n:
					</span>
					&nbsp;
				</div>

				<?php if($rowData->idperfil == 4 || $rowData->idperfil == 5):?>
					<div class="fleft">
						&nbsp;
						<span data-translate-html="perfiles.tutor">
							Tutor/a
						</span>
					</div>
				<?php else:?>
					<div class="f_left">
						&nbsp;
						<span data-translate-html="perfiles.coordinador">
							Coordinador/a
						</span>
					</div>
				<?php endif;?>
			</div>
		<?php endif;?>

		<br/>

		<div>
			<div class='fleft negrita' style="width:90px;">
				<span data-translate-html="sexo.titulo">
					Sexo
				</span>: &nbsp;
			</div>

			<?php if(isset($rowData->idsexo)):?>

				<?php if($rowData->idsexo == 1):?>

					<div class="fleft">
						&nbsp;
						<span data-translate-html="sexo.hombre">
							Hombre
						</span>
					</div>

				<?php elseif($rowData->idsexo == 2):?>

					<div class="fleft">
						<span data-translate-html="sexo.mujer">
							Mujer
						</span>
					</div>

				<?php endif;?>

			<?php else:?>

				<div class="fleft">
					&nbsp;
					<span data-translate-html="sexo.no">
						No especificado
					</span>
				</div>

			<?php endif;?>
		</div>
	</div>

	<div class="clear"></div>
	<div class="clear"></div>
</div>