$(document).ready(function()
{	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#home_show_cursos_inactive_search').keyup(function(event)
	{
		homeCursos_buscarInactivos();
	});
});

// BUSCADOR DE CURSOS INACTIVOS POR CONVOCATORIA
function homeCursos_buscarInactivos()
{
	var wordSearch = $('#home_show_cursos_inactive_search').val();
	//var idConvSearch = $('#home_show_cursos_inactive_conv').val();
	
	$('#home_show_cursos_list_inactive').find('.tutorizado').addClass('hide');
	
	if(wordSearch != "")
	{
		var titleScorm = '';
		var searchMatch = null;
		$('#home_show_cursos_list_inactive').find('.tutorizado').each(function(index)
		{
			titleScorm = $(this).find('a').html();
			wordSearch = LibMatch.escape(wordSearch);
			var regex = new RegExp('(' + wordSearch + ')', 'i');
			if(titleScorm.match(regex))
			{
				$(this).removeClass('hide');
			}
		});
	}
	else
	{
		$('#home_show_cursos_list_inactive').find('.tutorizado').each(function(index)
		{
				$(this).removeClass('hide');
		});
	}
}