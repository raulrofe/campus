<div class='cabeceralms'>
	<div class='fleft' style="margin-top: 5px;margin-left: 5px;">
		<a href='#/hall/<?php echo Usuario::getIdCurso()?>' onclick="cambiarHash('hall/<?php //echo Usuario::getIdCurso()?>'); return false;">
			<img src='archivos/config_web/<?php echo $logotipo['imagen_logo'];?>' style='margin-right:5px;' />
		</a>
	</div>
	<div class='datos_tutor' style="display: flex;align-items: center;justify-content: center;flex-wrap: wrap;">
		<div style="font-size: 0.9em;width: 100%">
				<?php echo $usuario;?>
		</div>

		<div class

		<div id="principalNombreCurso" style="font-size: 0.8em;">
			<?php if(isset($dato_curso)) echo Texto::textoPlano($dato_curso['titulo']);?>
		</div>

		<br/>

		<div style="font-size: 0.9em;width: 100%"">
			<a href="archivos/guia-de-usuario.pdf" target="_blank" data-translate-href="guiausuario.enlace" data-translate-html="contenidos.guiausuario">Guía de usuario</a>
		</div>

	</div>
</div>

<script type="text/javascript" src="js-cursos-default.js"></script>
<script type="text/javascript">LibPopupModal.closeAll();</script>

<div class='inicio'>
	<div id="home_show_cursos_list_inactive" class='listado_cursos'>
		<div style="text-align: center;margin: 0 auto">
			<img src="parallax/img/logo_campus.png" style="width: 70%;margin: 10px 0"/>
		</div>

		<?php if(!Usuario::compareProfile('alumno')):?>
			<div class="t_center">LISTADO DE CURSOS</div><br/>
		  	<input id="home_show_cursos_inactive_search" type="text" name="">
		<?php endif;?>

		<?php while($f = mysqli_fetch_assoc($registros)):?>

<!-- 			<div class="t_center" style="margin: 5px 0 15px 0;font-weight: bold">				
				Cursos activos
			</div> -->

			<div class='tutorizado t_center' style="font-size: 0.9em;" data-search-conv="<?php echo $f['idconvocatoria']?>">
				<?php if($f['f_inicio'] <= date("Y-m-d")): ?>
					<a href='#/hall/<?php echo $f['idcurso']?>' onclick="cambiarHash('hall/<?php echo $f['idcurso']?>'); return false;">
						<strong><?php echo $f['titulo']?></strong>
					</a>
				<?php else: ?>
					<?php if(Usuario::compareProfile('alumno')):?>
						<a href='#/hall/<?php echo $f['idcurso']?>' onclick="alertMsg('cursonoiniciado', 'El curso todavía no ha comenzado intentelo de nuevo, el día fijado para el inicio'); return false;">
							<strong><?php echo $f['titulo']?></strong>
						</a>
					<?php else: ?>
						<a href='#/hall/<?php echo $f['idcurso']?>' onclick="cambiarHash('hall/<?php echo $f['idcurso']?>'); return false;">
							<strong><?php echo $f['titulo']?></strong>
						</a>						
					<?php endif; ?>					
				<?php endif; ?>
				<br />
				<span class="t_center" style="font-size: 0.85em;">
					<span data-translate-html="login.inicio">
						Inicio
					</span>
					:
					<?php echo Fecha::invertir_fecha($f['f_inicio'], '-', '/')?>
					 - 
					<span data-translate-html="misprogresos.fin">
					 	Fin
					</span>
					:
					<?php echo Fecha::invertir_fecha($f['f_fin'], '-', '/')?>
				</span>
			</div>
		<?php endwhile;?>
	</div>
	
	<?php
	 if($_SESSION['perfil'] == 'alumno' && $_SESSION['encuesta'] == 0): ?>
		<div id="encuestaOpinion" class="hide">
			<iframe src="http://campusaulainteractiva.aulasmart.net/encuesta/index.html" style="width:100%;height:100%"></iframe>
		</div>
	<?php endif; ?>
</div>

<script type="text/javascript">
	if (($("#encuestaOpinion").length > 0)){
		$.fancybox({
			'width'				: '80%',
		    'height'			: '80%',
		    'autoSize' 			: false,
		    'live'				: false,
		    'scrolling' 		: 'no',
		    'closeBtn'			: false,
		    'showCloseButton'	: false,
			'content' 			: $('#encuestaOpinion').html(),
			'helpers' 			: {overlay : {closeClick: false}},
			'keys' 				: {close: null}
		});
	}
</script>

