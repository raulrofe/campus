<?php
//var_dump($_SESSION);

$db = new database();
$con = $db->conectar();

$mi_curso = new Cursos($con);


// cursos en los que esta dado de alta y estan inactivos
if(!Usuario::compareProfile('alumno'))
{
	// if(Usuario::compareProfile('tutor') && $_SESSION['idusuario'] != 95 && $_SESSION['idusuario'] != 61){
	// 	if(date("H:i") >= '14:00'){
	// 		Url::redirect('');
	// 	}
	// }

	if($_SESSION['idusuario'] == 64)
	{
		$registros = $mi_curso->cursos_usuario(21, null);
	}
	else
	{
		$registros = $mi_curso->cursos_usuario(null, null);
	}
}
else
{
	// cursos en los que esta dado de alta actualmente y estan activos
	$registros = $mi_curso->cursos_usuario(null, true);
}

//PARCHE PARA QUE EL USUARIO DE MANUEL PUEDA VER TODOS LOS CURSOS
$objModelo = new ModeloCurso();
$convocatorias = $objModelo->obtenerConvocatorias();

//Obtenemos el logo para mostrarlo
$mi_aula = new Aula($con);
$logotipo = $mi_aula->custom_logo();

//Obtenemos el logo para mostrarlo
$mi_aula = new Aula($con);
$logotipo = $mi_aula->custom_logo();

// id del perfil de usuasrio logueado
$idperfil = Usuario::obtenerIdPerfilLogueo();

// Obtenemos los datos del usuario
$mi_usuario = new usuarios($idperfil,$con);
$mi_usuario->set_usuario($_SESSION['idusuario']);
$registros_usuario = $mi_usuario->buscar_usuario();
$datos_usuario = mysqli_fetch_assoc($registros_usuario);

if($_SESSION['perfil'] != 'alumno')
{
	$usuario = ucwords($datos_usuario['nombrec']);
}

else
{
	$usuario = ucwords($datos_usuario['nombre'])." ".ucwords($datos_usuario['apellidos']);
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'cursos');