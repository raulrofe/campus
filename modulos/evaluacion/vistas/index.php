<div class='menu_interno'>
	<span class='elemento_menuinterno'>
		<a href="evaluaciones/calificaciones" title="" data-translate-html="evaluacion.calificaciones">
			Calificaciones
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href="evaluaciones/notas" title="" data-translate-html="evaluacion.notas">
			Notas
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href="evaluaciones/informes" title="" data-translate-html="evaluacion.informes">
			Informes
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href="evaluaciones/expedientes" title="" data-translate-html="evaluacion.expedientes">
			Expedientes
		</a>
	</span>
	
	<!-- <span class='elemento_menuinterno'><a href="evaluaciones/boletines" title="">Boletines</a></span> -->
	
	<div class="clear"></div>

</div>