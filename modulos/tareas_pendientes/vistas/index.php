<div id="tareas_pendientes">
	<div class='subtitle t_center'>
		<img src="imagenes/tareas pendientes/tasks.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="tareas_pendiente.titulo">
			TAREAS PENDIENTES
		</span>
	</div>
	<br/>
	<div class="tareas_pendientes_bloque">
		<div class="subtitleModulo" data-translate-html="tareas_pendiente.avisos">
			Avisos destacados
		</div>
		<!-- <div class='subtitleModulo'><span>Avisos destacados</span></div> -->
		<div class="tareas_pendientes_bloque_content">
			<?php if ($numCorreos > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/mail.png" alt=""/>
						<a href='#' onClick="parent.popup_open('Correos', 'gestor_correos', 'correos/bandeja_entrada', 750, 900); return false;"
						   data-translate-html="tareas_pendiente.correo_sin_leer">
							Mensajes sin leer del correo
						</a>
					</div>
					<div class="fright naviso"><?php echo $numCorreos ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>

			<?php if ($numTablonAnuncio > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/anuncios.png" alt=""/>
						<a href='#' onClick="parent.popup_open('tabl&oacute;n de anuncios', 'tablon_anuncio', 'aula/tablon_anuncio', 550, 800); return false;"
						   data-translate-html="tareas_pendiente.correo_sin_leer2">
							Mensajes sin leer del tabl&oacute;n de anuncios (aula):
						</a>
					</div>
					<div class="fright naviso">
						<?php echo $numTablonAnuncio ?>
					</div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>

			<?php if ($numRecomendaciones > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/recomendaciones.png" alt=""/>
						<a href='#' onclick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;"
						   data-translate-html="tareas_pendiente.recomendaciones">
							Recomendaciones del tutor/a:
						</a>
					</div>
					<div class="fright naviso"><?php echo $numRecomendaciones ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>

			<?php if ($numRecomendacionesPers > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/recomendaciones.png" alt=""/>
						<a href='#' onclick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;"
						   data-translate-html="tareas_pendiente.eventos">
							Eventos personales
						</a>
					</div>
					<div class="fright naviso"><?php echo $numRecomendacionesPers ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>

			<?php if ($tutoria->num_rows > 0): ?>
				<div class="avisoTareasPendientes">
					<img src="imagenes/tareas pendientes/tutoria.png" alt=""/>
					<a href='#' onClick="parent.popup_open('Tutor&iacute;a online', 'chat_tutoria', 'aula/tutorias/chat', 520, 700); return false;"
					   data-translate-html="tareas_pendiente.tutoria">
						La tutor&iacute;a est&aacute; abierta
					</a>
				</div>
			<?php endif; ?>

			<?php if ($numCorreos == 0 && $numTablonAnuncio == 0 && $tutoria->num_rows == 0 && $numRecomendaciones == 0 && $numRecomendacionesPers == 0): ?>
				<div class="avisoTareasPendientes" data-translate-html="tareas_pendiente.aviso">
					No tienes ning&uacute;n aviso
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tareas_pendientes_bloque">
		<!-- <div class='subtitleModulo'><span>Tareas pendientes</span></div> -->
		<div class="subtitleModulo" data-translate-html="tareas_pendiente.titulo">
			Tareas pendientes
		</div>
		<div class="tareas_pendientes_bloque_content">
			<?php if ($numTrabjPract > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/trabajos_practicos.png"/>
						<a href='#' onClick="parent.popup_open('Trabajos pr&aacute;cticos / Ejercicios de evaluaci&oacute;n', 'trabajos_practicos', 'aula/trabajos_practicos/grafica', 550, 800); return false;"
						   data-translate-html="tareas_pendiente.trabajos_pendientes">
							Tienes trabajo/s pr&aacute;ctico/s pendiente/s
						</a>
					</div>
					<div class="fright naviso"><?php echo $numTrabjPract ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>
			<?php if ($numAutoeval > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/test.png" alt=""/>
						<a href='#' onClick="parent.popup_open('Test / Ejercicios de autoevaluaci&oacute;n', 'autoevaluaciones', 'aula/autoevaluaciones/grafica', 550, 800); return false;"
						   data-translate-html="tareas_pendiente.autoeval_pendientes">
							Tienes autoevaluacion/es pendiente/s
						</a>
					</div>
					<div class="fright naviso"><?php echo $numAutoeval ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>
			<?php if ($numeroMensajesForoSinLeer > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/foro.png" alt=""/>
						<a href='#' onClick="parent.popup_open('Foros', 'foro', 'aula/foro', 550, 800); return false;"
						   data-translate-html="tareas_pendiente.correo_sin_leer3">
							Mensajes sin leer del foro
						</a>
					</div>
					<div class="fright naviso"><?php echo $numeroMensajesForoSinLeer ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>
			<?php if ($mensajesOpinionesSinLeer > 0): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/foro.png" alt=""/>
						<a href="#" onclick="popup_open('Publicar opiniones', 'foro_opiniones', 'opiniones', 485, 610); return false;"
						   data-translate-html="tareas_pendiente.correo_sin_leer4">
							Mensajes sin leer de opiniones
						</a>
					</div>
					<div class="fright naviso"><?php echo $mensajesOpinionesSinLeer ?></div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>
			<?php if ($numTrabjPract == 0 && $numAutoeval == 0 && $numeroMensajesForoSinLeer == 0 && $mensajesOpinionesSinLeer == 0): ?>
				<div class="avisoTareasPendientes" data-translate-html="tareas_pendiente.notarea">
					No tienes ninguna tarea pendiente
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="tareas_pendientes_bloque">
		<div class="subtitleModulo" data-translate-html="misprogresos.titulo">
			Mis progresos
		</div>
		<div class="tareas_pendientes_bloque_content">
			<div class="avisoTareasPendientes">
				<div class="fleft">
					<img src="imagenes/tareas pendientes/mis_progresos.png" alt=""/>
					<a href='#' onClick="popup_open('Mis progresos', 'mis_progresos', 'mis-progresos#3', 650, 800);return false;"
					   data-translate-html="misprogresos.trabajos.titulo">
						Trabajos pr&aacute;cticos
					</a>
				</div>
				<div class="fright naviso">
					<?php if (is_float($porcentajeTrabajos)): ?>
						<?php echo number_format($porcentajeTrabajos, '2', '.', ''); ?>
					<?php else: ?>
						<?php echo $porcentajeTrabajos; ?>
					<?php endif; ?>
					%</div>
				<div class="clear"></div>
			</div>
			<div class="avisoTareasPendientes">
				<div class="fleft">
					<img src="imagenes/tareas pendientes/mis_progresos.png" alt=""/>
					<a href='#' onClick="popup_open('Mis progresos', 'mis_progresos', 'mis-progresos#2', 650, 800);return false;"
					   data-translate-html="misprogresos.autoevaluaciones.titulo">
						Autoevaluaciones
					</a>
				</div>
				<div class="fright naviso">
					<?php if (is_float($porcentajeTest)): ?>
						<?php echo number_format($porcentajeTest, '2', '.', ''); ?>
					<?php else: ?>
						<?php echo $porcentajeTest; ?>
					<?php endif; ?>
					%</div>
				<div class="clear"></div>
			</div>
			
			
			<div class="avisoTareasPendientes">
				<div class="fleft">
					<img src="imagenes/tareas pendientes/mis_progresos.png" alt=""/>
					<a href='#' onClick="popup_open('Mis progresos', 'mis_progresos', 'mis-progresos#4', 650, 800);return false;"
					   data-translate-html="foro.foro">
						Foro
					</a>
				</div>
				<div class="fright naviso">
					<?php if (is_float($porcentajeForo)): ?>
						<?php echo number_format($porcentajeForo, '2', '.', ''); ?>
					<?php else: ?>
						<?php echo $porcentajeForo; ?>
					<?php endif; ?>
					%</div>
				<div class="clear"></div>
			</div>
			<?php if (isset($porcentajeScorm)): ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/mis_progresos.png" alt=""/>
						<a href='#' onClick="popup_open('Mis progresos', 'mis_progresos', 'mis-progresos#4', 650, 800);return false;"
						   data-translate-html="misprogresos.scorm.scorm">
							SCORM
						</a>
					</div>
					<div class="fright naviso">
						<span data-translate-html="misprogresos.scorm.no_evaluable">
							El Contenido SCORM es "No Evaluable"
						</span>
					</div>
					<div class="clear"></div>
				</div>
			<?php else: ?>
				<div class="avisoTareasPendientes">
					<div class="fleft">
						<img src="imagenes/tareas pendientes/mis_progresos.png"/>
						<span data-translate-html="misprogresos.scorm.no_evaluable">
							El Contenido SCORM es "No Evaluable"
						</span>
					</div>
					<div class="clear"></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>