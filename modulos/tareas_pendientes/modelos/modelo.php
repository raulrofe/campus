<?php
class ModeloTareasPendientes extends modeloExtend
{
	public function obtenerNumTrabajosPracticosPorRealizar($idalumno, $idcurso)
	{
		/*$sql = 'SELECT COUNT(at.idarchivo_temario) AS num FROM archivo_temario AS at' .
		' LEFT JOIN calificacion_trabajo_practico AS ctp ON ctp.idarchivo_temario = at.idarchivo_temario' .
		' WHERE ctp.nota_trabajo IS NULL';*/
		
		/*
		 * $sql="SELECT * from archivo_temario T, categoria_archivo CA 
		where T.idmodulo = ".$this->idmodulo."
		and T.idcategoria_archivo = CA.idcategoria_archivo ";
		 */
		
		$sql = 'SELECT at.idarchivo_temario AS num FROM archivo_temario AS at' .
		' LEFT JOIN calificacion_trabajo_practico AS ctp ON (ctp.idarchivo_temario = at.idarchivo_temario AND ctp.idalumnos = ' . $idalumno . ' AND ctp.idcurso = ' . $idcurso . ')' .
		' LEFT JOIN temario AS tm ON tm.idcurso = at.idcurso' .
		' LEFT JOIN curso AS c ON c.idaccion_formativa = tm.idaccion_formativa' .
		' LEFT JOIN matricula AS m ON m.idcurso = c.idcurso' .
		' LEFT JOIN centros_convocatorias AS ccv ON ccv.idconvocatoria = c.idconvocatoria' .
		' WHERE ctp.nota_trabajo IS NULL AND m.idcurso = ' . $idcurso . ' AND at.idcategoria_archivo = 2' .
		' AND m.idalumnos = ' . $idalumno . ' AND at.activo = 1' .
		' GROUP BY at.idarchivo_temario';		
	
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerNumAutoevalPorRealizar($idalumno, $idcurso)
	{
		$sql = 'SELECT ae.idautoeval' .
		' FROM autoeval AS ae' .
		' LEFT JOIN test AS t ON t.idautoeval = ae.idautoeval' .
		' LEFT JOIN temario AS tm ON tm.idcurso = t.idcurso' .
		' LEFT JOIN curso AS c ON c.idaccion_formativa = tm.idaccion_formativa' .
		' LEFT JOIN matricula AS m ON m.idcurso = c.idcurso AND m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' LEFT JOIN calificaciones AS cf ON (cf.idautoeval = ae.idautoeval AND m.idmatricula = cf.idmatricula)' .
		' WHERE cf.nota IS NULL AND m.idalumnos = ' . $idalumno .
		' AND m.idcurso = ' . $idcurso .
		' AND ae.borrado = 0' .
		' GROUP BY ae.idautoeval';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosRecomendacionHoy($idCurso)
	{
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT agr.idagenda_recomendacion AS idagenda, agr.contenido, agr.fecha' .
		' FROM agenda_recomendacion AS agr' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' WHERE agr.id_recomendacion = cr.idrecomendacion AND agr.fecha = "' . $fechaIni . '"' .
		' GROUP BY agr.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	

	public function obtenerEventosHoy($idCurso, $idUsuario)
	{
		$fechaIni = Fecha::fechaActual(false);
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha' .
		' FROM agenda AS ag' .
		' WHERE ag.idcurso = ' . $idCurso . ' AND ag.idusuario = "' . $idUsuario . '" AND ag.fecha = "' . $fechaIni . '"' .
		' GROUP BY ag.idagenda ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumeroScorm($idCurso)
	{
		$sql = "SELECT S.idscorm, S.idscorm_folder" .
		" FROM scorm AS S" .
		" LEFT JOIN temario AS T ON T.idmodulo = S.idmodulo" .
		" LEFT JOIN accion_formativa AS AF ON AF.idaccion_formativa = T.idaccion_formativa" .
		" LEFT JOIN curso AS C ON C.idaccion_formativa = AF.idaccion_formativa" .
		" WHERE C.idcurso = " . $idCurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEstadoScorm($idScorm, $idUsuario, $idCurso)
	{
		$sql = "SELECT sv.varValue" .
		" FROM scormvars AS sv" .
		" LEFT JOIN scorm_alumnos AS sa ON sa.SCOInstanceID = sv.SCOInstanceID" .
		" WHERE sa.idscorm = " . $idScorm .
		" AND sa.idusuario = " . $idUsuario .
		" AND sa.idcurso = " . $idCurso .
		" AND sv.varName = 'cmi.core.lesson_status'";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
			
	}
}