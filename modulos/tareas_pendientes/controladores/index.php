<?php
$idAlumno = Usuario::getIdUser();
$idCurso = Usuario::getIdCurso();

$objModelo = new ModeloTareasPendientes();

//Numero de emensajes en opiniones sin leer
require_once(PATH_ROOT.'modulos/opiniones/controladores/opiniones_sinleer.php');

mvc::cargarModuloSoporte('foro');
$objForo = new modeloForo();
$foros = $objForo->mensajesSinLeerForo(Usuario::getIdCurso());
$numeroMensajesForoSinLeer = 0;

mvc::cargarModuloSoporte('notas');
$objModeloNotas = new ModeloNotas();

mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$temasLogArray = array();
$temasLog = $objForo->obtenerForoLog($_SESSION['idcurso'], Usuario::getIdUser(true));
while($row = $temasLog->fetch_object())
{
	$temasLogArray[$row->idforo_temas] = $row->fechaUltConexion;
}

$temas = $objForo->obtenerTemas($_SESSION['idcurso']);
if($temas->num_rows > 0)
{
	while($tema = $temas->fetch_object())
	{
		$fechaLog = null;
		if(isset($temasLogArray[$tema->idforo_temas]))
		{
			$fechaLog = $temasLogArray[$tema->idforo_temas];
		}
					
		$newMsg = $objForo->obtenerNuevosMsg($idAlumno, $idCurso, $tema->idforo_temas, $fechaLog);
		
		if($newMsg->num_rows > 0)
		{
			$numeroMensajesForoSinLeer += $newMsg->num_rows;
		}
	}
}

// obetener los trabajos practicos no realizados por el alumno
$numTrabjPract = $objModelo->obtenerNumTrabajosPracticosPorRealizar($idAlumno, $idCurso);
if($numTrabjPract->num_rows > 0)
{
	//$numTrabjPract = $numTrabjPract->fetch_object();
	$numTrabjPract =  $numTrabjPract->num_rows;
}
else
{
	$numTrabjPract = 0;
}

// obtener las autoevaluaciones no realizadas por el alumno
$numAutoeval = $objModelo->obtenerNumAutoevalPorRealizar($idAlumno, $idCurso);
if($numAutoeval->num_rows > 0)
{
	$numAutoeval = $numAutoeval->num_rows;
}
else
{
	$numAutoeval = 0;
}

//obtener el numero de recomendacione pendientes por el tutor
$numRecomendaciones = $objModelo->obtenerEventosRecomendacionHoy($idCurso);
if($numRecomendaciones->num_rows > 0)
{
	$numRecomendaciones	= $numRecomendaciones->num_rows;
}
else
{
	$numRecomendaciones = 0;
}

//obtener el numero de eventos propios de la agenda
$numRecomendacionesPers = $objModelo->obtenerEventosHoy($idCurso, Usuario::getIdUser(true));
if($numRecomendacionesPers->num_rows > 0)
{
	$numRecomendacionesPers = $numRecomendacionesPers->num_rows;
}
else
{
	$numRecomendacionesPers = 0;
}

// correos sin leer
mvc::cargarModuloSoporte('gestor_correos');
$objModeloGestorCorreo = new Gestor_correos();
$numCorreos = $objModeloGestorCorreo->mensajes_sin_leer();
$numCorreos = $numCorreos->num_rows;

// tablon de anuncio de aula (mensajes sin leer)
mvc::cargarModuloSoporte('tablon_anuncio');
$objModeloTablonAnuncio = new TablonAnuncio();
$numTablonAnuncio = $objModeloTablonAnuncio->numero_anuncios();
$numTablonAnuncio = $numTablonAnuncio->num_rows;

// conocer si hay alguna tutoria abierta
$ct_idrrhh = null;
$ct_idcurso = null;
mvc::cargarModuloSoporte('chat_tutoria');
$objModeloChatTutoria = new ModeloChatTutoria();
if(Usuario::compareProfile('alumno'))
{
	$ct_idcurso = Usuario::getIdCurso();
}
else
{
	$ct_idrrhh = Usuario::getIdUser(true);
}

/* STATS DEL CURSO */

mvc::cargarModuloSoporte('notas');
$objNotas = new Notas();
$objModeloNotas = new ModeloNotas();

$objNotas->setCurso($idCurso);

$configNotas = $objModeloNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

$resultado_configuracion = $objModeloNotas->obtenerConfigPorcentajes($idCurso);
$configuracion = mysqli_fetch_assoc($resultado_configuracion);
$notaTrabPract = $objNotas->calcularMediaTrabajosPract($idAlumno, $configuracion['num_trabajos'], $configuracion['trabajos_practicos'], false);
$notaEval = $objNotas->calcularNotaMediaAutoeval($idAlumno, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);
$notaForo = $objNotas->calcularNotaMediaForo($idAlumno, $idCurso, false);

/* ------ */

$tutoria = $objModeloChatTutoria->obtenerTutoria($ct_idrrhh, $ct_idcurso);

/************************************************************************************************************************
Obtener los porcentajes de cada modulo
************************************************************************************************************************/
$numTemas = $objModeloNotas->obtenerNumTemas($idCurso);
$numTemas = $numTemas->num_rows;
//El numero de temas en los que ha participado el alumno
$temas = $objModeloNotas->obtenerTemas($idAlumno, $idCurso);
$numTemasParticipado = $temas->num_rows;

$porcentajeForo = ($numTemasParticipado * 100) / $numTemas; 

//Obtener el numero de test del curso
$numTest = $objModeloNotas->obtenerNumTestCurso($idCurso);
if($numTest->num_rows > 0)
{
	$numTest = $numTest->fetch_object();
	$numTest = $numTest->numTestCurso;
}

$idMatricula = Usuario::getIdMatriculaDatos($idAlumno, $idCurso);
$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);
$numTestParticipado = $numTestAlumno->num_rows;
$testSuperado = 0;

if($numTestAlumno->num_rows > 0)
{
	while($testParticipado = $numTestAlumno->fetch_object())
	{
		if($testParticipado->nota >= 5)
		{
			//$porcentajeTestTp += $valorTareaObligatoria; 
			$testSuperado++;
		}
	}
}

$porcentajeTest = ($testSuperado * 100) / $numTest;

//Obtener el numero de trabajos practicos de un curso
$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($idCurso);
if($numTrabajos->num_rows > 0)
{
	$numTrabajos = $numTrabajos->fetch_object();
	$numTrabajos = $numTrabajos->numTrabajosPracticos;
}
$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($idAlumno, $idCurso);
$trabajosParticipado = $trabajosParticipado->num_rows;

if($numTrabajos > 0)
{
	$porcentajeTrabajos = ($trabajosParticipado * 100) / $numTrabajos;		
}
else
{
	$porcentajeTrabajos = 0;	
}

$porcentajeTotal = ($porcentajeTrabajos + $porcentajeTest + $porcentajeForo) / 3;

//Obtengo la nota media y el porcentaje SCORM

$numScorms = 0;
$notaMediaScorm = 0;

$valorScorm = array();
$scormsCurso = $objModelo->obtenerNumeroScorm($idCurso);


if($scormsCurso->num_rows > 0)
{
	//Cuento cuantos SCORM hay en el curso y hago recuento
	while ($scormCurso = $scormsCurso->fetch_object()) 
	{
		if(file_exists(PATH_ROOT . 'archivos/scorm/' . $scormCurso->idscorm . '/sco_' . $scormCurso->idscorm_folder . '/default.html'))
		{
			$numScorms++;
			$estadoScorm = $objModelo->obtenerEstadoScorm($scormCurso->idscorm, $idAlumno, $idCurso);
			if($estadoScorm->num_rows > 0)
			{
				$estadoScorm = $estadoScorm->fetch_object();
				$valorScorm[] = $estadoScorm->varValue; 
			}
		}	
	}
		
	// Si el curso dispone de SCORM le calculo la nota media y el porcentaje en el curso
	if($numScorms > 0)
	{
		$notaScorm = 10 / $numScorms;
		if(isset($valorScorm) && count($valorScorm) > 0)
		{
			foreach($valorScorm as $nScorm)
			{
				//echo $nScorm;
				if($nScorm == 'completed') $notaMediaScorm = $notaMediaScorm + $notaScorm;
				else if($nScorm == 'browsed') $notaMediaScorm = $notaMediaScorm + ($notaScorm / 2);
			}
		}
		$porcentajeScorm = $notaMediaScorm * 10;	
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');