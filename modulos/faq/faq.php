<?php
class Faq
{	
	public static function paginar($paginaActual, $paginaMax, $url)
	{
		$numMax = 3;
		$pageIni = 1;
		
		// calcula la pagina donde se empieza a mostrar el paginado
		if(($numMax - $paginaActual) < 0)
		{
			$pageIni = $paginaActual - $numMax;
		}
		
		$html = '<div class="paginado fright"><ul>';
		
		// volver a la primera pagina
		if($pageIni > 1)
		{
			$html .= '<li><a data-page="1" href="' . $url . '1" title="Ir a la primera p&aacute;gina">Primera</a></li><li>...</li>';
		}
		
		// mostrar paginas antes de la pagina actual
		for($cont = $pageIni; $cont < $paginaActual; $cont++)
		{
			$html .= '<li><a data-page="' . $cont . '" href="' . $url . $cont . '" title="Ir a la p&aacute;gina ' . $cont . '">' . $cont . '</a></li>';
		}
		
		$html .= '<li><a data-page="' . $paginaActual . '" href="#" onclick="return false;" class="negrita">' . $paginaActual . '</a></li>';
		
		// mostrar paginas despues de la actual
		if($paginaMax > $pageIni)
		{
			$pageNext = ($paginaActual + 1);
			$cont = 0;
			while($cont < 3 && $pageNext <= $paginaMax)
			{
				$html .= '<li><a data-page="' . $pageNext . '" href="' . $url . $pageNext . '" title="Ir a la p&aacute;gina ' . $pageNext . '">' . $pageNext . '</a></li>';
				$pageNext++;
				$cont++;
			}
		}
		
		// ir a la ultima pagina
		if($paginaMax > ($paginaActual + 3))
		{
			$html .= '<li>...</li><li><a data-page="' . $paginaMax . '" href="' . $url . $paginaMax .  '" title="Ir a la &uacute;ltima p&aacute;gina">&Uacute;ltima</a></li>';
		}
		
		$html .= '</ul><div class="clear"></div></div><div class="clear"></div>';
		
		return $html;
	}
}