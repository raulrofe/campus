<?php
class ModeloFaq extends modeloExtend
{
	public function insertarFaq($pregunta, $respuesta)
	{
		$sql = 'INSERT INTO faq (pregunta, respuesta) VALUES ("' . $pregunta . '", "' . $respuesta . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function buscarFaq($busqueda, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$busqueda = preg_quote($busqueda, '-');
		
		$sql = 'SELECT * FROM faq WHERE pregunta LIKE "%' . $busqueda . '%" OR respuesta LIKE "%'. $busqueda . '%"' . $addQuery;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnaFaq($idFaq)
	{		
		$sql = 'SELECT * FROM faq WHERE idfaq = ' . $idFaq;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFaq($idFaq, $pregunta, $respuesta)
	{
		$sql = 'UPDATE faq SET pregunta = "' . $pregunta . '", respuesta = "' . $respuesta . '" WHERE idfaq = ' . $idFaq;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarFaq($idFaq)
	{		
		$sql = 'DELETE FROM faq WHERE idfaq = ' . $idFaq;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}