	<br/>
	<div class="popupIntro">
			<div class='subtitle t_center'>
		<img src="imagenes/faq/faq.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="faqs.titulo">
			CONSULTAS FRECUENTES
		</span>
	</div><div>
		<?php if($_SESSION['perfil'] != 'alumno'):?>
			<p class='t_justify' data-translate-html="faqs.descripcion">
				De forma simult&aacute;nea a la impartici&oacute;n del curso el/la tutor/a realiza una labor de 
				recopilaci&oacute;n y selecci&oacute;n de aquellas consultas que el alumnado le plantea con mayor frecuencia, as&iacute; como 
				de aqu&eacute;llas que valora de mayor inter&eacute;s para el resto del grupo.
			</p>
			<br/>
			<p class='t_justify' data-translate-html="faqs.descripcion2">
				Esta secci&oacute;n le ofrece la posibilidad de introducir nuevas consultas, con sus correspondientes respuestas y, 
				una vez que accede a su contenido, realizar modificaciones o eliminar aqu&eacute;llas que han quedado obsoletas.
			</p>
		<?php else: ?>
			<p class='t_justify' data-translate-html="faqs.descripcion">
				De forma simult&aacute;nea a la impartici&oacute;n del curso, el/la tutor/a realiza una labor de 
				recopilaci&oacute;n y selecci&oacute;n de aquellas consultas que el alumnado le plantea con mayor frecuencia, as&iacute; como 
				de aqu&eacute;llas que valora de mayor inter&eacute;s para el resto del grupo.
			</p>
		<?php endif; ?>
	</div>
