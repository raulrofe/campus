<div id="ayuda_faq">

	<?php require('intro_faq.php'); ?>	
		
	<?php if(Usuario::compareProfile('alumno', 'supervisor')):?>
		<div id="ayuda_faq_busqueda">
			<div class='subtitleModulo fleft' data-translate-html="faqs.preguntas">
				Preguntas frecuentes
			</div>
		
			<div class="busquedaFAQ" style="margin-left: 20px;">
				<form method="post" action="">
					<div class='fleft'>
						<input type="text" placeholder='Palabra de b&uacute;squeda' name="busqueda" value="<?php if(isset($post, $post['busqueda'])) echo $post['busqueda']?>" class='inputBusqueda'
							    data-translate-placeholder="faqs.busqueda"/>
					</div>
					<div class='clear'></div>
				</form>
			</div>

			<div class="clear"></div>
		</div>

	<?php elseif(Usuario::compareProfile('tutor', 'coordinador')): ?>

		<div id="ayuda_faq_busqueda">
			<div class="busquedaFAQ" style="margin-left: 20px;">
				<form method="post" action="">
					<div class='fleft'>
						<input type="text" placeholder='Palabra de b&uacute;squeda' name="busqueda" value="<?php if(isset($post, $post['busqueda'])) echo $post['busqueda']?>" class='inputBusqueda'
							    data-translate-placeholder="faqs.busqueda"/>
					</div>
					<div class='clear'></div>
				</form>
			</div>
			
			<div class="menu_interno">
				<div class="fleft elemento_menuinterno" style="margin: 26px 0 8px 0;">
					<a href="ayuda/faqs/insertar" data-translate-html="faqs.nueva">
						Introducir nueva consulta
					</a>
				</div>
			</div>

			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<!-- <div class='subtitleModulo t_center'>Preguntas frecuentes</div> -->
	
	<?php endif; ?>
	
	<div id="listarFaqs" class="listarElementos">
		<?php if($result->num_rows > 0):?>
			<?php while($row = $result->fetch_object()):?>
				<div class="elemento" style="margin:0 20px 10px 20px" data-page="<?php echo $countNumPage?>">
					<div class="elementoContenido">
						<p class='preguntaFaq negrita' style='margin-bottom:5px;'><?php echo Texto::textoPlano($row->pregunta)?></p>
						<p class='respuestaFaq'><?php echo Texto::textoFormateado($row->respuesta)?></p>
					</div>
					<?php if(Usuario::getProfile() == 'tutor'):?>
						<div class="elementoPie hide">
							<ul class="elementoListOptions fright">
								<li>
									<a rel="tooltip" href="ayuda/faq/editar/<?php echo $row->idfaq?>" data-translate-title="faqs.editar" title="Editar FAQ">
										<img src="imagenes/options/edit.png" alt="" />
									</a>
								</li>
								<li>
									<a rel="tooltip" href="#" <?php echo Alerta::alertConfirmOnClick('borrarfaq', '¿Deseas borrar esta faq?', 'ayuda/faq/eliminar/' . $row->idfaq)?>  data-translate-title="faqs.eliminar" title="Eliminar FAQ">
										<img src="imagenes/options/delete.png" alt="" />
									</a>
								</li>
							</ul>
							<div class="clear"></div>
						</div>
					<?php endif; ?>
				</div>
				<?php
					if($countPaging >= $maxElementsPaging)
					{
						$countNumPage++;
						$countPaging = 1;
					}
					else
					{
						$countPaging++;
					}
				?>
			<?php endwhile;?>
		<?php else:?>
			<div class="t_center" data-translate-html="faqs.noexisten">
				No existen preguntas fecuentes en este momento
			</div>
		<?php endif;?>
	</div>
</div>
<span id="faqPaginaActual" class="hide"><?php echo $numPagina?></span>

<?php echo Faq::paginar($numPagina, $maxPaging, 'ayuda/faq/pagina/')?>

<script type="text/javascript" src="js-faq-default.js"></script>
