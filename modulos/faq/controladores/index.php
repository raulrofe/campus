<?php
$objModeloFaq = new ModeloFaq();

$idUsuario = Usuario::getIdUser();

$idMatricula = Usuario::getIdMatricula();	
$idPerfil = Usuario::obtenerIdPerfilLogueo();

$get = Peticion::obtenerGet();

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

/*
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['busqueda']))
	{
		$result = $objModeloFaq->buscarFaq($post['busqueda']);
	}
	else
	{
		$result = $objModeloFaq->buscarFaq('');
	}
}
else
{
	$result = $objModeloFaq->buscarFaq('');
}
*/

// para el paginado con ajax
$countPaging = 1;
$countNumPage = 1;

$result = $objModeloFaq->buscarFaq('');
$maxElementsPaging = 8;
$maxPaging = $result->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		
//$result = $objModeloFaq->buscarFaq('', $elementsIni, $maxElementsPaging);

//$result = $objModeloFaq->buscarFaq('');

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');