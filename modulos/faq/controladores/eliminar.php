<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}
//////// --------------------------------- ///////////

$get = Peticion::obtenerGet();
if(isset($get['idfaq']) && is_numeric($get['idfaq']))
{
	$objModeloFaq = new ModeloFaq();
	
	$row = $objModeloFaq->obtenerUnaFaq($get['idfaq']);
	if($row->num_rows == 1)
	{	
		if($objModeloFaq->eliminarFaq($get['idfaq']))
		{
			Alerta::guardarMensajeInfo('preguntarespuestaeliminada','Se ha eliminado la pregunta/respuesta');
			Url::redirect('ayuda/faqs');
		}
	}
}