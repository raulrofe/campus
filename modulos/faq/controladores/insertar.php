<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}
//////// --------------------------------- ///////////

$objModeloFaq = new ModeloFaq();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(!empty($post['pregunta']) && ($post['respuesta']))
	{
		if($objModeloFaq->insertarFaq($post['pregunta'], $post['respuesta']))
		{
			Alerta::mostrarMensajeInfo('anadidopreguntarespuesta','Se ha a&ntilde;adido la pregunta/respuesta');
			Url::redirect('ayuda/faqs');
		}
	}
	else Alerta::mostrarMensajeInfo('preguntarespuestanovacia','La pregunta o respuesta esta vacia');
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'insertar');