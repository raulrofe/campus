<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}
//////// --------------------------------- ///////////

$get = Peticion::obtenerGet();
if(isset($get['idfaq']) && is_numeric($get['idfaq']))
{
	$objModeloFaq = new ModeloFaq();
	
	$row = $objModeloFaq->obtenerUnaFaq($get['idfaq']);
	if($row->num_rows == 1)
	{	
		$row = $row->fetch_object();
		
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			if(isset($post['pregunta'], $post['respuesta']))
			{
				if($objModeloFaq->actualizarFaq($get['idfaq'], $post['pregunta'], $post['respuesta']))
				{
					Alerta::guardarMensajeInfo('preguntarespuesta','Se ha actualizado la pregunta/respuesta');
					Url::redirect('ayuda/faqs');
				}
			}
		}
	
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar');
	}
}