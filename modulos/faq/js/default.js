$(document).ready(function()
{	
	FaqNumPag = 1;
	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#ayuda_faq_busqueda .inputBusqueda').keyup(function(event)
	{
		seguimientoBuscar();
	});
	
	faqInit();
});

function faqInit()
{
	// carga pag principal
	var page = $('#faqPaginaActual').html();
	faqPaginado(page);
	
	$('#ayuda_faq .paginado a').on('click', function(e)
	{
		var page = 1;
		if($(this).attr('data-page') != undefined)
		{
			page = $(this).attr('data-page');
		}
		
		faqPaginado(page);
		
		return false;
		//e.preventDefault();
	});
}

function faqPaginado(page)
{
	$('#listarFaqs').find('.elemento').addClass('hide');
	$('#listarFaqs').find('.elemento[data-page="' + page + '"]').removeClass('hide');
	
	$('#ayuda_faq .paginado li a').removeClass('negrita');
	$('#ayuda_faq .paginado li a[data-page="' + page + '"]').addClass('negrita');
}

// BUSCADOR DE CURSOS INACTIVOS POR CONVOCATORIA
function seguimientoBuscar()
{
	var wordSearch = $('#ayuda_faq_busqueda .inputBusqueda').val();
	
	$('#listarFaqs .elemento').addClass('hide');

	// remove subrayado
	$('#listarFaqs .elemento .subrayadoAmarillo').each(function(index)
	{
		$(this).replaceWith($(this).html());
	});
	
	if(wordSearch != "")
	{
		var inSearch = '';
		$('#listarFaqs .elemento').each(function(index)
		{
			inSearch = $(this).find('.preguntaFaq').text() + ' ' + $(this).find('.respuestaFaq').text();
			
			if(inSearch != undefined)
			{
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('(' + wordSearch + ')', 'ig');
				if(inSearch.match(regex))
				{
					$(this).removeClass('hide');
					
					// subraya
					var regex2 = new RegExp('(' + wordSearch + ')', 'ig');
					
					var preguntaText = $(this).find('.preguntaFaq').text();
					$(this).find('.preguntaFaq').html(
						preguntaText.replace(regex2, '<span class="subrayadoAmarillo">$1</span>')
					);
					
					var respuestaText = $(this).find('.respuestaFaq').text();
					$(this).find('.respuestaFaq').html(
						respuestaText.replace(regex2, '<span class="subrayadoAmarillo">$1</span>')
					);
				}
			}
		});
	}
	else
	{		
		var page = $('#ayuda_faq .paginado li a.negrita').attr('data-page');
		faqPaginado(page);
	}
}