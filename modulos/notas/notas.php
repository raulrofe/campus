<?php
class Notas
{
	private $_objModelo;
	//private static $_numModulos = null;
	private $_idCurso;

	public function __construct()
	{
		$this->_objModelo = new ModeloNotas();
	}

	public function setCurso($idCurso)
	{
		$this->_idCurso = $idCurso;
	}

	/*
	private function _getNumCursos()
	{
		$numModulos = $this->_objModelo->obtenerModulosPorCurso($this->_idCurso);
		self::$_numModulos = $numModulos->num_rows;
	}
	*/

	public function calcularMediaTrabajosPract($idAlumno, $configNotas, $porcentajeMedia, $calcularMedia = true)
	{
		/*
		if(!isset(self::$_numModulos))
		{
			self::_getNumCursos();
		}
		*/

		$notaTrabajosPract = 0;
		$idmoduloTP = 0;
		$arrayNotasTP = array();
		$notasTP = 0;
		$resultadoTP = 0;
		$num_trabajo = 0;

		$calfTrabajosPract = $this->_objModelo->obtenerCalfTrabajosPract($idAlumno, $this->_idCurso);
		if($calfTrabajosPract->num_rows > 0)
		{
			while($item = $calfTrabajosPract->fetch_object())
			{
				if($idmoduloTP != $item->idmodulo)
				{
					$idmoduloTP = $item->idmodulo;
					$arrayNotasTP[$idmoduloTP] = array();
				}

				$arrayNotasTP[$idmoduloTP][] = $item->nota_trabajo;
				$notasTP += $item->nota_trabajo;
				$num_trabajo++;
			}

			// variable que se usa para almacenar los trabajos practicos obligatorios no realizados
			$masTP = 0;
			foreach($arrayNotasTP as $modulo)
			{
				if(count($modulo) < $configNotas)
				{
					$masTP += $configNotas- count($modulo);
				}
			}

			$resultadoTP = round($notasTP / ($num_trabajo + $masTP), 2);
		}

		if($calcularMedia)
		{
			$resultadoTP = round(($resultadoTP) * ($porcentajeMedia / 100), 2);
		}
		return $resultadoTP;
	}

	public function calcularNotaMediaForo($idAlumno, $idCurso, $calcularMedia = true)
	{
		$foroPuntos = 0;

		$configNotas = $this->_objModelo->obtenerConfigPorcentajes($idCurso);

		$configNotas = $configNotas->fetch_object();

		$configNotaForo = $configNotas->foro;
		$configNotaForoNumMsg = $configNotas->num_msg_foro;

		$numTemas = $this->_objModelo->obtenerNumTemas($idCurso);
		$numTemas = $numTemas->num_rows;

		$notaTema = 10 / $numTemas;
		$notaTema;

		if($configNotaForoNumMsg > 0)
		{
			$notaMensaje = $notaTema / $configNotaForoNumMsg;
		}
		else
		{
			$notaMensaje = 0;
		}

		$notaMensaje;

		// obtenemos los datos del alumno
		$alumno = $this->_objModelo->obtenerUnAlumno($idAlumno, $idCurso);
		$alumno = $alumno->fetch_object();

		$mediaForo = $alumno->media_foro;

		$mediaForo;


		//Si comento esto la nota media del alumno no queda como preestablecida
		//if($mediaForo == null)
		//{
			$foroPuntos = 0;

			$temas = $this->_objModelo->obtenerTemas($idAlumno, $idCurso);

			if($temas->num_rows > 0)
			{
				while($tema = $temas->fetch_object())
				{
					//Obtengo la nota por mensajes escritos
					if($tema->numMsg >= $configNotaForoNumMsg)
					{
						$foroPuntos += $configNotaForoNumMsg * $notaMensaje;
					}
					else
					{
						$foroPuntos += $tema->numMsg * $notaMensaje;
					}
				}
			}

			if($calcularMedia)
			{
				$foroPuntos = ($foroPuntos * $configNotaForo) / 100;
			}
		//}
		//else
		//{
			//$foroPuntos = $mediaForo;
		//}

		$foroPuntos;//exit;

		return $foroPuntos;
	}

	public function calcularNotaMediaAutoeval($idAlumno, $idCurso, $configNota, $tipo = 'media', $calcularMedia = true)
	{
		$notaReturn = 0;
		//echo $tipo;
		switch($tipo)
		{
			case 1: //'ultima':
				$notaFinal = $this->_calcularNotaAutoeval_Ultima($idAlumno, $idCurso, $configNota);
				break;

			case 2: //'alta':
				$notaFinal = $this->_calcularNotaAutoeval_Alta($idAlumno, $idCurso, $configNota);
				break;

			case 3: //'media':
				$notaFinal = $this->_calcularNotaAutoeval_Media($idAlumno, $idCurso, $configNota);
				break;

			default:
				$notaFinal = 0;
				break;
		}

		if($calcularMedia)
		{
			$notaFinal = ($notaFinal * ($configNota / 100));
		}

		/*if($notaFinal > ($configNota / 10))
		{
			$notaFinal = 2;
		}*/

		$notaReturn = round($notaFinal, 2);

		return $notaReturn;
	}

	/*private function _calcularNotaAutoeval_Alta($idAlumno, $idCurso)
	{
		$notaFinal = 0;

		$rowNota = $this->_objModelo->obtenerNotasTestsMasAlta($idAlumno, $idCurso);
		if($rowNota->num_rows == 1)
		{
			$rowNota = $rowNota->fetch_object();
			$notaFinal = $rowNota->nota;
		}

		return $notaFinal;
	}*/

	/*private function _calcularNotaAutoeval_Ultima($idAlumno, $idCurso)
	{
		$notaFinal = 0;

		$rowNota = $this->_objModelo->obtenerNotasTestsUltima($idAlumno, $idCurso);
		if($rowNota->num_rows == 1)
		{
			$rowNota = $rowNota->fetch_object();
			$notaFinal = $rowNota->nota;
		}

		return $notaFinal;
	}*/

	private function _calcularNotaAutoeval_Media($idAlumno, $idCurso, $configNota)
	{
		$notaFinalTests = 0;
		$numTests = 0;

		$objCurso = new Cursos();
		$objCurso->set_curso($idCurso);
		$rowCurso = $objCurso->buscar_curso();
		$rowCurso = $rowCurso->fetch_object();
		$numTestsMin = $rowCurso->num_trabajos;

		// numero de test de este curso
		/*$numTests = $this->_objModelo->obtenerNumTests($idCurso);
		$numTests = $numTests->fetch_object();
		$numTests = $numTests->numTests;*/

		$notasTests = $this->_objModelo->obtenerNotasTestsParaMedia($idAlumno, $idCurso);

		if($notasTests->num_rows > 0)
		{
			$idautoeval = 0;
			$notasPorTest = array();

			while($notaTest = $notasTests->fetch_object())
			{
				if($idautoeval != $notaTest->idautoeval)
				{
					if(count($notasPorTest) > 0)
					{
						$numTestDivisor = $numTests;
						if(count($numTests) < $numTestsMin)
						{
							$numTestDivisor = $numTestsMin;
						}

						$numTests++;
						$notaFinalTests += (array_sum($notasPorTest) / $numTestDivisor);
					}

					$notasPorTest = array();
				}

				$notasPorTest[] = $notaTest->nota;

				$idautoeval = $notaTest->idautoeval;
			}

			//  el ultimo recorrido del array
			if(count($notasPorTest) > 0)
			{
				$numTestDivisor = $numTests;
				if(count($numTests) < $numTestsMin)
				{
					$numTestDivisor = $numTestsMin;
				}

				$notaFinalTests += (array_sum($notasPorTest) / $numTestDivisor);
				$numTests++;
			}
		}

		// numero minimo de test obligatorios
		if($numTestsMin > $numTests)
		{
			$numTests = $numTestsMin;
		}

		if($numTests > 0)
		{
			$mediaTests = ($notaFinalTests / $numTests);
			//$mediaTests = ($notaFinalTests / $numTests) * ($configNota / 100);
			//$mediaTests = ($configNota * $mediaTests) / 100;
		}
		else
		{
			$mediaTests = 0;
		}

		return $mediaTests;
	}

	private function _calcularNotaAutoeval_Ultima($idAlumno, $idCurso, $configNota)
	{
		$notaFinalTests = 0;
		$numTests = 0;

		$objCurso = new Cursos();
		$objCurso->set_curso($idCurso);
		$rowCurso = $objCurso->buscar_curso();
		$rowCurso = $rowCurso->fetch_object();
		$numTestsMin = $rowCurso->num_trabajos;

		// numero de test de este curso
		/*$numTests = $this->_objModelo->obtenerNumTests($idCurso);
		$numTests = $numTests->fetch_object();
		$numTests = $numTests->numTests;*/

		$notasTests = $this->_objModelo->obtenerNotasTestsParaMedia($idAlumno, $idCurso);

		if($notasTests->num_rows > 0)
		{
			$idautoeval = 0;
			$notasPorTest = array();

			while($notaTest = $notasTests->fetch_object())
			{
				if($idautoeval != $notaTest->idautoeval)
				{
					if(count($notasPorTest) > 0)
					{
						$numTests++;

						$notaUltima = array_pop($notasPorTest);
						$notaFinalTests += $notaUltima;
					}

					$notasPorTest = array();
				}

				$notasPorTest[] = $notaTest->nota;

				$idautoeval = $notaTest->idautoeval;
			}

			//  el ultimo recorrido del array
			if(count($notasPorTest) > 0)
			{
				$notaUltima = array_pop($notasPorTest);
				$notaFinalTests += $notaUltima;
				$numTests++;
			}
		}

		// numero minimo de test obligatorios
		if($numTestsMin > $numTests)
		{
			$numTests = $numTestsMin;
		}

		if($numTests > 0)
		{
			$mediaTests = ($notaFinalTests / $numTests);
			//$mediaTests = ($notaFinalTests / $numTests) * ($configNota / 100);
			//$mediaTests = ($configNota * $mediaTests) / 100;
		}
		else
		{
			$mediaTests = 0;
		}

		return $mediaTests;
	}

	private function _calcularNotaAutoeval_Alta($idAlumno, $idCurso, $configNota)
	{
		$notaFinalTests = 0;
		$numTests = 0;

		$objCurso = new Cursos();
		$objCurso->set_curso($idCurso);
		$rowCurso = $objCurso->buscar_curso();
		$rowCurso = $rowCurso->fetch_object();
		$numTestsMin = $rowCurso->num_trabajos;

		// numero de test de este curso
		/*$numTests = $this->_objModelo->obtenerNumTests($idCurso);
		$numTests = $numTests->fetch_object();
		$numTests = $numTests->numTests;*/

		$notasTests = $this->_objModelo->obtenerNotasTestsParaMedia($idAlumno, $idCurso);

		if($notasTests->num_rows > 0)
		{
			$idautoeval = 0;
			$notasPorTest = array();

			while($notaTest = $notasTests->fetch_object())
			{
				if($idautoeval != $notaTest->idautoeval)
				{
					if(count($notasPorTest) > 0)
					{
						$numTests++;
						$notaFinalTests += max($notasPorTest);
					}

					$notasPorTest = array();
				}

				$notasPorTest[] = $notaTest->nota;

				$idautoeval = $notaTest->idautoeval;
			}

			//  el ultimo recorrido del array
			if(count($notasPorTest) > 0)
			{
				$notaFinalTests += max($notasPorTest);
				$numTests++;
			}
		}

		// numero minimo de test obligatorios
		if($numTestsMin > $numTests)
		{
			$numTests = $numTestsMin;
		}

		if($numTests > 0)
		{
			$mediaTests = ($notaFinalTests / $numTests);
			//$mediaTests = ($notaFinalTests / $numTests) * ($configNota / 100);
			//$mediaTests = ($configNota * $mediaTests) / 100;
		}
		else
		{
			$mediaTests = 0;
		}

		return $mediaTests;
	}

	public function calcularPorcentajeTest($numTest, $numTestParticipacion)
	{
		return ($numForoParticipacion * 100) / $configForo;
	}
}