<?php
class ModeloNotas extends modeloExtend
{
	public function obtenerAlumnos($idCurso)
	{
		//$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM matricula AS m LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos WHERE m.idcurso = ' . $idCurso;
		$sql = 'SELECT al.idalumnos, CONCAT(al.apellidos, ", ", al.nombre) AS nombrec, m.media_foro, m.nota_coordinador' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE m.idcurso = ' . $idCurso . ' ORDER BY al.apellidos ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnosActivos($idCurso)
	{
		//$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM matricula AS m LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos WHERE m.idcurso = ' . $idCurso;
		$sql = 'SELECT al.idalumnos, al.dni, CONCAT(al.apellidos, ", ", al.nombre) AS nombrec, m.media_foro, m.nota_coordinador' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE m.idcurso = ' . $idCurso . 
		' AND m.borrado = 0' .
		' AND al.borrado = 0' .
		' ORDER BY al.apellidos ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarNotaCoordinador($idalumno, $idcurso, $nota)
	{
		$sql = 'UPDATE matricula SET nota_coordinador = ' . $nota . ' WHERE idalumnos = ' . $idalumno . ' AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarNotaForo($idAlumno, $idCurso, $notaForo)
	{
		$sql = 'UPDATE matricula SET media_foro = ' . $notaForo . ' WHERE idalumnos = ' . $idAlumno . ' AND idcurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/*public function obtenerMedias($idCurso)
	{
		$sql = 'SELECT AVG() FROM autoeval AS ae WHERE ';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}*/
	
	public function obtenerDatosForo($idAlumno, $idCurso)
	{
		$sql = 'SELECT al.idalumnos, fm.idforo_temas, COUNT(fm.idforo_mensaje) AS numMsgForos' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' LEFT JOIN foro_temas AS ft ON ft.idforo_temas = al.idalumnos' .
		' LEFT JOIN foro_mensaje AS fm ON fm.idusuario = al.idalumnos' .
		' WHERE al.idalumnos = ' . $idAlumno . ' AND m.idcurso = ' . $idCurso . ' AND fm.estado = 1' .
		' GROUP BY fm.idforo_temas';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumTemas($idCurso)
	{
		/*$sql = 'SELECT ft.idforo_temas' .
		' FROM foro_temas AS ft LEFT JOIN curso AS c ON c.idcurso = 5' .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN foro_mensaje AS fm ON fm.idforo_temas = ft.idforo_temas' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1) AND c.idcurso = ' . $idCurso . ' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';
		*/
		
		$sql = 'SELECT ft.idforo_temas, ft.titulo FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1)' .// AND c.f_inicio <= "' . $fecha . '" AND c.f_fin <= "' . $fecha . '"' .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';		
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTemas($idalumno, $idCurso)
	{
		/*$sql = 'SELECT ft.idforo_temas, COUNT(fm.idforo_mensaje) As numMsg' .
		' FROM foro_temas AS ft LEFT JOIN curso AS c ON c.idcurso = 5' .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN foro_mensaje AS fm ON fm.idforo_temas = ft.idforo_temas' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1) AND c.idcurso = ' . $idCurso . ' AND fm.idusuario = ' . $idalumno . ' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';
		*/
		
		$sql = 'SELECT ft.idforo_temas, COUNT(fm.idforo_mensaje) As numMsg FROM foro_temas AS ft' .
		' LEFT JOIN curso AS c ON c.idcurso = ' . $idCurso .
		' LEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo OR ft.es_general = 1' .
		' LEFT JOIN foro_mensaje AS fm ON fm.idforo_temas = ft.idforo_temas' .
		' LEFT JOIN rrhh ON rrhh.idrrhh = ft.idrrhh' .
		//' WHERE ft.idtemario = tm.idtemario' .
		' WHERE (ft.idmodulo = tm.idmodulo OR ft.es_general = 1) AND c.idcurso = ' . $idCurso . ' AND fm.idusuario = ' . $idalumno .
		// AND c.f_inicio <= "' . $fecha . '" AND c.f_fin <= "' . $fecha . '"' .
		' GROUP BY ft.idforo_temas ORDER BY ft.fecha DESC';
		//LEFT JOIN temario ON temario.idCurso = ' . $idCurso . ' 
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	
	public function obtenerConfigPorcentajes($idCurso)
	{
		$sql = 'SELECT c.tipo_autoeval, c.autoevaluaciones, c.foro, c.scorm, c.trabajo_equipo, c.tutoria, c.extra1, c.extra2, c.nota_coordinador, c.num_msg_foro, c.num_trabajos, c.trabajos_practicos' .
		' FROM curso AS c' .
		' WHERE c.idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function obtenerModulosPorCurso($idCurso)
	{
		$sql = 'SELECT md.idmodulo, md.nombre' .
		' FROM curso AS c' .
		' lEFT JOIN temario AS tm ON tm.idaccion_formativa = c.idaccion_formativa' .
		' LEFT JOIN modulo AS md ON md.idmodulo = tm.idmodulo ' .
		' WHERE c.idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumTests($idcurso)
	{
		$sql = 'SELECT COUNT(c.idmatricula) AS numTests' .
		' FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idcurso = ' . $idcurso .
		' WHERE c.idmatricula = m.idmatricula';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumTestCurso($idCurso)
	{
		$sql = "SELECT COUNT(a.idautoeval) as numTestCurso" .
		" FROM autoeval as a" .
		" LEFT JOIN test as t ON t.idautoeval = a.idautoeval" .
		" LEFT JOIN temario as tm ON tm.idcurso = t.idcurso" .
		" LEFT JOIN curso as c ON c.idaccion_formativa = tm.idaccion_formativa" .
		" WHERE c.idcurso = " . $idCurso . 
		" AND a.borrado = 0";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumTestAlumno($idMatricula)
	{
		$sql = "SELECT idcalificaciones, MAX(nota) as nota" . 
		" FROM calificaciones" .
		" WHERE idmatricula = " . $idMatricula .
		" GROUP BY idautoeval";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNotasTests($idalumno, $idcurso)
	{
		$sql = 'SELECT c.idmatricula, c.idautoeval, c.nota, c.fecha, c.idcalificaciones, au.titulo_autoeval' .
		' FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' LEFT JOIN autoeval AS au ON au.idautoeval = c.idautoeval' .
		' WHERE c.idmatricula = m.idmatricula';
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerNotasTestsAutoevalAlumno($idalumno, $idcurso, $idAutoevaluacion)
	{
		$sql = 'SELECT c.idmatricula, c.idautoeval, c.nota, c.fecha, c.idcalificaciones FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' WHERE c.idmatricula = m.idmatricula' .
		' AND c.idautoeval = ' . $idAutoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerNotasTestsParaMedia($idalumno, $idcurso)
	{
		$sql = 'SELECT c.idmatricula, c.idautoeval, c.nota, c.fecha, c.idcalificaciones' .
		' FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' WHERE c.idmatricula = m.idmatricula' .
		' ORDER BY c.idautoeval';
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerNotasTestsMasAlta($idalumno, $idcurso)
	{
		$sql = 'SELECT MAX(c.nota) AS nota FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' WHERE c.idmatricula = m.idmatricula';
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerNotasTestsUltima($idalumno, $idcurso)
	{
		$sql = 'SELECT c.nota AS nota FROM calificaciones AS c' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idalumno . ' AND m.idcurso = ' . $idcurso .
		' WHERE c.idmatricula = m.idmatricula ORDER BY c.fecha DESC LIMIT 1';
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	public function obtenerCalfTrabajosPract($idalumnos, $idCurso)
	{
		$sql = 'SELECT ctp.nota_trabajo, at.idmodulo FROM calificacion_trabajo_practico AS ctp' .
		' LEFT JOIN archivo_temario As at ON at.idarchivo_temario = ctp.idarchivo_temario' .
		' WHERE at.borrado = 0 AND at.activo = 1 AND ctp.idalumnos = ' . $idalumnos . ' AND ctp.idcurso = ' . $idCurso .
		' ORDER BY at.idmodulo DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT *' .
		' FROM alumnos AS al' .
		' LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos' .
		' WHERE al.idalumnos = ' . $idAlumno .
		' AND mt.idcurso = ' . $idCurso .
		' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTrabjosPracticosCurso($idcurso)
	{
		$sql = "SELECT count(idarchivo_temario) AS nArchivosTemario" .
		" FROM archivo_temario" .
		" WHERE idcurso = " . $idcurso .
		" AND idcategoria_archivo = 2" . 
		" AND borrado = 0";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
			
	}
	
	public function obtenerTestCurso($idcurso)
	{
		$sql = "SELECT count(t.idtest) AS nTestCurso" .
		" FROM test AS t" .
		" LEFT JOIN temario AS tm ON tm.idcurso = t.idcurso" .
		" LEFT JOIN curso AS c ON c.idaccion_formativa = tm.idaccion_formativa" .
		" WHERE c.idcurso = " . $idcurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
			
	}
}