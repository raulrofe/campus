<?php
$idCurso = Usuario::getIdCurso();
$objModelo = new ModeloNotas();

$objNotas = new Notas();
$objNotas->setCurso($idCurso);

$mi_curso = new Cursos();

mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$configNotas = $objModelo->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();
$tareasObligatoriasCurso = 0;

//Obtener el numero de test del curso
$numTest = $objModelo->obtenerNumTestCurso($idCurso);
if($numTest->num_rows > 0)
{
	$numTest = $numTest->fetch_object();
	$numTest = $numTest->numTestCurso;
}

//Obtener el numero de trabajos practicos de un curso
$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($idCurso);
if($numTrabajos->num_rows > 0)
{
	$numTrabajos = $numTrabajos->fetch_object();
	$numTrabajos = $numTrabajos->numTrabajosPracticos;
}


$numTemas = $objModelo->obtenerNumTemas($idCurso);
$numTemas = $numTemas->num_rows;
$notaTema = 10 / $numTemas;
$notaMensaje = $notaTema / $configNotas->num_msg_foro;

if($configNotas->extra1 > 0){
 	$tareasObligatoriasCurso = 1 + $numTemas;
}

$tareasObligatoriasCurso = $tareasObligatoriasCurso + $numTrabajos + $numTest;

$valorTareaObligatoria = 100 / $tareasObligatoriasCurso;
$porcentajeTestTp = 0;

$porcentajeTotal = array();


// actualizar notas de los foros manualmente
if(Peticion::isPost())
{
	$isCoordinador = Usuario::compareProfile('coordinador');

	$post = Peticion::obtenerPost();

	if(isset($post['actualizarNotas']) && $post['actualizarNotas'] == 1)
	{
		foreach($post as $item => $value)
		{
			$p_foro = explode('_', $item);

			// NOTA FORO
			if(count($p_foro) == 2 && $p_foro[0] == 'notaforo' && is_numeric($p_foro[1]))
			{
				$foroPuntos = 0;

				if(is_numeric($value) && ($value >= 0 && $value <= ($configNotas->foro / 10)))
				{
					$puntacionForoPorMensaje = 0;
					$numeroMensajeTotalAlumno = 0;

					//El numero de temas en los que ha participado el alumno
					$temas = $objModelo->obtenerTemas($p_foro[1], $idCurso);

					if($temas->num_rows > 0)
					{
						while($tema = $temas->fetch_object())
						{
							//Obtengo la nota por mensajes escritos
							if($tema->numMsg >= $configNotas->num_msg_foro)
							{
								$puntacionForoPorMensaje += $configNotas->num_msg_foro * $notaMensaje;
							}
							else
							{
								$puntacionForoPorMensaje += $tema->numMsg * $notaMensaje;
							}
						}
					}

					// calcula los puntos para la nota
					if($numTemas > 0)
					{
						$foroPuntos = round(($puntacionForoPorMensaje), 2);
					}

					if($foroPuntos == $value)
					{
						$value = null;
					}
				}
				else
				{
					$value = null;
				}

				// si la nueva nota es distinta  la no automatica se actualiza la nota manual
				if(isset($value))
				{
					$objModelo->actualizarNotaForo($p_foro[1], Usuario::getIdCurso(), $value);
				}
				else
				{
					$objModelo->actualizarNotaForo($p_foro[1], Usuario::getIdCurso(), 'NULL');
				}

			}
			// NOTA COORDINADOR
			else if(count($p_foro) == 2 && $p_foro[0] == 'notacoordinador' && is_numeric($p_foro[1]) && $isCoordinador)
			{
				if(is_numeric($value) && ($value >= 0 && $value <= 10))
				{

				}
				else if(empty($value))
				{
					$value = 'NULL';
				}
				else
				{
					$value = null;
				}

				if(isset($value))
				{
					$objModelo->actualizarNotaCoordinador($p_foro[1], $idCurso, $value);
				}
			}
		}
	}
}

$alumnos = $objModelo->obtenerAlumnosActivos($idCurso);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');