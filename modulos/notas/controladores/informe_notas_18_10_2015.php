<?php 

//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');

//Cargo los modelos basicos
$html2Pdf = new HTML2PDF();
$objAlumnos = new Alumnos();
$objUsuario = new Usuarios();
$objCurso = new Cursos();
$objModeloNotas = new ModeloNotas();
$objNotas = new Notas();

//Cargo el modelo foro
mvc::cargarModuloSoporte('foro');
$objForo = new modeloForo();

//Cargo el modelo calificaciones
mvc::cargarModuloSoporte('calificaciones');
$objTrabajos = new TrabajosPracticos();

$get = Peticion::obtenerGet();

$idCurso = Usuario::getIdCurso();
$objNotas->setCurso($idCurso);

//Obtengo la configuracion de las notas del curso
$configNotas = $objModeloNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

// Datos PORTADA PDF INFORME curso, accion formativa y grupo, tutores, tutorias y empresa organizadora
$datosCurso = $objCurso->obtenerIGAFCurso($idCurso);
if($datosCurso->num_rows > 0)
{
	$curso = $datosCurso->fetch_object();
	
	//Numero de temas de foro
	$temasForo = $objForo->obtenerTemas($idCurso);
	$numTemasForo = $temasForo->num_rows;
	
	//Obtener el numero de test del curso
	$numTest = $objModeloNotas->obtenerNumTestCurso($idCurso);
	if($numTest->num_rows > 0)
	{
		$numTest = $numTest->fetch_object();
		$numTest = $numTest->numTestCurso;
	}

	//Obtener el numero de trabajos practicos de un curso
	$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($idCurso);
	if($numTrabajos->num_rows > 0)
	{
		$numTrabajos = $numTrabajos->fetch_object();
		$numTrabajos = $numTrabajos->numTrabajosPracticos;
	}
	
	//Obtengo las tutorias
	$tutorias = $objCurso->obtenerTutoriaCursoTripartita($idCurso);
	if($tutorias->num_rows > 0)
	{
		$tutoria = $tutorias->fetch_object();
		$portadaPdf['tutoriaCurso'] = $tutoria->dias_tutoria . ' de ' . $tutoria->hora_inicio . ' a ' . $tutoria->hora_fin;

		//Entidad Organizadora
		$entidadOrganizadora = $objCurso->getEntidadOrganizadora();
		if($entidadOrganizadora->num_rows == 1)
		{
			$entidadOrganizadora = $entidadOrganizadora->fetch_object();	
			$portadaPdf['nombreEntidad'] = $entidadOrganizadora->nombre_entidad;
			$portadaPdf['cifEntidad'] = $entidadOrganizadora->cif;
		}

		$portadaPdf['codigoEntidad'] = '115-00';
	}
	
	//Obtengo los datos de los staff del curso
	$staff = $objCurso->obtenerTutoresCurso($idCurso);
	
	//Los datos del staf
	if($staff->num_rows > 0)
	{
		$contut = 0;
		while($rh = $staff->fetch_object())
		{
			if($rh->status == 'coordinador')
			{
				$portadaPdf['dniCoordinador'] = $rh->nif;		
				$portadaPdf['nombreCoordinador'] = $rh->nombrec;	
			}
			else if($rh->status == 'tutor1' OR $rh->status == 'tutor2' OR $rh->status == 'tutor3')
			{
				$portadaPdf['tutores'][$contut]['nombreTutor'] = $rh->nombrec;
				$portadaPdf['tutores'][$contut]['dniTutor'] = $rh->nif;	
				$contut++;	
			}
		}
	}
}

//Array para generar los informes en pdf
$dataPdf = array();

$informeAlumnos = array();

//Genero un array con los id de todos los alulmnos del curso
$idAlumnos = $objAlumnos->alumnos_del_curso_activos($idCurso);
if($idAlumnos->num_rows > 0)
{
	while($alumnos = $idAlumnos->fetch_object())
	{
		$informeAlumnos[] = $alumnos->idalumnos;
	}
}	


//Genero los array para los pdf
if(count($informeAlumnos) > 0)
{
	$datosAlumno = array();
	set_time_limit(0);
	//echo count($informeAlumnos)-1;exit;

	for($i=0; $i<=count($informeAlumnos)-1; $i++)
	{
		//Obtner el id de matricula del alumno
		$idMatriculacion = Usuario::getIdMatriculaDatos($informeAlumnos[$i], $idCurso);
		$datosAlumno = $objUsuario->obtenerMatriculaInforme($idMatriculacion);
		if($datosAlumno->num_rows > 0)
		{		

			//Nota media de los foros
			$foroPuntosReduce = $objNotas->calcularNotaMediaForo($informeAlumnos[$i], $idCurso, true);
			$foroPuntos = $objNotas->calcularNotaMediaForo($informeAlumnos[$i], $idCurso, false);
			//Porcentaje foro
			$temas = $objModeloNotas->obtenerTemas($informeAlumnos[$i], $idCurso);
			$numTemasParticipado = $temas->num_rows;
			
			$porcentajeForo = ($numTemasParticipado * 100) / $numTemasForo; 			

			//Nota media de los trabajos practicos
			$notaTPReduce = $objNotas->calcularMediaTrabajosPract($informeAlumnos[$i], $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
			$notaTP = $objNotas->calcularMediaTrabajosPract($informeAlumnos[$i], $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);
			
			//Porcentaje trabajos practicos
			$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($informeAlumnos[$i], $idCurso);
				//$trabajosParticipado = $trabajosParticipado->num_rows;
			$numTpParticipado = 0;
			while($tpParticipado = $trabajosParticipado->fetch_object())
			{
				if($tpParticipado->nota_trabajo >= 5)
				{
					$numTpParticipado ++;	
				}
			}

			$porcentajeTrabajos = ($numTpParticipado * 100) / $numTrabajos;
			
			//Nota media autoevaluaciones
			$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($informeAlumnos[$i], $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
			$mediaTests = $objNotas->calcularNotaMediaAutoeval($informeAlumnos[$i], $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);
			
			//Porcentaje autoevaluaciones
			$idMatricula = Usuario::getIdMatriculaDatos($informeAlumnos[$i], $idCurso);
			$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);
			//$numTestParticipado = $numTestAlumno->num_rows;
			$numTestRealizado = 0;
			if($numTestAlumno->num_rows > 0)
			{
				while($testParticipado = $numTestAlumno->fetch_object())
				{
					if($testParticipado->nota >= 5)
					{
						$numTestRealizado ++; 
					}
				}
			}

			$porcentajeTest = ($numTestRealizado * 100) / $numTest;
			
			//Porcentaje general del curso
			$tareasRealizadas = $numTestRealizado + $numTpParticipado;
			$tareasTotales = $numTest + $numTrabajos;
			$porcentajeTotal = $tareasRealizadas * 100 / $tareasTotales;
			
			//Obtener la nota media general
			$notaMediaFinal = round($foroPuntosReduce + $notaTPReduce + $mediaTestsReduce, 2);
			
			// Obtener el tiempo total que el alumno lleva invertido en el curso
			$matriculaAlumno = $datosAlumno->fetch_object();
			
			//Buscamos el primer acceso que haya en la base de datos
			$primerAcceso = $objUsuario->getFirstAccessParticipante($idMatriculacion); 
			if($primerAcceso->num_rows > 0)
			{
				$primerAcceso = $primerAcceso->fetch_object();
				$fechaPrimeraConexion = $primerAcceso->fecha_entrada;
			}
			else
			{
				$primerAcceso = 'No accedido';
			}


			//Datos alumno
			$dataPdf[$i]['nombreAlumno'] = $matriculaAlumno->nombre;
			$dataPdf[$i]['apellidosAlumno'] = $matriculaAlumno->apellidos;			
			$dataPdf[$i]['nif'] = $matriculaAlumno->dni;
			$dataPdf[$i]['email'] = $matriculaAlumno->email;
			$dataPdf[$i]['telefonoAlumno'] = $matriculaAlumno->telefono;
			$dataPdf[$i]['fechaMatriculacion'] = $matriculaAlumno->fecha;
			$dataPdf[$i]['fechaPrimeraConexion'] = $fechaPrimeraConexion;
			$dataPdf[$i]['fechaUltimaConexion'] = $matriculaAlumno->fecha_conexion_actual;
			$dataPdf[$i]['notaMediaFinal'] = $notaMediaFinal;
			$dataPdf[$i]['ponderaTP'] = $notaTPReduce;
			$dataPdf[$i]['ponderaTest'] = $mediaTestsReduce;
			$dataPdf[$i]['ponderaForo'] = $foroPuntosReduce;
			$dataPdf[$i]['centroAlumno'] = $matriculaAlumno->razon_social;
			$dataPdf[$i]['cifCentroAlumno'] = $matriculaAlumno->cif;
			
			if(is_float($porcentajeTotal))
			{
				$dataPdf[$i]['completado'] = number_format($porcentajeTotal, '2', '.', '');
			}
			else
			{
				$dataPdf[$i]['completado'] = $porcentajeTotal;
			}
						
			$fechaLogsAcceso = $objUsuario->obtenerTiempoConexion($matriculaAlumno->idmatricula);

			$totalSegundos = 0;
			if($fechaLogsAcceso->num_rows > 0)
			{
				while($fechaLog = $fechaLogsAcceso->fetch_object())
				{	
					if($fechaLog->fecha_entrada && $fechaLog->fecha_salida)		
					{	
						$diferencia = strtotime($fechaLog->fecha_salida) - strtotime($fechaLog->fecha_entrada);
						//echo $fechaLog->fecha_entrada . ' - ' . $fechaLog->fecha_salida . '/-----/' . $diferencia . '<br/>';
						
						$totalSegundos = $totalSegundos + $diferencia;
					}
					
					else
					{
						//$totalSegundos = $totalSegundos + 5;	
					}
				}
			}
			
			$tiempoConexion = Fecha::segundosHoras($totalSegundos);

			$dataPdf[$i]['tiempoTotal'] = $tiempoConexion;

			//************************************************************************************************************************
			// OBTENGO LOS TRABAJOS PRACTICOS DEL ALUMNO QUE HA REALIZADO
			//************************************************************************************************************************
			$trabajosPracticos = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($informeAlumnos[$i], $idCurso);
			if($trabajosPracticos->num_rows > 0)
			{
				$cont = 0;
				while($trabajoPractico = $trabajosPracticos->fetch_object())
				{
					$dataPdf[$i]['trabajos'][$cont]['tituloTrabajo'] = $trabajoPractico->titulo;
					$dataPdf[$i]['trabajos'][$cont]['notaTrabajo'] = $trabajoPractico->nota_trabajo;
					$dataPdf[$i]['trabajos'][$cont]['fechaTrabajo'] = $trabajoPractico->fecha;
					$dataPdf[$i]['trabajos'][$cont]['notaMediaTrabajo'] = $notaTP;
					$cont++;
				}				
			}	
			
			//************************************************************************************************************************
			// OBTENGO LOS TEST DEL ALUMNO QUE HA REALIZADO
			//************************************************************************************************************************			
			$NotasTestAlulmnos = $objModeloNotas->obtenerNotasTests($informeAlumnos[$i], $idCurso);
			if($NotasTestAlulmnos->num_rows > 0)
			{
				$cont = 0;
				while($notaTestAlumno = $NotasTestAlulmnos->fetch_object())
				{
					$dataPdf[$i]['test'][$cont]['tituloTest'] = $notaTestAlumno->titulo_autoeval;
					$dataPdf[$i]['test'][$cont]['notaTest'] = $notaTestAlumno->nota;
					$dataPdf[$i]['test'][$cont]['fechaTest'] = $notaTestAlumno->fecha;
					$dataPdf[$i]['test'][$cont]['notaMediaTest'] = $mediaTests;
					$cont++;
				}
			}
			
			//************************************************************************************************************************
			// OBTENGO LOS TEMAS DEL FORO DEL ALUMNO QUE HA REALIZADO
			//************************************************************************************************************************
			$temasForo = $objForo->obtenerTemas($idCurso);
			if($temasForo->num_rows > 0)
			{
				$cont = 0;
				while($tema = $temasForo->fetch_object())
				{
					$dataPdf[$i]['foro'][$cont]['nombreTema'] = $tema->titulo;
					$dataPdf[$i]['foro'][$cont]['idTema']= $tema->idforo_temas;
					
					$mensajesForo = $objForo->obtenerMensajesTemasParticipadoPorAlumno($informeAlumnos[$i], $idCurso, $tema->idforo_temas); 
					if($mensajesForo->num_rows > 0)
					{		
						$mensajeForo = $mensajesForo->fetch_object();
						$mensajesAlumnoTema = $mensajeForo->nMensajesTemasParticipados;
					}
					else
					{
						$mensajesAlumnoTema = 0;	
					}
					
					$dataPdf[$i]['foro'][$cont]['nMensajes'] = $mensajesAlumnoTema;
					$dataPdf[$i]['foro'][$cont]['notaMediaForo'] = $foroPuntos;
					$cont++;	
				}
			}
		}
	}
}

// Una vez terminado el proceso de creacion del array para generar el pdf nos ponemos a generarlo
// Recuperacion del contenido HTML
ob_start();
include(PATH_ROOT . 'modulos/notas/vistas/informe/informe_notas.php');
$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
	//echo $html2pdf->setModeDebug();exit;
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	
	$html2pdf->Output();
}
catch(HTML2PDF_exception $e) {echo $e;}
	


