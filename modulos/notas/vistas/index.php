<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.titulo">EVALUACIONES</span>
</div>
<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs_evaluacion.php'); ?>

	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso">

		<div class="tituloTabs">
			<div class="fleft" style="margin-top:15px;" data-translate-html="evaluaciones.notas">
				Notas
			</div>
			<div class="fright">
				<div class="menuVista printEvaluation">
					<a href="calificaciones/notas/informe" target="_blank" rel="tooltip" title="informe completo del curso">
						<img src="imagenes/menu_vistas/imprimir.png" alt="vista" />
					</a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<br/>

		<div id="notas">
				<?php if(isset($alumnos) && $alumnos->num_rows > 0):?>
					<form id="frm_notas" method="post" action="">
						<table class="tabla">
								<tr class='filaTabla'>
									
									<td data-translate-html="integrantes.alumnos">
										Alumnos
									</td>

									<td class="t_center" >
										<span data-translate-html="foro.foro">
											Foro
										</span> 
										(<?php echo $configNotas->foro?>%)
									</td>

									<td class="t_center">
										<span data-translate-html="misprogresos.trabajos.titulo">
											Trabajos pr&aacute;cticos
										</span> 
										<?php echo $configNotas->trabajos_practicos?>%)
									</td>

									<td class="t_center">
										<span data-translate-html="autoevaluaciones.autoevaluacion">
											Autoevaluaci&oacute;n
										</span> 
										(<?php echo $configNotas->autoevaluaciones?>%)
									</td>

									<?php if($configNotas->extra1 > 0): ?>
										<td class="t_center">
											<span data-translate-html="tutoria.menu.speaking">
												Speaking
											</span> 
											(<?php echo $configNotas->extra1?>%)
										</td>
									<?php endif; ?>

									<td class="t_center" data-translate-html="misprogresos.grafica.media">
										Nota media
									</td>

									<td class="t_center" data-translate-html="seguimiento.superado">
										Superado
									</td>
								</tr>
								<?php $cont = 0; ?>
								<?php while($alumno = $alumnos->fetch_object()):?>
								<?php
									$porcentajeTestTp = 0;
									$idMatricula = Usuario::getIdMatriculaDatos($alumno->idalumnos, Usuario::getIdCurso());
									$foroPuntosReduce = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, true);
									$notaTPReduce = $objNotas->calcularMediaTrabajosPract($alumno->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
									$mediaTestsReduce = $objNotas->calcularNotaMediaAutoeval($alumno->idalumnos, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);

									if($configNotas->extra1 > 0){
										$notaSpeaking = mysqli_fetch_object($mi_curso->getNotaSpeaking($idMatricula));
										if(!empty($notaSpeaking->nota)){
											$notaSpeaking = $notaSpeaking->nota / $configNotas->extra1;
											$porcentajeTestTp += $valorTareaObligatoria;
										} else {
											$notaSpeaking = 0.00;
										}

										$notaMediaFinal = round($foroPuntosReduce + $notaTPReduce + $mediaTestsReduce + $notaSpeaking, 2);


										/****************************
										 * FORO
										 ****************************/

										$foroPuntos = $objNotas->calcularNotaMediaForo($alumno->idalumnos, Usuario::getIdCurso(), false);
										//$percentForo = ($foroPuntos * 10);

										$temas = $objModelo->obtenerTemas($alumno->idalumnos, Usuario::getIdCurso());
										$numTemasParticipado = $temas->num_rows;

										//$porcentajeForo = ceil(($numTemasParticipado->num_rows*100)/$numTemas);

										// $porcentajeForo = $foroPuntos * 10;
										$porcentajeTestTp += $numTemasParticipado * $valorTareaObligatoria;

									} else {
										$notaMediaFinal = round($foroPuntosReduce + $notaTPReduce + $mediaTestsReduce, 2);
									}

									/***************************
									 * TRABAJOS PRACTICOS
									 **************************/
									$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($alumno->idalumnos, Usuario::getIdCurso());

									//Que cantidad de trabajo es el 75%
									if(!empty($numTrabajos)){
										$porcentajeTrabajoRealizado = ceil(($trabajosParticipado->num_rows*100)/$numTrabajos);
									}else{
										$porcentajeTrabajoRealizado = 0;
									}

									if($trabajosParticipado->num_rows > 0)
									{
										while($tpParticipado = $trabajosParticipado->fetch_object())
										{
											if($tpParticipado->nota_trabajo >= 5)
											{
												$porcentajeTestTp += $valorTareaObligatoria;
											}
										}
									}

									/***************************
									 * TEST
									 **************************/
									$numTestAlumno = $objModelo->obtenerNumTestAlumno($idMatricula);

									//Que cantidad de trabajo es el 75%
									if(!empty($numTrabajos)){
										$porcentajeAutoevalRealizado = ceil(($numTestAlumno->num_rows*100)/$numTest);
									}else{
										$porcentajeAutoevalRealizado = 0;
									}

									if($numTestAlumno->num_rows > 0)
									{
										while($testParticipado = $numTestAlumno->fetch_object())
										{
											if($testParticipado->nota >= 5)
											{
												$porcentajeTestTp += $valorTareaObligatoria;
											}
										}
									}

									$porcentajeTotal = $porcentajeTestTp;
								?>
									<tr <?php if($notaMediaFinal >= 5 && $porcentajeTotal >= 75) echo 'class="aprobado"'?>>
										<td>
											<?php echo Texto::textoPlano(ucwords($alumno->nombrec));?>
											<?php if($notaMediaFinal >= 5 && $porcentajeTotal >= 75) echo '<br/> cuestionario.aulasmart.es/public/valoracion/' . $idMatricula;?>
										</td>
										<td class="t_center">
											<?php $foroPuntos = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, true);?>

											<?php if(Usuario::compareProfile(array('coordinador', 'supervisor'))):?>
												<?php echo number_format($foroPuntos, '2', '.', '');?>
											<?php elseif(Usuario::compareProfile('tutor')):?>
												<input class="t_center" type="text" name="notaforo_<?php echo $alumno->idalumnos?>" value="<?php echo number_format($foroPuntosReduce, '2', '.', '');?>" size='1'/>
											<?php endif;?>
										</td>
										<td class="t_center">
											<?php $notaTP = $objNotas->calcularMediaTrabajosPract($alumno->idalumnos, $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);?>
											<?php echo number_format($notaTPReduce, '2', '.', '');?>
										</td>

										<td class="t_center">
											<?php $mediaTests = $objNotas->calcularNotaMediaAutoeval($alumno->idalumnos, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false); ?>
											<?php echo number_format($mediaTestsReduce, '2', '.', ''); ?>
										</td>

										<?php if($configNotas->extra1 > 0): ?>
											<?php $notaSpeaking = mysqli_fetch_object($mi_curso->getNotaSpeaking($idMatricula)); ?>
											<td class="t_center">
												<?php if(!empty($notaSpeaking->nota)): ?>
													<?php echo $notaSpeaking = $notaSpeaking->nota / $configNotas->extra1; ?>
												<?php else: ?>
													0.00
												<?php endif; ?>
											</td>
										<?php endif; ?>


										<td class="t_center">
											<?php if(isset($alumno->nota_coordinador)): ?>
												<?php $notaMediaFinal = round($alumno->nota_coordinador, 2); ?>
											<?php else: ?>
												<?php if(isset($notaSpeaking)): ?>
													<?php $notaMediaFinal = round($foroPuntosReduce + $notaTPReduce + $mediaTestsReduce + $notaSpeaking, 2); ?>
												<?php else: ?>
													<?php $notaMediaFinal = round($foroPuntosReduce + $notaTPReduce + $mediaTestsReduce, 2); ?>
												<?php endif; ?>

											<?php endif; ?>
											<?php if(Usuario::compareProfile('coordinador')):?>
												<input type="text" name="notacoordinador_<?php echo $alumno->idalumnos?>" value="<?php echo number_format($notaMediaFinal, '2', '.', '') ?>" />
											<?php else:?>
												<?php echo number_format($notaMediaFinal, 2, '.', '');?>
											<?php endif;?>
										</td>
										<td class="t_center">
											<?php if(is_float($porcentajeTotal)):?>
												<?php echo number_format($porcentajeTotal, '2', '.', '');?>
											<?php else: ?>
												<?php echo $porcentajeTotal; ?>
											<?php endif; ?>
											<?php $cont++; ?>
										%</td>
									</tr>
								<?php endwhile;?>
						</table>

						<div class="t_right">
							<br />
							<input type="hidden" name="actualizarNotas" value="1" />
							<input type="submit" value="Guardar Notas" data-translate-value="evaluaciones.guardarnotas" />
						</div>
					</form>

					<div style="font-size:0.8em">
						<p>
							<span data-translate-html="evaluaciones.text1">
								* El foro puede puntuar como m&aacute;ximo 
							</span>

							<?php echo round(($configNotas->foro / 10), 2)?> 

							<span data-translate-html="evaluaciones.puntos">
								punto/s
							</span>
						</p>

						<p>
							<span data-translate-html="evaluaciones.text2">
								* Los trabajos pr&aacute;cticos pr&aacute;cticos pueden puntuar como m&aacute;ximo 
							</span>

							<?php echo round(($configNotas->trabajos_practicos / 10), 2)?> 

							<span data-translate-html="evaluaciones.puntos">
								punto/s
							</span>
						</p>

						<p>
							<span data-translate-html="evaluaciones.text3">
								* Las autoevaluaciones pueden puntuar como m&aacute;ximo
							</span> 

							<?php echo round(($configNotas->autoevaluaciones / 10), 2)?> 

							<span data-translate-html="evaluaciones.puntos">
								punto/s
							</span>
						</p>
					</div>
				<?php endif;?>
		</div>
	</div>
</div>