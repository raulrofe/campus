<style>

p, table, td{
	margin:0;
	padding:0;	
}
b{font-weight: bold;}

/*CLASES GENERALES */
.v_middle{vertical-align: middle;}
.t_center{text-align:center;}
.t_left{text-align: left;}
.t_right{text-align: right;}
.small{font-size: 12px;}
.fleft{flaot:left;}
.borderBottom{border-bottom:2px solid #AAA;}
.borderRight{border-right:2px solid #AAA;}
.encabezado{
	border:2px solid #AAA;
	width: 100%;
	height: 25px;
	background-color:#EFEFEF;
}

.encabezado .nBox{
	border-right:2px solid #AAA;
	float:left;
	width:25px;
	text-align:center;
	font-weight: bold;
}

.encabezado .titleBox{
	float:left;
	height: 25px;
	width: 752px;
	text-align: center;
	font-weight: bold;
}

.encabezado td{vertical-align: middle;}

.contenido{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	border-bottom:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenido tr td{padding:0;margin:0;}
.contenido td{padding:5px;}


.titleInform{
	font-size: 18px;
	font-weight: bold;
	text-align:center;
	margin-bottom:10px;
}

</style>
<?php $portada= $portadaPdf; ?>
	<page>
		<br/>
		<div class="t_center"><img src="imagenes/informe/ase.jpg" /></div>
		<br/>
		<div class="titleInform">
			<p>INFORME COMPLETO DE SEGUIMIENTO DE APRENDIZAJE</p>
			<p>DE LOS ALUMNOS/AS</p>
		</div>
		<br/>
		
		<div id="entidadOrganizadora">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS DE LA ENTIDAD ORGANIZADORA</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">ENTIDAD ORGANIZADORA</td>
					<td class="borderRight" style="width:150px;">CIF</td>
					<td style="width:150px;">C&Oacute;DIGO ID DE PERFIL</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php echo $portada['nombreEntidad']?></td>
					<td class="borderRight" style="width:150px;"><?php echo $portada['cifEntidad']?></td>
					<td style="width:150px;"><?php echo $portada['codigoEntidad']?></td>
				</tr>			
			</table>
		</div>
		
		<br/>
		
		<div id="accionFormativa">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DATOS DE LA ACCI&Oacute;N FORMATIVA</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			
			<?php 
				$tituloDividido = explode('(', $curso->titulo);
				if(count($tituloDividido) > 0)
				{
					$codigos = explode('/', $tituloDividido[1]);
				}
			?>

			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:384px;">DENOMINACI&Oacute;N DE LA ACCI&Oacute;N</td>
					<td class="borderRight" style="width:150px;">C&Oacute;DIGO AF</td>
					<td style="width:150px;">GRUPO</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:384px;"><?php echo htmlentities($curso->titulo, ENT_QUOTES, 'utf-8'); ?></td>
					<td class="borderRight" style="width:150px;"><?php echo $codigos[0] ?></td>
					<td style="width:150px;"><?php echo str_replace(')', '', $codigos[1]) ?></td>
				</tr>
			</table>
			
			<table class="contenido" cellpadding="0" cellspacing="0">
				<tr>
					<td class="borderRight" style="width:150px;">FECHA INICIO</td>
					<td class="borderRight" style="width:150px;">DNI RESPONSABLE</td>
					<td style="width:384px;">RESPONSABLE FORMACI&Oacute;N</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:150px;"><?php echo Fecha::invertir_fecha($curso->f_inicio, '-', '/'); ?></td>
					<td class="borderRight" style="width:150px;"><?php echo $portada['dniCoordinador']; ?></td>
					<td style="width:384px;"><?php echo htmlentities($portada['nombreCoordinador'], ENT_QUOTES, 'utf-8')?></td>
				</tr>				
			</table>
			
			<table class="contenido" cellpadding="0" cellspacing="0">
				<tr>
					<td class="borderRight" style="width:150px;">FECHA FIN</td>
					<td class="borderRight" style="width:150px;">DNI FORMADOR</td>
					<td style="width:384px;">FORMADOR/TUTOR</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:150px;"><?php echo Fecha::invertir_fecha($curso->f_fin, '-', '/'); ?></td>
					<td class="borderRight" style="width:150px;">						
						<?php for($i=0; $i<=count($portada['tutores'])-1;$i++):?>
							<div><?php echo $portada['tutores'][$i]['dniTutor']?></div>
						<?php endfor; ?>
					</td>
					<td style="width:384px;">
						<?php for($i=0; $i<=count($portada['tutores'])-1;$i++):?>
							<div><?php echo htmlentities($portada['tutores'][$i]['nombreTutor'], ENT_QUOTES, 'utf-8'); ?></div>
						<?php endfor; ?>
					</td>
				</tr>				
			</table>

			<table class="contenido" style="border-bottom:0 #FFF;" cellpadding="0" cellspacing="0">
				<tr>
					<td class="borderRight" style="width:150px;">N&ordm; HORAS </td>
					<td style="width:558px;">FIRMADO</td>
				</tr>
				<tr>
					<td class="borderRight" style="width:150px;border-bottom:2px solid #AAA;"><?php echo $curso->n_horas; ?></td>
					<td style="width:558px;"></td>
				</tr>				
			</table>
			
			<table class="contenido" cellpadding="0" cellspacing="0">
				<tr>
					<td class="borderRight" style="width:150px;">HORARIO TUTOR&Iacute;AS</td>
					<td style="width:538px;" colspan="2"></td>
				</tr>
				<tr>
					<td class="borderRight" style="width:150px;"><?php echo $portada['tutoriaCurso']; ?></td>
					<td style="width:269px;">FOMADOR/TUTOR</td>
					<td style="width:269px;">RESPONSABLE</td>
				</tr>				
			</table>			
		</div>
		
		<br/>
		
		<div id="detalleTarea">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">DETALLE TAREAS</td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="vertical-align:middle;width:210px;border-bottom:2px solid #AAA;">Nombre tarea</td>
					<td class="borderRight" style="width:150px;text-align:center;border-bottom:2px solid #AAA;">N&ordm; Temas</td>
					<td class="borderRight" style="width:150px;text-align:center;border-bottom:2px solid #AAA;">Nota final</td>
					<td style="width:150px;text-align:center;border-bottom:2px solid #AAA;">Tipo de tarea</td>
				</tr>
				<tr>
					<td class="borderRight" style="vertical-align:middle;width:210px;">Trabajos pr&aacute;cticos</td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $numTrabajos; ?></td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $configNotas->trabajos_practicos; ?>%</td>
					<td style="width:150px;text-align:center;">Obligatoria</td>
				</tr>
				<tr>
					<td class="borderRight" style="vertical-align:middle;width:210px;">Autoevaluaciones</td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $numTest; ?></td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $configNotas->autoevaluaciones; ?>%</td>
					<td style="width:150px;text-align:center;">Obligatoria</td>
				</tr>
				<tr>
					<td class="borderRight" style="vertical-align:middle;width:210px;">Foro</td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $numTemasForo; ?></td>
					<td class="borderRight" style="width:150px;text-align:center;"><?php echo $configNotas->foro; ?>%</td>
					<td style="width:150px;text-align:center;">Extra</td>
				</tr>
			</table>				
		</div>
		
		<br/>
		
		<div id="observacionesGenerales">
			<table cellpadding="0" cellspacing="0" class="encabezado">
				<tr>
					<td class="titleBox">OBSERVACIONES GENERALES</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td style="width:732px;height:300px;"></td>
				</tr>
			</table>		
		</div>
		
	</page>
	
	<?php foreach ($dataPdf as $pdf):?>
		<page>
			<div class="datosParticipante">
				<table cellpadding="0" cellspacing="0" class="encabezado">
					<tr>
						<td class="titleBox">DATOS PARTICIPANTE</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:558px;">APELLIDOS, NOMBRE</td>
						<td style="width:150px;">DNI</td>
					</tr>
					<tr>
						<td class="borderRight" style="width:558px"><?php echo htmlentities(ucwords($pdf['apellidosAlumno']), ENT_QUOTES, 'utf-8') . ', ' . htmlentities(ucwords($pdf['nombreAlumno']), ENT_QUOTES, 'utf-8'); ?></td>
						<td style="width:150px;"><?php echo $pdf['nif']; ?></td>
					</tr>				
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:558px;">NOMBRE DEL CENTRO</td>
						<td style="width:150px;">CIF</td>
					</tr>
					<tr>
						<td class="borderRight" style="width:558px"><?php echo htmlentities(ucwords($pdf['centroAlumno']), ENT_QUOTES, 'utf-8'); ?></td>
						<td style="width:150px;"><?php echo $pdf['cifCentroAlumno']; ?></td>
					</tr>				
				</table>	
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:150px;">TIEMPO TOTAL</td>
						<td class="borderRight" style="width:384px;">TEL&Eacute;FONO / MAIL</td>
						<td style="width:150px;">FECHA MATRICULACI&Oacute;N</td>
					</tr>
					<tr>
						<td class="borderRight" style="width:150px;"><?php echo $pdf['tiempoTotal']; ?></td>
						<td class="borderRight" style="width:384px;"><?php echo $pdf['telefonoAlumno'] . ' / ' . $pdf['email']?></td>
						<td style="width:150px;"><?php echo Fecha::obtenerFechaFormateada($pdf['fechaMatriculacion'], false); ?></td>
					</tr>				
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:165px;">FECHA 1&ordf; CONEXI&Oacute;nN </td>
						<td class="borderRight" style="width:165px;">FECHA &Uacute;TIMA CONEXI&Oacute;N </td>
						<td class="borderRight"style="width:165px;">NOTA MEDIA DEL CURSO</td>
						<td style="width:165px;">COMPLETADO</td>
					</tr>
					<tr>
						<td class="borderRight" style="width:165px;"><?php echo Fecha::obtenerFechaFormateada($pdf['fechaPrimeraConexion'], false); ?></td>
						<td class="borderRight" style="width:165px;"><?php echo Fecha::obtenerFechaFormateada($pdf['fechaUltimaConexion'], false); ?></td>
						<td class="borderRight"style="width:165px;"><?php echo $pdf['notaMediaFinal']; ?></td>
						<td style="width:165px;"><?php echo $pdf['completado']; ?>%</td>
					</tr>				
				</table>		
			</div>	
			
			<br/>
			<div class="testParticipantes">
				<table cellpadding="0" cellspacing="0" class="encabezado">
					<tr>
						<td class="titleBox">TRABAJOS PR&Aacute;CTICOS</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:384px;font-weight:bold;">T&Iacute;TULO TRABAJO PR&Aacute;CTICO</td>
						<td class="borderRight" style="width:150px;font-weight:bold;">NOTA</td>
						<td style="width:150px;font-weight:bold;">FECHA REALIZACI&Oacute;N</td>
					</tr>				
				</table>
				<?php if(isset($pdf['trabajos'])):?>
					<?php foreach($pdf['trabajos'] as $trabajos):?>
						<table class="contenido" cellpadding="0" cellspacing="0">
							<tr>
								<td class="borderRight" style="width:384px;"><?php echo htmlentities($trabajos['tituloTrabajo'], ENT_QUOTES, 'utf-8'); ?></td>
								<td class="borderRight" style="width:150px;"><?php echo $trabajos['notaTrabajo']; ?></td>
								<td style="width:150px;"><?php echo Fecha::obtenerFechaFormateada($trabajos['fechaTrabajo']); ?></td>
							</tr>
						</table>	
					<?php endforeach; ?>
					<table class="contenido" cellpadding="0" cellspacing="0">
						<tr>
							<td class="t_right" style="width:558px;border-right:2px solid #AAA;font-weight:bold;">NOTA MEDIA</td>
							<td class="t_center" style="width:150px;"><b><?php echo round($trabajos['notaMediaTrabajo'], 2); ?></b></td>
						</tr>						
					</table>	
				<?php else: ?>
						<table class="contenido" cellpadding="0" cellspacing="0">
							<tr>
								<td class="t_center" style="width:764px;">No ha realizado autoevaluaciones</td>
							</tr>
						</table>	
				<?php endif; ?>
			</div>	
			
			<br/>
			
			<div class="testParticipantes">
				<table cellpadding="0" cellspacing="0" class="encabezado">
					<tr>
						<td class="titleBox">AUTOEVALUACIONES</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:384px;font-weight:bold;">AUTOEVALUACION</td>
						<td class="borderRight" style="width:150px;font-weight:bold;">NOTA</td>
						<td style="width:150px;font-weight:bold;">FECHA REALIZACI&Oacute;N</td>
					</tr>				
				</table>
				<?php if(isset($pdf['test'])):?>
					<?php foreach($pdf['test'] as $test):?>
						<table class="contenido" cellpadding="0" cellspacing="0">
							<tr>
								<td class="borderRight" style="width:384px;"><?php echo htmlentities($test['tituloTest'], ENT_QUOTES, 'utf-8'); ?></td>
								<td class="borderRight" style="width:150px;"><?php echo $test['notaTest']; ?></td>
								<td style="width:150px;"><?php echo Fecha::obtenerFechaFormateada($test['fechaTest']); ?></td>
							</tr>
						</table>	
					<?php endforeach; ?>
					<table class="contenido" cellpadding="0" cellspacing="0">
						<tr>
							<td class="t_right" style="width:558px;border-right:2px solid #AAA;font-weight:bold;">NOTA MEDIA</td>
							<td class="t_center" style="width:150px;"><b><?php echo round($test['notaMediaTest'], 2); ?></b></td>
						</tr>						
					</table>	
				<?php else: ?>
						<table class="contenido" cellpadding="0" cellspacing="0">
							<tr>
								<td class="t_center" style="width:732px;">No ha realizado autoevaluaciones</td>
							</tr>
						</table>	
				<?php endif; ?>
			</div>	
			
			<br/>
						
			<div class="forotParticipantes">
				<table cellpadding="0" cellspacing="0" class="encabezado">
					<tr>
						<td class="titleBox">FOROS</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:558px;font-weight:bold;">TEMA FORO</td>
						<td class="t_center" style="width:150px;font-weight:bold;">PARTICIPACI&Oacute;N</td>
					</tr>				
				</table>
				<?php if(isset($pdf['foro'])):?>
					<?php foreach($pdf['foro'] as $foro):?>
						<table class="contenido" cellpadding="0" cellspacing="0">
							<tr>
								<td class="borderRight" style="width:558px;"><?php echo htmlentities($foro['nombreTema'], ENT_QUOTES, 'utf-8'); ?></td>
								<td class="t_center" style="width:150px;"><?php echo $foro['nMensajes']; ?></td>
							</tr>
						</table>	
					<?php endforeach; ?>
					<table class="contenido" cellpadding="0" cellspacing="0">
						<tr>
							<td class="t_right" style="width:558px;border-right:2px solid #AAA;font-weight:bold;">NOTA MEDIA</td>
							<td class="t_center" style="width:150px;"><b><?php echo round($foro['notaMediaForo'], 2); ?></b></td>
						</tr>						
					</table>		
				<?php endif; ?>
			</div>	
			
			<br/>
			
			<div class="ponderacionParticipantes">
				<table cellpadding="0" cellspacing="0" class="encabezado">
					<tr>
						<td class="titleBox">PONDERACI&Oacute;N FINAL DE NOTAS</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:165px;font-weight:bold;">Trabajos pr&aacute;cticos (<?php echo $configNotas->trabajos_practicos; ?>%)</td>
						<td class="borderRight" style="width:165px;font-weight:bold;">Autoevaluaciones (<?php echo $configNotas->autoevaluaciones; ?>%)</td>
						<td class="borderRight" style="width:165px;font-weight:bold;">Foro (<?php echo $configNotas->foro; ?>%)</td>
						<td style="width:165px;font-weight:bold;">Nota final</td>
					</tr>				
				</table>
				<table class="contenido" cellpadding="0" cellspacing="0">
					<tr>
						<td class="borderRight" style="width:165px;font-weight:bold;"><?php echo number_format($pdf['ponderaTP'], 2, ',', ' '); ?></td>
						<td class="borderRight" style="width:165px;font-weight:bold;"><?php echo number_format($pdf['ponderaTest'], 2, ',', ' '); ?></td>
						<td class="borderRight" style="width:165px;font-weight:bold;"><?php echo number_format($pdf['ponderaForo'], 2, ',', ' '); ?></td>
						<td style="width:165px;font-weight:bold;"><?php echo number_format($pdf['ponderaTP'] + $pdf['ponderaTest'] + $pdf['ponderaForo'], 2, ',', ' '); ?></td>
					</tr>	
				</table>	
			</div>	
						
		</page>
	<?php endforeach; ?>
