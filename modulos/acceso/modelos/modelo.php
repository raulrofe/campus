<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

Class ModeloAcceso extends modeloExtend {

	// Sistema de Logeo
	public function loggin($idperfil,$usuario,$pass)
	{
		// para invitados
		/*if($idperfil == 6)
		{
			$sql = 'SELECT * FROM invitados AS i, perfil AS pf WHERE pf.idperfil = 6  AND i.borrado = 0 AND i.usuario = "' .$usuario . '" AND i.password = "' . md5($pass) . '"';
		}
		else */if($idperfil != 'alumno')
		{
			$sql = "SELECT * from rrhh RH , perfil P
			where borrado = 0 AND RH.alias = '".$usuario."'
			and RH.pass = '".$pass."'
			and RH.idperfil = ".$idperfil."
			and P.idperfil = RH.idperfil GROUP BY RH.idrrhh";
		}
		else
		{
			$sql="SELECT * from alumnos
			where borrado = 0 AND usuario = '".$usuario."'
			and pass = '".$pass."' GROUP BY alumnos.idalumnos";
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function logginEmail($idperfil,$usuario,$pass)
	{
		if($idperfil != 'alumno')
		{
			$sql = "SELECT * from rrhh RH , perfil P
			where borrado = 0 AND RH.email = '".$usuario."'
			and RH.pass = '".$pass."'
			and RH.idperfil = ".$idperfil."
			and P.idperfil = RH.idperfil GROUP BY RH.idrrhh";
		}
		else
		{
			$sql="SELECT * from alumnos
			where borrado = 0 AND email = '" . $usuario . "'
			and pass = '" . $pass . "' GROUP BY alumnos.idalumnos";
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	

	public function obtenerAlumnoPorEmail($email)
	{
		$sql = "SELECT * from alumnos AS al
		where borrado = 0 AND al.email = '".$email."'";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	
	public function obtenerAlumnoPorId($id)
	{
		$sql = "SELECT * from alumnos AS al
		where borrado = 0 AND al.idalumnos = '".$id."'";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function actualizarPassAlumno($idalumno, $pass)
	{
		$sql = 'UPDATE alumnos SET pass = "' . $pass . '" WHERE idalumnos = ' . $idalumno;

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	public function obtenerLogErrorAccess()
	{
		$sql = 'SELECT COUNT(ip_address) AS cont FROM log_error_access WHERE ip_address = "' . Url::getIpAddress() . '" AND date >= "' . date('Y-m-d H:i:s', time() - 3600) . '"';

		$resultado = $this->consultaSql($sql);
		$resultado = $resultado->fetch_object();
		$resultado = $resultado->cont;

		return $resultado;
	}

	public function eliminarLogErrorAccess()
	{
		$sql = 'DELETE FROM log_error_access WHERE ip_address = "' . Url::getIpAddress() . '"';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function insertarLogErrorAccess() 
	{
		$sql = 'INSERT INTO log_error_access (ip_address, user_agent, date) VALUES ("' . Url::getIpAddress() . '", "' . $_SERVER['HTTP_USER_AGENT'] . '", "' . date('Y-m-d H:i:s') . '")';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function fechaFinSesion($idsesionTime){
		$sql = 'UPDATE log_tiempo_sesion SET fin_sesion = "' . date("Y-m-d H:i:s") . '" WHERE id = ' . $idsesionTime;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	public function insertarRecordarContraseña($idalumno, $token)
	{
		$sql = "INSERT INTO recordar_contrasena(idalumnos, token) VALUES (" . $idalumno . ",'" . $token . "')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	

	public function borrarRecordarContraseña($idalumno, $token)
	{
		$sql = "DELETE FROM recordar_contrasena WHERE idalumnos = " . $idalumno;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}	

	public function obtenerRegistroToken($idalumno)
	{
		$sql = "SELECT * FROM recordar_contrasena WHERE idalumnos = '" . $idalumno . "' ORDER BY id DESC LIMIT 1";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}