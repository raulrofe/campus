<?php

if (!defined('I_EXEC'))
	exit('Acceso no permitido');

if (Peticion::isGet()) {

	require PATH_ROOT . 'lib/mail/phpmailer.php';
	require PATH_ROOT . 'lib/mail/mail.php';

	$get = Peticion::obtenerGet();
	$objMail = Mail::obtenerInstancia();
	$objModeloAcceso = new ModeloAcceso();

	if (!empty($get['id']) && !empty($get['token'])) {

		$rowAlumno = $objModeloAcceso->obtenerAlumnoPorId($get['id']);

		if ($rowAlumno->num_rows == 1) {

			$alumno = $rowAlumno->fetch_object();

			$rowToken = $objModeloAcceso->obtenerRegistroToken($get['id']);

			if ($rowToken->num_rows > 0) {

				$registroToken = $rowToken->fetch_object();

				if ($registroToken->token == $get['token']) {

					$nuevaContrasena = Texto::cadenaAleatoria(10);

					if ($objModeloAcceso->actualizarPassAlumno($alumno->idalumnos, md5($nuevaContrasena))) {

						$plantilla = file_get_contents('plantillas/email_new_pass.html');
						$mensaje = str_replace('{nuevaContrasena}', $nuevaContrasena, $plantilla);

						if ($objMail->enviar(
								CONFIG_EMAIL_LOST_PASS_EMAIL, $alumno->email, CONFIG_EMAIL_LOST_PASS_NAME, $alumno->nombre . ' ' . $alumno->apellidos, Texto::decode('Nueva contraseña Campus Aula Interactiva'), $mensaje)) {

							$objModeloAcceso->borrarRecordarContraseña($alumno->idalumnos);

							Alerta::guardarMensajeInfo('enviadocontrasena','Se ha enviado tu nueva contraseña a tu email');

						}
					} else {
						Alerta::guardarMensajeInfo('norecuperarcontrasena','No se ha podido recuperar la contraseña');
					}
				} else {
					Alerta::guardarMensajeInfo('tokennovalido','Token no válido, vuelva a pedir la recuperación de contraseña');
				}
			} else {
				Alerta::guardarMensajeInfo('nosolicitado','No se ha solicitado ninguna recuperación de contraseña para este usuario');
			}
		} else {
			Alerta::guardarMensajeInfo('nousuario','No existe ningún usuario con este email');
		}
	}
}

//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'login');  
Url::redirect('');
