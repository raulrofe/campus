<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

//Actualizo la fecha de fin
$objModeloAcceso = new ModeloAcceso();
if(isset($_SESSION['idsesionTime'])){
	$objModeloAcceso->fechaFinSesion($_SESSION['idsesionTime']);
}

session_unset();
session_destroy();

echo '<div style="display:none">fin</div>';

Url::redirect('');