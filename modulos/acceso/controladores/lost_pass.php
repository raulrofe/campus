<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

if(Peticion::isPost()){ 

	require PATH_ROOT . 'lib/mail/phpmailer.php';
	require PATH_ROOT . 'lib/mail/mail.php';

	$post = Peticion::obtenerPost();
	$objMail = Mail::obtenerInstancia();
	$objModeloAcceso = new ModeloAcceso();
	
	if(isset($post['email']) && !empty($post['email']) && filter_var($post['email'], FILTER_VALIDATE_EMAIL)){

		$rowAlumno = $objModeloAcceso->obtenerAlumnoPorEmail($post['email']);

		if($rowAlumno->num_rows == 1){

			$alumno = $rowAlumno->fetch_object();
			$newPassword = Texto::cadenaAleatoria(10);
			$token = hash_hmac('sha256', Texto::cadenaAleatoria(40), $post['email']);

			$objModeloAcceso->insertarRecordarContraseña($alumno->idalumnos, $token);

			$plantilla = file_get_contents('plantillas/email_lost_pass.html');
			$mensaje = str_replace('{id}', $alumno->idalumnos, $plantilla);
			$mensaje = str_replace('{token}', $token, $mensaje);

			if($objMail->enviar(
				CONFIG_EMAIL_LOST_PASS_EMAIL,
			 	$alumno->email,
			 	CONFIG_EMAIL_LOST_PASS_NAME,
			 	$alumno->nombre . ' ' . $alumno->apellidos,
				Texto::decode('Recuperación contraseña Campus Aula Interactiva'),
				$mensaje)){
				Alerta::guardarMensajeInfo('instrucciones','Se le ha enviado un correo con las intrucciones para restaurar su contraseña');
			}

		}else{
			Alerta::guardarMensajeInfo('noemail','No existe ningu&uacute;n usuario con este email');
		}
	}else{
		Alerta::guardarMensajeInfo('emailvalido','Introduce un email válido');
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'lost_pass');