<?php if(!defined('I_EXEC')) exit('Acceso no permitido');

require_once(PATH_ROOT . 'lib/recaptchalib.php');

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;

$mi_modeloAcceso = new ModeloAcceso();

$numErrorAccess = $mi_modeloAcceso->obtenerLogErrorAccess();
$numMaxErrors = 200;

// eliminamos la session de logueo
if(Usuario::is_login())
{
	Url::redirect('cursos/');
}

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	extract($post);

	if(isset($usuario, $pass, $idperfil))
	{
		$acceso_login = false;

		if($numErrorAccess >= $numMaxErrors)
		{
			if(isset($post["recaptcha_response_field"], $post["recaptcha_challenge_field"]))
			{
				if($post["recaptcha_response_field"])
				{
					$resp = recaptcha_check_answer(
						API_RECAPTCHA_PRIVATE_KEY,
						$_SERVER["REMOTE_ADDR"],
						$post["recaptcha_challenge_field"],
						$post["recaptcha_response_field"]
					);

					if($resp->is_valid)
					{
						$acceso_login = true;
					}
					else
					{
						# set the error code so that we can display it
						$error = $resp->error;
					}
				}
			}
		}
		else
		{
			$acceso_login = true;
		}

		if($acceso_login)
		{
			$pass = md5($pass);

			$pos = strpos($usuario, '@');

			if($pos === false){
				$resultado = $mi_modeloAcceso->loggin($idperfil,$usuario,$pass);			
			} else {
				$resultado = $mi_modeloAcceso->logginEmail($idperfil,$usuario,$pass);	
			}

			if($resultado->num_rows == 1)
			{
				$f = mysqli_fetch_assoc($resultado);

				if($idperfil != 'alumno')
				{
					$_SESSION['perfil'] = $f['perfil'];

					/*if($idperfil == 6)
					{
						$_SESSION['idusuario'] = $f['idinvitado'];
					}
					else
					{*/
						$_SESSION['idusuario'] = $f['idrrhh'];
					//}
				}
				else
				{
					$_SESSION['idusuario'] = $f['idalumnos'];
					$_SESSION['encuesta'] = $f['encuesta_realizada'];
					$_SESSION['perfil'] = 'alumno';
				}

				// sessiones de fechas de acceso
				$_SESSION['usuario_fecha_actual_acceso'] = date('Y-m-d H:i:s');
				$_SESSION['usuario_fecha_ult_interaccion_foro'] = array();
				$_SESSION['usuario_fecha_notifi'] = date('Y-m-d H:i:s');

				// para el sistema de notificaciones
				$_SESSION['usuario_fecha_acceso_curso'] = null;
				$_SESSION['usuario_fecha_listado_conectados'] = array();

				//sesion para tareas pendientes
				$_SESSION['inicioTareas'] = false;

				// ELIMINA LOS LOGS DE ESTA ip
				if($numErrorAccess > 0)
				{
					$mi_modeloAcceso->eliminarLogErrorAccess();
				}

				if($_SESSION['perfil'] == 'administrador')
				{
					$session = $_SESSION;
					session_destroy();
					session_name("campusadmin");
					session_start();
					$_SESSION = $session;

					Url::redirect('admin');
				}
				else
				{
					Url::redirect('cursos/');
				}
			}
			else
			{
				$mi_modeloAcceso->insertarLogErrorAccess();
				$numErrorAccess = $mi_modeloAcceso->obtenerLogErrorAccess();
 
				Alerta::guardarMensajeInfo('usuarioincorrecto', 'La contraseña o el usuario introducido es incorrecto, intentelo de nuevo');
			}
		}
	}
}


$ieOld = mvc::getBrowser();

if(isset($ieOld))
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), '_login');
}
else
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'login');
}