<?php if(!defined('I_EXEC')) exit('Acceso no permitido');?>

<form action='' method='post'>
	<div id="login-box">
		<h2 data-translate-html="contrasena.nueva">
			Nueva contrase&ntilde;a
		</h2>
		<p data-translate-html="contrasena.recuperarinfo">
			Recupera la contrase&ntilde;a para Campus Virtual de Formaci&oacute;n AULA_INTERACTIVA
		</p>
		<br />
		<div id="login-box-name" style="margin-top:20px;margin-left:-45px;">
			<span data-translate-html="formulario.email">
				Email
			</span>
			 :
		</div>
		<div id="login-box-field" style="margin-top:20px;">
			<input type='text' name='email' tabindex="1" autofocus="autofocus" class="form-login" title="email" value="" size="30"/>
		</div>
		<p style="margin-top:56px;" data-translate-html="contrasena.recuperarinfo2">
			* La nueva contrase&ntilde;a ser&aacute; enviada al email con el que se di&oacute; de alta en el campus (solo disponible para el alumnado)
		</p>
		<br />
		<div class='t_center'>
			<button type='submit' tabindex="4" data-translate-html="formulario.enviar" style="color:white;border:solid 1px white;padding: 8px 15px;">
				Enviar
			</button>
		</div>
		<br/>
		<div id="acceso_campus_mail" class='t_center'>
			<a href="#" data-translate-html="contrasena.accede">
				Accede a tu cuenta de usuario
			</a>
		</div>
	</div>
</form>
