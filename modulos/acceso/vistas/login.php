<!DOCTYPE HTML>
<html lang="es" xml:lang="es" style="overflow: auto !important">
	<head>
		<meta charset="UTF-8">
		<title>Campus AULA_INTERACTIVA</title>

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" type="text/css" media="all" href="http://fonts.googleapis.com/css?family=Dosis:400,700">
		<link rel="stylesheet" type="text/css" media="all" href="http://fonts.googleapis.com/css?family=PT+Sans:400,700">

		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/motioncss.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/motioncss-widgets.css">

		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/jquery.jgrowl.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/style.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/responsive.css">
		<link rel="stylesheet" type="text/css" media="all" href="parallax/css/colors/color_0.css">



		<script type="text/javascript" src="parallax/js/modernizr.js"></script>
		<script type="text/javascript" src="parallax/js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="parallax/js/mediaelement-and-player.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.easing.min.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.carousello.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.isotope.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.magnific-popup.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.jgrowl.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.parallax.js"></script>
		<script type="text/javascript" src="parallax/js/jquery.queryloader2.js"></script>
		<script type="text/javascript" src="parallax/js/waypoints.min.js"></script>
		<script type="text/javascript" src="parallax/js/responsive.js"></script>
		<script type="text/javascript" src="parallax/js/plugins.js"></script>
		<script type="text/javascript" src="parallax/js/us.widgets.js"></script>

		<!-- GMap-->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="parallax/js/jquery.gmap.min.js"></script>

		<!-- Translate -->
		<script type="text/javascript" src="js/translate.js"></script>

		<style>
			#mensajeInformacion {
				width: auto;
				max-width: 400px;
				min-width: 100px;
				position: fixed;
				top: 15px;
				padding: 10px 20px;
				text-align: center;
				background: #fef8ec;
				/*border: 3px solid #65574c;*/
				border: 3px solid #a24309;
				color: #a24309;
				cursor: pointer;
				font-size: 1.1em;
				font-weight: bold;
				overflow: hidden;
				z-index: 100;

				/* for IE */
				filter:alpha(opacity=90);
				/* CSS3 standard */
				opacity:0.9;

				box-shadow: 0 0 10px #000;

				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
			}
		</style>
	</head>

	<body class="l-body headertype_sticky headerpos_bottom hometype_fullscreen">

		<?php if (!defined('I_EXEC')) exit('Acceso no permitido'); ?>

		<script type="text/javascript">
			var RecaptchaOptions = {
				theme: 'clean',
				lang: 'es'
			};
		</script>
		<noscript>Su navegador no soporta JavaScript, activelo e intentelo de nuevo</noscript>

		<div class='l-preloader'></div>
		<!-- HEADER -->
		<div class="l-header">
			<div class="l-header-h">

				<!-- subheader -->
				<div class="l-subheader">
					<div class="l-subheader-h i-cf">

						<!-- logo -->
						<div class="w-logo">
							<div class="w-logo-h">
								<a class="w-logo-link" href="http://www.campusaulainteractiva.com">
									<img class="w-logo-img" src="parallax/img/logo_campus.png" data-translate-title="login.campus" title="Campus AULA INTERACTIVA" alt="logotipo campus aula interactiva">
									<span class="w-logo-title">
										<span class="w-logo-title-h"><h1 data-translate-html="login.campus">CAMPUS AULA_INTERACTIVA</h1></span>
									</span>
								</a>
							</div>
						</div>

						<!-- socials -->

						<!-- nav -->
						<nav class="w-nav">
							<div class="w-nav-h">
								<div class="w-nav-control">
									<img src="imagenes/menu_vistas/list.png"  data-translate-title="login.menu"/>
								</div>
								<div class="w-nav-list layout_hor width_auto level_1">
									<div class="w-nav-list-h">
										<div class="w-nav-item level_1">
											<div class="w-nav-item-h">
												<a href="#home" class="w-nav-anchor level_1"><span class="w-nav-title" data-translate-html="login.inicio">Inicio</span><span class="w-nav-hint"></span></a>
											</div>
										</div>
										<div class="w-nav-item level_1">
											<div class="w-nav-item-h">
												<a href="#contact" class="w-nav-anchor level_1"><span class="w-nav-title" data-translate-html="login.contacto">Contacto</span><span class="w-nav-hint"></span></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</nav>

					</div>
				</div>

			</div>
		</div>
		<!-- /HEADER -->

		<!--LANGUAGES BUTTONS-->
		<div id="languages-login">
			<a data-change-translate="es" >
				<img src="imagenes/banderas/es.png" data-translate-title="menu.espanol"/>
			</a>
			<a data-change-translate="ch">
				<img src="imagenes/banderas/ch.png" data-translate-title="menu.chino"/>
			</a>
			<a data-change-translate="en">
				<img src="imagenes/banderas/en.png" data-translate-title="menu.ingles"/>
			</a>
		</div>
		<!--LANGUAGES BUTTONS-->

		<!-- MAIN -->
		<div class="l-main">
			<div class="l-main-h">
				<!-- page: HOME -->
				<section id="home" class="l-section">
					<div id="parallax_subsection_0" class="l-subsection with_parallax" style="background-image:url(parallax/img/placeholder/aula02.jpg);">		
						<div class="l-subsection">
							<div class="l-subsection-h">
								<div class="l-subsection-hh g-html i-cf">
									<div id="franja">
										<div id="logo_bg">
											<img src="parallax/img/placeholder/logotipo.png"  data-translate-title="login.campus"/>
										</div>
										<div id="form_bg">
											<div class="w-form-loggin">
												<div class="w-form-loggin-h ">
													<form class="g-form" action="<?php echo URL_BASE ?>" method="post" id="loggin_form" 
														  style="
														  background-color:#227496;
														  opacity:0.8;
														  padding:15px;
														  margin:7%;
														  -webkit-box-shadow: 0px 0px 54px -5px rgba(0,0,0,0.75);
														  -moz-box-shadow: 0px 0px 54px -5px rgba(0,0,0,0.75);
														  box-shadow: 0px 0px 54px -5px rgba(0,0,0,0.75);
														  ">
														<div class="g-form-row" id="name_row_login">
															<div class="g-form-row-label" style="color:#FFF;font-weight:bold;text-align:center;">
																<label class="g-form-row-label-h" for="name">
																	<span style="font-size:2em;" data-translate-html="login.titulo">Acceso al Campus</span>
																	<br>
																	<span style="font-size:0.8em;" data-translate="1" data-translate-html="login.subtitulo">
																		Bienvenido al Campus Virtual de Formaci&oacute;n AULA_INTERACTIVA
																	</span>
																</label>
															</div>
															<div class="g-form-row-state" id="name_state-login"></div>
														</div>											
														<div class="g-form-row" id="user_row">
															<div class="g-form-row-label" style="float:left;margin-right:10px;width:25%;">
																<label class="g-form-row-label-h" for="usuario" style="color:#FFF;font-weight:bold;" data-translate-html="login.usuario">Usuario:</label>
															</div>
															<div class="g-form-row-field" style="float:left;">
																<input type="text" name="usuario" id="usuario" value="maricielocoord">
															</div>
															<div class="g-form-row-state" id="usuario_state"></div>
														</div>
														<div class="g-form-row" id="password_row">
															<div class="g-form-row-label" style="float:left;margin-right:10px;width:25%;">
																<label class="g-form-row-label-h" for="pass" style="color:#FFF;font-weight:bold;" data-translate-html="login.password">Contrase&ntilde;a:</label>
															</div>
															<div class="g-form-row-field" style="float:left">
																<input type="password" name="pass" id="pass" value="123456">
															</div>
															<div class="g-form-row-state" id="password_state"></div>
														</div>
														<div class="g-form-row" id="email2_row">
															<div class="g-form-row-label" style="float:left;margin-right:10px;width:25%;">
																<label class="g-form-row-label-h" for="idProfilesUsers" style="color:#FFF;font-weight:bold;" data-translate-html="login.perfil">Perfil:</label>
															</div>
															<div class="g-form-row-field" style="float:left;">
																<select name="idperfil" title="listado de perfiles" id="idProfilesUsers">
																	<option  data-translate-html="login.selector.alumno" value="alumno" style='padding:0 0 0 5px;'>Alumno</option>
																	<option  data-translate-html="login.selector.2" value="2" style='padding:0 0 0 5px;'>Supervisor</option>
																	<option  data-translate-html="login.selector.4" value="4" style='padding:0 0 0 5px;'>Tutor</option>
																	<option  data-translate-html="login.selector.3" value="3" style='padding:0 0 0 5px;'>Coordinador</option>
																	<option  data-translate-html="login.selector.6" value="6" style='padding:0 0 0 5px;'>Invitado</option>
																</select>
															</div>
															<div class="g-form-row-state" id="profile_state"></div>
														</div>
														<div class="g-form-row">
															<div class="g-form-row-label" style="float:left;margin-right:10px;width:25%;"></div>
															<div class="g-form-row-field" style="float:left;width:72%;text-align:left;color:#FFF;font-size:0.8em;">
																<a style="color:#FFF" href="solicitar-nueva-contrasena" data-translate-html="login.resetpassword" >He olvidado mi contraseña</a>
															</div>
														</div>										
														<div class="g-form-row">
															<div class="g-form-row-label" style="float:left;margin-right:10px;width:25%;"></div>
															<div class="g-form-row-field" style="float:left;text-align:left">
																<input type="submit" value="Acceder" class="g-btn type_primary" id="message_send_login" data-translate-value="login.acceder" />
																<!-- <button class="g-btn type_primary" id="message_send_login"><span>Acceder</span></button> -->
															</div>
														</div>
														<div style="text-align:center;font-size:0.8em;color:#FFF" data-translate-html="login.pie">Si no puede acceder póngase en <a href="#" style="color:#FFF">contacto con nosotros</a></div>
														<?php
														if ($numErrorAccess >= $numMaxErrors) {
															echo '<div style="color: white;text-align: center;line-height: 1em;font-size: 0.9em;margin-bottom: 5px;border-top: 1px dashed white;padding-top: 15px;margin: 10px 0 10px 0;"
															 data-translate-html="login.intentos">
																	El n&uacute;mero de intentos ha excedido, por seguridad intenta logearte rellenando el texto de seguridad
																</div>';
															echo recaptcha_get_html(API_RECAPTCHA_PUBLIC_KEY, $error);
														}
														?>
													</form>
												</div>
											</div>
										</div>
										<div style="clear:both;"></div>
									</div>							
								</div>
							</div>
						</div>	
					</div>			
				</section>

				<!-- page: CONTACT -->
				<section id="contact" class="l-section">

					<div class="l-subsection color_dark" style="background-image:url(parallax/img/placeholder/parallax_contacto.jpg);">
						<div class="l-subsection-h">
							<div class="l-subsection-hh g-html i-cf">

								<h2><strong data-translate-html="login.contacto" >Contacto</strong></h2>

							</div>
						</div>
					</div>

					<div class="l-subsection">
						<div class="l-subsection-h">
							<div class="l-subsection-hh g-html i-cf">

								<p class="size_big" style="text-align:center" data-translate-html="contacto.titulo" >Para contactar con nosotros, rellene el siguiente formulario:</p>

								<div class="w-form contacts_form">
									<div class="w-form-h contacts_form_form">
										<form class="g-form" action="<?php echo URL_BASE ?>" method="post" id="contact_form">
											<div class="g-form-row" id="name_row">
												<div class="g-form-row-label">
													<label class="g-form-row-label-h" for="name" data-translate-html="formulario.nombre" >Nombre</label>
												</div>
												<div class="g-form-row-field">
													<input type="text" name="name" id="name">
												</div>
												<div class="g-form-row-state" id="name_state"></div>
											</div>
											<div class="g-form-row" id="email_row">
												<div class="g-form-row-label">
													<label class="g-form-row-label-h" for="email" data-translate-html="formulario.email" >Email</label>
												</div>
												<div class="g-form-row-field">
													<input type="email" name="email" id="email">
												</div>
												<div class="g-form-row-state" id="email_state"></div>
											</div>
											<div class="g-form-row" id="phone_row">
												<div class="g-form-row-label">
													<label class="g-form-row-label-h" for="phone" data-translate-html="formulario.telefono" >Número de Teléfono</label>
												</div>
												<div class="g-form-row-field">
													<input type="text" name="phone" id="phone">
												</div>
												<div class="g-form-row-state" id="phone_state"></div>
											</div>
											<div class="g-form-row" id="message_row">
												<div class="g-form-row-label">
													<label class="g-form-row-label-h" for="message" data-translate-html="formulario.mensaje" >Mensaje</label>
												</div>
												<div class="g-form-row-field">
													<textarea name="message" id="message" cols="30" rows="10"></textarea>
												</div>
												<div class="g-form-row-state" id="message_state"></div>
											</div>
											<div class="g-form-row">
												<div class="g-form-row-label"></div>
												<div class="g-form-row-field" style="text-align:center">
													<input type="submit" class="g-btn type_primary" id="message_send"  data-translate-value="formulario.enviar_mensaje" value="Enviar mensaje">
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>

				</section>

			</div>
		</div>
		<!-- /MAIN -->

		<!-- FOOTER -->
		<div class="l-footer">
			<div class="l-footer-h">



				<!-- subfooter: bottom -->
				<div class="l-subfooter at_bottom">
					<div class="l-subfooter-h i-cf">

						<div class="w-copyright" data-translate-html="footer.copyright">© 2014 Fundación AULA_<strong>SMART</strong>® - Todos los derechos reservados.</div>

						<div class="w-socials size_normal">
							<div class="w-copyright">
								<a href="parallax/avisolegal.html" data-translate-html="footer.aviso_legal">Aviso Legal</a>
							</div>

						</div>

					</div>
				</div>

			</div>
		</div>
		<!-- /FOOTER -->

		<a class="w-toplink" href="#"><img src="imagenes/mis_progresos/arrow-nota-media.png"  data-translate-html="login.titulo" alt="subir" /></a>

		<script type="text/javascript" src="js/alert.js"></script>
		<script type="text/javascript">
			$(document).ready(function () {
<?php if (!empty($_SESSION['mensajeInfo'])): ?>
					alertMsg(<?php echo "'" . $_SESSION['mensajeInfo'] . "'"; ?>);
	<?php $_SESSION['mensajeInfo'] = null; ?>
<?php endif; ?>
			});
		</script>

	</body>
</html>
