<!-- 
<div id="ayuda_descargas">
	<div class='subtitleModulo'><span>Programas Necesarios</span></div>
	<p class='t_justify'>Para utilizar el audio de la Herramienta Multimedia es necesario instalar el siguiente programa:</p><br/>
	
	<div class='burbuja'>
		<a href="http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-client-419417.html#7372-jmf-2.1.1e-oth-JPR" title="" target="_blank">
			<img src="imagenes/iconos_programas/jmf.gif" alt="" /><span>Java Media Framework</span>
		</a>.
	</div>
		
	<br />
	
	<div class='subtitleModulo'><span>Programas &Uacute;tiles</span></div>
	<p class='t_justify'>Para visualizar los recursos incorporados al archivador electr&oacute;nico, adjuntos de correo, etc., es posible que necesite alguno de los siguientes visores:</p><br/>
	
	<div class='burbuja'>
		<a href="http://www.winzip.com/downwz.htm" title="" target="_blank"><img src="imagenes/iconos_programas/winzip.jpg" alt="" /><span>Winzip -&gt; Descomprimir archivos ZIP</span></a>.
		<br />
		<a href="http://www.7-zip.org/download.html" title="" target="_blank"><img src="imagenes/iconos_programas/7zip.png" alt="" /><span>7zip -&gt; Alternativa para descomprimir archivos ZIP</span></a>.
		<br />
		<a href="http://get.adobe.com/es/reader/" title="" target="_blank"><img src="imagenes/iconos_programas/acrobatreader.jpg" alt="" /><span>Adobe Acrobat Reader -&gt; Archivos PDF</span></a>.
		<br />
		<a href="http://get.adobe.com/es/flashplayer/" title="" target="_blank"><img src="imagenes/iconos_programas/flash.jpg" alt="" /><span>Macromedia Flash -&gt; Animaciones Flash</span></a>.
		<br />
		<a href="http://windows.microsoft.com/es-ES/windows/downloads/windows-media-player" title="" target="_blank"><img src="imagenes/iconos_programas/winmedia.jpg" alt="" /><span>Windows Media -&gt; Videos</span></a>.
	</div>
</div>
-->



<div style="padding:20px;">

	<div class='subtitle t_center'>
		<img src="imagenes/descargas/descargas.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="descargas.titulo">
			DESCARGAS
		</span>
	</div>
	<br/>

	<!-- menu de iconos -->
	<div class='fleft panelTabMenu' id="navDescargas">
		<div id="pNecesarios" class="blanco redondearBorde">
			<img src="imagenes/putiles.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' 
			   data-translate-html="descargas.necesario">
				Programas Necesarios
			</p>
		</div>
		<div id="pUtiles" class="redondearBorde">
			<img src="imagenes/pnecesarios.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' 
			   data-translate-html="descargas.tab1">
				Programas Útiles
			</p>
		</div>
	</div>
	<!-- Fin menu iconos -->


	<div class='fleft panelTabContent' id="contentDescargas">

		<!-- prgramas necesarios -->

		<div id="programasNecesarios">
			<p class="tituloTabs" data-translate-html="descargas.necesario">
				Programas Necesarios
			</p>
			<p class='t_justify' data-translate-html="descargas.descripcion1">
				Para utilizar el audio de la Herramienta Multimedia es necesario instalar el siguiente programa:
			</p>
			<br/>
			<div>
				<div class="listProgramas">
					<a href="http://get.adobe.com/es/reader/" title="" target="_blank">
						<img src="imagenes/iconos_programas/acrobatreader.jpg" alt="" />
						<span data-translate-html="descargas.reader">
							Adobe Acrobat Reader (Archivos PDF)
						</span>
					</a>
				</div>
				<div class="listProgramas">
					<a href="http://get.adobe.com/es/flashplayer/" title="" target="_blank">
						<img src="imagenes/iconos_programas/flash.jpg" alt="" />
						<span data-translate-html="descargas.flash">
							Macromedia Flash (Animaciones Flash)
						</span>
					</a>
				</div>
			</div>
		</div>

		<!-- Fin programas necesarios -->

		<!-- Programas utiles -->
		<div id="programasUtiles" class="hide" >
			<p class="tituloTabs" data-translate-html="descargas.tab1">
				Programas Útiles
			</p>
			<p class='t_justify' data-translate-html="descargas.descripcion2">
				Para visualizar los recursos incorporados al archivador electr&oacute;nico, adjuntos de correo, etc., es posible que necesite alguno de los siguientes visores:
			</p>
			<br/>

			<div>
				<div class="listProgramas">
					<a href="https://www.google.com/chrome?hl=es" title="" target="_blank">
						<img src="imagenes/iconos_programas/chrome.png" alt="" />
						<span data-translate-html="descargas.chrome">
							Google Chrome -&gt; Navegador web muy r&aacute;pido
						</span>
					</a>
				</div>
				<div class="listProgramas">
					<a href="http://www.winzip.com/downwz.htm" title="" target="_blank">
						<img src="imagenes/iconos_programas/winzip.jpg" alt="" />
						<span data-translate-html="descargas.winzip">
							Winzip -&gt; Descomprimir archivos ZIP
						</span>
					</a>
				</div>
				<div class="listProgramas">
					<a href="http://www.7-zip.org/download.html" title="" target="_blank">
						<img src="imagenes/iconos_programas/7zip.png" alt="" />
						<span data-translate-html="descargas.7zip">
							7zip -&gt; Alternativa para descomprimir archivos ZIP
						</span>
					</a>
				</div>
				<div class="listProgramas">
					<a href="http://windows.microsoft.com/es-ES/windows/downloads/windows-media-player" title="" target="_blank">
						<img src="imagenes/iconos_programas/winmedia.jpg" alt="" />
						<span data-translate-html="descargas.media">
							Windows Media -&gt; Videos
						</span>
					</a>
				</div>
			</div>
		</div>

		<!-- Fin de programas utiles -->

	</div>
	<div class='clear'></div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		$("#pNecesarios").click(function () {
			$("#pUtiles").removeClass("blanco");
			$("#pNecesarios").addClass("blanco");
			$("#programasUtiles").addClass("hide");
			$("#programasNecesarios").removeClass("hide");
		});

		$("#pUtiles").click(function () {
			$("#pNecesarios").removeClass("blanco");
			$("#pUtiles").addClass("blanco");
			$("#programasUtiles").removeClass("hide");
			$("#programasNecesarios").addClass("hide");
		});

	});
</script>