<div id="chat_tutoria_privado_externo_<?php echo $get['idtutoria'];?>" class="chat_tutoria_privado">
	<h2>Tutor&iacute;a privada con <?php echo Texto::textoPlano($tutoria->nombrec)?></h2>
	<div class="chat_tutoria_privado_mensajes"></div>
	<div class="clear"></div>
	
	<form id="frm_chat_tutoria_privado_externo_<?php echo $get['idtutoria'];?>" name="frm_chat_tutoria_privado_externo_<?php echo $get['idtutoria'];?>"
		action="" method="post" onsubmit="LibChatTutoriaPrivadoExterno.chat_enviar('#chat_tutoria_privado_externo_<?php echo $get['idtutoria'];?>'); return false;">
		<ul>
			<li>
				<textarea id="chat_tutoria_privado_externo_input_msg_<?php echo $get['idtutoria']?>" class="chat_tutoria_privado_input_msg" cols="1" rows="1" name="mensaje"></textarea>
				<input class="chat_tutoria_privado_idusuario_objetivo" type="hidden" name="idtutoria" value="<?php echo $get['idtutoria']?>" />
			</li>
			<li class="fright"><button type="submit">Enviar</button></li>
		</ul>
		<div class="clear"></div>
	</form>
	
	<div class="chat_horario">Tutor&iacute;a desde las <?php echo date('H:i', strtotime($tutoria->hora_inicio, true))?> hasta las <?php echo date('H:i', strtotime($tutoria->hora_fin, true))?></div>
</div>

<script type="text/javascript" src="js-chat_tutoria_priv-defaultPrivado.js"></script>

<script type="text/javascript">
	$('#popupModal_chat_tutoria_privado_externo_<?php echo $get['idtutoria']?> .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');

	LibChatTutoriaPrivadoExterno.chat_send(0, 1, LibChatTutoriaPrivadoExterno.obtenerNuevosMensajes, '#chat_tutoria_privado_externo_<?php echo $get['idtutoria'];?>');

	ckeditor_create('chat_tutoria_privado_externo_input_msg_<?php echo $get['idtutoria']?>', 60);
</script>