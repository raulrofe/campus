<div id="chat_tutorias_privadas_externas">
	<h1>Tutorias privadas abiertas</h1>
	<div>
		<?php while($tutoria = $tutorias->fetch_object()):?>
			<div class="chat_tutorias_privadas_externas_elemento">
				<div>
					<a href="#" onclick="popup_open('Tutor&iacute;a privada con <?php echo Texto::textoPlano($tutoria->nombrec);?>', 'chat_tutoria_privado_externo_<?php echo $tutoria->idalumnos;?>', 'aula/tutorias/privada/chat/<?php echo $tutoria->idtutoria_priv;?>', 400, 600); return false;" title="">
						<?php echo Texto::textoPlano($tutoria->nombrec);?>
					</a>
				</div>
				<div class="chat_tutorias_privadas_externas_elemento_pie">
					<?php echo date('H:i', strtotime($tutoria->hora_inicio));?>
					- <?php echo date('H:i', strtotime($tutoria->hora_fin));?>
				</div>
			</div>
		<?php endwhile;?>		
	</div>
</div>