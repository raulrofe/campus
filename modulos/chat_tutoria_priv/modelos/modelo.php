<?php
class ModeloChatTutoriaPrivada extends modeloExtend
{
	public function nuevoMensaje($iddestinatario, $idremitente, $contenido, $idtutoria)
	{		
		$sql = 'INSERT INTO chat_tutoria_privado (idremitente, iddestinatario, mensaje, fecha, idtutoria)' .
		' VALUES ("' . $idremitente . '", "' . $iddestinatario . '", "' . $contenido . '", "' . date('Y-m-d H:i:s') . '", ' . $idtutoria . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajes($idtutoria, $idUltimoMsg = null)
	{
		$addQuery = null;
		if(isset($idUltimoMsg))
		{
			$addQuery = ' AND ctp.id > ' . $idUltimoMsg;
		}

		$sql = 'SELECT ctp.id, ctp.mensaje, ctp.idremitente, ctp.iddestinatario,' .
		' CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec, rh.nombrec AS t_nombrec' .
		' FROM chat_tutoria_privado AS ctp' .
		' RIGHT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = ctp.idremitente OR CONCAT("t_", rh.idrrhh) = ctp.iddestinatario' .
		' LEFT JOIN tutoria_privada AS tp ON tp.idtutoria_priv = ctp.idtutoria' .
		' RIGHT JOIN alumnos AS al ON al.idalumnos = ctp.idremitente OR al.idalumnos = ctp.iddestinatario' .
		' WHERE ctp.idtutoria = ' . $idtutoria . ' AND tp.dia_semana = ' . date('N') . ' AND ctp.fecha >= "' . date('Y-m-d H:i:s', time() - 3600) . '"' . $addQuery .
		' GROUP BY ctp.id';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutoria($idTutoria)
	{
		$horaActual = date('H:i:s');
		$diaSemana = date('w');
		$idUsuario = Usuario::getIdUser(true);
		$idCurso = Usuario::getIdCurso();

		$sql = "SELECT tp.idtutoria_priv, tp.dia_semana, tp.hora_inicio, tp.hora_fin, tp.idrrhh, a.idalumnos, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec from tutoria_privada AS tp" .
		" LEFT JOIN alumnos AS a ON a.idalumnos = tp.idalumnos" .
		" LEFT JOIN staff AS st ON st.idcurso = " . $idCurso .
		" WHERE tp.idtutoria_priv = '" . $idTutoria . "' AND tp.hora_inicio < '" . $horaActual . "' AND tp.hora_fin > '" . $horaActual . "'" . ' AND tp.dia_semana = ' . $diaSemana .
		" AND (tp.idalumnos = '" . $idUsuario . "' OR CONCAT('t_', st.idrrhh) = '" . $idUsuario . "')" .
		" GROUP BY tp.idtutoria_priv";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionAlumno($idUsuario, $idCurso, $fecha)
	{
		$sql = 'UPDATE matricula SET fecha_conexion_tutoria = "' . $fecha . '" WHERE idalumnos = ' . $idUsuario . ' AND idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarFechaConexionTutor($idUsuario, $fecha)
	{
		$sql = 'UPDATE rrhh SET fecha_conexion = "' . $fecha . '" WHERE idrrhh = ' . $idUsuario;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnosConectados($idCurso = null)
	{
		$addQuery = null;
		if(isset($idCurso))
		{
			$addQuery = ' AND c.idcurso = ' . $idCurso;
		}
		
		$sql = 'SELECT m.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.foto, c.idcurso, t.idtutoria, c.titulo AS titulocurso' .
		' FROM matricula AS m' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' LEFT JOIN curso AS c ON c.idcurso = m.idcurso' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idcurso = c.idcurso' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = tc.idtutoria' .
		' WHERE t.dia_semana = ' . date('N') . $addQuery . ' AND m.fecha_conexion_tutoria >= "' . date('Y-m-d H:i:s', time()-15) . '"' .
		' GROUP BY c.idcurso, m.idalumnos';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTutoresConectados($idCurso = null)
	{
		$addQuery = null;
		if(isset($idCurso))
		{
			$addQuery = ' AND c.idcurso = ' . $idCurso;
		}
		
		$sql = 'SELECT st.idrrhh, rh.nombrec, rh.foto, c.idcurso, t.idtutoria' .
		' FROM staff AS st' .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = st.idrrhh' .
		' LEFT JOIN curso AS c ON c.idcurso = st.idcurso' .
		' LEFT JOIN tutoria_cursos AS tc ON tc.idcurso = c.idcurso' .
		' LEFT JOIN tutoria AS t ON t.idtutoria = tc.idtutoria' .
		' WHERE t.dia_semana = ' . date('N') . $addQuery . ' AND (st.status = "tutor1" OR st.status = "tutor2") AND rh.fecha_conexion  >= "' . date('Y-m-d H:i:s', time()-15) . '"' .
		' GROUP BY st.idrrhh';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerTutoriasPrivadas()
	{
		$horaActual = date('H:i:s');
		$diaSemana = date('w');
		$idUsuario = Usuario::getIdUser(true);
		$idCurso = Usuario::getIdCurso();

		$sql = "SELECT tp.idtutoria_priv, tp.dia_semana, tp.hora_inicio, tp.hora_fin, a.idalumnos, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec from tutoria_privada AS tp" .
		" LEFT JOIN alumnos AS a ON a.idalumnos = tp.idalumnos" .
		" LEFT JOIN staff AS st ON st.idcurso = " . $idCurso .
		" WHERE tp.hora_inicio < '" . $horaActual . "' AND tp.hora_fin > '" . $horaActual . "'" . ' AND tp.dia_semana = ' . $diaSemana .
		" AND (tp.idalumnos = '" . $idUsuario . "' OR CONCAT('t_', st.idrrhh) = '" . $idUsuario . "')" .
		" GROUP BY tp.idtutoria_priv";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}