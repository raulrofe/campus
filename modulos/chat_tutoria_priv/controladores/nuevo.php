<?php
$post = Peticion::obtenerPost();
if(Peticion::isPost() && isset($post['idtutoria'], $post['mensaje'])
	&& is_numeric($post['idtutoria']) && !empty($post['mensaje']))
{
	$idCurso = null;
	$idrrhh = null;
	if(Usuario::compareProfile('alumno'))
	{
		$idCurso = Usuario::getIdCurso();
	}
	else
	{
		$idrrhh = Usuario::getIdUser(true);
	}

	$objModelo = new ModeloChatTutoriaPrivada();
	
	$tutoria = $objModelo->obtenerTutoria($post['idtutoria']);
	if($tutoria->num_rows == 1)
	{
		$tutoria = $tutoria->fetch_object();
		
		$destinatario = null;
		
		$idUserLogin = Usuario::getIdUser();
		if(Usuario::compareProfile('alumno'))
		{
			$objModelo->nuevoMensaje('t_' . $tutoria->idrrhh, $idUserLogin, $post['mensaje'], $tutoria->idtutoria_priv);
		}
		else
		{
			$objModelo->nuevoMensaje($tutoria->idalumnos, 't_' . $idUserLogin, $post['mensaje'], $tutoria->idtutoria_priv);
		}

	}
}