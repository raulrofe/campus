<?php
$get = Peticion::obtenerGet();

if(isset($get['idtutoria']) && is_numeric($get['idtutoria']))
{
	$objModelo = new ModeloChatTutoriaPrivada();
	
	$tutoria = $objModelo->obtenerTutoria($get['idtutoria']);
	if($tutoria->num_rows == 1)
	{
		$tutoria = $tutoria->fetch_object();
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'privado');
	}
	else
	{
		echo 'No hay tutoria';
	}
}