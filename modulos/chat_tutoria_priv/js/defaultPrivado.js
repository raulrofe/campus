LibChatTutoriaPrivadoExterno = {};

window.chatPrivado_finTutoriaCerca = 0;

/*
LibChatTutoriaPrivado.establecerConfig = function(element)
{
	//LibChatTutoriaPrivado.http(elemento);
	
	$(element).parents('.popupModal').find('.popupModalHeaderIconReload').click(function()
	{
		
	});
};
*/

LibChatTutoriaPrivadoExterno.http = function(){
    if(typeof window.XMLHttpRequest!='undefined'){
        return new XMLHttpRequest();
    }else{
        try{
            return new ActiveXObject('Microsoft.XMLHTTP');
        }catch(e){
            alert('Su navegador no soporta AJAX');
            return false;
        }
    }
};

LibChatTutoriaPrivadoExterno.chatComprobarSiEstaAbierto = function(H, element)
{
	if($(element).parents('.popupModal').size() == 0)
	{
	    H.abort();
	    H=null;
	}
	else
	{
		setTimeout(function(){
			LibChatTutoriaPrivadoExterno.chatComprobarSiEstaAbierto(H, element);
		}, 50);
	}
};

LibChatTutoriaPrivadoExterno.chat_send = function(idLastMsg, ini, callback, element)
{
	var H=new http();
	    if(!H)return;
	    H.open('post','aula/tutorias/privada/chat/' + $(element).find('input[name="idtutoria"]').val() + '/mensajes/obtener/' + idLastMsg + '/' + ini
	    		+ '/' + window.chatPrivado_finTutoriaCerca + '/' + $(element).find('.chat_tutoria_privado_idusuario_objetivo').val(), true);
	    H.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	    H.onreadystatechange=function(){
	        if(H.readyState==4){
	            if(callback)
	                callback(H.responseText, element);
	            H.onreadystatechange=function(){};
	            H.abort();
	            H=null;
	        };
	    };
	    H.send();

		setTimeout(function(){
			LibChatTutoriaPrivadoExterno.chatComprobarSiEstaAbierto(H, element);
		}, 50);
		
		$(element).parents('.popupModal').find('.popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled').removeAttr('onclick').attr('onclick', 'return false');
};

LibChatTutoriaPrivadoExterno.obtenerNuevosMensajes = function(json, element)
{
	try
	{
		json = jQuery.parseJSON(json);
		if(json != null)
		{
			if(json.finTutoria != undefined && json.finTutoria == 1)
			{
				$(element).append(
					'<div class="chat_tutoria_disabled"></div>' +
					'<div class="chat_tutoria_disabled_content">' +
						'<div class="negrita">La tutor&iacute;a ha finalizado</div>' +
					'</div>'
				);
			}
			else
			{
				if(json.finTutoriaCerca != undefined)
				{
					$(element).find('.chat_tutoria_privado_mensajes').append('<div class="elemento"><i>La tutoria finalizara en 1 minuto</i></div>');
					$(element).find('.chat_tutoria_privado_mensajes').scrollTop($(element).find('.chat_tutoria_privado_mensajes').prop('scrollHeight'));
					
					window.chatPrivado_finTutoriaCerca = 1;
				}
				
				if(json.msg != undefined)
				{
					for(var i in json.msg)
					{
						$(element).find('.chat_tutoria_privado_mensajes').append('<div class="elemento"><strong>' + json.msg[i].nombre + ':</strong> ' + json.msg[i].contenido + '</div>');
					}
				}
				
				if(json != undefined && json.idLastMsg != undefined)
				{
					$(element).find('.chat_tutoria_privado_mensajes').scrollTop($(element).find('.chat_tutoria_privado_mensajes').prop('scrollHeight'));
				}
				
				setTimeout(function(){
					LibChatTutoriaPrivadoExterno.chat_send(json.idLastMsg, 0, LibChatTutoriaPrivadoExterno.obtenerNuevosMensajes, element);
				},10);
			}
		}
	}
	catch(e)
	{
		
	}
};

LibChatTutoriaPrivadoExterno.chat_enviar = function(element)
{
	var id = $(element).parents('.popupModal').find('.chat_tutoria_privado_input_msg').attr('id');

	CKEDITOR.instances[id].updateElement();
	
	$.ajax({
		url : 'aula/tutorias/privada/chat/mensaje/nuevo',
		type: 'POST',
		data : $(element).find('form').serialize()
	}).success(function(json)
	{		
		CKEDITOR.instances[id].setData('');
		CKEDITOR.instances[id].updateElement();
	}).complete(function(){
		$(element).parents('.popupModal').find('.popupModalHeaderLoader').addClass('hide');
	});
};