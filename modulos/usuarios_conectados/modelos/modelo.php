<?php

if (!defined('I_EXEC'))
	exit('Acceso no permitido');

class ModeloUsuariosConectados extends modeloExtend {

	public function obtenerAlumnosConectadosParaAlumnos($idusuario, $idcurso) {
		if (empty($idcurso)) {
			$idcurso = 0; // Forzamos una busuqeda vacia
		}
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, c.idcurso, c.titulo AS titulocurso' .
			' FROM alumnos AS al' .
			' LEFT JOIN curso AS c ON c.idcurso = ' . $idcurso .
			' LEFT JOIN matricula AS m ON m.idalumnos = al.idalumnos AND m.idcurso = c.idcurso' .
			' WHERE m.fecha_conexion_actual > "' . date('Y-m-d H:i:s', time() - 10) . '" AND al.idalumnos != ' . $idusuario .
			' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerAlumnosConectadosParaRRHH($idusuario) {
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, c.idcurso, c.titulo AS titulocurso, p.perfil_mostrar' .
			' FROM alumnos AS al' .
			' LEFT JOIN staff AS st ON st.idrrhh = ' . $idusuario .
			' LEFT JOIN rrhh AS rh ON rh.idrrhh = st.idrrhh' .
			' LEFT JOIN perfil as p ON rh.idperfil = p.idperfil' .
			' LEFT JOIN curso AS c ON c.idcurso = st.idcurso' .
			' LEFT JOIN matricula AS m ON m.idalumnos = al.idalumnos AND m.idcurso = st.idcurso' .
			' WHERE m.fecha_conexion_actual > "' . date('Y-m-d H:i:s', time() - 10) . '"' .
			' GROUP BY al.idalumnos';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerRRHHConectadosParaAlumnos($idcurso) {
		$sql = 'SELECT rh.idrrhh, rh.nombrec, p.perfil_mostrar' .
			' FROM rrhh AS rh' .
			' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
			' LEFT JOIN perfil as p ON rh.idperfil = p.idperfil' .
			' WHERE st.fecha_conexion_actual > "' . date('Y-m-d H:i:s', time() - 10) . '"' .
			' GROUP BY rh.idrrhh';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerRRHHConectadosParaRRHH($idusuario) {
		$sql = 'SELECT rh.idrrhh, rh.nombrec, p.perfil_mostrar' .
			' FROM rrhh AS rh' .
			' LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh' .
			' LEFT JOIN perfil as p ON rh.idperfil = p.idperfil' .
			' WHERE st.fecha_conexion_actual > "' . date('Y-m-d H:i:s', time() - 10) . '" AND rh.idrrhh != ' . $idusuario .
			' GROUP BY rh.idrrhh';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

}
