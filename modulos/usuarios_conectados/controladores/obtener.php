<?php

if (!defined('I_EXEC'))
	exit('Acceso no permitido');

$html = null;

$perfil = Usuario::getProfile();
$idusuario = Usuario::getIdUser();
$objModelo = new ModeloUsuariosConectados();


// parrafo antes de los conectados
$html .= "<div class='popupIntro'>
			<div class='subtitle t_center'><span data-translate-html='conectados.titulo'></span></div>
			<p class='t_justify' data-translate-html='conectados.descripcion'></p>
		</div>
		<br>";




// obtiene los alumnos y tutores que se han conectado
if (Usuario::compareProfile('alumno')) {
	$alumnosConectados = $objModelo->obtenerAlumnosConectadosParaAlumnos(Usuario::getIdUser(), Usuario::getIdCurso());
	$rrhhConectados = $objModelo->obtenerRRHHConectadosParaAlumnos(Usuario::getIdCurso());
	$idUser = Usuario::getIdUser();
} else {
	$alumnosConectados = $objModelo->obtenerAlumnosConectadosParaRRHH(Usuario::getIdUser());
	$rrhhConectados = $objModelo->obtenerRRHHConectadosParaRRHH(Usuario::getIdUser());
	$idUser = Usuario::getIdUser(true);
}

// el propio usuario
$nombrec = Usuario::getNameUser($idUser);
// contador de usuarios conectados
$numUsuarios = ($rrhhConectados->num_rows + $alumnosConectados->num_rows );


$html .= ''
	. '<li>'
	. '<img src="imagenes/mini_circle_green.png" alt="" />'
	. '	<span class="userDisable">' . Texto::textoPlano($nombrec) . ' '
	. '		<small data-translate-html="conectados.erestu">(este/a eres tú)</small>'
	. '	</span>'
	. '</li>';




// rrhh conectados
if ($rrhhConectados->num_rows > 0) {
	while ($usuarioConectado = $rrhhConectados->fetch_object()) {

		$html .= ''
			. '<li>'
			. '	<a href="#">'
//			. '	<a href="#" onclick="iniciarConversacion(\'t_' . $usuarioConectado->idrrhh . '\'); return false;" title="Enviar un mensaje emergente">'
			. '		<img src="imagenes/mini_circle_green.png" alt="" />'
			. '		<span>' . Texto::textoPlano($usuarioConectado->nombrec) . ' </span>'
			. '	</a>'
			. '<small>(' . ucfirst($usuarioConectado->perfil_mostrar) . ')</small>'
			. '</li>';
	}
}



// Alumnos conectados
if ($alumnosConectados->num_rows > 0) {
	while ($usuarioConectado = $alumnosConectados->fetch_object()) {
		$html .= ''
			. '<li>'
			. '	<a href="#">'
//			. '	<a href="#" onclick="iniciarConversacion(' . $usuarioConectado->idalumnos . '); return false;" title="Enviar un mensaje emergente">'
			. '		<img src="imagenes/mini_circle_green.png" alt="" />'
			. '		<span>' . Texto::textoPlano($usuarioConectado->nombrec) . ' </span>'
			. '	</a>'
			. '<small> - ' . $usuarioConectado->titulocurso . '</small>'
			. '</li>';
	}
}


if ($numUsuarios == 0) {
	$html .= '<br><br><p class="empty-chat" data-translate-html="conectados.noconectados"></p>';
}


echo json_encode(array('html' => $html, 'numUsuarios' => $numUsuarios));
