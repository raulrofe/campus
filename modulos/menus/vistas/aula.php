<div class='aula' id="aula">
	<div class='' style='position:absolute;left:62%;top:35%;font-size: 0.9em;'>
		<a class="boton_menu" href='#' onClick="popup_open('Tutor&iacute;a', 'tutorias', 'aula/tutorias', 550, 800); return false;"
		   data-translate-html="tutoria.tituloS">
			Tutor&iacute;a
		</a>
	</div>
	<div class='' style='position:absolute;left:25%;top:33%;font-size: 0.9em;width:150px;'>
		<a class="boton_menu" href='#'
		   onClick="popup_open('Tabl&oacute;n de anuncios', 'tablon_anuncio', 'aula/tablon_anuncio', 550, 800); return false;">
			<span data-translate-html="tablon_anuncios.titulo">
				Tabl&oacute;n de anuncios
			</span>
			<?php if ($_SESSION['perfil'] == 'alumno'): ?>
				<span class='nNew'>
					<?php if ($numero_anuncios > 0): ?>
						<span class='blink'>
						<?php endif; ?>
						<?php echo $numero_anuncios ?>
						<?php if ($numero_anuncios > 0): ?>
						</span>
					<?php endif; ?>
				</span>
			<?php endif ?>
		</a>
	</div>
	<div class='' style='position:absolute;left:70%;top:50%;font-size: 0.9em;width:200px;'>
		<a class="boton_menu" href='#' onClick="popup_open('Foros', 'foro', 'aula/foro', 550, 800); return false;">
			<span data-translate-html="foro.titulo">
				Foros
			</span>
			<?php if ($_SESSION['perfil'] == 'alumno'): ?>
				<span class='nNew'>
					<?php if ($numeroMensajesForoSinLeer > 0): ?>
						<span class='blink'>
						<?php endif; ?>
						<?php echo $numeroMensajesForoSinLeer ?>
						<?php if ($numeroMensajesForoSinLeer > 0): ?>
						</span>
					<?php endif; ?>
				</span>
			<?php endif; ?>
		</a>
	</div>
	<div class='' style='position:absolute;left:15%;top:50%;font-size: 0.9em;text-align:center;'>
		<a class="boton_menu" href='#' onClick="popup_open('Contenidos', 'archivador', 'aula/archivador/grafica', 550, 800); return false;"
		   data-translate-html="espacios.contenidos">
			Contenidos
		</a>
	</div>
	<?php if (Usuario::getIdCurso() == 15): ?>
		<div class='' style='position:absolute;left:35%;top:50%;font-size: 0.9em;text-align:center;'>
			<a class="boton_menu" href='#' onClick="popup_open('Grupos de trabajo', 'grupo_trabajo', 'aula/grupo-trabajo', 550, 800); return false;"
			   data-translate-html="espacios.grupos">
				Grupos de trabajo
			</a>
		</div>
	<?php endif; ?>
	<div class='' style='position:absolute;left:70%;top:60%;width: 150px;font-size: 0.9em;'>
		<a class="boton_menu" href='#'
		   onClick="popup_open('Trabajos pr&aacute;cticos / Ejercicios de evaluaci&oacute;n', 'trabajos_practicos', 'aula/trabajos_practicos/grafica', 550, 800); return false;"
		   data-translate-html="trabajos.titulo">
			Trabajos pr&aacute;cticos / Ejercicios de evaluaci&oacute;n
		</a>
	</div>
	<div class='' style='position:absolute;left:85%;top:80%;font-size: 0.9em;width:200px;'>
		<a class="boton_menu" href='#'
		   onClick="popup_open('Archivador electr&oacute;nico', 'archivador_electronico', 'aula/archivador_electronico', 550, 800); return false;"
		   data-translate-html="archivador.titulo">
			Archivador electr&oacute;nico
		</a>
	</div>
	<div class='' style='position:absolute;left:15%;top:76%;font-size: 0.9em;'>
		<a class="boton_menu" href='#'
		   onClick="popup_open('Test / Ejercicios de autoevaluaci&oacute;n', 'autoevaluaciones', 'aula/autoevaluaciones/grafica', 550, 800); return false;"
		   data-translate-html="autoevaluaciones.titulo">
			Test / Ejercicios de autoevaluaci&oacute;n
		</a>
	</div>
</div>

<script type="text/javascript" language="javascript">

	$(document).ready(function ()
	{
		$('.blink').blink({delay: 500}); // causes a 100ms blink interval.
	});

</script>