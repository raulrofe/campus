<div class='cabeceralms'>
	<div class='fleft' style="margin-top: 5px;margin-left: 5px;">
		<a href='#/hall/<?php echo Usuario::getIdCurso() ?>' onclick="cambiarHash('hall/<?php echo Usuario::getIdCurso() ?>'); return false;">
			<img src='archivos/config_web/<?php echo $logotipo['imagen_logo']; ?>' style='margin-right:5px;' />
		</a>
	</div>
	<div class='datos_tutor' style="line-height: 19px">
		<span style="font-size: 0.9em;"><?php echo $usuario; ?></span>
		<br/>
		<span id="principalNombreCurso" style="font-size: 0.8em;">
			<?php if (isset($dato_curso)) echo Texto::textoPlano($dato_curso['titulo']); ?>
		</span>
		<br/>
		<span>		
			<a href="archivos/guia-de-usuario.pdf" target="_blank" data-translate-href="guiausuario.enlace">Guía de usuario</a>
		</span>
	</div>
</div>


<div id="menu_ajax"><?php require_once $rutaVista; ?></div>

<div id="menuCurso">
	<ul class="fleft">
		<li>
			<a id="menuCurso_home" href="#" onclick="return false;" data-translate-html="menu.ir_a">
				Ir a
			</a>
			<ul id="accesRapidoMenu" class="accesRapidoMenu">
				<li>
					<a href="#/hall/<?php echo Usuario::getIdCurso() ?>" onclick="cambiarHash('hall/<?php echo Usuario::getIdCurso() ?>'); return false;"
					   data-translate-html="espacios.hall">
						Hall
					</a>
				</li>

				<li>
					<a href="cursos/#/aula" onclick="cambiarHash('aula'); return false;"
					   data-translate-html="espacios.aula">
						Aula
					</a>
				</li>

				<li>
					<a href="cursos/#/zona-tutor" onclick="cambiarHash('zona-tutor'); return false;"
					   data-translate-html="zonatutor.titulosimple">
						Zona entrega al tutor
					</a>
				</li>

				<li>
					<a href="cursos/#/cafeteria" onclick="cambiarHash('cafeteria'); return false;"
					   data-translate-html="espacios.cafeteria">
						Cafeter&iacute;a
					</a>
				</li>

				<li>
					<a href="cursos/#/biblioteca" onclick="cambiarHash('biblioteca'); return false;"
					   data-translate-html="espacios.biblioteca">
						Biblioteca
					</a>
				</li>

				<li>
					<a href="tablon-de-anuncios" onclick="popup_open('Tabl&oacute;n de anuncios', 'anuncios', 'tablon_anuncios', 600, 600); return false;"
					   data-translate-html="tablon_anuncios.titulo">
						Tabl&oacute;n de anuncios
					</a>
				</li>

				<li style="height: auto;"><hr /></li>

				<?php if (!Usuario::compareProfile('tutor')): ?>
					<li>
						<a href='#' onClick="popup_open('Secretar&iacute;a', 'secretaria', 'secretaria', 400, 500); return false;"
						   data-translate-html="secretaria.titulo">
							Secretar&iacute;a
						</a>
					</li>
				<?php endif; ?>

				<li>
					<a href='#' onclick="popup_open('Correo', 'gestor_correos', 'correos/bandeja_entrada', 750, 900); return false;"
					   data-translate-html="correo.titulo">
						Correo
					</a>
				</li>

				<li style="height: auto;"><hr /></li>

				<li>
					<a href='' onclick="cambiarHash(''); return false;"
					   data-translate-html="menu.mis_cursos">
						Mis cursos
					</a>
				</li>
			</ul>
		</li>

		<?php if (Usuario::compareProfile(array('tutor', 'coordinador', 'supervisor'))): ?>
			<li>
				<a href="#" onclick="return false;" data-translate-html="menu.gestion">
					Gesti&oacute;n
				</a>
				<ul class="accesRapidoMenu">
					<?php if (!Usuario::compareProfile('supervisor')): ?>
						<li>
							<a href="#" onClick="popup_open('Tareas pendientes', 'tareas_tutor', 'tareas-tutor', 600, 630); return false;"
							   data-translate-html="tareas_pendiente.titulo">
								Tareas pendientes
							</a>
						</li>
					<?php endif; ?>
					<li>
						<a href='#' onClick="popup_open('Seguimiento', 'seguimiento_alumnos', 'seguimiento', 650, 800); return false;"
						   data-translate-html="menu.seguimiento">
							Seguimiento
						</a>
					</li>

					<?php if (!Usuario::compareProfile('supervisor')): ?>
						<?php if (mvc::accesoServicio('trabajos_practicos')): ?>
							<li><a href='#' onClick="popup_open('Trabajos pr&aacute;cticos', 'gestion_trabajos_practicos', 'gestion_trabajos', 650, 800); return false;"
								   data-translate-html="menu.practicos">
									Trabajos pr&aacute;cticos
								</a>
							</li>
						<?php endif; ?>				

						<?php if (mvc::accesoServicio('autoevaluacion')): ?>
							<li><a href='#' onClick="popup_open('Autoevaluaciones', 'gestion_autoevaluaciones', 'autoevaluaciones', 550, 800); return false;"
								   data-translate-html="menu.autoevaluaciones">
									Autoevaluaciones
								</a>
							</li>
						<?php endif; ?>
					<?php endif; ?>

					<li><a href='#' onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones', 600, 800); return false;"
						   data-translate-html="menu.evaluaciones">
							Evaluaciones
						</a>
					</li>				
					<!-- <li><a href='#' onClick="popup_open('mensajes_emergentes', 'mensajes-emergentes/listar', 550, 600); return false;">Mensajes emergentes</a></li> -->
					<li><a href="#" onClick="popup_open('Integrantes del curso', 'integrantes_del_curso', 'integrantes-del-curso', 600, 630); return false;"
						   data-translate-html="menu.integrantes">
							Integrantes del curso
						</a>
					</li>
				</ul>
			</li>
		<?php else: ?>
			<li>
				<a href="#" onclick="return false;" data-translate-html="menu.curso">
					El curso
				</a>

				<ul class="accesRapidoMenu">
					<li>
						<a href='#' onClick="popup_open('Tareas pendientes', 'tareas_pendientes', 'tareas-pendientes', 400, 600); return false;"
						   data-translate-html="tareas_pendiente.titulo">
							Tareas pendientes
						</a>
					</li>

					<li>
						<a href='#' onClick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;"
						   data-translate-html="agenda.titulo">
							Agenda
						</a>
					</li>

					<li>
						<a href="#" onClick="popup_open('Compa&ntilde;eros', 'integrantes_del_curso', 'integrantes-del-curso', 600, 630); return false;"
						   data-translate-html="integrantes.compis">
							Compa&ntilde;eros
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#" onclick="return false;" data-translate-html="menu.progresos">
					Progresos
				</a>
				<ul class="accesRapidoMenu" style='padding: 10px;'>
					<li>
						<a href='#' onClick="popup_open('Mis progresos', 'mis_progresos', 'mis-progresos', 650, 800); return false;"
						   data-translate-html="menu.misprogresos">
							Mis progresos
						</a>
					</li>

					<li>
						<a href='#' onClick="popup_open('Cofinanciación', 'cofinanciacion', 'cofinanciacion', 650, 800); return false;"
						    data-translate-html="cofinanciacion.titulo">
							Cofinanciacion
						</a>						
					</li>

				</ul>
			</li>			
		<?php endif; ?>

		<li>
			<a href='#' onClick="popup_open('Agenda', 'agenda', 'agenda', 580, 840); return false;"
			   data-translate-html="menu.agenda">
				Agenda
			</a>
		</li>

		<li>
			<a href="#" onclick="return false;" data-translate-html="menu.personal">
				Espacio personal
			</a>
			<ul class="accesRapidoMenu" style="padding:10px;">
				<?php if (!Usuario::compareProfile('supervisor')): ?>
					<li>
						<a href="#" onclick="popup_open('Mensajes emergentes', 'Mensajes_emergentes', 'mensajes-emergentes/recibidos', 600, 600); return false;" title="Mensajes emergentes"
						   data-translate-html="emergentes.titulo">
							Mensajes emergentes
						</a>
					</li>

					<li>
						<a href='#' onClick="popup_open('Avisos personales', 'avisos_personales', 'avisos-personales/nuevo', 600, 750); return false;"
						   data-translate-html="personales.titulo">
							Avisos personales
						</a>
					</li>
					<li>
						<a href='#' onClick="popup_open('Bloc de notas', 'bloc_de_notas', 'bloc-de-notas', 550, 600); return false;"
						   data-translate-html="notas.titulo">
							Bloc de notas
						</a>
					</li>
				<?php endif; ?>
				<li>
					<a href='#' onclick="popup_open('Datos personales', 'datos_personales', 'datos-personales', 600, 800); return false;"
					   data-translate-html="usuario.tab1">
						Datos personales
					</a>
				</li>
			</ul>
		</li>

		<li>
			<a href="#" onclick="return false;" data-translate-html="menu.ayuda">
				Ayuda
			</a>
			<ul class="accesRapidoMenu">
				<li>
					<a href='#' onClick="popup_open('Descargas', 'descargas', 'ayuda/descargas', 600, 600); return false;"
					   data-translate-html="menu.descargas">
						Descargas
					</a>
				</li>
				<li>
					<a href='#' onClick="popup_open('FAQs', 'faqs', 'ayuda/faqs', 570, 610); return false;"
					   data-translate-html="menu.faqs">
						FAQs
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<ul class="fright">
		<li>
			<a href="#" onclick="return false;">
				<img data-flag-language="1" src="imagenes/banderas/es.png"/>
				<span data-translate-html="menu.idiomas">
					Idiomas
				</span>
			</a>
			<!--https://emojiterra.com/es/banderas/-->
			<ul class="accesRapidoMenu language" style='padding: 10px;'>
				<li data-change-translate="es" onclick="changeLanguage('es')"> 
					<a>
						<img src="imagenes/banderas/es.png"/>
						<span data-translate-html="menu.espanol" >
							Español
						</span>
					</a>
				</li>
				<li data-change-translate="ch" onclick="changeLanguage('ch')">
					<a>
						<img src="imagenes/banderas/ch.png"/>
						<span data-translate-html="menu.chino">
							Chino
						</span>
					</a>
				</li>
				<li data-change-translate="en" onclick="changeLanguage('en')">
					<a>
						<img src="imagenes/banderas/en.png"/>
						<span data-translate-html="menu.ingles">
							Inglés
						</span>
					</a>
				</li>
			</ul>
		</li>		
		<li>
			<a id="menuCurso_conversaciones" href='conversaciones' onclick="popup_open('Chat', 'conversaciones', 'conversaciones', 600, 800); return false;">
				<img src="imagenes/menu/conversaciones.png"/>
				<span data-translate-html="conversaciones.chat">
					Chat
				</span>
			</a>
			<span class="Sinleer hide" title="Mensajes sin leer">
			</span>
		</li>
		<li>
			<a id="menuCurso_conectados" href='conectados' onclick="popup_open('Conectados', 'usuarios_conectados', 'conectados', 400, 600); return false;"
			   data-translate-html="menu.conectados">
				Conectados
			</a>
			<span class="NumeroConectados" title="Número de alumnos conectados"  data-translate-title="menu.nconectados">
			</span>
		</li>
		<li>
			<a id="menuCurso_salir" href='salir' data-translate-html="menu.salir">
				Salir
			</a>
		</li>
	</ul>

	<div class="clear"></div>
</div>

<?php if (!empty($logotipo['imagen_fondo_publico'])): ?>
	<div id="fondoWebPublico">
		<img src="archivos/config_web/<?php echo $logotipo['imagen_fondo_publico']; ?>" alt="" />
	</div>
<?php endif; ?>

<script type="text/javascript">
	if ($('#panelAuxCursos').size() == 1) {

		var options = {to: {width: 10}};

		$('#panelAuxCursos').mouseenter(function () {
			$(this).animate({'margin-left': '0'}, 100);
		}).mouseleave(function () {
			$(this).animate({'margin-left': '-220px'}, 100);
		});
	}
</script>