<?php 
	//cargo el archivo para saber el numero de mensajes sin leer de opiniones
	require_once(PATH_ROOT.'modulos/opiniones/controladores/opiniones_sinleer.php');
?>
<div id="cafeteria" style="font-size: 0.9em;">
	<a id="cafeteria_chat" class="boton_menu" href="#"
		onclick="popup_open('La tertulia', 'chat', 'chat', 560, 610); return false;"
		 data-translate-html="tertulia.titulo">
		La tertulia
	</a>
	<a id="cafeteria_foro_opiniones" class="boton_menu" href="#"
		onclick="popup_open('Publicar opiniones', 'foro_opiniones', 'opiniones', 485, 610); return false;"
		 data-translate-html="opiniones.titulo">
		Publicar opiniones
		<?php if(Usuario::compareProfile('alumno')):?>
			<span class='nNew'>
				<?php if($mensajesOpinionesSinLeer > 0):?>
					<span class='blink'>
				<?php endif; ?>
					<?php echo $mensajesOpinionesSinLeer; ?>
				<?php if($mensajesOpinionesSinLeer > 0):?>
					</span>
				<?php endif ; ?>
			</span>
		<?php endif ?>		
	</a>
	<a id="cafeteria_revista" class="boton_menu" href="#" 
		onclick="popup_open('Publicaciones educativas', 'publicaciones_educativas', 'publicaciones_educativas', 485, 610); return false;"
		 data-translate-html="publicaciones.titulo">
		Publicaciones educativas
	</a>
</div>
 
<script type="text/javascript" language="javascript">

$(document).ready(function()
{
        $('.blink').blink({delay:500}); // causes a 100ms blink interval.
});
 
</script>