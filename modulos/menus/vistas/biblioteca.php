<div id="biblioteca" style="font-size: 0.9em;">
	<a id="biblioteca_nuevo" class="boton_menu" href="#"
		onClick="popup_open('Sugerir aportaci&oacute;n', 'bliblioteca_nuevo', 'biblioteca/nuevo/fichero', 550, 600); return false;"
		 data-translate-html="biblioteca.sugerir">
			Sugerir aportaci&oacute;n</a>
	<a id="biblioteca_buscar" class="boton_menu" href='#'
		onClick="popup_open('Buscar en el cat&aacute;logo', 'biblioteca_buscar', 'biblioteca/buscar', 570, 610); return false;"
		data-translate-html="biblioteca.titulo">
			Buscar en el cat&aacute;logo</a>
	
	<?php if(Usuario::compareProfile(array('tutor', 'coordinador'))):?>
		<a id="biblioteca_admin" class="boton_menu" href='#'
			onClick="popup_open('Administrar aportaciones', 'biblioteca_admin', 'biblioteca/admin', 570, 610); return false;"
			data-translate-html="biblioteca.administrar">
				Administrar aportaciones</a>
	<?php endif;?>
</div>