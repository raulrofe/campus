<div id="hall" style="font-size: 0.9em;">
	<a id="hall_aula" class="boton_menu" href="cursos/#/aula" onclick="cambiarHash('aula'); return false;"
	    data-translate-html="espacios.aula">
		Aula
	</a>

	<a id="hall_zona_tutor" class="boton_menu" href="cursos/#/zona-tutor" onclick="cambiarHash('zona-tutor'); return false;"
	    data-translate-html="zonatutor.titulosimple">
		Zona entrega al tutor
	</a>

	<a id="hall_cafeteria" class="boton_menu" href="cursos/#/cafeteria" onclick="cambiarHash('cafeteria'); return false;"
	    data-translate-html="espacios.cafeteria">
		Cafeteria
	</a>
	<a id="hall_biblioteca" class="boton_menu" href="cursos/#/biblioteca" onclick="cambiarHash('biblioteca'); return false;"
	    data-translate-html="espacios.biblioteca">
		Biblioteca
	</a>
	<a id="hall_secretaria" class="boton_menu" href="#" onclick="popup_open('Secretar&iacute;a', 'secretaria', 'secretaria', 400, 500); return false;"
	    data-translate-html="secretaria.titulo">
		Secretaria
	</a>
	<a id="hall_correo" class="boton_menu" href='#' onclick="popup_open('Correo', 'gestor_correos', 'correos/bandeja_entrada', 800, 900); return false;"
	    data-translate-html="correo.titulo">
		Correo
	</a>
	
	<a id="hall_tablon_anuncios" class="boton_menu" href='#' onclick="popup_open('Tabl&oacute;n p&uacute;blico', 'anuncios', 'tablon_anuncios', 550, 800); return false;">
		<span data-translate-html="tablon_publico.titulo">
			Tabl&oacute;n p&uacute;blico 
		</span>
			<?php if($_SESSION['perfil'] == 'alumno') echo "<span class='nNew'>".$numero_anuncios_hall."</span>"; ?>
	</a>
	
	<?php if($tutoriaPriv->num_rows > 0):?>
		<?php while($item = $tutoriaPriv->fetch_object()):?>
			<div id="hall_chat_tutoria_priv">
				<a href='#' onClick="popup_open('Tutor&iacute;a privada online', 'chat_tutoria_privada', 'aula/tutorias/privada/chat/listado', 430, 700); return false;" class="boton_menu"
				    data-translate-html="espacios.tutoriaprivada">
					Accede a la<br />tutor&iacute;a privada
				</a>
			</div>
		<?php endwhile;?>
	<?php endif;?>
	
	<?php if($tutoria->num_rows > 0):?>
		<div id="hall_chat_tutoria">
			<a href='#' onClick="popup_open('Tutor&iacute;a online', 'chat_tutoria', 'aula/tutorias/chat', 430, 700); return false;" class="boton_menu"
			    data-translate-html="espacios.tutoria">
				Accede a la<br />tutor&iacute;a
			</a>
		</div>
	<?php endif;?>
	
	<?php if(Usuario::compareProfile('alumno') && !$_SESSION['inicioTareas']):?>
		<script type="text/javascript">
			popup_open('Tareas pendientes', 'tareas_pendientes', 'tareas-pendientes', 400, 600);
		</script>
		<?php $_SESSION['inicioTareas'] = true;?>
	<?php endif; ?>
	
	<?php if(Usuario::compareProfile(array('tutor', 'coordinador')) && !$_SESSION['inicioTareas']):?>
		<script type="text/javascript">
			popup_open('Tareas pendientes', 'tareas_pendientes_tutor', 'tareas-tutor', 400, 600);
		</script>
		<?php $_SESSION['inicioTareas'] = true;?>
	<?php endif; ?>
	
</div>