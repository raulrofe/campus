<?php

//Conexion a base de datos
$db = new database();
$con = $db->conectar();

$get = Peticion::obtenerGet();
//$post = Peticion::obtenerPost();
// para que siempre exista la "v"
if (!isset($get['v']) || (isset($get['v']) && is_dir($get['v']))) {
	Url::redirect('');
}

//establecemos el id del curso
if ($get['v'] == 'hall' && isset($get['idcurso']) && is_numeric($get['idcurso'])/* && Peticion::isPost() */) {
	$mi_curso = new Cursos($con);
	$mi_curso->set_curso($get['idcurso']);

	if (Usuario::getIdUser() == 64) {
		$registro_curso = $mi_curso->buscar_curso();
	} else {
		$registro_curso = $mi_curso->buscar_curso_para_un_usuario(Usuario::getIdUser(true), true);
	}

	if ($registro_curso->num_rows == 1) {
		// pasamos el puntero al curso para poder obtener los datos
		$dato_curso = mysqli_fetch_assoc($registro_curso);
		$_SESSION['idcurso'] = $get['idcurso'];
		$_SESSION['nombrecurso'] = $dato_curso['titulo'];
		$_SESSION['idconvocatoria'] = $dato_curso['idconvocatoria'];

		if (Usuario::compareProfile('alumno')) {

			$_SESSION['usuario_fecha_acceso_curso'] = $dato_curso['fecha_actual_al'];

		} else {
			
			$_SESSION['usuario_fecha_acceso_curso'] = $dato_curso['fecha_actual_tutor'];
		}
	} else {
		Url::redirect('');
	}
} else if (isset($get['v'])) {
	$idcurso = Usuario::getIdCurso();
	if (isset($idcurso)) {
		$mi_curso = new Cursos($con);
		$mi_curso->set_curso($idcurso);
		$registro_curso = $mi_curso->buscar_curso_para_un_usuario(Usuario::getIdUser(true));
		if ($registro_curso->num_rows == 1) {
			// pasamos el puntero al curso para poder obtener los datos
			$dato_curso = mysqli_fetch_assoc($registro_curso);
		}
	}
}

if (!isset($_SESSION['idcurso'])) {
	Url::redirect('');
}

//Obtenemos el logo para mostrarlo
$mi_aula = new Aula($con);
$logotipo = $mi_aula->custom_logo();

// id del perfil de usuasrio logueado
$idperfil = Usuario::obtenerIdPerfilLogueo();

// Obtenemos los datos del usuario
$mi_usuario = new usuarios($idperfil, $con);
$mi_usuario->set_usuario($_SESSION['idusuario']);
$registros_usuario = $mi_usuario->buscar_usuario();
$datos_usuario = mysqli_fetch_assoc($registros_usuario);

if ($_SESSION['perfil'] != 'alumno') {
	$usuario = ucwords($datos_usuario['nombrec']);
} else {
	$usuario = ucwords($datos_usuario['nombre']) . " " . ucwords($datos_usuario['apellidos']);
	// actualiza la realizacion de la encuesta
	Usuario::actualizarEncuestaRealizada($_SESSION['idusuario']);
}

// --------- para saber si el usuario tiene activo el chat de tutoria
$ct_idrrhh = null;
$ct_idcurso = null;

mvc::cargarModuloSoporte('chat_tutoria');
$objModeloChatTutoria = new ModeloChatTutoria();
if (Usuario::compareProfile('alumno')) {
	$ct_idcurso = Usuario::getIdCurso();
} else {
	$ct_idrrhh = Usuario::getIdUser(true);
}

$tutoria = $objModeloChatTutoria->obtenerTutoria($ct_idrrhh, $ct_idcurso);

// -----------------------------------

mvc::cargarModuloSoporte('chat_tutoria_priv');
$objModeloChatTutoriaPriv = new ModeloChatTutoriaPrivada();
$tutoriaPriv = $objModeloChatTutoriaPriv->obtenerTutoriasPrivadas();

// -----------------------------------

$rutaVista = mvc::obtenerRutaVista(dirname(__FILE__), $get['v']);
if (is_file($rutaVista) && !is_dir($get['v'])) {


	if ($get['v'] == 'aula' OR $get['v'] == 'hall' and $_SESSION['perfil'] == 'alumno') {
		//cargo el archivo para mostrar el numero de anuncios sin leer del aula
		require_once(PATH_ROOT . 'modulos/tablon_anuncio/controladores/anuncios_sinleer.php');
		//cargo el archivo para mostrar el numero de anuncios sin leer del hall
		require_once(PATH_ROOT . 'modulos/tablon_anuncios/controladores/anuncios_sinleer.php');
		//cargo el archivo para saber el numero de mensajes del foro sin leer
		require_once(PATH_ROOT . 'modulos/foro/controladores/mensajes_sinleer.php');
	}

	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'html');
} else {
	Url::lanzar404();
}
