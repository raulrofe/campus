<?php
mvc::cargarModuloSoporte('notas');
$objModeloNotas = new ModeloNotas();

mvc::cargarModuloSoporte('foro');
$objModeloForo = new modeloForo();

mvc::cargarModuloSoporte('tareas_pendientes');
$objModelo = new ModeloTareasPendientes();

$mi_curso = new Cursos();

$idCurso = Usuario::getIdCurso();
$objModeloSeguimiento = new ModeloSeguimiento();


$page = 1;
$contPage = 1;
$asuntoAux = '';

$get = Peticion::obtenerGet();

//Configuraciones
$configNotas = $objModeloNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

/*if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['idmatricula']) && is_numeric($post['idmatricula']))
	{
		$resultAlumno = $objModeloSeguimiento->obtenerAlumnos($idCurso, $post['idmatricula']);
		if($resultAlumno->num_rows == 1)
		{
			$rowAlumno = $resultAlumno->fetch_object();
		}
	}
}*/


if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();


	if(isset($post['idalumno']) && is_numeric($post['idalumno']))
	{
		$resultAlumno = $objModeloSeguimiento->obtenerAlumnos($idCurso, $post['idalumno']);
		if($resultAlumno->num_rows == 1)
		{
			$rowAlumno = $resultAlumno->fetch_object();


			// comprueba que los datos del POST sean validos
			if(isset($post['hora'], $post['minutos'], $post['fecha'], $post['mensaje'], $post['asunto'], $post['telefono'], $post['telefono2'], $post['llamada_a'])
				&& is_numeric($post['hora']) && is_numeric($post['minutos'])
				&& ($post['llamada_a'] == 'alumno' || $post['llamada_a'] == 'centro')
				&& (empty($post['telefono']) || (!empty($post['telefono']) && is_numeric($post['telefono'])))
				&& (empty($post['telefono2']) || (!empty($post['telefono2']) && is_numeric($post['telefono2']))))
			{
				// a las llamadas no les obliga a cunmplimentar el campo nota, pero a las anotacione si
				if(!empty($post['mensaje']) && !empty($post['asunto']))
				{
					// comprueba que la fecha y hora sea valida
					$fechaArray = explode('/', $post['fecha']);
					if(checkdate($fechaArray[1], $fechaArray[0], $fechaArray[2]) && ($post['minutos'] >= 0 && $post['minutos'] < 60)
						&& ($post['hora'] > 0 && $post['hora'] < 24))
					{
						// construimos el campo de fecha y hora para insertarlo en la BBDD
						$fechaHora = $fechaArray[2] . '-' . $fechaArray[1]. '-' . $fechaArray[0] . ' ' . $post['hora'] . ':' . $post['minutos'] . ':00';

						// insertamos la anotaciopn y mostramos un mensaje de alerta u otro dependeiendo del tipo de anotacion
						if($objModeloSeguimiento->insertarAnotacion(
							$rowAlumno->idmatricula, $fechaHora, $post['mensaje'], $post['asunto'],
							$post['telefono'], $post['telefono2'], $post['llamada_a']))
						{
							// guarda el telefono en tabla alumnos
							if(!empty($post['telefono']))
							{
								$objModeloSeguimiento->actualizarTlfnAlumno($post['idalumno'], $post['telefono']);
							}

							Alerta::mostrarMensajeInfo('guardadoanotaciones','Se ha guardado la anotaci&oacute;n');
						}
					}
				}
				else
				{
					Alerta::mostrarMensajeInfo('debescampos','Debes rellenar los campos');
				}
			}
		}
		// END REQUEST POST

		/*$anotaciones = $objModeloSeguimiento->obtenerAnotaciones($rowAlumno->idmatricula);

		$fechaActual = date('d/m/Y');
		$horaActual = date('H');
		$minutoActual = date('i');*/

		//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'llamadas');

	}
}

//$modulosCurso = $objModeloNotas->obtenerModulosPorCurso();

//mvc::cargarModeloAdicional('seguimiento', 'modelo');

//$objModeloSeguimiento = new Autoevaluacion();

$dataSeguimiento = array();

$seguimientoEmails = $objModeloSeguimiento->obtenerSeguimientoEmail($idCurso);
$seguimientoEmailsPersonal = $objModeloSeguimiento->obtenerSeguimientoEmailPersonal($idCurso);

//$alumnos = $objModeloSeguimiento->obtenerAlumnos($idCurso);

/* ------------ */
$alumnosCount = $objModeloSeguimiento->obtenerAlumnos($idCurso);
$maxElementsPaging = 15;
$maxPaging = $alumnosCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);
$elementsIni = ($numPagina - 1) * $maxElementsPaging;
/* ------------ */

$alumnos = $objModeloSeguimiento->obtenerAlumnos($idCurso);

$alumnos2 = $objModeloSeguimiento->obtenerAlumnos($idCurso);

$anotaciones = $objModeloSeguimiento->obtenerAnotacionesCurso($idCurso);


// obtenemos los asuntos de las anotaciones para el autocompletado
$arrayAnotacionesAsuntos = array();
$anotacionesAsuntos = $objModeloSeguimiento->obtenerAsuntos();
while($row = $anotacionesAsuntos->fetch_object())
{
	$arrayAnotacionesAsuntos[] = $row->asunto;
}
$arrayAnotacionesAsuntos = array_unique($arrayAnotacionesAsuntos);

//Cuento el numero de trabajos practicos por curso
$resultTrabPract = $objModeloNotas->obtenerTrabjosPracticosCurso(Usuario::getIdCurso());
$resultTrabPract = $resultTrabPract->fetch_object();
$nTrabPract = $resultTrabPract->nArchivosTemario;

//Cuento el numero de autoevaluaciones por curso
$resultTest = $objModeloNotas->obtenerTestCurso(Usuario::getIdCurso());
$resultTest = $resultTest->fetch_object();
$nTest = $resultTest->nTestCurso;

if($configNotas->extra1 > 0){	

 	$tareasObligatorias = 4 + $nTrabPract + $nTest;
	
	if($idCurso == 536 || $idCurso == 568 || $idCurso == 594 || $idCurso == 633 || $idCurso == 681 ||  $idCurso == 718 || $idCurso == 759 || $idCurso == 783) {

		$tareasObligatorias = 2 + $nTrabPract + $nTest;

	}

} else {
	$tareasObligatorias = $nTrabPract + $nTest;
}
//$tareasMinimas = ceil($tareasObligatorias * 0.75);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');