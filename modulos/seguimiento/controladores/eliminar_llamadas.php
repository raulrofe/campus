<?php
$idCurso = Usuario::getIdCurso();
$objModeloSeguimiento = new ModeloSeguimiento();

$get = Peticion::obtenerGet();

if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	$resultAlumno = $objModeloSeguimiento->obtenerAlumnos($idCurso, $get['idalumno']);
	if($resultAlumno->num_rows == 1)
	{
		$rowAlumno = $resultAlumno->fetch_object();
		
		$rowAnotacion = $objModeloSeguimiento->obtenerAnotaciones($rowAlumno->idmatricula, $get['idnota']);
		if($rowAnotacion->num_rows == 1)
		{
			$rowAnotacion = $rowAnotacion->fetch_object();
			
			if($objModeloSeguimiento->eliminarAnotacion($rowAnotacion->idseguimiento_llamada))
			{
				Alerta::guardarMensajeInfo('anotacionborrada','Se ha borrado la anotación');
			}
		}
	}
}

Url::redirect('seguimiento/llamadas', true);