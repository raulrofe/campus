<?php
exit;
$idCurso = Usuario::getIdCurso();
$objModeloSeguimiento = new ModeloSeguimiento();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	
	if(isset($post['idalumno']) && is_numeric($post['idalumno']))
	{
		$resultAlumno = $objModeloSeguimiento->obtenerAlumnos($idCurso, $post['idalumno']);
		if($resultAlumno->num_rows == 1)
		{
			$rowAlumno = $resultAlumno->fetch_object();
		
			// comprueba que los datos del POST sean validos
			if(isset($post['hora'], $post['minutos'], $post['fecha'], $post['mensaje'], $post['tipo'])
				&& is_numeric($post['hora']) && is_numeric($post['minutos']) && is_numeric($post['tipo'])
					&& ($post['tipo'] == 1 || $post['tipo'] == 2))
			{
				// a las llamadas no les obliga a cunmplimentar el campo nota, pero a las anotacione si
				if($post['tipo'] == 1 || ($post['tipo'] == 2 && !empty($post['mensaje'])))
				{
					// comprueba que la fecha y hora sea valida
					$fechaArray = explode('/', $post['fecha']);
					if(checkdate($fechaArray[1], $fechaArray[0], $fechaArray[2]) && ($post['minutos'] >= 0 && $post['minutos'] < 60)
						&& ($post['hora'] > 0 && $post['hora'] < 24))
					{
						// construimos el campo de fecha y hora para insertarlo en la BBDD
						$fechaHora = $fechaArray[2] . '-' . $fechaArray[1]. '-' . $fechaArray[0] . ' ' . $post['hora'] . ':' . $post['minutos'] . ':00';
					
						// insertamos la anotaciopn y mostramos un menasje de alerta u otro dependeiendo del tipo de anotacion
						if($objModeloSeguimiento->insertarAnotacion($rowAlumno->idmatricula, $fechaHora, $post['mensaje'], $post['tipo']))
						{
							if($post['tipo'] == 1)
							{
								Alerta::mostrarMensajeInfo('registroguardado','Se ha guardado el registro de llamada');
							}
							else
							{
								Alerta::mostrarMensajeInfo('guardadoanotaciones','Se ha guardado la anotaci&oacute;n');
							}
						}
					}
				}
				else
				{
					Alerta::mostrarMensajeInfo('debescampos','Debes rellenar los campos');
				}
			}	
		}
		// END REQUEST POST
		
		$anotaciones = $objModeloSeguimiento->obtenerAnotaciones($rowAlumno->idmatricula);
		
		$fechaActual = date('d/m/Y');
		$horaActual = date('H');
		$minutoActual = date('i');
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'llamadas');
	}
}