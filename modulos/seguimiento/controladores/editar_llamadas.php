<?php
$idCurso = Usuario::getIdCurso();
$objModeloSeguimiento = new ModeloSeguimiento();

$get = Peticion::obtenerGet();
if(isset($get['idalumno'], $get['idnota']) && is_numeric($get['idalumno']) && is_numeric($get['idnota']))
{
	// obtenemos al alumno
	$resultAlumno = $objModeloSeguimiento->obtenerAlumnos($idCurso, $get['idalumno']);
	if($resultAlumno->num_rows == 1)
	{
		$rowAlumno = $resultAlumno->fetch_object();
		
		// obtebemos la anotaciion
		$rowAnotacion = $objModeloSeguimiento->obtenerAnotaciones($rowAlumno->idmatricula, $get['idnota']);
		if($rowAnotacion->num_rows == 1)
		{
			$rowAnotacion = $rowAnotacion->fetch_object();
			
			// START REQUEST POST
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();
				
				// comprueba que los datos del POST sean validos
				if(isset($post['hora'], $post['minutos'], $post['fecha'], $post['mensaje'], $post['asunto'], $post['telefono'], $post['telefono2'], $post['llamada_a'])
					&& is_numeric($post['hora']) && is_numeric($post['minutos'])
					&& ($post['llamada_a'] == 'alumno' || $post['llamada_a'] == 'centro')
					&& (empty($post['telefono']) || (!empty($post['telefono']) && is_numeric($post['telefono'])))
					&& (empty($post['telefono2']) || (!empty($post['telefono2']) && is_numeric($post['telefono2']))))
				{
					// a las llamadas no les obliga a cunmplimentar el campo nota, pero a las anotacione si
					if(!empty($post['asunto']) && !empty($post['mensaje']))
					{
						// comprueba que la fecha y hora sea valida
						$fechaArray = explode('/', $post['fecha']);
						if(checkdate($fechaArray[1], $fechaArray[0], $fechaArray[2]) && ($post['minutos'] >= 0 && $post['minutos'] < 60)
							&& ($post['hora'] > 0 && $post['hora'] < 24))
						{
							// construimos el campo de fecha y hora para insertarlo en la BBDD
							$fechaHora = $fechaArray[2] . '-' . $fechaArray[1]. '-' . $fechaArray[0] . ' ' . $post['hora'] . ':' . $post['minutos'] . ':00';
						
							// insertamos la anotaciopn y mostramos un menasje de alerta u otro dependeiendo del tipo de anotacion
							if($objModeloSeguimiento->actualizarAnotacion(
								$rowAnotacion->idseguimiento_llamada, $rowAlumno->idmatricula, $fechaHora, $post['mensaje'], $post['asunto'],
								$post['telefono'], $post['telefono2'], $post['llamada_a']))
							{
								Alerta::mostrarMensajeInfo('registroactualizado','Se ha actualizado el registro de llamada');
								
								// obtenemos de nuevo la anotacion con los nuevos datos
								$rowAnotacion = $objModeloSeguimiento->obtenerAnotaciones($rowAlumno->idmatricula, $get['idnota']);
								$rowAnotacion = $rowAnotacion->fetch_object();
								
								Url::redirect('seguimiento');
							}
						}
					}
					else
					{
						Alerta::mostrarMensajeInfo('debescampos','Debes rellenar los campos');
					}
				}
			}
			// END REQUEST POST
			
			// obtenemos los asuntos de las anotaciones para el autocompletado
			$anotacionesAsuntos = $objModeloSeguimiento->obtenerAsuntos();

			
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar_llamada');
		}
	}
}