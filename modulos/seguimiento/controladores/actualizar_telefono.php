<?php
	$idCurso = Usuario::getIdCurso();
	$objModeloSeguimiento = new ModeloSeguimiento();
	
	if(Peticion::isPost())
	{
		$post = Peticion::obtenerPost();
		
		if($objModeloSeguimiento->actualizarTelefonoAlumno($post['editAlumnoTlf'], $post['idAlumno']))
		{
			Alerta::guardarMensajeInfo('telefonoactualizado','El teléfono del alumno ha sido actualizado correctamente');
			Url::redirect('seguimiento/' . $post['idAlumno']);	
		}
	}
?>