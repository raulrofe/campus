<link href="css-seguimiento-default.css" rel="stylesheet" type="text/css" />

<div id="seguimientoAlumno">
	<form id="frmSeguimientoAlumno" method="post" action="">
		<ul>
			<li class="fleft">
				<select name="hora">
					<?php for($cont = 0; $cont < 24; $cont++):?>
						<option value="<?php echo $cont?>" <?php if($cont == $horaActual) echo 'selected="selected"'?>><?php echo $cont?></option>
					<?php endfor;?>
				</select>
				<select name="minutos">
					<?php for($cont = 0; $cont < 60; $cont++):?>
						<option value="<?php echo $cont?>" <?php if($cont == $minutoActual) echo 'selected="selected"'?>><?php echo $cont?></option>
					<?php endfor;?>
				</select>
			</li>

			<li class="fleft">
				<input id="datepicker" type="text" name="fecha" value="<?php echo $fechaActual?>" />
			</li>

			<li class="clear">
				<textarea name="mensaje" rows="1" cols="1"></textarea>
				<input type="hidden" name="idalumno" value="<?php echo $post['idalumno']?>" />
			</li>

			<li class="clear fleft">
				<input type="submit" value="Guardar" data-translate-value="formulario.guardar" />
			</li>

			<li class="fright">
				<div class="fleft">
					<input type="radio" name="tipo" value="1" checked="checked" /> 
					<label data-translate-html="seguimiento.llamada">Llamada</label>
				</div>

				<br />

				<div class="fleft">
					<input type="radio" name="tipo" value="2" /> 
					<label data-translate-html="seguimiento.anotacion">
						Anotaci&oacute;n
					</label>
				</div>
			</li>
		</ul>
		<div class="clear"></div>
	</form>
	
	<div class="listarElementos">
		<?php if($anotaciones->num_rows > 0):?>
			<?php while($anotacion = $anotaciones->fetch_object()):?>
				<div class="elemento">
					<div class="elementoContenido fleft"><p>
						<?php if($anotacion->tipo == 1):?>
							<span class='negrita cursiva' data-translate-html="seguimiento.llamada">
								Llamada
							</span>
						<?php else:?>
							<span class='negrita cursiva' data-translate-html="seguimiento.anotacion">
								Anotaci&oacute;n
							</span>
						<?php endif;?>
						
						<small>(<?php echo Fecha::obtenerFechaFormateada($anotacion->fecha)?>)</small>
						<?php if(!empty($anotacion->nota)):?>
							&rarr;
						<?php endif;?>
						
						<?php echo Texto::textoFormateado($anotacion->nota)?>
					</p></div>
					<div class="elementoPie">
						<ul class="elementoListOptions fright hide">
							<li>
								<a href="seguimiento/llamadas/<?php echo $post['idalumno']?>/editar/<?php echo $anotacion->idseguimiento_llamada?>" title="Editar nota" data-translate-title="general.editar" data-translate-html="general.editar">
									Editar
								</a>
							</li>
							<li>
								<a href="#" <?php echo Alerta::alertConfirmOnClick('borrarnota', '¿Deseas borrar esta nota?', 'seguimiento/llamadas/' . $post['idalumno'] . '/borrar/' .  $anotacion->idseguimiento_llamada);?> title="Borrar nota" data-translate-title="general.borrar" data-translate-html="general.borrar">
									Borrar
								</a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
				</div>
			<?php endwhile;?>
		<?php endif;?>
	</div>
</div>

<script type="text/javascript" src="js-seguimiento-default.js"></script>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>