<div class="subtitle t_center">
	<span data-translate-html="seguimiento.titulo">
		SEGUIMIENTO
	</span>
</div>

<div class="t_center" data-translate-html="seguimiento.descripcion">
	Desde aqu&iacute; podr&aacute; ver el seguimiento del curso y del alumno
</div>

<div id="seguimiento">
	<div id="interfazSeguimiento">
		<span id="postIdAlumno" class="hide">
			<?php
				if(isset($post['idalumno']) && is_numeric($post['idalumno'])) echo $post['idalumno'];
				else if(isset($get['idalumno']) && is_numeric($get['idalumno']) ) echo $get['idalumno'];
			?>
		</span>
		<div id="encabezadoSeguimiento">
			<div class="encabezadoAlumnoTitulo">
				<div data-translate-html="integrantes.alumnos">
					Alumnos
				</div>
			</div>
			<div class="encabezadoAlumno" style="width:68%">
				<div class="fleft">
					<!-- tab seguimiento telefonico -->
					<div class="encabezadoSeguimientoActive tabSeguimientoAnotaciones" id="segPhoneStudent">
						<a href="#" onclick="seguimientoMostrarLlamadas(); return false;" rel="tooltip" title="seguimiento telef&oacute;nico" data-translate-title="seguimiento.seg_telefonico">
							<img src="imagenes/seguimiento/seguimiento_llamadas.png" alt="" />
						</a>
					</div>

					<!-- tab seguimiento mail -->
					<div class="tabSeguimientoMailer" id="segMailStudent">
						<a href="#" onclick="seguimientoMostrarEmails(); return false;" rel="tooltip" title="Seguimiento por e-mail" data-translate-title="seguimiento.seg_email">
							<img src="imagenes/seguimiento/seguimiento_email.png" alt="" />
						</a>
					</div>
				</div>

				<!-- tab imprimir informe -->
				<div class="fright">
					<div class="menuVista printEvaluation">
						<a href="seguimiento/informe" target="_blank" rel="tooltip" title="informe completo del seguimiento" data-translate-title="seguimiento.informec">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista" />
						</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="cuerpoSeguimientoLlamada">
			<div class="alumnosSeguimientoLlamada fleft">
				<div id="seguimientoBuscador">
					<input type="text" placeholder="Buscar por apellidos" data-translate-placeholder="seguimiento.buscar"/>
				</div>
				<div class="alums">
					<?php while($alumno = $alumnos->fetch_object()):?>

						<?php
							//Obtengo la primera conexion
							$conectadoPrimera = false;
							$firstConexion = $objModeloSeguimiento->obtenerPrimeraConexion($alumno->idalumnos, $idCurso);
							if($firstConexion->num_rows > 0)
							{
								$conectadoPrimera = true;
								$rowFirstConexion = $firstConexion->fetch_object();
								$dataFirstConexion = $rowFirstConexion->fecha_entrada;

							}
							else
							{
								$conectadoPrimera = false;
							}

							//CUENTO MENSAJES DEL FORO
							//$nForo = $objModeloForo->obtenerMensajesPorAlumno($alumno->idalumnos, $idCurso);
							$nTemasForo = $objModeloForo->obtenerTemasNoGenerales($idCurso);
							$nForo = $objModeloForo->obtenerTemasParticipadoPorAlumno($alumno->idalumnos, $idCurso);

							$numTemasParticipado = $objModeloNotas->obtenerTemas($alumno->idalumnos, Usuario::getIdCurso())->num_rows;

							//CUENTO TRABAJOS PRACTICOS
							$r_numTrabjPract = $objModelo->obtenerNumTrabajosPracticosPorRealizar($alumno->idalumnos, $idCurso);
							if($r_numTrabjPract->num_rows > 0)
							{
								//$r_numTrabjPract = $r_numTrabjPract->fetch_object();
								$numTrabjPract =  $r_numTrabjPract->num_rows;
							}
							else
							{
								$numTrabjPract = 0;
							}

							//CUENTO LAS AUTOEVALUACIONES
							$testSuperado = 0;

							$idMatricula = Usuario::getIdMatriculaDatos($alumno->idalumnos, $idCurso);
							$numTestAlumno = $objModeloNotas->obtenerNumTestAlumno($idMatricula);

							//Autoeval superadas
							if($numTestAlumno->num_rows > 0)
							{
								while($testParticipado = $numTestAlumno->fetch_object())
								{
									if($testParticipado->nota >= 5)
									{
										$testSuperado++;
									}
								}
							}


							$numAutoeval = $objModelo->obtenerNumAutoevalPorRealizar($alumno->idalumnos, $idCurso);
							if($numAutoeval->num_rows > 0)
							{
								$numAutoeval = $numAutoeval->num_rows;
								//$numAutoeval =  $numAutoeval->num;
							}
							else
							{
								$numAutoeval = 0;
							}

							$testRealizados = $testSuperado;
							$tpRealizados = $nTrabPract - $numTrabjPract;

							if($configNotas->extra1 > 0){
								$notaSpeaking = mysqli_fetch_object($mi_curso->getNotaSpeaking($idMatricula));
								if(!empty($notaSpeaking->nota)){
									$tareaSpeaking = 1;
								} else {
									$tareaSpeaking = 0;
								}

							 	$tareasRealizadas = $testRealizados + $tpRealizados + $numTemasParticipado + $tareaSpeaking;

							 	// if($idCurso == 536 || $idCurso == 568 || $idCurso == 594 || $idCurso == 633 || $idCurso == 681 || 
							 	// 	$idCurso == 718 || $idCurso == 759 || $idCurso == 783) {

							 	// 	$tareasRealizadas = $testRealizados + $tpRealizados;
							 	// }


							} else {
								$tareasRealizadas = $testRealizados + $tpRealizados;
							}

							$porcentajeSuperado = $tareasRealizadas * 100 / $tareasObligatorias;

						?>

						<div class="datosAlumno" id="dates<?php echo $alumno->idalumnos ?>"
							data-page="<?php echo $page?>"
							data-tlfn="<?php if($alumno->telefono != 0) echo $alumno->telefono?>"
							data-tlfn2="<?php if($alumno->telefonoCentro != NULL) echo $alumno->telefonoCentro?>"
							data-search="<?php echo Texto::textoPlano($alumno->apellidos . ' ' . $alumno->nombre . ' ' . $alumno->dni)?>"
							data-id-alumno="<?php echo $alumno->idalumnos ?>" onclick="activateAnotacion(<?php echo $alumno->idalumnos ?>)">

							<div class="nombreAlumno">
								<?php echo Texto::textoPlano($alumno->apellidos) ?>, <?php echo Texto::textoPlano($alumno->nombre) ?>
							</div>
							<div class="referenciasAlumno">
								<div class="fleft telefono"><span>(<?php echo $alumno->dni?>)</span></div>

								<div class="clear"></div>

								<div>
									<div class="fleft">
										<!-- imagen conexion -->
										<?php if($conectadoPrimera):?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/informes/tiempos', 600, 800); return false;">
												<img src="imagenes/seguimiento/connect_ok.png" rel="tooltip" title="conectado" data-translate-title="seguimiento.conectado"/>
											</a>
										<?php else: ?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/informes/tiempos', 600, 800); return false;">
												<img src="imagenes/seguimiento/no_connect.png" rel="tooltip" title="no conectado" data-translate-title="seguimiento.no_conectado"/>
											</a>
										<?php endif ?>

										<!-- imagen foro -->
										<?php if($nForo->num_rows > 0):?>
											<?php $numeroTemasParticipados = $nForo->num_rows;?>
											<?php if($numeroTemasParticipados >= $nTemasForo->num_rows):?>
												<a href="#" onclick="popup_open('Mensajes del foro', 'calificaciones_foro_mensajes_<?php echo $alumno->idalumnos?>', 'evaluaciones/calificaciones/foros/<?php echo $alumno->idalumnos?>', '400', '500'); return false;">
													<img src="imagenes/seguimiento/foro.png" rel="tooltip" title="ha participado en el foro" data-translate-title="seguimiento.si_foro" />
												</a>
											<?php else:?>
												<a href="#" onclick="popup_open('Mensajes del foro', 'calificaciones_foro_mensajes_<?php echo $alumno->idalumnos?>', 'evaluaciones/calificaciones/foros/<?php echo $alumno->idalumnos?>', '400', '500'); return false;">
													<img src="imagenes/seguimiento/siforo.png" rel="tooltip" title="ha participado en alguno de los temas del foro" data-translate-title="seguimiento.algun_foro"/>
												</a>
											<?php endif ?>
										<?php else: ?>
											<a href="#" onclick="popup_open('Mensajes del foro', 'calificaciones_foro_mensajes_<?php echo $alumno->idalumnos?>', 'evaluaciones/calificaciones/foros/<?php echo $alumno->idalumnos?>', '400', '500'); return false;">
												<img src="imagenes/seguimiento/noforo.png" rel="tooltip" title="no ha participado en el foro" data-translate-title="seguimiento.no_foro"/>
											</a>
										<?php endif ?>

										<!-- imagen autoevaluaciones -->
										<?php if($numAutoeval == 0 && $nTest > 0):?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/autoevaluaciones/<?php echo $alumno->idalumnos?>', 600, 800); return false;">
												<img src="imagenes/seguimiento/autoeval.png" rel="tooltip" title="ha realizado todas las autoevaluaciones" data-translate-title="seguimiento.si_autoeval" />
											</a>
										<?php elseif($nTest > $numAutoeval):?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/autoevaluaciones/<?php echo $alumno->idalumnos?>', 600, 800); return false;">
												<img src="imagenes/seguimiento/siautoeval.png" rel="tooltip" title="ha realizado algunas autoevaluaciones" data-translate-title="seguimiento.algun_autoeval" />
											</a>
										<?php else: ?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/autoevaluaciones/<?php echo $alumno->idalumnos?>', 600, 800); return false;">
												<img src="imagenes/seguimiento/noautoeval.png" rel="tooltip" title="no ha realizado autoevaluaciones" data-translate-title="seguimiento.no_autoeval" />
											</a>
										<?php endif ?>

										<!-- imagen trabajos practicos -->
										<?php if($numTrabjPract == 0 && $nTrabPract > 0):?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/trabajos_practicos', 600, 800); return false;">
												<img src="imagenes/seguimiento/trab_pract.png" rel="tooltip" title="ha realizado todos los trabajos practicos" data-translate-title="seguimiento.si_trabajo" />
											</a>
										<?php elseif($nTrabPract > $numTrabjPract): ?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/trabajos_practicos', 600, 800); return false;">
												<img src="imagenes/seguimiento/sitrab_pract.png" rel="tooltip" title="ha realizado algunos trabajos practicos" data-translate-title="seguimiento.algun_trabajo" />
											</a>
										<?php else:?>
											<a href="#" onClick="popup_open('Evaluaciones', 'evaluaciones', 'evaluaciones/calificaciones/trabajos_practicos', 600, 800); return false;">
												<img src="imagenes/seguimiento/notrab_pract.png" rel="tooltip" title="no ha realizado trabajos practicos" data-translate-title="seguimiento.no_trabajo"/>
											</a>
										<?php endif ?>
									</div>
									<div class="fright" style="margin-top:6px;margin-right:-10px">
									<!-- Porcentaje superado Total de tareas -->
									<?php if($porcentajeSuperado < 75):?>
										<span style="font-size:0.8em;font-weight:bold;padding:3px;border-radius:10px;color:#C94237;"><?php echo number_format($porcentajeSuperado, 2, ',', ' ') ?>
												% 
												<span data-translate-html="seguimiento.superado">
													Superado
												</span>
										</span>
									<?php else: ?>
										<span style="font-size:0.8em;font-weight:bold;padding:3px;border-radius:10px;color:#518E31;"><?php echo number_format($porcentajeSuperado, 2, ',', ' ') ?>
												% 
												<span data-translate-html="seguimiento.superado">
													Superado
												</span>
										</span>
									<?php endif; ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="clear"></div>


						<?php if($contPage >= $maxElementsPaging)
						{
							$page++;
							$contPage = 0;
						}

						$contPage++;
						?>
					<?php endwhile;?>
				</div>
				<?php echo Paginado::crearAjax($numPagina, $maxPaging, 'seguimiento/pagina/');?>

			</div>
			<div class="contenidoSeguimientoLlamadas fleft">
				<div>
					<div id="infoAlumno">
						<?php while($alumno2 = $alumnos2->fetch_object()):?>

								<div id="alumnoImagenData" data-id-alumno="<?php echo $alumno2->idalumnos;?>" class="hide coment<?php echo $alumno2->idalumnos ?>">
									<div id="textAlumno" class="fleft">
										<div>
											<?php echo Texto::textoPlano($alumno2->nombrec) ?>
											<span class="dni">
												- <?php echo $alumno2->dni ?>
											</span></div>
										<div>

											<span data-translate-html="formulario.email">
												E-mail:
											</span>

											<span class="telefono">
												<?php if($alumno2->email):?>
													<?php echo "<span>" . utf8_decode($alumno2->email) . "</span>"; ?>
												<?php else: ?>
													<span data-translate-html="seguimiento.sin_determinar" >
														Sin determinar
													</span>
												<?php endif ?>
											</span>
										</div>
										<div>
											<span data-translate-html="seguimiento.telefono">
												Tel&eacute;fono:
											</span>
											<span class="telefono">
												<?php if($alumno2->telefono != 0):?>
													<?php echo "<span class='editable'  data-id='" . $alumno2->idalumnos . "'>". $alumno2->telefono . "</span>"; ?>
												<?php else: ?>
													<span class="editable" data-translate-html="seguimiento.sin_determinar">
														Sin determinar
													</span>
												<?php endif ?>
											</span>
										</div>
										<div>
											<span class="telefono">
												<span data-translate-html="seguimiento.primera_cox" >
													Primera conexi&oacute;n :
												</span>
												<?php
													//Obtengo la primera conexion
													$conectadoPrimera = false;
													$firstConexion = $objModeloSeguimiento->obtenerPrimeraConexion($alumno2->idalumnos, $idCurso);
													if($firstConexion->num_rows > 0)
													{
														$rowFirstConexion = $firstConexion->fetch_object();
														$dataFirstConexion = $rowFirstConexion->fecha_entrada;
														echo Fecha::obtenerFechaFormateada($dataFirstConexion);

													}
													else
													{
														echo '-';
													}
												?>
											</span>
											<br/>
											<span class="telefono">
												<span data-translate-html="seguimiento.ultima_cox">
													&Uacute;ltima conexi&oacute;n :
												</span>
												<?php
													if($alumno2->fecha_conexion_actual != '0000-00-00 00:00:00')
													{
														echo Fecha::obtenerFechaFormateada($alumno2->fecha_conexion_actual);
													}
													else
													{
														echo '-';
													}
												?>
											</span>
										</div>
									</div>
									<div id="imgAlumno" class="fright">
										<img src="imagenes/fotos/<?php echo $alumno2->foto ?>" alt="" />
									</div>
							</div>
						<?php endwhile ?>
					</div>
					<div class="clear"></div>
						<div class='t_center negrita tabFrm activeAnotation' id="frmTab2" onclick="seguimientoMostrarLlamadasListado();" data-translate-html="seguimiento.listado_nota">
							Listado de anotaciones
						</div>

						<div class='t_center negrita tabFrm' id="frmTab" onclick="seguimientoMostrarLlamadasNueva();" data-translate-html="seguimiento.add_nota">
							A&ntilde;adir anotaci&oacute;n
						</div>

					<div class="clear"></div>

					<div id="formularioAnotacion" class='hide'>
						<form class="" name="frmAnotaciones" id="frmSeguimientoAnotaciones" method="post" action="">
							<label data-translate-html="seguimiento.asunto">
								Asunto
							</label>

							<input type="text" style="width: 273px !important;" name="asunto" class="campos" value=""/>

							<!-- <label>Fecha</label> -->
							<input id="datepicker" type="text" name="fecha" value="<?php echo date('d/m/Y');?>" class="campos"/>

							<!--  <label>Hora</label> -->
							<input id="frmAnotaciones_1" maxlength="2" type="text" name="hora" value="<?php echo date('H');?>" class="campos hide"/>
							<!-- &nbsp;: -->
							<input id="frmAnotaciones_2" maxlength="2" type="text" name="minutos" value="<?php echo date('i');?>" class="campos hide"/>

							<br />

							<input type="radio" name="llamada_a" value="alumno" checked="checked" />
							<label data-translate-html="seguimiento.telefono_alu">
								Tel&eacute;fono alumno
							</label>

							<input type="text" name="telefono" maxlength="9" class="campos" value=""/>

							<br/>

							<input type="radio" name="llamada_a" value="centro" />
							<label data-translate-html="seguimiento.telefono_centro">
								Tel&eacute;fono centro
							</label>

							<input type="text" name="telefono2" maxlength="9" class="campos" value=""/>

							<div class="blockTextarea">
								<textarea name="mensaje" ></textarea>
							</div>

							<input type="hidden" name="idalumno" value="">
							<input type="submit" value="Enviar anotaci&oacute;n" style="width:100%;margin-top:5px;" data-translate-value="seguimiento.enviar"/>
						</form>
					</div>

	<div id="seguimientoEmail" class="hide">

		<div id="seguimientoEmailTabs">
			<ul>
				<li class="activeAnotation">
					<a href="#" onclick="seguimientoMostrarEmailMasivo(); return false;" title="" data-translate-html="seguimiento.general">
						General
					</a>
				</li>

				<li>
					<a href="#" onclick="seguimientoMostrarEmailPersonal(); return false;" title="" data-translate-html="seguimiento.personal">
						Personal
					</a>
				</li>
			</ul>
			<div class="clear"></div>
		</div>

		<div id="cuerpoSeguimiento">
			<div class="contenidoSeguimiento">
				<div class="emailSeguimiento">
					<?php if($seguimientoEmails->num_rows > 0):?>

						<div class="filaSeguimientoEmailCabecera">
							<div class="asuntoSeguimiento" style="width:50%" data-translate-html="seguimiento.asunto">
								Asunto
							</div>

							<div class="fechaSeguimiento" style="width:25%" data-translate-html="seguimiento.enviado_por">
								Enviado por
							</div>

							<div class="fechaSeguimiento" style="width:25%" data-translate-html="seguimiento.fecha_envio">
								Fecha de envio
							</div>
						</div>


						<?php while($seguimientoEmail = $seguimientoEmails->fetch_object()):?>

							<div class="filaSeguimientoEmail" data-id-alumno="<?php echo $seguimientoEmail->idalumnos?>">

								<div class="asuntoSeguimiento" style="width:50%">
									
									<!-- Asunto con enlace -->
									<a href='#' onclick="popup_open('Correo', 'gestor_correos', 'correos/detalle_mensaje/<?php echo $seguimientoEmail->iddestinatario?>/bandeja_salida', 800, 860); return false;">
										<?php echo Texto::textoPlano($seguimientoEmail->asunto)?>
									</a>

									<!-- Estatus del remitente -->
									<?php if($seguimientoEmail->status == 'coordinador'):?>
										<span class="cursiva">
											(<span  data-translate-html="perfiles.coordinador">coordinador/a</span>)
										</span>
									<?php endif;?>

								</div>

								<!-- Remitente -->
								<div class="fechaSeguimiento" style="width:25%">
									<?php echo Usuario::getNameUser($seguimientoEmail->remitente); ?>
								</div>

								<!-- Fecha de envio del mensaje -->
								<div class="fechaSeguimiento" style="width:25%">
									<?php echo Fecha::obtenerFechaFormateada($seguimientoEmail->fechaSeguimiento) ?>
								</div>
							</div>
						<?php endwhile ?>
					<?php else: ?>
						<div class="t_center" data-translate-html="seguimiento.no_emails">
							No se han enviado emails de tipo seguimiento
						</div>
					<?php endif ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div id="cuerpoSeguimientoPersonal" class="hide">
			<div class="contenidoSeguimiento">
				<div class="emailSeguimiento">
					<?php if($seguimientoEmailsPersonal->num_rows > 0):?>

						<div class="filaSeguimientoEmailCabecera">
							<div class="asuntoSeguimiento" style="width:50%" data-translate-html="seguimiento.asunto">
								Asunto
							</div>

							<div class="fechaSeguimiento" style="width:25%" data-translate-html="seguimiento.enviado_por">
								Enviado por
							</div>

							<div class="fechaSeguimiento" style="width:25%" data-translate-html="seguimiento.fecha_envio">
								Fecha de envio
							</div>
						</div>

						<?php while($unSeguimientoEmailPersonal = $seguimientoEmailsPersonal->fetch_object()):?>

							<div class="filaSeguimientoEmail" data-id-alumno="<?php echo $unSeguimientoEmailPersonal->idalumnos?>">
								<div class="asuntoSeguimiento" style="width:50%">
									
									<!-- Asunto del mensaje -->
									<a href='#' onclick="popup_open('Correo', 'gestor_correos', 'correos/detalle_mensaje/<?php echo $unSeguimientoEmailPersonal->iddestinatarios?>/bandeja_salida', 800, 860); return false;">
										<?php echo Texto::textoPlano($unSeguimientoEmailPersonal->asunto)?>
									</a>

									<!-- Perfil de quien envia el mensaje -->
									<?php if($unSeguimientoEmailPersonal->status == 'coordinador'):?>
										<span class="cursiva">
											(<span  data-translate-html="perfiles.coordinador">coordinador/a</span>)
										</span>
									<?php endif;?>

								</div>

								<!-- Remitente -->
								<div class="fechaSeguimiento" style="width:25%">
									<?php echo Usuario::getNameUser($unSeguimientoEmailPersonal->remitente); ?>
								</div>

								<!-- Fecha de envio -->
								<div class="fechaSeguimiento" style="width:25%">
									<!-- <img src='imagenes/correos/check_ok.png' alt='' /> -->
									<?php echo Fecha::obtenerFechaFormateada($unSeguimientoEmailPersonal->fechaSeguimiento) ?>
								</div>
							</div>
						<?php endwhile ?>
					<?php else: ?>
					<div class="t_center" data-translate-html="seguimiento.no_emails">
						No se han enviado emails de tipo seguimiento personales
					</div>
					<?php endif ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>

	</div>

	<!-- Anotaciones -->

	<div id="anotaciones">
		<div class="asuntosAnotaciones">Asuntos</div>
		<?php while($anotacion = $anotaciones->fetch_object()): ?>
			<?php //echo '<pre>';print_r($anotacion);echo '</pre>'; ?>
			<?php $asuntoMostrar = $anotacion->asunto . '-' . $anotacion->idalumnos ?>

			<?php if($asuntoAux != $asuntoMostrar):?>
				<h1>
					<a href="#" onclick="seguimientoMostrarAsuntos(this); return false;" data-id-alumno="<?php echo $anotacion->idalumnos?>">
						<img src="imagenes/seguimiento/plus.png" alt="mas opciones" style="vertical-align:middle;" data-translate-title="seguimiento.mas_opciones"/>
						<span><?php echo Texto::textoPlano($anotacion->asunto)?></span>
					</a>
				</h1>
			<?php endif;?>
			<div data-id-alumno="<?php echo $anotacion->idalumnos ?>" data-asunto="<?php echo Texto::textoPlano($anotacion->asunto)?>" class="hide coment<?php echo $anotacion->idalumnos ?> unaAnotacion">
				<p><b><?php echo Texto::textoPlano($anotacion->asunto);?></b></p>
				<div class="saltoAnotacion" style="margin-top:10px;">
					<?php echo Texto::textoFormateado($anotacion->nota)?>
					<br/>
					<div class='telefono'>
						<?php if($anotacion->telefono_llamada == 'alumno'):?>

							<span data-translate-html="seguimiento.telefono">
								Tel&eacute;fono
							</span> 

							<?php echo $anotacion->telefono_llamada?>: <?php echo Texto::textoPlano($anotacion->telefono_alumno);?>

						<?php else:?>

							<span data-translate-html="seguimiento.telefono">
								Tel&eacute;fono
							</span> 

							<?php echo $anotacion->telefono_llamada?>: <?php echo Texto::textoPlano($anotacion->telefono_centro);?>

						<?php endif;?>
					</div>
				</div>
				<div class="fleft">
					<div class='telefono'>
						<?php echo date('d/m/Y H:i', strtotime($anotacion->fecha));?>
					</div>
				</div>
				<div class="optionsLlamadas">
					<ul class='elementoListOptions fright hide'>
						<li>
							<a href="seguimiento/anotacion/<?php echo $anotacion->idalumnos;?>/editar/<?php echo $anotacion->idseguimiento_llamada;?>" title="">
								<img src="imagenes/foro/editar.png" alt="" class=/>
								<span data-translate-html="general.editar">
									Editar
								</span>
							</a>
						</li>
						<li>
							<a title='Borrar' <?php echo Alerta::alertConfirmOnClick('borraranotacion', "¿Deseas borrar esta anotación?", "seguimiento/anotacion/" . $anotacion->idalumnos . "/borrar/" . $anotacion->idseguimiento_llamada);?> href='#' data-translate-title="general.borrar">
								<img src="imagenes/foro/borrar.png" alt="" />
								<span data-translate-html="general.borrar">
									Borrar
								</span>
							</a>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<?php $asuntoAux = $anotacion->asunto . '-' . $anotacion->idalumnos;?>
		<?php endwhile ?>

		<p class='hide noAnotaciones t_center' data-translate-html="seguimiento.no_nota">
			No se han a&ntilde;adido anotaciones para este alumno
		</p>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;');
}
function htmlEntities2(str) {
    return String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#039;/g, '\'');
}

	var availableTags = [
		<?php foreach($arrayAnotacionesAsuntos as $item):?>
			<?php echo 'htmlEntities2("' . htmlspecialchars($item, ENT_QUOTES, 'UTF-8') . '"),';?>
		<?php endforeach;?>
	];

	/*for(avaliableTags)
	{

	}*/

	//alert(availableTags);
	$("#frmSeguimientoAnotaciones input[name='asunto']" ).autocomplete({
		source: availableTags
	});
</script>

<script type="text/javascript" src="js-seguimiento-default.js"></script>
<script type="text/javascript" src="js-seguimiento-jedit.js"></script>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>
