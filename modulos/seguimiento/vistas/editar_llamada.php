<div class="subtitle t_center">
	<span data-translate-html="seguimiento.titulo">
		SEGUIMIENTO
	</span>
</div>

<div class="t_center" data-translate-html="seguimiento.descripcion">
	Desde aqu&iacute; podr&aacute; ver el seguimiento del curso y del alumno
</div>

<div id="seguimiento">
	<div style="margin:20px;">
		<span id="postIdAlumno" class="hide"><?php if(isset($post['idalumno'])) echo $post['idalumno']?></span>

		<div id="cuerpoSeguimientoLlamada">
			<div id="seguimientoAlumno">
				<div class="tituloEditarAnotacion" data-translate-html="seguimiento.editar_nota">
					Editar anotaci&oacute;n
				</div>

				<div id="formularioAnotacion" class=''>
					<form name="frmAnotaciones" id="frmSeguimientoAnotaciones" method="post" action="">
						<label data-translate-html="seguimiento.asunto">
							Asunto
						</label>

						<input type="text" style="width: 505px !important;" name="asunto" class="campos" value="<?php echo Texto::textoPlano($rowAnotacion->asunto)?>"/>

						<input  id="datepicker" type="text" name="fecha" value="<?php echo date('d/m/Y', strtotime($rowAnotacion->fecha));?>" class="campos"/>

						<input class='hide' id="frmAnotaciones_1" maxlength="2" type="text" name="hora" value="<?php echo date('H', strtotime($rowAnotacion->fecha));?>" class="campos"/>

						<input class='hide' id="frmAnotaciones_2" maxlength="2" type="text" name="minutos" value="<?php echo date('i', strtotime($rowAnotacion->fecha));?>" class="campos"/>

						<br />

						<input type="radio" name="llamada_a" value="alumno" <?php if($rowAnotacion->telefono_llamada == 'alumno') echo 'checked="checked"'?> />

						<label data-translate-html="seguimiento.telefono_alu">
							Tel&eacute;fono alumno
						</label>

						<input type="text" name="telefono" maxlength="9" class="campos" value="<?php if($rowAnotacion->telefono_alumno != 0) echo Texto::textoPlano($rowAnotacion->telefono_alumno)?>"/>

						<input type="radio" name="llamada_a" value="centro" <?php if($rowAnotacion->telefono_llamada == 'centro') echo 'checked="checked"'?>  />

						<label data-translate-html="seguimiento.telefono_centro">
							Tel&eacute;fono centro
						</label>

						<input type="text" name="telefono2" maxlength="9" class="campos" value="<?php if($rowAnotacion->telefono_centro != 0) echo Texto::textoPlano($rowAnotacion->telefono_centro)?>"/>
						
						<textarea name="mensaje">
							<?php echo $rowAnotacion->nota?>
						</textarea>

						<input type="submit" value="Editar" style="width:100%;margin-top:5px;"/>
					</form>
				</div>
				<br/>
				<span style="margin-left:10px;padding:10px;border-radius:5px;border:1px solid #CCC;">
					<a href="seguimiento" data-translate-html="seguimiento.volver">
						volver a seguimiento
					</a>
				</span>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	var availableTags = [
		<?php while($row = $anotacionesAsuntos->fetch_object()):?>
	   		<?php echo 'htmlEntities2("' . htmlspecialchars($row->asunto, ENT_QUOTES, 'UTF-8') . '"),';?>
	  	<?php endwhile;?>
	];
	//alert(availableTags);
	$("#frmSeguimientoAnotaciones input[name='asunto']" ).autocomplete({
		source: availableTags
	});
</script>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>

<script type="text/javascript" src="js-seguimiento-default.js"></script>