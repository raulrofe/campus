<?php
class ModeloSeguimiento extends modeloExtend
{
	public function obternerEmailPorCurso($idCurso)
	{
		$sql = "SELECT dest.iddestinatarios, dest.destinatarios, dest.fecha_leido FROM correos AS c
				LEFT JOIN destinatarios AS dest ON dest.idcorreos = c.idcorreos
				WHERE c.idcurso = $idCurso
					AND dest.destinatarios IS NOT NULL AND dest.remitente IS NOT NULL
					AND dest.remitente REGEXP '^t_[0-9]+$' AND dest.destinatarios REGEXP '^[0-9]+$'
				GROUP BY dest.destinatarios, c.idcorreos
				ORDER BY dest.destinatarios ASC";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnos($idCurso, $idAlumno = null, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$addSql = null;
		if(isset($idAlumno))
		{
			$addSql = ' AND al.idalumnos = ' . $idAlumno;
		}
		
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.nombre, al.apellidos, al.foto, al.email, mt.idmatricula,' . 
		' mt.fecha_conexion, mt.fecha_conexion_actual, al.telefono, al.dni, ct.telefono AS telefonoCentro' . 
		' FROM alumnos AS al' .
		' LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos' .
		' LEFT JOIN centros AS ct ON ct.idcentros = al.idalumnos' .
		' WHERE al.borrado = 0' .
		' AND mt.borrado = 0' .
		' AND mt.idcurso = ' . $idCurso . $addSql .
		' GROUP BY al.idalumnos ORDER BY al.apellidos ASC' . $addQuery;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAnotaciones($idMatricula, $idAnotacion = null)
	{
		$addSql = null;
		if(isset($idAnotacion))
		{
			$addSql = ' AND idseguimiento_llamada = ' . $idAnotacion;
		}
		
		$sql = 'SELECT * FROM seguimiento_llamadas WHERE borrado = 0 AND idmatricula = ' . $idMatricula . $addSql . ' ORDER BY fecha DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerAsuntos()
	{
		$sql = 'SELECT sl.asunto FROM seguimiento_llamadas AS sl WHERE sl.borrado = 0';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAnotacionesCurso($idcurso)
	{	
		$sql = 'SELECT *, S.fecha AS fecha FROM seguimiento_llamadas AS S' .
		' LEFT JOIN matricula AS M ON S.idmatricula = M.idmatricula' .
		' WHERE M.idcurso = ' . $idcurso .
		' AND S.borrado = 0' .
		' GROUP BY S.idseguimiento_llamada' .
		' ORDER BY M.idalumnos, S.asunto ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerAnotacionesCursoAlumno($idMatricula)
	{	
		$sql = 'SELECT S.fecha, S.asunto, S.nota' .
		' FROM seguimiento_llamadas AS S' .
		' LEFT JOIN matricula AS M ON S.idmatricula = M.idmatricula' .
		' WHERE S.idmatricula = ' . $idMatricula .
		' AND S.borrado = 0' .
		' GROUP BY S.idseguimiento_llamada' .
		' ORDER BY S.fecha ASC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertarAnotacion($idMatricula, $fecha, $nota, $asunto, $telefono1, $telefono2, $llamada_a)
	{
		$sql = 'INSERT INTO seguimiento_llamadas (idmatricula, fecha, nota, asunto, telefono_alumno, telefono_centro, telefono_llamada)' .
		' VALUES (' . $idMatricula . ', "' . $fecha . '", "' . $nota . '", "' . $asunto . '", "' . $telefono1 . '", "' . $telefono2 . '", "' . $llamada_a . '")';

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarAnotacion($idAnotacion, $idMatricula, $fecha, $nota, $asunto, $telefono1, $telefono2, $llamada_a)
	{
		$sql = 'UPDATE seguimiento_llamadas SET' .
		' idmatricula = ' . $idMatricula . ', fecha = "' . $fecha . '", nota = "' . $nota . '", asunto = "' . $asunto . '", ' .
		' telefono_alumno = "' . $telefono1 . '", telefono_centro = "' . $telefono2 . '", telefono_llamada = "' . $llamada_a . '"' .
		' WHERE idseguimiento_llamada = ' . $idAnotacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarAnotacion($idAnotacion)
	{
		$sql = 'UPDATE seguimiento_llamadas SET borrado = 1 WHERE idseguimiento_llamada = ' . $idAnotacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerSeguimientoEmail($idcurso)
	{
		/*$sql = "SELECT *, SE.fecha AS fechaSeguimiento from seguimiento_email AS SE" .
		" LEFT JOIN matricula AS M ON M.idmatricula = SE.idmatricula" .
		" LEFT JOIN destinatarios AS D ON C.idcorreos = D.idcorreos" .
		" LEFT JOIN correos AS C ON C.idCurso = " . $idcurso .
		" WHERE C.borrado = 0 AND M.idcurso = " . $idcurso .
		" GROUP BY C.idcorreos" .
		" ORDER BY SE.fecha DESC";		
		*/
		
		$sql = "SELECT *, SE.fecha AS fechaSeguimiento from correos AS C" .
		" LEFT JOIN destinatarios AS D ON C.idcorreos = D.idcorreos" .
		" LEFT JOIN seguimiento_email AS SE ON SE.iddestinatario = D.iddestinatarios" .
		" LEFT JOIN matricula AS M ON M.idmatricula = SE.idmatricula" .
		" LEFT JOIN staff AS S ON CONCAT('t_', S.idrrhh) = D.remitente" .
		" WHERE D.remitente REGEXP 't_[0-9]+'" .
		" AND D.remitSuprimir = 0 AND M.idalumnos IS NOT NULL AND SE.iddestinatario IS NOT NULL AND C.idcurso = " . $idcurso .
		" GROUP BY M.idmatricula, C.idcorreos" .
		" ORDER BY SE.fecha DESC";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerSeguimientoEmailPersonal($idcurso)
	{
		$sql = "SELECT *, D.fecha AS fechaSeguimiento from correos AS C" .
		" LEFT JOIN destinatarios AS D ON C.idcorreos = D.idcorreos" .
		" LEFT JOIN seguimiento_email AS SE ON SE.iddestinatario = D.iddestinatarios" .
		" LEFT JOIN matricula AS M ON M.idalumnos = D.destinatarios AND C.idcurso = " . $idcurso .
		" LEFT JOIN staff AS S ON CONCAT('t_', S.idrrhh) = D.remitente" .
		" WHERE D.remitente REGEXP 't_[0-9]+'" .
		" AND D.destinatarios != '' " .
		" AND D.remitSuprimir = 0" . 
		" AND M.idalumnos IS NOT NULL" .
		" AND SE.iddestinatario IS NULL" .
		" AND C.idcurso = " . $idcurso .
		" GROUP BY D.iddestinatarios" .
		" ORDER BY SE.fecha DESC";		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerSeguimientoEmailAlumno($idMatricula)
	{		
		$sql = "SELECT SE.asunto_email, SE.fecha" .
		" FROM correos AS C" .
		" LEFT JOIN destinatarios AS D ON C.idcorreos = D.idcorreos" .
		" LEFT JOIN seguimiento_email AS SE ON SE.iddestinatario = D.iddestinatarios" .
		" LEFT JOIN matricula AS M ON M.idmatricula = SE.idmatricula" .
		" LEFT JOIN staff AS S ON CONCAT('t_', S.idrrhh) = D.remitente" .
		" WHERE D.remitente REGEXP 't_[0-9]+'" .
		" AND D.remitSuprimir = 0" .
		" AND M.idalumnos IS NOT NULL" .
		" AND SE.iddestinatario IS NOT NULL" .
		" AND SE.idmatricula = " . $idMatricula .
		" GROUP BY M.idmatricula, C.idcorreos" .
		" ORDER BY SE.fecha ASC";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerSeguimientoEmailPersonalAlumno($idMatricula)
	{
		$sql = "SELECT D.fecha, C.asunto" .
		" FROM correos as C" .
		" LEFT JOIN destinatarios AS D ON D.idcorreos = C.idcorreos" .
		" LEFT JOIN matricula AS M ON M.idalumnos = D.destinatarios" .
		" LEFT JOIN staff AS S ON S.idcurso = M.idcurso AND S.status != 'invitado'" . 
		" WHERE M.idmatricula = " . $idMatricula .
		" AND D.destinatarios != ''" .
		" AND D.remitente REGEXP 't_[0-9]+'" .
		" AND D.remitSuprimir = 0" . 
		" GROUP BY C.idcorreos" .
		" ORDER BY D.fecha ASC";
		
		/*
		$sql = "SELECT D.fecha, C.asunto" .
		" FROM correos AS C" .
		" LEFT JOIN destinatarios AS D ON C.idcorreos = D.idcorreos" .
		" LEFT JOIN matricula AS M ON M.idalumnos = D.destinatarios AND M.idmatricula = " . $idMatricula .
		" LEFT JOIN staff AS S ON CONCAT('t_', S.idrrhh) = D.remitente" .
		" WHERE D.remitente REGEXP 't_[0-9]+'" .
		" AND D.destinatarios != ''" .
		" AND D.remitSuprimir = 0" .
		" AND M.idmatricula = " . $idMatricula .
		" GROUP BY C.idcorreos";	
		*/	

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarTlfnAlumno($idalumno, $telefono)
	{
		$sql = 'UPDATE alumnos SET telefono = ' . $telefono . ' WHERE idalumnos = ' . $idalumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	//Devuelve true o false si se han superado los trabajos practicos obligatorios
	public function estadoTrabajosPracticos($confTrabajo, $modulosCurso)
	{
		$trabajosTerminados = TRUE;
		
		$modulosCurso = $modulosCurso->fetch_object();
		$nModulosCurso = $modulosCurso->num_rows();
		$nTrabajos = $nModulosCurso * $confTrabajo;
		
		$sql = "SELECT count(CTP.idcalificacion_trabajo_practico) AS ntrabajosrealizados" .
		" FROM calificacion_trabajo_practico" .
		"WHERE idcurso = " . $idCurso .
		"AND idalumnos = " . $idAlumno;
		$resultado = $this->consultaSql($sql);
		$resultTrabajos = $resultTrabajos->fetch_object();
		$resultTrabajos = $resultTrabajos->ntrabajosrealizados;
		
		return $trabajosTerminados;	
	}
	
	public function obtenerPrimeraConexion($idAlumno, $idCurso)
	{
		$sql = "SELECT LA.fecha_entrada" .
		" FROM logs_acceso as LA" .
		" LEFT JOIN matricula as M ON M.idmatricula = LA.idmatricula" .
		" WHERE M.idalumnos = " . $idAlumno .
		" AND M.idcurso = " . $idCurso .
		" ORDER BY M.fecha ASC" .
		" LIMIT 0,1";	
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarTelefonoAlumno($telefonoAlumno, $idalumno)
	{
		$sql = "UPDATE alumnos SET telefono = '$telefonoAlumno' where idalumnos = " . $idalumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}