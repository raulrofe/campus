
$(document).ready(function()
{
	var idAlum = $.trim($('#postIdAlumno').html());
	
	if(idAlum != '')
	{
		activateAnotacion(idAlum);
	}
	else
	{
		//alert ($('.alums .datosAlumno:first').attr('data-id-alumno'));
		activateAnotacion($('.alums .datosAlumno:first').attr('data-id-alumno'));
	}

	var page = $('.alums').find('.datosAlumno[data-id-alumno="' + idAlum + '"]').attr('data-page');
	if(page != undefined)
	{
		seguimientoPagina(page);
		$('.alumnosSeguimientoLlamada .alums > div[data-id-alumno="' + idAlum + '"]').click();
	}
	else
	{
		$('.alumnosSeguimientoLlamada .alums > div[data-page!="1"]').addClass('hide');
	}
	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#seguimientoBuscador').keyup(function(event)
	{
		seguimientoBuscar();
	});

	translateAll(getCookie('language'));

});

// BUSCADOR DE CURSOS INACTIVOS POR CONVOCATORIA
function seguimientoBuscar()
{
	var wordSearch = $('#seguimientoBuscador input').val();
	
	$('.alumnosSeguimientoLlamada .alums > div').addClass('hide');
	
	if(wordSearch != "")
	{
		var inSearch = '';
		$('.alumnosSeguimientoLlamada .alums > div').each(function(index)
		{
			inSearch = $(this).attr('data-search');
			if(inSearch != undefined)
			{
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('(' + wordSearch + ')', 'i');
				if(inSearch.match(regex))
				{
					$(this).removeClass('hide');
				}
			}
		});
	}
	else
	{
		$('.alumnosSeguimientoLlamada .alums > div').removeClass('hide');
		$('.alumnosSeguimientoLlamada .alums > div[data-page!="' + $('#cuerpoSeguimientoLlamada .paginado').attr('data-page-current') + '"]').addClass('hide');
	}
}


//Activa un usuario
function activateAnotacion(id){
	if($('#dates' + id).size() > 0)
	{
		$('#popupModal_seguimiento_alumnos_wrapper_aux').scrollTop(0);
		
		$('.datosAlumno').removeClass("active");
	  	$("#dates" + id).addClass("active");

	  	$('#anotaciones h1').addClass('hide');
	  	$('#anotaciones h1 > a[data-id-alumno="' + id + '"]').parent('h1').removeClass('hide');
	  	
	  	$('#formularioAnotacion input[name="idalumno"]').val($("#dates" + id).attr("data-id-alumno"));
	  	$('#formularioAnotacion input[name="telefono"]').val($.trim($("#dates" + id).attr('data-tlfn')));
	  	$('#formularioAnotacion input[name="telefono2"]').val($.trim($("#dates" + id).attr('data-tlfn2')));
	  	
	  	
  		$('#cuerpoSeguimiento .filaSeguimientoEmail[data-id-alumno="' + id + '"]').removeClass("hide");
  		$('.noAnotaciones').addClass("hide");
  		$('#infoAlumno > div').addClass("hide");
  		
  		$('#cuerpoSeguimiento .filaSeguimientoEmail').addClass("hide");
  		$('#cuerpoSeguimiento .filaSeguimientoEmail[data-id-alumno="' + id + '"]').removeClass("hide");

  		$('#cuerpoSeguimientoPersonal .filaSeguimientoEmail').addClass("hide");
  		$('#cuerpoSeguimientoPersonal .filaSeguimientoEmail[data-id-alumno="' + id + '"]').removeClass("hide");
  		
  		$('#infoAlumno .coment' + id).removeClass("hide");

	  	if($('#anotaciones .coment' + id + '[data-id-alumno="' + id + '"]').size() > 0)
	  	{
	  		//$('#anotaciones h1 > a').attr("data-id-alumno", id);

	  		$('#anotaciones > div').addClass("hide");
	  		$('#anotaciones .coment' + id).removeClass("hide");
	  		$('#anotaciones .unaAnotacion').addClass('hide');
	  		$('.asuntosAnotaciones').removeClass("hide");
	  		//$('#anotaciones > h1').removeClass("hide");
	  			  		
	  		//checked tlfn
	  		if($.trim($("#dates" + id).attr('data-tlfn')) == '' && $.trim($("#dates" + id).attr('data-tlfn2')) != '')
	  		{
	  			$('#formularioAnotacion input:last[name="llamada_a"]').attr('checked', 'checked');
	  		}
	  		else
	  		{
	  			$('#formularioAnotacion input:first[name="llamada_a"]').attr('checked', 'checked');
		  	}
	  	}
	  	else
	  	{
	  		$('#anotaciones > div').addClass("hide");
	  		$('.noAnotaciones').removeClass("hide");
	  	}	
	}
}

function seguimientoMostrarAsuntos(elemento)
{
	
	//alert($(elemento).html());
	
	asunto = $(elemento).find('span').html();
	idAlumno = $(elemento).attr('data-id-alumno');

	$('#anotaciones .unaAnotacion').addClass('hide');
	$('#anotaciones .unaAnotacion[data-asunto="' + asunto + '"][data-id-alumno="' + idAlumno + '"]').removeClass('hide');
}

//Muestra oculta los emails
function mostrar(id){
	$('#' + id).toggle(200);
}

function seguimientoPagina(pag)
{
	$('#popupModal_seguimiento_alumnos_wrapper_aux').scrollTop(0);
	
	$('.alumnosSeguimientoLlamada .alums > div[data-page]').removeClass('hide');
	$('.alumnosSeguimientoLlamada .alums > div[data-page!="' + pag +'"]').addClass('hide');
	
	$('.alumnosSeguimientoLlamada .alums > div:first[data-page="' + pag +'"]').click();

	$('.paginado ul a').css('font-weight', 'normal');
	$('.paginado ul a[data-page="' + pag +'"]').css('font-weight', 'bold');
	
	$('#cuerpoSeguimientoLlamada .paginado').attr('data-page-current', pag);
}

 /* ------- */

function seguimientoMostrarLlamadasListado()
{
	$('#frmTab').removeClass('activeAnotation');
	$('#frmTab2').addClass('activeAnotation');
	$('#anotaciones').removeClass('hide');
	$('#formularioAnotacion').hide();
}

function seguimientoMostrarLlamadasNueva()
{
	$('#frmTab2').removeClass('activeAnotation');
	$('#frmTab').addClass('activeAnotation');
	$('#anotaciones').addClass('hide');
	$('#formularioAnotacion').show();
}

/* ------- */

function seguimientoMostrarEmailMasivo()
{
	$('#seguimientoEmailTabs li:last').removeClass('activeAnotation');
	$('#seguimientoEmailTabs li:first').addClass('activeAnotation');
	$('#cuerpoSeguimiento').removeClass('hide');
	$('#cuerpoSeguimientoPersonal').addClass('hide');
}

function seguimientoMostrarEmailPersonal()
{
	$('#seguimientoEmailTabs li:first').removeClass('activeAnotation');
	$('#seguimientoEmailTabs li:last').addClass('activeAnotation');
	$('#cuerpoSeguimiento').addClass('hide');
	$('#cuerpoSeguimientoPersonal').removeClass('hide');
}

/* ---------- */

function seguimientoMostrarLlamadas()
{
	$('#seguimientoEmail').addClass('hide');
	$('#anotaciones').removeClass('hide');

	$('#segPhoneStudent').addClass('encabezadoSeguimientoActive');
	$('#segMailStudent').removeClass('encabezadoSeguimientoActive');
	
	$('#frmTab').removeClass('hide');
	$('#frmTab2').removeClass('hide');
}
function seguimientoMostrarEmails()
{
	$('#anotaciones').addClass('hide');
	$('#seguimientoEmail').removeClass('hide');
	
	$('#segMailStudent').addClass('encabezadoSeguimientoActive');
	$('#segPhoneStudent').removeClass('encabezadoSeguimientoActive');
	
	$('#frmTab').addClass('hide');
	$('#frmTab2').addClass('hide');
	$('#formularioAnotacion').hide();
	$('#formularioAnotacion').hide();
}