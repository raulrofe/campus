    (function($) {

        $.fn.inlineEdit = function(options) {
        
            // define some options with sensible default values
            // - hoverClass: the css classname for the hover style
            options = $.extend({
                hoverClass: 'hover'
            }, options);
        
            return $.each(this, function() {
        
                // define self container
                var self = $(this);
        
                // create a value property to keep track of current value
                self.value = self.text();
        
                // bind the click event to the current element, in this example it's span.editable
                self.bind('click', function() {

                	//var page = $('.alumnosSeguimientoLlamada .alums > div[data-page]');
                	var idAlumno = self.parents('div[data-id-alumno]').attr('data-id-alumno');
                	
                    self
                        // populate current element with an input element and add the current value to it
                        .html(
                                '<form id="frmEditTlfAlumno" target="popupModal_seguimiento_alumnos_form_iframe" action="seguimiento/actualizar/telefono-alumno" name="frmEditTlfAlumno" method="post">' +
	                        		'<input type="text" value="'+ self.value +'" name="editAlumnoTlf" />' +
	                        		'<input type="hidden" name="idAlumno" value="' + idAlumno + '" />' +
	                        	'</form>'
                        	)		
                        // select this newly created input element
                        .find('input')
                            // bind the blur event and make it save back the value to the original span area
                            // there by replacing our dynamically generated input element
                            .bind('blur', function(event) {                           	
                            	//self.value = $(this).val();
                                //self.text(self.value);
                                $('#frmEditTlfAlumno').submit();
                            })
                            // give the newly created input element focus
                            .focus();                                
                })
                // on hover add hoverClass, on rollout remove hoverClass
                .hover(
                    function(){
                        self.addClass(options.hoverClass);
                    },
                    function(){
                        self.removeClass(options.hoverClass);
                    }
                );
            });
        }
        
    })(jQuery);
        

	$(function() {
	  $('.editable').inlineEdit({
	    buttonText: 'Add',
	    save: function(e, data) {
	      return confirm('Change name to '+ data.value +'?');
	    }
	  });
	});
