<?php

// Conexion a base de datos
$db = new database();
$con = $db->conectar();

// Cargo los modelos
$mi_correo = new Gestor_correos();
$mi_usuario = new Usuarios();
$mi_alumno = new Alumnos();
$mi_curso = new Cursos();

// Realizo la peticion pos y get
$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

// Buscador
$urlPagingEnd = null;
$textBuscarHighlight = '';

if(isset($get['textBuscar']))
{
	$urlPagingEnd = '&textBuscar=' . urlencode($get['textBuscar']);
	$textBuscarHighlight = $get['textBuscar'];
}

//Cargo el controlador de foro para el paginado
mvc::cargarModuloSoporte('foro');

// Extension para comprobar las validas en el email
$extensionesEmail = array(
	'cpt',
	'csv',
	'rar',
	'psd',
	'pptx',
	'pdf',
	'ai',
	'eps',
	'ps',
	'odt',
	'xls',
	'ppt',
	'pptx',
	'gtar',
	'gz',
	'gzip',
	'swf',
	'tar',
	'tgz',
	'xhtml',
	'zip',
	'mid',
	'midi',
	'mpga',
	'mp2',
	'mp3',
	'wav',
	'bmp',
	'gif',
	'jpeg',
	'jpg',
	'jpe',
	'png',
	'tiff',
	'tif',
	'css',
	'html',
	'htm',
	'txt',
	'text',
	'xml',
	'xsl',
	'mpeg',
	'mpg',
	'mpe',
	'qt',
	'mov',
	'avi',
	'movie',
	'doc',
	'docx',
	'xlsx',
	'word',
	'xl',
	'3g2',
	'3gp',
	'mp4',
	'm4a',
	'f4v',
	'aac',
	'vlc',
	'wmv',
	'au',
	'ac3',
	'flac',
	'ogg',
	'kmz',
	'kml'
);

// Establezco el curso
$idSelectCurso = $_SESSION['idcurso'];
if(isset($post['idSelectCurso']) and !empty($post['idSelectCurso']))
{
	$idSelectCurso = $post['idSelectCurso'];
}

$perfil = Usuario::getProfile();

if($perfil != 'alumno') {

	 $idusuario = Usuario::getIdUser(true);
	 $resultado = $mi_usuario->buscar_usuario();
	 $datos_usuario = mysqli_fetch_assoc($resultado);
	 $nombre_usuario = $datos_usuario['nombrec'];

} else {

	$idusuario = Usuario::getIdUser();
	$mi_alumno->set_alumno($idusuario);
	$resultado = $mi_alumno->ver_alumno();
	$datos_usuario = mysqli_fetch_assoc($resultado);
	$nombre_usuario = $datos_usuario['nombre']." ".$datos_usuario['apellidos'];
}

if(!empty ($get['todo'])) {

	$todo = $get['todo'];
	if($todo == 'no') $todo2 = 'si';
	else $todo2 = 'no';

} else {

	$todo2 = 'si';
	$todo='no';
}


// buscamos los usuarios que pertenecen al curso
$staff_curso = $mi_usuario->buscar_usuarios_curso($idSelectCurso);

// clase para los alumnos que pertenecen al curso
$registro_alumnos = $mi_alumno->alumnos_del_curso_activos($idSelectCurso);

// Obtenemos los mensajes sin leer bandeja de entrada
$mensajes_sinleer = $mi_correo->contador_mensajes_sin_leer();
$mensajes_sin_leer = ($mensajes_sinleer->num_rows == 1) ? mysqli_fetch_object($mensajes_sinleer)->nme : 0;

// Obtenemos los mensajes sin leer papelera
$mensajes_sinleer_papelera = $mi_correo->contador_mensajes_sin_leer_papelera();
$mensajesPapeleraNoleidos = ($mensajes_sinleer_papelera->num_rows == 1) ? mysqli_fetch_object($mensajes_sinleer_papelera)->nmp : 0;

//Obtenemos los mensajes sin leer de la subcarpeta elegida
$mensajes_sinleer_subcarpeta = $mi_correo->contador_mensajes_sin_leer_subcarpeta();
$mensajesSubcarpetaNoleidos = ($mensajes_sinleer_subcarpeta->num_rows == 1) ? mysqli_fetch_object($mensajes_sinleer_subcarpeta)->nms : 0;

//Buscamos los cursos que pertenecen al usuario para crear las carpetas para organizarlas
$fechaInicioCurso = Usuario::getFechaInicioCurso();
$fechaFinCurso = Usuario::getFechaFinCurso();

$registros_carpetas = $mi_curso->cursos_usuario(null, null, true, $fechaInicioCurso, $fechaFinCurso);

$carpetasPersonales = $mi_correo->obtenerCarpetas(Usuario::getidUser(true));

$carpetasPersonalesArray = array();
while($carpetaPersonal = $carpetasPersonales->fetch_object()) {

	$carpetasPersonalesArray[] = $carpetaPersonal;
}


//Mostramos el nombre de la carpeta
$carpeta = ucfirst($get['c']);
$carpeta = str_replace('_',' ',$carpeta);

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina'])) {

	$numPagina = $get['pagina'];
}



if(isset($post['hacer']) and $post['hacer'] == 'noleido') {

	if(isset($post['idmensaje'])) {

		$idDelCorreo = $post['idmensaje'];

		if($mi_correo->marcarNoleido($idDelCorreo)) {

			Alerta::mostrarMensajeInfo('accioncompletada', 'La accion se ha completado correctamente');
		}
	} else if(isset($get['idcorreo']) && is_numeric($get['idcorreo'])) {

		$idDelCorreo = $get['idcorreo'];

		if($mi_correo->marcarNoleido(array($idDelCorreo))) {

			Alerta::guardarMensajeInfo('accioncompletada', 'La accion se ha completado correctamente');

			switch($get['menu']) {
				case 'carpeta':
					if(isset($post['idcarpeta_auto']) & is_numeric($post['idcarpeta_auto'])) {

						Url::redirect('correos/subcarpeta/' . $post['idcarpeta_auto']);
					}
					break;
				default:
					Url::redirect('correos/' . $get['menu']);
			}
		}
	} else {

		Alerta::mostrarMensajeInfo('marcarmensaje', 'Debes seleccionar al menos un mensaje para marcarlo como No le&iacute;do/Le&iacute;do');
	}
}

// asigna un correo/os a una carpeta personal
if(isset($post['idcarpeta_personal']) && is_numeric($post['idcarpeta_personal'])) {

	if(isset($post['idmensaje']) && is_array($post['idmensaje'])) {

		$resultCarpPersonales = $mi_correo->obtenerUnaCarpeta($post['idcarpeta_personal'], $idusuario);

		if($resultCarpPersonales->num_rows > 0) {

			foreach($post['idmensaje'] as $item) {

				$resultUnCorreo = $mi_correo->mostrar_mensaje($item);

				if(!empty($resultUnCorreo)) {

					$mi_correo->eliminarCorreoDeCarpetaPersonal($item);

					if($mi_correo->eliminarCorreoDeCarpetaCurso($item) && $mi_correo->asignarCarpetaACorreo($item, $post['idcarpeta_personal'])) {

						Alerta::guardarMensajeInfo('mensajearchivado', 'El mensaje ha sido archivado en la carpeta');
					}
				}
			}
		}
	} else {

		Alerta::guardarMensajeInfo('algunmensaje', 'Selecciona algun mensaje');
	}

} 

if(isset($get['ver_idcarpeta']) and !empty($get['ver_idcarpeta']) and empty($get['textBuscar'])) {

	if(isset($post['mandar'], $post['hacer']) && $post['hacer'] == 'eliminar' && $post['mandar'] == 'papelera') {

		if($mi_correo->enviar_papelera($post['idmensaje'], 'entrada')) {
			Alerta::guardarMensajeInfo('mensajepapelera', 'El mensaje ha sido enviado a la papelera');
		}
	}

	$verIdcarpeta = $get['ver_idcarpeta'];
	$registros_correos = $mi_correo->bandeja_carpetas($verIdcarpeta, $numPagina);
	$numero_registros = mysqli_num_rows($registros_correos);
	$maxElementsPaging = 20;
	$maxPaging = ceil($numero_registros / $maxElementsPaging);
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	$registros_correos = $mi_correo->bandeja_carpetas($verIdcarpeta);
	$mensaje_carpetas = '<span data-translate-html="correo.nomensajescarpeta">No existen mensajes archivados en esta carpeta';
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');

} else if($get['c'] == 'bandeja_entrada' && isset($get['idcarpeta']) and !empty($get['idcarpeta']) and empty($get['textBuscar'])) {

	$rowCarpeta = $mi_correo->obtenerUnaCarpeta($get['idcarpeta'], Usuario::getIdUser(true));

	if($rowCarpeta->num_rows == 1) {

		if(isset($post['mandar'], $post['hacer']) && $post['hacer'] == 'eliminar' && $post['mandar'] == 'papelera') {

			if($mi_correo->enviar_papelera($post['idmensaje'], 'entrada')) {

				Alerta::guardarMensajeInfo('mensajepapelera', 'El mensaje ha sido enviado a la papelera');
			}
		}

		$verIdcarpeta = $get['idcarpeta'];
		$registros_correos = $mi_correo->bandeja_carpetas_personales($verIdcarpeta, $numPagina);
		$numero_registros = mysqli_num_rows($registros_correos);
		$maxElementsPaging = 20;
		$maxPaging = ceil($numero_registros / $maxElementsPaging);
		$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		$registros_correos = $mi_correo->bandeja_carpetas_personales($verIdcarpeta);
		$mensaje_carpetas = '<span data-translate-html="correo.nomensajescarpeta">No existen mensajes archivados en esta carpeta</span>';
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
	}
} else if(!empty($get['textBuscar'])) {

	$textBuscar = '';

	if(isset($get['textBuscar'])) $textBuscar = $get['textBuscar'];

	$registros_correos = $mi_correo->textBuscar($textBuscar);

	$mensaje_buscar = '<span data-translate-html="correo.nobusqueda">No se encontraron mensajes para la busqueda realizada</span>';
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');

} else if(!isset($post['mandar']) and isset($post['hacer']) and $post['hacer'] == 'eliminar') {

	if(isset($post['idmensaje_correo'],$post['origen']) && is_numeric($post['idmensaje_correo'])) {

		if($post['origen'] != 'correos/papelera') {

			if($post['origen'] == 'correos/bandeja_entrada') {

				$bandeja_papelera = 'entrada';
				$redirigir = 'correos/bandeja_entrada';

			} else {

				$bandeja_papelera = 'salida';
				$redirigir = 'correos/bandeja_salida';
			}

			if ($mi_correo->enviar_papelera(array($post['idmensaje_correo']), $bandeja_papelera)) {

				Alerta::guardarMensajeInfo('mensajepapelera', 'Mensaje enviado a la papelera');
			}

		} else {

			$redirigir = 'correos/papelera';

			if ($mi_correo->eliminar_definitivamente($post['idmensaje_correo'])){
				
				Alerta::guardarMensajeInfo('mensajeeliminado', 'Mensaje eliminado');
			}
		}

		Url::redirect($post['origen']);
	}
}
