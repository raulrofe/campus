<?php
require_once("basic.php");

if( isset($post['idSelectCurso'])) $idcurso_envio = $post['idSelectCurso'];
else $idcurso_envio = $_SESSION['idcurso'];

// Valor del usuario
if(isset($_SESSION['idusuario']))
{
	$idusuario = $_SESSION['idusuario'];	
}

$idusuario_envio = Usuario::getIdUser(true);

$remitente = $_SESSION['idusuario'];


//SI EXISTE UNA PETION POR POST
if(Peticion::isPost())
{
	//Si existen alumnos seleccionados	
	if (isset ($post['idalumnos']))
	{
		$idalumnos = $post['idalumnos'];
	}
		
	//Si existe alguna opcion de envio masivo
	if(isset ($post['enviar_a'])) 
	{
		$enviar_a = $post['enviar_a'];
	}
	
	// Valor de importante
	if(isset($post['importante']) and is_numeric($post['importante']))
	{
		$importante = $post['importante'];
	}
	else 
	{
		$importante = 0;
	}
			
	if (!empty ($enviar_a) or !empty ($idalumnos))
	{	
		if( isset($post['idSelectCurso']) && is_numeric($post['idSelectCurso'])) 
		{
			$idcurso = $post['idSelectCurso'];
		}
		else 
		{
			$idcurso = $_SESSION['idcurso'];
		}

		$fecha = Fecha::fechaActual();
		$fecha_format = Fecha::obtenerFechaFormateada($fecha);
				
		if(!empty ($enviar_a))
		{
			// Genera array para todo los alumnos de la convocatoria
			if($enviar_a == 'conv')
			{
				$convocatorias = $mi_correo->obtenerIdConvocatoriaCurso($idcurso);
				$convocatoria = mysqli_fetch_assoc($convocatorias);
				$convocatoriaFinal = $convocatoria['idconvocatoria'];
				
				$res = $mi_correo->alumnosTutorizadosConvocatoria($convocatoriaFinal);
	
	
				//$idalumnos = array();
				$idalumnos = array();
				$cursoAlumno = array();
				$n_registros = mysqli_num_rows($res);
				
				while($fil = mysqli_fetch_assoc($res))
				{
					$idalumnos[]	= $fil['idalumnos'];
					$cursoAlumno[]	= $fil['idcurso'];
				}
				//print_r($idalumnos);
			}
			
			// Genera array para todo los alumnos del curso
			if($enviar_a == 'curso')
			{
				//Obtenmos el id de los alumnos del curso para enviarle el mensaje
				$res = $mi_correo->obtenerAlumnosCurso($idcurso_envio);
				$idalumnos=array();
				$n_registros = mysqli_num_rows($res);
				while($fil = mysqli_fetch_assoc($res))
				{
					$idalumnos[]	= $fil['idalumnos'];
					$cursoAlumno[]	= $idcurso_envio;
				}
			}
		}
		
		$reenvio = false;
		if(isset ($idalumnos))
		{
			mvc::importar('lib/mail/phpmailer.php');
			mvc::importar('lib/mail/mail.php');
				
			$objMail = Mail::obtenerInstancia();
			$rowEntOrg = $mi_correo->obtenerConfigEntidadOrg();
			$rowEntOrg = $rowEntOrg->fetch_object();
		
			// Obtengo el idcurso a traves del id del correo
			$Correo = $mi_correo->obtenerIdCursoPorCorreo($post['idcorreo']);
			$idCurso = mysqli_fetch_assoc($Correo);
			$idc = $idCurso['idcurso'];	

			// Inserto el nuevo correo
			if($mi_correo->insertarCorreo($post['reenvioAsunto'], $post['reenvioMensaje'], $idc, $importante))
			{
				$ultimoId = $mi_correo->obtenerUltimoIdInsertado();
			}
			
			for($i=0;$i<=count($idalumnos)-1;$i++)
			{
				if(isset($cursoAlumno))
				{
					$idcurso_envio = $cursoAlumno[$i];
				}
				$resultadoAlumno = $mi_correo->obtenerUsuarioDestinatario($idalumnos[$i], $idcurso_envio);
				
				if($resultadoAlumno->num_rows == 1)
				{		
					$rowAlumno = mysqli_fetch_assoc($resultadoAlumno);
					
					// Inserto un nuevo registro para el destinatario
					if($mi_correo->insertarDestinatario($idusuario_envio, $idalumnos[$i], $fecha, $ultimoId))
					{
						$reenvio = true;
								
						//Incluimos el mensaje en seguimietno
						if(isset($post['addSeguimiento']) && $post['addSeguimiento'] == 1)
						{
							if(Usuario::checkAlumno($idalumnos[$i]))
							{
								$ultimoIdDestinatario = $mi_correo->obtenerUltimoIdInsertado();
								$idMatricula = Usuario::getIdMatriculaDatos($idalumnos[$i], $idc);
								$mi_correo->incluirCorreoSeguimiento($post['reenvioAsunto'], $fecha, $idMatricula, $ultimoIdDestinatario);
							}
						}
						
						// envia una notificacion de email
						/*
						if(isset($rowAlumno['notifi_correo']) && $rowAlumno['notifi_correo'] == '1')
						{
							$enviado = $objMail->enviarNotificacion($rowEntOrg->email_notificaciones, 'Campus Aula Interactiva - Aula_SMART', $rowAlumno['email'],
												$rowAlumno['nombre'], Usuario::getNameUser(Usuario::getIdUser(true)), $post['reenvioMensaje'], $post['reenvioAsunto']);
						}
						*/
					}
				}
			} //END FOR
			
			$n_archivo = 0;

			// adjuntos por subida de archivos
			if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]))
			{
				if(!file_exists(PATH_ROOT . "archivos/correo/".$ultimoId))
				{
					mkdir(PATH_ROOT . "archivos/correo/".$ultimoId);	
				}
				$directorio = 'archivos/correo/'.$ultimoId.'/';
				$tamano_total=0;
				$n_archivo = 0;
				foreach ($_FILES['archivo']['error'] as $key => $error) 
				{		
					if ($error == UPLOAD_ERR_OK) 
					{	
						$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
						if($archivosNoValidos)
						{
							//Genero el nombre y enlace del archivo		
							$nombre_fichero = $_FILES["archivo"]["name"][$key];
							$enlace_fichero = $directorio . Texto::quitarTildes($n_archivo . "_" . $nombre_fichero);
							
							//copio el archivo
						   if(move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero))
						   {
						   		//Inserto el registro del archivo adjunto copiado en la base de datos						   
						   		if($mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoId))
						   		{
						   			//Obtengo el ultimo id insertado y realacion el fichero adjunto con el correo
						   			$maxfa = $mi_correo->obtenerUltimoIdInsertado();
						   			if($mi_correo->adjuntarArchivoCorreo($ultimoId, $maxfa))
						   			{
							    		$reenvio = true;
						   			}
						   		}		
						   }
						}
					}
					$n_archivo++;
				} // END FOREACH
			} // END IF ARCHIVOS
			
			// archivos adjuntos de los borradores
			if(isset($post['adjuntosBorrador']) && is_array($post['adjuntosBorrador'])
				&& count($post['adjuntosBorrador']) > 0)
			{
				if(!file_exists(PATH_ROOT . "archivos/correo/". $ultimoId))
				{
					mkdir(PATH_ROOT . "archivos/correo/". $ultimoId);	
				}
					
				foreach($post['adjuntosBorrador'] as $idFichero)
				{
					//JOSE comprobar permisos de usuario
					$filaFicheroAdjunto = $mi_correo->obtenerUnAdjuntoPorUsuario($idusuario_envio, $idFichero);
	
					if($filaFicheroAdjunto->num_rows == 1)
					{
						$filaFicheroAdjunto = $filaFicheroAdjunto->fetch_object();
						
						if(file_exists(PATH_ROOT . $filaFicheroAdjunto->enlace_fichero))
						{
						    if( copy(PATH_ROOT . $filaFicheroAdjunto->enlace_fichero,
						    	PATH_ROOT . "archivos/correo/". $ultimoId . '/' . Texto::quitarTildes($n_archivo . "_" . $filaFicheroAdjunto->nombre_fichero)
						    ))
						    {
						    	if($mi_correo->insertarFicheroAdjunto($filaFicheroAdjunto->nombre_fichero, 'archivos/correo/'.$ultimoId.'/'.$n_archivo . "_" . $filaFicheroAdjunto->nombre_fichero, $ultimoId))
						    	{
						    		$maxfa = $mi_correo->obtenerUltimoIdInsertado();
						    		if($mi_correo->adjuntarArchivoCorreo($ultimoId, $maxfa))
						    		{
						    			$reenvio = true;
						    		}
						    	}	
	
							}
						}
					}
					$n_archivo++;
				}
			} //END IF ADJUNTOS BORRADOR
		} //END IF ALUMNOS
						
		if($reenvio)
		{
			$notificacion = 'Su mensaje se ha enviado correctamente';
			if(isset($archivosNoValidos) && (!$archivosNoValidos)) { $notificacion.= ' , algunos archivos no fueron adjuntados por su extension';} 
			Alerta::guardarMensajeInfo($notificacion);
			Url::redirect('correos/bandeja_entrada');
		}
	}
	else
	{
		Alerta::guardarMensajeInfo('Debes seleccionar un destinatario');
	}
	
	
	//Si pulsamos el boton de "Guardar como borrador" en enviar mensaje
	if(isset($post['actionBoton']) && $post['actionBoton'] == 'borrador')
	{
		$fecha = Fecha::fechaActual();
		
		$mi_correo->insertarCorreo($post['reenvioAsunto'], $post['reenvioMensaje'], $idcurso_envio, $importante, '1');
		$ultimoIdCorreo = $mi_correo->obtenerUltimoIdInsertado();
		
		$n_archivo = 0;
		
		if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]) && count($_FILES['archivo']) > 0)
		{
			if(!file_exists(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo))
			{
				mkdir(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo);	
			}
			$directorio = 'archivos/correo/'.$ultimoIdCorreo.'/';
			$tamano_total=0;
		
			foreach ($_FILES['archivo']['error'] as $key => $error) 
			{		
				if ($error == UPLOAD_ERR_OK) 
				{	
					$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
					if($_FILES['archivo']['type'][$key] != 'application/octet-stream' && $_FILES['archivo']['type'][$key] != 'application/x-msdownload')
					{
						//echo $error_codes[$error];		
						$nombre_fichero = $_FILES["archivo"]["name"][$key];
						$enlace_fichero = $directorio .Texto::quitarTildes($n_archivo . "_" . $nombre_fichero);
					    move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero) or die("Ocurrio un problema al intentar subir el archivo.");
						$mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoIdCorreo);
						$id_ultimoarchivo = $mi_correo->obtenerUltimoIdInsertado();
		
						$mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $id_ultimoarchivo);
					}
					else
					{
						Alerta::mostrarMensajeInfo('El archivo adjunto no es v&aacute;lido');
					}
				}
				$n_archivo++;
			}
		}
		
		// archivos adjuntos de los borradores
		if(isset($post['adjuntosBorrador']) && is_array($post['adjuntosBorrador'])
			&& count($post['adjuntosBorrador']) > 0)
		{
			if(!file_exists(PATH_ROOT . "archivos/correo/". $ultimoIdCorreo))
			{
				mkdir(PATH_ROOT . "archivos/correo/". $ultimoIdCorreo);	
			}
				
			foreach($post['adjuntosBorrador'] as $idFichero)
			{
				//JOSE comprobar permisos de usuario
				$filaFicheroAdjunto = $mi_correo->obtenerUnAdjuntoPorUsuario($idusuario_envio, $idFichero);

				if($filaFicheroAdjunto->num_rows == 1)
				{
					$filaFicheroAdjunto = $filaFicheroAdjunto->fetch_object();
				
					if(file_exists(PATH_ROOT . $filaFicheroAdjunto->enlace_fichero))
					{
					    if( copy(PATH_ROOT . $filaFicheroAdjunto->enlace_fichero,
					    	PATH_ROOT . "archivos/correo/". $ultimoIdCorreo . '/' . Texto::quitarTildes($n_archivo . "_" . $filaFicheroAdjunto->nombre_fichero)
					    ))
					    {
					    	if($mi_correo->insertarFicheroAdjunto($filaFicheroAdjunto->nombre_fichero, 'archivos/correo/'.$ultimoIdCorreo.'/'.$n_archivo . "_" . $filaFicheroAdjunto->nombre_fichero, $ultimoIdCorreo))
					    	{
					    		$maxfa = $mi_correo->obtenerUltimoIdInsertado();
					    		$mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $maxfa);
					    	}
					    }
					}
				}
				
				$n_archivo++;
			}
		} //END IF ADJUNTOS BORRADOR
			
		//Insertamos el destinatario
		if($mi_correo->insertarDestinatario($idusuario_envio, NULL, $fecha, $ultimoIdCorreo))
		{
			Alerta::guardarMensajeInfo('El mensaje ha sido guardado como borrador');
			Url::redirect('correos/bandeja_entrada');
		}
	}
	
}
// Datos de detalle para el correo
if(is_numeric($get['idcorreo']))
{
	$idcorreo = $get['idcorreo'];
		
	$mi_correo->actualizar_leido($idcorreo);	
	
	$detalle_mensaje = $mi_correo->mostrar_mensaje($idcorreo);
	
	$array = Usuario::esAlumno($detalle_mensaje['remitente']);	

	if($array[0] != 'alumno')
	{
		if(!empty($array[1]))
		{
			$sql = "SELECT * from rrhh where idrrhh = ".$array[1];
			$result = mysqli_query($con,$sql);
			$fil = mysqli_fetch_assoc($result);
			$de = $fil['nombrec'];
		}
	}
	else 
	{
		if(!empty($array[1]))
		{
			$sql = "SELECT * from alumnos where idalumnos = ".$array[1];
			$result = $mi_curso->consultaSql($sql);
			$fil = mysqli_fetch_assoc($result);
			$de = $fil['nombre']." ".$fil['apellidos'];
		}
	}
		
		$destin = $mi_correo->obtenerDestinatario($detalle_mensaje['idcorreos']);
		$los_destinatarios='';
		
		while($fila = mysqli_fetch_assoc($destin))
		{
			if(!empty($fila['destinatarios']))
			{
				$dest = explode("_",$fila['destinatarios']);
				if($dest[0] == 't')
				{
					$sql2 = "SELECT * from rrhh where idrrhh = ".$dest[1];
					$resultado2 = mysqli_query($con,$sql2);
					$f2 = mysqli_fetch_assoc($resultado2);
					$los_destinatarios.= $f2['nombrec'].", ";
				}
				else 
				{
					$sql3 = "SELECT * from alumnos where idalumnos = ".$dest[0];
					//echo $sql3;
					$resultado3 = mysqli_query($con,$sql3);
					
					$los_destinatarios = '';
					if(!empty($resultado3) && mysqli_num_rows($resultado3) > 0)
					{
						$f3 = mysqli_fetch_assoc($resultado3);
						$los_destinatarios.= $f3['nombre']." ".$f3['apellidos'].", ";
					}
				}
			}
		}
}
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
