<?php
require_once("basic.php");

$get = Peticion::obtenerGet();

if(isset($get['idcarpeta']) && is_numeric($get['idcarpeta']))
{
	$objModelo = new Gestor_correos();
					
	$rowCarpetaEdit = $objModelo->obtenerUnaCarpeta($get['idcarpeta'], Usuario::getIdUser(true));
	if($rowCarpetaEdit->num_rows == 1)
	{
		$rowCarpetaEdit = $rowCarpetaEdit->fetch_object();
		if(Peticion::isPost())
		{
			ob_clean();
			header('Content-Type: application/json');
			
			$json = array('mensaje' => null, 'url' => null);
			
			$post = Peticion::obtenerPost();
			if(isset($post['nombre']))
			{
				if(!empty($post['nombre']))
				{
					$rowCarpeta = $objModelo->obtenerUnaCarpetaPorNombre($post['nombre'], Usuario::getIdUser(true));
					if($rowCarpeta->num_rows == 0)
					{
						if($objModelo->editarCarpeta($get['idcarpeta'], $post['nombre']))
						{
							$json['mensaje'] = 'Se ha editado la carpeta';
							$json['url'] = 'correos/bandeja_entrada';
						}
					}
					else
					{
						$json['mensaje'] = 'Ya existe una carpeta con ese nombre';
					}
				}
				else
				{
					$json['mensaje'] = 'Introduce el nombre de la carpeta';
				}
			}
			
			echo json_encode($json);
			exit;
		}
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar_carpeta');
	}
	else
	{
		echo 'No se pudo encontrar la carpeta';
	}
}