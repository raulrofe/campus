<?php
$get = Peticion::obtenerGet();

require 'configMimes.php';

$idusuario = Usuario::getIdUser(true);

$modeloCorreo = new Gestor_correos();

$rowAdjunto = $modeloCorreo->obtenerUnAdjuntoPorUsuario($idusuario, $get['idfichero']);
if($rowAdjunto->num_rows == 1)
{
	$rowAdjunto = $rowAdjunto->fetch_object();
	
	$arrayFichero = explode('/', $rowAdjunto->enlace_fichero);

	$nombreFichero = PATH_ROOT . 'archivos/correo/' . $rowAdjunto->idcorreos . '/'  . Texto::clearFilename($arrayFichero[3]);
	$extension = strtolower(Fichero::obtenerExtension($rowAdjunto->nombre_fichero));
	
	if(file_exists($nombreFichero) && isset($mimes[$extension]))
	{
		//echo Fichero::obtenerMime($rowAdjunto->nombre_fichero);exit;
		header("Content-disposition: attachment; filename=" . Texto::quitarTildes($rowAdjunto->nombre_fichero));
		header("Content-type: application/octet-stream");
		readfile($nombreFichero);
	}
	else
	{
		Url::lanzar404();
	}
}
else
{
	Url::lanzar404();
}