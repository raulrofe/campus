<?php
// SIN USO
exit;

if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

require_once PATH_ROOT . 'lib/plantilla.php';

$json = array('asunto' => '', 'mensaje' => '', 'adjuntos' => array());

$get = Peticion::obtenerGet();

if(isset($get['idplantilla']) && is_numeric($get['idplantilla']) && Usuario::compareProfile('tutor'))
{
	$objGestCorreo = new Gestor_correos();
	$rowPlantilla = $objGestCorreo->obtenerPlantillas($get['idplantilla']);
	if($rowPlantilla->num_rows == 1)
	{
		$rowPlantilla = $rowPlantilla->fetch_object();
		
		$json['asunto'] = $rowPlantilla->asunto;
		
		$objPlantilla = new Plantilla();
		$objPlantilla->set(Usuario::getIdCurso(), Usuario::getIdUser());
		$objPlantilla->cargarPlantilla($rowPlantilla->contenido);
		$json['mensaje'] = $objPlantilla->reemplazar(array('curso'));
		$objPlantilla->limpiar();
		
		$arrayAdjuntos = explode(',', $rowPlantilla->adjuntos);
		foreach($arrayAdjuntos as $item)
		{
			$json['adjuntos'][] = $item;
		}
	}
}

echo json_encode($json);