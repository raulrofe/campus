<?php
require_once("basic.php");
if(is_numeric($get['idcorreo']))
{
	$idcorreo = $get['idcorreo'];
	
	if(isset($post['hacer']) && $post['hacer'] == 'eliminar')
	{
		if(isset($post['menu']))
		{
			switch ($post['menu'])
			{
				case 'bandeja_entrada' :
					if($mi_correo->enviar_papelera(array($post['idmensaje_correo']), 'entrada'))
					{
						Alerta::guardarMensajeInfo('El mensaje se ha enviado a la carpeta elementos eliminados');
					}
					Url::redirect('correos/bandeja_entrada');
					break;

				case 'bandeja_salida' :
					if($mi_correo->enviar_papelera(array($post['idmensaje_correo']), 'salida'))
					{
						Alerta::guardarMensajeInfo('El mensaje se ha enviado a la carpeta elementos eliminados');
					}
					Url::redirect('correos/bandeja_salida');
					break;
				
				case 'borrador' :
					if($mi_correo->enviar_papelera(array($post['idmensaje_correo']), 'borrador'))
					{
						Alerta::guardarMensajeInfo('El mensaje se ha enviado a la carpeta elementos eliminados');
					}
					Url::redirect('correos/borrador');
					break;
					
				case 'papelera' :
					if(isset($post['idmensaje_correo']) && is_numeric($post['idmensaje_correo']))
					{
						$mi_correo->eliminar_definitivamente(array($post['idmensaje_correo']));		
						Alerta::guardarMensajeInfo('El mensaje se ha eliminado definiticamente');
					}
									
					Url::redirect('correos/papelera');
					break;
			}
		}
	}
	else if(isset($post['hacer']) and $post['hacer'] == 'archivar')
	{
		if(isset($post['idmensaje_correo']))
		{
			$detalleMensaje = $mi_correo->mostrar_mensaje($post['idmensaje_correo']);
			$idCarpeta = $detalleMensaje['idcurso'];
			
			if($perfil != 'alumno') $idDestinatario = 't_'.$_SESSION['idusuario'];
				else $idDestinatario = $_SESSION['idusuario']; 
				
			$mi_correo->eliminarCorreoDeCarpetaPersonal($post['idmensaje_correo']);
			$mi_correo->organizar_mail($idCarpeta, $idDestinatario, $post['idmensaje_correo']);
			
			Alerta::guardarMensajeInfo('El mensaje/s ha sido autoarchivado en la carpeta correspondiente');
			Url::redirect($post['url-referer']);
		}
		else
		{
			Alerta::mostrarMensajeInfo('Debes seleccionar al menos un mensaje para poder moverlo');
		}
	}
	else if(isset($post['hacer']) and $post['hacer'] == 'desarchivar')
	{
		if(isset($post['idmensaje_correo']))
		{
			$mi_correo->eliminarCorreoDeCarpetaCurso($post['idmensaje_correo']);
			$mi_correo->eliminarCorreoDeCarpetaPersonal($post['idmensaje_correo']);
				
			Alerta::guardarMensajeInfo('El mensaje/s ha sido desarchivado');
			Url::redirect($post['url-referer']);
		}
		else
		{
			Alerta::mostrarMensajeInfo('Debes seleccionar al menos un mensaje');
		}
	}
	// asigna un correo/os a una carpeta personal
	else if(isset($post['idcarpeta_personal']) && is_numeric($post['idcarpeta_personal']))
	{
		if(isset($post['idmensaje_correo']) && is_numeric($post['idmensaje_correo']))
		{
			$resultCarpPersonales = $mi_correo->obtenerUnaCarpeta($post['idcarpeta_personal'], $idusuario);
			if($resultCarpPersonales->num_rows > 0)
			{
				$resultUnCorreo = $mi_correo->mostrar_mensaje($post['idcarpeta_personal']);
				if(!empty($resultUnCorreo))
				{
					$mi_correo->eliminarCorreoDeCarpetaPersonal($post['idcarpeta_personal']);
				
					if($mi_correo->eliminarCorreoDeCarpetaCurso($post['idcarpeta_personal'])
						&& $mi_correo->asignarCarpetaACorreo($post['idcarpeta_personal'], $post['idcarpeta_personal']))
					{
						Alerta::guardarMensajeInfo('El mensaje ha sido archivado en la carpeta');
					}
				}
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('Selecciona algun mensaje');
		}
	}
	else if(isset($post['hacer']) && $post['hacer'] = 'restaurar')
	{
		if($mi_correo->restaurarMensaje(array($post['idmensaje_correo'])))
		{
			Alerta::guardarMensajeInfo('El mensaje se ha restaurado');
		}
		
		Url::redirect('correos/papelera');
	}
		
	$mi_correo->actualizar_leido($idcorreo);	
	
	$detalle_mensaje = $mi_correo->mostrar_mensaje($idcorreo);
		
	$array = Usuario::esAlumno($detalle_mensaje['remitente']);	

	if($array[0] != 'alumno')
	{
		if(!empty($array[1]))
		{
			$sql = "SELECT * from rrhh where idrrhh = ".$array[1];
			$result = mysqli_query($con,$sql);
			$fil = mysqli_fetch_assoc($result);
			$de = $fil['nombrec'];
		}
	}
	else
	{
		if(!empty($array[1]))
		{
			$sql = "SELECT * from alumnos where idalumnos = ".$array[1];
			$result = $mi_curso->consultaSql($sql);
			$fil = mysqli_fetch_assoc($result);
			$de = $fil['nombre']." ".$fil['apellidos'];
		}
	}

		$sql = "SELECT destinatarios, fecha_leido from destinatarios where idcorreos = ".$detalle_mensaje['idcorreos']." and fecha = '".$detalle_mensaje['fecha']."'";
		//echo $sql;die();
		$resultado = mysqli_query($con,$sql);
		$los_destinatarios='';
		$leidoDestinatario = array();
		$nombreDestinatario = array();
		
		if(!empty($resultado))
		{
			while($fila = mysqli_fetch_assoc($resultado))
			{
				$dest = explode("_",$fila['destinatarios']);
				if($dest[0] == 't')
				{
					$sql2 = "SELECT * from rrhh where idrrhh = '".$dest[1] . "'";
					$resultado2 = mysqli_query($con,$sql2);
					if($resultado2->num_rows > 0)
					{
						$f2 = mysqli_fetch_assoc($resultado2);
						$los_destinatarios.= $f2['nombrec'].", ";
						$leidoDestinatario[] = $fila['fecha_leido'];
						$nombreDestinatario[] = $f2['nombrec'];
					}				
				}
				else 
				{
					$sql3 = "SELECT * from alumnos where idalumnos = '".$dest[0] . "'";
					$resultado3 = mysqli_query($con,$sql3);
					if($resultado3->num_rows > 0)
					{
						$f3 = mysqli_fetch_assoc($resultado3);
						$los_destinatarios.= $f3['nombre']." ".$f3['apellidos'].", ";
						$leidoDestinatario[] = $fila['fecha_leido'];
						$nombreDestinatario[] = $f3['nombre'] . " " . $f3['apellidos'];
					}
				}
			}
		}
}
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');
