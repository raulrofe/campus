<?php
require_once("basic.php");

header('Content-type: text/json');

$get = Peticion::obtenerGet();
$json = array();

//Cargo el controlador de foro para el paginado
mvc::cargarModuloSoporte('foro');

if(isset($get['word']) && strlen($get['word']) >= 3)
{
	// obtiene los mensajes con paginado
	$registros_correos_count = $mi_correo->textBuscar($get['word']);
	$maxElementsPaging = 20;
	$maxPaging = $registros_correos_count->num_rows;
	$maxPaging = ceil($maxPaging / $maxElementsPaging);
	$elementsIni = ($numPagina - 1) * $maxElementsPaging;
	
	$registros_correos = $mi_correo->textBuscar($get['word'], $elementsIni, $maxElementsPaging);
	if($registros_correos->num_rows > 0)
	{
		$json['lugar'] = 'bandeja_entrada';
		$json['mensajes'] = array();
		$json['numMensajes'] = $registros_correos->num_rows;
		$json['paginado'] = Paginado::crear($numPagina, $maxPaging, 'correos/bandeja_entrada?pagina=', '&textBuscar=' . urlencode($get['word']));
		
		while($mensaje = $registros_correos->fetch_object())
		{
			// destinatarios
			$sql_tutor = "SELECT * from correos C, destinatarios D
				where C.idcorreos = ".$mensaje->idcorreos." and C.idcorreos = D.idcorreos";
			$resultado_tutor = $mi_correo->consultaSql($sql_tutor);
			$f_tutor = mysqli_fetch_assoc($resultado_tutor);
			$array = Usuario::esAlumno($f_tutor['remitente']);		
		
			if($array[0] != 'alumno')
			{
				$sql = "SELECT * from rrhh where idrrhh = ".$array[1];
				$result = mysqli_query($con,$sql);
				$fil = mysqli_fetch_assoc($result);
				$de = $fil['nombrec'];
			}
			else 
			{
				$sql = "SELECT * from alumnos where borrado = 0 AND idalumnos = ".$array[1];
				$result = mysqli_query($con,$sql);
				$fil = mysqli_fetch_assoc($result);
				$de = $fil['nombre']." ".$fil['apellidos'];
			}
		
			//comprobamos si tiene archivos adjuntos
			$sql_adjuntos = "SELECT * from adjuntos where idcorreos = ".$mensaje->idcorreos;
			$resultado_adjuntos = mysqli_query($con,$sql_adjuntos);
			$num_adjuntos = mysqli_num_rows($resultado_adjuntos);
				
			$json['mensajes'][] = array(
				'id'            => $mensaje->iddestinatarios,
				'asunto'        => str_ireplace($get['word'],'<span class="highlight">' . $get['word'] . '</span>', Texto::textoPlano($mensaje->asunto)),
				//'mensaje'       => str_ireplace($get['word'],'<span class="highlight">' . $get['word'] . '</span>', Texto::textoPlano($mensaje->mensajes)),
				'importante'    => $mensaje->importante,
				'leido'         => $mensaje->leido,
				'n_adjuntos'    => $num_adjuntos,
				'destinatarios' => str_ireplace($get['word'],'<span class="highlight">' . $get['word'] . '</span>', Texto::textoPlano($de)),
				'fecha'         => Fecha::obtenerFechaFormateada($mensaje->fecha)
			);
		}
	}
}

echo json_encode($json);