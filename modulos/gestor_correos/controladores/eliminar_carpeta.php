<?php
require_once("basic.php");

ob_clean();
header('Content-Type: application/json');
			
$json = array('mensaje' => null, 'url' => null);

$get = Peticion::obtenerGet();

if(isset($get['idcarpeta']) && is_numeric($get['idcarpeta']))
{
	$objModelo = new Gestor_correos();
					
	$rowCarpetaEdit = $objModelo->obtenerUnaCarpeta($get['idcarpeta'], Usuario::getIdUser(true));
	if($rowCarpetaEdit->num_rows == 1)
	{
		$rowCarpetaEdit = $rowCarpetaEdit->fetch_object();
		
		if($objModelo->eliminarCarpeta($get['idcarpeta']))
		{
			$json['mensaje'] = 'Se ha eliminado la carpeta';
			$json['url'] = 'correos/bandeja_entrada';
		}
	}
	else
	{
		$json['mensaje'] = 'No se pudo eliminar la carpeta que deseas eliminar';
	}
}

echo json_encode($json);
exit;