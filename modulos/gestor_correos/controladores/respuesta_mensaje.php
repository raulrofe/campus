<?php
require_once("basic.php");

//Obtenemos el valor del usuario
$idUsuario = Usuario::getIdUser(true);
if(isset($post['idCurso']))
{
	$idCurso = $post['idCurso'];
}
else
{
	$idcurso = Usuario::getIdCurso();
}

$mi_usuario = new Usuario();

require PATH_ROOT . 'lib/mail/phpmailer.php';
require PATH_ROOT . 'lib/mail/mail.php';

if(isset($get['idcorreo']) and is_numeric($get['idcorreo']))
{
	$idcorreo = $get['idcorreo'];
	$detalle_mensaje = $mi_correo->mostrar_mensaje($idcorreo);
	
	$nombreRemitente = Usuario::getNameUser($detalle_mensaje['remitente']);
	$nombreDestinatario = Usuario::getNameUser($detalle_mensaje['destinatarios']); 
	
	if(Peticion::isPost())
	{
		$fecha = Fecha::fechaActual();
		$fecha_format = Fecha::obtenerFechaFormateada($fecha);
		
		//Obtenmos id del remitente
		if($_SESSION['perfil'] != 'alumno') 
		{
			$idremitente = $mi_usuario->getIdUser(true);
		}
		else 
		{
			$idremitente = $mi_usuario->getIdUser();
		}
		
		// Obtengo el valor de importante
		if(isset($post['importante']) and is_numeric($post['importante']))
		{
			$importante = $post['importante'];
		}
		else 
		{
			$importante = 0;	
		}
		
		if(isset($post['actionBoton']) && $post['actionBoton'] == 'Enviar')
		{
			$result = $mi_correo->responder_mensaje($post['asunto'],$post['mensaje'],$post['iddestinatario'], $idremitente, $idcorreo, $importante, $post['idCurso']);
			if($result['rs'])
			{
				//Incluimos el mensaje en seguimietno
				if(isset($post['addSeguimiento']) && $post['addSeguimiento'] == 1)
				{
					if(Usuario::checkAlumno($post['iddestinatario']))
					{
						$idMatricula = Usuario::getIdMatriculaDatos($post['iddestinatario'], $post['idCurso']);
						$mi_correo->incluirCorreoSeguimiento($post['asunto'], $fecha, $idMatricula, $result['iddestinatario']);	
					}
				}
				
				// --------------- START - UPLOAD FILES ---------------- ////
				if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]))
				{
					if(!file_exists(PATH_ROOT . "archivos/correo/".$result['id']))
					{
						mkdir(PATH_ROOT . "archivos/correo/".$result['id']);	
					}
					$directorio = 'archivos/correo/'.$result['id'].'/';
					$tamano_total=0;
					$n_archivo = 0;
					foreach ($_FILES['archivo']['error'] as $key => $error) 
					{		
						if($error == UPLOAD_ERR_OK) 
						{
							//echo $error_codes[$error];		
							$nombre_fichero = $_FILES["archivo"]["name"][$key];
							$enlace_fichero = $directorio . Texto::quitarTildes($directorio. $n_archivo . "_ " . $nombre_fichero);
						    if(move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero))
						    {
						    	if($mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $result['id']))
						    	{
						    		$id_ultimoarchivo = $mi_correo->obtenerUltimoIdInsertado();
						    		if($mi_correo->adjuntarArchivoCorreo($result['id'], $id_ultimoarchivo))
						    		{
						    			Alerta::guardarMensajeInfo('Respuesta enviada');
						    		}
						    		else
						    		{
						    			Alerta::guardarMensajeInfo('Error al responder');
						    		}
						    	}
						    }					
						}
						$n_archivo++;
					} // END FOREACH
				}
				// --------------- END - UPLOAD FILES ---------------- ////
				
				// --------------- START - SEND NOTIFY --------------- ////
				
				$rowEntOrg = $mi_correo->obtenerConfigEntidadOrg();
				$rowEntOrg = $rowEntOrg->fetch_object();
				
				$resultadoAlumno = $mi_correo->obtenerUsuarioDestinatario($post['iddestinatario'], $post['idCurso']);
				if($resultadoAlumno->num_rows == 1)
				{
					$rowAlumno = mysqli_fetch_assoc($resultadoAlumno);
					
					// envia una notificacion de email
					if(isset($rowAlumno['notifi_correo']) && $rowAlumno['notifi_correo'] == '1')
					{
						$objMail = Mail::obtenerInstancia();
						$enviado = $objMail->enviarNotificacion($rowEntOrg->email_notificaciones, 'Campus Aula Interactiva - Aula_SMART', $rowAlumno['email'],
							$rowAlumno['nombre'], Usuario::getNameUser(Usuario::getIdUser(true)), $post['mensaje'], $post['asunto']);
						}
				}
				// --------------- END - SEND NOTIFY --------------- ////
						
				Alerta::guardarMensajeInfo('Respuesta enviada');
				Url::redirect('correos/bandeja_entrada');
				//url::redirect('correos/bandeja_entrada');
						
			} //END IF SI NO ESTA VACIO $ENVIAR_A Y $IDALUMNOS
		}
		//Si pulsamos el boton de "Guardar como borrador" en enviar mensaje
		else if(isset($post['actionBoton']) && $post['actionBoton'] == 'borrador')
		{
			$mi_correo->insertarCorreo($post['asunto'], $post['mensaje'], $post['idCurso'], $importante, '1');
			$ultimoIdCorreo = $mi_correo->obtenerUltimoIdInsertado();
			
			$n_archivo = 0;
			
			if(isset($_FILES['archivo']['name'][0]) && !empty($_FILES['archivo']['name'][0]) && count($_FILES['archivo']) > 0)
			{
				if(!file_exists(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo))
				{
					mkdir(PATH_ROOT . "archivos/correo/".$ultimoIdCorreo);	
				}
				$directorio = 'archivos/correo/'.$ultimoIdCorreo.'/';
				$tamano_total=0;
			
				foreach ($_FILES['archivo']['error'] as $key => $error) 
				{		
					if ($error == UPLOAD_ERR_OK) 
					{	
						$archivosNoValidos = Fichero::validarTipos($_FILES['archivo']['name'][$key], $_FILES['archivo']['type'][$key], $extensionesEmail);
						if($_FILES['archivo']['type'][$key] != 'application/octet-stream' && $_FILES['archivo']['type'][$key] != 'application/x-msdownload')
						{
							//echo $error_codes[$error];		
							$nombre_fichero = $_FILES["archivo"]["name"][$key];
							$enlace_fichero = $directorio . Texto::quitarTildes($n_archivo . "_" . $nombre_fichero);
						    move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$enlace_fichero) or die("Ocurrio un problema al intentar subir el archivo.");
							$mi_correo->insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoIdCorreo);
							$id_ultimoarchivo = $mi_correo->obtenerUltimoIdInsertado();
			
							$mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $id_ultimoarchivo);
						}
						else
						{
							Alerta::mostrarMensajeInfo('El archivo adjunto no es v&aacute;lido');
						}
					}
					$n_archivo++;
				} //END FOREACH
			}
				
			//Insertamos el destinatario
			if($mi_correo->insertarDestinatario($idremitente, NULL, $fecha, $ultimoIdCorreo))
			{
				Alerta::guardarMensajeInfo('El mensaje ha sido guardado como borrador');	
				Url::redirect('correos/bandeja_entrada');
			}
		}
	}
		
} //END IF EXISTE IDCORREO Y ES NUMERICO
	
//var_dump($detalle_mensaje);

$esRespuesta = explode(':',$detalle_mensaje['asunto']);

if($esRespuesta[0] == 'Re') {$elAsunto = $detalle_mensaje['asunto'];}
else {$elAsunto = 'Re: '.$detalle_mensaje['asunto'];}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'correos');