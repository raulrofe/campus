<?php
require_once("basic.php");

if(Peticion::isPost())
{
	ob_clean();
	header('Content-Type: application/json');
	
	$json = array('mensaje' => null, 'url' => null);
	
	$post = Peticion::obtenerPost();
	if(isset($post['nombre']))
	{
		if(!empty($post['nombre']))
		{
			$objModelo = new Gestor_correos();
			
			$rowCarpeta = $objModelo->obtenerUnaCarpetaPorNombre($post['nombre'], Usuario::getIdUser(true));
			if($rowCarpeta->num_rows == 0)
			{
				if($objModelo->insertarCarpeta($post['nombre'], Usuario::getIdUser(true)))
				{
					$json['key'] 		= 'correo.subcarpeta';
					$json['mensaje'] 	= 'Se ha creado la carpeta';
					$json['url'] 		= 'correos/bandeja_entrada';
				}
			}
			else
			{
				$json['mensaje'] = 'Ya existe una carpeta con ese nombre';
			}
		}
		else
		{
			$json['mensaje'] = 'Introduce el nombre de la carpeta';
		}
	}
	

	echo json_encode($json);
	exit;
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nueva_carpeta');