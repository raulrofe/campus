<form name='frm_reenvio_1' method='post' action=''>
	<?php if (Usuario::compareProfile(array('tutor', 'coordinador'))): ?>
		<div class='tituloBloques' data-translate-html="general.cursos">
			Cursos
		</div>

		<div class='selectorCurso'>
			<?php
			$allCurso = $mi_curso->cursos_usuario($_SESSION['idusuario'], true);
			echo "<select name='idSelectCurso' onchange='this.form.submit();'>";
			while ($selectCurso = mysqli_fetch_assoc($allCurso)) {

				if ($idSelectCurso == $selectCurso['idcurso'])
					echo "<option value='" . $selectCurso['idcurso'] . "' selected>" . $selectCurso['titulo'] . "</option>";
				else
					echo "<option value='" . $selectCurso['idcurso'] . "'>" . $selectCurso['titulo'] . "</option>";
			}
			echo "</select>";
			?>
		</div>
<?php endif; ?>
</form>

<?php
if ($_SESSION['perfil'] == 'tutor') {
	$tutorExtra = $mi_correo->obtenerTutor2($_SESSION['idcurso']);
	$turtor2 = mysqli_fetch_assoc($tutorExtra);
}
?>

<form name='frm_reenvio' id="frm_reenvio" method='post' action='' enctype="multipart/form-data">
<?php if ($_SESSION['perfil'] != 'alumno') {
	include("menu_masivo.php");
} ?>

	<input type="hidden" name="idSelectCurso" value="<?php echo $idSelectCurso ?>" />

	<div class='tituloBloques' data-translate-html="correo.destinatarios">
		Destinatarios
	</div>

	<div class='menvio'>
		<div>
			<div id="frm_gestor_reenvio_destinatarios">
				<?php
				while ($datos_staff = mysqli_fetch_assoc($staff_curso)) {
					if ($_SESSION['idusuario'] != $datos_staff['idrrhh']) {
						echo "<div><input name='idalumnos[]' data-type='rrhh' id='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "' type='checkbox' value='t_" . $datos_staff['idrrhh'] . "' />" .
						"<label for='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "'>" . Texto::textoPlano($datos_staff['nombrec']) . " (" . ucwords($datos_staff['perfil']) . "/a)</label></div>";
					}
				}

				if ($_SESSION['perfil'] == 'tutor') {
					echo "<div><input name='idalumnos[]' data-type='rrhh' id='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "' type='checkbox' value='t_" . $turtor2['idrrhh'] . "' />" .
					"<label for='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "'>" . Texto::textoPlano($turtor2['nombrec']) . " (<span data-translate-html='perfiles.tutor'>Tutor/a</span>)</label></div>";
				}

				while ($datos_alumno = mysqli_fetch_assoc($registro_alumnos)) {
					if ($remitente != $datos_alumno['idalumnos']) {
						if (isset($idremitente) && $idremitente == $datos_alumno['idalumnos']) {
							echo "<div><input name='idalumnos[]' data-type='alumno' id='frm_correo_reenvio_t_" . $datos_alumno['idalumnos'] . "' type='checkbox' value='" . $datos_alumno['idalumnos'] . "' selected>" .
							"<label for='frm_correo_reenvio_t_" . $datos_alumno['idalumnos'] . "'>" . Texto::textoPlano($datos_alumno['apellidos']) . " " . Texto::textoPlano($datos_alumno['nombre']) . " (<span data-translate-html='perfiles.alumno'>Alumno/a</span>)</label></div>";
						} else {
							echo "<div><input name='idalumnos[]' data-type='alumno' id='frm_correo_reenvio_t_" . $datos_alumno['idalumnos'] . "' type='checkbox' value='" . $datos_alumno['idalumnos'] . "'>" .
							"<label for='frm_correo_reenvio_t_" . $datos_alumno['idalumnos'] . "'>" . Texto::textoPlano($datos_alumno['apellidos']) . " " . Texto::textoPlano($datos_alumno['nombre']) . " (<span data-translate-html='perfiles.alumno'>Alumno/a</span>)</label></div>";
						}
					}
				}
				?>
			</div>
		</div>
	</div>
				<?php if ($_SESSION['perfil'] != 'alumno'): ?>
		<div class='menuMasivoTutor'>
			<input type='checkbox' name='enviar_a' value='curso'/>

			<label>
				<span class='alignVertical' data-translate-html="correo.enviar_todos">
					Enviar a todos los alumnos del curso
				</span>
			</label>

			&nbsp;&nbsp;&nbsp;

			<input type='checkbox' name='enviar_a' value='conv' />

			<label>
				<span class='alignVertical' data-translate-html="correo.enviar_todos_conv">
					Enviar a todos los alumnos de la convocatoria
				</span>
			</label>
		</div>
<?php endif; ?>

	<div id='frm_reenvio_1_msg' style="padding-left:15px;"></div>

	<br/>

	<div class='tituloBloques' data-translate-html="formulario.mensaje">
		Mensaje
	</div>

	<br/>

	<div class='api_adjuntar'>
<?php $letra = explode("_", $detalle_mensaje['destinatarios']); ?>
		<div>
			<div class='fleft etiquetaAsunto'>
				<label data-translate-html="correo.asunto">
					Asunto
				</label>
			</div>

			<div class='fleft'>
				<div style='overflow:hidden;'>
					<input type='text' name='reenvioAsunto' value='<?php echo Texto::textoPlano($detalle_mensaje['asunto']); ?>' style='border:1px solid #ccc;margin:0 20px 0 10px;width:95%;'/>
				</div>
			</div>

			<div class="clear"></div>

		</div>

		<div>
			<div class='menuMasivoTutorSin'>
				<div class='upload fleft'>
					<div>
							<input type='file' name='archivo[]' class="multi"/>
					</div>
				</div>

				<div class='fleft importanciaAlta'>
					<input type='hidden' name='importante' value='0' />
					<span>
						<a href="#" onclick="correoBotonImportancia();return false;">
							<img src='imagenes/correos/important.png' class='alignVertical'/>
							<label data-translate-html="correo.importancia">
								Importancia alta
							</label>
						</a>
					</span>
				</div>

<?php if (Usuario::compareProfile(array('tutor'))): ?>
					<input type='hidden' name='addSeguimiento' value='0' />
<?php endif; ?>

			</div>

			<div class="clear"></div>

<?php
if (empty($idcorreo))
	$idcorreo = $detalle_mensaje['idcorreos'];

$sql_adjuntos = "SELECT * from ficheros_adjuntos FA, adjuntos A, destinatarios D
				 where D.iddestinatarios = " . $get['idcorreo'] . "
				 and D.idcorreos = A.idcorreos
				 and A.idficheros_adjuntos = FA.idficheros_adjuntos";
$resultado_adjuntos = mysqli_query($con, $sql_adjuntos);
if (mysqli_num_rows($resultado_adjuntos) != 0) {
	echo"<div style='margin-left:62px;'>";
	while ($f_adjuntos = mysqli_fetch_assoc($resultado_adjuntos)) {
		echo '<div class="MultiFile-label lineaAdjunto burbuja">' .
		'<span title="' . $f_adjuntos['nombre_fichero'] . '" class="MultiFile-title">' .
		'<a href="forzar_descarga/' . $f_adjuntos['idficheros_adjuntos'] . '" target="_blank">' .
		'<img 	lt="" src="imagenes/correos/clip_adjunto.png">' . $f_adjuntos['nombre_fichero'] .
		'</a>' .
		'</span>' .
		'<a href="#" onclick="correoEliminarAdjuntoBorrador(this); return false;" class="MultiFile-remove">' .
		'<img alt="" src="imagenes/correos/delete_adjunto.png">' .
		'</a>' .
		'<input type="hidden" name="adjuntosBorrador[]" value="' . $f_adjuntos['idficheros_adjuntos'] . '" />' .
		'</div>';
	}
	echo "</div>";
}

//else echo "Este mensaje no contiene ficheros adjuntos";
?>
		</div>
		<div class="clear"></div>
		<div class='mensaje_correo'>
			<textarea id="correo_textearea" name='reenvioMensaje' class='mensaje'>
			<?php echo $detalle_mensaje['mensajes']; ?>
			</textarea>
		</div>

		<input type='hidden' name='idcorreo' value='<?php echo $detalle_mensaje['idcorreos']; ?>' />
		<input type='hidden' id='actionBoton' name='actionBoton' value='noGuardar' />
	</div>

	<div id="gestor_correos_botones" style="margin-left:15px;margin-bottom:15px;">

		<button style="width:49%;" type="submit" data-translate-html="formulario.enviar">
			Enviar
		</button>

		<button style="width:48%;" type="button" onclick='$("#actionBoton").val("borrador");document["frm_reenvio"].submit(); return false;' data-translate-html="correo.guardar_borrador">
			Guardar como borrador
		</button>

		<div class="clear"></div>
	</div>

</form>

<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		min_height: 250,
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
	});
</script>
<script type="text/javascript" src="js-gestor_correos-validacionreenvio.js"></script>
<script type="text/javascript" src="js-gestor_correos-nuevo.js"></script>
<script type="text/javascript">$('.menuMasivoTutorSin input.multi').MultiFile();</script>
