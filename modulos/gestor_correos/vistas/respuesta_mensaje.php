<?php //var_dump($detalle_mensaje);   ?>

<form name='frm_respuesta_mensaje' method='post' enctype='multipart/form-data' action=''>
	<div class='tituloBloques' data-translate-html="formulario.mensaje">
		Mensaje
	</div>

	<div id="gestor_correos_botones">
		<ul>
			<li>
				<button type="submit" data-translate-html="formulario.enviar">
					Enviar
				</button>
			</li>

			<li>
				<button type="button" onclick='$("#actionBoton").val("borrador");document["frm_seleccion_correo"].submit(); return false;' data-translate-html="correo.guardar_borrador">
					Guardar como borrador
				</button>
			</li>
		</ul>
		<div class="clear"></div>
	</div>

	<div class='api_adjuntar'>

		<div style="margin-left: 10px;">
			<br />
			<div >
				<span>
					<span data-translate-html="formulario.mensaje"r>Mensaje</span> 
					<span data-translate-html="general.para">para:</span>
				</span> 
				<?php echo Texto::textoPlano($nombreRemitente) ?>
			</div>

			<br />

			<div class='fleft etiquetaAsunto'>
				<label data-translate-html="correo.asunto">
					Asunto
				</label>
			</div>

			<div class='fleft'>
				<div style='overflow:hidden;'>
					<input type='text' size='90' name='asunto' value='<?php echo $elAsunto; ?>'/>
				</div>
			</div>
		</div>
		<div class='clear'></div>

		<div>
			<div class='menuMasivoTutorSin'>

<!--				<div class='upload fleft'>
					<div class='file-custom'>
						<label for="archivo[]">
							<input type='file' name='archivo[]' class="multi"/>
							<span data-translate-html="zonatutor.seleccionar">Seleccionar archivo</span>
						</label>
					</div>
				</div>-->

				<div class='upload fleft'>
					<input name="archivo[]" class="multi" type="file"/>
				</div>

				<div class='fleft importanciaAlta'>
					<input type='hidden' name='importante' value='0' />
					<span>
						<a href="#" onclick="correoBotonImportancia();return false;">

							<img src='imagenes/correos/important.png' alt='' class='alignVertical'/>

							<label data-translate-html="correo.importancia">
								Importancia alta
							</label>
						</a>
					</span>
				</div>
			</div>

			<?php if (Usuario::compareProfile(array('tutor'))): ?>
				<input type='hidden' name='addSeguimiento' value='0' />
			<?php endif; ?>

		</div>

		<div class='mensaje_correo clear'>
			<textarea id="" name='mensaje' class='mensaje'>
				<?php
				echo "<span style='color:#6F6F6F'><br/><br/><br/><br/>______________________________________________________________________<br/><br/>" .
				"<span data-translate-html=general.de'>De:</span> " . $nombreRemitente . "<br/>" .
				"<span data-translate-html=general.enivado_el'>Enviado el:</span> " . Fecha::obtenerFechaFormateada($detalle_mensaje['fecha']) . "<br/>" .
				"<span data-translate-html=general.para'>Para:</span> " . $nombreDestinatario . "<br/>" .
				"<span data-translate-html=correo.asunto'>Asunto:</span> " . $detalle_mensaje['asunto'] . "<br/>" .
				$detalle_mensaje['mensajes'] . "<br/></span>";
				?>
			</textarea>
		</div>
	</div>

	<input type='hidden' name='iddestinatario' value='<?php echo $detalle_mensaje['remitente']; ?>' />
	<input type='hidden' name='idcorreo' value='<?php echo $detalle_mensaje['idcorreos']; ?>' />
	<input type='hidden' name='idCurso' value='<?php echo $detalle_mensaje['idcurso']; ?>' />
	<input id='actionBoton' type='hidden' name='actionBoton' value='Enviar' />
</form>

<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		min_height: 300,
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
	});
</script>
<script type="text/javascript" src="js-gestor_correos-nuevo.js"></script>
<script type="text/javascript">$('.menuMasivoTutorSin input.multi').MultiFile();</script>
