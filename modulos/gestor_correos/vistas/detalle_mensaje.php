<?php 
if(isset($detalle_mensaje)):?>	
	<div class='detalle_mensaje'>
		<div class='cabecera_mail'>

		<?php $letra = explode("_",$detalle_mensaje['destinatarios']);?>

			<p>
				<span class ='cursiva negrita' style="margin-right: 5px;" data-translate-html="correo.asunto">
					Asunto:
				</span> 
				<?php echo Texto::textoPlano($detalle_mensaje['asunto']); ?>
			</p>
			
			<?php //if(!empty($los_destinatarios) && isset($get['menu']) && $get['menu'] == 'bandeja_salida'):?>

				<p>
					<span class ='cursiva negrita' data-translate-html="general.para">
						Para:
					</span> 
					<?php echo Texto::textoPlano($los_destinatarios); ?>
				</p>

			<?php //elseif(isset($get['menu']) && ($get['menu'] == 'bandeja_entrada' || $get['menu'] == 'carpeta' || $get['menu'] == 'papelera')):?>

				<p>
					<span class ='cursiva negrita'style="margin-right: 35px;" data-translate-html="general.de">
						De:
					</span> 
					<?php 
						echo Texto::textoPlano($de);
						if ($_SESSION['perfil'] != 'alumno' 
							&& isset($get['menu']) 
							&& ($get['menu'] == 'bandeja_entrada' || $get['menu'] == 'carpeta' || $get['menu'] == 'papelera')) echo " <br/><span class='cursiva' style='font-size:0.9em;margin-left:60px;'>".Texto::textoPlano($detalle_mensaje['titulo'])."</span>"; 
					?>
				</p>
			<?php //endif;?>
			
			<p>
				<span class ='cursiva negrita'style="margin-right: 10px;" data-translate-html="correo.fecha">
					Fecha:
				</span> 
				<?php echo Fecha::obtenerFechaFormateada($detalle_mensaje['fecha']); ?>
			</p>

			<?php if(Usuario::compareProfile(array('tutor', 'coordinador')) && $get['menu'] == 'bandeja_salida'):?>
				<div>
					<a href='#' onClick="desplegarConfirmacionLectura();" title=''>

						<img src='imagenes/correos/confirmacionlectura.png' style='vertical-align:middle'/>
						<span data-translate-html="correo.confirmacion">Confirmacion de lectura</span>
					</a>
				</div>	

				<div id="confirmacionLectura" class="ocultarInicio">
					<?php 
						for($i=0;$i<=count($nombreDestinatario)-1;$i++) {

							if(is_null($leidoDestinatario[$i])) {

								$fechaLeido = "<img src='imagenes/correos/check_fail.png' />";

							} else {

								$fechaLeido = "<img src='imagenes/correos/check_ok.png' />" . " " . Fecha::obtenerFechaFormateada($leidoDestinatario[$i]);
							}

							echo "<div class='filaConfirmacion'><span class='NombreConfirmacion verticalAlign'>" . Texto::textoPlano($nombreDestinatario[$i]) . "</span><span style='font-size:0.9em;'>" . $fechaLeido . "</span></div>";
						}
					?>
				</div>
			<?php endif; ?>
				
		</div>
			<?php 
			if(empty ($idcorreo)) $idcorreo = $detalle_mensaje['idcorreos'];
			
			 $sql_adjuntos = "SELECT * from ficheros_adjuntos FA, adjuntos A, destinatarios D
			 where D.iddestinatarios = ".$get['idcorreo']." 
			 and A.idcorreos = D.idcorreos
			 and A.idficheros_adjuntos = FA.idficheros_adjuntos";
			 
			 $resultado_adjuntos = mysqli_query($con,$sql_adjuntos);
			 if(mysqli_num_rows($resultado_adjuntos) != 0)
			 {
			 	 echo "<div class='datos_adjuntos'>";
					 while($f_adjuntos = mysqli_fetch_assoc($resultado_adjuntos))
					 {		
					 	echo "<div class='archivo_adjunto'><img src='imagenes/correos/clip.png' />&nbsp;<a href='forzar_descarga/".$f_adjuntos['idficheros_adjuntos']."' target='_blank'>".Texto::textoPlano($f_adjuntos['nombre_fichero'])."</a></div>";
					 }
				 echo "</div>";
			 }
			 //else echo "Este mensaje no contiene ficheros adjuntos";
			 ?>
		
		<div class='cuerpo_mail'><?php echo $detalle_mensaje['mensajes']; ?></div>
		
		<?php if($get['menu'] == 'carpeta'):?>
			<input type="hidden" name="idcarpeta_auto" value="<?php echo $detalle_mensaje['carpeta']?>" />
		<?php endif;?>
		
		<input type="hidden" name="idmensaje_correo" value="<?php echo $detalle_mensaje['iddestinatarios']?>" />
		<input type="hidden" name="menu" value="<?php echo $get['menu']; ?>" />
	</div>
<?php else:?>
	<div class='detalle_mensaje'>
		<strong data-translate-html="correo.no_mensajes">
			No existen mensajes
		</strong>
	</div>
<?php endif;?>

<script type="text/javascript">
function desplegarConfirmacionLectura()
{
	$("#confirmacionLectura").toggle(200); 
}
</script>