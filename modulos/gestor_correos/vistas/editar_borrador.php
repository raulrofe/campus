<?php if (isset($detalle_mensaje) && !empty($detalle_mensaje)): ?>

	<form name='frm_seleccion_correo' method='post' action='' enctype='multipart/form-data'>

		<br/>

		<div class='tituloBloques' data-translate-html="formulario.mensaje">
			Mensaje
		</div>	

		<div id="gestor_correos_botones">
			<ul>
				<li>
					<button type="submit" data-translate-html="formulario.guardar">
						Guardar
					</button>
				</li>
			</ul>
		</div>
		<div class='api_adjuntar clear'>
			<div>
				<div class='fleft etiquetaAsunto' data-translate-html="correo.asunto">
					Asunto
				</div>

				<div class='fleft'>
					<div style='overflow:hidden;'>
						<input type='text' name='asunto' value='<?php echo Texto::textoPlano(addslashes($detalle_mensaje['asunto'])); ?>'/>
					</div>
				</div>
			</div>

			<div class='clear'></div>

			<div>
				<div class='menuMasivoTutorSin'>
					<div class='upload fleft'>
						<div>
								<input type='file' name='archivo[]' class="multi"/>
						</div>
					</div>				
					<div class='fleft importanciaAlta <?php if ($detalle_mensaje['importante']) echo 'importanciaAltaActive'; ?>'>

						<input value="<?php if ($detalle_mensaje['importante']) echo '1';
	else echo '0'; ?>" type="hidden" name='importante' />
						<span>
							<a href="#" onclick="correoBotonImportancia();return false;">
								<img src='imagenes/correos/important.png' alt='' class='alignVertical'/>
								<span data-translate-html="correo.importancia">Importancia alta</span>
							</a>
						</span>

					</div>
				</div>
			</div>
			<div class="clear"></div>
			<?php
			if (empty($idcorreo))
				$idcorreo = $detalle_mensaje['idcorreos'];

			$adjuntosActuales = $mi_correo->obtenerTodosAdjuntos($detalle_mensaje['idcorreos']);
			if ($adjuntosActuales->num_rows > 0) {
				while ($f_adjuntos = $adjuntosActuales->fetch_array()) {
					echo '<div class="MultiFile-label lineaAdjunto burbuja">' .
					'<span title="Archivo seleccionado: ' . Texto::textoPlano($f_adjuntos['nombre_fichero']) . '" class="MultiFile-title" data-translate-title="correo.archivo_sel" >' .
					'<a href="forzar_descarga/' . $f_adjuntos['idficheros_adjuntos'] . '" target="_blank">' .
					'<img alt="" src="imagenes/correos/clip_adjunto.png">' . Texto::textoPlano($f_adjuntos['nombre_fichero']) .
					'</a>' .
					'</span>' .
					'<a href="#" onclick="correoEliminarAdjuntoBorrador(this); return false;" class="MultiFile-remove">' .
					'<img alt="" src="imagenes/correos/delete_adjunto.png">' .
					'</a>' .
					'<input type="hidden" name="adjuntosBorrador[]" value="' . Texto::textoPlano($f_adjuntos['idficheros_adjuntos']) . '" />' .
					'</div>';
				}
			}
			?>

			<div class="clear"></div>

			<div class='mensaje_correo clear' style="margin-top: 10px;">
				<textarea id="correo_textearea" name='mensaje' class='mensaje'>
					<?php echo Texto::textoPlano($detalle_mensaje['mensajes']); ?>	
				</textarea>
			</div>

		</div>

		<input id='actionBoton' type='hidden' name='actionBoton' value='borrador' />	

	</form>

		<!-- <script type="text/javascript" src="<?php //echo URL_BASE;  ?>/modulos/gestor_correos/js/source.js"></script> -->

	<script type="text/javascript" src="js-gestor_correos-nuevo.js"></script>

	<script type="text/javascript">$('.menuMasivoTutorSin input.multi').MultiFile();</script>

	<script type="text/javascript">
		tinymce.init({
			selector: "textarea",
			menubar: false,
			statusbar: false,
			plugins: ["emoticons"],
			toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
		});
	</script>

<?php endif; ?>