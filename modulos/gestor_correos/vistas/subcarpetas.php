<div class='gestor_de_correos'>

<h4 data-translate-html="correo.gestion_carpetas">
	GESTI&Oacute;N DE CARPETAS
</h4>
<br/>

<div class='asunto'>
	<form name='frm_seleccion_correo' method='post' action=''>
		<span class='title_asunto' data-translate-html="correo.crear_carpeta">
			Crear carpeta
		</span>

		<?php if(!empty ($idsubcarpeta)): ?>
			<span class='fleft'>
				<input type='text' size='90' name='enombre_carpeta' value="<?php echo $datos_de_subcarpeta['nombre_carpeta'] ?>"" />
			</span>

		<?php else: ?>

			<span class='fleft'>
				<input type='text' size='90' name='nombre_carpeta'/>
			</span>

		<?php endif; ?>

		<span class='fleft'>
			&nbsp;&nbsp;
			<input type='submit' value='enviar' data-translate-value="formulario.enviar"/>
		</span>

		<input type='hidden' name='mail' value='<?php echo $mail ?>' />
		<input type='hidden' name='mandar' value='<?php echo $mandar ?>' />
	</form>
</div>

<div>

	<div class='encabezado_tabla' data-translate-html="correo.carpetas">
		CARPETAS
	</div>

	<div class='listado_tabla'>
		<?php while($datos_carpetas = mysqli_fetch_assoc($registros_carpetas)): ?>
			<div>
				<div class='elemento_tabla'>
					<?php echo $datos_carpetas['nombre_carpeta'] ?>
				</div>

				<div class='objeto_tabla'>
					<a href='correos/subcarpetas/editar_subcarpeta/<?php echo $datos_carpetas['idcarpetas'] ?>' data-translate-html="general.editar">
						Editar
					</a>
				</div>

				<div class='objeto_tabla'>
					<a href='correos/subcarpetas/eliminar_subcarpeta/<?php echo $datos_carpetas['idcarpetas'] ?>' data-translate-html="general.eliminar">
						Eliminar
					</a>
				</div>
			</div>";
		<?php endwhile ?>
	</div>
</div>
</div>