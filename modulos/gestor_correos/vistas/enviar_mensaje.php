<form name='frm_seleccion_correo_1' method='post' action='' enctype='multipart/form-data'>

	<?php if (Usuario::compareProfile(array('tutor', 'coordinador'))): ?>

		<div class='tituloBloques' data-translate-html="general.cursos">
			Cursos
		</div>

		<div class='selectorCurso'>
			<?php
			$allCurso = $mi_curso->cursos_usuario($_SESSION['idusuario'], true);
			echo "<select name='idSelectCurso' onchange='this.form.submit();' style='width:99%;''>";
			while ($selectCurso = mysqli_fetch_assoc($allCurso)) {
				if ($idSelectCurso == $selectCurso['idcurso'])
					echo "<option value='" . $selectCurso['idcurso'] . "' selected>" . $selectCurso['titulo'] . "</option>";
				else
					echo "<option value='" . $selectCurso['idcurso'] . "'>" . $selectCurso['titulo'] . "</option>";
			}
			echo "</select>";
			?>
		</div>
	<?php endif; ?>
</form>

<form id="frm_reenvio" name='frm_seleccion_correo' method='post' action='' enctype='multipart/form-data'>
	<br/>

	<div class='tituloBloques' data-translate-html="correo.destinatarios">
		Destinatarios
	</div>

	<div class='menvio'>

		<div id="frm_gestor_reenvio_destinatarios">

			<input type="hidden" name="idSelectCurso" value="<?php echo $idSelectCurso ?>" />

			<?php
			while ($datos_staff = mysqli_fetch_assoc($staff_curso)) {
				if ($_SESSION['idusuario'] != $datos_staff['idrrhh']) {
					echo "<div>"
					. "<input name='idalumnos[]' data-type='rrhh' id='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "' type='checkbox' value='t_" . $datos_staff['idrrhh'] . "' />"
					. "<label for='frm_correo_reenvio_t_" . $datos_staff['idrrhh'] . "'>" . Texto::textoPlano($datos_staff['nombrec']) . " (<span data-translate-html='perfiles." . $datos_staff['perfil'] . "'>" . ucwords($datos_staff['perfil']) . "/a</span>)</label></div>";
				}
			}

			while ($datos_alumno = mysqli_fetch_assoc($registro_alumnos)) {
				if ($remitente != $datos_alumno['idalumnos']) {
					if (isset($idremitente) && $idremitente == $datos_alumno['idalumnos']) {
						echo "<div><input name='idalumnos[]' data-type='alumno' id='frm_correo_reenvio_" . $datos_alumno['idalumnos'] . "' type='checkbox' value='" . $datos_alumno['idalumnos'] . "' selected>" .
						"<label for='frm_correo_reenvio_" . $datos_alumno['idalumnos'] . "'>" . Texto::textoPlano($datos_alumno['apellidos']) . ", " . Texto::textoPlano($datos_alumno['nombre']) . " (<span data-translate-html='perfiles.alumno'>Alumno/a</span>)</label></div>";
					} else {
						echo "<div><input name='idalumnos[]' data-type='alumno' id='frm_correo_reenvio_" . $datos_alumno['idalumnos'] . "' type='checkbox' value='" . $datos_alumno['idalumnos'] . "'>" .
						"<label for='frm_correo_reenvio_" . $datos_alumno['idalumnos'] . "'>" . Texto::textoPlano($datos_alumno['apellidos']) . ", " . Texto::textoPlano($datos_alumno['nombre']) . " (<span data-translate-html='perfiles.alumno'>Alumno/a</span>)</label></div>";
					}
				}
			}
			?>
		</div>
		<?php if ($_SESSION['perfil'] != 'alumno'): ?>
			<div class='menuMasivoTutor'>
				<input type='checkbox' name='enviar_a' value='curso'/> 
				<label>
					<span class='alignVertical' data-translate-html="correo.enviar_todos">
						Enviar a todos los alumnos del curso
					</span>
				</label>
				&nbsp;&nbsp;&nbsp;
			</div>
		<?php endif; ?>
	</div>

	<div id='frm_reenvio_1_msg' style="padding-left:15px;"></div>

	<br/>

	<div class='tituloBloques' data-translate-html="formulario.mensaje">
		Mensaje
	</div>

	<div class='api_adjuntar'>
		<div>
			<div class='fleft etiquetaAsunto'>
				<label data-translate-html="correo.asunto">
					Asunto
				</label>
			</div>
			<div class='fleft'>
				<div >
					<input type='text' name='asunto' />
				</div>
			</div>
		</div>

		<div class='clear'></div>

		<div>
			<div class='menuMasivoTutorSin'>

				<div class='upload fleft'>
					<div style="margin-left: 20px;">
						<span data-translate-html="adjuntar.archivo">Adjuntar <br/> archivo</span>
					</div>
					<input name="archivo[]" class="multi" type="file"/>
				</div>

				<div class='fleft importanciaAlta'>
					<input type='hidden' name='importante' value='0' />
					<span>
						<a href="#" onclick="correoBotonImportancia();return false;">

							<img src='imagenes/correos/important.png' class='alignVertical'/>

							<span data-translate-html="correo.importancia">
								Importancia alta
							</span>
						</a>
					</span>
				</div>
				<?php if ($_SESSION['perfil'] != 'alumno'): ?>
					<input type='hidden' name='addSeguimiento' value='0' />
				<?php endif ?>
			</div>
		</div>
		<br>
		<br>
		<br>
		<div class='mensaje_correo clear'>
			<textarea id="correo_textearea" name='mensaje' class='mensaje'>
				<?php if (isset($post['mensaje'])) echo $_POST['mensaje']; ?>
			</textarea>
		</div>
	</div>

	<div id="gestor_correos_botones" style="margin-left:15px;margin-bottom:15px;">

		<button type="submit" style="width:49%;" data-translate-html="formulario.enviar">
			Enviar
		</button>

		<button type="button" style="width:48%;" onclick='$("#actionBoton").val("borrador");document["frm_seleccion_correo"].submit(); return false;' data-translate-html="correo.guardar_borrador">
			Guardar como borrador
		</button>

		<div class="clear"></div>
	</div>

		<!-- <input type='submit' name='actionBoton' value='Enviar' style='width:95%;margin:0 20px 0 15px;' class='fleft'/> -->
	<input id='actionBoton' type='hidden' name='actionBoton' value='Enviar' data-translate-value="formulario.enviar"/>
</form>

<script type="text/javascript">
	tinymce.remove();
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
	});
</script>

<script type="text/javascript" src="js-gestor_correos-validacionenvio.js"></script>
<script type="text/javascript" src="js-gestor_correos-nuevo.js"></script>
<script type="text/javascript">$('.menuMasivoTutorSin input.multi').MultiFile();</script>

