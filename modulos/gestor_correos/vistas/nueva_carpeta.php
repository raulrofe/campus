<div id="correos_nueva_carpeta">
	<form name="frm_correos_nueva_carpeta" id="frm_correos_nueva_carpeta" method="post" data-target="popupModal_gestor_correos"
		action="correos/subcarpeta/personal/nueva" onsubmit="LibModalInto.post(this); return false;">
		<ul>
			<li>
				<label data-translate-html="correo.nombre_carpeta">
					Nombre carpeta
				</label>

				<input type="text" name="nombre" maxlength="100" />
			</li>
			<li class="clear">
				<br />
				<input type="submit" value="Crear" data-translate-value="formulario.crear" />
			</li>
		</ul>	
	</form>
</div>