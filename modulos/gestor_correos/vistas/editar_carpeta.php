<div id="correos_editar_carpeta">
	<form name="frm_correos_editar_carpeta" id="frm_correos_editar_carpeta" method="post"
		data-target="popupModal_gestor_correos" onsubmit="LibModalInto.post(this); return false;"
		action="correos/subcarpeta/personal/<?php echo $get['idcarpeta']?>/editar">
		<ul>
			<li>
				<label data-translate-html="correo.nombre_carpeta">
					Nombre carpeta
				</label>
				<input type="text" name="nombre" value="<?php echo Texto::textoPlano($rowCarpetaEdit->nombre_carpeta)?>" maxlength="100" />
			</li>
			<li class="clear">
				<br />
				<input type="submit" value="Editar" data-translate-value="general.editar" />
			</li>
		</ul>	
	</form>
</div>