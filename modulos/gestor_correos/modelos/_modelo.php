<?php
class Gestor_correos extends modeloExtend{

	private $idcurso;
	private $idperfil;
	private $iduser;
	private $con;
	
	public function obtenerTutor2($idcursoActual)
	{
		$sql = "SELECT * from staff S, rrhh RH
		where S.idcurso = ".$idcursoActual."
		and S.status = 'tutor2' and
		S.idrrhh = RH.idrrhh";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerConfigEntidadOrg()
	{
		$sql = 'SELECT * FROM entidad_organizadora';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarAdjunto($idcorreo, $idadjunto)
	{
		$sql = 'DELETE FROM adjuntos' .
		' WHERE idcorreos = ' . $idcorreo . ' AND idficheros_adjuntos = ' . $idadjunto;
		$resultado = $this->consultaSql($sql);

		$sql = 'DELETE FROM ficheros_adjuntos' .
		' WHERE idcorreos = ' . $idcorreo . ' AND idficheros_adjuntos = ' . $idadjunto;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarCorreo($idcorreo, $mensaje, $asunto, $importante)
	{
		$sql = 'UPDATE correos SET asunto="' . $asunto . '", mensajes="' . $mensaje . '", importante=' . $importante .
		' WHERE idcorreos = ' . $idcorreo;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnAdjunto($idcorreo, $idadjunto)
	{
		$sql = 'SELECT * FROM ficheros_adjuntos AS fa' .
		' WHERE fa.idcorreos = ' . $idcorreo . ' AND idficheros_adjuntos = ' . $idadjunto;
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnAdjuntoIndependiente($idadjunto)
	{
		$sql = 'SELECT * FROM ficheros_adjuntos AS fa' .
		' WHERE fa.idficheros_adjuntos = ' . $idadjunto;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerTodosAdjuntos($idcorreo)
	{
		$sql = 'SELECT * FROM ficheros_adjuntos AS fa' .
		' WHERE fa.idcorreos = ' . $idcorreo;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnAdjuntoPorUsuario($idusuario, $idadjunto)
	{
		$sql = 'SELECT * FROM ficheros_adjuntos AS fa' .
		' LEFT JOIN destinatarios AS D ON D.idcorreos = fa.idcorreos' .
		" WHERE ((D.remitente = '".Usuario::getIdUser(true)."' OR D.remitente = 't_" . $_SESSION['idusuario2'] . "')
			or (D.destinatarios = '".Usuario::getIdUser(true)."' OR D.destinatarios = 't_" . $_SESSION['idusuario2'] . "'))" .
		' AND idficheros_adjuntos = ' . $idadjunto .
		' LIMIT 1';
		//die();
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	/*public function obtenerPlantillas($idPlantilla = null)
	{
		$addQuery = null;
		if(isset($idPlantilla))
		{
			$addQuery = ' AND idplantilla = ' . $idPlantilla;
		}

		$sql = 'SELECT * FROM plantillas WHERE correo = 1' . $addQuery;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}*/

	//Respondemos a un mensaje
	public function responder_mensaje($asunto, $mensaje, $destinatario, $remitente, $idcorreo, $importante, $idCurso)
	{
		$fecha = Fecha::fechaActual();
		$sql = "INSERT into correos
		(asunto, mensajes, idcurso, importante)
		VALUES ('$asunto', '$mensaje', '$idCurso', '$importante')";
		if($this->consultaSql($sql))
		{
			//ultimo id correo
			$ultimoid = $this->obtenerUltimoIdInsertado();

			$sql = "INSERT into destinatarios (remitente,destinatarios,fecha,fecha_modificacion,idcorreos) VALUES ('$remitente','$destinatario','$fecha','$fecha','$ultimoid')";

			$resultado = $this->consultaSql($sql);

			//Ultimo id destinatario
			$ultimoIdDestinatario = $this->obtenerUltimoIdInsertado();

			$return = array('rs' => $resultado, 'id' => $ultimoid, 'iddestinatario' => $ultimoIdDestinatario);
		}

		return $return;
	}

	public function contador_mensajes_sin_leer() {

		$sql = "SELECT count(D.iddestinatarios) as nme from destinatarios D
		left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
		where D.destinatarios = 't_".$_SESSION['idusuario']."'
		and D.carpeta = 0
		and D.leido = 0
		and CCA.iddestinatarios IS NULL
		and D.destPapelera = 0
		and D.destSuprimir = 0
		GROUP BY D.fecha";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}



	//Obtenemos el numero de mensaje no leidos
	public function mensajes_sin_leer(){
		if($_SESSION['perfil'] != 'alumno')
		{
			//echo $_SESSION['perfil'];
			$sql = "SELECT C.idcorreos from correos C
			left join destinatarios D on C.idcorreos = D.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.carpeta = 0
			and D.leido = 0
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.destSuprimir = 0
			GROUP BY C.idCorreos";
		}
		else
		{
			$sql = "SELECT * from correos C, destinatarios D
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and D.carpeta = 0
			and D.leido = 0
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.destSuprimir = 0
			and C.idcurso = " . $_SESSION['idcurso'] .
			" GROUP BY C.idCorreos";
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function contador_mensajes_sin_leer_papelera() {
		
		$sql = "SELECT count(D.iddestinatarios) as nmp from destinatarios D
		where D.destinatarios = 't_".$_SESSION['idusuario']."'
		and D.leido = 0
		and D.destPapelera = 1
		and D.destSuprimir = 0";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Obtenemos el numero de mensaje no leidos de la papelera
	public function mensajes_sin_leer_papelera(){
		if($_SESSION['perfil'] != 'alumno')
		{
			//echo $_SESSION['perfil'];
			$sql = "SELECT C.idcorreos from correos C
			left join destinatarios D on C.idcorreos = D.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and C.idcorreos = D.idcorreos
			and D.leido = 0
			and D.destPapelera = 1
			and D.destSuprimir = 0";
			//echo $sql;
		}
		else
		{
			$sql = "SELECT * from correos C, destinatarios D
			where D.destinatarios = ".$_SESSION['idusuario']."
			and D.idcorreos = C.idcorreos
			and D.leido = 0
			and D.destPapelera = 1
			and D.destSuprimir = 0
			and C.idcurso = " . $_SESSION['idcurso'];
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

		public function contador_mensajes_sin_leer_subcarpeta() {

			$sql = "SELECT count(D.iddestinatarios) as nms from destinatarios D
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.leido = 0
			and D.carpeta != 0
			and (D.destPapelera = 0 AND D.destSuprimir = 0)";

			$resultado = $this->consultaSql($sql);
			return $resultado;

		}


	//Obtenemos el numero de mensaje no leidos de las subcarpetas
	public function mensajes_sin_leer_subcarpeta(){
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos from correos C
			left join destinatarios D on C.idcorreos = D.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and C.idcorreos = D.idcorreos
			and D.leido = 0
			and D.carpeta != 0
			and (D.destPapelera = 0 AND D.destSuprimir = 0)";
		}
		else
		{
			$sql = "SELECT * from correos C, destinatarios D
			where D.destinatarios = ".$_SESSION['idusuario']."
			and D.idcorreos = C.idcorreos
			and D.leido = 0
			and D.carpeta != 0
			and (D.destPapelera = 0 AND D.destSuprimir = 0)
			and C.idcurso = " . $_SESSION['idcurso'];
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Buscamos los mensajes de borrador
	public function bandeja_borrador($limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			//accion_formativa AF, alumnos A,
			$sql = "SELECT * from correos C, destinatarios D
			where D.remitente = 't_" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and C.borrador = 1
			and D.remitPapelera = 0
			GROUP BY D.fecha
			order by D.fecha_modificacion DESC" . $addQuery;
			//echo $sql;
		}
		else
		{
			$sql = "SELECT DISTINCT * from correos C, destinatarios D
			where D.remitente = '".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and C.borrador = 1
			and D.remitPapelera = 0
			order by D.fecha_modificacion DESC" . $addQuery;
		}
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Buscamos los mensajes recibidos
	public function contador_bandeja_entrada()
	{
		$sql = "SELECT count(D.iddestinatarios) as mensajesBandejaEntrada
		from destinatarios as D
		left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
		where D.destinatarios = 't_" . $_SESSION['idusuario'] ."'
		and CCA.iddestinatarios IS NULL
		and D.destPapelera = 0
		and D.carpeta = 0
		GROUP BY D.fecha";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	// Buscamos los mensajes recibidos
	public function bandeja_entrada($limitIni = null, $limitEnd = null)
	{

		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos, C.asunto, C.importante, D.iddestinatarios, D.remitente, D.fecha, D.leido, CUR.titulo
			from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			left join curso as CUR on C.idcurso = CUR.idcurso
			where D.destinatarios = 't_" . $_SESSION['idusuario'] ."'
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.carpeta = 0
			GROUP BY D.fecha
			order by D.fecha_modificacion DESC" . $addQuery;
			//echo $sql;
		}
		else
		{
			$sql = "SELECT DISTINCT C.idcorreos, C.asunto, C.mensajes, C.importante, C.borrador, C.idcurso,
			D.iddestinatarios, D.remitente, D.destinatarios, D.fecha, D.fecha_modificacion, D.leido, D.fecha_leido,
			D.carpeta, D.remitPapelera, D.destPapelera, D.remitSuprimir, D.destSuprimir, D.idcorreos, CCA.idcarpeta_personal
			from correos C, destinatarios D
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.carpeta = 0
			and C.idcurso = " . $_SESSION['idcurso'] .
			" order by D.fecha_modificacion DESC".$addQuery;
		}
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Buscamos los mensajes enviados
	public function mensajes_enviados($limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno'){
			$sql = "SELECT * from correos C, destinatarios D
			where D.remitente = 't_".$_SESSION['idusuario']."'
			and D.remitPapelera = 0
			and C.idcorreos = D.idcorreos
			and D.destinatarios != ''
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC" . $addQuery;
		}
		else
		{
			$sql = "SELECT * from correos C , destinatarios D
			where D.remitente = '".$_SESSION['idusuario']."'
			and D.remitPapelera = 0
			and C.idcorreos = D.idcorreos
			and D.destinatarios != ''
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC" . $addQuery;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Buscamos los mensajes de la papelera
	public function mensajes_papelera($limitIni = null, $limitEnd = null)
	{
		$papelera = array();
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			// meto en un array los mensaje de la papelera cuando eres remitente
			$sql = "SELECT * from correos C, destinatarios D
			where D.remitente = 't_".$_SESSION['idusuario']."' and D.remitPapelera = 1
			and D.idcorreos = C.idcorreos
			and D.remitSuprimir = 0
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC".$addQuery;
			$remit = $this->consultaSql($sql);
			//echo $sql.'<br/>';
			if(mysqli_num_rows($remit) != 0)
			{
				while($remitPapelera = mysqli_fetch_assoc($remit))
				{
					$papelera['id'][] = $remitPapelera['idcorreos'];
					$papelera['iddestinatario'][] = $remitPapelera['iddestinatarios'];
					$papelera['asunto'][] = $remitPapelera['asunto'];
					$papelera['de'][] = $remitPapelera['remitente'];
					$papelera['fecha'][] = $remitPapelera['fecha'];
					$papelera['importante'][] = $remitPapelera['importante'];
					$papelera['leido'][] = $remitPapelera['leido'];
				}
			}

			//meton en el mismo array creado arriba los mensaje de la papelera cuando eres destinatario
			$sql = "SELECT * from correos C, destinatarios D
			where D.destinatarios = 't_".$_SESSION['idusuario']."' and D.destPapelera = 1
			and D.idcorreos = C.idcorreos
			and D.destSuprimir = 0
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC".$addQuery;
			//echo $sql.'<br/>';
			$dest = $this->consultaSql($sql);
			if(mysqli_num_rows($dest) != 0)
			{
				while($destPapelera = mysqli_fetch_assoc($dest))
				{
					$papelera['id'][] = $destPapelera['idcorreos'];
					$papelera['iddestinatario'][] = $destPapelera['iddestinatarios'];
					$papelera['asunto'][] = $destPapelera['asunto'];
					$papelera['de'][] = $destPapelera['remitente'];
					$papelera['fecha'][] = $destPapelera['fecha'];
					$papelera['importante'][] = $destPapelera['importante'];
					$papelera['leido'][] = $destPapelera['leido'];
				}
			}
		}
		else
		{
			//Creamos el array para los remitente con los mensajes de la papelera de alumnos
			$sql = "SELECT DISTINCT * from correos C, destinatarios D
			where D.remitente = '".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.remitPapelera = 1
			and D.remitSuprimir = 0
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC".$addQuery;
			//echo $sql;
			$remit = $this->consultaSql($sql);
			if(mysqli_num_rows($remit) != 0)
			{
				while($remitPapelera = mysqli_fetch_assoc($remit))
				{
					$papelera['id'][] = $remitPapelera['idcorreos'];
					$papelera['iddestinatario'][] = $remitPapelera['iddestinatarios'];
					$papelera['asunto'][] = $remitPapelera['asunto'];
					$papelera['de'][] = $remitPapelera['remitente'];
					$papelera['fecha'][] = $remitPapelera['fecha'];
					$papelera['importante'][] = $remitPapelera['importante'];
					$papelera['leido'][] = $remitPapelera['leido'];
				}
			}

			//Creamos el array con los mensajes de la papelera para los destinatarios que son alumnos
			$sql = "SELECT DISTINCT * from correos C, destinatarios D
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.destPapelera = 1
			and D.destSuprimir = 0
			GROUP BY D.fecha
			order by D.fecha_modificacion DESC".$addQuery;
			//echo $sql;
			$dest = $this->consultaSql($sql);
			if(mysqli_num_rows($dest) != 0)
			{
				while($destPapelera = mysqli_fetch_assoc($dest))
				{
					$papelera['id'][] = $destPapelera['idcorreos'];
					$papelera['iddestinatario'][] = $destPapelera['iddestinatarios'];
					$papelera['asunto'][] = $destPapelera['asunto'];
					$papelera['de'][] = $destPapelera['remitente'];
					$papelera['fecha'][] = $destPapelera['fecha'];
					$papelera['importante'][] = $destPapelera['importante'];
					$papelera['leido'][] = $destPapelera['leido'];
				}
			}
		}
		return $papelera;
	}

	// enviamos el mensaje a la papelera
	public function enviar_papelera($idmensaje,$bandeja)
	{
		if($bandeja == 'entrada')
		{
				//print_r($idmensaje);

				for($i=0;$i<=count($idmensaje)-1;$i++)
				{

					$sql = "SELECT fecha from destinatarios where iddestinatarios = ".$idmensaje[$i];
					$lafecha = $this->consultaSql($sql);
					$fecha = mysqli_fetch_assoc($lafecha);

					if(is_numeric($idmensaje[$i]))
					{
						if($_SESSION['perfil'] != 'alumno')
						{
							//comprobamos si ya esta en la papelera
							$sql = "SELECT iddestinatarios from destinatarios
							where destPapelera = 1 and
							leido = 1
							and destinatarios = 't_".$_SESSION['idusuario']."'
							and fecha = '".$fecha['fecha']."'";
							$fila = $this->consultaSql($sql);

							if(mysqli_num_rows($fila) == 0)
							{
								$fechaModificada = date("Y-m-d H:i:s");
								$sql = "UPDATE destinatarios SET destPapelera = 1, fecha_modificacion = '$fechaModificada'
								where destinatarios = 't_".$_SESSION['idusuario']."'
								and fecha = '".$fecha['fecha']."'";
							}

							else
							{
								//$resultado = $this->eliminar_definitivamente($idmensaje[$i]);
							}
						}
						else
						{
							$fechaModificada = date("Y-m-d H:i:s");
							$sql = "UPDATE destinatarios SET destPapelera = 1, fecha_modificacion = '$fechaModificada'
							where destinatarios = ".$_SESSION['idusuario']."
							and fecha = '".$fecha['fecha']."'";
						}
					//echo $sql;
					$resultado = $this->consultaSql($sql);
					}
				}
				return $resultado;
		}
		else if ($bandeja == 'salida' OR $bandeja == 'borrador')
		{
				for($i=0;$i<=count($idmensaje)-1;$i++)
				{

					$sql = "SELECT fecha from destinatarios where iddestinatarios = ".$idmensaje[$i];
					$lafecha = $this->consultaSql($sql);
					$fecha = mysqli_fetch_assoc($lafecha);

					if(is_numeric($idmensaje[$i]))
					{
						if($_SESSION['perfil'] != 'alumno')
						{
							$fechaModificada = date("Y-m-d H:i:s");
							$sql = "UPDATE destinatarios SET remitPapelera = 1, fecha_modificacion = '$fechaModificada'
							where remitente = 't_".$_SESSION['idusuario']."'
							and fecha = '".$fecha['fecha']."'";
						}
						else
						{
							$fechaModificada = date("Y-m-d H:i:s");
							$sql = "UPDATE destinatarios SET remitPapelera = 1, fecha_modificacion = '$fechaModificada'
							where remitente = ".$_SESSION['idusuario']."
							and fecha = '".$fecha['fecha']."'";
						}
					$resultado = $this->consultaSql($sql);
					}
					//echo $sql;;
				}
				return $resultado;
		}
	}

	// Movemos el mensaje a la carpeta elegida segun su id
	public function enviar_a_carpeta($idcarpeta,$idmensaje)
	{
		$fechaModificada = date("Y-m-d H:i:s");
		for($i=0;$i<=count($idmensaje)-1;$i++)
		{
			$sql = "UPDATE correos SET idcarpetas = ".$idcarpeta.", fecha_modificacion = '$fechaModificada' where idcorreos = ".$idmensaje[$i];
			$this->consultaSql($sql);
		}
	}

	// eliminar definitivamente el mensaje elejido - borrado logico -
	public function eliminar_definitivamente($idmensaje)
	{
		for($i=0;$i<=count($idmensaje)-1;$i++)
		{
			$fechaModificada = date("Y-m-d H:i:s");
			$sql = "SELECT * from destinatarios
			where iddestinatarios = ".$idmensaje[$i];
			//echo $sql;
			$resultadoS = $this->consultaSql($sql);
			$Sup = mysqli_fetch_assoc($resultadoS);

			if($_SESSION['perfil'] != 'alumno')
			{
				if($Sup['remitente'] == $Sup['destinatarios'])
				{
					$sql = "UPDATE destinatarios SET remitSuprimir = 1, destSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and iddestinatarios = '".$idmensaje[$i]."'";
				}

				elseif($Sup['remitente'] == 't_'.$_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET remitSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and remitente = '".$Sup['remitente']."'";
				}
				elseif($Sup['destinatarios'] == 't_'.$_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET destSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and remitente = '".$Sup['remitente']."'";
				}
			}
			else
			{
				if($Sup['remitente'] == $Sup['destinatarios'])
				{
					$sql = "UPDATE destinatarios SET remitSuprimir = 1, destSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and iddestinatarios = '".$idmensaje[$i]."'";
				}
				elseif($Sup['remitente'] == $_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET remitSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and remitente = '".$Sup['remitente']."'";
				}
				elseif($Sup['destinatarios'] == $_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET destSuprimir = 1, fecha_modificacion = '$fechaModificada' where fecha = '".$Sup['fecha']."' and remitente = '".$Sup['remitente']."'";
				}

			}
			//echo $sql;
			$resultado = $this->consultaSql($sql);
		}
		return $resultado;
	}

	// Actualizamos el mensaje como leido
	public function actualizar_leido($idcorreo){

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from destinatarios where destinatarios = 't_".$_SESSION['idusuario']."' and iddestinatarios = ".$idcorreo;
		}
		else
		{
			$sql = "SELECT * from destinatarios where destinatarios = ".$_SESSION['idusuario']." and iddestinatarios = ".$idcorreo;
		}

		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado)>0)
		{
			$addLeido = '';
			$fila = mysqli_fetch_assoc($resultado);

			if($_SESSION['perfil'] != 'alumno')
			{
				if(is_null($fila['fecha_leido']))
				{
					$fechaLeido = date("Y-m-d H:i");
					$addLeido = ", fecha_leido = '" . $fechaLeido . "' ";
				}

				$sql_leido = "UPDATE destinatarios
				SET leido = 1 "
				. $addLeido . "
				where iddestinatarios = ".$idcorreo."
				and destinatarios = 't_".$_SESSION['idusuario']."'";


			}
			else
			{
				if(is_null($fila['fecha_leido']))
				{
					$fechaLeido = date("Y-m-d H:i");
					$addLeido = ", fecha_leido = '" . $fechaLeido ."' ";
				}

				$sql_leido = "UPDATE destinatarios
				SET leido = 1 "
				. $addLeido . "
				where iddestinatarios = ".$idcorreo."
				and destinatarios = '".$_SESSION['idusuario']."'";
			}
			//echo $sql;
			$this->consultaSql($sql_leido);
		}
	}

	// Vemos en detalle el mensaje
	public function mostrar_mensaje($idcorreo)
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from correos C, rrhh RH, destinatarios D, curso CU
			where D.idcorreos = C.idcorreos
			and  D.iddestinatarios = ".$idcorreo."
			and ((D.remitente = 't_".$_SESSION['idusuario']."' OR D.remitente = 't_" . $_SESSION['idusuario2'] . "')
			or (D.destinatarios = 't_".$_SESSION['idusuario']."' OR D.destinatarios = 't_" . $_SESSION['idusuario2'] . "'))
			and CU.idcurso = C.idcurso";
			
			if($_SESSION['idusuario'] == 65) {
				$sql = "SELECT * from correos C, rrhh RH, destinatarios D, curso CU
				where D.idcorreos = C.idcorreos and  D.iddestinatarios = ".$idcorreo." and CU.idcurso = C.idcurso";				
			}	

			$resultado = $this->consultaSql($sql);
			if(mysqli_num_rows($resultado) == 0)
			{
				$sql = "SELECT * from correos C, rrhh RH, destinatarios D, curso CU
				where D.iddestinatarios = " . $idcorreo . "
				and D.idcorreos = C.idcorreos
				and (D.remitente = '" . $_SESSION['idusuario'] . "' OR D.remitente = '" . $_SESSION['idusuario2'] . "')
				and CU.idcurso = C.idcurso
				GROUP BY C.idcurso";
				$resultado = $this->consultaSql($sql);
			}
		}
		else
		{
				// muestra el detalle del mensaje cuando el alumno es el destinatario
				$sql = "SELECT * from correos C, destinatarios D, curso CU
				where D.destinatarios = '" . $_SESSION['idusuario'] . "'
				and D.idcorreos = C.idcorreos
				and CU.idcurso = C.idcurso
				and D.iddestinatarios = " . $idcorreo;

				$resultado = $this->consultaSql($sql);
				if(mysqli_num_rows($resultado) == 0)
				{
					// muestra el detalle del mensaje cuando el alumno es el remitente
					$sql = "SELECT * from correos C, destinatarios D, curso CU
					where D.remitente = '" . $_SESSION['idusuario'] . "'
					and D.idcorreos = C.idcorreos
					and CU.idcurso = C.idcurso
					and D.iddestinatarios = " . $idcorreo;
					$resultado = $this->consultaSql($sql);
				}
		}

		$detalle_mail = mysqli_fetch_assoc($resultado);

		//var_dump($detalle_mail);


		return $detalle_mail;
	}

	public function textBuscar($textBuscar, $limitIni = null, $limitEnd = null)
	{

		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		$textBuscar = $this->escaparCadena(preg_quote($textBuscar, '-'));

		if($_SESSION['perfil'] != 'alumno')
		{

			$sql = "SELECT C.idcorreos, C.asunto, C.mensajes, C.importante, C.borrador, C.idcurso,
			D.iddestinatarios, D.remitente, D.destinatarios, D.fecha, D.fecha_modificacion, D.leido, D.fecha_leido,
			D.carpeta, D.remitPapelera, D.destPapelera, D.remitSuprimir, D.destSuprimir, D.idcorreos
			from correos C, destinatarios D
			LEFT JOIN alumnos AS al ON al.idalumnos = D.destinatarios OR al.idalumnos = D.remitente
			LEFT JOIN rrhh AS rh ON CONCAT('t_', rh.idrrhh) = D.destinatarios OR CONCAT('t_', rh.idrrhh) = D.remitente
			LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh
			LEFT JOIN curso AS cr ON cr.idcurso = st.idcurso
			where
			(C.asunto like '%" . $textBuscar . "%' or C.mensajes like '%" . $textBuscar . "%' or cr.titulo like '%" . $textBuscar . "%'
			or CONCAT(al.nombre, ' ', al.apellidos) LIKE '%" . $textBuscar . "%' or rh.nombrec LIKE '%" . $textBuscar . "%')
			&&
			(D.remitente = 't_" . $_SESSION['idusuario'] . "' or D.destinatarios = 't_" . $_SESSION['idusuario'] . "')
			&& (D.destSuprimir = 0 and D.remitSuprimir = 0)
			&&(C.idcorreos = D.idcorreos)
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC " . $addQuery;

			/*
			$sql = "SELECT C.idcorreos, C.asunto, C.mensajes, C.importante, C.borrador, C.idcurso,
			D.iddestinatarios, D.remitente, D.destinatarios, D.fecha, D.fecha_modificacion, D.leido, D.fecha_leido,
			D.carpeta, D.remitPapelera, D.destPapelera, D.remitSuprimir, D.destSuprimir, D.idcorreos
			FROM correos C, destinatarios D
			LEFT JOIN alumnos AS al ON al.idalumnos = D.destinatarios OR al.idalumnos = D.remitente
			LEFT JOIN rrhh AS rh ON CONCAT('t_', rh.idrrhh) = D.destinatarios OR CONCAT('t_', rh.idrrhh) = D.remitente
			LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh
			LEFT JOIN curso AS cr ON cr.idcurso = st.idcurso
			where
			(C.asunto like '%" . $textBuscar . "%' or C.mensajes like '%" . $textBuscar . "%' or cr.titulo like '%" . $textBuscar . "%'
				or CONCAT(al.nombre, ' ', al.apellidos) LIKE '%" . $textBuscar . "%' or rh.nombrec LIKE '%" . $textBuscar . "%')
			&&
			(D.remitente = 't_" . $_SESSION['idusuario'] . "' or D.destinatarios = 't_" . $_SESSION['idusuario'] . "')
			&& (D.destSuprimir = 0 and D.remitSuprimir = 0)
			&&(C.idcorreos = D.idcorreos)
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC " . $addQuery;
			*/
		}
		else
		{
			$sql = "SELECT * from correos C, destinatarios D
			LEFT JOIN alumnos AS al ON al.idalumnos = D.destinatarios OR al.idalumnos = D.remitente
			LEFT JOIN rrhh AS rh ON CONCAT('t_', rh.idrrhh) = D.destinatarios OR CONCAT('t_', rh.idrrhh) = D.remitente
			LEFT JOIN matricula AS mt ON mt.idalumnos = al.idalumnos
			LEFT JOIN curso AS cr ON cr.idcurso = mt.idcurso
			where
			(C.asunto like '%".$textBuscar."%' or C.mensajes like '%".$textBuscar."%' or cr.titulo like '%".$textBuscar."%'
				or CONCAT(al.nombre, ' ', al.apellidos) LIKE '%".$textBuscar."%' or rh.nombrec LIKE '%".$textBuscar."%')
			&&
			(D.remitente = ".$_SESSION['idusuario']." or D.destinatarios = ".$_SESSION['idusuario'].")
			&& (D.destSuprimir = 0 and D.remitSuprimir = 0)
			&&(C.idcorreos = D.idcorreos)
			GROUP BY D.fecha
			ORDER BY D.fecha_modificacion DESC ".$addQuery;
			//echo $sql;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

/*************************************** GESTION DE CARPETAS ***********************************************/

	/*
	 * Metemos el email en la carpeta elegida
	 */


	public function organizar_mail($idcarpeta,$iddestinatario,$idCorreo)
	{
		$fechaModificada = date("Y-m-d H:i:s");
		$sql = "SELECT C.idcurso from correos C, destinatarios D where iddestinatarios = ".$idCorreo." and D.idcorreos = C.idcorreos";
		$resultado = $this->consultaSql($sql);
		$carpeta = mysqli_fetch_assoc($resultado);
		$idcarpeta = $carpeta['idcurso'];

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "UPDATE destinatarios SET carpeta = ".$idcarpeta.", fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idCorreo;
		}
		else
		{
			$sql = "UPDATE destinatarios SET carpeta = ".$idcarpeta.", fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idCorreo;
		}
		//echo $sql;
		$this->consultaSql($sql);
	}

	public function bandeja_carpetas($verIdcarpeta, $limitIni = null, $limitEnd = null)
	{

		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from correos C, accion_formativa AF, alumnos A, destinatarios D
			WHERE D.destinatarios = 't_" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and D.carpeta = " . $verIdcarpeta . "
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC".$addQuery;
		}
		else
		{
			$sql = "SELECT DISTINCT * from correos C, destinatarios D
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.destPapelera = 0
			and D.carpeta = ".$verIdcarpeta."
			order by D.fecha_modificacion DESC".$addQuery;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function bandeja_carpetas_personales($verIdcarpeta, $limitIni = null, $limitEnd = null)
	{

		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join alumnos A on D.remitente = A.idalumnos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			left join accion_formativa AF on D.idcorreos = C.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC".$addQuery;
		}
		else
		{
			$sql = "SELECT DISTINCT * from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.destPapelera = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			order by D.fecha_modificacion DESC".$addQuery;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	public function contador_bandeja_carpetas_personales_sin_leer($verIdcarpeta)
	{
		$sql = "SELECT count(D.iddestinatarios) as nmcp from destinatarios D
		left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
		where D.destinatarios = 't_".$_SESSION['idusuario']."'
		and D.destPapelera = 0
		and D.leido = 0
		and CCA.idcarpeta_personal = ".$verIdcarpeta."
		order by D.fecha_modificacion DESC";

		$resultado = $this->consultaSql($sql);
		return $resultado;

	}

	public function bandeja_carpetas_personales_sin_leer($verIdcarpeta)
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join alumnos A on D.remitente = A.idalumnos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			left join accion_formativa AF on D.idcorreos = C.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and D.leido = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC";
		}
		else
		{
			$sql = "SELECT DISTINCT * from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.destPapelera = 0
			and D.leido = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			order by D.fecha_modificacion DESC";
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/*
	 * Resauramos un mensaje de la papelera
	 */
	public function restaurarMensaje($idMsj)
	{

		//echo $_SESSION['idusuario'];

		$fechaModificada = date("Y-m-d H:i:s");
		for($i=0;$i<=count($idMsj)-1;$i++)
		{
			$sql = "SELECT * from destinatarios where iddestinatarios = ".$idMsj[$i];
			$resultado = $this->consultaSql($sql);
			$rowCorreo = mysqli_fetch_assoc($resultado);

			if($_SESSION['perfil'] != 'alumno')
			{
				if($rowCorreo['remitente'] == $rowCorreo['destinatarios'])
				{
					$sql = "UPDATE destinatarios SET remitPapelera = 0, destPapelera = 0, fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idMsj[$i];
				}
				elseif($rowCorreo['remitente'] == 't_'.$_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET remitPapelera = 0, fecha_modificacion = '$fechaModificada' where fecha = '".$rowCorreo['fecha']."'";
				}
				elseif($rowCorreo['destinatarios'] == 't_'.$_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET destPapelera = 0, fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idMsj[$i];
					//echo $sql;
				}
			}

			else
			{
				if($rowCorreo['remitente'] == $rowCorreo['destinatarios'])
				{
					$sql = "UPDATE destinatarios SET remitPapelera = 0, destPapelera = 0, fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idMsj[$i];
				}
				elseif($rowCorreo['remitente'] == $_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET remitPapelera = 0, fecha_modificacion = '$fechaModificada' where fecha = '".$rowCorreo['fecha']."'";
				}
				elseif($rowCorreo['destinatarios'] == $_SESSION['idusuario'])
				{
					$sql = "UPDATE destinatarios SET destPapelera = 0, fecha_modificacion = '$fechaModificada' where iddestinatarios = ".$idMsj[$i];
				}
			}
			//echo $sql;
			$this->consultaSql($sql);
		}
	}

	public function marcarNoleido($iddestinatario)
	{
		for($i=0;$i<=count($iddestinatario)-1;$i++)
		{
			$leido='';
			$sql = "SELECT leido from destinatarios where iddestinatarios = ".$iddestinatario[$i];
			$fila = $this->consultaSql($sql);
			$f = mysqli_fetch_assoc($fila);

			if($f['leido'] == '0') $leido = 1;
			else $leido = 0;

			$sql = "UPDATE destinatarios SET leido = ".$leido." where iddestinatarios = ".$iddestinatario[$i];
			$resultado = $this->consultaSql($sql);
		}
		return $resultado;
	}

	public function insertarCorreo($asunto, $mensaje, $idcurso, $importante, $borrador = '0')
	{
		$sql = "INSERT into correos (asunto, mensajes, idcurso, importante, borrador)
		VALUES ('$asunto', '$mensaje', '$idcurso', '$importante', '$borrador')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerIdConvocatoriaCurso($idcurso)
	{
		$sql = " SELECT idconvocatoria from curso where idcurso = ".$idcurso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function alumnosTutorizadosConvocatoria($idconvocatoriaFinal)
	{
		$sql = "SELECT M.idalumnos, M.idcurso from matricula AS M" .
		" LEFT JOIN curso AS C ON C.idconvocatoria = " . $idconvocatoriaFinal .
		" LEFT JOIN staff AS S ON C.idcurso = S.idcurso" .
		" AND S.idcurso = M.idcurso" .
		" LEFT JOIN alumnos AS A ON M.idalumnos = A.idalumnos" .
		" WHERE S.idrrhh = " . $_SESSION['idusuario'] .
		" AND C.borrado = 0" .
		" AND M.borrado = 0" .
		" AND A.borrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerAlumnosCurso($idcurso)
	{
		$sql = "SELECT M.idalumnos, M.idcurso" .
		" FROM matricula AS M" .
		" LEFT JOIN curso AS C ON C.idcurso = " . $idcurso .
		" AND C.idcurso = M.idcurso" .
		" LEFT JOIN alumnos AS A ON A.idalumnos = M.idalumnos" .
		" WHERE M.idcurso = C.idcurso " .
		" AND M.borrado = 0 " .
		" AND A.borrado = 0 " .
		" GROUP BY A.idalumnos";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerUsuarioDestinatario($idalumnos, $idcurso)
	{
		if(Usuario::checkAlumno($idalumnos))
		{
			$sql= "SELECT al.idalumnos, al.nombre, al.apellidos, al.email, al.notifi_correo from alumnos AS al
			LEFT JOIN matricula AS m ON m.idalumnos = al.idalumnos
			where m.idalumnos = '".$idalumnos . "' AND m.idcurso = " . $idcurso;
		}
		else
		{
			$sql= "SELECT rh.idrrhh, rh.nombrec from rrhh AS rh
			LEFT JOIN staff AS st ON st.idrrhh = rh.idrrhh
			where CONCAT('t_', rh.idrrhh) = '" . $idalumnos . "' AND st.idcurso = " . $idcurso;
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function insertarDestinatario($remitente, $idalumnos, $fecha, $ultimoIdCorreo)
	{
		$sql = "INSERT into destinatarios " .
		" (remitente,destinatarios,fecha,fecha_modificacion,idcorreos)" .
		" VALUES ('$remitente','$idalumnos','$fecha','$fecha','$ultimoIdCorreo')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}


	public function insertarFicheroAdjunto($nombre_fichero, $enlace_fichero, $ultimoIdCorreo)
	{
		$sql = "INSERT into ficheros_adjuntos (nombre_fichero,enlace_fichero,idcorreos)
		VALUES ('$nombre_fichero','$enlace_fichero','$ultimoIdCorreo')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function adjuntarArchivoCorreo($ultimoIdCorreo, $maxfa)
	{
		$sql = "INSERT into adjuntos (idcorreos,idficheros_adjuntos)
		VALUES ('$ultimoIdCorreo','$maxfa')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerIdCursoPorCorreo($idcorreo)
	{
		$sql = "SELECT idcurso from correos where idcorreos = " . $idcorreo;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerDestinatario($detalle_mensaje)
	{
		$sql = "SELECT destinatarios from destinatarios
		where idcorreos = ".$detalle_mensaje;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function incluirCorreoSeguimiento($asunto, $fecha, $idMatricula, $ultimoIdCorreo)
	{
		$sql = "INSERT into seguimiento_email" .
		" (asunto_email, fecha, idmatricula, iddestinatario)" .
		" VALUES ('$asunto', '$fecha', '$idMatricula', '$ultimoIdCorreo')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCarpetas($idUsuario)
	{
		$sql = "SELECT * FROM correos_carpetas WHERE borrado = 0 AND idusuario = '$idUsuario'";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnaCarpeta($idCarpeta, $idUsuario)
	{
		$sql = "SELECT * FROM correos_carpetas WHERE borrado = 0 AND idcarpeta = $idCarpeta AND idusuario = '$idUsuario' LIMIT 1";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerUnaCarpetaPorNombre($nombre, $idUsuario)
	{
		$sql = "SELECT * FROM correos_carpetas WHERE borrado = 0 AND nombre_carpeta = '$nombre' AND idusuario = '$idUsuario' LIMIT 1";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarCarpeta($nombre, $idUsuario)
	{
		$sql = "INSERT into correos_carpetas (nombre_carpeta, idusuario)
		VALUES ('$nombre', '$idUsuario')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function editarCarpeta($idCarpeta, $nombre)
	{
		$sql = "UPDATE correos_carpetas SET nombre_carpeta = '$nombre' WHERE borrado = 0 AND idcarpeta = $idCarpeta";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarCarpeta($idCarpeta)
	{
		$sql = "UPDATE correos_carpetas SET borrado = 1 WHERE borrado = 0 AND idcarpeta = $idCarpeta";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCarpetaACorreo($idCorreo, $idCarpeta)
	{
		$sql = "SELECT * FROM correos_carpetas_asig WHERE iddestinatarios = $idCorreo AND idcarpeta_personal = $idCarpeta";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function asignarCarpetaACorreo($idCorreo, $idCarpeta)
	{
		$sql = "INSERT INTO correos_carpetas_asig (iddestinatarios, idcarpeta_personal) VALUES($idCorreo, $idCarpeta)";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarCorreoDeCarpetaCurso($idCorreoDest)
	{
		$sql = "UPDATE destinatarios SET carpeta = 0 WHERE iddestinatarios = $idCorreoDest";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarCorreoDeCarpetaPersonal($idCorreoDest)
	{
		$sql = "DELETE FROM correos_carpetas_asig WHERE iddestinatarios = $idCorreoDest";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/***********************************************************************************************************************************************************************
	METODOS OPTIMIZADOS DEL GESTOR DE CORREOS
	************************************************************************************************************************************************************************/

	public function getNameUser()
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT nombrec from rrhh where idrrhh = " . $_SESSION['idusuario'];
		}
		else
		{
			$sql = "SELECT CONCAT(apellidos, ', ', nombre) AS nombrec from alumnos where idalumnos = " . $_SESSION['idusuario'];
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function getNameAlumno($idusuario)
	{
		$sql = "SELECT CONCAT(apellidos, ', ', nombre) AS nombrec from alumnos where idalumnos = " . $idusuario;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function getAlumnosActivoCurso($idCurso)
	{
		 $sql = "SELECT A.idalumnos, A.nombre, A.apellidos from matricula M , alumnos A" .
		 " WHERE M.idcurso = " . $idCurso .
		 " AND M.idalumnos = A.idalumnos" .
		 " AND M.borrado = 0" .
		 " AND A.borrado = 0" .
		 " ORDER BY A.apellidos ASC";

		 $resultado = $this->consultaSql($sql);
		 return $resultado;
	}




	public function mensajesSinLeerBandejaEntrada()
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT D.iddestinatarios from correos C, destinatarios D
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = 't_" . $_SESSION['idusuario'] . "'
			and C.idcorreos = D.idcorreos
			and D.carpeta = 0
			and D.leido = 0
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.destSuprimir = 0
			GROUP BY C.idCorreos";
		}
		else
		{
			$sql = "SELECT D.iddestinatarios  from correos C, destinatarios D
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and D.carpeta = 0
			and D.leido = 0
			and CCA.iddestinatarios IS NULL
			and D.destPapelera = 0
			and D.destSuprimir = 0
			and C.idcurso = " . $_SESSION['idcurso'] .
			" GROUP BY C.idCorreos";
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function mensajesSinLeerPapelera()
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT D.iddestinatarios from correos C, destinatarios D
			where D.destinatarios = 't_" . $_SESSION['idusuario'] . "'
			and C.idcorreos = D.idcorreos
			and D.leido = 0
			and D.destPapelera = 1
			and D.destSuprimir = 0";
		}
		else
		{
			$sql = "SELECT D.iddestinatarios from correos C, destinatarios D" .
			" WHERE D.destinatarios = " . $_SESSION['idusuario'] .
			" AND D.idcorreos = C.idcorreos
			and D.leido = 0
			and D.destPapelera = 1
			and D.destSuprimir = 0
			and C.idcurso = " . $_SESSION['idcurso'];
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function mensajesSinLeerSubcarpeta()
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT D.iddestinatarios from correos C, destinatarios D
			WHERE D.destinatarios = 't_" . $_SESSION['idusuario'] . "'
			AND C.idcorreos = D.idcorreos
			AND D.leido = 0
			AND D.carpeta != 0
			AND (D.destPapelera = 0 AND D.destSuprimir = 0)";
		}
		else
		{
			$sql = "SELECT D.iddestinatarios from correos C, destinatarios D
			WHERE D.destinatarios = " .$_SESSION['idusuario'] .
			" AND D.idcorreos = C.idcorreos
			AND D.leido = 0
			AND D.carpeta != 0
			AND (D.destPapelera = 0 AND D.destSuprimir = 0)
			AND C.idcurso = " . $_SESSION['idcurso'];
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function cursosUsuario($idusuario = null, $active = null)
	{
		if(!isset($idusuario))
		{
			$idusuario = $_SESSION['idusuario'];
		}

		// para mostrar todos los cursos, los activos o los no activos
		$addSql = null;
		if(isset($active))
		{
			$date = date('Y-m-d');
			if($active)
			{
				$addSql = 'AND (C.f_inicio <= "' . $date . '" AND C.f_fin >= "' . $date . '")';
			}
			else
			{
				$addSql = 'AND C.f_fin <= "' . $date . '"';
			}
		}

		if(isset($_SESSION['perfil']) && $_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcurso, C.titulo from staff S, curso C
			where C.borrado = 0
			AND S.idrrhh = " . $idusuario ." and S.idcurso = C.idcurso $addSql
			GROUP BY C.idcurso
			order by C.f_inicio, C.titulo ASC";
			//log::set($sql, __FILE__);
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
		else
		{
			$sql = "SELECT C.idcurso, C.titulo from matricula M, curso C
			where M.idalumnos = ".$idusuario."
			AND (M.borrado = 0 OR C.borrado = 0)
			AND M.idcurso = C.idcurso $addSql
			GROUP BY C.idcurso
			order by C.f_inicio, C.titulo ASC";
			//echo "consulta de cursos".$sql;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}

	public function mostrarMnensaje($idcorreo)
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos from correos C, rrhh RH, destinatarios D, curso CU
			where D.idcorreos = C.idcorreos
			and  D.iddestinatarios = ".$idcorreo."
			and ((D.remitente = 't_".$_SESSION['idusuario']."' OR D.remitente = 't_" . $_SESSION['idusuario2'] . "')
			or (D.destinatarios = 't_".$_SESSION['idusuario']."' OR D.destinatarios = 't_" . $_SESSION['idusuario2'] . "'))
			and CU.idcurso = C.idcurso";
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			if(mysqli_num_rows($resultado) == 0)
			{
				$sql = "SELECT C.idcorreos from correos C, rrhh RH, destinatarios D, curso CU
				where D.iddestinatarios = " . $idcorreo . "
				and D.idcorreos = C.idcorreos
				and (D.remitente = '" . $_SESSION['idusuario'] . "' OR D.remitente = '" . $_SESSION['idusuario2'] . "')
				and CU.idcurso = C.idcurso
				GROUP BY C.idcurso";
				$resultado = $this->consultaSql($sql);
			}
		}
		else
		{
				// muestra el detalle del mensaje cuando el alumno es el destinatario
				$sql = "SELECT C.idcorreos from correos C, destinatarios D, curso CU
				where D.destinatarios = '" . $_SESSION['idusuario'] . "'
				and D.idcorreos = C.idcorreos
				and CU.idcurso = C.idcurso
				and D.iddestinatarios = " . $idcorreo;

				$resultado = $this->consultaSql($sql);
				if(mysqli_num_rows($resultado) == 0)
				{
					// muestra el detalle del mensaje cuando el alumno es el remitente
					$sql = "SELECT C.idcorreos from correos C, destinatarios D, curso CU
					where D.remitente = '" . $_SESSION['idusuario'] . "'
					and D.idcorreos = C.idcorreos
					and CU.idcurso = C.idcurso
					and D.iddestinatarios = " . $idcorreo;
					$resultado = $this->consultaSql($sql);
				}
		}

		$detalle_mail = mysqli_fetch_assoc($resultado);

		return $detalle_mail;
	}

	public function bandejaCarpetas($verIdcarpeta, $limitIni = null, $limitEnd = null)
	{

		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos, C.asunto, C.importante, D.iddestinatarios, D.leido, D.fecha from correos C, accion_formativa AF, alumnos A, destinatarios D
			where D.destinatarios = 't_" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and D.carpeta = " . $verIdcarpeta . "
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC" . $addQuery;
		}
		else
		{
			$sql = "SELECT DISTINCT C.idcorreos, C.asunto, C.importante, D.iddestinatarios, D.leido, D.fecha from correos C, destinatarios D
			where D.destinatarios = '" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and D.destPapelera = 0
			and D.carpeta = " . $verIdcarpeta . "
			order by D.fecha_modificacion DESC" . $addQuery;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function bandejaCarpetasPersonales($verIdcarpeta, $limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos, C.asunto, C.importante, D.iddestinatarios, D.leido, D.fecha from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join alumnos A on D.remitente = A.idalumnos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			left join accion_formativa AF on D.idcorreos = C.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC".$addQuery;
		}
		else
		{
			$sql = "SELECT DISTINCT C.idcorreos, C.asunto, C.importante, D.iddestinatarios, D.leido, D.fecha from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.destPapelera = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			order by D.fecha_modificacion DESC".$addQuery;
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;

	}

	public function bandejaCarpetasPersonalesSinLeer($verIdcarpeta)
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT C.idcorreos from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join alumnos A on D.remitente = A.idalumnos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			left join accion_formativa AF on D.idcorreos = C.idcorreos
			where D.destinatarios = 't_".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and D.remitente = A.idalumnos
			and D.destPapelera = 0
			and D.leido = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			GROUP BY C.idcorreos
			order by D.fecha_modificacion DESC";
		}
		else
		{
			$sql = "SELECT DISTINCT C.idcorreos from correos C
			left join destinatarios D on D.idcorreos = C.idcorreos
			left join correos_carpetas_asig CCA on CCA.iddestinatarios = D.iddestinatarios
			where D.destinatarios = '".$_SESSION['idusuario']."'
			and D.destPapelera = 0
			and D.leido = 0
			and CCA.idcarpeta_personal = ".$verIdcarpeta."
			order by D.fecha_modificacion DESC";
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Buscamos los mensajes de borrador
	public function bandejaBorrador($limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}

		if($_SESSION['perfil'] != 'alumno')
		{
			//accion_formativa AF, alumnos A,
			$sql = "SELECT C.idcorreos from correos C, destinatarios D
			where D.remitente = 't_" . $_SESSION['idusuario'] . "'
			and D.idcorreos = C.idcorreos
			and C.borrador = 1
			and D.remitPapelera = 0
			GROUP BY D.fecha
			order by D.fecha_modificacion DESC" . $addQuery;
			//echo $sql;
		}
		else
		{
			$sql = "SELECT DISTINCT C.idcorreos from correos C, destinatarios D
			where D.remitente = '".$_SESSION['idusuario']."'
			and D.idcorreos = C.idcorreos
			and C.borrador = 1
			and D.remitPapelera = 0
			order by D.fecha_modificacion DESC" . $addQuery;
		}
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

}
?>
