
//variables de control
var menuId = "menuContextual";
var menu = $("#"+menuId);

var menuId2 = "menuContextual2";
var menu2 = $("#"+menuId2);

$(document).unbind("contextmenu");
$('#gestor_correos_ver_idcarpeta a[data-context-menu="1"]').unbind("contextmenu");
$('#gestor_correos_link_mail_into').unbind("contextmenu");
//$(document).unbind('click');
$(document).unbind('keydown');
$(menu).unbind('click');
$(menu2).unbind('click');

$(document).bind("contextmenu", function(e){
	menu.css("display", "none");
	menu2.css("display", "none");
});

//EVITAMOS que se muestre el MENU CONTEXTUAL del sistema operativo al hacer CLICK con el BOTON DERECHO del RATON
$('#gestor_correos_ver_idcarpeta a[data-context-menu="1"]').bind("contextmenu", function(e){	
	menu2.css("display", "none");
	menu.css({'display':'block', 'left':e.pageX, 'top':e.pageY});
	menu.attr('data-id', $(this).attr('data-id'));
	return false;
});
$('.gestor_correos_ver_idcarpeta_icon_menu').bind("click", function(e){	
	menu2.css("display", "none");
	menu.css({'display':'block', 'left':e.pageX, 'top':e.pageY});
	menu.attr('data-id', $(this).parent().find('a[data-id]').attr('data-id'));
	return false;
});
	
	
/**** MENU BANDEJA ENTRADA */
$('#gestor_correos_link_mail_into').bind("contextmenu", function(e){	
	menu.css("display", "none");
	menu2.css({'display':'block', 'left':e.pageX, 'top':e.pageY});
	return false;
});
$('#gestor_correos_bandeja_entrada_icon_menu').bind("click", function(e){	
	menu.css("display", "none");
	menu2.css({'display':'block', 'left':e.pageX, 'top':e.pageY});
	return false;
});
/* ---- */


	//controlamos ocultado de menu cuando esta activo
	//click boton principal raton
	$(document).click(function(e){
		if(e.button == 0 && e.target.parentNode.parentNode.id != menuId){
			menu.css("display", "none");
			menu2.css("display", "none");
		}
	});
	//pulsacion tecla escape
	$(document).keydown(function(e){
		if(e.keyCode == 27){
			menu.css("display", "none");
			menu2.css("display", "none");
		}
	});
	
	//Control sobre las opciones del menu contextual
	menu.click(function(e){
		//si la opcion esta desactivado, no pasa nada
		if(e.target.className == "disabled"){
			return false;
		}
		//si esta activada, gestionamos cada una y sus acciones
		else{
			switch(e.target.id){
				case "menuContextual_edit":
					LibModalInto.create('popupModal_gestor_correos', 'correos/subcarpeta/personal/' + $('#menuContextual').attr('data-id') + '/editar');
					break;
				case "menuContextual_delete":
					alertConfirmAjax('popupModal_gestor_correos', 'eliminarcarpeta', '¿Desea eliminar esta carpeta y todo su contenido?', 'correos/subcarpeta/personal/' + $('#menuContextual').attr('data-id') + '/eliminar');
					break;
			}
			menu.css("display", "none");
		}
	});
	
	//Control sobre las opciones del menu contextual
	menu2.click(function(e){
		//si la opcion esta desactivado, no pasa nada
		if(e.target.className == "disabled"){
			return false;
		}
		//si esta activada, gestionamos cada una y sus acciones
		else{
			switch(e.target.id){
				case "menuContextual_new":

					LibModalInto.create('popupModal_gestor_correos', 'correos/subcarpeta/personal/nueva');
					break;
			}
			menu2.css("display", "none");
		}
	});
	