$(document).ready(function()
{
	$('.buscador_correo input[type="text"]').keypress(function(event)
	{
		if(event.which == 13)
		{
			urlCurrent = $('#popupModal_gestor_correos').attr('data-url');
			
			urlCurrent = removeParamRequestGet(urlCurrent, 'textBuscar');
			urlCurrent = removeParamRequestGet(urlCurrent, 'pagina');

			LibPopupModal.loadPage(this, addParamToUrl(urlCurrent, 'textBuscar=' + $(this).val()));
			
			event.preventDefault();
		}
	});
	
	$('.link_buscar a').click(function()
	{
		urlCurrent = $('#popupModal_gestor_correos').attr('data-url');
			
		urlCurrent = removeParamRequestGet(urlCurrent, 'textBuscar');
		urlCurrent = removeParamRequestGet(urlCurrent, 'pagina');

		urlCurrent = addParamToUrl(urlCurrent , 'textBuscar=' + $('.buscador_correo input[type="text"]').val());
		
		LibPopupModal.loadPage(this, urlCurrent);
					
		event.preventDefault();
	});
});

function gestorCorreoMostrarOcultarCapertas()
{
	if($('#gestor_correos_ver_idcarpeta').css('display') == 'none')
	{
		$.cookie('mail_folder_show', '1', { expires: 7, path: '/' });
	}
	else
	{
		$.cookie('mail_folder_show', null, { expires: 7, path: '/' });
	}
	
	//alert(elemento);
	$("#gestor_correos_ver_idcarpeta").toggle(200); 
}

/*
window.gestor_correo = {}
window.gestor_correo.buscador = {};
window.gestor_correo.buscador.resultado = {};
window.gestor_correo.buscador.word = '';
*/

$('.buscador_correo input[type="text"]').keyup(function()
{
	var word = $(this).val();
	
	if(word.length >= 3)
	{
		
		// cache
		/*if(word.length > 3)
		{
			var wordMatch = new RegExp('^(' + window.gestor_correo.buscador.word + ').*', 'ig');
			if(word.match(wordMatch))
			{
				for(i in window.gestor_correo.buscador.resultado)
				{
					var wordMatchCache = new RegExp('.*(' + window.gestor_correo.buscador.resultado[i] + ').*', 'ig');
					if(word.match(wordMatchCache))
					{
						html += gestorCorreoPintarMensaje(window.gestor_correo.buscador.resultado, i);
					}
				}
			}
		}
		// no cache
		else
		{*/
		//window.gestor_correo.buscador.word = word;
		
		$.ajax({
			url: "correos/buscador/json",
			dataType: "json",
			data:
			{
				word: word
			},
			success: function(data)
			{
				var html = '';
		
				if(data.numMensajes > 0)
				{
					for(var item in data.mensajes)
					{
						html += gestorCorreoPintarMensaje(data, item);
					}
					
					html += '<input type="hidden" name="mandar" value="papelera" />';
				}
				else
				{
					html = '<br/><div class="mensaje_correo t_center">No existen mensajes</div>';
				}
				
				$('#gestor_correo_listado').html(html);
				
				$('.gestor_de_correos').find('.paginado').replaceWith(data.paginado);
				
				//window.gestor_correo.buscador.resultado = data;
				
				//$(".asunto_a *").highlight(word, "highlight");
				//$(".asunto_d").highlight(word, "highlight");
			}
		});
		//}
	}
	else if(word.length == 0)
	{
		LibPopupModal._ajaxLoad('popupModal_gestor_correos', 'correos/bandeja_entrada');
	}
});

function gestorCorreoPintarMensaje(data, item)
{
	var html = '';
	
	leido = '';
	if(data.mensajes[item].leido == false)
	{
		leido = 'negrita';
	}
						
	icon = '';
	icon2 = '';
	icon3 = '';
	if(data.mensajes[item].leido == false)
	{
		icon = '<img src="imagenes/correos/mail_noleido.png" alt="leido" />';
	}
	else
	{
		icon = '<img src="imagenes/correos/mail_leido.png" alt="leido" />';
	}
				
	if(data.mensajes[item].importante == true)
	{
		icon2 = '<img src="imagenes/correos/important.png" alt="" />&nbsp;';
	}
		
	if(data.mensajes[item].n_adjuntos > 0)
	{
		icon3 = '<img src="imagenes/correos/clip.png" alt="" />';
	}
			
	html +=
		'<div class="mensajes_correo">' +
			'<div class="asunto_s"><input type="checkbox" name="idmensaje[]" value="' + data.mensajes[item].id + '" /></div>' +
			'<div class="asunto_a ' + leido + '">' +
				icon + icon2 + icon3 +
				'<a href="correos/detalle_mensaje/' + data.mensajes[item].id + '/' + data.lugar + '">' + data.mensajes[item].asunto + '</a>' +
			'</div>' +
			'<div class="asunto_d">' + data.mensajes[item].destinatarios + '</div>' +
			'<div class="asunto_f">' + data.mensajes[item].fecha + '</div>' +
			'<div class="clear"></div>' +
		'</div>' +
		'<input type="hidden" name="iddestino[]" value="' + data.mensajes[item].id + '" />';
		
	return html;
}

/*
jQuery.fn.highlight = function (str, className) {
    var regex = new RegExp(str, "gi");
    return this.each(function () {
        $(this).contents().filter(function() {
            return this.nodeType == 3 && regex.test(this.nodeValue);
        }).replaceWith(function() {
            return (this.nodeValue || "").replace(regex, function(match) {
                return "<span class=\"" + className + "\">" + match + "</span>";
            });
        });
    });
};
*/