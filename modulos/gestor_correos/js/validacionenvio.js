$("#frmEnvio").validate({
		errorElement: "div",
		messages: {
			asunto: {
				required : 'Introduce un asunto para el correo'
			},
			mensaje: {
				required : 'Introduce unmensaje para el correo'
			}
		},
		rules: {
			asunto: {
				required  : true
			},
			mensaje: {
				required  : true
			}
		}
	});