<div id="editarEvento">

	<h2 data-translate-html="agenda.editar">
		Editar evento
	</h2>
	
	<div id="agenda_eventos_editar">
		<div class="fechaEditarAgenda">
			<div class="t_center">
				<div class="diaNumero"><?php echo $elementDate[2]?></div>
				<div class="diaMes">
					<span><?php echo $dia ?></span><br/>
					<span><?php echo $mes ?></span><br/>
					<span><?php echo $elementDate[0] ?></span>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<form id="frm_agenda_ev_editar" method="post" action="agenda/evento/editar/<?php echo $evento->idagenda?>">
			<ul>
				<li><textarea id="frm_agenda_ev_contenido_2" name="contenido" cols="1" rows="1"><?php echo $evento->contenido?></textarea></li>
				<li class="fright" style="margin-top:20px;">
					<button type="submit" data-translate-html="agenda.actualizar">
						Actualizar evento
					</button>
				</li>
			</ul>
			<div class="clear"></div>
		</form>
	</div>
</div>