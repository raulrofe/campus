<!-- cambiado el dia 18/06/2012 -->
<div id="agenda">
	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="agenda.titulo">
			AGENDA
		</span>
	</div>

	<br /><br />

	<?php if (Usuario::compareProfile(array('tutor', 'coordinador'))): ?>
		<!-- menu de iconos -->
		<div class="fleft panelTabMenu" id="menuAgendaAdmin">
			<?php if (Usuario::getIdUser() != 58): ?>
				<div id="menuAgendaAdmin_1" class="blanco redondearBorde">
					<a href="agenda">
						<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
						<span data-translate-html="agenda.personal">
							<br />Agenda<br />personal
						</span>
					</a>
				</div>
			<?php endif; ?>

			<div id="menuAgendaAdmin_2" class=" redondearBorde">
				<?php if (Usuario::getIdUser() != 58): ?>
					<a href="agenda/administrar">
						<img src="imagenes/agenda/menu_listado.png" alt="" />
						<span data-translate-html="agenda.curso">
							<br />Agenda<br />del curso
						</span>
					</a>
				<?php else: ?>
					<a href="agenda">
						<img src="imagenes/agenda/menu_listado.png" alt="" />
						<span data-translate-html="agenda.curso">
							<br />Agenda<br />del curso
						</span>
					</a>
				<?php endif; ?>
			</div>

		</div>
		<!-- Fin menu iconos -->
	<?php endif; ?>

	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAdminAgenda">
		<div id="agenda_vista">
			<div class="fleft" style="width:50%;">
				<div class="menuVista fleft t_left">
					<div class="fleft listBlack ActiveOption" >
						<a href="#" onclick="return false;" rel="tooltip" data-translate-title="agenda.listado" title="Listado de eventos"
						   data-translate-title="agenda.listado">
							<img src="imagenes/menu_vistas/list.png" alt="vista clasica"/>
						</a>
					</div>
					<div class="fleft t_right calendarBlack" >
						<a href="agenda" onclick="return false;"  rel="tooltip" title="Agenda"
						   data-translate-title="agenda.titulo">
							<img src="imagenes/menu_vistas/calendario.png"/>
						</a>
					</div>
					<div class="fleft t_right printBlack" >
						<a href="agenda/imprimir/eventos/<?php echo $tipoListado; ?>" target="_blank" rel="tooltip" title="Imprimir listado"
						   data-translate-title="agenda.imprimirlistado">
							<img src="imagenes/menu_vistas/imprimir.png"/>
						</a>
					</div>
				</div>

				<div class="fright t_right eventoAlumnoTitle">
					<strong data-translate-html="agenda.eventos">
						Eventos
					</strong>
				</div>
			</div>
			<div class="fright t_right" style="width:50%;">
				<select id="agendaTipoEvento_options" name="tipoEvento" onchange="agendaCambiarVista(this);">
					<option value="0" data-translate-html="agenda.todos">Todos los eventos</option>
					<?php if (Usuario::compareProfile(array('alumno', 'supervisor'))): ?>
						<option value="3" <?php if (isset($get['tipo']) && $get['tipo'] == 3) echo 'selected="selected"' ?> data-translate-html="agenda.ttutor">Eventos del tutor</option>
					<?php elseif (Usuario::compareProfile('tutor')): ?>
						<option value="4" <?php if (isset($get['tipo']) && $get['tipo'] == 4) echo 'selected="selected"' ?> data-translate-html="agenda.ecoordinador">Eventos del coordinador</option>
					<?php endif; ?>
					<option value="1" <?php if (isset($get['tipo']) && $get['tipo'] == 1) echo 'selected="selected"' ?> data-translate-html="agenda.tdia">Eventos del dia</option>
					<option value="2" <?php if (isset($get['tipo']) && $get['tipo'] == 2) echo 'selected="selected"' ?> data-translate-html="agenda.tmis">Mis eventos</option>
				</select>
			</div>
			<div class="clear"></div>

			<?php if ($get['c'] != 'modo_listado'): ?>
				<div class="leyendaCalendario">
					<?php if (Usuario::compareProfile(array('alumno', 'supervisor'))): ?>
						<span id="agenda_ayuda_color_3"></span>
						<span data-translate-html="agenda.eventos_tutor">Eventos creados por el tutor/a</span>
					<?php else: ?>
						<span id="agenda_ayuda_color_4"></span>
						<span data-translate-html="agenda.eventos_coordinador">Eventos creados por el coordinador/a</span>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php require_once mvc::obtenerRutaVista(dirname(__FILE__), 'vista'); ?>
		</div>
	</div>
</div>

<script type="text/javascript">/*agendaDiaOpen();*/</script>