<style>

p, table, td{
	margin:0;
	padding:0;	
}
b{font-weight: bold;}

/*CLASES GENERALES */
.v_middle{vertical-align: middle;}
.t_center{text-align:center;}
.t_left{text-align: left;}
.t_right{text-align: right;}
.small{font-size: 12px;}
.fleft{flaot:left;}
.borderBottom{border-bottom:2px solid #AAA;}
.borderRight{border-right:2px solid #AAA;}
.encabezado{
	border:2px solid #AAA;
	width: 100%;
	height: 25px;
	background-color:#EFEFEF;
}

.encabezado .nBox{
	border-right:2px solid #AAA;
	float:left;
	width:25px;
	text-align:center;
	font-weight: bold;
}

.encabezado .titleBox{
	float:left;
	height: 25px;
	width: 752px;
	text-align: center;
	font-weight: bold;
}

.encabezado td{vertical-align: middle;}

.contenido{
	border-left:2px solid #AAA;
	border-right:2px solid #AAA;
	border-bottom:2px solid #AAA;
	width: 100%;
	height: 25px;
	font-size: 12px;
}

.contenido tr td{padding:0;margin:0;}
.contenido td{padding:5px;}


.titleInform{
	font-size: 18px;
	font-weight: bold;
	text-align:center;
	margin-bottom:10px;
}

</style>

<page>
	<br/>
	<div class="t_center"><img src="imagenes/informe/ase.jpg" /></div>
	<br/>
	<div class="titleInform">
		<p>EVENTOS DEL CURSO</p>
	</div>
	
	<div id="eventosAgenda">
		<table cellpadding="0" cellspacing="0" class="encabezado">
			<tr>
				<td class="titleBox"><?php echo $tituloEventos; ?></td>
			</tr>
		</table>
		<?php if(count($eventosOrdenados) > 0):?>
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="borderRight" style="width:150px;"><b>Fecha</b></td>
					<td class="borderRight" style="width:150px;"><b>Tipo de evento</b></td>
					<td  style="width:384px;"><b>Descripci&oacute;n</b></td>
				</tr>
			</table>
			<?php foreach($eventosOrdenados as $evento):?>
				<table cellpadding="0" cellspacing="0" class="contenido">
					<tr>
						<td class="borderRight" style="width:150px;"><?php echo Fecha::obtenerFechaFormateada($evento['fecha'], false); ?></td>
						<td class="borderRight" style="width:150px;">
							<?php if($evento['recomendacion']  && !$evento['coordinador']):?>
								<?php echo 'Recomendacion del tutor/a para el curso'; ?>
							<?php elseif(!$evento['recomendacion']  && !$evento['coordinador']): ?>
								<?php echo 'Evento personal'; ?>
							<?php elseif(!$evento['recomendacion']  && $evento['coordinador']): ?>
								<?php echo 'Evento creado por el coordinador/a'; ?>
							<?php endif; ?>
						</td>
						<td style="width:384px;"><?php echo htmlentities($evento['contenido'], ENT_QUOTES, 'utf-8') ; ?></td>
					</tr>	
				</table>
			<?php endforeach; ?>
		<?php else: ?>
			<table cellpadding="0" cellspacing="0" class="contenido">
				<tr>
					<td class="t_center" style="width:732px;">No existen eventos</td>
				</tr>
			</table>
		<?php endif; ?>		
	</div>				
</page>

