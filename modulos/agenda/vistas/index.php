<!-- cambiado el dia 18/06/2012 -->
<div id="agenda">
	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="agenda.titulo">
			AGENDA
		</span>
	</div>

	<br /><br />

	<?php if (Usuario::compareProfile('tutor')): ?>
		<!-- menu de iconos -->
		<div class="fleft panelTabMenu" id="menuAgendaAdmin">
			<div id="menuAgendaAdmin_1" class="blanco redondearBorde">
				<a href="agenda">
					<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
					<span data-translate-html="agenda.personal">
						<br />Agenda<br />personal
					</span>
				</a>
			</div>
			<div id="menuAgendaAdmin_2" class=" redondearBorde">
				<a href="agenda/administrar">
					<img src="imagenes/agenda/menu_listado.png" alt="" />
					<span data-translate-html="agenda.curso">
						<br />Agenda<br />del curso
					</span>
				</a>
			</div>
		</div>
		<!-- Fin menu iconos -->
	<?php elseif (Usuario::compareProfile('coordinador')): ?>
		<!-- menu de iconos -->
		<div class="fleft panelTabMenu" id="menuAgendaAdmin">
			<div id="menuAgendaAdmin_1" class="blanco redondearBorde">
				<a href="agenda">
					<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
					<span data-translate-html="agenda.curso">
						<br />Agenda<br />del curso
					</span>
				</a>
			</div>
			<?php if (Usuario::getIdUser() != 58): ?>
				<div id="menuAgendaAdmin_2" class=" redondearBorde">
					<a href="agenda/administrar">
						<img src="imagenes/agenda/menu_listado.png" alt="" />
						<span data-translate-html="agenda.cursotutores">
							<br />Agenda<br />del curso<br /> para tutores
						</span>
					</a>
				</div>
			<?php endif; ?>
		</div>
		<!-- Fin menu iconos -->
	<?php endif; ?>

	<!-- Contenido tabs -->
	<?php if (Usuario::compareProfile(array('tutor', 'coordinador'))): ?>
		<?php require_once('contenido_agenda_tutor.php'); ?>
	<?php else: ?>
		<?php require_once('contenido_agenda_alumno.php') ?>
	<?php endif; ?>
</div>

<script type="text/javascript">/*agendaDiaOpen();*/</script>