<div style="width:580px;margin:0 auto">
	<div id="calendario_content">
		<div id="calendario">
			<div id="agenda_mes" class="t_center">
				<div class="fleft">
					<a href="agenda/<?php echo $mesAnterior?>/<?php echo $anioAnterior?>"
					    data-translate-title="agenda.mesanterior" title="Ir al mes anterior">
						<img src="imagenes/agenda/arrow_left.png"/>	
					</a>
				</div>
				<div class="nombreMes">
					<span id="agenda_mes_actual_num" class="hide"><?php echo mb_strtoupper($mesActual)?></span>
					<span><?php echo mb_strtoupper($meses[$mesActual - 1])?> <?php echo $anioActual;?></span>
				</div>
				<div class="fleft">
					<a href="agenda/<?php echo $mesSig?>/<?php echo $anioSig?>"
					   data-translate-title="agenda.messiguiente" title="Ir al mes siguiente">
						<img src="imagenes/agenda/arrow_right.png"/>	
					</a>
				</div>
			</div>
			<div class="clear"></div>
			
			<div id="agenda_dias_semana" class="clear">
				<?php foreach($diasSemana as $item_ds):?>
					<span><?php echo $item_ds?></span>
				<?php endforeach;?>
			</div>
			<div class="clear"></div>
			
			<ul id="agenda_dias" class="clear">
				<?php for($cont = 1; $cont <= $iniDia; $cont++):?>
					<li><span>--</span></li>
				<?php endfor;?>
					
				<?php for($cont = 1; $cont <= $dias; $cont++):?>
					<?php 
						if($iniDia == 7)
						{
							$addClass = 'clear';
							$iniDia = 0;
						}
						else 
						{					
							$addClass = '';
						}
						$iniDia++;
						
						$addClass2  = '';
						if($diaActual == $cont)
						{
							$addClass2 = 'agenda_dia_activo negrita';
						}
					?>
					<li class="<?php echo $addClass . ' ' . $addClass2?> <?php 
							if(isset($arEv[$cont], $arEvRcd[$cont])) echo 'agenda_dia_get_ev_mix';
							elseif(isset($arEv[$cont])) echo 'agenda_dia_get_ev';
							else if(isset($arEvRcd[$cont])) echo 'agenda_dia_get_ev_rcd';
							else if(isset($arEvCoord[$cont])) echo 'agenda_dia_get_ev_coord';
					?>">
						<a href="agenda/vista/<?php echo $cont . '/' . $mesActual . '/' . $anioActual?>" title="Ir a este d&iacute;a"><?php echo $cont?></a>
					</li>
				<?php endfor;?>
			</ul>
			
			<span id="agenda_mes_actual" class="hide"><?php echo $mesActual?></span>
			<span id="agenda_anio_actual" class="hide"><?php echo $anioActual?></span>
			
		</div>
	
		<?php if(!Usuario::compareProfile('supervisor')): ?>
			<div class="fright">
				<div class="crearEventoPersonal">
					<form id="frm_agenda_ev" method="post" action="" <?php if(!Usuario::compareProfile(array('tutor', 'coordinador'))) echo 'style="top: 24px;"'?>>
						<ul>
							<li><textarea id="frm_agenda_ev_contenido" name="contenido" cols="1" rows="1"></textarea></li>
							<li>
								<button type="submit" data-translate-html="agenda.crear">
									A&ntilde;adir a la agenda
								</button>
							</li>
						</ul>
						<div class="clear"></div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
		<?php endif; ?>
		<div class="clear"></div>
	</div>
	
	<?php if($get['c'] != 'modo_listado'):?>
		<div class="leyendaCalendario t_center" style="width:580px;">
			<span id="agenda_ayuda_color_1"></span>
			<span data-translate-html="agenda.d_seleccionado">
				D&iacute;a seleccionado
			</span>				
			<span id="agenda_ayuda_color_2"></span>
			<span data-translate-html="agenda.eventos_personales">
				Eventos personales
			</span>	
			<?php if(Usuario::compareProfile(array('alumno', 'supervisor'))):?>		
				<span id="agenda_ayuda_color_3"></span>
				<span data-translate-html="agenda.eventos_tutor">
					Eventos creados por el tutor/a
				</span>
			<?php elseif(Usuario::compareProfile('tutor')):?>
				<span id="agenda_ayuda_color_4"></span>
				<span data-translate-html="agenda.eventos_coordinador">
					Eventos creados por el coordinador/a
				</span>
			<?php endif;?>
		</div>
	<?php endif;?>
	
	<div id="agenda_vista">
		<br />
		<div>
			<div class="menuVista fleft">
				<div class="fleft listBlack" >
					<a href="agenda/modo/listado" onclick="return false;" rel="tooltip" data-translate-title="agenda.listado" title="Listado de eventos">
						<img src="imagenes/menu_vistas/list.png" alt="vista clasica"/>
					</a>
				</div>
				<div class="fleft calendarBlack ActiveOption" >
					<a href="#" onclick="return false;"  rel="tooltip" title="Agenda" data-translate-title="agenda.titulo">
						<img src="imagenes/menu_vistas/calendario.png"/>
					</a>
				</div>
				<div class="fleft printBlack" >
					<a href="agenda/imprimir/eventos_dia/<?php echo $diaActual ?>/<?php echo $mesActual ?>/<?php echo $anioActual ?>"  target="_blank" rel="tooltip" title="Imprimir eventos del d&iacute;a"
					    data-translate-title="agenda.imprimir">
						<img src="imagenes/menu_vistas/imprimir.png" alt="vista"/></a>
				</div>
			</div>
			<div class="fright t_right" style="margin-top:7px;">
				<strong data-translate-html="agenda.eventos">
					Eventos
				</strong>
			</div>
		</div>
		<div class="clear"></div>
		<?php Fichero::importar(PATH_ROOT . 'modulos/agenda/controladores/vista.php');?></div>

</div>

