<div id="agenda_eventos" <?php if(Usuario::compareProfile(array('alumno', 'supervisor'))) echo 'style="width:580px"';?>>
	<div id="agenda_ev_listado" class="listarElementos <?php if($get['c'] == 'modo_listado') echo 'agenda_evento_no_color'?>">

		<?php if(count($eventosOrdenados) > 0):?>
			<?php foreach($eventosOrdenados as $ev):?>
				<?php if($ev['recomendacion']):?>
					<div class="elementoRecomendacion">
						<?php if($get['c'] == 'modo_listado'):?>
							<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($ev['fecha'], false)?>">
				  				<?php echo date('d', strtotime($ev['fecha']))?>
				  				<div class="elementoFecha2_hr"></div>
				  				<?php echo date('M', strtotime($ev['fecha']))?>
				  			</div>
				  		<?php endif;?>

						<div class="anotacion"><?php echo Texto::textoFormateado($ev['contenido'])?></div>

						<div class="pieAgenda">
							<div class="t_right">
								<strong data-translate-html="agenda.tutor">Tutor/a:</strong> 
								<strong><?php echo Texto::textoPlano($ev['creador'])?></strong>
							</div>
						</div>
					</div>
				<?php elseif($ev['coordinador']):?>
					<div class="elementoCoordinador">

						<?php if($get['c'] == 'modo_listado'):?>
							<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($ev['fecha'], false)?>">
				  				<?php echo date('d', strtotime($ev['fecha']))?>
				  				<div class="elementoFecha2_hr"></div>
				  				<?php echo date('M', strtotime($ev['fecha']))?>
				  			</div>
				  		<?php endif;?>

						<div class="anotacion"><?php echo Texto::textoFormateado($ev['contenido'])?></div>
						<div class="pieAgenda">
							<div class="t_right">
								<strong data-translate-html="agenda.coordinador">Coordinador/a:</strong>
								<strong><?php echo Texto::textoPlano($ev['creador'])?></strong>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="elementoPersonal">

						<?php if($get['c'] == 'modo_listado'):?>
							<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($ev['fecha'], false)?>">
				  				<?php echo date('d', strtotime($ev['fecha']))?>
				  				<div class="elementoFecha2_hr"></div>
				  				<?php echo date('M', strtotime($ev['fecha']))?>
				  			</div>
				  		<?php endif;?>

			  			<div class="anotacion"><?php echo Texto::textoFormateado($ev['contenido'])?></div>
						<div class="clear"></div>

						<div class="elementoPie">
							<ul class="elementoListOptions fright hide">
								<li>
									<a href="agenda/evento/editar/<?php echo $ev['idagenda']?>" 
									   title="Editar este evento de la agenda" data-translate-title="agenda.editarevento">
										<img src="imagenes/foro/editar.png" alt=""/>
										<span data-translate-html="general.editar">
											Editar
										</span>
									</a>
								</li>
								<li>
									<a asd  href="#" 
										<?php echo Alerta::alertConfirmOnClick('borraragenda','¿Deseas borrar este evento de la agenda?', 'agenda/evento/borrar/' . $ev['idagenda']);?> 
									   title="Borrar este evento de la agenda" data-translate-title="agenda.borrarevento">
										<img src="imagenes/foro/borrar.png" alt=""/>
										<span data-translate-html="general.borrar">
											Borrar
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				<?php endif;?>
				<div class="clear"></div>
			<?php endforeach;?>
		<?php else:?>
			<div class="t_center">
				<strong data-translate-html="agenda.noeventos">
					No tienes eventos este d&iacute;a
				</strong>
			</div>
		<?php endif;?>
	</div>
</div>