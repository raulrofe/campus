function agendaCambiarVista(element)
{
	var value = $(element).val();
	
	if(value == '0')
	{
		LibPopupModal.loadPage(element, 'agenda/modo/listado');
	}
	else
	{
		LibPopupModal.loadPage(element, 'agenda/modo/listado/filtrar/' + value);
	}
}