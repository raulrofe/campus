<?php

//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');
$html2Pdf = new HTML2PDF();
$objModelo = new ModeloAgenda();

$get = Peticion::obtenerGet();

if(isset($get['fechaActual']))
{
	$fechaActual = $get['fechaActual']; 	
}
else
{
	$fechaActual = date('Y-m-d');	
}
	
if(isset($get['idSelection']) && is_numeric($get['idSelection']))
{

	switch($get['idSelection'])
	{
		//todos los eventos
		case 0:
			$tituloEventos = 'Todos los enventos';
			$eventos = $objModelo->obtenerTodosEventos(Usuario::getIdUser(true), Usuario::getIdCurso());
			if(Usuario::compareProfile(array('alumno', 'supervisor')))
			{
				$eventosRecomendacion = $objModelo->obtenerTodosEventosRecomendacion(Usuario::getIdCurso());
			}
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				$eventosCoordinador = $objModelo->obtenerTodosEventosCoordinador(Usuario::getIdCurso());
			}
			break;
		// eventos del dia
		case 1:	
			$tituloEventos = 'Eventos del d&iacute;a';
			$eventos = $objModelo->obtenerEventosPorDia(Usuario::getIdUser(true), Usuario::getIdCurso(), date('Y-m-d'));
			
			if(Usuario::compareProfile(array('alumno', 'supervisor')))
			{
				$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorDia(Usuario::getIdCurso(), date('Y-m-d'));
			}
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorDia(Usuario::getIdCurso(), date('Y-m-d'));
			}
			break;
		// eventos mios (todos)
		case 2:
			$tituloEventos = 'Mis eventos';
			$eventos = $objModelo->obtenerTodosEventos(Usuario::getIdUser(true), Usuario::getIdCurso());
			break;
		// eventos de recomendacion (todos)
		case 3:
			$tituloEventos = 'Eventos del tutor/a';
			if(Usuario::compareProfile(array('alumno', 'supervisor')))
			{
				$eventosRecomendacion = $objModelo->obtenerTodosEventosRecomendacion(Usuario::getIdCurso());
			}
			break;
		// eventos de coordinador (todos)
		case 4:
			$tituloEventos = 'Eventos del cordiandor/a';
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				$eventosCoordinador = $objModelo->obtenerTodosEventosCoordinador(Usuario::getIdCurso());
			}
			break;
		default:
			break;
	}
}
if(isset($get['dia'], $get['mes'], $get['anio']) && is_numeric($get['dia']) && is_numeric($get['mes']) && is_numeric($get['anio']))
{
	$fechaDia = $get['anio'] . '-' . $get['mes'] . '-' . $get['dia'];
	$tituloEventos = 'Eventos del d&iacute;a';
	$eventos = $objModelo->obtenerEventosPorDia(Usuario::getIdUser(true), Usuario::getIdCurso(), $fechaDia);
	
	if(Usuario::compareProfile(array('alumno', 'supervisor')))
	{
		$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorDia(Usuario::getIdCurso(), $fechaDia);
	}
	if(Usuario::compareProfile(array('tutor', 'coordinador')))
	{
		$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorDia(Usuario::getIdCurso(), $fechaDia);
	}	
}

$eventosOrdenados = array();
if(isset($eventos) && $eventos->num_rows > 0)
{
	while($evento = $eventos->fetch_object())
	{
		$eventosOrdenados[] = array(
			'idagenda' => $evento->idagenda,
			'contenido' => $evento->contenido,
			'fecha' => $evento->fecha,
			'recomendacion' => false,
			'coordinador' => false,
			'creador' => ''
		);
	}
}

if(isset($eventosRecomendacion) && $eventosRecomendacion->num_rows > 0)
{
	while($evento = $eventosRecomendacion->fetch_object())
	{
		$eventosOrdenados[] = array(
			'idagenda'		=> $evento->idagenda_recomendacion,
			'contenido'		=> $evento->contenido,
			'fecha'			=> $evento->fecha,
			'recomendacion'	=> true,
			'coordinador'	=> false,
			'creador'		=> $evento->nombrec
		);
	}
}

if(Usuario::compareProfile(array('tutor')))
{
	if(isset($eventosCoordinador) && $eventosCoordinador->num_rows > 0)
	{
		while($evento = $eventosCoordinador->fetch_object())
		{
			$eventosOrdenados[] = array(		
				'idagenda' 			=> $evento->idagenda_coordinador,
				'contenido' 		=> $evento->contenido,
				'fecha' 			=> $evento->fecha,
				'recomendacion' 	=> false,
				'coordinador' 		=> true,
				'creador' 			=> $evento->nombrec
			);
		}
	}
}

$eventosOrdenados = Fecha::orderByDate($eventosOrdenados);

//var_dump($eventosOrdenados);

// Una vez terminado el proceso de creacion del array para generar el pdf nos ponemos a generarlo
// Recuperacion del contenido HTML
ob_start();
include(PATH_ROOT . 'modulos/agenda/vistas/imprimir/imprimir_eventos.php');
$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
	//$html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	
	$html2pdf->Output();
}
catch(HTML2PDF_exception $e) {echo $e;}