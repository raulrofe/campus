<?php
$get = Peticion::obtenerGet();

$fechaActual = date('Y-m-d');

$objModelo = new ModeloAgenda();

// todos los eventos
if(!isset($get['tipo']))
{
	$tipoListado = 0;
	$eventos = $objModelo->obtenerTodosEventos(Usuario::getIdUser(true), Usuario::getIdCurso());
	if(Usuario::compareProfile(array('alumno', 'supervisor', 'coordinador')))
	{
		$eventosRecomendacion = $objModelo->obtenerTodosEventosRecomendacion(Usuario::getIdCurso());
	}
	if(Usuario::compareProfile(array('tutor', 'coordinador')))
	{
		$eventosCoordinador = $objModelo->obtenerTodosEventosCoordinador(Usuario::getIdCurso());
	}
}
else
{
	switch($get['tipo'])
	{
		// eventos del dia
		case 1:
			$tipoListado = 1;
			$eventos = $objModelo->obtenerEventosPorDia(Usuario::getIdUser(true), Usuario::getIdCurso(), date('Y-m-d'));

			if(Usuario::compareProfile(array('alumno', 'supervisor', 'coordinador')))
			{
				$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorDia(Usuario::getIdCurso(), date('Y-m-d'));
			}
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorDia(Usuario::getIdCurso(), date('Y-m-d'));
			}
			break;
		// eventos mios (todos)
		case 2:
			$tipoListado = 2;
			$eventos = $objModelo->obtenerTodosEventos(Usuario::getIdUser(true), Usuario::getIdCurso());
			break;
		// eventos de recomendacion (todos)
		case 3:
			$tipoListado = 3;
			if(Usuario::compareProfile(array('alumno', 'supervisor')))
			{
				$eventosRecomendacion = $objModelo->obtenerTodosEventosRecomendacion(Usuario::getIdCurso());
			}
			break;
		// eventos de coordinador (todos)
		case 4:
			$tipoListado = 4;
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				$eventosCoordinador = $objModelo->obtenerTodosEventosCoordinador(Usuario::getIdCurso());
			}
			break;
		default:
			break;
	}

}

$eventosOrdenados = array();
if(isset($eventos) && $eventos->num_rows > 0)
{
	while($evento = $eventos->fetch_object())
	{
		$eventosOrdenados[] = array(
			'idagenda' => $evento->idagenda,
			'contenido' => $evento->contenido,
			'fecha' => $evento->fecha,
			'recomendacion' => false,
			'coordinador' => false,
			'creador' => ''
		);
	}
}

if(isset($eventosRecomendacion) && $eventosRecomendacion->num_rows > 0)
{
	while($evento = $eventosRecomendacion->fetch_object())
	{
		$eventosOrdenados[] = array(
			'idagenda' => $evento->idagenda_recomendacion,
			'contenido' => $evento->contenido,
			'fecha' => $evento->fecha,
			'recomendacion' => true,
			'coordinador' => false,
			'creador' => $evento->nombrec
		);
	}
}

if(Usuario::compareProfile(array('tutor')))
{
	if(isset($eventosCoordinador) && $eventosCoordinador->num_rows > 0)
	{
		while($evento = $eventosCoordinador->fetch_object())
		{
			$eventosOrdenados[] = array(
				'idagenda' => $evento->idagenda_coordinador,
				'contenido' => $evento->contenido,
				'fecha' => $evento->fecha,
				'recomendacion' => false,
				'coordinador' => true,
				'creador' => $evento->nombrec
			);
		}
	}
}

$eventosOrdenados = Fecha::orderByDate($eventosOrdenados);


//Cargo la vista correspondiente al tutor o al alumno
if(Usuario::compareProfile(array('tutor', 'coordinador')))
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'modo_listado_tutor');
}
else
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'modo_listado_alumno');
}