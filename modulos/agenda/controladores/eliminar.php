<?php
$objModelo = new ModeloAgenda();

$get = Peticion::obtenerGet();
if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	if(Usuarios::comprobarPermisosLogueo(Usuario::getIdUser(true)))
	{
		$evento = $objModelo->obtenerUnEvento($get['idMsg']);
		if($evento->num_rows == 1)
		{
			if($objModelo->eliminarEvento($get['idMsg']))
			{
				$evento = $evento->fetch_object();
				
				Url::redirect('agenda/vista/' . date('d/m/Y', strtotime($evento->fecha)));
			}
		}
	}
}