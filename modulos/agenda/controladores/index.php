<?php
// fecha sobre la que se basa el calendario
$get = Peticion::obtenerGet();
if(isset($get['mes'], $get['anio'])
	&& is_numeric($get['mes']) && is_numeric($get['anio'])
		&& (strlen($get['mes']) == 1 || strlen($get['mes'])) == 2 && strlen($get['anio']) == 4)
{
	$fechaActual = strtotime($get['anio'] . '-' . $get['mes'] . '-01');
}
else
{
	$fechaActual = time();
}

// ----  PARAMETROS DE FECHAS PARA LA NEVEGACION DEL CALENDARIO  ----- /
// calcula el mes
$mesActual = intval(date('m', $fechaActual));
$mesAnterior = $mesActual - 1;
$mesSig = $mesActual + 1;

// calcula el anio
$anioActual = intval(date('Y', $fechaActual));
$anioAnterior = $anioActual;
$anioSig = $anioActual;

// si es el primer mes ...
if($mesActual == 1)
{
	$anioAnterior = $anioActual - 1;
	$mesAnterior = 12;
}
// si es el ultimo mes ...
else if($mesActual == 12)
{
	$anioSig = $anioActual + 1;
	$mesSig = 1;
}
/* ------------------------- */

// valores de cadena de dias de la semana y meses
$diasSemana = array('LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM');
$meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

// nuemros de dias del mes
$dias = intval(date('t', $fechaActual));

// calcula el dia inicial con respecto al dia de la semana en que que empieza el calendario
$iniDia = intval(date('N', strtotime($anioActual . '-' . $mesActual . '-01'))) - 1;

// Dia actual para ponero marcado por defecto y abrirlo
if(isset($get['dia']) && is_numeric($get['dia']))
{
	$diaActual = $get['dia'];
}
else if($mesActual == date('m'))
{
	$diaActual = date('d');
}
else
{
	$diaActual = null;
}

// eventos del mes
$objModelo = new ModeloAgenda();
$eventos = $objModelo->obtenerEventosPorMes(Usuario::getIdUser(true), Usuario::getIdCurso(), $anioActual . '-' . $mesActual . '-01', $anioActual . '-' . $mesActual . '-' . $dias);
$arEv = array();
if($eventos->num_rows > 0)
{
	while($evento = $eventos->fetch_object())
	{
		$arEv[date(date('j', strtotime($evento->fecha)))] = true;
	}
}

if(Usuario::compareProfile(array('alumno', 'supervisor', 'coordinador')))
{
	// eventos de recomendacion por mes
	$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorMes(Usuario::getIdCurso(), $anioActual . '-' . $mesActual . '-01', $anioActual . '-' . $mesActual . '-' . $dias);
	$arEvRcd = array();
	if($eventosRecomendacion->num_rows > 0)
	{
		while($eventoRecomendacion = $eventosRecomendacion->fetch_object())
		{
			$arEvRcd[date(date('j', strtotime($eventoRecomendacion->fecha)))] = true;
		}
	}
}

if(Usuario::compareProfile(array('tutor', 'supervisor')))
{
	// eventos de recomendacion por mes
	$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorMes(Usuario::getIdCurso(), $anioActual . '-' . $mesActual . '-01', $anioActual . '-' . $mesActual . '-' . $dias);
	$arEvCoord = array();
	if($eventosCoordinador->num_rows > 0)
	{
		while($eventoCoordinador = $eventosCoordinador->fetch_object())
		{
			$arEvCoord[date(date('j', strtotime($eventoCoordinador->fecha)))] = true;
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');