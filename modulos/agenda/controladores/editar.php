<?php
$objModelo = new ModeloAgenda();

$get = Peticion::obtenerGet();

if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	if(Usuarios::comprobarPermisosLogueo(Usuario::getIdUser(true)))
	{
		$evento = $objModelo->obtenerUnEvento($get['idMsg']);
		if($evento->num_rows == 1)
		{
			$evento = $evento->fetch_object();
			
			$elementDate = explode('-', $evento->fecha);
			
			//obtengo texto con el mes
			$mes = Fecha::mes($elementDate[1]);

			//obteno texto con el dia
			$semana = date('w', mktime(0,0,0,$elementDate[1], $elementDate[2], $elementDate[0])); 		
			$dia = Fecha::dias($semana);
			
			
			if(Peticion::isPost())
			{
				$post = Peticion::obtenerPost();
				if(isset($post['contenido']) && !empty($post['contenido']))
				{
					$objModelo->actualizarEvento($get['idMsg'], $post['contenido']);
					
					Url::redirect('agenda/vista/' . date('d/m/Y', strtotime($evento->fecha)));
				}
			}
			
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar');
		}
	}
}