<?php
class ModeloAgenda extends modeloExtend
{
	public function nuevoEvento($idUsuario, $idCurso, $contenido, $fecha)
	{
		$sql = 'INSERT INTO agenda (idcurso, idusuario, contenido, fecha) VALUES (' . $idCurso . ', "' . $idUsuario . '", "' . $contenido . '", "' . $fecha . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function nuevoAgendaBlocNota($idagenda, $idusuario)
	{
		$sql = 'INSERT INTO bloc_notas_agenda (idagenda, idusuario) VALUES (' . $idagenda . ', "' . $idusuario . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarEvento($idEvento, $contenido)
	{
		$sql = 'UPDATE agenda SET  contenido = "' . $contenido . '" WHERE idagenda = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosPorDia($idUsuario, $idCurso, $fecha)
	{
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha FROM agenda AS ag' .
		' WHERE ag.idusuario = "' . $idUsuario . '" AND idcurso = ' . $idCurso . ' AND ag.fecha = "' . $fecha . '"' .
		' GROUP BY ag.idagenda ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerTodosEventos($idUsuario, $idCurso)
	{
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha' .
		' FROM agenda AS ag' .
		' WHERE ag.idusuario = "' . $idUsuario . '" AND idcurso = ' . $idCurso . 
		' GROUP BY ag.idagenda ASC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	public function obtenerTodosEventosRecomendacion($idCurso)
	{
		$sql = 'SELECT agr.idagenda_recomendacion, agr.contenido, agr.fecha, rh.nombrec' .
		' FROM agenda_recomendacion AS agr' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = agr.idrrhh' .
		' WHERE agr.id_recomendacion = cr.idrecomendacion' .
		' GROUP BY agr.idagenda_recomendacion ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerTodosEventosCoordinador($idCurso)
	{
		$sql = 'SELECT agc.idagenda_coordinador, agc.contenido, agc.fecha, rh.nombrec' .
		' FROM agenda_coordinador AS agc' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = agc.idrrhh' .
		' WHERE agc.id_conv = cr.idconvocatoria' .
		' GROUP BY agc.idagenda_coordinador ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosPorMes($idUsuario, $idCurso, $fechaIni, $fechaFin)
	{
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha FROM agenda AS ag' .
		' WHERE ag.idusuario = "' . $idUsuario . '" AND idcurso = ' . $idCurso . ' AND ag.fecha >= "' . $fechaIni . '" AND ag.fecha <= "' . $fechaFin . '"' .
		' GROUP BY ag.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosRecomendacionPorDia($idCurso, $fecha)
	{
		$sql = 'SELECT agr.idagenda_recomendacion, agr.contenido, agr.fecha, rh.nombrec FROM agenda_recomendacion AS agr' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = agr.idrrhh' .
		' WHERE agr.id_recomendacion = cr.idrecomendacion AND agr.fecha = "' . $fecha . '"' .
		' GROUP BY agr.idagenda_recomendacion ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerEventosCoordinadorPorDia($idCurso, $fecha)
	{
		$sql = 'SELECT agc.idagenda_coordinador, agc.contenido, agc.fecha, rh.nombrec FROM agenda_coordinador AS agc' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = agc.idrrhh' .
		' WHERE agc.id_conv = cr.idconvocatoria AND agc.fecha = "' . $fecha . '"' .
		' GROUP BY agc.idagenda_coordinador ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosRecomendacionPorMes($idCurso, $fechaIni, $fechaFin)
	{
		$sql = 'SELECT agr.idagenda_recomendacion AS idagenda, agr.contenido, agr.fecha FROM agenda_recomendacion AS agr' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' WHERE agr.id_recomendacion = cr.idrecomendacion AND agr.fecha >= "' . $fechaIni . '" AND agr.fecha <= "' . $fechaFin . '"' .
		' GROUP BY agr.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosCoordinadorPorMes($idCurso, $fechaIni, $fechaFin)
	{
		$sql = 'SELECT agc.idagenda_coordinador AS idagenda, agc.contenido, agc.fecha FROM agenda_coordinador AS agc' .
		' LEFT JOIN curso AS cr ON cr.idcurso = ' . $idCurso .
		' WHERE agc.id_conv = cr.idconvocatoria AND agc.fecha >= "' . $fechaIni . '" AND agc.fecha <= "' . $fechaFin . '"' .
		' GROUP BY agc.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
		
	public function obtenerUnEvento($idEvento)
	{
		$sql = 'SELECT ag.idagenda, ag.contenido, ag.fecha FROM agenda AS ag' .
		' WHERE ag.idagenda = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarEvento($idEvento)
	{
		$sql = 'DELETE FROM agenda WHERE idagenda = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}