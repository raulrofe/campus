<?php
class ModeloAgendaAdmin extends modeloExtend
{
	public function nuevoEventoRecomendacion($idRecomendacion, $idrrhh, $contenido, $fecha)
	{
		$sql = 'INSERT INTO agenda_recomendacion (id_recomendacion, idrrhh, contenido, fecha) VALUES (' . $idRecomendacion . ', ' . $idrrhh . ', "' . $contenido . '", "' . $fecha . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function nuevoEventoCoordinador($idConv, $idrrhh, $contenido, $fecha)
	{
		$sql = 'INSERT INTO agenda_coordinador (id_conv, idrrhh, contenido, fecha) VALUES (' . $idConv .  ', ' . $idrrhh . ', "' . $contenido . '", "' . $fecha . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarEventoCoordinador($idEvento, $contenido, $fecha)
	{
		$sql = 'UPDATE agenda_coordinador SET contenido = "' . $contenido . '", fecha = "' . $fecha . '" WHERE idagenda_coordinador = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarEventoRecomendacion($idEvento, $contenido, $fecha)
	{
		$sql = 'UPDATE agenda_recomendacion SET contenido = "' . $contenido . '", fecha = "' . $fecha . '" WHERE idagenda_recomendacion = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerEventosCoordinadora($idConv)
	{
		$sql = 'SELECT agr.idagenda_coordinador AS idagenda_recomendacion, agr.contenido, agr.fecha FROM agenda_coordinador AS agr' .
		' WHERE agr.id_conv = ' . $idConv .
		' ORDER BY agr.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtenerEventosPorRecomendacion($idRecomendacion)
	{
		$sql = 'SELECT agd.idagenda_recomendacion, agd.id_recomendacion, agd.contenido, agd.fecha FROM agenda_recomendacion AS agd' .
		' WHERE agd.id_recomendacion = ' . $idRecomendacion .
		' ORDER BY agd.fecha ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnEventoRecomendacion($idEvento)
	{
		$sql = 'SELECT agd.idagenda_recomendacion, agd.id_recomendacion, agd.contenido, agd.fecha FROM agenda_recomendacion AS agd' .
		' WHERE agd.idagenda_recomendacion = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnEventoCoordinador($idEvento)
	{
		$sql = 'SELECT agr.idagenda_coordinador AS idagenda_recomendacion, agr.contenido, agr.fecha, agr.id_conv FROM agenda_coordinador AS agr' .
		' WHERE agr.idagenda_coordinador = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerListadoRecomendaciones()
	{
		$sql = 'SELECT rd.idrecomendacion, rd.titulo_recomendacion AS titulo, rd.descripcion' .
		' FROM recomendacion AS rd' .
		' ORDER BY titulo DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarEventoRecomendacion($idEvento)
	{
		$sql = 'DELETE FROM agenda_recomendacion WHERE idagenda_recomendacion = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarEventoCoordinador($idEvento)
	{
		$sql = 'DELETE FROM agenda_coordinador WHERE idagenda_coordinador = ' . $idEvento;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	// RECOMENDACIONES //
	public function addRecomendacion($titulo, $descripcion)
	{
		$sql = 'INSERT INTO recomendacion (titulo_recomendacion, descripcion) VALUES ("' . $titulo . '", "' . $descripcion . '")';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function updateRecomendacion($idrecomendacion, $titulo, $descripcion)
	{
		$sql = 'UPDATE recomendacion SET titulo_recomendacion = "' . $titulo . '", descripcion = "' . $descripcion . '" WHERE idrecomendacion = ' . $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRecomendacion($idrecomendacion)
	{
		$sql = 'SELECT idrecomendacion, titulo_recomendacion AS titulo, descripcion FROM recomendacion WHERE idrecomendacion = '. $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarRecomendacion($idrecomendacion)
	{
		$sql = 'DELETE FROM recomendacion WHERE idrecomendacion = ' . $idrecomendacion;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}