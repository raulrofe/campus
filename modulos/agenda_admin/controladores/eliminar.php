<?php
$objModelo = new ModeloAgendaAdmin();

$get = Peticion::obtenerGet();
if(isset($get['idRcd']) && is_numeric($get['idRcd']))
{
	if(Usuario::compareProfile('coordinador'))
	{	
		$evento = $objModelo->obtenerUnEventoCoordinador($get['idRcd']);
	}
	else
	{
		$evento = $objModelo->obtenerUnEventoRecomendacion($get['idRcd']);
	}
	
	if($evento->num_rows == 1)
	{
		if(Usuario::compareProfile('coordinador'))
		{
			$objModelo->eliminarEventoCoordinador($get['idRcd']);
		}
		else
		{
			$objModelo->eliminarEventoRecomendacion($get['idRcd']);
		}
		
		$evento = $evento->fetch_object();
	
		Alerta::guardarMensajeInfo('eventoeliminado','Se ha eliminado el evento');
		Url::redirect('agenda/administrar');
	}
}

Url::redirect('agenda/administrar');