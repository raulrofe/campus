<?php
$objModelo = new ModeloAgendaAdmin();

if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
	if(isset($post['contenido'], $post['fecha']) && !empty($post['contenido']))
	{
		$objCurso = new Cursos();
		$objCurso->set_curso(Usuario::getIdCurso());
		$curso = $objCurso->buscar_curso();
		$curso = $curso->fetch_object();
		
		$idUsuario = Usuario::getIdUser();
		
		if(isset($curso->idrecomendacion))
		{	
			$arrayFecha = explode('/', $post['fecha']);
			if(is_array($arrayFecha) && count($arrayFecha) == 3)
			{
				// comprueba si la fecha es valida
				if(checkdate($arrayFecha[1], $arrayFecha[0], $arrayFecha[2]))
				{
					// formatea la fecha para insertarla
					$fecha = $arrayFecha[2] . '-' . $arrayFecha[1] . '-' . $arrayFecha[0];
					
					if(Usuario::compareProfile('coordinador'))
					{						
						$objModelo->nuevoEventoCoordinador($curso->idconvocatoria, $idUsuario, $post['contenido'], $fecha);
					}
					else
					{
						$objModelo->nuevoEventoRecomendacion($curso->idrecomendacion, $idUsuario, $post['contenido'], $fecha);
					}
					
					Alerta::guardarMensajeInfo('eventocreado','Se ha creado el evento');
					Url::redirect('agenda/administrar');
				}
			}
		}
		else
		{
			Alerta::mostrarMensajeInfo('norecomendacion','Este curso no tiene asignada una recomendaci&oacute;n');
		}
	}
}

$recomendaciones = $objModelo->obtenerListadoRecomendaciones();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevo');
