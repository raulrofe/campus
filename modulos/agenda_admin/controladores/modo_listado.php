<?php
$objModelo = new ModeloAgendaAdmin();
$objCurso = new Cursos();

$objCurso->set_curso(Usuario::getIdCurso());
$curso = $objCurso->buscar_curso();
$curso = $curso->fetch_object();

if(Usuario::compareProfile('coordinador'))
{
	if(isset($curso->idconvocatoria))
	{
		$eventos = $objModelo->obtenerEventosCoordinadora($curso->idconvocatoria);
	}
}
else
{
	if(isset($curso->idrecomendacion))
	{
		$eventos = $objModelo->obtenerEventosPorRecomendacion($curso->idrecomendacion);
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'modo_listado');