<?php
mvc::cargarModeloAdicional('agenda');
$objModelo = new ModeloAgenda();
$perfil = Usuario::getProfile();

// fecha sobre la que se basa el calendario
$get = Peticion::obtenerGet();

if(isset($get['mes'], $get['anio'])
		&& is_numeric($get['mes']) && is_numeric($get['anio'])
		&& (strlen($get['mes']) == 1 || strlen($get['mes'])) == 2 && strlen($get['anio']) == 4)
{
	if(isset($get['dia']) && is_numeric($get['dia']) && (strlen($get['dia']) == 1 || strlen($get['dia']) == 2))
	{
		
	}
	else
	{
		$get['dia'] = 1;
	}
	
	// valida la fecha
	if(checkdate($get['mes'], $get['dia'], $get['anio']))
	{
		$fechaActual = $get['anio'] . '-' . $get['mes'] . '-' . $get['dia'];	
	}
	else
	{
		$fechaActual = date('Y-m-d');
	}
}
else
{
	$fechaActual = date('Y-m-d');
}
$fechaActual = strtotime($fechaActual);

// ----  PARAMETROS DE FECHAS PARA LA NEVEGACION DEL CALENDARIO  ----- /
// calcula el mes
$mesActual = intval(date('m', $fechaActual));
$mesAnterior = $mesActual - 1;
$mesSig = $mesActual + 1;

// calcula el anio
$anioActual = intval(date('Y', $fechaActual));
$anioAnterior = $anioActual;
$anioSig = $anioActual;

// si es el primer mes ...
if($mesActual == 1)
{
	$anioAnterior = $anioActual - 1;
	$mesAnterior = 12;
}
// si es el ultimo mes ...
else if($mesActual == 12)
{
	$anioSig = $anioActual + 1;
	$mesSig = 1;
}

/* ------------------------- */
			
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
		
	// valida el campo fecha y que no este ninguno vacio
	if(isset($post['contenido']) && !empty($post['contenido']) )
	{
		// ID del usuario
		$idUsuario = Usuario::getIdUser(true);
				
		$objModeloAdmin = New ModeloAgendaAdmin();
		
		$objCurso = new Cursos();
		$objCurso->set_curso(Usuario::getIdCurso());
		$curso = $objCurso->buscar_curso();
		$curso = $curso->fetch_object();
		
		if(isset($curso->idrecomendacion))
		{
			// inserta el evento nuevo
			if(Usuario::compareProfile('coordinador'))
			{
				$objModeloAdmin->nuevoEventoCoordinador($curso->idconvocatoria, Usuario::getIdUser(), $post['contenido'], date('Y-m-d', $fechaActual));
			}
			else
			{
				$objModeloAdmin->nuevoEventoRecomendacion($curso->idrecomendacion, Usuario::getIdUser(), $post['contenido'], date('Y-m-d', $fechaActual));
			}
		}
		
		Url::redirect('agenda/administrar/vista/' . date('d', $fechaActual) . '/' . date('m', $fechaActual) . '/' . date('Y', $fechaActual));
	}
}

/* ------------------------- */

// valores de cadena de dias de la semana y meses
$diasSemana = array('LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM');
$meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

// nuemros de dias del mes
$dias = intval(date('t', $fechaActual));

// calcula el dia inicial con respecto al dia de la semana en que que empieza el calendario
$iniDia = intval(date('N', strtotime($anioActual . '-' . $mesActual . '-01'))) - 1;

// Dia actual para ponero marcado por defecto y abrirlo
if(isset($get['dia']) && is_numeric($get['dia']))
{
	$diaActual = $get['dia'];
}
else if($mesActual == date('m'))
{
	$diaActual = date('d');
}
else
{
	$diaActual = null;
}

// eventos de recomendacion por mes
$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorMes(Usuario::getIdCurso(), $anioActual . '-' . $mesActual . '-01', $anioActual . '-' . $mesActual . '-' . $dias);
$arEvRcd = array();
if($eventosRecomendacion->num_rows > 0)
{
	while($eventoRecomendacion = $eventosRecomendacion->fetch_object())
	{
		$arEvRcd[date(date('j', strtotime($eventoRecomendacion->fecha)))] = true;
	}
}

if(Usuario::compareProfile(array('tutor', 'coordinador')))
{
	// eventos de recomendacion por mes
	$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorMes(Usuario::getIdCurso(), $anioActual . '-' . $mesActual . '-01', $anioActual . '-' . $mesActual . '-' . $dias);
	$arEvCoord = array();
	if($eventosCoordinador->num_rows > 0)
	{
		while($eventoCoordinador = $eventosCoordinador->fetch_object())
		{
			$arEvCoord[date(date('j', strtotime($eventoCoordinador->fecha)))] = true;
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');