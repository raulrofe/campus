<?php
$objModelo = new ModeloAgendaAdmin();

$get = Peticion::obtenerGet();
if(isset($get['idRcd']) && is_numeric($get['idRcd']))
{
	if(Usuario::compareProfile('coordinador'))
	{
		$evento = $objModelo->obtenerUnEventoCoordinador($get['idRcd']);
	}
	else
	{
		$evento = $objModelo->obtenerUnEventoRecomendacion($get['idRcd']);
	}
	
	if($evento->num_rows == 1)
	{
		$evento = $evento->fetch_object();
			
		if(Peticion::isPost())
		{
			$post = Peticion::obtenerPost();
			if(isset($post['contenido'], $post['fecha']) && !empty($post['contenido']))
			{
				$arrayFecha = explode('/', $post['fecha']);
				if(is_array($arrayFecha) && count($arrayFecha) == 3)
				{
					// comprueba si la fecha es valida
					if(checkdate($arrayFecha[1], $arrayFecha[0], $arrayFecha[2]))
					{
						// formatea la fecha para insertarla
						$fecha = $arrayFecha[2] . '-' . $arrayFecha[1] . '-' . $arrayFecha[0];

						if(Usuario::compareProfile('coordinador'))
						{
							$objModelo->actualizarEventoCoordinador($get['idRcd'], $post['contenido'], $fecha);
						}
						else
						{
							$objModelo->actualizarEventoRecomendacion($get['idRcd'], $post['contenido'], $fecha);
						}
						
						Alerta::guardarMensajeInfo('eventoactualizado','Se ha actualizado el evento');
				
						Url::redirect('agenda/administrar');
					}
				}
			}
		}
		
		$recomendaciones = $objModelo->obtenerListadoRecomendaciones();
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'editar');
	}
}