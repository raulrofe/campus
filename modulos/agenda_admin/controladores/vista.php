<?php
$get = Peticion::obtenerGet();

if(isset($get['dia'], $get['mes'], $get['anio'])
	&& is_numeric($get['dia']) && is_numeric($get['mes']) && is_numeric($get['anio'])
		&& (strlen($get['dia']) == 1 || strlen($get['dia']) == 2) && (strlen($get['mes']) == 1 || strlen($get['mes'])) == 2 && strlen($get['anio']) == 4)
{
	// valida la fecha
	if(checkdate($get['mes'], $get['dia'], $get['anio']))
	{
		$fechaActual = $get['anio'] . '-' . $get['mes'] . '-' . $get['dia'];	
	}
	else
	{
		$fechaActual = date('Y-m-d');
	}
}
else
{
	$fechaActual = date('Y-m-d');
}
	
$objModelo = new ModeloAgenda();
			
if(Peticion::isPost())
{
	$post = Peticion::obtenerPost();
		
	// valida el campo fecha y que no este ninguno vacio
	if(isset($post['contenido']) && !empty($post['contenido']) )
	{
		// ID del usuario
		$idUsuario = Usuario::getIdUser(true);
				
		// inserta el evento nuevo
		$objModelo->nuevoEvento($idUsuario, Usuario::getIdCurso(), $post['contenido'], $fechaActual);
		$objModelo->nuevoAgendaBlocNota($objModelo->obtenerUltimoIdInsertado(), $idUsuario);
			
		Url::redirect('agenda', true);
	}
}

if(Usuario::compareProfile('tutor'))
{
	$eventosRecomendacion = $objModelo->obtenerEventosRecomendacionPorDia(Usuario::getIdCurso(), $fechaActual);
}

if(Usuario::compareProfile('coordinador'))
{
	$eventosCoordinador = $objModelo->obtenerEventosCoordinadorPorDia(Usuario::getIdCurso(), $fechaActual);
}

$eventosOrdenados = array();

if(Usuario::compareProfile('tutor'))
{
	if($eventosRecomendacion->num_rows > 0)
	{
		while($evento = $eventosRecomendacion->fetch_object())
		{
			$eventosOrdenados[] = array(
				'idagenda' => $evento->idagenda_recomendacion,
				'contenido' => $evento->contenido,
				'fecha' => $evento->fecha,
				'recomendacion' => true,
				'coordinador' => false,
				'creador' => $evento->nombrec	
			);
		}
	}
}

if(Usuario::compareProfile('coordinador'))
{
	if($eventosCoordinador->num_rows > 0)
	{
		while($evento = $eventosCoordinador->fetch_object())
		{
			$eventosOrdenados[] = array(
				'idagenda' => $evento->idagenda_coordinador,
				'contenido' => $evento->contenido,
				'fecha' => $evento->fecha,
				'recomendacion' => false,
				'coordinador' => true,
				'creador' => $evento->nombrec
			);
		}
	}
}
		
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'vista');