<?php

//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');
$html2Pdf = new HTML2PDF();
$objModelo = new ModeloAgendaAdmin();
$objCurso = new Cursos();

mvc::cargarModuloSoporte('agenda');
$objModeloPersonal = new ModeloAgenda();

$objCurso->set_curso(Usuario::getIdCurso());
$curso = $objCurso->buscar_curso();
$curso = $curso->fetch_object();

$get = Peticion::obtenerGet();

if(isset($get['fechaActual']))
{
	$fechaActual = $get['fechaActual']; 	
}
else
{
	$fechaActual = date('Y-m-d');	
}

if(isset($get['dia'], $get['mes'], $get['anio']) && is_numeric($get['dia']) && is_numeric($get['mes']) && is_numeric($get['anio']))
{
	$fechaDia = $get['anio'] . '-' . $get['mes'] . '-' . $get['dia'];
	$tituloEventos = 'Eventos del d&iacute;a';
	//$eventos = $objModeloPersonal->obtenerEventosPorDia(Usuario::getIdUser(true), Usuario::getIdCurso(), $fechaDia);
	
	if(Usuario::compareProfile('tutor'))
	{
		$eventos = $objModeloPersonal->obtenerEventosRecomendacionPorDia(Usuario::getIdCurso(), $fechaDia);
	}
	
	if(Usuario::compareProfile('coordinador'))
	{
		$eventos = $objModeloPersonal->obtenerEventosCoordinadorPorDia(Usuario::getIdCurso(), $fechaDia);
	}	
}
else
{
	if(Usuario::compareProfile('coordinador'))
	{
		if(isset($curso->idconvocatoria))
		{
			$tituloEventos = 'Eventos coordinadora';
			$eventos = $objModelo->obtenerEventosCoordinadora($curso->idconvocatoria);
		}
	}
	else
	{
		if(isset($curso->idrecomendacion))
		{
			$tituloEventos = 'Agenda del curso';
			$eventos = $objModelo->obtenerEventosPorRecomendacion($curso->idrecomendacion);
		}
	}
}





$eventosOrdenados = array();
if(isset($eventos) && $eventos->num_rows > 0)
{
	while($evento = $eventos->fetch_object())
	{
		$eventosOrdenados[] = array(
			'contenido' => $evento->contenido,
			'fecha' => $evento->fecha,
			'recomendacion' => false,
			'coordinador' => false,
			'creador' => ''
		);
	}
}

if(isset($eventosRecomendacion) && $eventosRecomendacion->num_rows > 0)
{
	while($evento = $eventosRecomendacion->fetch_object())
	{
		$eventosOrdenados[] = array(
			'idagenda'		=> $evento->idagenda_recomendacion,
			'contenido'		=> $evento->contenido,
			'fecha'			=> $evento->fecha,
			'recomendacion'	=> true,
			'coordinador'	=> false,
			'creador'		=> $evento->nombrec
		);
	}
}

if(Usuario::compareProfile(array('tutor')))
{
	if(isset($eventosCoordinador) && $eventosCoordinador->num_rows > 0)
	{
		while($evento = $eventosCoordinador->fetch_object())
		{
			//var_dump($evento);
			$eventosOrdenados[] = array(		
				'idagenda' 			=> $evento->idagenda_recomendacion,
				'contenido' 		=> $evento->contenido,
				'fecha' 			=> $evento->fecha,
				'recomendacion' 	=> false,
				'coordinador' 		=> true,
				'creador' 			=> $evento->nombrec
			);
		}
	}
}

$eventosOrdenados = Fecha::orderByDate($eventosOrdenados);

//var_dump($eventosOrdenados);

// Una vez terminado el proceso de creacion del array para generar el pdf nos ponemos a generarlo
// Recuperacion del contenido HTML
ob_start();
include(PATH_ROOT . 'modulos/agenda/vistas/imprimir/imprimir_eventos.php');
$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
	//$html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	
	$html2pdf->Output();
}
catch(HTML2PDF_exception $e) {echo $e;}