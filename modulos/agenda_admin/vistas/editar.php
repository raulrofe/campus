<div id="agenda_admin">
	
	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span>AGENDA - Editar evento</span>
	</div>
	
	<br /><br />
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuAgendaAdmin">
		<div id="menuAgendaAdmin_1" class="redondearBorde">
			<a href="agenda">
				<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
				<br />Agenda<br />personal
			</a>
		</div>
		<div id="menuAgendaAdmin_2" class="blanco redondearBorde">
			<a href="agenda/administrar">
				<img src="imagenes/agenda/menu_listado.png" alt="" />
				<br />Agenda<br />del curso
			</a>
		</div>
	</div>
	<!-- Fin menu iconos -->
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAdminAgenda">
  		<form id="frm_agenda_ev_editar" method="post" action="agenda/administrar/editar/<?php echo $evento->idagenda_recomendacion?>">
			<ul>
				<li><textarea id="frm_agenda_ev_contenido" name="contenido" cols="1" rows="1"><?php echo $evento->contenido?></textarea></li>
				<li>
		  			<input id="datepicker" type="text" name="fecha" value="<?php echo date('d/m/Y', strtotime($evento->fecha))?>" />
		  		</li>
				<li class="fright"><button type="submit">Actualizar evento</button></li>
			</ul>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>