<!-- cambiado el dia 18/06/2012 -->
<div id="agenda">
	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="agenda.tituloadministrar">
			AGENDA - Administrar eventos
		</span>
	</div>

	<br /><br />

	<?php if (Usuario::compareProfile('tutor')): ?>
		<!-- menu de iconos -->
		<div class="fleft panelTabMenu" id="menuAgendaAdmin">
			<div id="menuAgendaAdmin_1" class="redondearBorde">
				<a href="agenda">
					<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
					<span data-translate-html="agenda.personal">
						<br />Agenda<br />personal
					</span>
				</a>
			</div>
			<div id="menuAgendaAdmin_2" class="blanco redondearBorde">
				<a href="agenda/administrar">
					<img src="imagenes/agenda/menu_listado.png" alt="" />
					<span data-translate-html="agenda.curso">
						<br />Agenda<br />del curso
					</span>
				</a>
			</div>
		</div>
		<!-- Fin menu iconos -->
	<?php elseif (Usuario::compareProfile('coordinador')): ?>
		<!-- menu de iconos -->
		<div class="fleft panelTabMenu" id="menuAgendaAdmin">
			<div id="menuAgendaAdmin_1" class="redondearBorde">
				<a href="agenda">
					<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
					<span data-translate-html="agenda.curso">
						<br />Agenda<br />del curso
					</span>
				</a>
			</div>
			<div id="menuAgendaAdmin_2" class="blanco redondearBorde">
				<a href="agenda/administrar">
					<img src="imagenes/agenda/menu_listado.png" alt="" />
					<span data-translate-html="agenda.cursotutores">
						<br />Agenda<br />del curso<br />para tutores
					</span>
				</a>
			</div>
		</div>
		<!-- Fin menu iconos -->
	<?php endif; ?>

	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAdminAgenda">	
		<div id="calendario_content">
			<div id="calendario">
				<div id="agenda_mes" class="t_center">
					<div class="fleft">
						<a href="agenda/administrar/<?php echo $mesAnterior ?>/<?php echo $anioAnterior ?>" data-translate-title="agenda.mesanterior" title="Ir al mes anterior">
							<img src="imagenes/agenda/arrow_left.png" alt="mes anterior" />	
						</a>
					</div>
					<div class="nombreMes">
						<span id="agenda_mes_actual_num" class="hide"><?php echo mb_strtoupper($mesActual) ?></span>
						<span><?php echo mb_strtoupper($meses[$mesActual - 1]) ?> <?php echo $anioActual; ?></span>
					</div>
					<div class="fleft">
						<a href="agenda/administrar/<?php echo $mesSig ?>/<?php echo $anioSig ?>" data-translate-title="agenda.messiguiente" title="Ir al mes siguiente">
							<img src="imagenes/agenda/arrow_right.png" alt="mes siguiente" />	
						</a>
					</div>
				</div>
				<div class="clear"></div>

				<div id="agenda_dias_semana" class="clear">
					<?php foreach ($diasSemana as $item_ds): ?>
						<span><?php echo $item_ds ?></span>
					<?php endforeach; ?>
				</div>
				<div class="clear"></div>

				<ul id="agenda_dias" class="clear">
					<?php for ($cont = 1; $cont <= $iniDia; $cont++): ?>
						<li><span>--</span></li>
					<?php endfor; ?>

					<?php for ($cont = 1; $cont <= $dias; $cont++): ?>
						<?php
						if ($iniDia == 7) {
							$addClass = 'clear';
							$iniDia = 0;
						} else {
							$addClass = '';
						}
						$iniDia++;

						$addClass2 = '';
						if ($diaActual == $cont) {
							$addClass2 = 'agenda_dia_activo negrita';
						}
						?>
						<li class="<?php echo $addClass . ' ' . $addClass2 ?> <?php
						if (isset($arEv[$cont], $arEvRcd[$cont]))
							echo 'agenda_dia_get_ev_mix';
						elseif (isset($arEv[$cont]))
							echo 'agenda_dia_get_ev';
						else if ($perfil == 'tutor' && isset($arEvRcd[$cont]))
							echo 'agenda_dia_get_ev_rcd';
						else if ($perfil == 'coordinador' && isset($arEvCoord[$cont]))
							echo 'agenda_dia_get_ev_coord';
						?>">
							<a href="agenda/administrar/vista/<?php echo $cont . '/' . $mesActual . '/' . $anioActual ?>" data-translate-title="agenda.irdia" title="Ir a este d&iacute;a"><?php echo $cont ?></a>
						</li>
					<?php endfor; ?>
				</ul>

				<span id="agenda_mes_actual" class="hide"><?php echo $mesActual ?></span>
				<span id="agenda_anio_actual" class="hide"><?php echo $anioActual ?></span>

			</div>


			<div class="fright">
				<div class="crearEventoPersonal">
					<form id="frm_agenda_ev" method="post" action="" <?php if (!Usuario::compareProfile(array('tutor', 'coordinador'))) echo 'style="top: 24px;"' ?>>
						<ul>
							<li><textarea id="frm_agenda_ev_contenido" name="contenido" cols="1" rows="1"></textarea></li>
							<li>
								<button type="submit" data-translate-html="agenda.crear">
									A&ntilde;adir a la agenda
								</button>
							</li>
						</ul>
						<div class="clear"></div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="leyendaCalendario">
			<span id="agenda_ayuda_color_1"></span>
			<span data-translate-html="agenda.d_seleccionado">
				D&iacute;a seleccionado
			</span>		
			<?php if (Usuario::compareProfile('tutor')): ?>		
				<span id="agenda_ayuda_color_3"></span>
				<span data-translate-html="agenda.eventos_tutor">
					Eventos creados por el tutor/a
				</span>
			<?php else: ?>
				<span id="agenda_ayuda_color_4"></span>
				<span data-translate-html="agenda.eventos_coordinador">
					Eventos creados por el coordinador/a
				</span>
			<?php endif; ?>
		</div>

		<div id="agenda_vista">	
			<br />
			<div>
				<div class="menuVista fleft">
					<div class="fleft listBlack" >
						<a href="agenda/administrar/modo/listado" onclick="return false;" rel="tooltip" data-translate-title="agenda.listado" title="Listado de eventos">
							<img src="imagenes/menu_vistas/list.png" alt="vista clasica"/>
						</a>
					</div>
					<div class="fleft calendarBlack ActiveOption" >
						<a href="#" onclick="return false;" data-translate-title="agenda.titulo" rel="tooltip" title="Agenda">
							<img src="imagenes/menu_vistas/calendario.png" alt="vista"/>
						</a>
					</div>
					<div class="fleft printBlack" >
						<a href="agenda/imprimir/eventos_dia_admin/<?php echo $diaActual ?>/<?php echo $mesActual ?>/<?php echo $anioActual ?>" target="_blank" rel="tooltip" data-translate-title="agenda.imprimirlistado" title="Imprimir listado">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista"/>
						</a>
					</div>
				</div>
				<div class="fright t_right" style="margin-top:7px;">
					<strong data-translate-html="agenda.eventos">
						Eventos
					</strong>
				</div>
			</div>
			<div class="clear"></div>
			<?php Fichero::importar(PATH_ROOT . 'modulos/agenda_admin/controladores/vista.php'); ?>
		</div>

	</div>
</div>

<script type="text/javascript">/*agendaDiaOpen();*/</script>