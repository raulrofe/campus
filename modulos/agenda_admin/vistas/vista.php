<div id="agenda_eventos">	
	<div id="agenda_ev_listado" class="listarElementos">
		<?php if(count($eventosOrdenados) > 0):?>
			
			<?php foreach($eventosOrdenados as $ev):?>
				<?php if($ev['recomendacion']):?>
					<div class="elementoRecomendacion">
					
						<?php if($get['c'] == 'modo_listado'):?>
							<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($ev['fecha'], false)?>">
				  				<?php echo date('d', strtotime($ev['fecha']))?>
				  				<div class="elementoFecha2_hr"></div>
				  				<?php echo date('M', strtotime($ev['fecha']))?>
				  			</div>
				  		<?php endif;?>
				  		
						<div class="anotacion"><?php echo Texto::textoFormateado($ev['contenido'])?></div>
						<div class="pieAgenda">
							<div class="t_right"><strong>Tutor/a: <?php echo Texto::textoPlano($ev['creador'])?></strong></div>
						</div>
						<div class="clear"></div>
						<div class="elementoPie">
							<ul class="elementoListOptions fright hide">
								<li>
									<a href="agenda/administrar/editar/<?php echo $ev['idagenda']?>" title="Editar este evento de la agenda">
										<img src="imagenes/foro/editar.png" alt="" />
										<span style="color:#000;font-weight:bold;font-size:0.9em;">Editar</span>
									</a>
								</li>
								<li>
									<a href="#" <?php echo Alerta::alertConfirmOnClick('borrarrecomendacion','¿Deseas borrar esta recomendacion?', 'agenda/administrar/borrar/' . $ev['idagenda']);?> title="Borrar este evento de la agenda" style="color:#444;">
										<img src="imagenes/foro/borrar.png" alt=""/>
										<span style="color:#000;font-weight:bold;font-size:0.9em;">Borrar</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				<?php elseif($ev['coordinador']):?>
					<div class="elementoCoordinador">
					
						<?php if($get['c'] == 'modo_listado'):?>
							<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($ev['fecha'], false)?>">
				  				<?php echo date('d', strtotime($ev['fecha']))?>
				  				<div class="elementoFecha2_hr"></div>
				  				<?php echo date('M', strtotime($ev['fecha']))?>
				  			</div>
				  		<?php endif;?>
				  		
						<div class="anotacion"><?php echo Texto::textoFormateado($ev['contenido'])?></div>
						<div class="pieAgenda">
							<div class="t_right"><strong>Coordinador/a: <?php echo Texto::textoPlano($ev['creador'])?></strong></div>
						</div>
						<div class="clear"></div>
						<div class="elementoPie">
							<ul class="elementoListOptions fright hide">
								<li>
									<a href="agenda/administrar/editar/<?php echo $ev['idagenda']?>" title="Editar este evento de la agenda">
										<img src="imagenes/foro/editar.png" alt="" />
										<span style="color:#000;font-weight:bold;font-size:0.9em;">Editar</span>
									</a>
								</li>
								<li>
									<a href="#" <?php echo Alerta::alertConfirmOnClick('borrarrecomendacion','¿Deseas borrar esta recomendacion?', 'agenda/administrar/borrar/' . $ev['idagenda']);?> title="Borrar este evento de la agenda" style="color:#444;">
										<img src="imagenes/foro/borrar.png" alt=""/>
										<span style="color:#000;font-weight:bold;font-size:0.9em;">Borrar</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				<?php endif;?>
				<div class="clear"></div>
			<?php endforeach;?>
		<?php else:?>
			<div class="t_center"><strong>No tienes eventos este d&iacute;a</strong></div>
		<?php endif;?>
	</div>
</div>