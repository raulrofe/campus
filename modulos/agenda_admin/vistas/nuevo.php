<div id="agenda_admin">
	
	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="agenda.titulonuevo">
			AGENDA - Nuevo evento
		</span>
	</div>
	
	<br /><br />
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuAgendaAdmin">
		<div id="menuAgendaAdmin_1" class="redondearBorde">
			<a href="agenda">
				<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
				<span data-translate-html="agenda.personal">
					<br />Agenda<br />personal
				</span>
			</a>
		</div>
		<div id="menuAgendaAdmin_2" class="blanco redondearBorde">
			<a href="agenda/administrar">
				<img src="imagenes/agenda/menu_listado.png" alt="" />
				<span data-translate-html="agenda.curso">
					<br />Agenda<br />del curso
				</span>
			</a>
		</div>
	</div>
	<!-- Fin menu iconos -->
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAdminAgenda">
	
		<div class="listadoEventosAlumno">
			<div class="menuVista" class="fleft t_left" style="width:192px;">
				<div class="fleft listBlack ActiveOption" >
					<a href="agenda/administrar/modo/listado" onclick="return false;" rel="tooltip" data-translate-title="agenda.listado" title="Listado de eventos">
						<img src="imagenes/menu_vistas/list.png" alt="vista clasica"/>
					</a>
				</div>
				<div class="fleft t_right calendarBlack" >
					<a href="agenda/administrar" onclick="return false;" data-translate-title="agenda.titulo" rel="tooltip" title="Agenda">
						<img src="imagenes/menu_vistas/calendario.png" alt="vista"/>
					</a>
				</div>
				<div class="fleft t_right printBlack" >
					<a href="#" onclick="return false;"  rel="tooltip" data-translate-title="agenda.imprimirlistado" title="Imprimir listado">
						<img src="imagenes/menu_vistas/imprimir.png" alt="vista"/>
					</a>
				</div>
			</div>
			
			<div class="fright t_right eventoAlumnoTitle" style="width:305px;">
				<strong data-translate-html="agenda.insertar">
					Insertar nueva recomendaci&oacute;n
				</strong>
			</div>
			<div class="clear"></div>	
		</div>
		<br />
				
  		<?php if($recomendaciones->num_rows > 0):?>
  			<form id="frm_agenda_ev_nuevo" method="post" action="">
				<ul>
					<li><textarea id="frm_agenda_ev_contenido" name="contenido" cols="1" rows="1"></textarea></li>
					<li class="fleft">
			  			<input id="datepicker" type="text" name="fecha" value="<?php echo date('d/m/Y')?>" />
			  		</li>
					<li class="fright">
						<button type="submit" data-translate-html="agenda.anadirevento">
							A&ntilde;adir evento
						</button>
					</li>
				</ul>
				<div class="clear"></div>
			</form>
		<?php else:?>
			<div>
				<strong data-translate-html="agenda.debescrear">
					Debes crear antes una recomendaci&oacute;n
				</strong>
			</div>
		<?php endif;?>
  	</div>
</div>

<script type="text/javascript">
	$("#datepicker").datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
</script>