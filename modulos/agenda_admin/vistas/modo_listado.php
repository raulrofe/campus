<div id="agenda_admin">

	<div class='subtitle t_center'>
		<img src="imagenes/agenda/agenda.png" alt="" style="vertical-align:middle;"/>
		<span data-translate-html="agenda.tituloadministrar">
			AGENDA - Administrar eventos
		</span>
	</div>

	<br /><br />
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuAgendaAdmin">
		<div id="menuAgendaAdmin_1" class="redondearBorde">
			<a href="agenda">
				<img src="imagenes/agenda/menu_mi_agenda.png" alt="" />
				<span data-translate-html="agenda.personal">
					<br />Agenda<br />personal
				</span>
			</a>
		</div>
		<div id="menuAgendaAdmin_2" class="blanco redondearBorde">
			<a href="agenda/administrar">
				<img src="imagenes/agenda/menu_listado.png" alt="" />
				<span data-translate-html="agenda.curso">
					<br />Agenda<br />del curso
				</span>
			</a>
		</div>
	</div>
	<!-- Fin menu iconos -->

	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAdminAgenda">
		<div class="listadoEventosAlumno">
			<div class="menuVista" class="fleft t_left" style="width:192px;">
				<div class="fleft listBlack ActiveOption" >
					<a href="#" onclick="return false;" rel="tooltip" data-translate-title="agenda.listado" title="Listado de eventos">
						<img src="imagenes/menu_vistas/list.png" alt="vista clasica"/>
					</a>
				</div>
				<div class="fleft t_right calendarBlack" >
					<a href="agenda/administrar" onclick="return false;"  rel="tooltip" title="Agenda">
						<img src="imagenes/menu_vistas/calendario.png" alt="vista"/>
					</a>
				</div>
				<div class="fleft t_right printBlack" >
					<a href="agenda/imprimir/eventos_admin/recomendaciones" target="_blank" rel="tooltip" data-translate-title="agenda.imprimirlistado" title="Imprimir listado">
						<img src="imagenes/menu_vistas/imprimir.png" alt="vista"/>
					</a>
				</div>
			</div>

			<div class="fleft t_right eventoAlumnoTitle" style="width:275px;">
				<strong data-translate-html="agenda.recomendaciones">
					Recomendaciones
				</strong>
			</div>	

			<div id="agenda_admin_boton_nuevo" class="t_right">
				<button type="button" data-url="agenda/administrar/nuevo" data-translate-html="agenda.nuevoevento">
					Nuevo evento
				</button>
			</div>
		</div>
		<br />

		<?php if (isset($curso->idrecomendacion)): ?>
			<?php if (isset($eventos)): ?>
				<?php if ($eventos->num_rows > 0): ?>
					<div class="listarElementos">
						<?php while ($evento = $eventos->fetch_object()): ?>
							<div class="elemento">
								<div class="fleft elementoFecha2" rel="tooltip" title="<?php echo Fecha::obtenerFechaFormateada($evento->fecha, false) ?>">
									<?php echo date('d', strtotime($evento->fecha)) ?>
									<div class="elementoFecha2_hr"></div>
									<?php echo date('M', strtotime($evento->fecha)) ?>
								</div>
								<div class="elementoContenido"><?php echo Texto::textoFormateado($evento->contenido) ?></div>
								<div class="elementoPie">
									<ul class="elementoListOptions fright hide">
										<li>
											<a href="agenda/administrar/editar/<?php echo $evento->idagenda_recomendacion ?>" title="Editar esta recomendacion de la agenda">
												<img src="imagenes/foro/editar.png" alt=""/>
												<span data-translate-html="formulario.editar">
													Editar
												</span>
											</a>
										</li>
										<li>
											<a href="#" <?php echo Alerta::alertConfirmOnClick('borrarrecomendacion','¿Deseas borrar esta recomendación?', 'agenda/administrar/borrar/' . $evento->idagenda_recomendacion); ?> data-translate-title="agenda.borrarrecomendacion" title="Borrar esta recomendacion de la agenda">
												<img src="imagenes/foro/borrar.png" alt=""/>
												<span data-translate-html="formulario.borrar">
													Borrar
												</span>
											</a>
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php else: ?>
					<div class="t_center">
						<strong data-translate-html="agenda.nohay">
							No hay eventos
						</strong>
					</div>
				<?php endif; ?>
			<?php else: ?>

			<?php endif; ?>
		<?php else: ?>
			<div class="t_center">
				<strong data-translate-html="agenda.norecomendacion">
					Este curso no tiene asignada una recomendaci&oacute;n
				</strong>
			</div>
		<?php endif; ?>
	</div>
</div>