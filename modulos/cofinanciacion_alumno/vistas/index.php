<div id="cofinanciacion_alumno">

	<br/><br/>

	<div>
		<div class="t_center">
			<img src="imagenes/mis_progresos/progress.png" alt="" style="vertical-align:middle;width:32px;height:32px;"/>
			<span data-translate-html="cofinanciacion.titulo">
				Cofinanciación
			</span>
		</div>
	</div>

	<br/><br/>

	<div style="padding: 15px">
		<span  data-translate-html="cofinanciacion.horas">
			(*) Horas a cumplir dentro de la joranada laboral:
		</span>
		<?php if(!empty($horasCampusCumplir)): ?>
		 	<?php echo $horasCampusCumplir ?>
		<?php else: ?>
			<span>No se ha podido calcular las horas</span>
		<?php endif; ?>
	</div>
	<div id="informes" style="padding: 15px">
		<div>
			<table class="tabla">
				<thead>
					<tr>
						<td class="t_center" data-translate-html="tiempos.primeracceso">Total Accesos</td>
						<td class="t_center" data-translate-html="tiempos.primeracceso">Primer acceso</td>
						<td class="t_center" data-translate-html="tiempos.ultimoacceso">&Uacute;ltimo acceso</td>
						<td class="t_center" data-translate-html="tiempos.tiempototal">Tiempo total</td>
						<td class="t_center" data-translate-html="cofinanciacion.titulo">Cofinanciadas</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="t_center"><?php echo count($alumno->accesos_reales) ?></td>
						<!-- PRIMERA CONEXION -->									
						<?php if (!empty($alumno->primero)): ?>
							<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', $alumno->primero)); ?></td>
						<?php else: ?>
							<td class="t_center">00:00:00</td>
						<?php endif; ?>

						<!-- ULTIMA CONEXION -->									
						<?php if (!empty($alumno->ultimo)): ?>
							<td class="t_center"><?php echo Fecha::obtenerFechaFormateadaSegundos(date('H:i:s d-m-Y', $alumno->ultimo)); ?></td>
						<?php else: ?>
							<td class="t_center">00:00:00</td>
						<?php endif; ?>

						<!-- HORAS TOTALES -->
						<?php if (isset($alumno->suma)): ?>
							<td class="t_center">
								<?php echo $alumno->sumaFormateada; ?>
							</td>
						<?php else: ?>
							<td class="t_center">00:00:00</td>
						<?php endif; ?>

						<!-- HORAS COFINANCIADAS -->
						<?php if (isset($alumno->horaConfinaciacion)): ?>
							<td class="t_center">
								<?php echo $alumno->horaConfinaciacion; ?>
							</td>
						<?php else: ?>
							<td class="t_center">00:00:00</td>
						<?php endif; ?>

					</tr> 

				</tbody>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript" src="js-calificaciones-detalle_foro.js"></script>
