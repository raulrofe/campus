<?php

// Funcion auxiliar para ordenar un array por el campo fecha_entrada
function compareFechaEntrada($a, $b){
	if(!empty($a->fecha_entrada) && !empty($b->fecha_entrada)){
	    if (strtotime($a->fecha_entrada) == strtotime($b->fecha_entrada)) {
	        return 0;
	    }
	    return (strtotime($a->fecha_entrada) < strtotime($b->fecha_entrada)) ? -1 : 1;
	}else{
		return 0;
	}
}
$objModelo = new ModeloCofinanciacion();

// $idUsuario 	= Usuario::getIdUser();
$idCurso 	= Usuario::getIdCurso();

// Obtengo ID de Matricula segun usuario y curso sesion
$idMatricula = Usuario::getIdMatricula();

// Obtengo los Accesos y las Sesiones
$accesos 	= $objModelo->obtenerAccesosMatricula($idMatricula);
$sessions 	= $objModelo->obtenerSessionsMatricula($idMatricula);

//Obtenemos los datos del alumno
$alumno = $objModelo->obtenerMatriculasCurso($idMatricula)->fetch_object();

//Obtengo los datos del curso para la peticion curl
//Para sacar la informacion cofinanciada del participante
//Obtengo codigo inicio grupo para relacionarlo con Iberform
$curso 		= $objModelo -> obtenerDatosCursoPorSesion($idCurso)->fetch_object();

if(strpos($curso->titulo,"/") > 0){
	$tituloDesglosado = explode('/', $curso->titulo);
	$codigoIG = str_replace(')', '', $tituloDesglosado[1]);
	$accionFormativa = substr($tituloDesglosado[0], -5);
}

// Guardamos los accesos y sesiones en un mismo array 
$alumnoAccesos=array();
if($accesos->num_rows > 0){
	while($register = $accesos->fetch_object()){
		array_push ( $alumnoAccesos, $register );
	}
}
if($sessions->num_rows > 0){
	while($register = $sessions->fetch_object()){
		array_push ( $alumnoAccesos, $register );
	}
}

// Ordenamos el array de accesos y sesiones por fecha de entrada
//Cofinanciacion::orderArrayCofinanciacion($alumnoAccesos);
usort($alumnoAccesos, "compareFechaEntrada");

// Inicializo variables
$accesosReales = array();

$InicioReal=null;
$FinReal=null;
$sumaTiempos = 0;

for( $i=0; $i < count($alumnoAccesos); $i++) {

	// Cacheamos el acceso
	$acceso = $alumnoAccesos[$i];
	$inicioAcceso = $acceso->fecha_entrada;
	$finAcceso = $acceso->fecha_salida;	
	// Margen de 10 minutos entre fecha_salida de una conexion y la fecha_entrada de la siguiente conexion
	$margen_milisegundos=10*60; // 10 minutos

	// Si es el primer registro
	if($i ==  0){
		$InicioReal = $inicioAcceso;
		$FinReal = $finAcceso;
	}
	// Si es el último registro
	elseif($i == count($alumnoAccesos)-1){

		// Si no existe Inicio Real, ponemos el inicio de este ultimo acceso
		if(empty($InicioReal) || $InicioReal == '0000-00-00 00:00:00'){ 
			$InicioReal = $inicioAcceso;		
		}
		// Si no existe Fin Real, vemos si ponemos el fin de este ultimo acceso, o el inciio si el fin esta vacio (Daría un rango real de 0s)
		if(empty($FinReal) || $FinReal == '0000-00-00 00:00:00'){ 
			if(empty($finAcceso) || $finAcceso == '0000-00-00 00:00:00'){ 
				$FinReal = $inicioAcceso;
			}else{
				$FinReal = $finAcceso;
			}
		}
		// Cerramos el rango real
		$accesosReales = Cofinanciacion::createRegistroReal($InicioReal, $FinReal, $accesosReales);
		// Sumamos al contador de tiempo
		$sumaTiempos = Cofinanciacion::sumaTiempos($InicioReal, $FinReal, $sumaTiempos);
		
	}
	// Si no es el primer ni el último registro
	else{

		// Si entra dentro del rango anterior (Se SOLAPA)
		if(strtotime($inicioAcceso) <= (strtotime($FinReal) + $margen_milisegundos)){
			// Si existe "Fin de Acceso" y si el fin del acceso es mayor que el guarado, sobreescribimos
			if(!empty($finAcceso) && (strtotime($finAcceso) > strtotime($FinReal))){
				$FinReal = $finAcceso;
			}

		}
		// Si NO entra dentro del rango anterior (NO se solapa)
		else{

			// Si no existe un Fin para ese rango lo obviamos y pasamos al siguiente rango
			if(empty($FinReal) || $FinReal == '0000-00-00 00:00:00'){ 
				$FinReal = $inicioAcceso;
			}
			
			// Si existe un rango real, lo guardamos para empezar con el nuevo rango que no solapa con el anteiorr
			if(!empty($InicioReal) && !empty($FinReal)){
				// Cerramos el rango real
				$accesosReales = Cofinanciacion::createRegistroReal($InicioReal, $FinReal, $accesosReales);
				// Sumamos al contador de tiempo
				$sumaTiempos = Cofinanciacion::sumaTiempos($InicioReal, $FinReal, $sumaTiempos);
			}


			// Ponemos las variables con los nuevos valores, si esta vacio el fin de acceso del acceso obviamos este registro
			if(!empty($finAcceso) && $finAcceso != '0000-00-00 00:00:00'){ 
				$InicioReal = $inicioAcceso;
				$FinReal = $finAcceso; 
			}else{
				$InicioReal = '';
				$FinReal = '';
			}
						
		}
		
	}  
}


$alumno->primero = strtotime($alumnoAccesos[0]->fecha_entrada);
$alumno->ultimo = (!empty($alumno->fecha_conexion_actual))? strtotime($alumno->fecha_conexion_actual) : strtotime($alumnoAccesos[count($alumno->accesos)]->fecha_salida);

// Filtramos y quitamos los registros de duracion menor de 5s
$accesosRealesFiltrados = array();
foreach($accesosReales as $accesosReal){
	if($accesosReal['diferencia'] > 10){
		array_push($accesosRealesFiltrados, $accesosReal);
	}
}

$alumno->accesos_reales = $accesosRealesFiltrados;
$alumno->suma 			= $sumaTiempos;
$alumno->sumaFormateada = Cofinanciacion::secondToFormat($sumaTiempos);

if (!empty($codigoIG) && !empty($accionFormativa)){

	//Script para las horas cofinanciadas
	$dataIberform = array(
		'nif'				=> $alumno->dni,
		'codigoIG'			=> $codigoIG,
		'accionFormativa' 	=> $accionFormativa
	);	

	$horarioLaboral = $objModelo->horasCofinanciadasIberform($dataIberform);

	//Calculo el numero de horas de conexion en la plataforma
	$horasDia = 0;
	if(count($horarioLaboral->diasLaborales) > 0){ 
		$horasDia = ($horarioLaboral->horas_semanales / 7); 
	}

	if(!empty($horarioLaboral->salario) && $horarioLaboral->salario != 0.00 && $horasDia > 0){
		$costeHora  = $horarioLaboral->salario / 30 / $horasDia;	
	}

	if(isset($horarioLaboral->cofinancion)) {

		$costeSalarial = ($horarioLaboral->coste * $horarioLaboral->cofinancion) / 100;

		if($horarioLaboral->cofinancion == 0){ 
		
			$horasCampusCumplir = 0;

		} else {

			if(isset($costeHora) && isset($horasDia) && $costeHora > 0 && $horasDia > 0){

				$horasCampusCumplir = round($costeSalarial / $costeHora, 2);

				list($horasC, $minutosC) = explode(',', $horasCampusCumplir);

				$minutosC = round($minutosC * 0.60);

				$horasCampusCumplir = $horasC . ' horas y ' . $minutosC . ' minutos';

			} else {

				$horasCampusCumplir = 'Faltan datos para el cálculo';
			}
		}
	}


	$horaInicioLaboralManana 	= strtotime($horarioLaboral->horariom_inicio);
	$horaFinLaboralManana 		= strtotime($horarioLaboral->horariom_fin);

	$horaInicioLaboralTarde 	= strtotime($horarioLaboral->horariot_inicio);
	$horaFinLaboralTarde 		= strtotime($horarioLaboral->horariot_fin);

	$horaConfinaciacion = 0;

	foreach($accesosRealesFiltrados as $accesoReal) {
		$timeInicio = strtotime($accesoReal['fecha_entrada']);
		$timeFin	= strtotime($accesoReal['fecha_salida']);

		$diaSemanaNumerico 	= date("N", $timeInicio);
		$horaInicio 		= strtotime(date("H:i:s", $timeInicio));
		$horaFin 			= strtotime(date("H:i:s", $timeFin));				

		if(in_array($diaSemanaNumerico, $horarioLaboral->diasLaborales)) {

			if(!empty($horarioLaboral->horariom_inicio) && !empty($horarioLaboral->horariom_fin)) {

				$start=0;
				$end=0;
				// MAÑANAS
				if($horaInicio <= $horaInicioLaboralManana && $horaFin >= $horaInicioLaboralManana) {
					$start = $horaInicioLaboralManana;
				}elseif($horaInicio >= $horaInicioLaboralManana && $horaInicio <= $horaFinLaboralManana) {
					$start = $horaInicio;
				}

				if($horaFin <= $horaFinLaboralManana && $horaFin >= $horaInicioLaboralManana) {
					$end = $horaFin;
				}elseif($horaFin >= $horaFinLaboralManana && $horaInicio <= $horaFinLaboralManana) {
					$end = $horaFinLaboralManana;
				}
				if(!empty($end)){
					$horaConfinaciacion = $horaConfinaciacion + ($end - $start);			
				}
			}

			if(!empty($horarioLaboral->horariot_inicio) && !empty($horarioLaboral->horariot_fin)) {
				$start=0;
				$end=0;

				// TARDES
				if($horaInicio <= $horaInicioLaboralTarde && $horaFin >= $horaInicioLaboralTarde) {
					$start = $horaInicioLaboralTarde;
				}elseif($horaInicio >= $horaInicioLaboralTarde && $horaInicio <= $horaFinLaboralTarde) {
					$start = $horaInicio;
				}

				if($horaFin <= $horaFinLaboralTarde && $horaFin >= $horaInicioLaboralTarde) {
					$end = $horaFin;
				}elseif($horaFin >= $horaFinLaboralTarde && $horaInicio <= $horaFinLaboralTarde) {
					$end = $horaFinLaboralTarde;
				}
				if(!empty($end)){
					$horaConfinaciacion = $horaConfinaciacion + ($end - $start);			
				}
			}
		}
	}

	$alumno->horaConfinaciacion = Cofinanciacion::secondToFormat($horaConfinaciacion);

}else{

	$horasCampusCumplir = 'Faltan datos para el cálculo';
	$alumno->horaConfinaciacion = Cofinanciacion::secondToFormat(0);

}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');