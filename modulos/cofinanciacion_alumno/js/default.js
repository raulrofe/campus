//captura el valor de la pestaña de mis progreso que viene desde tareas pendientes
$(function()
{
	var hash = $('#popupModal_mis_progresos').attr('data-url');
	var num = hash.match(/#([0-9]+)$/);
	
	if((num != undefined || num != null) && (num[1] != undefined || num[1] != null))
	{
		num = num[1];
	}
	else
	{
		num = 1;
	}
	
	changeTab(num);
});

function changeTab(num)
{
	$('#misProgresos > div').removeClass('blanco');
	$('#contenidoMisProgresos > div').addClass('hide');
	
	$('#tab' + num).removeClass('hide');
	$('#nav' + num).addClass('blanco');
}
