<?php
class ModeloCofinanciacion extends modeloExtend
{
	public function obtenerAccesosMatricula($idMatricula)	{
		$sql = 'SELECT fecha_entrada, fecha_salida' .
		' FROM logs_acceso' .
		//' WHERE fecha_salida IS NOT NULL AND idmatricula = ' . $idMatricula .
		' WHERE idmatricula = ' . $idMatricula .
		' ORDER BY fecha_entrada ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerSessionsMatricula($idMatricula)	{
		$sql = 'SELECT inicio_sesion as fecha_entrada, fin_sesion as fecha_salida' .
		' FROM log_tiempo_sesion' .
		//' WHERE fecha_salida IS NOT NULL AND idmatricula = ' . $idMatricula .
		' WHERE idmatricula = ' . $idMatricula .
		' ORDER BY inicio_sesion ASC';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerMatriculasCurso($idMatricula) {
		$sql = 'SELECT alumnos.idalumnos, alumnos.nombre, alumnos.apellidos, alumnos.dni, matricula.idmatricula, matricula.fecha_conexion_actual' .
		' FROM matricula' .
		' LEFT JOIN alumnos ON matricula.idalumnos = alumnos.idalumnos' .
		' WHERE idmatricula = ' . $idMatricula ;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerDatosCursoPorSesion($idCurso) {
		$sql = 'SELECT titulo' .
		' FROM curso' .
		' WHERE idcurso = ' . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

	// CONECTAR A IBERFORM PARA LAS HORAS COFINANCIADAS
	public function horasCofinanciadasIberform($post) {

		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, 'http://www.fundacionaulasmart.org/horas-cofinanciacion/' . $post['nif'] . '/' . $post['codigoIG'] . '/' . $post['accionFormativa']);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post);


		// En localhost hay que poner en la curl_setopt true porque si no no realiza la peticion
		// No olvidar de cambiar a false para produccion

		curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, false);

		// curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($post));

		$buffer = curl_exec($curl_handle);

		curl_close($curl_handle);
		
		if (!empty($buffer)){
			return json_decode($buffer);
		}else{
			return false;
		}
	}


}