<div id="anuncios">
	<?php require(dirname(__FILE__) . '/menu_tutores.php'); ?>
	
	<div class='subtitleModulo'>
		<span data-translate-html="tablon_publico.anuncios">
			Anuncios
		</span>
	</div>

	<?php if(mysqli_num_rows($mensajesCount) > 0): ?>

		<?php while($f = mysqli_fetch_assoc($mensajesCount)):?>

			<?php $fecha = Fecha::obtenerFechaFormateada($f['fecha']); ?>

			<div class='elemento'>

				<div class='elementoTitulo'>
					<?php echo Texto::textoPlano($f['titulo_anuncio']); ?>
				</div>

					<div class='elementoContenido'>
						<?php echo $f['descripcion_anuncio']; ?>
					</div>

					<?php if(!empty($f['archivo_tablon'])): ?>

						<div class='elementoContenido'>
							<a href='tablon_anuncios/forzar_descarga/<?php echo $f['idanuncio']; ?>' target='_blank'>
								<img alt="" src="imagenes/correos/clip.png">
								<span data-translate-html="general.adjunto">
									Archivo adjunto
								</span>
							</a>
						</div>
						
					<?php endif; ?>

					<div class='elementoPie'>
						<div class='elementoFecha' style="width:85%">
							<p>
								<span data-translate-html="general.de">De:</span>
								<?php if(!empty ($f['nombrec'])):?>
									<?php echo Texto::textoPlano($f['nombrec'])?>
								<?php else: ?>
									<?php echo Texto::textoPlano($f['nombrecAl'])?>
								<?php endif; ?>
								(<?php echo $fecha; ?>)
							</p>
							<p style="margin-top:3px;">
								<span data-translate-html="general.curso">
									Curso: 
								</span>
								<?php echo Texto::textoPlano($f['titulo'])?>
							</p>
						</div>
		 				<?php if($idUsuario == $f['idusuario'] || Usuario::compareProfile('coordinador')):?>

			 				<div class="elementoPieButtons fright">
			 					<ul>
			 						<li>
			 							<a href="tablon_anuncios/editar/<?php echo $f['idanuncio']; ?>" title="Editar anuncio" rel="tooltip" data-translate-title="tablon_anuncios.editar">
			 								<img alt="" src="imagenes/options/edit.png">
			 							</a>
			 						</li>
			 						<li>
			 							<a <?php echo Alerta::alertConfirmOnClick("rechazarpeticion","¿Deseas borrar el mensaje del tablón de anuncios público?", "tablon_anuncios/eliminar/" . $f['idanuncio'])?> href="#" rel="tooltip" title="Petici&oacute;n eliminaci&oacute;n anuncio" data-translate-title="tablon_anuncios.peticion_el">
			 								<img alt="" src="imagenes/options/delete.png">
			 							</a>
			 						</li>
			 					</ul>
								<div class='clear'></div>
			 				</div>

		 				<?php endif ?>
						<div class='clear'></div>
				   	</div>
					<div class='clear'></div>
				</div>
		<?php endwhile; ?>

		<?php Paginado::crear($numPagina, $maxPaging, 'tablon_anuncios/pagina/');?>

	<?php else: ?>
		<div class='burbuja t_center' data-translate-html="tablon_publico.noanuncio">El tabl&oacute;n de anuncios se encuentra vac&iacute;o en estos momentos</div>
	<?php endif; ?>
</div>