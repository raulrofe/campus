<div class="caja_separada">
	<div id="anuncios">
	
		<?php require(dirname(__FILE__) . '/menu_tutores.php'); ?>
		<div class='subtitleModulo'><span>Administrar anuncios</span></div>
		<div class='clear'></div>
	
		<?php if(mysqli_num_rows($registros_anuncios) > 0): ?>
			<?php while($f = mysqli_fetch_assoc($registros_anuncios)):?>
				<?php 
					$descripcion = $f['descripcion_anuncio'];
					$fecha = Fecha::obtenerFechaFormateada($f['fecha']);
				?>
				<div class='elemento'>
					<div class='elementoTitulo'><?php echo Texto::textoPlano($f['titulo_anuncio']); ?></div>
						
						<?php if(!empty($f['descripcion_anuncio'])): ?>
							<div class='elementoContenido'><?php echo $descripcion; ?></div>
						<?php elseif($f['modo'] == 'eliminar'):?>
							<div class='elementoContenido cursiva'>Eliminar anuncio</div>
						<?php elseif($f['modo'] == 'eliminar-adjunto'):?>
							<div class='elementoContenido cursiva'>Eliminar archivo de anuncio</div>
						<?php endif;?>
						
						<?php if(!empty($f['archivo_tablon'])): ?>
							<div class='elementoContenido'>
								<a href='tablon_anuncios/admin/forzar_descarga/<?php echo$f['idanuncio']; ?>' target='_blank'>
									<img alt="" src="imagenes/correos/clip.png"><span>Archivo adjunto</span>
								</a>
							</div>
						<?php endif; ?>
						<div class='elementoPie'>
							<div class='elementoFecha'>De:
								<?php if(!empty ($f['nombrec'])):?>		
									<?php echo Texto::textoPlano($f['nombrec'])?>
								<?php else: ?>
									<?php echo Texto::textoPlano($f['nombrecAl'])?>	
								<?php endif; ?>
								(<?php echo $fecha; ?>)
							</div>	
				 			<div class="elementoPieButtons fright">
				 				<ul>
				 					<li>
				 						<a href="tablon_anuncios/peticion/aceptar/<?php echo $f['idanuncio']; ?>">
				 							<img alt="" src="imagenes/correos/check_ok.png"><span>Aceptar</span>
			 							</a>
				 					</li>
				 					<li>
						 				<a <?php echo Alerta::alertConfirmOnClick("rechazarpeticion", "¿Deseas rechazar esta petición, el anuncio será eliminado?", "tablon_anuncios/peticion/denegar/" . $f['idanuncio'])?> href="#" title="Eliminar arvhivo del archivador">
											<img alt="" src="imagenes/foro/borrar.png"><span>Denegar</span>
					 					</a>
					 				</li>
					 			</ul>
				 			</div>
					   	</div>
						<div class='clear'></div>
					</div>
			<?php endwhile; ?>
			<?php Paginado::crear($numPagina, $maxPaging, 'tablon_anuncios/pagina/');?>
		<?php else: ?>
			<div class="t_center">No hay nada que moderar</div>
		<?php endif; ?>
	</div>
</div>
