<div class='caja_separada'>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/tablon_anuncios/news.png" style="vertical-align:middle;" />
				<span data-translate-html="tablon_anuncios.titulo">Tabl&oacute;n p&uacute;blico</span>
				<br/>
			</div>
			<p>Utilice el tabl&oacute;n de anuncios para publicar noticias, acontecimientos, recordatorios y mensajes relacionados con el curso, que considere interesante dar a conocer a sus alumnos/as.</p><br/>
			<p>Adem&aacute;s, puede consultar todos los anuncios que figuran en el tabl&oacute;n, realizar modificaciones en los textos que usted ha publicado, o eliminar aqu&eacute;llos que ya resulten obsoletos. </p>
		</div>
	<?php else:?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/tablon_anuncios/news.png" style="vertical-align:middle;" />
				<span data-translate-html="tablon_anuncios.titulo">Tabl&oacute;n p&uacute;blico</span>
			</div>
			<br/>
			<p data-translate-html="tablon_anuncios.descripcion">
				Utilice el tabl&oacute;n de anuncios para ver las noticias, acontecimientos, recordatorios y mensajes relacionados con el curso, que considere interesante dar a conocer su tutor/ra.
			</p>
		</div>
	<?php endif;?>
	
	<?php require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']); ?>	
</div>
