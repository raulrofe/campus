<div class="caja_separada">
	<div id="anuncios">

		<?php require(dirname(__FILE__) . '/menu_tutores.php'); ?>
		<div class='subtitleModulo'>
			<span data-translate-html="tablon_anuncios.insertar">
				Insertar anuncio
			</span>
		</div>

		<div class='clear'></div>

		<div class='caja_frm'>
			<div>
				<form id="frmTablonAnuncioHallNuevo" name='' method='post' action='' enctype='multipart/form-data'>
					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="formulario.titulo">
							T&iacute;tulo
						</div>

						<div class='campofrm'><input type='text' name='titulo' size='65'/></div>
					</div>

					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="formulario.descripcion">
							Descripci&oacute;n
						</div>
						<div class='campofrm'>
							<textarea id="frmTablonAnuncioHallNuevo_1" name='descripcion' rows='12' cols='49'>
							</textarea>
						</div>
					</div>

					<div class='filafrm'>
						<div class='etiquetafrm' data-translate-html="formulario.archivo" >
							Archivo
						</div>

						<div class="fleft">
							<div>
									<input type='file' name='archivotablon' size='30'/>
							</div>
						</div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'><input type='submit' value='Enviar' data-translate-value="formulario.enviar"/></div>
					</div>
				</form>
			</div>
		</div>

		<script type="text/javascript" src="js-tablon_anuncios-nuevo.js"></script>

		<script type="text/javascript">$('#frmTablonAnuncioHallNuevo input.multi').MultiFile();</script>

		<script type="text/javascript">
			tinymce.init({
				selector: "textarea",
				menubar: false,
				statusbar: false,
				plugins: ["emoticons"],
				toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
			});
		</script>
	</div>
</div>