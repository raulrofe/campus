<div class='menu_interno'>
	<span class='elemento_menuinterno'>
		<a href='tablon_anuncios' data-translate-html="tablon_anuncios.veranuncios">
			Ver anuncios
		</a>
	</span>

	<span class='elemento_menuinterno'>
		<a href='tablon_anuncios/insertar' data-translate-html="tablon_anuncios.insertar">
			Insertar anuncio
		</a>
	</span>

	<?php if(Usuario::compareProfile('coordinador')):?>
		<span class="elemento_menuinterno">
			<a href="tablon_anuncios/administrar" data-translate-html="tablon_anuncios.administrar">
				Administrar anuncios
			</a>
		</span>
	<?php endif; ?>
</div>
