<div class="caja_separada">
	<div id="anuncios">

		<?php require(dirname(__FILE__) . '/menu_tutores.php'); ?>
		<div class='subtitleModulo'><span>Editar anuncio</span></div>
		<div class='clear'></div>

		<div class='caja_frm'>
			<div>
				<form id="frmTablonAnuncioHallEditar" name='' method='post' action='' enctype="multipart/form-data">
					<div class='filafrm'>
						<div class='etiquetafrm'>T&iacute;tulo</div>
						<div class='campofrm'><input type='text' name='titulo' value='<?php echo Texto::textoPlano($registros_anuncio['titulo_anuncio']); ?>' size='65'/></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Descripci&oacute;n</div>
						<div class='campofrm'><textarea id="frmTablonAnuncioHallEditar_1" name='descripcion' rows='12' cols='49'><?php echo $registros_anuncio['descripcion_anuncio']; ?></textarea></div>
					</div>
					<div class='filafrm'>
						<div class='etiquetafrm'>Archivo</div>
						<div class='campofrm'>
							<div>
									<input type='file' name='archivotablon' size='30'/>
							</div>
							<?php if (!empty($registros_anuncio['archivo_tablon'])): ?>
								<div id="frmTablonAnuncioHallEditarFile">
									<ul>
										<li>
											<a rel="tooltip" title="Archivo adjunto" href="tablon_anuncios/forzar_descarga/<?php echo Texto::textoPlano($registros_anuncio['idanuncio']) ?>" target="_blank">
												<img alt="" src="imagenes/options/archivo-adjunto.png">
											</a>
										</li>
										<li>
											<a rel="tooltip" title="Eliminar archivo adjunto" <?php echo Alerta::alertConfirmOnClick('eliminaradjunto', '¿Realmente quieres eliminar el archivo adjunto?', 'tablon_anuncios/eliminar-adjunto/' . $registros_anuncio['idanuncio']) ?>>
												<img alt="" src="imagenes/options/delete.png">
											</a>
										</li>
									</ul>
									<div class='clear'></div>
								</div>
							<?php endif; ?>
						</div>
					</div>

					<div><input type='submit' value='Actualizar' class="width100" /></div>

					<div class='clear'></div>
					<!-- <input type='hidden' name='v' value='<?php //echo $v; ?>' /> -->
					<!-- <input type='hidden' name='v2' value='<?php //echo $v2; ?>' /> -->
					<input type='hidden' name='idanuncio' value='<?php echo $registros_anuncio['idanuncio']; ?>' />
				</form>
			</div>
		</div>

		<script type="text/javascript" src="js-tablon_anuncios-editar.js"></script>

		<script type="text/javascript">
			tinymce.init({
				selector: "textarea",
				menubar: false,
				statusbar: false,
				plugins: ["emoticons"],
				toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
			});
		</script>
	</div>
</div>