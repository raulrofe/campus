<?php 
class TablonAnuncios extends modeloExtend{
	
	private $idstaff;
	private $idanuncio;
	
	//Archivos pertidos para adjuntar
	private $extensionesAdjunto = array(
		'jpg',
		'jpeg',
		'png',
		'doc', 
		'docx', 
		'xls', 
		'xlsx', 
		'pdf',
		'ppt',
		'pptx',
		'swf',
		'bmp',
		'tiff',
		'tif',
		'txt',
		'text',
		'html',
		'htm',
		'css',
		'zip',
		'rar'
	);
	
	public function __construct()
	{
		parent::__construct();
	}

	public function set_anuncio($idanuncio = null)
	{
		if(isset($idanuncio))
		{
			$this->idanuncio = $idanuncio;
		}
		else
		{
			$get = Peticion::obtenerGet();
			
			if(isset($get['idanuncio']) && is_numeric( $get['idanuncio']))
			{
				$this->idanuncio = $get['idanuncio'];
			}
			else
			{
				exit;
				
				Url::lanzar404();
			}
		}
	}
	
	public function insertar_anuncio($titulo,$descripcion, $idUsuario, $idCurso)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall (titulo_anuncio,descripcion_anuncio,fecha,idusuario, idcurso)
		VALUES ('$titulo','$descripcion','$fecha','$idUsuario', '$idCurso')";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{	
			if ($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					$extension =  Texto::obtenerExtension($_FILES['archivotablon']['name']);	
					$directorio = 'archivos/tablon_anuncios_hall/';
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$id.'.'.$extension) or die("Ocurrio un problema al intentar subir el archivo.");
					if(file_exists($directorio.$id.'.'.$extension))
					{
						$archivoTablon = $directorio.$id.'.'.$extension;
						$nombreArchivo = Texto::clearFilename($_FILES['archivotablon']['name']);
						$sql = "UPDATE anuncio_hall SET archivo_tablon = '$archivoTablon', nombre_archivo='$nombreArchivo' where idanuncio = ".$id;
						$resultado = $this->consultaSql($sql);
					}
					else echo "Error al mover el archivo";
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}
			}
			//else echo "Error al subir el archivo";			
		}
		return $resultado;
	}

	public function aceptarEdicionPeticionCopia($titulo, $descripcion, $idUsuario, $idCurso, $idUsuarioHall)
	{
		$fecha = Fecha::fechaActual();

		$sql = "UPDATE anuncio_hall SET titulo_anuncio = '$titulo', descripcion_anuncio = '$descripcion',
		fecha = '$fecha', idusuario = '$idUsuario', idcurso = '$idCurso'
		WHERE idanuncio = $idUsuarioHall";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function eliminarAdjunto($idUsuarioHall)
	{
		$sql = "UPDATE anuncio_hall SET archivo_tablon = '', nombre_archivo = '' WHERE idanuncio = $idUsuarioHall";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function aceptarPeticionCopia($titulo, $descripcion, $idUsuario, $idCurso)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall (titulo_anuncio,descripcion_anuncio,fecha,idusuario, idcurso)
		VALUES ('$titulo','$descripcion','$fecha','$idUsuario', '$idCurso')";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function aceptarPeticionCopiaAdjunto($idAnuncio, $archivoTablon, $nombreArchivo)
	{
		$sql = "UPDATE anuncio_hall SET archivo_tablon='$archivoTablon', nombre_archivo='$nombreArchivo' WHERE idanuncio=$idAnuncio";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function insertar_anuncio_moderar($titulo, $descripcion, $idUsuario, $idCurso)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall_admin (titulo_anuncio,descripcion_anuncio,fecha,idusuario, idcurso)
			VALUES ('$titulo','$descripcion','$fecha','$idUsuario', '$idCurso')";
		
		$resultado = $this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{	
			if ($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					$extension =  Texto::obtenerExtension($_FILES['archivotablon']['name']);	
					$directorio = 'archivos/tablon_anuncios_hall_admin/';
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$id.'.'.$extension) or die("Ocurrio un problema al intentar subir el archivo.");
					if(file_exists($directorio.$id.'.'.$extension))
					{
						$archivoTablon = $directorio.$id.'.'.$extension;
						$nombreArchivo = Texto::clearFilename($_FILES['archivotablon']['name']);
						$sql = "UPDATE anuncio_hall_admin SET archivo_tablon = '$archivoTablon', nombre_archivo='$nombreArchivo' where idanuncio = ".$id;
						$resultado = $this->consultaSql($sql);
					}
					else echo "Error al mover el archivo";
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}
			}
			//else echo "Error al subir el archivo";			
		}
		return $resultado;
	}
	
	/*public function aceptarPeticionCopiaAdjunto($idanuncio)
	{
		$sql = 'UPDATE anuncio_hall SET archivo_tablon = "" WHERE idanuncio = ' . $idanuncio;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}*/
	
	public function eliminar_anuncio_moderar($idUsuario, $idCurso, $idAnuncioHall)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall_admin (fecha,idusuario, idcurso, modo, idanuncio_hall)
			VALUES ('$fecha','$idUsuario', '$idCurso', 'eliminar', '$idAnuncioHall')";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminar_anuncio_adjunto_moderar($idUsuario, $idCurso, $idAnuncioHall)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall_admin (fecha,idusuario, idcurso, modo, idanuncio_hall)
			VALUES ('$fecha','$idUsuario', '$idCurso', 'eliminar-adjunto', '$idAnuncioHall')";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function editar_anuncio_moderar($titulo, $descripcion, $idUsuario, $idCurso, $idAnuncioHall)
	{
		$fecha = Fecha::fechaActual();

		$sql = "INSERT into anuncio_hall_admin (titulo_anuncio,descripcion_anuncio,fecha,idusuario, idcurso, modo, idanuncio_hall)
			VALUES ('$titulo','$descripcion','$fecha','$idUsuario', '$idCurso', 'editar', '$idAnuncioHall')";
		
		$resultado = $this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{	
			if ($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					$extension =  Texto::obtenerExtension($_FILES['archivotablon']['name']);	
					$directorio = 'archivos/tablon_anuncios_hall_admin/';
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$id.'.'.$extension) or die("Ocurrio un problema al intentar subir el archivo.");
					if(file_exists($directorio.$id.'.'.$extension))
					{
						$archivoTablon = $directorio.$id.'.'.$extension;
						$nombreArchivo = Texto::clearFilename($_FILES['archivotablon']['name']);
						$sql = "UPDATE anuncio_hall_admin SET archivo_tablon = '$archivoTablon', nombre_archivo='$nombreArchivo' where idanuncio = ".$id;
						$resultado = $this->consultaSql($sql);
					}
					else echo "Error al mover el archivo";
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}
			}
			//else echo "Error al subir el archivo";			
		}
		return $resultado;
	}

	public function modificar_anuncio($titulo,$descripcion){
		$fecha = date("Y-m-d");
		$sql = "UPDATE anuncio_hall SET titulo_anuncio = '$titulo' , descripcion_anuncio = '$descripcion'
		where idanuncio = ".$this->idanuncio;
		//echo $sql;
		$resultado2 = $this->consultaSql($sql);
		
		$sql = "SELECT * from anuncio_hall where idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		$registroArchivo = mysqli_fetch_assoc($resultado);
		
		if(isset($_FILES['archivotablon']['tmp_name']))
		{
			if($_FILES['archivotablon']['error'] == 0)
			{
				if($archivosNoValidos = Fichero::validarTipos($_FILES['archivotablon']['name'], $_FILES['archivotablon']['type'], $this->extensionesAdjunto))
				{
					if($registroArchivo['archivo_tablon'] != NULL)
					{
						unlink($registroArchivo['archivo_tablon']);	
					}
					
					$extension = Texto::obtenerExtension($_FILES['archivotablon']['name']);
					$directorio = 'archivos/tablon_anuncios_hall/';
					$nombreArchivo = $this->idanuncio.'.'.$extension;
					$archivoTablon = $directorio.$nombreArchivo;
					move_uploaded_file($_FILES["archivotablon"]["tmp_name"],$directorio.$nombreArchivo) or die("Ocurrio un problema al intentar subir el archivo.");	
					$sql = "UPDATE anuncio_hall SET nombre_archivo = '$nombreArchivo', archivo_tablon = '$archivoTablon' where idanuncio = ".$this->idanuncio;
					$this->consultaSql($sql);
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivonovalido','El tipo de archivo no es valido');
				}
			}
		}
		
		return $resultado2;
	}
	
	public function ver_anuncios($limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = "SELECT ah.idanuncio, ah.titulo_anuncio, ah.descripcion_anuncio, ah.fecha, ah.archivo_tablon, ah.idusuario, 
		rh.nombrec, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl, cs.titulo" .
		" FROM anuncio_hall as ah" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = ah.idusuario" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ah.idusuario" .
		" LEFT JOIN curso as cs ON cs.idcurso = ah.idcurso" .
		" WHERE ah.borrado = 0" .
		" ORDER BY ah.idanuncio DESC" . $addQuery;			
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;	
	}
	
	public function buscar_anuncio(){
		$sql = "SELECT *" .
		" FROM anuncio_hall as a" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = a.idusuario" .
		" LEFT JOIN staff s ON s.idrrhh = rh.idrrhh AND s.idcurso = " . Usuario::getIdCurso() .
		" where a.borrado = 0 AND idanuncio = ".$this->idanuncio;

		$resultado = $this->consultaSql($sql);
		return mysqli_fetch_assoc($resultado);
	}
	
	public function buscar_anuncio_moderar($idanuncio)
	{
		$sql = "SELECT *, a.idcurso, a.idusuario" .
		" FROM anuncio_hall_admin as a" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = a.idusuario" .
		" LEFT JOIN staff s ON s.idrrhh = rh.idrrhh " .
		" where moderado = 0 AND idanuncio = " . $idanuncio;

		$resultado = $this->consultaSql($sql);
		return mysqli_fetch_assoc($resultado);
	}
	
	public function eliminar_anuncio(){
		$sql="SELECT * from anuncio_hall where idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		$registroArchivo = mysqli_fetch_assoc($resultado);
		
		
		if($registroArchivo['archivo_tablon'] != NULL)
		{
			if(file_exists($registroArchivo['archivo_tablon']))
			{
				chmod($registroArchivo['archivo_tablon'], 0777);
				unlink($registroArchivo['archivo_tablon']);
			}
		}
		
		$sql = "UPDATE anuncio_hall SET borrado = 1 where idanuncio = ".$this->idanuncio;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Obtenemos el id del staff (relacion curso - tutor)
	public function buscar_staff(){
		$sql = "SELECT * from staff where idcurso = ".$_SESSION['idcurso']." 
		and idrrhh = ".$_SESSION['idusuario'];
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->idstaff = $f['idstaff'];
		return $this->idstaff;
	}
	
	public function numero_anuncios()
	{
		$idCurso = Usuario::getIdCurso();
		$idAlumno = Usuario::getIdUser();
		
		$sql = "SELECT A.idanuncio" .
		" FROM anuncio_hall as A" .
		" WHERE A.fecha > (SELECT MAX(LLA.fecha_entrada)" .
			" FROM logs_acceso as LLA" .
			" LEFT JOIN matricula as MM ON MM.idmatricula = LLA.idmatricula" .
			" WHERE LLA.idlugar = 20" .
			" AND A.borrado = 0 " .
			" AND MM.idalumnos = " . $idAlumno .
			" AND MM.idcurso = " . $idCurso . 
			" AND MM.idmatricula = LLA.idmatricula)";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function anunciosPendientes()
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = "SELECT ah.idanuncio, ah.titulo_anuncio, ah.descripcion_anuncio, ah.fecha, ah.archivo_tablon, ah.idusuario, 
		rh.nombrec, CONCAT(a.nombre, ' ', a.apellidos) as nombrecAl, c.titulo, ah.modo" .
		" FROM anuncio_hall_admin as ah" .
		" LEFT JOIN rrhh as rh ON rh.idasociado = ah.idusuario" .
		" LEFT JOIN alumnos as a ON a.idalumnos = ah.idusuario" .
		" LEFT JOIN curso as c ON c.idcurso = ah.idcurso" .
		" WHERE moderado = 0" .
		" ORDER BY ah.idanuncio DESC" . $addQuery;			
	
		$resultado = $this->consultaSql($sql);
		
		return $resultado;	
	}
	
	public function aceptarPeticion($idAnuncio)
	{
		$sql = "UPDATE anuncio_hall_admin" .
		" SET moderado = 1" .
		" WHERE idanuncio = " . $idAnuncio;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function denegarPeticion($idAnuncio)
	{
		$sql = "UPDATE anuncio_hall_admin" .
		" SET moderado = 1" .
		" WHERE idanuncio = " . $idAnuncio;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumeroAnunciosAlumno($idAlumno, $idCurso)
	{
		$sql = "SELECT idanuncio as nAnuncios" .
		" FROM anuncio_hall" .
		" WHERE idusuario = " . $idAlumno .
		" AND borrado = 0" .
		" AND idcurso = " . $idCurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}
?>