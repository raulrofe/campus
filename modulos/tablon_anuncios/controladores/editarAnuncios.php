<?php

$mi_anuncio = new TablonAnuncios();

$get = Peticion::obtenerGet();
extract(Peticion::obtenerPost());

if(isset($get['idanuncio']) && is_numeric($get['idanuncio']))
{
	$mi_anuncio->set_anuncio();
	
	$registros_anuncio = $mi_anuncio->buscar_anuncio();
	if(isset($registros_anuncio) && $registros_anuncio['idusuario'] == Usuario::getIdUser(true) || Usuario::compareProfile('coordinador'))
	{
		if(isset($get['opcion']) && $get['opcion'] = 'eliminar_adjunto')
		{
			if(Usuario::compareProfile('coordinador'))
			{
				if($mi_anuncio->eliminarAdjunto($get['idanuncio']))
				{
					if(file_exists(PATH_ROOT . $registros_anuncio['archivo_tablon']))
					{
						chmod(PATH_ROOT . $registros_anuncio['archivo_tablon'], 0777);
						unlink(PATH_ROOT . $registros_anuncio['archivo_tablon']);
					}
					
					Url::redirect('tablon_anuncios');
				}
			}
			else
			{
				if($mi_anuncio->eliminar_anuncio_adjunto_moderar(Usuario::getIdUser(true), Usuario::getIdCurso(), $get['idanuncio']))
				{
					Alerta::guardarMensajeInfo('archivoanuncioparamoderar','Archivo de anuncio mandado a moderar');
					Url::redirect('tablon_anuncios');	
				}
			}
		}
		else
		{
			if(!empty($titulo) and ($descripcion))
			{
				if(Usuario::compareProfile('coordinador'))
				{
					if($mi_anuncio->modificar_anuncio($titulo,$descripcion))
					{
						Alerta::guardarMensajeInfo('anuncioactualizado', 'Anuncio actualizado');
						Url::redirect('tablon_anuncios');	
					}
				}
				else
				{
					if($mi_anuncio->editar_anuncio_moderar($titulo, $descripcion, Usuario::getIdUser(true), Usuario::getIdCurso(), $get['idanuncio']))
					{
						Alerta::guardarMensajeInfo('anuncioparamoderar','Anuncio mandado a moderar');
						Url::redirect('tablon_anuncios');	
					}
				}
			}
		}
		
		$registros_anuncio = $mi_anuncio->buscar_anuncio();
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'anuncios');
	}
}