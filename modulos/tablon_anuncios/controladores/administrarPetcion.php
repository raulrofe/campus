<?php

if(!Usuario::compareProfile('coordinador'))
{
	Url::lanzar404();	
}

$get = Peticion::obtenerGet();

$mi_anuncio = new TablonAnuncios();

if(isset($get['idAnuncio'], $get['accion']) && is_numeric($get['idAnuncio']))
{
	$row = $mi_anuncio->buscar_anuncio_moderar($get['idAnuncio']);
	if(!empty($row))
	{
		if($get['accion'] == 'aceptar')
		{
			//var_dump($row);exit;
			if($mi_anuncio->aceptarPeticion($get['idAnuncio']))
			{
				// moderar nueva frase
				if(!isset($row['idanuncio_hall']) && !isset($row['modo']))
				{
					if($mi_anuncio->aceptarPeticionCopia($row['titulo_anuncio'], $row['descripcion_anuncio'], $row['idusuario'], $row['idcurso']))
					{
						$idAnuncio = $mi_anuncio->obtenerUltimoIdInsertado();
						
						$directorio = 'archivos/tablon_anuncios_hall/';
						$extension = Texto::obtenerExtension($row['nombre_archivo']);
						$nuevoNombreArchivo = $directorio . $idAnuncio . '.' . $extension;
						
						if(file_exists($row['archivo_tablon']))
						{
							if($mi_anuncio->aceptarPeticionCopiaAdjunto($idAnuncio, $nuevoNombreArchivo, $row['nombre_archivo']))
							{
								copy($row['archivo_tablon'], $nuevoNombreArchivo);
								chmod($row['archivo_tablon'], 0777);
								unlink($row['archivo_tablon']);
							}
						}
						
						Alerta::guardarMensajeInfo('peticionanuncioaceptada','La petición de anuncio ha sido aceptada');
					}
				}
				// moderar editar frase
				else if(isset($row['modo']) && $row['modo'] == 'editar')
				{
					if($mi_anuncio->aceptarEdicionPeticionCopia($row['titulo_anuncio'], $row['descripcion_anuncio'], $row['idusuario'],
							$row['idcurso'], $row['idanuncio_hall']))
					{
						$idAnuncio = $row['idanuncio_hall'];
						
						$directorio = 'archivos/tablon_anuncios_hall/';
						$extension = Texto::obtenerExtension($row['nombre_archivo']);
						$nuevoNombreArchivo = $directorio . $idAnuncio . '.' . $extension;
						
						if(file_exists($row['archivo_tablon']))
						{
							if($mi_anuncio->aceptarPeticionCopiaAdjunto($idAnuncio, $nuevoNombreArchivo, $row['nombre_archivo']))
							{
								// eliminamos imagen actual del anuncio
								$mi_anuncio->set_anuncio($row['idanuncio_hall']);
								$row2 = $mi_anuncio->buscar_anuncio();
								if(!empty($row2))
								{
									if(file_exists($row2['archivo_tablon']))
									{
										chmod($row2['archivo_tablon'], 0777);
										unlink($row2['archivo_tablon']);
									}
								}
								
								// movemos la imagen del anuncio en moderacion al anuncio real
								copy($row['archivo_tablon'], $nuevoNombreArchivo);
								chmod($row['archivo_tablon'], 0777);
								unlink($row['archivo_tablon']);
							}
						}
						
						Alerta::guardarMensajeInfo('peticionanuncioaceptada','peticionanuncioaceptada','La petición de anuncio ha sido aceptada');
					}
				}
				// moderar eliminar archivo frase
				else if(isset($row['modo']) && $row['modo'] == 'eliminar-adjunto')
				{
					$mi_anuncio->set_anuncio($row['idanuncio_hall']);
					$row2 = $mi_anuncio->buscar_anuncio();
					if(!empty($row2))
					{
						if($mi_anuncio->eliminarAdjunto($row2['idanuncio']))
						{
							if(file_exists($row2['archivo_tablon']))
							{
								chmod($row2['archivo_tablon'], 0777);
								unlink($row2['archivo_tablon']);
							}
						}
					}
				}
				// moderar eliminar frase
				else if(isset($row['modo']) && $row['modo'] == 'eliminar')
				{
					$mi_anuncio->set_anuncio($row['idanuncio_hall']);
					$row2 = $mi_anuncio->buscar_anuncio();
					if(!empty($row2))
					{
						if($mi_anuncio->eliminar_anuncio($row2['idanuncio']))
						{
							Alerta::guardarMensajeInfo('anuncioeliminado','El anuncio fue eliminado');
						}
					}
				}
			}	
		}
		else if($get['accion'] == 'denegar')
		{
			if($mi_anuncio->denegarPeticion($get['idAnuncio']))
			{	
				if(file_exists($row['archivo_tablon']))
				{
					chmod($row['archivo_tablon'], 0777);
					unlink($row['archivo_tablon']);
				}
				
				Alerta::guardarMensajeInfo('peticionanunciorechazada','La petición de anuncio ha sido rechazada');
			}
		}
	}
}

Url::redirect('tablon_anuncios/administrar');