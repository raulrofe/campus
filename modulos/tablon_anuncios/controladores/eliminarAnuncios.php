<?php

$mi_anuncio = new TablonAnuncios();

$get = Peticion::obtenerGet();

if(isset($get['idanuncio']) && is_numeric($get['idanuncio']) || Usuario::compareProfile('coordinador'))
{
	$mi_anuncio->set_anuncio();
	
	$registros_anuncio = $mi_anuncio->buscar_anuncio();
	
	if(Usuario::compareProfile('coordinador'))
	{
		if(isset($registros_anuncio))
		{
			$mi_anuncio->set_anuncio();
			if($mi_anuncio->eliminar_anuncio())
			{
				Alerta::guardarMensajeInfo('anuncioeliminado','Anuncio eliminado');
			}
		}
	}
	else
	{
		if(isset($registros_anuncio) && $registros_anuncio['idusuario'] == Usuario::getIdUser(true))
		{
			$mi_anuncio->set_anuncio();
			if($mi_anuncio->eliminar_anuncio_moderar(Usuario::getIdUser(true), Usuario::getIdCurso(), $get['idanuncio']))
			{
				Alerta::guardarMensajeInfo('anunciomoderadoeliminar','Anuncio moderado para eliminar');
			}
		}
	}
}

Url::redirect('tablon_anuncios');