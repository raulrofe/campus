<?php
/*
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}
*/

$get = Peticion::obtenerGet();

$mi_anuncio = new TablonAnuncios();
$obj = new modeloExtend();

extract(Peticion::obtenerPost());

$mi_anuncio->buscar_staff();

$idCurso = Usuario::getIdCurso();

if(!empty($titulo) and ($descripcion))
{
	if(!Usuario::compareProfile('coordinador'))
	{
		$idUsuario = Usuario::getIdUser(true);
		
		if($mi_anuncio->insertar_anuncio_moderar($titulo,$descripcion, $idUsuario, $idCurso))
		{
			Alerta::guardarMensajeInfo('anuncioparamoderar','Anuncio mandado a moderar');
			Url::redirect('tablon_anuncios');	
		}
	}
	else
	{
		$idUsuario = Usuario::getIdUser(true);
		
		if($mi_anuncio->insertar_anuncio($titulo,$descripcion, $idUsuario, $idCurso))
		{
			Alerta::guardarMensajeInfo('anuncioinsertado','Anuncio insertado');
			Url::redirect('tablon_anuncios');	
		}
	}
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'anuncios');
