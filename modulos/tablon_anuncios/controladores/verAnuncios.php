<?php 
$get = Peticion::obtenerGet();

$mi_anuncio = new TablonAnuncios();

$idstaff = $mi_anuncio->buscar_staff();

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}

$idUsuario = Usuario::getIdUser(true);

$mensajesCount = $mi_anuncio->ver_anuncios();
$maxElementsPaging = 7;
$maxPaging = $mensajesCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;		
	
$registros_anuncios = $mi_anuncio->ver_anuncios($elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'anuncios');
