<?php
if(!Usuario::compareProfile('coordinador'))
{
	Url::lanzar404();
}

$get = Peticion::obtenerGet();

require 'configMimes.php';

$idusuario = Usuario::getIdUser(true);

$modeloTablonAnuncios = new TablonAnuncios();

$rowAdjunto = $modeloTablonAnuncios->buscar_anuncio_moderar($get['idanuncio']);
if(!empty($rowAdjunto))
{
	$extension = Fichero::obtenerExtension($rowAdjunto['archivo_tablon']);
	if(file_exists($rowAdjunto['archivo_tablon']) && isset($mimes[$extension]))
	{
		header("Content-disposition: attachment; filename=" . $rowAdjunto['nombre_archivo']);
		header("Content-type: application/octet-stream");
		readfile($rowAdjunto['archivo_tablon']);
	}
	else
	{
		Url::lanzar404();
	}
}
else
{
	Url::lanzar404();
}