<?php
if(!Usuario::compareProfile(array('tutor', 'coordinador')))
{
	Url::lanzar404();
}

if(!mvc::accesoServicio('autoevaluacion'))
{
	exit;
}
?>

<script type="text/javascript" src="js-autoevaluaciones-default.js"></script>
