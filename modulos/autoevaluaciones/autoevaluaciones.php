<?php
require_once PATH_ROOT .  'lib/thumb/ThumbLib.inc.php';

class Autoevaluaciones{
	
	
	public static function obtener_letra($letra){
		switch ($letra){
			case '1':
				$letra = 'A - ';
				break;
			case '2':
				$letra = 'B - ';
				break;
			case '3':
				$letra = 'C - ';
				break;
			case '4':
				$letra = 'D - ';
				break;
			case '5':
				$letra = 'E - ';
				break;
			case '6':
				$letra = 'F - ';
				break;
			case '7':
				$letra = 'G - ';
				break;
			case '8':
				$letra = 'H - ';
				break;
			case '9':
				$letra = 'I - ';
				break;
			case '10':
				$letra = 'J - ';
				break;
		}
		return $letra;
	}

	public static function guardarFoto($imagen, $id, $para)
	{
	
		//echo $para;
		
		$db = new database();
		$con = $db->conectar();

		$objThumb = PhpThumbFactory::create($imagen['tmp_name']);
		
		//echo $imagen['tmp_name'];
						
		$newFilename = $id . '.' . strtolower($objThumb->getFormat());
		
			if($para == 'pregunta') 
			{
				$path = 'imagenes/autoevaluaciones/preguntas/';
			}
			else 
			{
				$path = 'imagenes/autoevaluaciones/respuestas/';
			}
			
			//echo $path;
						
		$objThumb->resize(250, 250);
		$objThumb->save($path . $newFilename);
		
		@chmod($path . $usuario->foto, 0777);
		@unlink($path . $usuario->foto);
							
		if($para == 'pregunta')
		{
			$sql = "UPDATE plantilla_preguntas SET PTimagen_pregunta = '".$path.$newFilename."' where PTidpreguntas = ".$id;
		}
		else
		{
			$sql = "UPDATE plantilla_respuesta SET PTimagen_respuesta = '".$path.$newFilename."' where PTidrespuesta = ".$id;
		}
		//echo $sql;
		mysqli_query($con,$sql);
	}

	public static function guardarFotoCurso($imagen, $id, $para)
	{
	
		//echo $para;
		
		$db = new database();
		$con = $db->conectar();

		$objThumb = PhpThumbFactory::create($imagen['tmp_name']);
		
		//echo $imagen['tmp_name'];
						
		$newFilename = $id . '.' . strtolower($objThumb->getFormat());
		
			if($para == 'pregunta') 
			{
				$path = 'imagenes/autoevaluaciones/preguntas/';
			}
			else 
			{
				$path = 'imagenes/autoevaluaciones/respuestas/';
			}
			
			//echo $path;
						
		$objThumb->resize(250, 250);
		$objThumb->save($path . $newFilename);
		
		@chmod($path . $usuario->foto, 0777);
		@unlink($path . $usuario->foto);
							
		if($para == 'pregunta')
		{
			$sql = "UPDATE preguntas SET imagen_pregunta = '".$path.$newFilename."' where idpreguntas = ".$id;
		}
		else
		{
			$sql = "UPDATE respuesta SET imagen_respuesta = '".$path.$newFilename."' where idrespuesta = ".$id;
		}
		//echo $sql;
		mysqli_query($con,$sql);
	}

}
?>