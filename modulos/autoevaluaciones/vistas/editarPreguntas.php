<div class='subtitleModulo' data-translate-html="gestioncurso.pr_res">
	Editar Pregunta y Respuestas
</div>

<div class='cajafrm'>
	<form method='post' action='autoevaluaciones/preguntas/editar/<?php echo $get['idpregunta']; ?>' enctype='multipart/form-data' id="editarPreguntas">
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.enunciado">
				Enunciado pregunta
			</div>
			<div class='campofrm'><textarea name='enunciado' class='estilotextarea2'><?php echo Texto::textoPlano($pre->pregunta); ?></textarea></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
				Im&aacute;gen
			</div>
			<div class='campofrm'>
				<?php if (!empty($pre->imagen_pregunta)) echo "<img src='" . $pre->imagen_pregunta . "' alt='' width='150px' class='fleft'/><br/>"; ?>
					<input type='file' name='imagen_pregunta' size='50'/>
			</div>
		<div class='subtitleModulo' data-translate-html="gestioncurso.editar_res">
			Editar Respuestas
		</div>

		<?php
		$cont = 1;
		if ($registros_respuestas->num_rows > 0) {
			while ($res = $registros_respuestas->fetch_object()) {
				?>
				<div class='filafrm'>
					<div class='etiquetafrm'>
						<span data-translate-html="gestioncurso.respuesta">
							Respuesta
						</span> 
		<?php echo $cont; ?>
					</div>

					<div class='campofrm'>
						<input type='text' name='respuesta[]' size='60' value='<?php echo Texto::textoPlano($res->respuesta); ?>' />
					</div>
					<div class='campofrm' style='margin-left:10px;'>
						<input type='radio' name='correcta[]' value='<?php echo $cont; ?>' <?php if ($res->correcta == '1') echo 'checked'; ?>/>
					</div>
				</div>
				<div class='filafrm'> 

					<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
						Im&aacute;gen
					</div>

					<div class='campofrm'>
		<?php if (!empty($res->imagen_respuesta)): ?>
							<?php echo "<img src='" . $res->imagen_respuesta . "' alt='' width='150px' class='fleft'/><br/>"; ?>
							<a href="#" <?php echo Alerta::alertConfirmOnClick('eliminarimagen', '¿Realmente quieres eliminar esta imagen?', 'autoevaluaciones/pregunta/imagen/eliminar/' . $res->idrespuesta) ?> data-translate-html="general.eliminar">
								Eliminar
							</a>
		<?php endif; ?>
						<input type='file' name='imagen_respuesta_<?php echo $cont; ?>' size='50'/>
					</div>
				</div>
				<br/>
		<?php
		$cont++;
		echo "<input type='hidden' name='idrespuesta[]' value='" . $res->idrespuesta . "' />";
	}
}
else {
	?>
			<div class='filafrm'>
				<div class='etiquetafrm' >
					<span data-translate-html="gestioncurso.respuesta">Respuesta</span> 1
				</div>
				<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
				<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='1' /></div>
			</div>
			<div class='filafrm'> 
				<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
					Im&aacute;gen
				</div>
				<div class='campofrm'>
					<input type='file' name='imagen_respuesta_1' size='50' /></div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' >
					<span data-translate-html="gestioncurso.respuesta">Respuesta</span> 2
				</div>
				<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
				<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='2' /></div>
			</div>
			<div class='filafrm'> 
				<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
					Im&aacute;gen
				</div>
				<div class='campofrm'>
					<input type='file' name='imagen_respuesta_2' size='50' /></div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm' >
					<span data-translate-html="gestioncurso.respuesta">Respuesta</span> 3
				</div>
				<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
				<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='3' /></div>
			</div>
			<div class='filafrm'> 
				<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
					Im&aacute;gen
				</div>
				<div class='campofrm'>
					<input type='file' name='imagen_respuesta_3' size='50' /></div>
			</div>

			<div class='filafrm'>
				<div class='etiquetafrm'>
					<span data-translate-html="gestioncurso.respuesta">Respuesta</span> 4
				</div>
				<div class='campofrm'><input type='text' name='respuesta[]' size='60' /></div>
				<div class='campofrm' style='margin-left:10px;'><input type='radio' name='correcta[]' value='4' /></div>
			</div>
			<div class='filafrm'> 
				<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
					Im&aacute;gen
				</div>
				<div class='campofrm'>
					<input type='file' name='imagen_respuesta_4' size='50' /></div>
			</div>
			<input type='hidden' name='idPregunta' value='<?php echo $pre->idpreguntas; ?>' />
	<?php
}
?>
		<input type='submit' value='modificar' data-translate-value="gestioncurso.modificar"/>
	</form>
</div>	

<script src="js-autoevaluaciones-editarpreguntas.js"></script>
