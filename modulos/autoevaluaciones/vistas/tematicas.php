<div class='subtitleModulo' data-translate-html="gestioncurso.tematica">
	Tem&aacute;ticas para autoevaluaciones
</div>

<div class='cajafrm'>
	<form method='post' action='autoevaluaciones/tematica' name='tematica'>
		<div class='t_center' style='margin-bottom:10px;background-color:#ccc;padding:5px 0;'>
			<select name='idTematica' onchange="this.form.submit()">
				<option value='' data-translate-html="gestioncurso.seleccione_te"> - Selecciona una tem&aacute;tica - </option>
				<?php while($temas = mysqli_fetch_assoc($tematicas)):?>
					<?php if($temas['idtematica_autoeval'] == $post['idTematica']):?> 
						<option value="<?php echo $temas['idtematica_autoeval']; ?>" selected ><?php echo $temas['tematica_autoeval']; ?></option>
					<?php else:?>
						<option value="<?php echo $temas['idtematica_autoeval']; ?>"><?php echo $temas['tematica_autoeval']; ?></option>
					<?php endif; ?>
				<?php endwhile; ?>
			</select>
		</div>
	</form>

	<div style='background-color:#eee;padding:10px'>
		<form method='post' action='autoevaluaciones/tematica' name='insertar tematica' >
			<div class='filafrm'>
				<div class='etiquetafrm' data-translate-html="gestioncurso.te_comun" > Tem&aacute;tica com&uacute;n</div>
				<?php if(isset($datosTematica)):?>
					<div class='campofrm'><input type='text' name='atematica' value='<?php echo $rowTematica['tematica_autoeval']; ?>' /></div>
					<input type='hidden' name='idTematica' value='<?php echo $post['idTematica']; ?>' />
				<?php else:?>
					<div class='campofrm'><input type='text' name='tematica' /></div>
				<?php endif; ?>
			</div>
			<div class='t_center'><input type='submit' value='Enviar' data-translate-value="formulario.enviar" /></div>
		</form>
	</div>
</div>

