<br/>
<div class='t_center' style='background-color:#ccc;padding:5px 0;margin-bottom:5px;font-weight:bold;'>
	<a href='autoevaluaciones/asignar' data-translate-html="gestioncurso.nueva_asig">
		Nueva asignaci&oacute;n
	</a>
</div>
<form action='' method='post'>
	<div class='t_center' style='background-color:#eee;padding:10px 0;margin-bottom:5px;'>
		<p class='t_center' data-translate-html="gestioncurso.mod2">
			Seleccione un m&oacute;dulo para ver las autoevaluaciones que tiene asignada
		</p>

		<br/>

		<?php $mi_modulo->select_modulos($resultado); ?>
	</div>
</form>
<?php if(isset($idmodulo, $datoModulo)):?>
	
	<div class='subtitleModulo t_center'>
		<span data-translate-html="gestioncurso.mod3">
			Autoevaluaciones asignadas al m&oacute;dulo
		</span> 
		<?php echo $datoModulo['nombre']?>
	</div>

	<?php if(mysqli_num_rows($autoevalModulos) > 0):?>
		<?php while($autoeval = mysqli_fetch_assoc($autoevalModulos)):?>
			<div style='border:1px solid #ccc;margin-bottom:5px;padding:5px;'>
				<?php echo $autoeval['titulo_autoeval']?> - <?php echo $autoeval['f_creacion']?>
				<span class='fright'>
					<a href='#' <?php echo Alerta::alertConfirmOnClick('eliminarasignacion','¿Estas seguro que desea eliminar la asignación?', 'autoevaluaciones/eliminar-asignar/'.$autoeval['idtest'])?> data-translate-html="general.eliminar">
						eliminar
					</a>
				</span>
				<span class='fright' style='margin-right:10px;'>
					<a href='autoevaluaciones/editar-asignar/<?php echo $autoeval['idtest'].'/'.$post['idmodulo'];?>') title='editar asignacion' data-translate-html="general.editar">
						editar
					</a>
				</span>
			</div>
		<?php endwhile;?>
	<?php else:?>
		<p class='t_center borde' style='margin-bottom:5px;padding:5px;' data-translate-html="gestioncurso.no_eval2">
			No existen autoevaluaciones asignadas para este m&oacute;dulo
		</p>
	<?php endif; ?>
<?php endif; ?>
