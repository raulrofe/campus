<div>
	<?php while($autoevaluacionesCurso = $resulAutoeval->fetch_object()):?>
		<div class="listAutoeval burbuja">
			<span class="fleft">
				<?php echo $autoevaluacionesCurso->titulo_autoeval ?>
			</span>
			<span class="fright">
				<img src="imagenes/foro/editar.png" alt=""/>
				<a href='autoevaluaciones/editar/<?php echo $autoevaluacionesCurso->idautoeval;?>' data-translate-html="general.editar">
					Editar
				</a>
				<img src="imagenes/foro/borrar.png" alt=""/>
				<a href='#' <?php echo Alerta::alertConfirmOnClick('eliminarautoevaluacion','¿Realmente queires eliminar esta autoevaluación?', 'autoevaluaciones/eliminar/' . $autoevaluacionesCurso->idautoeval);?>' data-translate-html="general.borrar">
					Borrar
				</a>
			</span>
		</div>
		<div class="clear"></div>
		<br/>
	<?php endwhile; ?>
</div>