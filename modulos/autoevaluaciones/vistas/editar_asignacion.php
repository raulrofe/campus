<br/>

	<div class='subtitleModulo' data-translate-html="gestioncurso.act_asig">
		ACTUALIZAR DATOS DE ASIGNACI&Oacute;N
	</div>

	<form method='post' action='' name='autoevaluaciones'>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modulo">
				M&oacute;dulo
			</div>
			<div class='campofrmExtra'><?php $objModulo->select_modulos_noauto($resultado2); ?></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.max_int">
				N&uacute;mero m&aacute;ximo de intentos
			</div>
			<div class='campofrmExtra'><input type='text' name='n_intentos' value='<?php echo $rowAsignacion['n_intentos'];?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.penalizacion">
				Penalizaci&oacute;n por respuesta en blanco
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='<?php echo $rowAsignacion['penalizacion_blanco'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nc">
				Penalizaci&oacute;n por respuesta no contestada
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='<?php echo $rowAsignacion['penalizacion_nc'];?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nota_minima">
				Nota m&iacute;nima
			</div>
			<div class='campofrmExtra'><input type='text' name='nota_minima' value='<?php echo $rowAsignacion['nota_minima'];?>'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_es">
				Tiempo establecido
			</div>
			<div class='campofrmExtra'><?php echo $rowAsignacion['tiempo'];?></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_real">
				Tiempo realizaci&oacute;n
			</div>
			<div class='campofrmExtra'>

				<input type="checkbox" name="limitTime" value="0" />&nbsp;

				<span data-translate-html="gestioncurso.sin_limite">
					Sin l&iacute;mite de tiempo
				</span>

				<br/><br/>

				<span data-translate-html="autoevaluaciones.horas">
					Horas :
				</span> 

				<select name='horas'><?php Html::horas();?></select>

				<span data-translate-html="autoevaluaciones.minutos">
					Minutos :
				</span> 

				<select name='minutos'><?php Html::minutos();?></select>

			</div>
		</div>
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modo_correc">
				Seleccione el modo de correción
			</div>

			<div class='campofrmExtra'>
				<input type="radio" name="modoCorreccion" value="0" <?php if($rowAsignacion['correccion'] == 0) echo 'checked'; ?> />&nbsp; 
				<span data-translate-html="gestioncurso.mostrar_final">
					Mostrar s&oacute;lo el resultado final
				</span>
				
				<br/>
				
				<input type="radio" name="modoCorreccion" value="1" <?php if($rowAsignacion['correccion'] == 1) echo 'checked'; ?> />&nbsp; 
				<span data-translate-html="gestioncurso.mostrar_inc">
					Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="2" <?php if($rowAsignacion['correccion'] == 2) echo 'checked'; ?> />&nbsp; 
				<span data-translate-html="gestioncurso.modo_todo">
					Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
				</span>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.aleatorias">
				Preguntas aleatorias
			</div>

			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' <?php if($rowAsignacion['aleatorias'] == 1) echo 'checked'; ?>/> 
				<span data-translate-html="gestioncurso.si">
					Sí 
				</span>

				&nbsp;&nbsp;

				<input type='radio' name='aleatoria' value='0' <?php if($rowAsignacion['aleatorias'] == 0) echo 'checked'; ?>/>

				<span data-translate-html="gestioncurso.no">
					No
				</span>
			</div>
		</div>
		<input type='submit' value='Actualizar' data-translate-value="formulario.actualizar"/>
		<input type='hidden' name='boton' value='Actualizar' />
		<input type='hidden' name='idAsignacion' value='<?php echo $rowAsignacion['idtest'];?>' />
		<input type='hidden' name='tiempo' value='<?php echo $rowAsignacion['tiempo']; ?>' />
	</form>
