<br/>
<div class='t_center' style='background-color:#ccc;padding:5px 0;margin-bottom:5px;font-weight:bold;'>
	<a href='autoevaluaciones/asignar' data-translate-html="gestioncurso.nuevasig">
		Nueva asignaci&oacute;n
	</a>
</div>

<form action='autoevaluaciones/ver_asignaciones' method='post'>
	<div class='t_center' style='background-color:#eee;padding:10px 0;margin-bottom:5px;'>
		<p class='t_center' data-translate-html="gestioncurso.sel_modulo">
			Seleccione un m&oacute;dulo para ver las autoevaluaciones que tiene asignada
		</p>

		<br/>

		<?php $mi_modulo->select_modulos($resultado); ?>
	</div>
</form>

<div class='subtitleModulo' data-translate-html="gestioncurso.autoeval_modulo">
	Asignar Autoevaluaciones a M&oacute;dulo
</div>

<div class='cajafrm'>
<form method='post' action='autoevaluaciones/asignar' name='tematica'>
	<div class='filafrm t_center' style='margin:10px auto;width:700px;'>
		<div class='t_center'>
			<select name='idTematica' onchange = "this.form.submit()">
				
				<option value='0' data-translate-html="gestioncurso.comun">
					Tem&aacute;tica com&uacute;n
				</option>

				<?php 
					while($f = mysqli_fetch_assoc($tematicas)){
						if($idTematica == $f['idtematica_autoeval']) echo "<option value='".$f['idtematica_autoeval']."' selected>".Texto::textoPlano($f['tematica_autoeval'])."</option>";
						else echo "<option value='".$f['idtematica_autoeval']."'>".Texto::textoPlano($f['tematica_autoeval'])."</option>";
							
					}
				?>
			</select>
		</div>
	</div>
</form>
	<form method='post' action='autoevaluaciones/asignar' name='autoevaluaciones' id="frmAsignarAutoevaluacion">
		<div class='filafrm'>
			<div style='line-height:25px;'>
				<?php if(mysqli_num_rows($autoevaluaciones) > 0):?>
					<?php while($autoevaluacion = mysqli_fetch_assoc($autoevaluaciones)):?>
						<input type='checkbox' name='idAutoevaluacion[]' value='<?php echo $autoevaluacion['PTidautoeval']?>' class="ck"/> <?php echo Texto::textoPlano($autoevaluacion['PTtitulo_autoeval'])?><br/>
					<?php endwhile;?>
				<?php else:?>
					<p data-translate-html="gestioncurso.no_eval">
						No existen Autoevaluaciones relacionadas con esta tem&aacute;tica
					</p>
				<?php endif?>
			</div>
		</div>
		<div class='filafrm'>
			<!-- <div class='etiquetafrm'>M&oacute;dulo</div> -->
			<div class="t_center"><?php $mi_modulo->select_modulos_noauto($resultado2); ?></div>
		</div>
		<div class='filafrm'>

			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.max_int">
				N&uacute;mero m&aacute;ximo de intentos
			</div>

			<div class='campofrmExtra'><input type='text' name='n_intentos' value='2'/></div>
		</div>
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.penalizacion">
				Penalizaci&oacute;n respuesta en blanco
			</div>

			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='0.25' /></div>
		</div>
		<div class='filafrm'>

			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nc">
				Penalizaci&oacute;n respuesta no contestada
			</div>

			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='0.25' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nota_minima">
				Nota m&iacute;nima
			</div>
			<div class='campofrmExtra'>
				<input type='text' name='nota_minima' value='5' data-translate-value="gestioncurso.nota_minima"/>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_real">
				Tiempo realizaci&oacute;n
			</div>

			<div class='campofrmExtra' style='width:400px;'>

				<input type="checkbox" name="limitTime" value="0" />&nbsp;

				<span data-translate-html="gestioncurso.sin_limite">
					Sin l&iacute;mite de tiempo
				</span>

				<br/><br/>

				<span data-translate-html="autoevaluaciones.horas">
					Horas :
				</span> 

				<select name='horas'>
					<?php Html::horas();?>
				</select>

				<span data-translate-html="autoevaluaciones.minutos">
					Minutos :
				</span> 
				
				<select name='minutos'>
					<?php Html::minutos();?>
				</select>
			</div>
		</div>
		<div class='filafrm'>

			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modo_correc">
				Seleccione el modo de correción
			</div>

			<div class='campofrmExtra' style='width:400px;'>
				<input type="radio" name="modoCorreccion" value="0" />&nbsp; 
				<span data-translate-html="gestioncurso.autoevaluaciones">
					Mostrar s&oacute;lo el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="1" />&nbsp; 
				<spna data-translate-html="gestioncurso.mostrar_inc">
					Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final
				</spna>

				<br/>

				<input type="radio" name="modoCorreccion" value="2" checked/>&nbsp; 
				<span data-translate-html="gestioncurso.modo_todo">
					Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
				</span>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.autoevaluaciones">
				Preguntas aleatorias
			</div>

			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' /> 
				
				<span data-translate-html="gestioncurso.autoevaluaciones">
					Sí
				</span>

				<input type='radio' name='aleatoria' value='0' checked /> 

				<span data-translate-html="gestioncurso.autoevaluaciones">
					No
				</span>
			</div>
		</div>
		<input type='submit' value='Asignar' data-translate-value="gestioncurso.asignar" />
	</form>
</div>

<script src="js-autoevaluaciones-asignar_autoevaluacion.js"></script>
