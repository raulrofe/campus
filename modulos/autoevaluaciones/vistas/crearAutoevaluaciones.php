<div class='subtitleModulo' data-translate-html="gestioncurso.in_eval">
	Insertar Autoevaluaci&oacute;n
</div>

<div class='cajafrm'>
	<form method='post' action='' id="nuevaAutoevaluacion">
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.titulo_autoeval">
				T&iacute;tulo autoevaluaci&oacute;n
			</div>

			<div class='campofrmExtra'><input type='text' name='titulo' size='60' /></div>

			<div class='obligatorio'>(*)</div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modulo_cu">
				M&oacute;dulos curso
			</div>
			<div class='campofrmExtra'>
				<select name='idmodulo' class='select100'>
					<?php while($modulosCurso = $registros_modulos->fetch_object()):?>
						<?php echo "<option value='" . $modulosCurso->idmodulo . "'>" . $modulosCurso->nombre . "</option>" ?>
					<?php endwhile ?>
				</select>
			</div>
		</div>
		 
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tipo_auto">
				Tipo autoevaluaci&oacute;n
			</div>
			<div class='campofrmExtra'>
				<select name='idtipo' class='select100'>
					<?php 
						while($fila = mysqli_fetch_assoc($registros_tematica_autoevaluacion)){
							echo "<option value='".$fila['idtipo_autoeval']."'>".$fila['formato']."</option>";
						}
					?>
				</select>
			</div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.auto_des">
				Descripci&oacute;n
			</div>
			<div class='campofrmExtra'><textarea name='descripcion' class='estilotextarea2'></textarea></div>
		</div>

		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.max_int">
				N&uacute;mero m&aacute;ximo de intentos
			</div>
			<div class='campofrmExtra'><input type='text' name='n_intentos' value='2'/></div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.penalizacion">
				Penalizaci&oacute;n respuesta en blanco
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='0.25' /></div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nc">
				Penalizaci&oacute;n respuesta no contestada
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='0.25' /></div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nota_minima">
				Nota m&iacute;nima
			</div>
			<div class='campofrmExtra'><input type='text' name='nota_minima' value='5'/></div>
		</div>
		
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_real">
				Tiempo realizaci&oacute;n
			</div>

			<div class='campofrmExtra'>
				<input type="checkbox" name="limitTime" value="0" checked/>&nbsp;
				<sapn data-translate-html="gestioncurso.sin_limite">
					Sin l&iacute;mite de tiempo
				</sapn>

				<br/><br/>

				<span data-translate-html="autoevaluaciones.horas">
					Horas :
				</span> 

				<select name='horas'><?php Html::horas();?></select>

				<span data-translate-html="autoevaluaciones.minutos">
					Minutos :
				</span> 

				<select name='minutos'><?php Html::minutos();?></select>
			</div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modo_correc">
				Seleccione el modo de correción
			</div>

			<div class='campofrmExtra'>
				<input type="radio" name="modoCorreccion" value="0" />&nbsp; 
				<span data-translate-html="gestioncurso.mostrar_final">
					Mostrar s&oacute;lo el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="1" />&nbsp; 
				<span data-translate-html="gestioncurso.mostrar_inc">
					Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="2" checked />&nbsp; 

				<span data-translate-html="gestioncurso.modo_todo">
					Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
				</span>
			</div>
		</div>
		
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.aleatorias">
				Preguntas aleatorias
			</div>

			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' /> 
				
				<span data-translate-html="gestioncurso.si">
					Sí 
				</span>

				&nbsp;&nbsp;

				<input type='radio' name='aleatoria' value='0' checked /> 
				
				<span data-translate-html="gestioncurso.no">
					No
				</span>
			</div>
		</div>
	
		<div class='filafrm'>
			<div class='obligatorio'>
				<span data-translate-html="gestioncurso.campos_obli">
					Campos obligatorios
				</span> 
				(*)
			</div>
		</div>
		
		<input type='submit' value='Enviar' data-translate-value="formulario.enviar"/>
	</form>
</div>

<script src="js-autoevaluaciones-nuevo_autoevaluacion.js"></script>