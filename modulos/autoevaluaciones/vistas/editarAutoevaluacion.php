

<div class='cajafrm'>
	<form method='post' action='' >
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.titulo_autoeval">
				T&iacute;tulo autoevaluaci&oacute;n
			</div>
			<div class='campofrm'><input type='text' name='titulo' size='60' value='<?php echo Texto::textoPlano($fila['titulo_autoeval']); ?>' /></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.modulo">
				M&oacute;dulo
			</div>
			<div class='campofrm'>
				<?php $mi_modulo->select_modulos($registros_modulos);?>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.no">
				Tem&aacute;tica autoevaluaci&oacute;n
			</div>
			<div class='campofrm'>
				<select name='idtematica'>
				<?php 
					while($fila = mysqli_fetch_assoc($registros_tematica_autoevaluacion)){
						echo "<option value='".$fila['idtipo_autoeval']."'>".$fila['formato']."</option>";
					}
				?>
				</select>
			</div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="formulario.descripcion">
				Descripci&oacute;n
			</div>
			<div class='campofrm'><textarea name='descripcion' class='estilotextarea' ><?php echo Texto::textoFormateado($fila['descripcion']);?></textarea></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.tiempo_real">
				Tiempo realizaci&oacute;n
			</div>
			<div class='campofrm'>
				<span data-translate-html="autoevaciones.horas">
					Horas :
				</span> 

				<select name='horas'><?php $mi_autoevaluacion->horas_autoevaluacion($tiempo[0]);?></select>

				<span data-translate-html="autoevaciones.minutos">
					Minutos :
				</span> 

				<select name='minutos'><?php $mi_autoevaluacion->minutos_autoevaluacion($tiempo[1]);?></select>
			</div>
		</div>
		<input type='hidden' name='idautoevaluacion' value='<?php echo $idautoevaluacion;?>'/>
		<input type='hidden' name='v2' value='<?php echo $v2;?>'/>
		<input type='submit' value='enviar' data-translate-html="formulario.enviar" />
	</form>
</div>