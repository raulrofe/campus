<div class='subtitleModulo' data-translate-html="gestioncurso.insertar_pr">Insertar preguntas</div>
<div class='cajafrm'>

	<!-- FORMULARIO CON EL LISTADO DE AUTOEVALUACIONES -->
	<form method='post' action='autoevaluaciones/preguntas' name='select_autoevaluacion'>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.autoevaluacion">
				Autoevaluaci&oacute;n
			</div>

			<div class='campofrm'>
			<!-- <div class='campofrm'> -->
				<select name='idautoevaluacion' id='eval' onchange='this.form.submit()' class='select100'>
					<?php while($f = $autoevaluacionesPlantilla->fetch_object()): ?>
						<?php if($idautoevaluacion == $f->idautoeval): ?>
							<?php echo "<option value='" . $f->idautoeval . "' selected>" . $f->titulo_autoeval . "</option>" ?>
						<?php else: ?>
							<?php echo "<option value='" . $f->idautoeval . "'>" . $f->titulo_autoeval . "</option>" ?>
						<?php endif ?>
					<?php endwhile ?>
				</select>
			<!-- </div> -->
			</div>
		</div>	
	</form>	
	
	<!-- FORMULARIO CON PARA LA PREGUNTA -->
	<form method='post' action='autoevaluaciones/preguntas' enctype='multipart/form-data' name='pregunta_autoevaluacion' id="frmCrearPregunta">
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.enunciado">
				Enunciado pregunta
			</div>

			<div class='campofrm'><textarea name='enunciado' class='estilotextarea2'></textarea></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.nrespuestas">
				N&uacute;mero de respuestas
			</div>
			<div class='campofrm'><input type='text' name='n_respuestas' size='1' value='4'/></div>
		</div>
		<div class='filafrm'>
			<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
				Im&aacute;gen
			</div>
			<div class='campofrm'>
				<input type='file' name='archivo' size='72'/>
			</div>
		</div>
		<input type='hidden' name='idautoevaluacion' value='<?php  echo $idautoevaluacion; ?>' />
		<br>
		<input type='submit' value='Crear respuestas' data-translate-value="gestioncurso.crearrespuestas"/>
	</form>
	
</div>	
<div class='nota' style='width:705px;'>* 
	<span data-translate-html="gestioncurso.ntotal">
		N&uacute;mero total de preguntas creadas para esta evaluaci&oacute;n:
	</span> 
	<?php echo $numero_preguntas;?>
</div>

<div class='preguntaTest'>
	<?php 
	$cont=1;
	if($numero_preguntas > 0)
	{
		while($row = $preguntas->fetch_object())
		{
			echo $cont . ".- <a href='autoevaluaciones/preguntas/editar/" . $row->idpreguntas . "'>" . $row->pregunta . "</a>
			
			<span class='fright'><a href='#' " . Alerta::alertConfirmOnClick('eliminarpreguntasyrespuestas','¿Estas seguro que desea eliminar esta pregunta con todas sus respuestas?','autoevaluaciones/preguntas/eliminar/' . $row->idpreguntas  ) . ">eliminar</a></span>
			<span class='fright'><a href='autoevaluaciones/preguntas/editar/" . $row->idpreguntas . "' data-translate-html='general.editar'>editar </a>|</span>
			
			<br/><br/>";
			
			$respuestas = $mi_autoevaluacion->buscar_respuestas($row->idpreguntas);
			while($respuesta = $respuestas->fetch_object())
			{			
				if($respuesta->correcta == 1)
				{
					echo "<span class='verde'> - " . $respuesta->respuesta . "</span><br/>";
				}
				else
				{
					echo "<span> - " . $respuesta->respuesta . "</span><br/>";	
				}
			}
			$cont++;
			echo "<br/>";
		}
		
	} else {
		echo "<span data-translate-html='gestioncurso.no_preguntas'>No existen preguntas</span>";
	}
	?>
</div>

<script src="js-autoevaluaciones-crearpregunta.js"></script>