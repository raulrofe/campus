
<div class='subtitleModulo' data-translate-html="gestioncurso.editar_eval">
	Editar Autoevaluaci&oacute;n
</div>

<div class='cajafrm'>
	<form method='post' action='' id="nuevaAutoevaluacion">
	
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.titulo_autoeval">
				T&iacute;tulo autoevaluaci&oacute;n
			</div>

			<div class='campofrmExtra'><input type='text' name='titulo' size='60' value='<?php echo Texto::textoPlano($fila->titulo_autoeval); ?>' /></div>
		</div>
	
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modulo">
				Módulo
			</div>

			<div class='campofrmExtra'>
				<select name='idmodulo'>
					<?php while($modulosCurso = $registros_modulos->fetch_object()):?>
						<?php if($modulosCurso->idmodulo == $fila->idmodulo):?>
							<option value="<?php echo $modulosCurso->idmodulo ?>" selected > <?php echo $modulosCurso->nombre ?> </option>
						<?php else: ?>
							<option value="<?php echo $modulosCurso->idmodulo ?>" > <?php echo $modulosCurso->nombre ?> </option>
						<?php endif ?>
					<?php endwhile ?>
				</select>
			</div>
		</div>
		
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tipo_auto">
				Tipo autoevaluaci&oacute;n
			</div>

			<div class='campofrmExtra'>
				<select name='idtipo_autoeval'>
					<option value='1' <?php if($fila->idtipo_autoeval == 1){echo 'selected';}?> data-translate-html="gestioncurso.test">
						Test
					</option>
					<option value='2' <?php if($fila->idtipo_autoeval == 2){echo 'selected';}?> data-translate-html="gestioncurso.desarrollo">
						Desarrollo
					</option>
				</select>
			</div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.auto_des">
				Descripci&oacute;n
			</div>
			<div class='campofrmExtra'>
				<textarea name='descripcion' class='estilotextarea' >
					<?php echo Texto::textoPlano($fila->descripcion);?>
				</textarea>
			</div>
		</div>
	
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.max_int">
				N&uacute;mero m&aacute;ximo de intentos
			</div>

			<div class='campofrmExtra'>
				<input type='text' name='n_intentos' value='<?php echo Texto::textoPlano($fila->n_intentos);?>'/>
			</div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.penalizacion">
				Penalizaci&oacute;n respuesta en blanco
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_blanco' value='<?php echo Texto::textoPlano($fila->penalizacion_blanco);?>' /></div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nc">
				Penalizaci&oacute;n respuesta no contestada
			</div>
			<div class='campofrmExtra'><input type='text' name='penalizacion_nc' value='<?php echo Texto::textoPlano($fila->penalizacion_nc);?>' /></div>
		</div>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.nota_minima">
				Nota m&iacute;nima
			</div>
			<div class='campofrmExtra'><input type='text' name='nota_minima' value='<?php echo Texto::textoPlano($fila->nota_minima);?>'/></div>
		</div>
		
		<?php if($fila->tiempo != '00:00:00'):?>
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_es">
				Tiempo establecido
			</div>
			<div class='campofrmExtra'><?php echo $fila->tiempo;?></div>
		</div>
		<?php endif ?>
		
		<div class='filafrm'>
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.tiempo_real">
				Tiempo realizaci&oacute;n
			</div>

			<div class='campofrmExtra'>
				<input type="checkbox" name="limitTime" value="0" checked/>&nbsp;
				<span data-translate-html="gestioncurso.sin_limite">
					Sin l&iacute;mite de tiempo
				</span>

				<br/><br/>

				<span data-translate-html="autoevaciones.horas">
					Horas :
				</span> 

				<select name='horas'><?php Html::horas();?></select>

				<span data-translate-html="autoevaciones.minutos">
					Minutos :
				</span> 

				<select name='minutos'><?php Html::minutos();?></select>
			</div>
		</div>
		
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.modo_correc">
				Seleccione el modo de correción
			</div>

			<div class='campofrmExtra'>
				<input type="radio" name="modoCorreccion" value="0" <?php if($fila->correccion == 0) echo 'checked'; ?> />&nbsp; <span data-translate-html="gestioncurso.mostrar_final">
					Mostrar s&oacute;lo el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="1" <?php if($fila->correccion == 1) echo 'checked'; ?> />&nbsp; 
				<span data-translate-html="gestioncurso.mostrar_inc">
					Mostrar "CORRECTA" &oacute; "INCORRECTA" y el resultado final
				</span>

				<br/>

				<input type="radio" name="modoCorreccion" value="2" <?php if($fila->correccion == 2) echo 'checked'; ?> />&nbsp; 
				<span data-translate-html="gestioncurso.modo_todo">
					Mostrar "CORRECTA" &oacute; "INCORRECTA", resaltar el item correcto y el resultado final
				</span>
			</div>
		</div>
		
		<div class='filafrm'>
			
			<div class='etiquetafrmExtra' data-translate-html="gestioncurso.aleatorias">
				Preguntas aleatorias
			</div>

			<div class='campofrmExtra'>
				<input type='radio' name='aleatoria' value='1' <?php if($fila->aleatorias == 1) echo 'checked'; ?> /> 
				<span data-translate-html="gestioncurso.si">Sí</span> &nbsp;&nbsp;
				<input type='radio' name='aleatoria' value='0' <?php if($fila->aleatorias == 0) echo 'checked'; ?> /> <span data-translate-html="gestioncurso.no">No</span>
			</div>
		</div>
		
		<input type="hidden" name="idAsignacion" value="<?php echo $fila->idtest; ?>" />
		
		<input type='submit' value='enviar' data-translate-value="formulario.enviar"/>
	</form>
</div>

<script src="js-autoevaluaciones-nuevo_autoevaluacion.js"></script>