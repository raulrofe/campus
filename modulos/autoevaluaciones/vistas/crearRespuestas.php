<div class='subtitle t_center'>
	<span data-translate-html="gestioncurso.autoevaluaciones">
		Autoevaluaci&oacute;nes
	</span>
</div>

<div class='caja_separada'>
	<?php if ($_SESSION['perfil'] != 'alumno') {
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'menu_tutores');
	} ?>
	<div class='subtitleModulo' data-translate-html="gestioncurso.insertar_res">
		Insertar respuestas
	</div>

	<div class='cajafrm'>
		<p class='subtitleModulo'><?php echo Texto::textoPlano(stripslashes($enunciado)); ?></p>
		<p style='background-color:#ffcccc;border:1px solid #cccccc;margin:10px 0;padding:5px;'>* 
			<span data-translate-html="gestioncurso.no_olvidar">
				No olvides marcar la respuesta correcta
		</p>
		<form method='post' action='autoevaluaciones/preguntas' enctype='multipart/form-data' id="frmCrearRespuesta">
<?php for ($i = 1; $i <= $n_respuestas; $i++) { ?>
				<div class='filafrm'>

					<div class='etiquetafrm'>
						<span data-translate-html="gestioncurso.respuesta">
							Respuesta
						</span> 
	<?php echo $i; ?>
					</div>

					<div class='campofrm'>
						<input type='text' name='respuesta[]' style='width:83%;'/>
						<input type='radio' name='correcta[]' value='<?php echo $i; ?>' />
					</div>
				</div>

				<div class='filafrm'> 

					<div class='etiquetafrm' data-translate-html="gestioncurso.imagen">
						Imagen
					</div>

					<div class='campofrm'>
						<input type='file' name='imagen_respuesta_<?php echo $i; ?>' size='74'/>
					</div>
				</div>

				<br/>

<?php } ?>
			<input type='hidden' name='idautoevaluacion' value='<?php echo $idautoevaluacion; ?>' />
			<input type='hidden' name='enunciado' value='<?php echo $enunciado; ?>' />
			<input type='hidden' name='n_respuestas' value='<?php echo $n_respuestas; ?>' />
			<input type='submit' value='Finalizar' data-translate-value="gestioncurso.finalizar"/>
		</form>
	</div>
</div>

<!-- <script src="js-autoevaluaciones-crearrespuesta.js"></script> -->