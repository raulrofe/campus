<div id="autoevaluaciones">
	<div class="t_center">
		<img src="imagenes/mis_progresos/autoevaluaciones2.png" alt="" class="imageIntro"/>
		<span data-translate-html="gestioncurso.autoevaluaciones">
			AUTOEVALUACIONES
		</span>
	</div>
	<br/><br/>
	<!-- menu de iconos -->
	<div class="fleft panelTabMenu" id="menuGestionContenidos">
		<div id="menuTrabajosPracticos" class="redondearBorde" onclick="changeTab(1)">
			<img src="imagenes/trabajos_practicos/trabajos-practicos2.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="gestioncurso.trabajos">
				Trabajos Pr&aacute;cticos
			</p>
		</div>
		<div id="menuAutoevaluaciones" class="blanco redondearBorde" onclick="changeTab(2)">
			<img src="imagenes/mis_progresos/autoevaluaciones.png" alt="" />
			<p style='font-size:0.8em;text-align:center;padding-bottom:10px;' data-translate-html="gestioncurso.autoevaluaciones">
				Autoevaluaciones
			</p>
		</div>
	</div>
	<!-- Fin menu iconos -->
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoAutoevaluaciones">
		<?php 
			if(Usuario::compareProfile(array('tutor', 'coordinador')))
			{
				require_once mvc::obtenerRutaVista(dirname(__FILE__), 'menu_tutores');	
			}
		?>
		<div style='overflow:hidden;width:100%;'>
			<div>
				<?php 
				require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);
				?>
			</div>
		</div>
	</div>
</div>