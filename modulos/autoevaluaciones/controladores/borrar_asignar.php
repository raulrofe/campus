<?php 
$mi_autoevaluacion = new Autoevaluacion();
$get = Peticion::obtenerGet();
	if(isset($get['idAsignacion']) && is_numeric($get['idAsignacion']))
	{
		if($mi_autoevaluacion->eliminarAsignacionAutoevaluacion($get['idAsignacion']))
		{
			Alerta::guardarMensajeInfo('autoevaluacioneliminada','La autoevaluación ha sido eliminada del módulo');
			Url::redirect('autoevaluaciones/ver_asignaciones');
		}	
	}
