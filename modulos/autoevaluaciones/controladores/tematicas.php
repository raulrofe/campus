<?php
$mi_autoevaluacion = new Autoevaluacion();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

//print_r($post);

if(isset($get['idautoevaluacion'])) $idautoevaluacion = $get['idautoevaluacion'];
if(isset($post['idautoevaluacion'])) $idautoevaluacion = $post['idautoevaluacion'];

if(isset($post['tematica']))
{
	if($mi_autoevaluacion->insertarTematica($post['tematica']))
	{
		Alerta::guardarMensajeInfo('tematicainsertada','Nueva temática insertada correctamente');
	}
}

if(isset($post['atematica'], $post['idTematica']) && is_numeric($post['idTematica']))
{
	if($mi_autoevaluacion->actualizarTematica($post['atematica'], $post['idTematica']))
	{
		Alerta::guardarMensajeInfo('tematicaactualizada','Temática actualizada correctamente');
	}
}

if(isset($post['idTematica']) && is_numeric($post['idTematica']))
{
	$datosTematica = $mi_autoevaluacion->buscarTematicas($post['idTematica']);
	$rowTematica = mysqli_fetch_assoc($datosTematica);
}

$tematicas = $mi_autoevaluacion->tematicas();
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');