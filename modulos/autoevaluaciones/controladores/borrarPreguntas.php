<?php
$get = Peticion::obtenerGet();
$mi_autoevaluacion = new Autoevaluacion();

if(isset($get['idpregunta']) && is_numeric($get['idpregunta']))
{
	$pregunta = $mi_autoevaluacion->buscar_pregunta($get['idpregunta']);
	$lasRespuestas = $mi_autoevaluacion->buscar_respuestas($get['idpregunta']);
	if($pregunta->num_rows > 0)
	{
		$borrarPregunta = false;
		
		$f = $mi_autoevaluacion->idautoevaluacionPregunta($get['idpregunta']);
		$f = $f->fetch_object();
		$idautoevaluacion = $f->idautoeval;
		
		if($lasRespuestas->num_rows > 0)
		{
			while($idrespuesta = $lasRespuestas->fetch_object())
			{
				$mi_autoevaluacion->borrardoLogicoRespuesta($idrespuesta->idrespuesta);	
			}
			$compruebaRespuestas = $mi_autoevaluacion->buscar_respuestas($get['idpregunta']);
		}
		
		if(isset($compruebaRespuestas) && $compruebaRespuestas->num_rows == 0)
		{
			$borrarPregunta = true;
		}

		else
		{
			$borrarPregunta = true;
		}
		
		echo $borrarPregunta;
		
		if($borrarPregunta)
		{
			if($mi_autoevaluacion->borradoLogicoPregunta($get['idpregunta']))
			{
				Alerta::guardarMensajeInfo('preguntaborrada','Pregunta borrada');
			}	
			else
			{
				Alerta::guardarMensajeInfo('preguntaborradaerror','Error al borrar la pregunta');
			}	
		}
				
		Url::redirect('autoevaluaciones/preguntas/'.$idautoevaluacion);
	}
}