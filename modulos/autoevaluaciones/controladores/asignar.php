<?php
$mi_modulo = new Modulos();
$mi_autoevaluacion = new Autoevaluacion();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

//print_r($post);

if(isset($get['idAutoevaluacion'])) $idautoevaluacion = $get['idAutoevaluacion'];
if(isset($post['idAutoevaluacion'])) $idautoevaluacion = $post['idAutoevaluacion'];
if(isset($post['idTematica'])) $idTematica = $post['idTematica'];
else{$idTematica = 0;}
if (isset($post['aleatoria'])) $aleatorias = $post['aleatoria'];
else $aleatorias = 0;

	if(isset($post['idAutoevaluacion']))
	{
		$cont = count($post['idAutoevaluacion'])-1;
		for($i=0;$i<=$cont;$i++)
		{
			$mi_autoevaluacion->set_autoeval($post['idAutoevaluacion'][$i]);
			$eval = $mi_autoevaluacion->buscar_autoevalPlantilla();
			$datos = mysqli_fetch_assoc($eval);
			
			// Comprobamos si existe un modulo para la asignacion de la autoevaluacion
			if(isset($post['idmodulo']) && is_numeric($post['idmodulo']) && $post['idmodulo'] != 0)
			{
				//Comprobacion del tiempo para el test
				if($post['horas'] == '00' && $post['minutos'] == '00' && !isset($post['limitTime']))
				{
					Alerta::mostrarMensajeInfo('asignartiempo','Debes seleccionar el tiempo de realizaci&oacute;n');
				}
				else
				{
					//Establecesmos el limite de tiempo
					if(isset($limitTime) && $post['limitTime'] == 0)
					{
						$post['horas'] = '00';
						$post['minutos'] = '00';
					}
					
					$tiempoRealizacion = $post['horas'].":".$post['minutos'];
				
					//copiamos la autoevaluacion de la platilla a la tabla de autoevaluaciones de preguntas y plantillas del curso
					if($mi_autoevaluacion->insertar_autoevaluacion($datos['PTtitulo_autoeval'], $datos['PTdescripcion'], 
					$datos['PTf_creacion'], $datos['PTidtipo_autoeval']))
					{
						$idasignacion = $mi_autoevaluacion->obtenerUltimoIdInsertado();
						$preguntas = $mi_autoevaluacion->preguntasPlantilla();
						while($pregunta = mysqli_fetch_assoc($preguntas))
						{
							if($mi_autoevaluacion->copiarPregunta($pregunta['PTpregunta'], $pregunta['PTimagen_pregunta'], $idasignacion))
							{
								$idUltimaPregunta = $mi_autoevaluacion->obtenerUltimoIdInsertado();
								$respuestas = $mi_autoevaluacion->buscar_respuestasPlantilla($pregunta['PTidpreguntas']);
								while($respuesta = mysqli_fetch_assoc($respuestas))
								{
									$mi_autoevaluacion->copiarRespuesta($respuesta['PTrespuesta'], $respuesta['PTimagen_respuesta'], $respuesta['PTcorrecta'], $idUltimaPregunta);
				
								}
							}
						}
	
						if($mi_autoevaluacion->configuracionTest($post['penalizacion_blanco'], $post['penalizacion_nc'], $post['n_intentos'], $post['nota_minima'], $post['aleatoria'], $tiempoRealizacion, $post['modoCorreccion'], $post['idmodulo'], $idasignacion))
						{
							Alerta::guardarMensajeInfo('asignacioncompletada','Asignación completada');
						}
					}
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('moduloobligatorio','El Módulo es obligatorio');
			}
		}
	}

$resultado = $mi_modulo->ver_modulos();
$resultado2 = $mi_modulo->ver_modulos();
$autoevaluaciones = $mi_autoevaluacion->autoevalActivasTematicaPlantilla($idTematica);
$tematicas = $mi_autoevaluacion->tematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');