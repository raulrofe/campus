<?php

$mi_modulo = new Modulos();
$mi_autoevaluacion = new Autoevaluacion();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

if(isset($post['idmodulo'])) $idmodulo = $post['idmodulo'];

if(isset($idmodulo) && is_numeric($idmodulo))
{
	$mi_modulo->set_modulo($idmodulo);
	$autoevalModulos = $mi_modulo->autoevaluacionesModulo($idmodulo);
	$elModulo = $mi_modulo->buscar_modulo($idmodulo);
	$datoModulo = mysqli_fetch_assoc($elModulo);
}

$resultado = $mi_modulo->ver_modulos();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
