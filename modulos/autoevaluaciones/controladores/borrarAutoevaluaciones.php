<?php 

$get = Peticion::obtenerGet();

$mi_autoevaluacion = new Autoevaluacion();

if(isset($get['idautoevaluacion']) && is_numeric($get['idautoevaluacion']))
{
	$mi_autoevaluacion->set_autoeval($get['idautoevaluacion']);
	$autoevaluacion = $mi_autoevaluacion->buscar_autoeval();
		
	if(mysqli_num_rows($autoevaluacion)>0)
	{
		if($_SESSION['perfil'] != 'alumno')
		{
			$mi_autoevaluacion->eliminar_autoeval();
			Alerta::guardarMensajeInfo('autoevaluacionborrada','Autoevaluación borrada');
			Url::redirect('autoevaluaciones');
		}
	}
}
