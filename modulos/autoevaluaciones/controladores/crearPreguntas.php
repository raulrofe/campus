<?php

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

$mi_autoevaluacion = new Autoevaluacion();
//$registros_autoevaluacion = $mi_autoevaluacion->autoeval();

$autoevaluacionesPlantilla = $mi_autoevaluacion->autoevalCursoOptimize(Usuario::getIdCurso());

//Extraccion de variables manualmente
if(isset ($get['idautoevaluacion']) and is_numeric($get['idautoevaluacion'])) 
{
	$idautoevaluacion = $get['idautoevaluacion'];
}
if(isset ($post['idautoevaluacion']) and is_numeric($post['idautoevaluacion'])) 
{
	$idautoevaluacion = $post['idautoevaluacion'];
}
if(isset ($post['enunciado'])) 
{
		$enunciado = $post['enunciado'];
}
if(isset ($post['correcta']) and is_numeric($post['correcta'])) 
{
	$correcta = $post['correcta'];
}
if(isset ($post['n_respuestas']))
{
	$n_respuestas = $post['n_respuestas'];	
} 

if(!isset ($idautoevaluacion))
{
	$fresultado = $mi_autoevaluacion->autoevalCursoOptimize(Usuario::getIdCurso());
	$fila = $fresultado->fetch_object();
	$idautoevaluacion = $fila->idautoeval;
}

//Insertamos las respuestas
if(!empty ($post['respuesta']))
{
	$respuesta = $post['respuesta'];
	if(isset($post['correcta']))
	{
		for($i=0;$i<=count($respuesta)-1;$i++){$respuesta[$i] = trim($respuesta[$i]);}
		
		$max = $mi_autoevaluacion->buscar_ultima_pregunta();
		$mi_autoevaluacion->insertar_respuesta($respuesta,$post['correcta'],$max);
		Alerta::guardarMensajeInfo('preguntacreada','Ha creado una nueva pregunta con respuestas');
		Url::redirect('autoevaluaciones/preguntas/'.$idautoevaluacion);
	}
	else
	{
		Alerta::mostrarMensajeInfo('marcarrespuesta','Debes marcar una respuesta como correcta');
	}
}

if((!empty ($post['respuesta']) && !isset ($post['correcta'])) || !isset($post['respuesta']))
{	
	// Contamos el numero de preguntas que contiene esta autoevaluacion
	if(isset($idautoevaluacion))
	{
			$mi_autoevaluacion->set_autoeval($idautoevaluacion);
			$preguntas = $mi_autoevaluacion->preguntas();
			$numero_preguntas = $preguntas->num_rows;	}

	// Si no esta vacio el enunciado de la pregunta la insertamos en caso contrario mostramos la vista autoevaluaciones
	if(!empty($enunciado) && !isset($correcta) && !isset($post['respuesta']))
	{
		if(is_numeric($n_respuestas) && $n_respuestas > 1)
		{
			$mi_autoevaluacion->insertar_pregunta($enunciado);
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'crearRespuestas');
		}
		else
		{
			Alerta::mostrarMensajeInfo('respuestasmayoruno','El numero de respuestas debe ser mayor a 1');
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
		}
	}
	
	else require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
}