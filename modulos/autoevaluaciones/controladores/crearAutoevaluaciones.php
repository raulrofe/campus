<?php
$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

if(isset($get['idautoevaluacion'])) $idautoevaluacion = $get['idautoevaluacion'];
if(isset($post['idautoevaluacion'])) $idautoevaluacion = $post['idautoevaluacion'];

$mi_autoevaluacion = new Autoevaluacion();
$mi_modulo = new Modulos();

if(Peticion::isPost())
{
	if(!empty($post['titulo']))
	{
		$fecha_creacion = Fecha::fechaActual(false);
		if($mi_autoevaluacion->insertar_autoevaluacion($post['titulo'], $post['descripcion'], $fecha_creacion, $post['idtipo']))
		{
			$ultimo_id = $mi_autoevaluacion->obtenerUltimoIdInsertado();
			
			//Asignamos automaticamente la autoevaluacion
			if($mi_autoevaluacion->configuracionTest($post['penalizacion_blanco'], $post['penalizacion_nc'], 
			$post['n_intentos'], $post['nota_minima'], $post['aleatoria'], $post['idmodulo'], $ultimo_id, $post['modoCorreccion']))
			{
				Alerta::guardarMensajeInfo('autoevaluacioncreada','La autoevaluación ha sido creada correctamente para este curso');
				//Url::redirect('autoevaluaciones/preguntas/'.$ultimo_id);
				Url::redirect('autoevaluaciones');	
			}
		}			
	}
}


//$registros_modulos = $mi_modulo->ver_modulos();
$registros_modulos = $mi_modulo->modulos_asignado_curso();


$registros_tematica_autoevaluacion = $mi_autoevaluacion->tematica_autoevaluacion();
$tematicas = $mi_autoevaluacion->tematicas();

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
