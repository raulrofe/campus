<?php

$mi_autoevaluacion = new Autoevaluacion();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

if(isset($post['idPregunta']) && is_numeric($post['idPregunta']))
{
	$mi_autoevaluacion->insertar_respuesta($post['respuesta'],$post['correcta'],$post['idPregunta']);
}

if(isset ($post['idrespuesta']))
{
		$respuesta = $post['respuesta'];
		$correcta = $post['correcta'];
		$mi_autoevaluacion->actualizar_respuestas($respuesta,$correcta,$post['idrespuesta']);
}

// elimina una imagen de un respuesta de la autoevaluacion
else if(isset($get['idrespuesta'], $get['option']) && $get['option'] == 'eliminar_imagen' && is_numeric($get['idrespuesta']))
{
	$rowRespuesta = $mi_autoevaluacion->buscar_una_respuesta($get['idrespuesta']);
	if($rowRespuesta->num_rows == 1)
	{
		$rowRespuesta = $rowRespuesta->fetch_object();
		
		$filename = PATH_ROOT . $rowRespuesta->imagen_respuesta;
		if(!empty($rowRespuesta->imagen_respuesta) && file_exists($filename))
		{
			if($mi_autoevaluacion->eliminarImagenRespuesta($rowRespuesta->idrespuesta))
			{
				chmod($filename, 0777);
				unlink($filename);
			}
		}
	}
	
	Url::redirect('autoevaluaciones', true);
}

if(!empty ($post['enunciado']))
{
	$enunciado = $post['enunciado'];
	if($mi_autoevaluacion->actualizar_pregunta($enunciado,$get['idpregunta']))
	{
		Alerta::mostrarMensajeInfo('preguntamodificada','Pregunta modificada');
	}
}
	
$registros_preguntas = $mi_autoevaluacion->buscar_pregunta($get['idpregunta']);
$pre = $registros_preguntas->fetch_object();
$registros_respuestas = $mi_autoevaluacion->buscar_respuestas($get['idpregunta']);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');
