<?php
$mi_autoevaluacion = new Autoevaluacion();
$mi_modulo = new Modulos();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

if(isset($get['idautoevaluacion']) && is_numeric($get['idautoevaluacion']))
{
	// Establecemos la autoevaluacion	
	$mi_autoevaluacion->set_autoeval($get['idautoevaluacion']);
	
	// Cogemos los parametros y actualizamos la autoevaluacion
	if(Peticion::isPost())
	{
		if(!empty ($post['titulo']))
		{
			if($mi_autoevaluacion->actualizar_autoeval($post['titulo'], $post['idtipo_autoeval'], $post['descripcion']))
			{
				if(isset($post['limitTime']))
				{
					$tiempo = '00:00';
				}
				else
				{
					if($post['horas'] == '00' && $post['minutos'] == '00')
					{
						$tiempo = $post['tiempo'];
					}
					else
					{
						$tiempo = $post['horas'].':'.$post['minutos'];
					}
				}
				
				if($mi_autoevaluacion->actualizarAsignacion($post['idmodulo'], $post['n_intentos'], 
				$post['penalizacion_blanco'], $post['penalizacion_nc'], $post['nota_minima'], $tiempo, 
				$post['modoCorreccion'], $post['aleatoria'], $post['idAsignacion']))
				{
					Alerta::guardarMensajeInfo('autoevaluacioneditada','Autoevaluación editada correctamente');
					Url::redirect('autoevaluaciones');
				}
			}
		}
	}
	
	// BUSAMOS LA AUTOEVALUACION SEGUN ID
	$autoevaluacion = $mi_autoevaluacion->buscar_autoeval();
	if($autoevaluacion->num_rows > 0)
	{
		$fila = $autoevaluacion->fetch_object();
	}
	
	// BUSCAMOS TODOS LOS MODULOS DEL CURSO
	$registros_modulos = $mi_modulo->modulos_asignado_curso();
	
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');

