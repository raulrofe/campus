<?php
$objModulo = new Modulos();
$mi_autoevaluacion = new Autoevaluacion();

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

if(isset($post['idTematica']))
{
	$idTematica = $post['idTematica'];
}
else if(isset($get['idTematica']))
{
	$idTematica = $get['idTematica'];
}
else
{
	$idTematica = 0;
}


	if(isset($post['idAsignacion']) && is_numeric($post['idAsignacion']))
	{
		if(isset($post['limitTime']))
		{
			$tiempo = '00:00';
		}
		else
		{
			if($post['horas'] == '00' && $post['minutos'] == '00')
			{
				$tiempo = $post['tiempo'];
			}
			else
			{
				$tiempo = $post['horas'].':'.$post['minutos'];
			}
		}
		if($mi_autoevaluacion->actualizarAsignacion($post['idmodulo'], $post['n_intentos'], 
			$post['penalizacion_blanco'], $post['penalizacion_nc'], $post['nota_minima'], $tiempo, 
			$post['modoCorreccion'], $post['aleatoria'], $post['idAsignacion']))
		{
			Alerta::guardarMensajeInfo('asignacionactualizada','Asignación actualizada correctamente');
		}
	}
	
$resultado = $objModulo->ver_modulos();
$resultado2 = $objModulo->ver_modulos();
$autoevaluaciones = $mi_autoevaluacion->autoevalActivasTematicaPlantilla($idTematica);
$tematicas = $mi_autoevaluacion->tematicas();

if(isset($get['idAsignacion']) && is_numeric($get['idAsignacion']))
{
	$objModulo->set_modulo($get['idModulo']);
	$datosAsignacion = $mi_autoevaluacion->obtenerAsignacion($get['idAsignacion']);
	$rowAsignacion = mysqli_fetch_assoc($datosAsignacion);
}
//$modulos =$objModulo->ver_modulos();

require_once(mvc::obtenerRutaVista(dirname(__FILE__), 'editar_asignacion'));