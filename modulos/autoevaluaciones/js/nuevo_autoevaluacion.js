$(document).ready(function()
{
	$("#nuevaAutoevaluacion").validate({
			errorElement: "div",
			messages: {
				titulo: {
					required: 'Introduce un t&iacute;tulo'
				},
				idTematica: {
					required: 'Seleccione una tem&aacute;tica',
					digits: 'El valor de tem&aacute;tica debe ser num&eacute;rico'
				}
			},
			rules: {
				titulo : {
					required  : true
				},
				idTematica : {
					required  : true,
					digits    :true
				}
			}
		});
});