$(document).ready(function()
		{
			$("#frmCrearPregunta").validate({
					errorElement: "div",
					messages: {
						enunciado: {
							required: 'Introduce un enunciado'
						},
						n_respuestas: {
							required: 'Debes introducir un n&uacute;mero de respuestas mayor a 1',
							digits: 'Debes introducir un n&uacute;mero de respuestas mayor a 1',
							min: 'Debes introducir un n&uacute;mero de respuestas mayor a 1'
						}
					},
					rules: {
						enunciado : {
							required  : true
						},
						n_respuestas: {
							required: true,
							digits: true,
							min: 2
						}
					}
				});
		});