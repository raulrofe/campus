$(document).ready(function()
{
	$("#frmCrearRespuesta").validate({
			errorElement: "div",
			messages: {
				respuesta: {
					required: 'Introduce una respuesta'
				},
				correcta: {
					required: 'Selecciona una respuesta como correcta'
				}
			},
			rules: {
				respuesta : {
					required  : true
				},
				correcta: {
					required  : true
				}
			}
		});
	
	$('#frmCrearRespuesta').submit(function(e)
	{
		// comrpueba si al menos un checkbox esta marcado
		if($(this).find('input.respuestaAutoevalRadioCorrecta:checked').size() == 0)
		{
			$(this).prepend('<div id="frmCrearRespuestaErrorNoChecked" class="error">Debes marcar una respuesta como correcta</div>');
			
			e.preventDefault();
		}
		else
		{
			$(this).find('#frmCrearRespuestaErrorNoChecked').remove();
			
			numRespuestas = 0;
			respuestaMarcadaIsCheck = false;
			$(this).find('.filafrm').each(function(index)
			{
				if($.trim($(this).find('input[type="text"]').val()) != '')
				{
					numRespuestas++;
				}
				
				if($.trim($(this).find('input[type="text"]').val()) != '' && $(this).find('input.respuestaAutoevalRadioCorrecta:checked').size() == 1)
				{
					respuestaMarcadaIsCheck = true;
				}
			});
			if(numRespuestas >= 2 && respuestaMarcadaIsCheck)
			{
				$(this).find('#frmCrearRespuestaErrorNoChecked2').remove();
			}
			else
			{
				$(this).prepend('<div id="frmCrearRespuestaErrorNoChecked2" class="error">' +
				'- Debes rellenar al menos dos respuestas <br/> - La respuesta marcada como correcta no puede estar vac&iacute;a</div>');

				e.preventDefault();
			}
		}
	});
});