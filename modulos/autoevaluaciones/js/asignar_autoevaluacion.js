$(document).ready(function()
{
	$("#frmAsignarAutoevaluacion").validate({
			errorElement: "div",
			messages: {
				n_intentos: {
					required	: 'Introduce un n&uacute;mero de intentos mayor a 0',
					digits		: 'Introduce un n&uacute;mero de intentos mayor a 0',
					min			: 'Introduce un n&uacute;mero de intentos mayor a 0'
				},
				penalizacion_blanco: {
					required	: 'Introduce un valor de penalizaci&oacute;n por respuesta en blanco'
				},
				penalizacion_nc: {
					required	: 'Introduce un valor de penalizaci&oacute;n por respuesta no contestada'
				},
				nota_minima: {
					required	: 'Introduce un valor para la nota m&iacute;nima',
					number	: 'Introduce un valor para la nota m&iacute;nima',
					min			: 'Introduce un valor para la nota m&iacute;nima'
				},
				modoCorreccion: {
					required	: 'Seleccione un modo de correcci&oacute;n',
					digits		: 'Seleccione un modo de correcci&oacute;n'
				},
				aleatoria : {
					required	: 'Seleeccione si desea que se generen las preguntas de manera aleatoria',
					digits		: 'Seleeccione si desea que se generen las preguntas de manera aleatoria'
				}
			},
			rules: {
				n_intentos : {
					required	: true,
					digits		: true,
					min			: 1
				},
				penalizacion_blanco : {
					required	: true,
					number		: true,
					min			: 1
				},
				penalizacion_nc : {
					required	: true
				},
				nota_minima : {
					required	: true
				},
				modoCorreccion : {
					required	: true,
					digits		: true
				},
				aleatoria : {
					required	: true,
					digits		: true
				}
			}
		});
});