<?php
class Autoevaluacion extends modeloExtend{

	private $idautoevaluacion;
	private $max;
	private $maxr;

	//constructor
	public function __construct(){parent::__construct();}

/******************************************************************************************
AUTOEVALUACIONES
******************************************************************************************/

	public function obtenerNumAutoevalsPorAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT ae.titulo_autoeval, c.fecha, c.nota FROM autoeval AS ae' .
		' LEFT JOIN matricula AS m ON m.idalumnos = ' . $idAlumno . ' AND m.idcurso = ' . $idCurso .
		' LEFT JOIN calificaciones AS c ON c.idmatricula = m.idmatricula' .
		' WHERE ae.idautoeval = c.idautoeval GROUP BY c.idcalificaciones';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Establecemos el id de la autoevaluaci_n
	public function set_autoeval($idautoevaluacion){$this->idautoevaluacion = $idautoevaluacion;}

	// buscamos todas las autoevaluaci_n
	public function autoeval()
	{
		$sql = "SELECT * from autoeval ORDER BY titulo_autoeval ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaci_n activas
	public function autoevalActivas()
	{
		$sql = "SELECT * from autoeval where borrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos autoevaluacion segun id pasado como parametro
	public function buscar_autoeval()
	{
		$sql = "SELECT * from autoeval a, test t" .
		" WHERE a.idautoeval = " . $this->idautoevaluacion .
		" AND a.idautoeval = t.idautoeval";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// buscamos las autoevaluaciones de un curso
	public function autoeval_curso()
	{
		$idcurso = Usuario::getIdCurso();
		$sql = "SELECT M.nombre, TE.idautoeval, A.titulo_autoeval, A.descripcion, TE.penalizacion_blanco, TE.penalizacion_nc, TE.n_intentos, TE.nota_minima, TE.tiempo, TE.correccion, TE.aleatorias, TE.idmodulo" .
		" FROM curso C, accion_formativa AF, temario T, modulo M, autoeval A, test TE" .
		" WHERE TE.idcurso = " . $idcurso .
		" AND TE.idautoeval = A.idautoeval" .
		" AND A.borrado = 0" .
		" GROUP BY TE.idtest";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos las autoevaluaciones de un curso
	public function autoevalCursoOptimize($idCurso)
	{
		$idcurso = Usuario::getIdCurso();
		$sql = "SELECT TE.idautoeval, A.titulo_autoeval, A.descripcion, TE.penalizacion_blanco, TE.penalizacion_nc, TE.n_intentos, TE.nota_minima, TE.tiempo, TE.correccion, TE.aleatorias, TE.idmodulo" .
		" FROM autoeval A, test TE" .
		" WHERE TE.idcurso = " . $idcurso .
		" AND TE.idautoeval = A.idautoeval" .
		" AND A.borrado = 0" .
		" GROUP BY TE.idtest";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos las autoevaluaciones de un modulo
	public function autoeval_modulo($idmodulo)
	{
		$idcurso = Usuario::getIdCurso();
		$sql = "SELECT M.nombre, TE.idautoeval, A.titulo_autoeval, A.descripcion, TE.penalizacion_blanco,
		TE.penalizacion_nc, TE.n_intentos, TE.nota_minima, TE.tiempo, TE.correccion, TE.aleatorias, TE.idmodulo" .
		" FROM curso C, accion_formativa AF, temario T, modulo M, autoeval A, test TE" .
		" WHERE C.idcurso = " . $idcurso .
		" AND  TE.idcurso = C.idcurso" .
		//" and C.idaccion_formativa = AF.idaccion_formativa" .
		//" and AF.idaccion_formativa = T.idaccion_formativa" .
		" and T.idcurso = C.idcurso" .
		//" and M.idmodulo = TE.idmodulo" .
		" and TE.idautoeval = A.idautoeval" .
		" and A.borrado = 0" .
		" GROUP BY TE.idtest";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// comprobamos si la autoevaluacion pertene al curso
	public function compruebaAutoeval($idautoevaluacion)
	{
		$sql = "SELECT *
		from curso C, accion_formativa AF, temario T, modulo M, autoeval A, test TE
		where C.idcurso = ".$_SESSION['idcurso']."
		and C.idaccion_formativa = AF.idaccion_formativa
		and AF.idaccion_formativa = T.idaccion_formativa
		and T.idmodulo = TE.idmodulo
		and TE.idautoeval = A.idautoeval
		and A.idautoeval = ".$idautoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) > 0)
		{
			return true;
		}
	}

	// insertamos un formato para la autoevaluacion
	public function insertar_autoevaluacion($titulo, $descripcion, $fecha_creacion, $idtipo){
		$fecha_creacion = Fecha::fechaActual(false);
		$sql = "INSERT into autoeval" .
		" (titulo_autoeval, descripcion, f_creacion, idtipo_autoeval)" .
		" VALUES ('$titulo', '$descripcion', '$fecha_creacion', '$idtipo')";
		$this->consultaSql($sql);
		return $this->obtenerUltimoIdInsertado();
	}

	// eliminamos un tipo de formato para la autoevaluacion
	public function eliminar_autoeval()
	{
		$sql = "UPDATE autoeval SET borrado = 1 where idautoeval = ".$this->idautoevaluacion;
		//echo $sql;
		$this->consultaSql($sql);
	}

	// actualizamos la autoevaluacion
	public function actualizar_autoeval($titulo, $idtipo, $descripcion)
	{
		$sql = "UPDATE autoeval" .
		" SET titulo_autoeval = '$titulo'," .
		" idtipo_autoeval = '$idtipo'," .
		" descripcion = '$descripcion'" .
		" WHERE idautoeval = " . $this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos todas las autoevaluaciones que pertenecen a un curso
	public function autoevaluaciones_curso($idcurso = false)
	{
		if(!$idcurso)
		{
			$idcurso = Usuario::getIdCurso();
		}

		$sql = "SELECT * from curso C, modulo M, autoeval A, temario T
		where C.idcurso = ".$idcurso."
		and C.idcurso = T.idcurso
		and T.idmodulo = M.idmodulo
		and M.idmodulo = A.idmodulo";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Corregimos la autoevaluacion y devolvemos el resultado
	public function corregir_test()
	{
		$post = Peticion::obtenerPost();

		//print_r($post);

		$sql = "SELECT * from preguntas" .
		" WHERE idautoeval = " . $this->idautoevaluacion . " AND borrado = 0";
		$resultado = $this->consultaSql($sql);
		$numero_preguntas = mysqli_num_rows($resultado);

		$matriz = 1;
		$acertada = 0;
		$respuestasBlanca = 0;
		$fallada = 0;

		$correccion = array();

		for($i=0;$i<=$numero_preguntas-1;$i++)
		{
			//Comprueba si existe una eleccion del alumno al enviar el test
			if(isset($post[$matriz.'eleccion']))
			{
				$eleccion = explode("_",$post[$matriz.'eleccion']);

				// Compruebo que existe una respuesta , la respuesta es $eleccion[1]
				if (!empty ($eleccion[1]))
				{
					$sql = "SELECT * from respuesta where idpreguntas = ".$eleccion[0]." and idrespuesta = ".$eleccion[1];
					//echo $sql;
					$resultado = $this->consultaSql($sql);
					$f = mysqli_fetch_assoc($resultado);

					if($f['correcta'] == '1')
					{
						$acertada++;
						$correccion['errores'][]=$eleccion[0];
					}
					else
					{
						$fallada++;
					}
				}
				$matriz++;
			}
			else
			{
				$respuestasBlanca++;
				$matriz++;
			}
		}


		$valor_preguntas = 10/$numero_preguntas;
		$acertadas = $valor_preguntas * $acertada;

		//Obtengo el valor de las respuestas contestadas mal
		$sql = "SELECT penalizacion_nc, penalizacion_blanco, nota_minima from test where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);

		$falladas = $f['penalizacion_nc'] * $fallada;
		$repuestasBlancas = $f['penalizacion_blanco'] * $respuestasBlanca;
		$valorErrores = $repuestasBlancas + $falladas;

		// Para cuando existe restricciones cuando fallas y o lo dejas en blanco
		// $nota = $acertadas - $valorErrores;
		$nota = $acertadas;

		if($nota<0)
		{
			$nota = 0;
		}

		$nota = number_format($nota, 2);
		$correccion['nota'] = $nota;
		$correccion['nota_minima'] = $f['nota_minima'];
		return $correccion;
	}

/******************************************************************************************
PREGUNTA Y RESPUESTAS
******************************************************************************************/

	// insertamos un pregunta
	public function insertar_pregunta($enunciado)
	{
		$sql = "SELECT * from preguntas where pregunta = '".$enunciado."' and idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		if(mysqli_num_rows($resultado) == 0)
		{
			$enunciado = $enunciado;
			$sql = "INSERT into preguntas (pregunta,idautoeval) VALUES ('$enunciado','$this->idautoevaluacion')";
			//echo $sql;
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
			{
				$tipoArchivo = $_FILES['imagen_pregunta']['type'];
				if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
				Autoevaluaciones::guardarFotoCurso($_FILES['imagen_pregunta'], $id, 'pregunta');
			}
			$this->actualizar_pregunta($enunciado,$id);
		}
		else
		{
			Alerta::mostrarMensajeInfo('preguntayacreada','La pregunta ya fue creada');
		}
	}

	// borramos una pregunta
	public function borrar_pregunta($idpregunta){
		$sql = "SELECT * from preguntas P, respuesta R
		where P.idpreguntas = ".$idpregunta."
		and P.idpreguntas = R.idpreguntas";

		$resultado = $this->consultaSql($sql);
		while ($f = $resultado->fetch_object())
		{
			if(!empty ($f->imagen_pregunta) and file_exists($f->imagen_pregunta))
			{
				unlink($f->imagen_pregunta);
			}
			if(!empty ($f->imagen_respuesta) and file_exists($f->imagen_respuesta))
			{
				unlink($f->imagen_respuesta);
			}

		}

		$sql = "DELETE from preguntas where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//borramos las respuestas logicamente
	public function borrardoLogicoRespuesta($idrespuesta)
	{
		$sql = "UPDATE respuesta" .
		" SET borrado = 1" .
		" WHERE idrespuesta = " . $idrespuesta;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Borramos la pregunta logicamente
	public function borradoLogicoPregunta($idpregunta)
	{
		$sql = "UPDATE preguntas" .
		" SET borrado = 1" .
		" WHERE idpreguntas = " . $idpregunta;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// obtenemos todas las preguntas de una autoevaluacion
	public function preguntas(){
		$sql = "SELECT * from preguntas" .
		" WHERE borrado = 0" .
		" AND idautoeval = " . $this->idautoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la ultima pregunta insertada en la base de datos
	public function buscar_ultima_pregunta(){
		$sql = "SELECT MAX(idpreguntas) from preguntas";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->max = $f['MAX(idpreguntas)'];
		return $this->max;
	}

	// buscamos una pregunta segun su id
	public function buscar_pregunta($idpregunta){
		$sql = "SELECT * from preguntas where idpreguntas = ".$idpregunta;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta
	public function buscarRespuesta($idRespuesta)
	{
		$sql = "SELECT * from respuesta where idrespuesta = ".$idRespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos el id de autoevaluacion segun su el id de pregunta
	public function idautoevaluacionPregunta($idpregunta){
		$sql = "SELECT idautoeval from preguntas where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Actualizamos una pregunta
	public function actualizar_pregunta($enunciado,$idpregunta){
		$return = true;

		$enunciado = $enunciado;
		$sql = "UPDATE preguntas SET pregunta = '$enunciado' where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
		{
			$sql = "SELECT * from preguntas where idpreguntas = ".$idpregunta;
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			$f = mysqli_fetch_assoc($resultado);
			if(!empty ($f['imagen_pregunta'])) {unlink($f['imagen_pregunta']);}
			$tipoArchivo = $_FILES['imagen_pregunta']['type'];
			if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/gif' || $tipoArchivo == 'image/png')
			{
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $idpregunta, 'pregunta');
			}
			else
			{
				Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');

				$return = false;
			}
		}
		return $return;
	}

	// buscamos una ultima respuesta
	public function buscar_ultima_respuesta(){
		$sql = "SELECT MAX(idrespuesta) from respuesta";
		$resultado = mysqli_query($this->con,$sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->maxr = $f['MAX(idrespuesta)'];
	}

	// buscamos todas las repuestas que pertenezcan a una pregunta
		public function buscar_respuestas($idpregunta){
		$sql = "SELECT * from respuesta where idpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta segun el id pasado
	public function buscar_una_respuesta($idrespuesta){
		$sql = "SELECT * from respuesta where idrespuesta = ".$idrespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos las respuestas para una pregunta
	public function insertar_respuesta($respuesta,$correcta,$max)
	{
		//print_r($respuesta);

		for($i=0;$i<=count($respuesta)-1;$i++){
			if(!empty ($respuesta[$i]))
			{

				$respuesta[$i] = $respuesta[$i];

				if($correcta[0]-1 == $i) $bien = 1;
				else $bien = 0;
				$sql = "Insert into respuesta (respuesta,correcta,idpreguntas) VALUES ('$respuesta[$i]','$bien','$max')";
				//echo $sql."<br/>";
				$this->consultaSql($sql);
				$num = $i+1;

				$id = $this->obtenerUltimoIdInsertado();


				if(isset($_FILES['imagen_respuesta_'.$num]['tmp_name']) && !empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
				{
					$tipoArchivo = $_FILES['imagen_respuesta_'.$num]['type'];
					if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
					{
						Autoevaluaciones::guardarFotoCurso($_FILES['imagen_respuesta_'.$num], $id, 'respuesta');
					}
					else
					{
						Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
					}
				}
			}

			}
		}

	// actualizamos una respuesta
	public function actualizar_respuestas($respuesta,$correcta,$idrespuesta)
	{
			for($i=0;$i<=count($respuesta)-1;$i++)
			{
				if(!empty ($respuesta[$i]) && is_numeric($idrespuesta[$i]))
				{

					$respuesta[$i] = $respuesta[$i];

					if($correcta[0]-1 == $i) $bien = 1;
					else $bien = 0;
					$sql = "UPDATE respuesta SET
					respuesta = '$respuesta[$i]',
					correcta = '$bien'
					where idrespuesta = ".$idrespuesta[$i];
					//echo $sql."<br/>";
					$this->consultaSql($sql);
					$num = $i+1;

					if(!empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
					{

						$consulta = "SELECT * from respuesta where idrespuesta = ".$idrespuesta[$i];
						$resultado = $this->consultaSql($consulta);
						$f = mysqli_fetch_assoc($resultado);
						if(!empty ($f['imagen_respuesta'])) unlink($f['imagen_respuesta']);

							$info=GetImageSize($_FILES['imagen_respuesta_'.$num]['tmp_name']);
							switch ($info[2])
							{
							case 1:
								$extension='gif';
								break;
							case 2:
								$extension='jpg';
								break;
							case 3:
								$extension='png';
								break;
							default:
								$extension = '';
								break;
							}

						if($extension == 'gif' || $extension == 'jpg' || $extension == 'png')
						{

							$dir = "imagenes/autoevaluaciones/respuestas/";
							chmod($dir,0777);
							copy($_FILES['imagen_respuesta_'.$num]['tmp_name'],$dir.$idrespuesta[$i].".".$extension);
							$sql = "UPDATE respuesta SET imagen_respuesta = '".$dir.$idrespuesta[$i].".".$extension."'
							where idrespuesta = ".$idrespuesta[$i];
							$this->consultaSql($sql);
						}
						else
						{
							Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
						}
					}
				}
			}
	}
/******************************************************************************************
PLANTILLAS PARA LA AUTOEVALUACION
******************************************************************************************/

	// buscamos todas las autoevaluaciones de la tabla plantillas
	public function plantillasAutoeval(){
		$sql = "SELECT * from plantilla_autoeval ORDER BY PTtitulo_autoeval ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaciones activas de la plantilla
	public function autoevalActivasPlantilla(){
		$sql = "SELECT * from plantilla_autoeval where PTborrado = 0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos todas las autoevaluaciones activas de la plantilla segun su tematica
	public function autoevalActivasTematicaPlantilla($idTematica){
		$sql = "SELECT * from plantilla_autoeval
		where PTborrado = 0
		and PTidtematica_autoeval = ".$idTematica;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos autoevaluacion segun id pasado como parametro de la tabla plantilla
	public function buscar_autoevalPlantilla(){
		$sql = "SELECT * from plantilla_autoeval where PTidautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un formato para la autoevaluacion plantilla
	public function insertar_autoevaluacionPlantilla($titulo, $descripcion, $fecha_creacion, $idtematica, $idTematica){
		$fecha_creacion = date("Y-m-d");
		$sql = "INSERT into plantilla_autoeval" .
		" (PTtitulo_autoeval, PTdescripcion, PTf_creacion, PTidtipo_autoeval, PTidtematica_autoeval)" .
		" VALUES ('$titulo', '$descripcion', '$fecha_creacion', '$idtematica', '$idTematica')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// actualizamos la autoevaluacion de la plantilla
	public function actualizar_autoevalPlantilla($titulo, $idTematica, $descripcion, $tipoTest){
		$sql = "UPDATE plantilla_autoeval" .
		" SET PTtitulo_autoeval = '$titulo'," .
		" PTidtipo_autoeval = '$tipoTest'," .
		" PTdescripcion = '$descripcion'," .
		" PTidtematica_autoeval = '$idTematica'" .
		" where PTidautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluacion para la tabla plantilla
	public function eliminar_autoevalPlantilla()
	{
		$sql = "UPDATE plantilla_autoeval SET PTborrado = 1 where PTidautoeval = ".$this->idautoevaluacion;
		//echo $sql;
		$this->consultaSql($sql);
	}

	public function copiarPregunta($enunciado, $img, $idautoeval)
	{
		$sql = "INSERT into preguntas (pregunta, imagen_pregunta, idautoeval) VALUES ('$enunciado','$img','$idautoeval')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function copiarRespuesta($respuesta, $img, $correcta, $idpregunta)
	{
		$sql = "INSERT into respuesta (respuesta, imagen_respuesta, correcta, idpreguntas) VALUES ('$respuesta','$img','$correcta','$idpregunta')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un pregunta para la tabla plantilla
	public function insertar_preguntaPlantilla($enunciado)
	{
		$sql = "SELECT * from plantilla_preguntas where PTpregunta = '".$enunciado."' and PTidautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);

		if(mysqli_num_rows($resultado) == 0)
		{
			$enunciado = $enunciado;
			$sql = "INSERT into plantilla_preguntas (PTpregunta,PTidautoeval) VALUES ('$enunciado','$this->idautoevaluacion')";
			//echo $sql;
			$this->consultaSql($sql);
			$id = $this->obtenerUltimoIdInsertado();
			if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
			{
				$tipoArchivo = $_FILES['PTimagen_pregunta']['type'];
				if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $id, 'pregunta');
			}
			$this->actualizar_pregunta($enunciado,$id);
		}
		else
		{
			Alerta::mostrarMensajeInfo('preguntayacreada','La pregunta ya fue creada');
		}
	}

	// Actualizamos una pregunta
	public function actualizar_preguntaPlantilla($enunciado,$idpregunta)
	{
		$return = true;

		$enunciado = $enunciado;
		$sql = "UPDATE plantilla_preguntas SET PTpregunta = '$enunciado' where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		if(isset($_FILES['imagen_pregunta']['tmp_name']) && !empty($_FILES['imagen_pregunta']['tmp_name']))
		{
			$sql = "SELECT * from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			$f = mysqli_fetch_assoc($resultado);
			if(!empty ($f['PTimagen_pregunta'])) {unlink($f['PTimagen_pregunta']);}
			$tipoArchivo = $_FILES['imagen_pregunta']['type'];
			if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/gif' || $tipoArchivo == 'image/png')
			{
				Autoevaluaciones::guardarFoto($_FILES['imagen_pregunta'], $idpregunta, 'pregunta');
			}
			else
			{
				Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');

				$return = false;
			}
		}
		return $return;
	}

	// borramos una pregunta para la tabla plantilla
	public function borrar_preguntaPlantilla($idpregunta){
		$sql = "SELECT * from plantilla_preguntas P, plantilla_respuesta R
		where P.PTidpreguntas = ".$idpregunta."
		and P.PTidpreguntas = R.PTidpreguntas";
		//echo $sql;
		$resultado = $this->consultaSql($sql);

		while ($f = mysqli_fetch_assoc($resultado))
		{
			if(!empty ($f['PTimagen_pregunta']) and file_exists($f['PTimagen_pregunta']))
			{
				unlink($f['PTimagen_pregunta']);
			}
			if(!empty ($f['PTimagen_respuesta']) and file_exists($f['PTimagen_respuesta']))
			{
				unlink($f['PTimagen_respuesta']);
			}

		}

		$sql = "DELETE from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$this->consultaSql($sql);
	}

	// obtenemos todas las preguntas de una autoevaluacion de la tabla plantilla
	public function preguntasPlantilla(){
		$sql = "SELECT * from plantilla_preguntas where PTidautoeval = ".$this->idautoevaluacion;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la ultima pregunta insertada en la base de datos para la plantilla
	public function buscar_ultima_preguntaPlantilla(){
		$sql = "SELECT MAX(PTidpreguntas) from plantilla_preguntas";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->max = $f['MAX(PTidpreguntas)'];
		return $this->max;
	}

	// buscamos una pregunta segun su id para la tabla plantilla
	public function buscar_preguntaPlantilla($idpregunta){
		$sql = "SELECT * from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta de la tabla plantilla
	public function buscarRespuestaPlantilla($idRespuesta)
	{
		$sql = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idRespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos el id de autoevaluacion segun su el id de pregunta de la tabla plantilla
	public function idautoevaluacionPreguntaPlantilla($idpregunta){
		$sql = "SELECT PTidautoeval from plantilla_preguntas where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f;
	}

	// buscamos una ultima respuesta para la plantilla
	public function buscar_ultima_respuestaPlantilla(){
		$sql = "SELECT MAX(PTidrespuesta) from plantilla_respuesta";
		$resultado = mysqli_query($this->con,$sql);
		$f = mysqli_fetch_assoc($resultado);
		$this->maxr = $f['MAX(PTidrespuesta)'];
	}

	// buscamos todas las repuestas que pertenezcan a una pregunta para la plantilla
		public function buscar_respuestasPlantilla($idpregunta){
		$sql = "SELECT * from plantilla_respuesta where PTidpreguntas = ".$idpregunta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos una respuesta segun el id pasado
	public function buscar_una_respuestaPlantilla($idrespuesta){
		$sql = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idrespuesta;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos las respuestas para una pregunta
	public function insertar_respuestaPlantilla($respuesta,$correcta,$max)
	{
		//print_r($respuesta);

		for($i=0;$i<=count($respuesta)-1;$i++){
			if(!empty ($respuesta[$i]))
			{

				$respuesta[$i] = $respuesta[$i];

				if($correcta[0]-1 == $i) $bien = 1;
				else $bien = 0;
				$sql = "Insert into plantilla_respuesta (PTrespuesta,PTcorrecta,PTidpreguntas) VALUES ('$respuesta[$i]','$bien','$max')";
				//echo $sql."<br/>";
				$this->consultaSql($sql);
				$num = $i+1;

				$id = $this->obtenerUltimoIdInsertado();


				if(isset($_FILES['imagen_respuesta_'.$num]['tmp_name']) && !empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
				{
					$tipoArchivo = $_FILES['imagen_respuesta_'.$num]['type'];
					if($tipoArchivo == 'image/jpeg' || $tipoArchivo == 'image/png' || $tipoArchivo == 'image/gif')
					{
						Autoevaluaciones::guardarFoto($_FILES['imagen_respuesta_'.$num], $id, 'respuesta');
					}
					else
					{
						Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
					}
				}
			}

			}
		}

// actualizamos una respuesta para la plantilla
	public function actualizar_respuestasPlantilla($respuesta,$correcta,$idrespuesta)
	{
			for($i=0;$i<=count($respuesta)-1;$i++)
			{
				if(!empty ($respuesta[$i]) && is_numeric($idrespuesta[$i]))
				{

					$respuesta[$i] = $respuesta[$i];

					if($correcta[0]-1 == $i) $bien = 1;
					else $bien = 0;
					$sql = "UPDATE plantilla_respuesta SET
					PTrespuesta = '$respuesta[$i]',
					PTcorrecta = '$bien'
					where PTidrespuesta = ".$idrespuesta[$i];
					//echo $sql."<br/>";
					$this->consultaSql($sql);
					$num = $i+1;

					if(!empty($_FILES['imagen_respuesta_'.$num]['tmp_name']))
					{

						$consulta = "SELECT * from plantilla_respuesta where PTidrespuesta = ".$idrespuesta[$i];
						$resultado = $this->consultaSql($consulta);
						$f = mysqli_fetch_assoc($resultado);
						if(!empty ($f['PTimagen_respuesta'])) unlink($f['PTimagen_respuesta']);

							$info=GetImageSize($_FILES['imagen_respuesta_'.$num]['tmp_name']);
							switch ($info[2])
							{
							case 1:
								$extension='gif';
								break;
							case 2:
								$extension='jpg';
								break;
							case 3:
								$extension='png';
								break;
							default:
								$extension = '';
								break;
							}

						if($extension == 'gif' || $extension == 'jpg' || $extension == 'png')
						{

							$dir = "imagenes/autoevaluaciones/respuestas/";
							chmod($dir,0777);
							copy($_FILES['imagen_respuesta_'.$num]['tmp_name'],$dir.$idrespuesta[$i].".".$extension);
							$sql = "UPDATE plantilla_respuesta SET PTimagen_respuesta = '".$dir.$idrespuesta[$i].".".$extension."'
							where PTidrespuesta = ".$idrespuesta[$i];
							$this->consultaSql($sql);
						}
						else
						{
							Alerta::mostrarMensajeInfo('extensionesvalidas','Las extensiones v&aacute;lidas son gif, jpg y png');
						}
					}
				}
			}
	}

	//eliminamos la imagen de una respuesta
	public function eliminarImagenRespuestaPlantilla($id)
	{
		$sql = "UPDATE plantilla_respuesta SET PTimagen_respuesta = '' where PTidrespuesta = ".$id;
		//echo $sql;
		return $this->consultaSql($sql);
	}

	public function configuracionTest($blanco, $nc, $nIntentos, $notaMinima, $aleatorio, $idmodulo, $idautoeval, $correccion)
	{
		$sql = "INSERT into test
		(penalizacion_blanco, penalizacion_nc, n_intentos, nota_minima, aleatorias, idmodulo, idautoeval, correccion)
		VALUES ('$blanco', '$nc', '$nIntentos', '$notaMinima', '$aleatorio', '$idmodulo', '$idautoeval', '$correccion')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function datosConfTest($idautoevaluacion)
	{
		$sql="SELECT tiempo, correccion FROM test where idautoeval = ".$idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

/******************************************************************************************
TIPO AUTOEVALUACION TEST / DESARROLLO
******************************************************************************************/

	// buscamos todas las tematicas de la autoevaluaci_n
	public function tematica_autoevaluacion(){
		$sql = "SELECT * from tipo_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un tipo de formato para la autoevaluaci_n
	public function tipo_autoeval($formato){
		$sql = "INSERT into tipo_autoeval (formato) VALUES ('$formato')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluaci_n
	public function eliminar_tipo_autoeval($formato){
		$sql = "DELETE tipo_autoeval where idtipo_autoeval = ".$idtipo_autoeval;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// actualizamos el tipo de autoevaluacion
	public function actualizar_tipo_autoeval($formato){
		$sql = "UPDATE tipo_autoeval SET formato = '$formato' where idtipo_autoeval = ".$idtipo_autoeval;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


/******************************************************************************************
TEMATICAS, RAMAS O FAMILIAS DE AUTOEVALUACION
******************************************************************************************/

	// buscamos todas las tematicas de la autoevaluacion
	public function tematicas(){
		$sql = "SELECT * from tematica_autoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// buscamos la autoevaluacion segun su id
	public function buscarTematicas($idTematica){
		$sql = "SELECT * from tematica_autoeval where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// insertamos un tipo de formato para la autoevaluacion
	public function insertarTematica($tematica){
		$sql = "INSERT into tematica_autoeval (tematica_autoeval) VALUES ('$tematica')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// eliminamos un tipo de formato para la autoevaluacion
	public function eliminarTematica($idTematica){
		$sql = "DELETE tematica_autoeval where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// actualizamos el tipo de autoevaluacion
	public function actualizarTematica($tematica, $idTematica){
		$sql = "UPDATE tematica_autoeval SET tematica_autoeval = '$tematica' where idtematica_autoeval = ".$idTematica;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


/*******************************************************************************************
INTRODUCCION DE CALIFICACIONES
*******************************************************************************************/

	//Creamos el registro para que cuente como un intento
	public function cuenta_intento($idmatricula,$idautoevaluacion)
	{
		$sql = "INSERT into calificaciones (idmatricula,idautoeval) VALUES ('$idmatricula','$idautoevaluacion')";
		//echo $sql.'<br/>';
		$this->consultaSql($sql);
		$id = $this->obtenerUltimoIdInsertado();
		return $id;
	}

	//Introducimos la nota de la autoevaluacion
	public function insertar_calificacion($idmatricula,$idautoevaluacion,$nota,$idcalificaciones)
	{
		$fecha = date("Y-m-d H:i:s");
		$sql = "UPDATE calificaciones SET
		nota = '$nota',
		fecha = '$fecha'
		where idcalificaciones = ".$idcalificaciones;
		//echo $sql.'<br/>';
		$this->consultaSql($sql);
	}

	//Comprobar numero de intentos
	public function numero_intentos($idmatricula,$idautoevaluacion)
	{
		$sql = "SELECT * from calificaciones where idmatricula = ".$idmatricula." and idautoeval = ".$idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Obtengo el numero de acceso de la configuracion de calificaciones
	public function configuracion_numero_acceso($idautoevaluacion)
	{
		$sql = "SELECT n_intentos from test
		where idautoeval = ".$idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f['n_intentos'];
	}


	//Respuestas de los alumnos
	public function respuestasAlumnos($idCalificacion)
	{
		$post = Peticion::obtenerPost();

		$sql = "SELECT * from preguntas where idautoeval = ".$this->idautoevaluacion;
		$resultado = $this->consultaSql($sql);
		$numero_preguntas = mysqli_num_rows($resultado);

		$matriz = 1;
		for($i=0;$i<=$numero_preguntas-1;$i++)
		{
			//Comprueba si existe una eleccion del alumno al enviar el test
			if(isset($post[$matriz.'eleccion']))
			{
				$eleccion = explode("_",$post[$matriz.'eleccion']);

				// Compruebo que existe una respuesta , la respuesta es $eleccion[1]
				if (!empty ($eleccion[1]))
				{
					$sql = "INSERT into respuestasalumno (idCalificaciones,idPreguntas,idRespuestas) VALUES ('$idCalificacion','$eleccion[0]','$eleccion[1]')";
					//echo $sql;
					$this->consultaSql($sql);
				}
				$matriz++;
			}
			else{$matriz++;}
		}
	}

	//Buscasmos la autoevaluacion segun el id de Calificacion
	public function datosCalificacion($idCalificacion)
	{
		$sql = "SELECT * from calificaciones where idcalificaciones = ".$idCalificacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos las respuestas almacenadas de los alumnos segun el id de calificacion
	public function alumnosCalificacion($idCalificacion)
	{
		$sql = "SELECT * from respuestasalumno where idCalificaciones = ".$idCalificacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos el alumnos a partir del id de calificacion
	public function alumnoCursoCalificacion($idCalificacion)
	{
		$sql = "SELECT * from alumnos A, matricula M, calificaciones C, curso CU
		where C.idcalificaciones = ".$idCalificacion."
		and C.idmatricula = M.idmatricula
		and M.idalumnos = A.idalumnos
		and M.idcurso = CU.idcurso";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerAsignacion($idAsignacion)
	{
		$sql = "SELECT * from test where idtest = ".$idAsignacion;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function actualizarAsignacion($idmodulo, $n_intentos, $penalizacion_blanco, $penalizacion_nc, $nota_minima, $tiempo,
	$modoCorreccion, $aleatoria, $idAsignacion)
	{
		$sql = "UPDATE test SET
		penalizacion_blanco = '$penalizacion_blanco',
		penalizacion_nc = '$penalizacion_nc',
		n_intentos = '$n_intentos',
		nota_minima = '$nota_minima',
		tiempo = '$tiempo',
		correccion = '$modoCorreccion',
		aleatorias = '$aleatoria',
		idmodulo = '$idmodulo'
		where idtest = ".$idAsignacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function eliminarAsignacionAutoevaluacion($idAsignar)
	{
		$sql="DELETE from test where idtest = ".$idAsignar;
		echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Eliminar Intento de participante
	public function eliminarIntentoParticipante($idCalificacion) {
		$sql="DELETE from calificaciones where idcalificaciones = " . $idCalificacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
}

