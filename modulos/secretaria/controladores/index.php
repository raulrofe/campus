<?php

	if(Peticion::isPost())
	{
		mvc::importar('lib/mail/phpmailer.php');
		mvc::importar('lib/mail/mail.php');

		$post = Peticion::obtenerPost();

		if(isset($post['asunto'], $post['mensaje']))
		{
			$mensaje = $post['mensaje'];
			$asunto = $post['asunto'];
			if(!empty($asunto) && !empty($mensaje))
			{
				$objModelo = new ModeloSecretaria();
				if($objModelo->addMensaje(Usuario::getIdUser(true), Usuario::getIdCurso(), $asunto, $mensaje))
				{
					$modelo = new ModeloSecretaria();
					if(Usuario::compareProfile('alumno'))
					{
						$usuario = $modelo->obtenerAlumno(Usuario::getIdUser(), Usuario::getIdCurso());
					}
					else
					{
						$usuario = $modelo->obtenerRRHH(Usuario::getIdUser(), Usuario::getIdCurso());
					}

					$usuario = $usuario->fetch_object();

					$objMail = Mail::obtenerInstancia();
					$enviado = $objMail->enviarSecretaria($usuario->email, $post['asunto'] . ' - ' . $usuario->nombrec . ' - ' . $usuario->nombreCurso, $asunto, $mensaje);

					if($enviado)
					{
						Alerta::mostrarMensajeInfo('enviadomensaje','Mensaje enviado');
					}
					else
					{
						Alerta::mostrarMensajeInfo('noenviar','No se pudo enviar el mensaje');
					}
				}
			}
		}
	}

	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');
