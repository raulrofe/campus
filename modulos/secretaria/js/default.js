$("#frmSecretaria").validate({
	errorElement: "div",
	messages: {
		asunto: {
			required: 'Introduce el asunto de tu mensaje'
		},
		mensaje : {
			required  : 'Introduce tu mensaje'
		}
	},
	rules: {
		asunto : {
			required  : true
		},
		mensaje : {
			required  : true
		}
	}
});