<?php
class ModeloSecretaria extends modeloExtend
{
	public function addMensaje($idUsuario, $idCurso, $asunto, $mensaje)
	{
		$sql = 'INSERT INTO secretaria (mensaje, asunto, idusuario, idcurso) VALUES' .
		' ("' . $mensaje . '", "' . $asunto . '","' . $idUsuario . '", ' . $idCurso . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT CONCAT(al.nombre, " ", al.apellidos) AS nombrec, al.email, c.titulo AS nombreCurso, al.dni, c.titulo FROM alumnos AS al LEFT JOIN curso AS c ON c.idcurso = ' .$idCurso  . ' WHERE al.idalumnos = ' . $idAlumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRRHH($idrrhh, $idCurso)
	{
		$sql = 'SELECT rh.nombrec, rh.email, c.titulo AS nombreCurso FROM rrhh AS rh LEFT JOIN curso AS c ON c.idcurso = ' .$idCurso  . ' WHERE rh.idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}