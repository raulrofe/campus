<div id="secretaria">
	<div class='subtitle t_center'><span data-translate-html="secretaria.titulo">Secretar&iacute;a</span></div><br/>
	<form id="frmSecretaria" action="secretaria" method="post">
		<ul>
			<li>
				<label class="fleft" for="secretariaAsunto" data-translate-html="formulario.asunto">Asunto</label>
				<input class="fright" id="secretariaAsunto" type="text" name="asunto" />
				<div class="clear"></div>
			</li>
			<li>
				<label class="fleft" for="secretariaAsunto" data-translate-html="formulario.mensaje">Mensaje</label>
				<textarea name="mensaje" rows="1" cols="1" class="fright"></textarea>
			</li>
			<li class="t_right">
			<div class='clear'></div><br/>
				<button type="submit" data-translate-html="formulario.enviar">Enviar</button>
			</li>
		</ul>
	</form>
</div>

<script type="text/javascript" src="js_secretaria.js"></script>