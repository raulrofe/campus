<br/>
<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.titulo">EVALUACIONES</span>
</div>
<br/>

<div class="gestorTab">	
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs_evaluacion.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent evaluacionRectif' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
	
		<!--*********************************** TAB CALIFICACIONES -> FORO ************************************************* -->
		
		<?php require(dirname(__FILE__) . '/submenu.php') ?>	
		
		<div id="calificaciones_foro">
		
			<!-- ****************************************** -->
			<!-- Desplegable y boton de imprimir para foros -->
			
			<div class="headEvaluation" style="overflow:hidden">
				<div class="fleft">
					<form  name='frm_buscador' method='post' action=''>
						<select name='id_alumno' onchange='this.form.submit()'>
							<option value="0" <?php if($id_alumno == 0) echo 'selected'; ?> data-translate-html="evaluaciones.todos_alu">
								Todos los alumnos
							</option>
							<?php if(isset($dataAlumnos)): ?>
								<?php for($a=0;$a<=count($dataAlumnos['ident'])-1;$a++): ?>
									<option value="<?php echo $dataAlumnos['ident'][$a] ?>" <?php if($id_alumno == $dataAlumnos['ident'][$a]) echo 'selected'; ?>>
										<?php echo Texto::textoFormateado(ucwords($dataAlumnos['apellidos'][$a])).", ".Texto::textoFormateado(ucwords($dataAlumnos['nombre'][$a])) ?>
									</option>
								<?php endfor; ?>
							<?php endif; ?>
						</select>
					</form>
				</div>
				<div class="fright">
					<div class="menuVista printEvaluation">
						<a href="calificaciones/foros/informe/<?php echo $id_alumno; ?>" target="_blank" rel="tooltip" title="imprimir" data-translate-title="evaluaciones.imprimir">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista" />
						</a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			
			<!-- ********************************************** -->
			<!-- FIN DESPLEGABLE + BOTON DE IMPRIMIR PARA FOROS -->		
			<!-- VISTA DETALLE O LISTADO -->
			<?php if($id_alumno == 0):?>
				<?php require(dirname(__FILE__) . '/foro/listado.php') ?>
			<?php else: ?>
				<?php require(dirname(__FILE__) . '/foro/detalle.php') ?>	
			<?php endif; ?>
				
		</div>
	</div>
</div>