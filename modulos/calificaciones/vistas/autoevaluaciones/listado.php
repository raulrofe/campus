<?php if(isset($resultadoAlumno) && $resultadoAlumno->num_rows > 0):?>

<table class="tabla">

	<tr class="headTableEvaluation">
		<td data-translate-html="calificaciones.bus">Apellidos, nombre</td>
		<td data-translate-html="centro.nif">NIF</td>
		<td class="t_center" data-translate-html="evaluaciones.participacion">Participaci&oacute;n</td>
		<td class="t_center" data-translate-html="evaluaciones.nmedia">Nota Media</td>
		<td class="t_center">
			<span data-translate-html="evaluaciones.nmedia">Nota Media</span> 
			(<?php echo $configNotas->autoevaluaciones; ?>%)
		</td>
		<td class="t_center" <?php if(Usuario::compareProfile(array('tutor'))): ?> colspan="2" <?php endif; ?> data-translate-html="centro.detalle">Detalle</td>
	</tr>

	<?php while($alumno = $resultadoAlumno->fetch_object()):?>
		<!-- Calculo las notas medias -->
		<?php $mediaTestsReduce = $objN->calcularNotaMediaAutoeval($alumno->idalumnos, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true); ?>
		<?php $mediaTests = $objN->calcularNotaMediaAutoeval($alumno->idalumnos, $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);?>
		<?php $percentTest = ($mediaTests * 10); ?>

		<!-- Calculo el numero de test que ha realizado el alumno -->
		<?php $numTestAlumno = $objNotas->obtenerNumTestAlumno($alumno->idmatricula); ?>
		<?php if($numTestAlumno->num_rows > 0):?>
			<?php $numTestAlumno = $numTestAlumno->num_rows; ?>
		<?php else: ?>
			<?php $numTestAlumno = 0; ?>
		<?php endif; ?>

		<tr class="rowStudent">
			<td><?php echo Texto::textoPlano(ucwords($alumno->apellidos) . ', ' . ucwords($alumno->nombre))?></td>
			<td><?php echo Texto::textoPlano($alumno->dni)?></td>
			<td class="t_center"><?php echo $numTestAlumno . ' de ' . $numTest; ?></td>
			<td class="t_center"><?php echo $mediaTests; ?></td>
			<td class="t_center"><?php echo $mediaTestsReduce; ?></td>
			<td class="t_center" <?php if(Usuario::compareProfile(array('tutor'))): ?> colspan="2" <?php endif; ?>>
				<a href="#" onclick="viewEvaluationForo(<?php echo $alumno->idalumnos; ?>);return false" rel="tooltip" title="ver detalle" data-translate-title="centro.detalle">
					<img src="imagenes/menu_vistas/view.png" />
				</a>
			</td>
		</tr>

		<tr class="hide detalleForo detalleForoHead" data-student="<?php echo $alumno->idalumnos; ?>">
			<td colspan="3" data-translate-html="gestioncurso.autoevaluacion">Autoevaluaci&oacute;n</td>
			<td class="t_center" data-translate-html="calificaciones.nota">Nota</td>
			<td class="t_center" data-translate-html="correo.fecha">Fecha</td>
			<td class="t_center" data-translate-html="centro.detalle">Detalle</td>
			<?php if(Usuario::compareProfile(array('tutor'))): ?>
				<td class="t_center" data-translate-html="general.eliminar">Eliminar</td>
			<?php endif; ?>
		</tr>

			<?php $resultado_test = $objNotas->obtenerNotasTests($alumno->idalumnos, $idCurso);?>

			<?php if(mysqli_num_rows($resultado_test) > 0): ?>
				<?php while($notaTest = mysqli_fetch_assoc($resultado_test)): ?>
					<?php
						$resultado_autoeval = $mi_trabajopractico->datosAutoevaluacion($notaTest['idautoeval']);
						$fila_autoeval = mysqli_fetch_assoc($resultado_autoeval);
					?>
					<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>">
						<td colspan="3" class='celdaTabla'><a href='evaluaciones/calificaciones/autoevaluacion_alumno/<?php echo $notaTest['idcalificaciones'] ?>' target='_blank'><?php echo Texto::textoFormateado($fila_autoeval['titulo_autoeval'])?></a></td>
						<td class='t_center'><?php echo $notaTest['nota']?></td>
						<td class='t_center'><?php echo Fecha::obtenerFechaFormateada($notaTest['fecha'],false)?></td>
						<td class='celdaTabla t_center'>
							<a href='evaluaciones/calificaciones/autoevaluacion_alumno/<?php echo $notaTest['idcalificaciones'] ?>' target='_blank'>
								<img alt="" src="imagenes/menu_vistas/view2.png" />
							</a>
						</td>
						<?php if(Usuario::compareProfile(array('tutor'))): ?>
							<td class='celdaTabla t_center'>
								<a href='evaluaciones/calificaciones/eliminar_autoevaluacion_alumno/<?php echo $notaTest['idcalificaciones'] ?>'>
									<img alt="" src="imagenes/options/delete.png" />
								</a>
							</td>
						<?php endif; ?>
					</tr>
				<?php endwhile ?>
			<?php else: ?>
				<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>">
					<td class="t_center" colspan="7" data-translate-html="centro.no_test">No ha realizado test de autoevaluaciones todav&iacute;a</td>
				</tr>
			<?php endif; ?>

	<?php endwhile;?>
</table>

<script type="text/javascript" src="js-calificaciones-detalle_foro.js"></script>


	<div id='autoevaluacion'>
	<?php while($dato_unAlumno = mysqli_fetch_assoc($resultadoAlumno)): ?>
		<?php $resultado_test = $objNotas->obtenerNotasTests($dato_unAlumno['idalumnos'],$idCurso);?>
			<div>
			<form name='frm_trabajos' method='post' action='' >
				<div><?php  echo Texto::textoFormateado(ucwords($dato_unAlumno['nombre']))." ".Texto::textoFormateado(ucwords($dato_unAlumno['apellidos']))." (".$dato_unAlumno['dni'].")"; ?></div>
				<table class='tabla'>
					<tr class='filaTabla'>
						<td class='celdaTabla'>Autoevaluaciones</td>
						<td class='celdaTabla'>Fecha realizaci&oacute;n</td>
						<td class='celdaTabla'>Nota</td>
					</tr>
						<?php if(mysqli_num_rows($resultado_test) > 0): ?>
							<?php while($notaTest = mysqli_fetch_assoc($resultado_test)): ?>
								<?php
									$resultado_autoeval = $mi_trabajopractico->datosAutoevaluacion($notaTest['idautoeval']);
									$fila_autoeval = mysqli_fetch_assoc($resultado_autoeval);
								?>
								<tr>
									<td class='celdaTabla'><a href='evaluaciones/calificaciones/autoevaluacion_alumno/<?php echo $notaTest['idcalificaciones'] ?>' target='_blank'><?php echo Texto::textoFormateado($fila_autoeval['titulo_autoeval'])?></a></td>
									<td class='celdaTabla'><?php echo Fecha::obtenerFechaFormateada($notaTest['fecha'],false)?></td>
									<td class='celdaTabla'><?php echo $notaTest['nota']?></td>
								</tr>
							<?php endwhile ?>
						<?php else: ?>
							<tr>
								<td class="celdaTabla" data-translate-html="centro.no_test">No ha realizado test de autoevaluaciones todav&iacute;a</td>
								<td></td>
								<td></td>
							</tr>
						<?php endif; ?>
				</table>
			</form>
		</div>
		<br/>
		<div class="clear"></div>
	<?php endwhile; ?>
	</div>
<?php endif; ?>
