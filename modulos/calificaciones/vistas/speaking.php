<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.titulo">EVALUACIONES</span>
</div>
<br/><br/>

<div class="gestorTab">
		<!-- menu de iconos -->
		<?php require(dirname(__FILE__) . '/tabs_evaluacion.php'); ?>

		<!-- Contenido tabs -->
		<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">

		<?php require(dirname(__FILE__) . '/submenu.php'); ?>

		<div id='speaking'>
			<div>
				<div class="fleft">
					<form name='frm_buscador' method='post' action=''>
						<input type='text' placeholder='Introduce el nombre, apellido o DNI' value="<?php if(isset($post['buscar'])) echo Texto::textoPlano($post['buscar'])?>" name='buscar' style='width:300px;' data-translate-placeholder="evaluaciones.bus"/>
					</form>
				</div>
				<div class="fright">
					<div class="menuVista printEvaluation" style="border:1px solid #D5D5D5;border-radius:5px;">
						<a href="calificaciones/trabajos-practicos/informe" target="_blank" rel="tooltip" title="imprimir" data-translate-title="evaluaciones.imprimir">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista"/>
						</a>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div id='imprimirSpeaking'>
				<form name='frm_speaking' method='post' action='' >
					<div class='clear'></div>
					<table style='border:1px solid #666;'>
						<tr class='filaTabla'>
							<td colspan="2"  class='rojo_pastel' data-translate-html="integrantes.alumnos">Alumnos</td>
							<td class='amarillo' data-translate-html="tutoria.menu.speaking">Speaking</td>
							<td class='naranja' data-translate-html="misprogresos.notafinal.titulo">Nota final</td>
						</tr>
						<tr>
							<td class='rojo_pastel' style='text-align:left;' data-translate-html="usuario.nombre">Nombre</td>
							<td class='rojo_pastel' data-translate-html="usuario.dni">DNI</td>
							<td class='amarillo' data-translate-html="misprogresos.autoevaluaciones.nota">Nota</td>
							<td class='naranja'>
								<span data-translate-html="evaluaciones.media">
									Media
								</span> 
								<?php echo $configNotas->extra1 ?> %
							</td>
						</tr>
						<?php while($alum = mysqli_fetch_object($resultado_alumnos)): ?>
							<tr>
								<td class='rojo_pastel' style='text-align:left;'><?php echo $alum->apellidos . ', ' . $alum->nombre ?></td>
								<td class='rojo_pastel'><?php echo $alum->dni ?></td>
								<?php if(!empty($notasSpeakings) && isset($notasSpeakings[$alum->idmatricula])): ?>
									<td class='amarillo'><input type='text' name="notaSpeaking[<?php echo $alum->idmatricula ?>]" value="<?php echo $notasSpeakings[$alum->idmatricula] ?>" /></td>
								<?php else: ?>
									<td class='amarillo'><input type='text' name="notaSpeaking[<?php echo $alum->idmatricula ?>]" value="" /></td>
								<?php endif; ?>
								<td class='naranja'>-</td>
							</tr>
						<?php endwhile; ?>
					</table>
					<?php if(!Usuario::compareProfile('supervisor')): ?>
						<input type='submit' value='Guardar Notas' style='width:100%;margin:10px 0 20px;' data-translate-value="evaluaciones.guardarnotas"/>
					<?php endif; ?>
				</form>
			</div>
		</div>

		<!-- ***************************** TAB 2 TRABAJOS PRACTICOS ************************************************** -->

		<div id="tab2" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php'); ?>
			<div class="clear"></div>
		</div>

		<!-- ***************************** TAB 3 FOROS ************************************************************ -->

		<div id="tab3" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php'); ?>
			<div class="clear"></div>
		</div>



		<!-- Fin de programas utiles -->

	</div>
</div>

<script type="text/javascript" src="js-calificaciones-trab_pract.js"></script>
