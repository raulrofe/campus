<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es" style="overflow: initial !important">
	<head profile="http://gmpg.org/xfn/11">
		<title data-translate-html="evaluaciones.aulainteractiva">AulaInteractiva</title>

		<base href="<?php echo URL_BASE?>" />

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript" src="js_default.js"></script>
		<link href="css_default.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div class='caja_separada' id='autoevaluacionImprimir'>
	<div class='cabeceraAutoeval' style='font-size:1.2em;padding:10px;'>
		<div>
			<span class='negrita cursiva' data-translate-html="usuario.nombre">
				Nombre: 
			</span>

			<?php echo $matricula['nombre']." ".$matricula['apellidos']." (".$matricula['dni'].")";?>

		</div>

		<div>
			<span class='negrita cursiva' data-translate-html="genral.curso">Curso: </span>
			<?php echo Texto::textoFormateado($matricula['titulo']);?>
		</div>

		<div>
			<span class='negrita cursiva' data-translate-html="autoevaluaciones.test">Test: </span>
			<?php echo Texto::textoFormateado($autoevaluacion['titulo_autoeval']);?>
		</div>

		<div>
			<span class='negrita cursiva' data-translate-html="evaluaciones.fecharealizacion">
				Fecha realizaci&oacute;n: 
			</span>
			<?php echo Fecha::obtenerFechaFormateada($calificacion['fecha'],false);?>
		</div>

		<div>
			<span class='negrita cursiva' data-translate-html="autevaluaciones.nota">Nota: </span>
			<?php echo $calificacion['nota'];?>
		</div>
	</div>
		<?php while($pregunta = mysqli_fetch_assoc($preguntas)):?>
		<div class='cabeceraAutoeval'>
			<div class='negrita' style='padding:0 5px;'>
				<?php echo $numPregunta." ".Texto::textoFormateado($pregunta['pregunta'])?>
				<?php if(!empty($pregunta['imagen_pregunta'])):?>
					<img src='<?php echo $pregunta['imagen_pregunta'];?>' alt='' />
				<?php endif;?>
			</div>
			<?php $respuestas = $objAutoevaluacion->buscar_respuestas($pregunta['idpreguntas']);?>
			<?php $letraRespuesta=1;?>
			<br/>

			<?php while($respuesta = mysqli_fetch_assoc($respuestas)):?>

				<?php if($respuesta['correcta'] == '1'):?>
					<div class='verde sangria pregunta'>
						<?php echo Autoevaluaciones::obtener_letra($letraRespuesta).Texto::textoFormateado($respuesta['respuesta'])?>
						<?php if(!empty($respuesta['imagen_respuesta'])):?>
							<img src='<?php echo $respuesta['imagen_respuesta'];?>' alt='' />
						<?php endif;?>
					</div>
				<?php else:?>
					<div class='sangria respuesta'>
						<?php echo Autoevaluaciones::obtener_letra($letraRespuesta).Texto::textoFormateado($respuesta['respuesta'])?>
						<?php if(!empty($respuesta['imagen_respuesta'])):?>
							<img src='<?php echo $respuesta['imagen_respuesta'];?>' alt='' />
						<?php endif;?>
					</div>
				<?php endif;?>
				<?php $letraRespuesta++;?>
			<?php endwhile;?>

			<br/>
			<div style='border-top:1px solid #CECECE;padding:5px 0 0 0;;padding:5px;background:#FEF8EC;'>
			<?php $respuestaBlanco = 0;?>
			<?php for($i=0;$i<=$numRespuestas;$i++):?>
					<?php if(isset($matrizRespuestas['pregunta'][$i]) && $matrizRespuestas['pregunta'][$i] == $pregunta['idpreguntas']):?>
						<?php $respuestas = $objAutoevaluacion->buscarRespuesta($matrizRespuestas['respuesta'][$i])?>
						<?php $resp = mysqli_fetch_assoc($respuestas);?>
						<div>Respuesta elegida: <?php echo Texto::textoFormateado($resp['respuesta']); ?></div>

						<?php if($resp['correcta'] == '1'):?>
							<span class='verde' data-translate-html="evaluaciones.bien">¡Muy Bien!</span>
						<?php else:?>
							<span class='rojo' data-translate-html="evaluaciones.incorrecta">Respuesta Incorrecta</span>
						<?php endif;?>
					<?php $respuestaBlanco = 1;?>
					<?php endif;?>
			<?php endfor;?>

			<?php if($respuestaBlanco == 0):?>
				<div data-translate-html="evaluaciones.elegidablanco">Respuesta elegida: Respuesta en blanco</div>
				<span class='rojo' data-translate-html="evaluaciones.incorrecta">Respuesta Incorrecta</span>
			<?php endif;?>

			</div>
			<?php $numPregunta++ ?>
		</div>
		<?php endwhile;?>
	</div>
		<div class="fright">
			<input type="button" onclick="imprimir('autoevaluacionImprimir'); return false;" value="Imprimir"  data-translate-value="evaluaciones.imprimir"/>
		</div>
</body>
</html>