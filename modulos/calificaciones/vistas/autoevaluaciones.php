	<div class="t_center">
		<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
		<span data-translate-html="evaluaciones.titulo">EVALUACIONES</span>
	</div>
	<br/><br/>

<div class="gestorTab">
	<!-- menu de iconos -->
	<?php require(dirname(__FILE__) . '/tabs_evaluacion.php') ?>
	
	<!-- Contenido tabs -->
	<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">
	
		<!--*********************************** TAB 1 CALIFICACIONES ************************************************* -->
				<?php require(dirname(__FILE__) . '/submenu.php') ?>				
				<div id='autoevauaciones'>
					<div class="headEvaluation" style="overflow:hidden">
						<div class="fleft">
							<form  name='frm_buscador' method='post' action='#'>
								<select name='id_alumno' onchange='this.form.submit()'>
									
									<option value="0" <?php if($id_alumno == 0) echo 'selected'; ?> data-translate-html="evaluaciones.todos_alu">
										Todos los alumnos
									</option>

									<?php if(isset($alumnos)): ?>
										<?php for($a=0;$a<=count($alumnos['ident'])-1;$a++): ?>
											<option value="<?php echo $alumnos['ident'][$a] ?>" <?php if($id_alumno == $alumnos['ident'][$a]) echo 'selected'; ?>>
												<?php echo Texto::textoFormateado(ucwords($alumnos['apellidos'][$a])).", ".Texto::textoFormateado(ucwords($alumnos['nombre'][$a])) ?>
											</option>
										<?php endfor; ?>
									<?php endif; ?>
								</select>
							</form>
						</div>
						<div class="fright">
							<div class="menuVista printEvaluation">
								<a href="calificaciones/autoevaluaciones/informe/<?php echo $id_alumno; ?>" target="_blank" rel="tooltip" title="imprimir" data-translate-title="evaluaciones.imprimir">
									<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista"/>
								</a>
							</div>
						</div>
					</div>
					
					<!-- VISTA DETALLE O LISTADO -->
					<?php if($id_alumno == 0):?>
						<?php require(dirname(__FILE__) . '/autoevaluaciones/listado.php') ?>	
					<?php else: ?>
						<?php require(dirname(__FILE__) . '/autoevaluaciones/detalle.php') ?>	
					<?php endif; ?>
					

					<div class='clear'></div>
				</div>
		
		<!-- ***************************** TAB 2 TRABAJOS PRACTICOS ************************************************** -->
		
		<div id="tab2" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php') ?>	
			<div class="clear"></div>
		</div>
	
		<!-- ***************************** TAB 3 FOROS ************************************************************ -->
		
		<div id="tab3" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php') ?>	
			<div class="clear"></div>
		</div>
		

		
		<!-- Fin de programas utiles -->
		
	</div>
</div>
<script type="text/javascript" src="js-calificaciones-changetab.js"></script>