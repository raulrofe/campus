<div class="t_center">
	<img src="imagenes/integrantes_curso/integrantes-curso.png" alt="" class="imageIntro"/>
	<span data-translate-html="evaluaciones.titulo">EVALUACIONES</span>
</div>
<br/><br/>

<div class="gestorTab">
		<!-- menu de iconos -->
		<?php require(dirname(__FILE__) . '/tabs_evaluacion.php'); ?>

		<!-- Contenido tabs -->
		<div class='fleft panelTabContent' id="contenidoIntegrantesCurso" style="padding: 0 30px;">

		<?php require(dirname(__FILE__) . '/submenu.php'); ?>

		<div id='trabajos'>

			<div>
				<div class="fleft">
					<form name='frm_buscador' method='post' action=''>
						<input type='text' placeholder='Introduce el nombre, apellido o DNI' value="<?php if(isset($post['buscar'])) echo Texto::textoPlano($post['buscar'])?>" name='buscar' style='width:300px;' data-translate-placeholder="evaluaciones.bus"/>
					</form>
				</div>
				<div class="fright">
					<div class="menuVista printEvaluation" style="border:1px solid #D5D5D5;border-radius:5px;">
						<a href="calificaciones/trabajos-practicos/informe" target="_blank" rel="tooltip" title="imprimir" data-translate-title="evaluaciones.imprimir">
							<img src="imagenes/menu_vistas/imprimir.png" alt="vista" data-translate-title="seguimiento.vista"/>
						</a>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div id='imprimirTrabajosPrecticos'>
			<form name='frm_trabajos' method='post' action='' >

				<div class='clear'></div>
				<table style='border:1px solid #666;'>
					<tr class='filaTabla'>
						<td colspan="2"  class='rojo_pastel' data-translate-html="integrantes.alumnos">Alumnos</td>
						<?php $numero_modulos; ?>
							<?php for($i=0;$i<=$numero_modulos-1;$i++): ?>
								<?php $n = $i + 1; ?>
								<?php $colspan = $numero_archivos[$i]; ?>

								<?php if($i == $numero_modulos-1): ?>
									<td colspan="<?php echo $colspan ?>" class="<?php echo $colores[$i] ?>">
										<span data-translate-html="gestioncurso.modulo">
											M&oacute;dulo
										</span> 
										<?php echo $n ?></td>
								<?php else: ?>
									<td colspan="<?php echo $colspan ?>" class="<?php echo $colores[$i] ?>">
										<span data-translate-html="gestioncurso.modulo">
											M&oacute;dulo
										</span> 
										<?php echo $n ?></td>
								<?php endif; ?>
							<?php endfor; ?>
						<td class='naranja' data-translate-html="misprogresos.notafinal.titulo">Nota final</td>
					</tr>
					<tr>
						<td class='rojo_pastel' style='text-align:left;' data-translate-html="usuario.nombre">Nombre</td>

						<td class='rojo_pastel' data-translate-html="usuario.dni">DNI</td>

						<?php if(count($numero_archivos) > 0): ?>
							<?php for($i=0;$i<=count($numero_archivos)-1;$i++): ?>
								<?php $n_romano=1; ?>
								<?php if($numero_archivos[$i] > 0): ?>
									<?php for($j=0;$j<$numero_archivos[$i];$j++): ?>
										<?php if(($i == $numero_archivos) AND ($j == $numero_archivos[$i])): ?>
											<td class="<?php echo $colores[$i] ?>">
												<?php echo TrabajosPracticos::obtener_numero_romano($n_romano)?>
											</td>
										<?php else: ?>
											<td class="<?php echo $colores[$i] ?>">
												<?php echo TrabajosPracticos::obtener_numero_romano($n_romano) ?>
											</td>
											<?php $n_romano++; ?>

										<?php endif; ?>
									<?php endfor; ?>
								<?php else: ?>
									<td class="<?php echo $colores[$i] ?>"> - </td>
								<?php endif; ?>
							<?php endfor; ?>
						<?php else: ?>
							<td class="<?php echo $colores[$i] ?>"> - </td>
						<?php endif; ?>
						<td class='naranja' data-translate-html="evaluaciones.media">Media</td>
					</tr>
						<?php if(!empty($alumnos)): ?>
							<?php reset($numero_archivos); ?>
							<?php for($a=0;$a<=count($alumnos['ident'])-1;$a++): ?>
								<?php $trabajosParticipado = $mi_trabajopractico->obtenerNotaTrabajosPracticosAlumno($alumnos['ident'][$a], $idCurso); ?>
								<?php $trabajosParticipado = $trabajosParticipado->num_rows; ?>
								<?php $porcentajeTrabajos = ($trabajosParticipado * 100) / $numTrabajos; ?>
								<?php $nota_final = 0; ?>
								<tr class='filaResult'>
									<td class='rojo_pastel' style='text-align:left;'>
										<?php echo $alumnos['apellidos'][$a] . ", " . $alumnos['nombre'][$a] ?>
									</td>

									<td class='rojo_pastel'>
										<?php echo $alumnos['dni'][$a] ?>
									</td>

									<?php for($i=0;$i<=count($numero_archivos)-1;$i++): ?>
										<?php if($numero_archivos[$i] > 0): ?>
											<?php for($j=0;$j<$numero_archivos[$i];$j++): ?>
												<?php $nota = $mi_trabajopractico->obtengoNota($alumnos['ident'][$a],$archivos[$i][$j]); ?>

												<?php $nota_final = $nota_final + $nota; ?>

												<?php if(Usuario::compareProfile('supervisor')): ?>
													<td class="<?php echo $colores[$i] ?>">
														<input type='text' name="<?php echo $_SESSION['idcurso'] . "_" . $alumnos['ident'][$a] . "_" . $archivos[$i][$j] ?>" value="<?php echo $nota ?>" disabled/>
													</td>
												<?php else: ?>
													<td class="<?php echo $colores[$i] ?>">
														<input type='text' name="<?php echo $_SESSION['idcurso'] . "_" . $alumnos['ident'][$a] . "_" . $archivos[$i][$j] ?>" value="<?php echo $nota ?>" />
													</td>
												<?php endif; ?>
											<?php endfor; ?>
										<?php else: ?>
											<td class="<?php echo $colores[$i] ?>"> - </td>
										<?php endif; ?>
									<?php endfor; ?>

									<?php $nota_final = $objNotas->calcularMediaTrabajosPract($alumnos['ident'][$a],$configuracion['num_trabajos'],$configuracion['trabajos_practicos'], false); ?>

									<td class='naranja'><?php echo $nota_final ?></td>
								</tr>
							<?php endfor; ?>
						<?php else: ?>
							<tr>
								<td class="rojo_pastel" colspan="100%" data-translate-html="evaluaciones.no_busqueda">
									No se encontraron coincidencias en la b&uacute;squeda
								</td>
							</tr>
						<?php endif; ?>
				</table>
				<?php if(!Usuario::compareProfile('supervisor')): ?>
					<input type='submit' value='Guardar Notas' style='width:100%;margin:10px 0 20px;' data-translate-value="evaluaciones.guardarnotas"/>
				<?php endif; ?>
			</form>
			</div>
		</div>

		<!-- ***************************** TAB 2 TRABAJOS PRACTICOS ************************************************** -->

		<div id="tab2" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php'); ?>
			<div class="clear"></div>
		</div>

		<!-- ***************************** TAB 3 FOROS ************************************************************ -->

		<div id="tab3" class="hide" >
			<?php require(dirname(__FILE__) . '/submenu.php'); ?>
			<div class="clear"></div>
		</div>



		<!-- Fin de programas utiles -->

	</div>
</div>

<script type="text/javascript" src="js-calificaciones-trab_pract.js"></script>
