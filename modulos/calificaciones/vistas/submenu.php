<div class="tituloTabs">
	<div class="tituloEvaluaciones" data-translate-html="popup.<?php echo Texto::textoPlano(ucfirst(str_replace('_', ' ', $get['c']))); ?>">

		<?php echo Texto::textoPlano(ucfirst(str_replace('_', ' ', $get['c']))); ?>
			
	</div>
	<div class="submenuTabs">
		<div class="elementSubmenuTab tests <?php if($get['c'] == 'autoevaluaciones') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/calificaciones/autoevaluaciones" rel="tooltip" title="Autoevaluaciones" data-translate-title="gestioncurso.autoevaluaciones">
				<img src="imagenes/calificaciones/tests.png" alt="autoevaluaciones" data-translate-title="gestioncurso.autoevaluaciones"/>
			</a>
		</div>
		<div class="elementSubmenuTab trabajosPracticos <?php if($get['c'] == 'trabajos_practicos') echo 'activeSubmenu'; ?>"style="border-left:1px solid #CCC;border-right:1px solid #CCC;">
			<a href="evaluaciones/calificaciones/trabajos_practicos" rel="tooltip" title="Trabajos pr&aacute;cticos" data-translate-title="menu.practicos">
				<img src="imagenes/calificaciones/trabajos-practicos.png" alt="Trabajos pr&aacute;cticos" data-translate-title="menu.practicos" />
			</a>
		</div>
		<?php if(isset($configNotas) && $configNotas->extra1 > 0): ?>
			<div class="elementSubmenuTab speaking <?php if($get['c'] == 'speaking') echo 'activeSubmenu'; ?>"style="border-left:1px solid #CCC;border-right:1px solid #CCC;">
				<a href="evaluaciones/calificaciones/speaking" rel="tooltip" title="Speaking" data-translate-title="tutoria.menu.speaking" data-translate-title="tutoria.menu.speaking">
					<img src="imagenes/calificaciones/speaking.png" alt="Speaking" />
				</a>
			</div>
		<?php endif; ?>
		<div class="elementSubmenuTab foros <?php if($get['c'] == 'foros') echo 'activeSubmenu'; ?>">
			<a href="evaluaciones/calificaciones/foros" rel="tooltip" title="Foros" data-translate-title="foro.titulo">
				<img src="imagenes/calificaciones/foro.png" alt="foros" data-translate-title="foro.titulo"/>
			</a>
		</div>
	</div>
	<div class="clear"></div>
</div>
