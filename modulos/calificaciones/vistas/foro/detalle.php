<table class="tabla">
	<tr class="headTableEvaluation">
		<td data-translate-html="calificaciones.bus">Apellidos, nombre</td>
		<td data-translate-html="centro.nif">NIF</td>
		<td class="t_center">
			<span rel="tooltip" title="participaci&oacute;n en temas de foros" data-translate-html="evaluaciones.participacion">
				Participaci&oacute;n
			</span>
		</td>
		<td class="t_center"><span rel="tooltip" title="Nota media de participaci&oacute;n en foros" data-translate-html="evaluaciones.nmedia">Nota Media</span></td>
		<td class="t_center"><span rel="tooltip" title="Nota media ponderada con el 20%">
		<span data-translate-html="evaluaciones.nmedia">
			Nota Media</span> (<?php echo $configNotas->foro; ?>%)</span></td>
		<td class="t_center" data-translate-html="centro.detalle">Detalle</td>
	</tr>
	 
	<?php while($alumno = $alumnos->fetch_object()):?>
		<!-- Calculo las notas medias -->
		<?php $foroPuntosReduce = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, true); ?>	
		<?php $foroPuntos = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, false); ?>
		<?php $percentForo = ($foroPuntos * 10); ?>		
		
		<!-- Calculo el numero de participacion en foros -->
		<?php $numMensajesTemasAlumno = $objForo->obtenerTemasAlumnoForo($alumno->idalumnos, $idCurso);?>
		<?php if($numMensajesTemasAlumno->num_rows > 0):?>
			<?php $numMensajesTemasAlumno = $numMensajesTemasAlumno->num_rows; ?>
		<?php else: ?>
			<?php $numMensajesTemasAlumno = 0; ?>
		<?php endif;?>
		
		<?php $porcentajeForo = ($numMensajesTemasAlumno * 100) / $numTemasForo; ?> 
		
		<tr class="rowStudent">
			<td><?php echo Texto::textoPlano(ucwords($alumno->nombrec))?></td>
			<td><?php echo Texto::textoPlano($alumno->dni)?></td>
			<td class="t_center"><?php echo $numMensajesTemasAlumno; ?> de <?php echo $numTemasForo; ?></td>
			<td class="t_center"><?php echo $foroPuntos; ?></td>
			<td class="t_center"><?php echo $foroPuntosReduce; ?></td>
			<td class="t_center"><img src="imagenes/menu_vistas/view.png" /></td>	
		</tr>

		<tr class="detalleForo detalleForoHead" data-student="<?php echo $alumno->idalumnos; ?>">
			<td colspan="4" data-translate-html="calificaciones.temaforo">Tema foro</td>
			<td class="t_center" data-translate-html="calificaciones.mensajes">Mensajes</td>
			<td class="t_center" data-translate-html="calificaciones.detalle">Detalle</td>
		</tr>
		<?php if($numMensajesTemasAlumno > 0):?>
			<?php for($i=0; $i<=count($temaForo['idTema'])-1;$i++):?>
				<?php 
					$mensajesForo = $objForo->obtenerMensajesTemasParticipadoPorAlumno($alumno->idalumnos, $idCurso, $temaForo['idTema'][$i]); 
					if($mensajesForo->num_rows > 0)
					{		
						$mensajeForo = $mensajesForo->fetch_object();
						$mensajesAlumnoTema = $mensajeForo->nMensajesTemasParticipados;
					}
					else
					{
						$mensajesAlumnoTema = 0;	
					}
				?>
				<tr class="detalleForo" data-student="<?php echo $alumno->idalumnos; ?>" >
					<td class='celdaTabla' colspan="4"><?php echo $temaForo['nombreTema'][$i] ?></td>
					<td class='celdaTabla t_center'><?php echo $mensajesAlumnoTema; ?></td>
					<td class='celdaTabla t_center'>
						<a href="#" rel="tooltip" title="Ver mensajes del alumno" onclick="parent.popup_open('Mensajes del foro', 'calificaciones_foro_mensajes_<?php echo $alumno->idalumnos?>', 'evaluaciones/calificaciones/foros/<?php echo $alumno->idalumnos?>', '400', '500'); return false;" title="">
							<img alt="" src="imagenes/menu_vistas/view2.png" />
						</a>
					</td>
				</tr>
			<?php endfor; ?>
		<?php else: ?>
			<tr class="detalleForo" data-student="<?php echo $alumno->idalumnos; ?>">
				<td class="t_center" colspan="7" data-translate-html="calificaciones.no_foro">No ha participado en los foros</td>
			</tr>
		<?php endif; ?>			
	<?php endwhile;?>	
</table>
