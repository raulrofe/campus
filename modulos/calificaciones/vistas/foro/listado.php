<table class="tabla">
	<tr class="headTableEvaluation">
		<td data-translate-html="calificaciones.bus">Apellidos, Nombre</td>
		<td data-translate-html="centro.nif">NIF</td>
		<td class="t_center"><span rel="tooltip" title="participaci&oacute;n en temas de foros" data-translate-html="evaluaciones.participacion">Participaci&oacute;n</span></td>
		<td class="t_center"><span rel="tooltip" title="Nota media de participaci&oacute;n en foros" data-translate-html="evaluaciones.nmedia">Nota Media</span></td>
		<td class="t_center"><span rel="tooltip" title="Nota media ponderada con el 20%">
			<span data-translate-html="evaluaciones.nmedia">Nota Media</span> (<?php echo $configNotas->foro; ?>%)</span></td>
		<td class="t_center" data-translate-html="centro.detalle">Detalle</td>
	</tr>
	<?php while($alumno = $alumnos->fetch_object()):?>
		<!-- Calculo las notas medias -->
		<?php $foroPuntosReduce = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, true); ?>

		<?php $foroPuntos = $objNotas->calcularNotaMediaForo($alumno->idalumnos, $idCurso, false); ?>

<?php //echo $foroPuntos . '<br/>'; ?>

		<?php $percentForo = ($foroPuntos * 10); ?>

		<!-- Calculo el numero de participacion en foros -->
		<?php $numMensajesTemasAlumno = $objForo->obtenerTemasAlumnoForo($alumno->idalumnos, $idCurso);?>
		<?php if($numMensajesTemasAlumno->num_rows > 0):?>
			<?php $numMensajesTemasAlumno = $numMensajesTemasAlumno->num_rows; ?>
		<?php else: ?>
			<?php $numMensajesTemasAlumno = 0; ?>
		<?php endif;?>

		<?php //echo $numMensajesTemasAlumno . ' dividido ' . $numTemasForo . '<br/>' ?>

		<?php $porcentajeForo = ($numMensajesTemasAlumno * 100) / $numTemasForo; ?>

		<tr class="rowStudent">
			<td><?php echo Texto::textoPlano(ucwords($alumno->nombrec))?></td>
			<td><?php echo Texto::textoPlano($alumno->dni)?></td>
			<td class="t_center"><?php echo $numMensajesTemasAlumno; ?> de <?php echo $numTemasForo; ?></td>
			<td class="t_center"><?php echo number_format($foroPuntos, '2', '.', ''); ?></td>
			<td class="t_center"><?php echo number_format($foroPuntosReduce, '2', '.', ''); ?></td>
			<td class="t_center">
				<a href="#" onclick="viewEvaluationForo(<?php echo $alumno->idalumnos; ?>);return false" rel="tooltip" title="ver detalle">
					<img src="imagenes/menu_vistas/view.png" />
				</a>
			</td>
		</tr>

		<tr class="hide detalleForo detalleForoHead" data-student="<?php echo $alumno->idalumnos; ?>">
			<td colspan="4" data-translate-html="calificaciones.temaforo">Tema foro</td>
			<td class="t_center" data-translate-html="calificaciones.mensajes">Mensajes</td>
			<td class="t_center" data-translate-html="calificaciones.detalle">Detalle</td>
		</tr>
		<?php if($numMensajesTemasAlumno > 0):?>
			<?php for($i=0; $i<=count($temaForo['idTema'])-1;$i++):?>
				<?php
					$mensajesForo = $objForo->obtenerMensajesTemasParticipadoPorAlumno($alumno->idalumnos, $idCurso, $temaForo['idTema'][$i]);
					if($mensajesForo->num_rows > 0)
					{
						$mensajeForo = $mensajesForo->fetch_object();
						$mensajesAlumnoTema = $mensajeForo->nMensajesTemasParticipados;
					}
					else
					{
						$mensajesAlumnoTema = 0;
					}
				?>
				<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>" >
					<td class='celdaTabla' colspan="4"><?php echo $temaForo['nombreTema'][$i] ?></td>
					<td class='celdaTabla t_center'><?php echo $mensajesAlumnoTema; ?></td>
					<td class='celdaTabla t_center'>
						<a href="#" rel="tooltip" title="Ver mensajes del alumno" onclick="parent.popup_open('Mensajes del foro', 'calificaciones_foro_mensajes_<?php echo $alumno->idalumnos?>', 'evaluaciones/calificaciones/foros/<?php echo $alumno->idalumnos?>', '400', '600'); return false;" title="">
							<img alt="" src="imagenes/menu_vistas/view2.png" />
						</a>
					</td>
				</tr>
			<?php endfor; ?>
		<?php else: ?>
			<tr class="hide detalleForo" data-student="<?php echo $alumno->idalumnos; ?>">
				<td class="t_center" colspan="7" data-translate-html="calificaciones.no_foro">No ha participado en los foros</td>
			</tr>
		<?php endif; ?>
	<?php endwhile;?>
</table>

<script type="text/javascript" src="js-calificaciones-detalle_foro.js"></script>