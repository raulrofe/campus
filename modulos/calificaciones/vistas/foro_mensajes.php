<div id="calificaciones_foro_mensajes">
	<div class='cabeceraAutoeval' style='font-size:1.2em;padding:10px;'>
		<div>
			<span class='negrita cursiva' data-translate-html="usuario.nombre">Nombre:</span> 
			<?php echo $alumno->nombrec . " (" . $alumno->dni . ")";?>
		</div>

		<div>
			<span class='negrita cursiva' data-translate-html="general.curso">Curso:</span> 
			<?php echo Texto::textoFormateado($alumno->titulo);?>
		</div>
	</div>

	<?php if($mensajes->num_rows > 0):?>
			<?php echo Calificaciones::listarMensajes($mensajes) ?>
	<?php else:?>
		<div data-translate-html="evaluaciones.no_mensaje">No ha publicado ning&uacute;n mensaje</div>
	<?php endif;?>
</div>

<script type="text/javascript" src="js-calificaciones-foro_mensajes.js"></script>