function changeTab(numTab)
{
	var n = numTab;

	$('.submenuTabs > div').removeClass('activeSubmenu');

	if(n == 1)
	{
		$('.tests').addClass('activeSubmenu');
		$('#tab2').addClass('hide');
		$('#tab3').addClass('hide');
		$('#tab4').addClass('hide');
		$('#tab1').removeClass('hide');
	}
	else if(n == 2)
	{
		$('.trabajosPracticos').addClass('activeSubmenu');
		$('#tab1').addClass('hide');
		$('#tab3').addClass('hide');
		$('#tab4').addClass('hide');
		$('#tab2').removeClass('hide');
	}
	else if(n == 3)
	{
		$('.speaking').addClass('activeSubmenu');
		$('#tab1').addClass('hide');
		$('#tab2').addClass('hide');
		$('#tab4').addClass('hide');
		$('#tab3').removeClass('hide');
	}
	else if(n == 4)
	{
		$('.foros').addClass('activeSubmenu');
		$('#tab1').addClass('hide');
		$('#tab2').addClass('hide');
		$('#tab3').addClass('hide');
		$('#tab4').removeClass('hide');
	}

}