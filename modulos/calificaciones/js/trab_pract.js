$(document).ready(function()
{	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#trabajos input[name="buscar"]').keyup(function(event)
	{
		trabPractBuscar();
	});
});

// BUSCADOR DE CURSOS INACTIVOS POR CONVOCATORIA
function trabPractBuscar()
{
	var wordSearch = $('#trabajos input[name="buscar"]').val();
	
	$('#imprimirTrabajosPrecticos tbody tr').addClass('hide');
	$('#imprimirTrabajosPrecticos tbody tr.filaTabla').removeClass('hide');
	
	if(wordSearch != "")
	{
		var inSearch = '';
		$('#imprimirTrabajosPrecticos tbody tr').each(function(index)
		{
			inSearch = $(this).find('.rojo_pastel').text() + ' ' + $(this).find('.rojo_pastel').text();
			
			if(inSearch != undefined)
			{
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('(' + wordSearch + ')', 'ig');
				if(inSearch.match(regex))
				{
					$(this).removeClass('hide');
				}
			}
		});
	}
	else
	{
		$('#imprimirTrabajosPrecticos tbody tr').removeClass('hide');
	}
}