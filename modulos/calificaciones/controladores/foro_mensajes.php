<?php
$get = Peticion::obtenerGet();
if(isset($get['idalumno']) && is_numeric($get['idalumno']))
{
	mvc::cargarModuloSoporte('secretaria');
	$objModeloSecretaria = new ModeloSecretaria();
	
	$alumno = $objModeloSecretaria->obtenerAlumno($get['idalumno'], Usuario::getIdCurso());
	if($alumno->num_rows > 0)
	{
		mvc::cargarModuloSoporte('foro');
		$objModeloForo = new modeloForo();
		
		$idforo_tema= 0;
	
		$alumno = $alumno->fetch_object();
		
		$mensajes = $objModeloForo->obtenerMensajesPorAlumno($get['idalumno'], Usuario::getIdCurso());
		
		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'foro_mensajes');
	}
}