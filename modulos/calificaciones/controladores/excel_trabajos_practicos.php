<?php

// Enviamos los encabezados de hoja de calculo
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=excel.xls");

require_once(PATH_ROOT . 'lib/numRomanos.php');

//Colores de fondo para excel
$colors = array('#FF9999', '#E6F59A', '#FFCCCC', '#FFCC99', '#FFFF99', '#99CCCC', '#CCFFFF');

// Cargo las librerias
$mi_alumno = new Alumnos();	
$objCurso = new Cursos();
$objModulo = new Modulos();
$objTB = new TrabajosPracticos();

//cargo el modulo de notas y obtengo la configuracion del curso
mvc::cargarModuloSoporte('notas');
$objNotas = new Notas();
	
$objModeloNotas = new ModeloNotas();
$ConfiguracionesPorcentajes = $objModeloNotas->obtenerConfigPorcentajes($_SESSION['idcurso']);
$confPorcentaje = mysqli_fetch_assoc($ConfiguracionesPorcentajes);

$confP = $confPorcentaje['num_trabajos'];
$percentTP = $confPorcentaje['trabajos_practicos'];


// Obtengo los datos del alumno en 2 arrays
$resultado = $mi_alumno->alumnos_del_curso($_SESSION['idcurso']);
while($fila_resultado = mysqli_fetch_assoc($resultado))
{
	$nombre = $fila_resultado['apellidos'].", ".$fila_resultado['nombre'];
	$nombre = Texto::quitarTildes($nombre);
	$alumno['nombre'][] = $nombre;
	$alumno['dni'][] = $fila_resultado['dni'];
	$alumno['id'][] = $fila_resultado['idalumnos'];
}
	
//INICIOS DE LA PAGINA EXCEL
echo "<table>";
	
	//INICIO PRIMERA FILA EXCEL
	echo "<tr>";
		echo "<td colspan='2' style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[0] . ";'>ALUMNOS</td>";
	
$modulosCurso = $objModulo->modulos_asignado_curso();
$numModulo=1;
$nRomans = '';
$nco = 0;

while($moduloCurso = mysqli_fetch_assoc($modulosCurso))
{
	$numColor = $numModulo;
	//Cuento el numero de archivos
	$objModulo->set_modulo($moduloCurso['idmodulo']);
	$objModulo->set_categoria_archivo('2');
	$trabajosPracticos = $objModulo->archivosModulosActivos();
	$nTrabajosPracticos = mysqli_num_rows($trabajosPracticos);
	$numTrabajosPracticos[] = $nTrabajosPracticos;
	
		echo "<td colspan='" . $nTrabajosPracticos . "' style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[$numColor] . ";'>MODULO " . $numModulo . "</td>";

	$aslugarNumeroRomano = 1;
	$nco++;
	while($trabajoPractico = mysqli_fetch_assoc($trabajosPracticos))
	{
		$numeroRo = NumRomanos::decimalRomano($aslugarNumeroRomano);
		$nRomans .= "<td style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[$nco] . ";'>" . $numeroRo . "</td>";
		$aslugarNumeroRomano++;
		$idTrabajosPracticos[] = $trabajoPractico['idarchivo_temario'];
		$colorNota[] = $nco;
	}
	$numModulo++;
}
		$colorMedia = $numModulo+1;
		echo "<td style='border-right:1px solid #000000;border-bottom:1px solid #000000;text-align:center;background-color:" .$colors[$colorMedia] . ";'>NOTA FINAL</td>";

	echo "</tr>";

	//INICO SEGUNDA FILA
	echo "<tr>";
	
		echo "<td style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[0] . ";'>NOMBRE</td>";
		echo "<td style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[0] . ";'>DNI</td>";
		echo $nRomans;
		echo "<td style='border-right:1px solid #000000;border-bottom:1px solid #000000;text-align:center;background-color:" .$colors[$colorMedia] . ";'>MEDIA</td>";
	
	echo "</tr>";
		
	for($i=0;$i<=count($alumno['nombre'])-1;$i++)
	{
		echo "<tr>";	
		
			$objNotas->setCurso($_SESSION['idcurso']);
			$notaMedia = $objNotas->calcularMediaTrabajosPract($alumno['id'][$i], $confP, $percentTP, false);
			
			echo "<td style='border-bottom:1px solid #000000;background-color:" . $colors[0] . ";'>" . $alumno['nombre'][$i] . "</td>";
			echo "<td style='border-bottom:1px solid #000000;background-color:" . $colors[0] . ";'>" . $alumno['dni'][$i] . "</td>";

			$numCo = $i+1;
			for($t=0;$t<=count($idTrabajosPracticos)-1;$t++)
			{
				$nota = $objTB->obtengoNota($alumno['id'][$i],$idTrabajosPracticos[$t]);
				if(!empty($nota))
				{
					echo "<td style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[$colorNota[$t]] . ";'>" . $nota . "</td>";		
				}
				else
				{
					echo "<td style='border-bottom:1px solid #000000;text-align:center;background-color:" . $colors[$colorNota[$t]] . ";'> - </td>";	
				}
							
			}
				
			echo "<td style='border-right:1px solid #000000;border-bottom:1px solid #000000;text-align:center;background-color:" .$colors[$colorMedia] . ";'>" . $notaMedia . "</td>";
		
		echo "</tr>";
	}

echo "</table>";


?> 
