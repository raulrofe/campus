<?php 

//Obtengo numero romanos para apartado trabajos practicos
require(PATH_ROOT . 'lib/numRomanos.php');

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

//cargo modelo de notas
mvc::cargarModuloSoporte('notas');
$objNotas = new ModeloNotas();
$objN = new Notas();

$mi_trabajopractico = new TrabajosPracticos();
$mi_modulo = new Modulos();
$mi_alumno = new Alumnos();

$idCurso = Usuario::getIdCurso();

//Obtengo la configuracion de las notas del curso
$configNotas = $objNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();


$alumnos = array();

if(isset($post['id_alumno']) && is_numeric($post['id_alumno']))
{
	$id_alumno = $post['id_alumno'];
}
else if(isset($get['id_alumno']) && is_numeric($get['id_alumno']))
{
	$id_alumno = $get['id_alumno'];
}
else
{
	$id_alumno = 0;
}

if(isset($id_alumno))
{
	if($id_alumno == 0)
	{
		$resultadoAlumno = $mi_alumno->alumnos_del_curso_activos($idCurso);	
	}
	else
	{
		$mi_alumno->set_alumno($id_alumno);
		$resultadoAlumno = $mi_alumno->ver_alumno();
		
		$idMartriculaAlumno = Usuario::getIdMatriculaDatos($id_alumno, $idCurso);
	}
}

//Los datos del curso
$mi_curso = new Cursos();
$mi_curso->set_curso($_SESSION['idcurso']);
$registro_curso = $mi_curso->buscar_curso();
if($registro_curso->num_rows == 1)
{		
	$dato_curso = mysqli_fetch_assoc($registro_curso);
}


//datos alumno
$resultado_alumnos = $mi_alumno->alumnos_del_curso_activos($idCurso);

$alumnos = null;

while($alumnos_curso = mysqli_fetch_assoc($resultado_alumnos))
{
	$alumnos['nombre'][]=$alumnos_curso['nombre'];
	$alumnos['apellidos'][]=$alumnos_curso['apellidos'];
	$alumnos['dni'][]=$alumnos_curso['dni'];
	$alumnos['ident'][]=$alumnos_curso['idalumnos'];
}

//Obtener el numero de test del curso
$numTest = $objNotas->obtenerNumTestCurso($idCurso);
if($numTest->num_rows > 0)
{
	$numTest = $numTest->fetch_object();
	$numTest = $numTest->numTestCurso;
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);
