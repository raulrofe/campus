<?php

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

mvc::cargarModuloSoporte('notas');
mvc::cargarModuloSoporte('foro');

//**************************************************************************
// CARGO LOS MODELOS
$idCurso = Usuario::getIdCurso();
$objModelo = new ModeloNotas();
$objNotas = new Notas();
$objAlumno = new Alumnos();
$objForo = new modeloForo();

//**************************************************************************
//OBTENGO LA CONFIGURACION DE NOTAS
$configNotas = $objModelo->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

//**************************************************************************
// ESTABLECEMOS EL VALOR A ID ALUMNO
if(isset($post['id_alumno']) && is_numeric($post['id_alumno']))
{
	$id_alumno = $post['id_alumno'];
}
else if(isset($get['id_alumno']) && is_numeric($get['id_alumno']))
{
	$id_alumno = $get['id_alumno'];
}
else
{
	$id_alumno = 0;
}

//***************************************************************************
// COJEMOS LOS ID DE LOS ALUMNOS A SACAR EL INFORME
{
	if($id_alumno == 0)
	{
		$alumnos = $objModelo->obtenerAlumnosActivos($idCurso);
	}
	else
	{
		$objAlumno->set_alumno($id_alumno);
		$alumnos = $objAlumno->ver_alumno();
	}
}

//***************************************************************************
// DATOS ALUMNO PARA EL SELECT
$resultado_alumnos = $objAlumno->alumnos_del_curso_activos($idCurso);

$dataAlumnos = null;

while($alumnos_curso = mysqli_fetch_assoc($resultado_alumnos))
{
	$dataAlumnos['nombre'][] = $alumnos_curso['nombre'];
	$dataAlumnos['apellidos'][] = $alumnos_curso['apellidos'];
	$dataAlumnos['dni'][] = $alumnos_curso['dni'];
	$dataAlumnos['ident'][] = $alumnos_curso['idalumnos'];
}

//****************************************************************************
// OBTENGO LOS TEMAS DEL FORO
$temaForo = array();
$temasForo = $objForo->obtenerTemas($idCurso);
$numTemasForo = $temasForo->num_rows;
if($numTemasForo > 0)
{
	while($tema = $temasForo->fetch_object())
	{
			$temaForo['nombreTema'][] = $tema->titulo;
			$temaForo['idTema'][] = $tema->idforo_temas;
	}
}

//*****************************************************************************
// CARGO LA VISTA
require_once mvc::obtenerRutaVista(dirname(__FILE__), 'foros');