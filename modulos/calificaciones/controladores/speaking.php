<?php
$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

//cargo modelo de notas
mvc::cargarModuloSoporte('notas');

//Inicializo los objetos
$objNotas = new ModeloNotas();
$objN = new Notas();
$mi_alumno = new Alumnos();
$mi_curso = new Cursos();

//Obtengo el Id y datos del curso
$idCurso = Usuario::getIdCurso();
$mi_curso->set_curso($_SESSION['idcurso']);
$registro_curso = $mi_curso->buscar_curso();
if($registro_curso->num_rows == 1)
{
	$dato_curso = mysqli_fetch_object($registro_curso);
}

//Obtengo la configuracion de las notas del curso
$configNotas = $objNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();


//Asigno las notas a la matricula
if(!empty($post)) {
	foreach($post['notaSpeaking'] as $key => $value) {
		if(!empty($value)) {
			//Busco la nota
			$notaSpeaking = $mi_curso->getNotaSpeaking($key);

			if($notaSpeaking->num_rows == 1) {
				//Actualizo nota del speaking
				$mi_curso->updateNotaSpeaking($key, $value);
			} else {
				//Inserto la nota del speaking
				$mi_curso->insertNotaSpeaking($key, $value);
			}
		} else {
			//Elimino el registro
			$mi_curso->deleteSpeakingCurso($key);
		}
	}
}

//Obtengo los speakings del curso
$notasSpeakings = array();
$speakings = $mi_curso->getSpeakingsCurso($idCurso);
if($speakings) {
	while($sp = mysqli_fetch_object($speakings)) {
		$notasSpeakings[$sp->idmatricula] = $sp->nota;
	}
}

if(isset($post['id_alumno']) && is_numeric($post['id_alumno']))
{
	$id_alumno = $post['id_alumno'];
}
else if(isset($get['id_alumno']) && is_numeric($get['id_alumno']))
{
	$id_alumno = $get['id_alumno'];
}

if(isset($id_alumno) && isset($idCurso))
{
	$mi_alumno->set_alumno($id_alumno);
	$resultadoAlumno = $mi_alumno->ver_alumno();
	$idMartriculaAlumno = Usuario::getIdMatriculaDatos($id_alumno, $idCurso);

} else {
	$resultadoAlumno = $mi_alumno->alumnos_del_curso_activos($idCurso);
}

//datos alumno
$resultado_alumnos = $mi_alumno->alumnos_del_curso_activos($idCurso);

//Obtener el numero de test del curso
$numTest = $objNotas->obtenerNumTestCurso($idCurso);
if($numTest->num_rows > 0)
{
	$numTest = $numTest->fetch_object();
	$numTest = $numTest->numTestCurso;
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'speaking');
