<?php

set_time_limit(0);
require_once(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');

mvc::cargarModeloAdicional('configuraciones', 'modelo', 'admin');
$objEntidadOrganizadora = new AModeloConfig();
$objCurso = new Cursos();

$idCurso = Usuario::getIdCurso();


$entidadOrganizadora = $objEntidadOrganizadora->obtenerConfig();
if($entidadOrganizadora->num_rows > 0)
{
	$entidadOrganizadora = $entidadOrganizadora->fetch_object();
}

if(isset($idCurso) && is_numeric($idCurso))
{
	$cursos = $objCurso->obtenerIGAFCurso($idCurso);
	$curso = $cursos->fetch_object();
}

// Cargo el contenido
ob_start();
include(PATH_ROOT . '/plantillas/pdf/cabecera_pdf.php');
$content = ob_get_clean();

// conversion HTML => PDF
try
{
	$html2pdf = new HTML2PDF('P', 'A4', 'fr', false, 'UTF-8');
								
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
									
	$html2pdf->Output(PATH_ROOT . 'archivos/certificados/4_diploma.pdf', 'F');
}

catch(HTML2PDF_exception $e)
{
	echo $e;
}
