<?php

$get = Peticion::obtenerGet();

require_once(PATH_ROOT."modulos/autoevaluaciones/autoevaluaciones.php");
mvc::cargarModuloSoporte('autoevaluaciones');
$objAutoevaluacion = new Autoevaluacion ();

/*
mvc::cargarModuloSoporte('notas');
$mis_notas = new ModeloNotas();
$mi_trabajopractico = new TrabajosPracticos();
$mi_modulo = new Modulos();
$mi_alumno = new Alumnos();
*/

if (isset($get['idCalificacion']) and is_numeric($get['idCalificacion']))
{
	$matrizRespuestas = array();
	$numRespuestas = 0;
	
	//respuestas del alumno
	$elecciones = $objAutoevaluacion->alumnosCalificacion($get['idCalificacion']);
	while($eleccion = mysqli_fetch_assoc($elecciones))
	{
		$matrizRespuestas['pregunta'][] = $eleccion['idPreguntas'];
		$matrizRespuestas['respuesta'][] = $eleccion['idRespuestas'];
	}
	
	if(isset($matrizRespuestas['pregunta']) && is_array($matrizRespuestas['pregunta']))
	{
		$numRespuestas = count($matrizRespuestas['pregunta']); // -1 _?
	}
	
	//datos calificacion - nota sacada
	$calificaciones = $objAutoevaluacion->datosCalificacion($get['idCalificacion']);
	$calificacion = mysqli_fetch_assoc($calificaciones);
	
	//establecemos el id de autoevaluacion
	$objAutoevaluacion->set_autoeval($calificacion['idautoeval']);
	
	//datos Autoevaluacion
	$Autoevaluaciones = $objAutoevaluacion->buscar_autoeval();
	$autoevaluacion = mysqli_fetch_assoc($Autoevaluaciones);
	
	//preguntas
	$preguntas = $objAutoevaluacion->preguntas();
	
	
	//obtenemos el alumno y el curso de la calificacion
	$Matriculas = $objAutoevaluacion->alumnoCursoCalificacion($get['idCalificacion']);
	$matricula = mysqli_fetch_assoc($Matriculas);
	
	$numPregunta=1;
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);	
}