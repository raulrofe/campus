<?php

$get = Peticion::obtenerGet();

require_once(PATH_ROOT."modulos/autoevaluaciones/autoevaluaciones.php");
mvc::cargarModuloSoporte('autoevaluaciones');
$objAutoevaluacion = new Autoevaluacion ();

if (isset($get['idCalificacion']) and is_numeric($get['idCalificacion'])){
	$objAutoevaluacion->eliminarIntentoParticipante($get['idCalificacion']);
}

echo 'El intento de Autoevaluación ha sido eliminada correctamente';