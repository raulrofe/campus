<?php 
//Cargo la libreria para convertir html en pdf
require(PATH_ROOT . 'lib/htmlpdf/html2pdf.class.php');


$html2Pdf = new HTML2PDF();
$objAlumnos = new Alumnos();
$objUsuario = new Usuarios();
$objCurso = new Cursos();
$objTrabajos = new TrabajosPracticos();

mvc::cargarModuloSoporte('notas');
$objNotas = new ModeloNotas();
$objN = new Notas();

mvc::cargarModuloSoporte('foro');
$objForo = new modeloForo();

$get = Peticion::obtenerGet();

//var_dump($get);

$idCurso = Usuario::getIdCurso();

$temasForo = $objForo->obtenerTemas($idCurso);
$numTemasForo = $temasForo->num_rows;

$objN->setCurso($idCurso);

//Obtengo la configuracion de las notas del curso
$configNotas = $objNotas->obtenerConfigPorcentajes($idCurso);
$configNotas = $configNotas->fetch_object();

$portadaPdf = array();

// Datos PORTADA PDF INFORME curso, accion formativa y grupo, tutores, tutorias y empresa organizadora
$datosCurso = $objCurso->obtenerIGAFCurso($idCurso);
if($datosCurso->num_rows > 0)
{
	$curso = $datosCurso->fetch_object();

	/*
	//var_dump($curso);exit;
	$portadaPdf['horasCurso'] = $curso->n_horas;
	$portadaPdf['nombreCurso'] = $curso->titulo;
	$portadaPdf['inicioCurso'] = $curso->f_inicio;
	$portadaPdf['finCurso'] = $curso->f_fin;
	$portadaPdf['accionFormativaCurso'] = $curso->accion_formativa;
	$portadaPdf['grupoCurso'] = $curso->codigoIG;
	*/

	//Obtengo las tutorias
	$tutorias = $objCurso->obtenerTutoriaCursoTripartita($idCurso);

	if($tutorias->num_rows > 0)
	{
		$tutoria = $tutorias->fetch_object();

		$portadaPdf['tutoriaCurso'] = $tutoria->dias_tutoria . ' de ' . $tutoria->hora_inicio . ' a ' . $tutoria->hora_fin;

		
	}

			//Entidad Organizadora
		$entidadOrganizadora = $objCurso->getEntidadOrganizadora();
		if($entidadOrganizadora->num_rows == 1)
		{
			$entidadOrganizadora = $entidadOrganizadora->fetch_object();	
			$portadaPdf['nombreEntidad'] = $entidadOrganizadora->nombre_entidad;
			$portadaPdf['cifEntidad'] = $entidadOrganizadora->cif;
			$portadaPdf['codigoEntidad'] = '115-00';
		}

	//Obtengo los datos de los staff del curso
	$staff = $objCurso->obtenerTutoresCurso($idCurso);
	
	//Los datos del staf
	if($staff->num_rows > 0)
	{
		$contut = 0;
		while($rh = $staff->fetch_object())
		{
			if($rh->status == 'coordinador')
			{
				$portadaPdf['dniCoordinador'] = $rh->nif;		
				$portadaPdf['nombreCoordinador'] = $rh->nombrec;	
			}
			else if($rh->status == 'tutor1' OR $rh->status == 'tutor2' OR $rh->status == 'tutor3')
			{
				$portadaPdf['tutores'][$contut]['nombreTutor'] = $rh->nombrec;
				$portadaPdf['tutores'][$contut]['dniTutor'] = $rh->nif;	
				$contut++;	
			}
		}
	}
}

//Array para generar los informes en pdf
$dataPdf = array();

$informeAlumnos = array();

//meto en un array los id de los alumnos de los que vamos a sacar los informes
if(isset($get['idAlumno']) && is_numeric($get['idAlumno']))
{
	if($get['idAlumno'] == 0)
	{
		$idAlumnos = $objAlumnos->alumnos_del_curso_activos($idCurso);
		if($idAlumnos->num_rows > 0)
		{
			while($alumnos = $idAlumnos->fetch_object())
			{
				$informeAlumnos[] = $alumnos->idalumnos;
			}
		}		
	}	
	else
	{
		$informeAlumnos[] = $get['idAlumno'];	
	}

	//Genero los array para los pdf
	if(count($informeAlumnos) > 0)
	{
		$datosAlumno = array();
		
		for($i=0; $i<=count($informeAlumnos)-1;$i++)
		{
			//Obtner el id de matricula del alumno
			$idMatriculacion = Usuario::getIdMatriculaDatos($informeAlumnos[$i], $idCurso);
			$datosAlumno = $objUsuario->obtenerMatriculaInforme($idMatriculacion);
			if($datosAlumno->num_rows > 0)
			{		

				//Nota media de los foros
				$foroPuntosReduce = $objN->calcularNotaMediaForo($informeAlumnos[$i], $idCurso, true);
				$foroPuntos = $objN->calcularNotaMediaForo($informeAlumnos[$i], $idCurso, false);
				//Porcentaje foro
				$numTemas = $objNotas->obtenerNumTemas($idCurso);
				$numTemas = $numTemas->num_rows;
				
				//El numero de temas en los que ha participado el alumno
				$temas = $objNotas->obtenerTemas($informeAlumnos[$i], $idCurso);
				$numTemasParticipado = $temas->num_rows;
				
				$porcentajeForo = ($numTemasParticipado * 100) / $numTemas; 		

				//Nota media de los trabajos practicos
				$notaTPReduce = $objN->calcularMediaTrabajosPract($informeAlumnos[$i], $configNotas->num_trabajos, $configNotas->trabajos_practicos, true);
				$notaTP = $objN->calcularMediaTrabajosPract($informeAlumnos[$i], $configNotas->num_trabajos, $configNotas->trabajos_practicos, false);
				//Porcentaje trabajos practicos
				$numTrabajos = $objTrabajos->obtenerNumTrabajosPracticosCurso($idCurso);
				if($numTrabajos->num_rows > 0)
				{
					$numTrabajos = $numTrabajos->fetch_object();
					$numTrabajos = $numTrabajos->numTrabajosPracticos;
				}
				$trabajosParticipado = $objTrabajos->obtenerNotaTrabajosPracticosAlumno($informeAlumnos[$i], $idCurso);
				$trabajosParticipado = $trabajosParticipado->num_rows;
				
				$porcentajeTrabajos = ($trabajosParticipado * 100) / $numTrabajos;
				
				//Nota media autoevaluaciones
				$mediaTestsReduce = $objN->calcularNotaMediaAutoeval($informeAlumnos[$i], $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, true);
				$mediaTests = $objN->calcularNotaMediaAutoeval($informeAlumnos[$i], $idCurso, $configNotas->autoevaluaciones, $configNotas->tipo_autoeval, false);
				//Porcentaje autoevaluaciones
				$numTest = $objNotas->obtenerNumTestCurso($idCurso);
				if($numTest->num_rows > 0)
				{
					$numTest = $numTest->fetch_object();
					$numTest = $numTest->numTestCurso;
				}
				
				$numTestAlumno = $objNotas->obtenerNumTestAlumno($informeAlumnos[$i]);
				$numTestParticipado = $numTestAlumno->num_rows;
				
				$porcentajeTest = ($numTestParticipado * 100) / $numTest;
				
				//Porcentaje general del curso
				$porcentajeTotal = ($porcentajeTrabajos + $porcentajeTest + $porcentajeForo) / 3;
				
				//Obtener la nota media general
				$notaMediaFinal = $foroPuntosReduce + $notaTPReduce + $mediaTestsReduce;
				
				// Obtener el tiempo total que el alumno lleva invertido en el curso
				$matriculaAlumno = $datosAlumno->fetch_object();
				
				//Datos alumno
				$dataPdf[$i]['nombreAlumno'] = $matriculaAlumno->nombre;
				$dataPdf[$i]['apellidosAlumno'] = $matriculaAlumno->apellidos;			
				$dataPdf[$i]['nif'] = $matriculaAlumno->dni;
				$dataPdf[$i]['email'] = $matriculaAlumno->email;
				$dataPdf[$i]['telefonoAlumno'] = $matriculaAlumno->telefono;
				$dataPdf[$i]['fechaMatriculacion'] = $matriculaAlumno->fecha;
				$dataPdf[$i]['fechaPrimeraConexion'] = $matriculaAlumno->fecha_conexion;
				$dataPdf[$i]['fechaUltimaConexion'] = $matriculaAlumno->fecha_conexion_actual;
				$dataPdf[$i]['notaMediaFinal'] = $notaMediaFinal;
				$dataPdf[$i]['centroAlumno'] = $matriculaAlumno->nombre_centro;
				$dataPdf[$i]['cifCentroAlumno'] = $matriculaAlumno->cif;				
				if(is_float($porcentajeTotal))
				{
					$dataPdf[$i]['completado'] = number_format($porcentajeTotal, '2', '.', '');
				}
				else
				{
					$dataPdf[$i]['completado'] = $porcentajeTotal;
				}
				
				$fechaLogsAcceso = $objUsuario->obtenerTiempoConexion($matriculaAlumno->idmatricula);

				$totalSegundos = 0;
				if($fechaLogsAcceso->num_rows > 0)
				{
					while($fechaLog = $fechaLogsAcceso->fetch_object())
					{	
						if($fechaLog->fecha_entrada && $fechaLog->fecha_salida)		
						{			
							$inicio = strtotime($fechaLog->fecha_entrada);
							$fin = strtotime($fechaLog->fecha_salida);
	
							$diferencia = $fin - $inicio;
							
							$totalSegundos = $totalSegundos + $diferencia;
						}
						
						else
						{
							$totalSegundos = $totalSegundos + 5;	
						}
					}
				}
				
				$tiempoConexion = Fecha::formatearSegundosHoras($totalSegundos);
				$dataPdf[$i]['tiempoTotal'] = $tiempoConexion;
								
				//****************************************************************************
				// OBTENGO LOS TEMAS DEL FORO
				$temasForo = $objForo->obtenerTemas($idCurso);
				if($temasForo->num_rows > 0)
				{
					$cont = 0;
					while($tema = $temasForo->fetch_object())
					{
						$dataPdf[$i]['foro'][$cont]['nombreTema'] = $tema->titulo;
						$dataPdf[$i]['foro'][$cont]['idTema']= $tema->idforo_temas;
						
						$mensajesForo = $objForo->obtenerMensajesTemasParticipadoPorAlumno($informeAlumnos[$i], $idCurso, $tema->idforo_temas); 
						if($mensajesForo->num_rows > 0)
						{		
							$mensajeForo = $mensajesForo->fetch_object();
							$mensajesAlumnoTema = $mensajeForo->nMensajesTemasParticipados;
						}
						else
						{
							$mensajesAlumnoTema = 0;	
						}
						
						$dataPdf[$i]['foro'][$cont]['nMensajes'] = $mensajesAlumnoTema;
						$dataPdf[$i]['foro'][$cont]['notaMediaForo'] = $foroPuntos;
						$cont++;	
					}
				}
				
				//var_dump($dataPdf);exit;
				//Agrego los datos del curso al array

			}
		}
	}
	
	// Una vez terminado el proceso de creacion del array para generar el pdf nos ponemos a generarlo
	// Recuperacion del contenido HTML
 	ob_start();
 	include(PATH_ROOT . 'modulos/calificaciones/vistas/template/informe_foros.php');
	$content = ob_get_clean();

	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
		//$html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content);
		
		$html2pdf->Output();
	}
	catch(HTML2PDF_exception $e) {echo $e;}
	
}


