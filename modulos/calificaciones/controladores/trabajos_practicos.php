<?php

require(PATH_ROOT . 'lib/numRomanos.php');

$get = Peticion::obtenerGet();
$post = Peticion::obtenerPost();

//pongo los distintos tipos de clase para crear fondos de colores
$colores = array('amarillo','manzana','rosa','amarillo','amarillo','manzana','rosa','amarillo','manzana','rosa','amarillo','manzana','rosa');

//cargo el modulo de notas y obtengo la configuracion del curso
mvc::cargarModuloSoporte('notas');
$mis_notas = new ModeloNotas();
$resultado_configuracion = $mis_notas->obtenerConfigPorcentajes($_SESSION['idcurso']);
$configuracion = mysqli_fetch_assoc($resultado_configuracion);

// cargo clase base de notas
$objNotas = new Notas();
$objNotas->setCurso($_SESSION['idcurso']);


//Los datos del curso
$idCurso = Usuario::getIdCurso();

$mi_curso = new Cursos();
$mi_curso->set_curso($_SESSION['idcurso']);
$registro_curso = $mi_curso->buscar_curso();
if($registro_curso->num_rows == 1)
{		
	$dato_curso = mysqli_fetch_assoc($registro_curso);
}

$mi_trabajopractico = new TrabajosPracticos();
$mi_modulo = new Modulos();
$mi_alumno = new Alumnos();

//Buscamos un alumno segun el buscador
if(isset($post['buscar']) && !empty($post['buscar']))
{
	
	$resultado_alumnos = $mi_alumno->alumnos_del_curso_buscado($_SESSION['idcurso']);
	/*if(mysqli_num_rows($resultado_alumnos) == 0)
	{
		$resultado_alumnos = $mi_alumno->alumnos_del_curso($_SESSION['idcurso']); //en caso de que no encuentre usuario los busco todos de nuevo
	}*/
}
else 
{
	$resultado_alumnos = $mi_alumno->alumnos_del_curso_activos($_SESSION['idcurso']);
}

if(!empty($post) && !isset($post['buscar']))
{
	foreach($post as $key => $valor){

		if((is_numeric($valor)) && ($valor >= 0) && ($valor <= 10)) {

			$idents = explode("_",$key);
			$mi_trabajopractico->insertarNotaTrabajo($idents,$valor);

			//Inserto la nota en la parte de zona de entrega al tutor
			list($idCursoNota, $idUsuarioNota, $idArchivoTemarioNota) = explode("_", $key);

			// Obtengo el ID de matricula
			$idMatriculaNota = Usuario::getIdMatriculaDatos($idUsuarioNota, $idCursoNota);

			$actividad = $mi_trabajopractico->obtengoActividadMatricula($idCursoNota, $idMatriculaNota, $idArchivoTemarioNota);

			if($actividad->num_rows > 0) {
				$actividad = $actividad->fetch_object();

				$idActividad = $actividad->id;
				
				//Actualizo nota de la actividad
				$mi_trabajopractico->actualizarNotaActividad($idActividad, $valor);
			}
		}

		if(empty($valor)) {

			$idents = explode("_",$key);
			$mi_trabajopractico->eliminarNotaTrabajo($idents,$valor);	
		}
	}
}

$modulos = array();
$numero_archivos = array();
$alumnos = array();
$archivos = array();
$num_array = 0;

$resultado_modulos = $mi_modulo->modulos_asignado_curso();
$numero_modulos = mysqli_num_rows($resultado_modulos);

while($modulos_curso = mysqli_fetch_assoc($resultado_modulos))
{
	$modulos[] = $modulos_curso['nombre'];
	
	$mi_modulo->set_modulo($modulos_curso['idmodulo']);
	$mi_modulo->set_categoria_archivo('2');
	$resultado_archivos = $mi_modulo->archivosModulosActivos();
	$numero_archivos[] = mysqli_num_rows($resultado_archivos);
	
	while($archivos_modulo = mysqli_fetch_assoc($resultado_archivos))
	{
		//var_dump($archivos_modulo['idarchivo_temario']);
		//echo $num_array."<br/>";
		$archivos[$num_array][] = $archivos_modulo['idarchivo_temario'];
	}
	
	$num_array++;
}

if($resultado_alumnos->num_rows > 0)
{
	while($alumnos_curso = mysqli_fetch_assoc($resultado_alumnos))
	{
		$alumnos['nombre'][]=$alumnos_curso['nombre'];
		$alumnos['apellidos'][]=$alumnos_curso['apellidos'];
		$alumnos['dni'][]=$alumnos_curso['dni'];
		$alumnos['ident'][]=$alumnos_curso['idalumnos'];
	}
}

//Obtener el numero de trabajos practicos de un curso
$numTrabajos = $mi_trabajopractico->obtenerNumTrabajosPracticosCurso($idCurso);
if($numTrabajos->num_rows > 0)
{
	$numTrabajos = $numTrabajos->fetch_object();
	$numTrabajos = $numTrabajos->numTrabajosPracticos;
}

require_once mvc::obtenerRutaVista(dirname(__FILE__), $get['c']);
