<?php
class Calificaciones
{	
	private function __construct() {}
	
	public static function listarMensajes($mensajes)
	{
		$modeloForo = new modeloForo();
		
		$idcurso = Usuario::getIdCurso();
		$html = '';
		$cont=0;
		$nRespuesta = 0;
		$idforo_tema = 0;
		
		while($mensaje = $mensajes->fetch_object())
		{
			//var_dump($mensaje);
			//echo '<br>';
			if($idforo_tema != $mensaje->idforo_temas)
			{
				$numMensajesTemasAlumno = $modeloForo->obtenerMensajesTemasParticipadoPorAlumno($mensaje->idusuario, $idcurso, $mensaje->idforo_temas);
				$numMensajesTemasAlumno = $numMensajesTemasAlumno->fetch_object();
				$numMensajesTemasAlumno = $numMensajesTemasAlumno->nMensajesTemasParticipados;
				
				$numMsgForo = $modeloForo->obtenerMensajesTemas($idcurso, $mensaje->idforo_temas);
				$numMsgForo = $numMsgForo->fetch_object();
				$numMsgForo = $numMsgForo->nMensajesTemas;
				
				$html .= '<h2 class="elementoTemaClick">' . Texto::textoPlano($mensaje->tituloTema) . '</h2>';
				$html .= '<div class="elementoCalfForoMsg"><b>Participaci&oacute;n:</b> ' . $numMensajesTemasAlumno . ' de ' .  $numMsgForo . '</div>';
				$idforo_tema = $mensaje->idforo_temas;
			}
			
			$cont++;
			
			$html .= self::pintarUnMensaje($mensaje, false, $cont, $nRespuesta);
			
		}
		
		return $html;
	}
	
	public static function pintarUnMensaje($mensaje, $msg_respuesta = false, $cont, $nRespuesta)
	{
		$addClass = '';
		$addId = '';
		if($msg_respuesta)
		{
			$addClass =  'un_mensaje_respuesta respuestaForo';
			$addId = "id='ocultarRespuesta". $cont ."'";
		}
		else
		{
			$addClass = 'beige';
		}
		
		if(isset($mensaje->t_foto))
		{
			$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
			$foto = $mensaje->t_foto;
		}
		else
		{
			$nombrec = $mensaje->a_nombrec;
			$foto = $mensaje->a_foto;
		}
	
		$html = '<div '. $addId .' class="elemento ' . $addClass . '">' .
		/*	'<div class="elementoAvatar"><img src="imagenes/fotos/' . $foto. '" alt="" /></div>' .*/
			'<div class="elementoAll">' .
				'<div class="elementoNombreForo fleft"><p>' . Texto::textoFormateado($nombrec) . '</p></div>' .
				'<div class="fechaForo fright">' . Fecha::obtenerFechaFormateada($mensaje->fecha) . '</div>' .
				'<div class="clear"></div>' .
				'<div class="elementoContenido">' .
					'<span class="contenidoRespuesta">' . html_entity_decode(Texto::bbcode($mensaje->contenido)) . '</span></p>' .
				'</div>' .
				 '<div class="elementoPie optionsForo">' .
					'<ul class="elementoListOptions fright">';
						if(!$msg_respuesta && $nRespuesta > 0)
						{
							$html.= '<li class="galleta"><a href="#" onclick="desplegarRespuestas('.$cont.')" title="Ver respuestas"><span class="textGalleta">'
								. $nRespuesta .'</span><span>Respuestas</span></a></li>';
						}
			$html .= '</ul>' .
					'<div class="clear"></div>' .
					'</div>' .
				'</div>' .
				'<div class="clear"></div>' .
			'</div>' .
			'<div class="clear"></div>';

		return $html;
	}
}