<?php
class TrabajosPracticos extends modeloExtend
{
	public static function obtener_numero_romano($numero)
	{
		switch ($numero)
		{
			case '1':
				$romano = 'I';
				break;
			case '2':
				$romano = 'II';
				break;
			case '3':
				$romano = 'III';
				break;
			case '4':
				$romano = 'IV';
				break;
			case '5':
				$romano = 'V';
				break;
			case '6':
				$romano = 'VI';
				break;
			case '7':
				$romano = 'VII';
				break;
			case '8':
				$romano = 'VIII';
				break;
			case '9':
				$romano = 'IX';
				break;
			case '10':
				$romano = 'X';
				break;
		}
		return $romano;
	}
	
	public function insertarNotaTrabajo($array,$nota)
	{
		$fecha = date("Y-m-d H:i");
		$sql="SELECT * 
		from calificacion_trabajo_practico 
		where idcurso = ".$array[0].
		" and idalumnos = ".$array[1].
		" and idarchivo_temario = ".$array[2];
		$resultado = $this->consultaSql($sql);
		
		if(mysqli_num_rows($resultado) == 0)
		{
			$sql = "INSERT into calificacion_trabajo_practico 
			(idcurso,idalumnos,idarchivo_temario,fecha,nota_trabajo)
			VALUES ('$array[0]','$array[1]','$array[2]','$fecha','$nota')";
		}
		
		else
		{
			$sql = "SELECT * from calificacion_trabajo_practico
			where nota_trabajo = '$nota'
			and idcurso = ".$array[0]." 
			and idalumnos = ".$array[1]." 
			and idarchivo_temario = ".$array[2];
			$comprobacionNota = $this->consultaSql($sql);
			
			if(mysqli_num_rows($comprobacionNota) == 0 )
			{		
				$sql = "UPDATE calificacion_trabajo_practico 
				SET fecha = '$fecha',
				nota_trabajo = '$nota' 
				where idcurso = ".$array[0]." and idalumnos = ".$array[1]." and idarchivo_temario = ".$array[2];
				//echo $sql;
			}
		}
		
		$this->consultaSql($sql);
	}
	
	public function eliminarNotaTrabajo($array,$nota)
	{
		$fecha = date("Y-m-d");
		$sql="SELECT * 
		from calificacion_trabajo_practico 
		where idcurso = ".$array[0].
		" and idalumnos = ".$array[1].
		" and idarchivo_temario = ".$array[2];
		$resultado = $this->consultaSql($sql);
		
		if(mysqli_num_rows($resultado) > 0)
		{
			$fila = mysqli_fetch_assoc($resultado);
			$consulta_eliminar = "DELETE from calificacion_trabajo_practico 
			where idcalificacion_trabajo_practico = ".$fila['idcalificacion_trabajo_practico'];
			//echo $consulta_eliminar;
			$this->consultaSql($consulta_eliminar);
		}		
	}
	
	public function obtengoNota($idalumno,$idarchivo)
	{
		$sql = "SELECT nota_trabajo from calificacion_trabajo_practico  where idcurso = ".$_SESSION['idcurso']." and idalumnos = ".$idalumno." and idarchivo_temario = ".$idarchivo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$fila = mysqli_fetch_assoc($resultado);
		return $fila['nota_trabajo'];
	}	
	
	public function datosAutoevaluacion($idautoevaluacion)
	{
		$sql = "SELECT * from autoeval where idautoeval = ".$idautoevaluacion." ORDER BY titulo_autoeval ASC";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Obtengo el numero de trabajos practicos de un curso
	public function obtenerNumTrabajosPracticosCurso($idCurso)
	{
		$sql = "SELECT COUNT(at.idarchivo_temario) as numTrabajosPracticos" .
		" FROM archivo_temario AS at" .
		" WHERE at.idcurso = " . $idCurso .
		" AND at.borrado = 0" .
		" AND at.idcategoria_archivo = 2" .
		" AND at.activo = 1";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	//Obtengo la nota de los trabajos practicos por alumno
	public function obtenerNotaTrabajosPracticosAlumno($idAlumno, $idCurso)
	{
		$sql = "SELECT ctp.nota_trabajo, ctp.fecha, at.titulo" .
		" FROM calificacion_trabajo_practico AS ctp" .
		" LEFT JOIN archivo_temario AS at ON at.idarchivo_temario = ctp.idarchivo_temario" .
		" WHERE ctp.idcurso = " . $idCurso .
		" AND ctp.idalumnos = " . $idAlumno .
		" AND at.borrado =  0" .
		" AND at.activo = 1" .
		" AND idcategoria_archivo = 2";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	public function obtengoActividadMatricula($idCurso, $idMatricula, $idArchivoTemario) {

		$sql = "SELECT id" .
		" FROM actividades" .
		" WHERE idarchivo_temario = " . $idArchivoTemario .
		" AND idcurso = " . $idCurso .
		" AND idmatricula =" . $idMatricula;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		

	}

	public function actualizarNotaActividad($id, $nota){
		$sql = " UPDATE actividades" . 
		" SET nota = '$nota'" .
		" WHERE id = " . $id;

		$resultado = $this->consultaSql($sql);	

		return $resultado;	
	}

}