<?php

$get = Peticion::obtenerGet();

// Cargo el modelo
$objModelo = new TrabajosPracticosTutor();

$actividades = $objModelo->obtenerMisActividades($get['idMatricula']);


if($actividades->num_rows == 0) {
	$html = '<p>No se han subido actividades</p>';
} else {

	$html = '<table class="width100_nomargin">';
	$html .= '<tr>
				<th>Actividad</th>
				<th>Título</th>
				<th>Documento</th>
				<th>Anotación</th>
				<th>fecha</th>
				<th>estado</th>
				<th class="t_center">nota</th>
			</tr>';


	while($actividad = $actividades->fetch_assoc()) {

		if($actividad['estado'] == 1) {

			$actividadEstado = 'Enviada'; 

		} elseif($actividad['estado'] == 2) {

			$actividadEstado = 'Revisada'; 
		}

		$html .= '<tr>
					<td class="paddingTdTable">' . $actividad['tituloTrabajo'] . '</td>
					<td class="paddingTdTable">' . $actividad['titulo'] . '</td>
					<td class="paddingTdTable">
						<a href="archivos/actividades/' . $actividad['archivo'] . '" target="_blank">' . $actividad['archivo'] . '</a>
					</td>
					<td class="paddingTdTable">' . $actividad['anotacion'] . '</td>
					<td class="paddingTdTable">' . date('d/m/Y', strtotime($actividad['fecha'])) . '</td>
					<td class="paddingTdTable">' . $actividadEstado . '</td>
					<td class="t_center paddingTdTable">' . $actividad['nota'] . '</td>
				</tr>';
	}

	$html .= '</table';
}

echo json_encode(array('html' => $html));

