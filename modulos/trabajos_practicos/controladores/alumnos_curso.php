<?php

$get = Peticion::obtenerGet();

// Cargo el modelo
$objModelo = new TrabajosPracticosTutor();

$alumnos = $objModelo->obtenerMatriculasCurso($get['idCurso']);


$html = '<option value=""> - Selecciona un alumno - </option>';

while($alumno = $alumnos->fetch_object()) {
	$html .= '<option value="' . $alumno->idmatricula . '">' . $alumno->nombre . ' ' . $alumno->apellidos . '</option>';
}

echo json_encode(array('html' => $html));
