<?php

//Conexion a base de datos
$db = new database();
$con = $db->conectar();

// OBtengo el id del curso
$idCurso = Usuario::getIdCurso();

// Obtengo el id del usuario
$idUsuario = Usuario::getIdUser();

// Obtengo el id de matricula
$idMatricula = Usuario::getIdMatricula();

// Cargo el modelo
$objModelo = new TrabajosPracticosTutor();

// Modelo de correo
mvc::cargarModuloSoporte('gestor_correos');
$mi_correo = new Gestor_correos();

// Obtengo los trabjos practicos para mostrarlo en la vista
$trabajosPracticos = $objModelo->getTrabajosPracticosCurso($idCurso); 


if(Peticion::isPost()){
	$post = Peticion::obtenerPost();

	if(Usuario::compareProfile('alumno')){

		// Compruebo si existe el titulo
		if(!empty($post['titulo'])){

			// Compruebo si existe el archivo adjunto
			if(!empty($_FILES['actividad']['tmp_name']) && is_uploaded_file($_FILES['actividad']['tmp_name'])){

				$nombreArchivo = uniqid() . '.' . Fichero::obtenerExtension($_FILES['actividad']['name']);

				// Si el archivo se ha movido correctamente
				if(move_uploaded_file($_FILES['actividad']['tmp_name'], PATH_ROOT . 'archivos/actividades/' . $nombreArchivo)){

					// Insertamos el archivo
					$objModelo->insertarActividades($post['titulo'], $post['anotacion'], $nombreArchivo, 1, $post['idTrabajoPractico'], $idCurso, $idMatricula);

					$cuerpoMail  = $post['anotacion'];
					$cuerpoMail .= '<br/> ## Uno de tus alumnos ha realizado y enviado un trabajo práctico ##';
					//Enviamos mensaje al tutor al correo
					//Insertamos el correo y obtenemo su id		
					$mi_correo->insertarCorreo($post['titulo'], $cuerpoMail, $idCurso, 0);
					$ultimoIdCorreo = $mi_correo->obtenerUltimoIdInsertado();

					// Obtengo la fecha actual
					$fecha = Fecha::fechaActual();

					// Obtengo el id del tutor principal
					$idTutor = $objModelo->obtenerIdTutorPrincipal($idCurso)->fetch_object();

					// Insertar destinatario
					if($mi_correo->insertarDestinatario($idUsuario, 't_' . $idTutor->idrrhh, $fecha, $ultimoIdCorreo)) {
						$ultimoIdDestinatario = $mi_correo->obtenerUltimoIdInsertado();

				    	$mi_correo->insertarFicheroAdjunto($post['titulo'], $nombreArchivo, $ultimoIdCorreo);
						$maxfa = $mi_correo->obtenerUltimoIdInsertado();
						$mi_correo->adjuntarArchivoCorreo($ultimoIdCorreo, $maxfa);
					}

					// Guardamos mensaje de alerta
					Alerta::guardarMensajeInfo("La actividad ha sido subida correctamente");
				}
			} else {
				
				Alerta::guardarMensajeInfo("Debes adjuntar un archivo para poder enviar tu trabajo práctico");
			}
		} else {

			Alerta::guardarMensajeInfo("Debes indicar un titulo para tu trabajo práctico");
		}

	} else {

		// Compruebo si existe el titulo
		if(!empty($post['titulo'])){

			$notaTrabajo = null;

			// Compruebo si existe el archivo adjunto
			if(!empty($_FILES['actividad']['tmp_name']) && is_uploaded_file($_FILES['actividad']['tmp_name'])){

				$nombreArchivo = uniqid() . '.' . Fichero::obtenerExtension($_FILES['actividad']['name']);

				// Si el archivo se ha movido correctamente
				if(move_uploaded_file($_FILES['actividad']['tmp_name'], PATH_ROOT . 'archivos/actividades/' . $nombreArchivo)){

					// Insertamos el archivo
					$objModelo->insertarActividades($post['titulo'], $post['anotacion'], $nombreArchivo, 2, $post['idTrabajoPractico'], $post['idCurso'], $post['idAlumno']);

					//Actualizamos la nota
					$idAlumno = Usuario::getIdUsuarioDesdeMatricula($post['idAlumno']);
					$notaTrabajo = $objModelo->obtengoNota($idAlumno, $post['idTrabajoPractico'], $post['idCurso']);


					$actividadBuscar = $objModelo->obtengoActividadMatricula($post['idCurso'], $post['idAlumno'], $post['idTrabajoPractico']);

					if($actividadBuscar->num_rows == 1 && $notaTrabajo) {

						$actividadBuscar = $actividadBuscar->fetch_object();

						$objModelo->actualizarNotaActividad($actividadBuscar->id, $notaTrabajo);

					}
				}
			}
		}
	}	
}


if(Usuario::compareProfile('alumno')) { 

	// Obtengo mis trabajos practicos para mostrarlos en la vista
	$actividades = $objModelo->obtenerMisActividades($idMatricula);

	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'zona_tutor');	

} else {

	// Obtengo los cursos en los que participa el tutor
	$cursosTutor = $objModelo->obtenerCursosTutor($idUsuario);

	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'zona_tutor_revision');	
}


