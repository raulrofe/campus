<?php

//Conexion a base de datos
$db = new database();
$con = $db->conectar();

$mi_curso = new Cursos($con);
$mi_modulo = new Modulos($con);

$mi_curso->set_curso($_SESSION['idcurso']);
$registros_modulos = $mi_modulo->modulos_asignado_curso($_SESSION['idcurso']);
	
$get = Peticion::obtenerGet();

if(isset($get['vista']) && $get['vista'] == 'listado')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'trabajos_practicos');	
}
else if(isset($get['vista']) && $get['vista'] == 'grafica')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'trabajos_practicos');	
	//require_once mvc::obtenerRutaVista(dirname(__FILE__), 'trabajos_practicos_libreria');	
}
