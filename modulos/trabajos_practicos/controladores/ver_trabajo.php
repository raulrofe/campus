<?php
$get = Peticion::obtenerGet();

$mi_modulo = new Modulos();

if(isset($get['id_archivo']) && is_numeric($get['id_archivo']))
{
	$mi_modulo->set_archivo($get['id_archivo']);
	$rowAdjunto = $mi_modulo->buscar_archivo(Usuario::getIdCurso());
	if($rowAdjunto->num_rows > 0)
	{
		$rowAdjunto = $rowAdjunto->fetch_array();

		require_once mvc::obtenerRutaVista(dirname(__FILE__), 'trabajo_practico_ver');
	}
}
else
{
	Url::lanzar404();
}