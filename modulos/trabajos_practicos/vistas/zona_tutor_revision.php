<div class="caja_separada">
	<div id='trabajosPracticos'>
		<br/>
		
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/trabajos_practicos/trabajos-practicos.png" alt="" style="vertical-align:middle;" />
				<span>
					ZONA DE ENTREGA AL TUTOR - TRABAJOS PRÁCTICOS
				</span>
			</div>
			<br/>
			<p class='t_center' style="margin-bottom:10px;">
				Aqu&iacute; puede acceder a los Trabajos Pr&aacute;cticos propuestos, estos son de car&aacute;cter obligatorio.
			</p>
		</div>
	</div>
</div>

<div style="padding: 20px 40px">

		<div class='panelTabContent' style="margin: 0 auto;width: 100% !important;">
			<!-- TAB 3 : NOTIFICACIONES -->
			<p class="tituloTabs">Subir actividad enviada por medios externos</p>

			<form method="post" action="aula/trabajos_practicos/enviar_tutor" enctype='multipart/form-data'>
				<div class='filafrm'>
					<div class='etiquetafrm'>Cursos</div>
					<div class='campofrm'>
						<select name="idCurso" class="width100_nomargin">
							<option value=""> - Selecciona un curso - </option>
							<?php while($cursoTutor = mysqli_fetch_object($cursosTutor)): ?>
								<option value="<?php echo $cursoTutor->idcurso ?>">
									<?php echo $cursoTutor->titulo ?>
								</option>
							<?php endwhile; ?>
						</select>
					</div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm'>Alumno</div>
					<div class='campofrm'>
						<select name="idAlumno" class="width100_nomargin">
							<option value=""> - Selecciona un alumno, antes debes elegir un curso - </option>
							<?php while($cursoTutor = mysqli_fetch_object($cursosTutor)): ?>
								<option value="<?php echo $cursoTutor->idcurso ?>">
									<?php echo $cursoTutor->titulo ?>
								</option>
							<?php endwhile; ?>
						</select>
					</div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm'>Trabajo práctico</div>
					<div class='campofrm'>
						<select name="idTrabajoPractico" class="width100_nomargin">
							<?php while($trabajoPractio = mysqli_fetch_object($trabajosPracticos)): ?>
								<option value="<?php echo $trabajoPractio->idarchivo_temario ?>">
									<?php echo $trabajoPractio->titulo ?>
								</option>
							<?php endwhile; ?>
						</select>
					</div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm'>Título</div>
					<div class='campofrm'><input type='text' name='titulo'/></div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm'>Anotación</div>
					<div class='campofrm'><textarea name="anotacion" rows="8"></textarea></div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm'>Archivo</div>
					<div class='campofrm'>
							<input type='file' name='actividad'/>
					</div>
				</div>
				
				<div class="clear filafrm"><input type='submit' value='Adjuntar actividad al alumno' style="width:100%;margin: 10px 0"></input></div>
			</form>
		</div>

	<div class='panelTabContent' style="margin: 0 auto;width: 100% !important;">
			<!-- TAB 3 : NOTIFICACIONES -->
			<p class="tituloTabs">Actividades enviadas por el alumno</p>

			<div id="resultadoActividades"></div>
	</div>

</div>

<script type="text/javascript" src="js-trabajos_practicos-default.js"></script>


