<div class="caja_separada">
	<div id='trabajosPracticos'>
		<br/>
		
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/trabajos_practicos/trabajos-practicos.png" alt="" style="vertical-align:middle;" />
				<span data-translate-html="zonatutor.titulo">
					ZONA DE ENTREGA AL TUTOR - TRABAJOS PRÁCTICOS
				</span>
			</div>
			<br/>
			<p class='t_center' style="margin-bottom:10px;" data-translate-html="zonatutor.descripcion">
				Aqu&iacute; puede acceder a los Trabajos Pr&aacute;cticos propuestos, estos son de car&aacute;cter obligatorio.
			</p>
		</div>
	</div>
</div>

<div style="padding: 20px 40px">

	<?php if(Usuario::compareProfile('alumno')): ?>
		<div class='panelTabContent' style="margin: 0 auto;width: 100% !important;">
			<!-- TAB 3 : NOTIFICACIONES -->
			<p class="tituloTabs" data-translate-html="zonatutor.enviar_actividad">Enviar actividad</p>

			<form method="post" action="aula/trabajos_practicos/enviar_tutor" enctype='multipart/form-data'>
				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="zonatutor.trabajo_practico">Trabajo práctico</div>
					<div class='campofrm'>
						<select name="idTrabajoPractico" class="width100_nomargin">
							<?php while($trabajoPractio = mysqli_fetch_object($trabajosPracticos)): ?>
								<option value="<?php echo $trabajoPractio->idarchivo_temario ?>">
									<?php echo $trabajoPractio->titulo ?>
								</option>
							<?php endwhile; ?>
						</select>
					</div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="zonatutor.titulo2">Título</div>
					<div class='campofrm'><input type='text' name='titulo'/></div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="zonatutor.anotacion">Anotación</div>
					<div class='campofrm'><textarea name="anotacion" rows="8"></textarea></div>
				</div>

				<div class='filafrm'>
					<div class='etiquetafrm' data-translate-html="zonatutor.archivo">Archivo</div>
					<div class='campofrm'>
							<input type='file' name='actividad'/>
					</div>
				</div>
				
				<div class="clear filafrm">
					<input type='submit' value='Enviar actividad al tutor' style="width:100%;margin: 10px 0" data-translate-value="zonatutor.enviar_actividad" />
				</div>
			</form>
		</div>
	<?php endif; ?>

	<div class='panelTabContent' style="margin: 0 auto;width: 100% !important;">
			<!-- TAB 3 : NOTIFICACIONES -->
			<p class="tituloTabs" data-translate-html="zonatutor.actividades_subidas">Actividades subidas</p>

		<?php if($actividades->num_rows > 0): ?>
			<form method="post" action="aula/trabajos_practicos/enviar">
				<table class="width100_nomargin">
					<tr>
						<th data-translate-html="zonatutor.actividad">Actividad</th>
						<th data-translate-html="zonatutor.titulo2">Título</th>
						<th data-translate-html="zonatutor.archivo">Archivo</th>
						<th data-translate-html="zonatutor.anotacion">Anotación</th>
						<th data-translate-html="zonatutor.fecha">Fecha</th>
						<th data-translate-html="zonatutor.estado">Estado</th>
						<th class="t_center" data-translate-html="zonatutor.nota">Nota</th>
					</tr>
					<?php while($actividad = mysqli_fetch_assoc($actividades)): ?>
						<tr data-idactividad="<?php echo $actividad['idarchivo_temario'] ?>">
							<td class="paddingTdTable">
								<?php echo $actividad['tituloTrabajo']; ?>
							</td>

							<td class="paddingTdTable">
								<?php echo $actividad['titulo']; ?>
							</td>

							<td class="paddingTdTable">
								<a href="archivos/actividades/<?php echo $actividad['archivo']; ?>" target="_blank"><?php echo $actividad['archivo']; ?>
								</a>
							</td>
	
							<td class="paddingTdTable">
								<?php echo $actividad['anotacion'] ?>
							</td>

							<td class="paddingTdTable">
								<?php echo date('d/m/Y', strtotime($actividad['fecha'])); ?>
							</td>

							<td class="paddingTdTable">
								<?php if($actividad['estado'] == 1): ?>

									<span data-translate-html="zonatutor.enviada">Enviada</span>

								<?php elseif($actividad['estado'] == 2): ?>

									<span data-translate-html="zonatutor.revisada">Revisada</span>

								<?php endif; ?>
							</td>

							<?php if(Usuario::compareProfile('alumno')): ?>
								<td class="t_center paddingTdTable">
									<?php if(is_null($actividad['nota'])): ?>
										<span>-</span>
									<?php else: ?>
										<?php echo $actividad['nota']; ?>
									<?php endif; ?>
								</td>
							<?php else: ?>

								<td class="t_center paddingTdTable">
									<input type="text" name="nota[<?php echo $actividad['id'] ?>]" value="<?php echo $actividad['nota'] ?>" />
								</td>
							<?php endif; ?>
						</tr>
					<?php endwhile; ?>
				</table>

				<?php if(!Usuario::compareProfile('alumno')): ?>

					<input type="submit" value="Guardar notas" style="width:100%">

				<?php endif; ?>

			</form>

		<?php else: ?>

			<p data-translate-html="zonatutor.noactividad">No se han subido actividades</p>

		<?php endif; ?>

	</div>

</div>

<script type="text/javascript" src="js-trabajos_practicos-default.js"></script>

