<div id="trabajosPracticosGrafica">

	<?php require DIRNAME(__FILE__) . '/intro_trabajos_practicos.php'?>
	
	<div class="menuVista2" style="float:right;">
		<div class="vista1" >
			<a href="aula/trabajos_practicos/listado">
				<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.listado" alt="Listado"/></a>
		</div>
		<div class="vista2" >
			<a href="#" onclick="return false;">
				<img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.clasica" alt="Vista Clásica"/></a>
		</div>
	</div>
	<div class="clear"></div>
	<br/>
	
	<div id="mesaTrabajosPracticos">	
		<div id="libretaTrabajosPracticos">

		</div>
	</div>
</div>

<?php /* ?>
			<?php $numero_de_modulo = 1; ?>
			<?php $totalModulos = mysqli_num_rows($registros_modulos); ?>
			<?php while($f = mysqli_fetch_assoc($registros_modulos)):?>
				<?php 
				//buscamos los archivos dentro de cada modulo
				$mi_modulo->set_modulo($f['idmodulo']);
				$mi_modulo->set_categoria_archivo(2);
				$registros_archivos = $mi_modulo->archivosModulosActivos();
				$nArchivos = mysqli_num_rows($registros_archivos);
				?>
				<div>
					<?php if(mysqli_num_rows($registros_archivos) > 0):?>
						<?php while ($f = mysqli_fetch_assoc($registros_archivos)):?>
							<?php if(file_exists($f['enlace_archivo'])):?>
								<?php 
								$peso_archivo = filesize($f['enlace_archivo'])/1024;
								$peso_archivo = $peso_archivo/1024;
								$peso_archivo = round($peso_archivo,2);
								$fecha = Fecha::invertir_fecha($f['f_creacion']);
								?>
								<div class='book'>
									<a class="Ntooltip" href='aula/trabajos_practicos/forzar_descarga/<?php echo $f['idarchivo_temario'] ?>' target='_blank'>
										<?php echo strtoupper($f['titulo']) ?>
										<span class="arrow_box">
											<p>curso: <?php echo $_SESSION['nombrecurso'];?></p>
											<p><?php echo $peso_archivo ?> Mb</p><br/>
											<?php if(!empty($f['descripcion'])):?>
												<p style='font-size:0.9em;margin-top:-8px;width:87%;' class='t_justify'><?php echo Texto::textoFormateado($f['descripcion']) ?></p>
											<?php endif ?>
										</span>
									</a>
								</div>
							<?php endif ?>
							<?php $nArchivos--; ?>
							<?php if($nArchivos > 0):?>
								<hr/>
							<?php endif ?>
						<?php endwhile; ?>
					<?php else: ?>
						<p>No existe ningun archivo para este modulo</p>
					<?php endif ?>
				</div>
				<?php $numero_de_modulo ++ ?>
			<?php endwhile ?>
<?php */ ?>