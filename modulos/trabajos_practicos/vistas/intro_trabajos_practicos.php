<div>
	<?php if($_SESSION['perfil'] != 'alumno'):?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/trabajos_practicos/trabajos-practicos.png" alt="" style="vertical-align:middle;" />
				<span data-translate-html="practicos.practicos">
					Trabajos pr&aacute;cticos / Ejercicios de evaluaci&oacute;n
				</span>
			</div>
			<br/>
			<p class='t_justify' style="margin-bottom:10px;" data-translate-html="practicos.descripcion">
				Esta secci&oacute;n contiene ficheros, notas, apuntes breves y recordatorios que el docente considera necesario 
				destacar para mejorar la asimilaci&oacute;n de los contenidos del curso por parte del alumnado. 
			</p>
			<p class='t_justify' data-translate-html="practicos.descripcion2">
				Puede introducir nuevas notas de inter&eacute;s o ficheros, revisar las que introdujo anteriormente, 
				modificar su contenido o eliminar las que ya no suscitan duda alguna.
			</p> 
		</div>
	<?php else:?>
		<div class="popupIntro">
			<div class='subtitle t_center'>
				<img src="imagenes/trabajos_practicos/trabajos-practicos.png" alt="" style="vertical-align:middle;" />
				<span data-translate-html="practicos.practicos">
					Trabajos pr&aacute;cticos / Ejercicios de evaluaci&oacute;n
				</span>
			</div>
			<br/>
			<p class='t_center' style="margin-bottom:10px;" data-translate-html="practicos.aqui">
				Aqu&iacute; puede acceder a los Trabajos Pr&aacute;cticos propuestos, estos son de car&aacute;cter obligatorio.
			</p>
		</div>
	<?php endif;?>
</div>