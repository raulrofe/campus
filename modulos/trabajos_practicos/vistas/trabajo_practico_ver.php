<div class="caja_separada">
	<div id='trabajosPracticos'>

		<br/>
		<div class='subtitleModulo'><?php echo $_SESSION['nombrecurso'] ?></div>

		<?php if (isset($rowAdjunto)): ?>
			<div class='tituloArchivo'>
				<span data-translate-html="practicos.titulo">
					Titulo: 
				</span>
					<?php echo $rowAdjunto['titulo'] ?></div>
			<div class="clear"></div><br/>
			<div style="line-height:1.2em"><?php echo $rowAdjunto['enunciado'] ?></div>
			<div class="clear"></div><br/>
		<?php endif; ?>
	</div>
</div>