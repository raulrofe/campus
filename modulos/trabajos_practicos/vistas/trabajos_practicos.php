<div class="caja_separada">
	<div id='trabajosPracticos'>
		<br/>
		<?php require DIRNAME(__FILE__) . '/intro_trabajos_practicos.php'; ?>
		<?php $numero_de_modulo = 1; ?>
		<?php $totalModulos = mysqli_num_rows($registros_modulos); ?>
		<?php while ($f = mysqli_fetch_assoc($registros_modulos)): ?>

			<?php if ($totalModulos > 1): ?>
				<div class='subtitleModulo'>M&oacute;dulo <?php echo $numero_de_modulo . ':' . $f['nombre'] ?> </div>
			<?php else: ?>
				<div class='subtitleModulo'><?php echo $_SESSION['nombrecurso'] ?></div>
			<?php endif; ?>

			<?php
			$mi_modulo->set_modulo($f['idmodulo']);
			$mi_modulo->set_categoria_archivo(2);
			$registros_archivos = $mi_modulo->archivosModulosActivos();
			$nArchivos = mysqli_num_rows($registros_archivos);
			?>

			<div class='archivosModulo'>
				<?php if (mysqli_num_rows($registros_archivos) > 0): ?>
					<?php while ($f = mysqli_fetch_assoc($registros_archivos)): ?>
						<?php if (file_exists($f['enlace_archivo'])): ?>

							<?php $peso_archivo = filesize($f['enlace_archivo']) / 1024; ?>
							<?php $peso_archivo = $peso_archivo / 1024; ?>
							<?php $peso_archivo = round($peso_archivo, 2); ?>
							<?php $fecha = Fecha::invertir_fecha($f['f_creacion']); ?>

							<div class='listado_tabla'>
								<div class="tituloArchivo fleft">
									<a href="aula/trabajos_practicos/forzar_descarga/<?php echo $f['idarchivo_temario'] ?>" target="_blank">
										<?php echo $f['titulo'] ?>
									</a>
								</div>

								<div class="fright">
									<?php
									if ($_SESSION['idcurso'] == 76 || $_SESSION['idcurso'] == 75 || $_SESSION['idcurso'] == 437 ||
										$_SESSION['idcurso'] == 91 || $_SESSION['idcurso'] == 92 || $_SESSION['idcurso'] == 95 ||
										$_SESSION['idcurso'] == 206 || $_SESSION['idcurso'] == 207):
										?>
										<a class='fancybox' data-tp="<?php echo $f['idarchivo_temario'] ?>" rel='tooltip' title='vista web' href='#' onclick='return false'>
											<img src='imagenes/menu_vistas/view.png' alt='responder' />
										</a>
									<?php endif; ?>
									<a rel='tooltip' title='descargar' href='aula/trabajos_practicos/forzar_descarga/<?php echo $f['idarchivo_temario'] ?>' target='_blank'>
										<img src='imagenes/options/descargar.png' alt='responder' />
									</a>
								</div>

								<div class='clear'><br/></div>

								<?php if (!empty($f['descripcion'])): ?>
									<div style='font-size:0.8em;margin-top:-8px;' class='t_justify'><?php echo Texto::textoFormateado($f['descripcion']) ?> </div>
								<?php endif; ?>
							</div>

						<?php endif; ?>

						<?php $nArchivos--; ?>

						<div id="modal_<?php echo $f['idarchivo_temario'] ?>" class="hide" style="max-width:500px!important;">
							<div class="caja_separada">
								<div id='trabajosPracticos'>
									<br/>
									<div class='subtitleModulo'><?php echo $_SESSION['nombrecurso'] ?></div>
									<div class='tituloArchivo'>
										<span data-translate-html="practicos.titulo">
											Titulo: 
										</span>
										<?php echo $f['titulo'] ?></div>
									<div class="clear"></div><br/>
									<div style="line-height:1.2em"><?php echo $f['enunciado'] ?></div>
									<div class="clear"></div><br/>
								</div>
							</div>
						</div>

					<?php endwhile; ?>
				<?php else: ?>
				<span data-translate-html="practicos.noarchivo">
					No existe ningun archivo para este modulo
				</span>
				<?php endif; ?>
			</div>
			<?php $numero_de_modulo ++; ?>
		<?php endwhile; ?>
	</div>
</div>

<script>
	$(".fancybox").click(function () {

		//Obtengo el id del elemento
		var idTrabajo = $(this).attr('data-tp');

		// alert('id Trabajo practico: ' + idTrabajo);

		if (idTrabajo != '' && idTrabajo != undefined)
		{
			var actividadWeb = $("#modal_" + idTrabajo).html();

			$.fancybox({
				'width': '100%',
				'height': '100%',
				'showNavArrows': false,
				'titleShow': true,
				'scrolling': 'auto',
				'nextEffect': 'fade',
				'openSpeed': 600,
				'closeSpeed': 200,
				'autoSize': false,
				'beforeLoad': function () {
					this.width = 680;
					this.height = 660;
				},
				'content': actividadWeb,
			});
			return false;
		}

		return false;
	});
</script>