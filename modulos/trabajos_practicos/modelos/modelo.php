<?php
class TrabajosPracticosTutor extends modeloExtend {

	public function getTrabajosPracticosCurso($idCurso) {

		$sql = "SELECT idarchivo_temario, titulo" .
				" FROM archivo_temario " .
				" WHERE idcurso = " . $idCurso . " AND idcategoria_archivo = 2" . 
				" ORDER BY prioridad ASC";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;

	}

	public function obtenerTodasActividades(){
		$sql =  "SELECT *" .
				" FROM actividades as act" .
				" LEFT JOIN alumnos as al ON al.idalumnos = act.idusuario" .
				" WHERE act.idmodulo = " . $idModulo;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}

	public function obtenerMisActividades($idMatricula){
		$sql =  "SELECT a.*, at.titulo as tituloTrabajo" .
				" FROM actividades a" .
				" LEFT JOIN archivo_temario as at ON at.idarchivo_temario = a.idarchivo_temario" .
				" WHERE a.idmatricula = " . $idMatricula;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}	

	public function insertarActividades($titulo, $anotacion, $nombreArchivo, $estado, $idArchivoTemario, $idCurso, $idUsuario){
		$sql="INSERT into actividades (titulo, anotacion, archivo, estado, idarchivo_temario, idcurso, idmatricula)
		VALUES ('$titulo','$anotacion', '$nombreArchivo', '$estado', '$idArchivoTemario', '$idCurso', '$idUsuario')";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}	

	public function actualizarNotaActividad($id, $nota){
		$sql = " UPDATE actividades" . 
		" SET nota = '$nota'" .
		" WHERE id = " . $id;

		$resultado = $this->consultaSql($sql);	

		return $resultado;	
	}

	// Obtengo el ID del tutor uno en la plataforma
	public function obtenerIdTutorPrincipal($idCurso) {
		$sql =  "SELECT idrrhh" .
				" FROM staff" .
				" WHERE status = 'tutor1' AND idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}

	// Obtengo los cursos del tutor
	public function obtenerCursosTutor($idTutor) {
		$sql =  "SELECT c.titulo, c.idcurso" .
				" FROM staff as s" .
				" LEFT JOIN curso as c ON c.idcurso = s.idcurso" .
				" WHERE s.status = 'tutor1' AND s.idrrhh = " . $idTutor . " AND borrado = 0 ORDER BY f_inicio DESC";

		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}

	public function obtenerMatriculasCurso($idCurso) {
		$sql =  "SELECT al.idalumnos, al.nombre, al.apellidos, m.idmatricula" .
				" FROM matricula as m" .
				" LEFT JOIN alumnos as al ON m.idalumnos = al.idalumnos" .
				" WHERE m.idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;			
	}

	public function obtengoNota($idalumno, $idarchivo, $idCurso) {

		$sql = "SELECT nota_trabajo from calificacion_trabajo_practico  where idcurso = " . $idCurso . " and idalumnos = ".$idalumno." and idarchivo_temario = ".$idarchivo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		$fila = mysqli_fetch_assoc($resultado);
		return $fila['nota_trabajo'];
	}	

	public function obtengoActividadMatricula($idCurso, $idMatricula, $idArchivoTemario) {

		$sql = "SELECT id" .
		" FROM actividades" .
		" WHERE idarchivo_temario = " . $idArchivoTemario .
		" AND idcurso = " . $idCurso .
		" AND idmatricula =" . $idMatricula;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;		
	}
}