$( document ).ready(function() {
	$("tr[data-idactividad]").each(function(){ 
		var idArchivo = $(this).attr('data-idactividad');

		$("option[value=" + idArchivo + "]").remove();
	});

	var nOptions = $("select[name=idTrabajoPractico] option").length;

	if(nOptions == 0) {
		var text = '<option data-translate-html="zonatutor.realizadotodos"> - Ya has realizado todos los trabajos prácticos - </option>';

		$("select[name=idTrabajoPractico]").html(text);
	}


	$("select[name=idCurso]").on('change', function(){

		var idCurso = $(this).val();

		$.ajax({

			url : 'aula/trabajos_practicos/zona_tutor/curso/' + idCurso,
			type: 'GET',

		}).success(function(json) {

			json = JSON.parse(json);

			$("select[name=idAlumno]").html(json.html);
			
		}).complete(function(){
			
		});
	});

	$("select[name=idAlumno]").on('change', function(){

		var idAlumno = $(this).val();

		$.ajax({

			url : 'aula/trabajos_practicos/zona_tutor/alumno/' + idAlumno,
			type: 'GET',

		}).success(function(json) {

			json = JSON.parse(json);

			$("#resultadoActividades").html(json.html);
			
		}).complete(function(){
			
		});
	});

});
