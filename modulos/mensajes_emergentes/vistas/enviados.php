<div id="mensajes_emergentes">
	<div class="subtitle t_center">
		<img src="imagenes/mensajes_emergentes/mensajes_emergentes.png" alt="" class="imageIntro"/>
		<span data-translate-html="emergentes.titulo">
			MENSAJES EMERGENTES
		</span>
	</div>
	<br/><br/>
	 
	 <div id="mensajesEmergentesBuscador">
		 <input type="text" name="buscar" placeholder="Buscar" data-translate-placeholder="general.buscar" />
	 </div>
	 
	<div>	
		<div id="navMensajesEmergentes" class="fleft panelTabMenu">		
			<div class="redondearBorde" id="nav1">
				<a title="Nuevo aviso personal" href="mensajes-emergentes/recibidos">
					<img src="imagenes/mensajes_emergentes/recibidos.png">
					<span data-translate-html="emergentes.recibidos">
						Recibidos
					</span>
				</a>
			</div>
			
			<div class="blanco redondearBorde" id="nav2" style="margin-right:-3px;">
				<a title="listado avisos personales" href="mensajes-emergentes/enviados">
					<img alt="" src="imagenes/mensajes_emergentes/salida.png">
					<span data-translate-html="emergentes.enviados">
						Enviados
					</span>
				</a>
			</div>	
		</div>
	</div>
		
	<div id="contenidoMensajesEmergentes" class="fleft panelTabContent listarElementos">
		<?php if($mensajes->num_rows > 0):?>
			<?php while($mensaje = $mensajes->fetch_object()):?>

				<div class="elemento" data-dni="<?php echo $mensaje->dni . $mensaje->nif?>">
					<div class="elementoNombre">
						<p>
							<strong>
								<span data-translate-html="general.para">
									Para:
								</span>
							</strong>

							<span>
								<?php echo Texto::textoPlano($mensaje->nombrec_a)?>
								<?php echo Texto::textoPlano($mensaje->nombrec_rh)?>
							</span>
						</p>
					</div>

					<div class="elementoContenido">
						<p>
							<?php echo $mensaje->mensaje?>
						</p>
					</div>

					<div class="elementoPie">
						<div class="elementoFecha fleft">
							<?php echo Fecha::obtenerFechaFormateada($mensaje->fecha)?>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<div class="clear"></div>

			<?php endwhile;?>
		<?php else:?>
			<div class="elementosNoEncontrados">
				<strong>
					<span data-translate-html="emergentes.no_emergentes">No has mandado ning&uacute;n mensaje emergente</span>
				</strong>
			</div>
		<?php endif;?>
	</div>
</div>