<div id="mensajes_emergentes">
	<div class="subtitle t_center">

		<img src="imagenes/mensajes_emergentes/mensajes_emergentes.png" class="imageIntro"/>

		<span data-translate-html="emergentes.titulo">
			MENSAJES EMERGENTES
		</span>

	</div>

	<br/><br/>
	 
	 <div id="mensajesEmergentesBuscador">
		 <input type="text" name="buscar" placeholder="Buscar" data-translate-placeholder="general.buscar"/>
	 </div>
	 
	<div>	
		<div id="navMensajesEmergentes" class="fleft panelTabMenu">		
			<div class="blanco redondearBorde" id="nav1" style="margin-right:-3px;">
				<a title="Nuevo mensaje emergente" href="mensajes-emergentes/recibidos" data-translate-title="emergentes.nuevo">
					
					<img src="imagenes/mensajes_emergentes/recibidos.png">
					
					<span data-translate-html="emergentes.recibidos">
						Recibidos
					</span>

				</a>
			</div>
			
			<div class="redondearBorde" id="nav2">
				<a title="listado avisos personales" href="mensajes-emergentes/enviados" data-translate-title="emergentes.listado"> 

					<img src="imagenes/mensajes_emergentes/salida.png">

					<span data-translate-html="emergentes.enviados">
						Enviados
					</span>
				</a>
			</div>	
		</div>
	</div>
	
	<div id="contenidoMensajesEmergentes" class="fleft panelTabContent listarElementos">

		<?php if($mensajes->num_rows > 0):?>

			<?php while($mensaje = $mensajes->fetch_object()):?>
				
				<?php $urlMensajeUsuario = Usuario::esAlumno($mensaje->idusuario_envio)?>
				
				<div class="elemento" data-dni="<?php echo $mensaje->dni . $mensaje->nif?>">

					<div class="elementoNombre">

						<p>
							<strong data-translate-html="general.de">De:</strong>

							<span>
								<?php echo Texto::textoPlano($mensaje->nombrec_a)?>
								<?php echo Texto::textoPlano($mensaje->nombrec_rh)?>
							</span>
						</p>
					</div>
					<div class="elementoContenido">
						<p>
							<?php echo $mensaje->mensaje?>
						</p>
					</div>

					<div class="elementoPie">
						<div class="elementoFecha fleft">
							<?php echo Fecha::obtenerFechaFormateada($mensaje->fecha)?>	
						</div>

						<div class="fright">
							<a href="#" rel="tooltip" 
							   title="Responder" 
							   onclick="popup_open('Mensajes emergentes', 'Mensajes_emergentes_alumnos_<?php echo $urlMensajeUsuario[1] ?>',  'mensajes-emergentes/nuevo/<?php echo $urlMensajeUsuario[0] ?>/<?php echo $urlMensajeUsuario[1] ?>', 600, 600); return false;" 
								data-translate-title="emergentes.responder">

								<img alt="responder" src="imagenes/options/responder.png" data-translate-title="emergentes.responder" />

							</a>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<div class="clear"></div>

			<?php endwhile;?>

		<?php else:?>

			<div class="elementosNoEncontrados">
				<strong data-translate-html="emergentes.no_emergentes">
					No has recibido ning&uacute;n mensaje emergente
				</strong>
			</div>

		<?php endif;?>
	</div>
</div>