<br/>

<?php $idCKEditor = time();?>

<div class='popupIntro'>
	<div class='subtitle t_center'>
		<span data-translate-html="emergentes.nuevo">
			Nuevo mensaje emergente
		</span>
	</div>
</div>

<div class="t_center">
	<span data-translate-html="emergentes.a">
		Este mensaje va dirigido a:
	</span>
	<?php echo Texto::textoPlano($nombreUsuario)?>
</div>

<div id="mensajes_emergentes">
	<form id="frm_mensajes_emergentes" action="" method="post">
		<ul>
			<li id="mensajes_emergentes_iconos">
				<h2 data-translate-html="emergentes.icono">Selecciona un icono para el mensaje</h2>
				<ul>
					<li>
						<a href="#" onclick="return false;" title="Alegre" rel="1" data-translate-title="iconos.alegre">
							<img src="imagenes/mensajes_emergentes/icono_alegre.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Gui&ntilde;o" rel="2" data-translate-title="iconos.gino">
							<img src="imagenes/mensajes_emergentes/icono_guino.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Sonriente" rel="3" data-translate-title="iconos.sonriente">
							<img src="imagenes/mensajes_emergentes/icono_sonrisa.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Triste" rel="4" data-translate-title="iconos.triste">
							<img src="imagenes/mensajes_emergentes/icono_triste.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Amor" rel="5" data-translate-title="iconos.amor">
							<img src="imagenes/mensajes_emergentes/icono_amor.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Enfadado" rel="6" data-translate-title="iconos.enfadado">
							<img src="imagenes/mensajes_emergentes/icono_enfadado.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Tiempo" rel="7" data-translate-title="iconos.tiempo">
							<img src="imagenes/mensajes_emergentes/icono_reloj.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Pregunta" rel="8" data-translate-title="iconos.pregunta">
							<img src="imagenes/mensajes_emergentes/icono_pregunta.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Informaci&oacute;n" rel="9" data-translate-title="iconos.informacion">
							<img src="imagenes/mensajes_emergentes/icono_info.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Bien" rel="10" data-translate-title="iconos.bien">
							<img src="imagenes/mensajes_emergentes/icono_ok.png" />
						</a>
					</li>

					<li>
						<a href="#" onclick="return false;" title="Esclamaci&oacute;n" rel="11" data-translate-title="iconos.esclamacion">
							<img src="imagenes/mensajes_emergentes/icono_exclamacion.png" />
						</a>
					</li>
				</ul>

				<input id="mensajes_emergentes_iconos_hidden" type="hidden" name="icono" />

				<div class="clear"></div>
			</li>
			<li>
				<h2 data-translate-html="emergentes.escribe">
					Escribe el mensaje
				</h2>
				<textarea id="mensajes_emergentes_textarea_<?php echo $idCKEditor;?>" name="mensaje" rows="1" cols="1"></textarea>
			</li>

			<li class="t_right">
				<button type="submit" data-translate-html="formulario.enviar">Enviar</button>
			</li>
		</ul>

		<div class="clear"></div>

	</form>

</div>

<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		menubar: false,
		statusbar: false,
		plugins: ["emoticons"],
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | emoticons",
	});
</script>