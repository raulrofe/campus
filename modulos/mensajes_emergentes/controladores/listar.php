<?php

$objModelo = new ModeloMsgEmergente();

$get = Peticion::obtenerGet();

if(isset($get['menu']) && $get['menu'] == 'enviados')
{
	$mensajes = $objModelo->obtenerMensajesEnviados(Usuario::getIdUser(true));
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'enviados');
}
else
{
	$mensajes = $objModelo->obtenerMensajesRecibidos(Usuario::getIdUser(true));
	
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'recibidos');
}