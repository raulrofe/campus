<?php
die();
$objModelo = new ModeloMsgEmergente();

$get = Peticion::obtenerGet();
if(isset($get['idMsg']) && is_numeric($get['idMsg']))
{
	if(Usuarios::comprobarPermisosLogueo(Usuario::getIdUser(), Usuario::getIdCurso()))
	{
		$mensaje = $objModelo->obtenerUnMensaje($get['idMsg']);
		if($mensaje->num_rows == 1)
		{
			if($objModelo->eliminarMensaje($get['idMsg']))
			{
				Alerta::guardarMensajeInfo('mensajeeliminado','Mensaje eliminado');
			}
		}
	}
}

Url::redirect('mensajes-emergentes/listar');