<?php
$objModelo = new ModeloMsgEmergente();

if(Usuario::compareProfile(array('tutor', 'coordinador', 'alumno')))
{
	//$alumnos = $objModelo->obtenerAlumnos($_SESSION['idcurso']);
	//if($alumnos->num_rows > 0)
	//{
	$get = Peticion::obtenerGet();
	if(isset($get['perfil'], $get['idusuario']) && ($get['perfil'] == 'tutor' || $get['perfil'] == 'alumno') && is_numeric($get['idusuario']))
	{
		// obtiene el usuario de destino segun el perfil de usuario
		if($get['perfil'] == 'tutor')
		{
			$usuario = $objModelo->obtenerRRHH($get['idusuario']);
		}
		else if($get['perfil'] == 'alumno')
		{
			$objAlumno = new Alumnos();
			$objAlumno->set_alumno($get['idusuario']);
			$usuario = $objAlumno->ver_alumno();
		}
		
		// si existe el usuario de destino ...
		if(isset($usuario) && $usuario->num_rows == 1)
		{
			$usuario = $usuario->fetch_object();
			
			// obtiene el nombre y el id del usuario destinatario
			if($get['perfil'] == 'tutor')
			{
				$nombreUsuario = $usuario->nombrec;
			}
			else if($get['perfil'] == 'alumno')
			{
				$nombreUsuario = $usuario->apellidos . ', ' . $usuario->nombre;
			}
		
			// POST
			$post = peticion::obtenerPost();
			
			$esNegrita = false;
			if(isset($post['letranegrita']) && $post['letranegrita'] == 'on')
			{
				$esNegrita = true;
			}
			
			$iconosArray = array(
				'icono_alegre.png', 'icono_guino.png', 'icono_sonrisa.png', 'icono_triste.png', 'icono_amor.png', 'icono_enfadado.png',
				'icono_reloj.png', 'icono_pregunta.png', 'icono_info.png', 'icono_ok.png', 'icono_exclamacion.png'
			);

			if(isset($post['mensaje']) && !empty($post['mensaje']))
			{
				// obtenemos el id del usuario destinatario
				$idUsuarioDest = $get['idusuario'];
				if($get['perfil'] == 'tutor')
				{
					$idUsuarioDest = 't_' . $idUsuarioDest;
				}
				
				$icono = 'NULL';
				if(isset($post['icono']) && is_numeric($post['icono']) && isset($iconosArray[$post['icono']-1]))
				{
					$icono = $iconosArray[$post['icono']-1];
				}
				
				/*if($post['idalumno'] != 0)
				{
					$alumno = $objModelo->obtenerUnAlumno(Usuario::getIdCurso(), $post['idalumno']);
					if($alumno->num_rows != 1)
					{
						$post['idalumno'] = null;
					}
				}
			
				if(isset($post['idalumno']))
				{*/
					$resultado = $objModelo->insertarMensaje(
						Usuario::getIdUser(true), $idUsuarioDest, Usuario::getIdCurso(), $post['mensaje'], $icono
					);
					
					if($resultado)
					{
						Alerta::mostrarMensajeInfo('enviadomensaje','Se ha enviado el mensaje');
					}
					
					/*if($resultado)
					{
						$idMensaje = $objModelo->obtenerUltimoIdInsertado();
						if($post['idalumno'] == 0)
						{
							$alumnosInsert = $objModelo->obtenerAlumnos($_SESSION['idcurso']);
							while($oneAlumno = $alumnosInsert->fetch_object())
							{
								$resultado = $objModelo->insertarDestinatarios($idMensaje, $oneAlumno->idalumnos);
							}
						}
						else
						{
							$resultado = $objModelo->insertarDestinatarios($idMensaje, $post['idalumno']);
						}
						
						Alerta::mostrarMensajeInfo('enviadomensaje','Se ha enviado el mensaje');
					}
				}*/
			}
			else if(isset($post['mensaje']))
			{
				Alerta::mostrarMensajeInfo('debesmensaje','Debes escribir un mensaje');
			}
		
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevo');
		}
	}
}