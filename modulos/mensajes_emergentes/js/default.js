$(document).ready(function()
{
	$('#mensajes_emergentes_iconos img').click(function()
	{
		var clickHasClass = $(this).hasClass('mensajes_emergentes_iconos_active');
		
		if(clickHasClass)
		{
			$('#mensajes_emergentes_iconos_hidden').val('');
		}
		
		$('#mensajes_emergentes_iconos img').removeClass('mensajes_emergentes_iconos_active');
		
		if(!clickHasClass)
		{
			$(this).addClass('mensajes_emergentes_iconos_active');
			$('#mensajes_emergentes_iconos_hidden').val($(this).parent().attr('rel'));
		}
	});
});

$(document).ready(function()
{	
	// BUSCADOR DE CURSOS INACTIVOS
	$('#mensajesEmergentesBuscador input').keyup(function(event)
	{
		mensajesEmergentesBuscar();
	});
});

function mensajesEmergentesBuscar()
{
	var wordSearch = $('#mensajesEmergentesBuscador input').val();
	
	$('#contenidoMensajesEmergentes .elemento').addClass('hide');
	
	if(wordSearch != "")
	{
		var inSearch = '';
		$('#contenidoMensajesEmergentes .elemento').each(function(index)
		{
			inSearch = $(this).find('.elementoNombre span').text() + ' ' +
						$(this).find('.elementoContenido p').text() + ' ' +
						$(this).find('.elementoContenido p').text() + ' ' +
						$(this).attr('data-dni');
			
			if(inSearch != undefined)
			{
				wordSearch = LibMatch.escape(wordSearch);
				var regex = new RegExp('(' + wordSearch + ')', 'i');
				if(inSearch.match(regex))
				{
					$(this).removeClass('hide');
				}
			}
		});
	}
	else
	{
		$('#contenidoMensajesEmergentes .elemento').removeClass('hide');
		//$('#contenidoMensajesEmergentes .elemento[data-page!="' + $('#contenidoMensajesEmergentes .paginado').attr('data-page-current') + '"]').addClass('hide');
	}
}