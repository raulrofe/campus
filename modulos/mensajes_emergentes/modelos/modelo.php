<?php
class ModeloMsgEmergente extends modeloExtend
{
	public function insertarMensaje($idusuario_envio, $idusuario_destino, $idcurso, $mensaje, $icono)
	{
		$sql = 'INSERT INTO mensaje_emergente (idusuario_envio, idusuario_destino, mensaje, fecha, url_icono, idcurso)' .
		' VALUES ("' . $idusuario_envio . '", "' . $idusuario_destino . '", "' . $mensaje . '", "' . date('Y-m-d H:i:s') . '",' .
		' "' . $icono . '", ' . $idcurso . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerRRHH($idrrhh)
	{
		$sql = 'SELECT rh.idrrhh, rh.nombrec FROM rrhh AS rh WHERE rh.idrrhh = ' . $idrrhh;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAlumnos($idCurso)
	{
		$sql = 'SELECT al.idalumnos, CONCAT(al.nombre, " ", al.apellidos) AS nombrec FROM matricula AS mt LEFT JOIN alumnos AS al ON mt.idalumnos = al.idalumnos WHERE mt.idcurso = ' . $idCurso;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnAlumno($idCurso, $idalumno)
	{
		$sql = 'SELECT mt.idalumnos FROM matricula AS mt WHERE mt.idcurso = ' . $idCurso . ' AND mt.idalumnos = ' . $idalumno;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajesEnviados($idUsuario)
	{
		$sql = 'SELECT me.idmensaje_emergente, me.mensaje, me.fecha, rh.nif, al.dni,
			CONCAT( al.nombre, " ", al.apellidos ) AS nombrec_a, rh.nombrec AS nombrec_rh
			FROM mensaje_emergente AS me
			LEFT JOIN alumnos AS al ON al.idalumnos = me.idusuario_destino
			LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = me.idusuario_destino
			WHERE me.idusuario_envio = "' . $idUsuario . '"
			GROUP BY me.idmensaje_emergente
			ORDER BY me.fecha DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerMensajesRecibidos($idUsuario)
	{
		$sql = 'SELECT me.idmensaje_emergente, me.mensaje, me.fecha, rh.nif, al.dni,
			CONCAT( al.nombre, " ", al.apellidos ) AS nombrec_a, me.idusuario_envio, rh.nombrec AS nombrec_rh
			FROM mensaje_emergente AS me
			LEFT JOIN alumnos AS al ON al.idalumnos = me.idusuario_envio
			LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = me.idusuario_envio
			WHERE me.idusuario_destino = "' . $idUsuario . '"
			GROUP BY me.idmensaje_emergente
			ORDER BY me.fecha DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnMensaje($idMsg)
	{
		$sql = 'SELECT me.idmensaje_emergente' .
		' FROM mensaje_emergente AS me' .
		' WHERE me.idmensaje_emergente = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function eliminarMensaje($idMsg)
	{
		$sql = 'DELETE FROM mensaje_emergente WHERE idmensaje_emergente = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	/*public function insertarDestinatarios($idMensaje, $idAlumno)
	{
		$sql = 'INSERT INTO mensaje_emergente_destinatario (idmensaje_emergente, idalumno)' .
		' VALUES (' . $idMensaje . ', ' . $idAlumno . ')';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}*/
}