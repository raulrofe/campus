<?php
$get = Peticion::obtenerGet();

$objModelo = new ModeloOpiniones();

$numPagina = 1;
if(isset($get['pagina']) && is_numeric($get['pagina']))
{
	$numPagina = $get['pagina'];
}
		
$mensajesCount = $objModelo->obtenerMensajes($numPagina);
$maxElementsPaging = 5;
$maxPaging = $mensajesCount->num_rows;
$maxPaging = ceil($maxPaging / $maxElementsPaging);

$elementsIni = ($numPagina - 1) * $maxElementsPaging;
		
$mensajes = $objModelo->obtenerMensajes($elementsIni, $maxElementsPaging);

require_once mvc::obtenerRutaVista(dirname(__FILE__), 'index');