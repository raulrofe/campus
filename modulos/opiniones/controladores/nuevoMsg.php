<?php
$objModelo = new ModeloOpiniones();

$get = Peticion::obtenerGet();

if($_SESSION['perfil'] != 'supervisor')
{
	// POST
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$post = Peticion::obtenerPost();
		if(isset($post['mensaje']))
		{
			if(!empty($post['mensaje']))
			{
				$idUsuario = $_SESSION['idusuario'];
				if($_SESSION['perfil'] != 'alumno')
				{
					$idUsuario = 't_' . $idUsuario;
				}
				
				if($objModelo->nuevoMensaje($idUsuario, Usuario::getIdCurso(), $post['mensaje']))
				{
					Alerta::guardarMensajeInfo('opinionpublicada','Opinión publicada');
				}
				else
				{
					Alerta::guardarMensajeInfo('opinionnopublicada','La opinión no pudo ser publicada');
				}
				
				// POR SI ES EN RESPUESTA A OTRO MENSAJE
				if(isset($post['idMsgResp']) && is_numeric($post['idMsgResp']))
				{
					// ID del mensaje recien insertado
					$idMensajeInsertado = $objModelo->obtenerUltimoIdInsertado();
					
					// si existe el mensaje al que vamos a hacer respuesta ...
					$mensaje = $objModelo->obtenerUnMensaje($post['idMsgResp'], false);
					if($mensaje->num_rows == 1)
					{
						$idMsgRespuesta = $post['idMsgResp'];
						
						$mensaje = $mensaje->fetch_object();
						if(isset($mensaje->idforo_mensaje_respondido))
						{
							$idMsgRespuesta = $mensaje->idforo_mensaje_respondido;
						}
						
						$objModelo->nuevoMensajeRespuesta($idMsgRespuesta, $idMensajeInsertado);
					}
				}
					
				Url::redirect('opiniones' , true);
			}
		}
		
		Url::redirect('opiniones', true);
	}
	// MENSAJE EN RESPUESTA A OTRO
	else
	{
		if(isset($get['idMsgResp']) && is_numeric($get['idMsgResp']))
		{
			$mensaje = $objModelo->obtenerUnMensaje($get['idMsgResp']);
			if($mensaje->num_rows == 1)
			{
				$mensaje = $mensaje->fetch_object();
				
				if(isset($mensaje->t_foto))
				{
					$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
					$foto = $mensaje->t_foto;
				}
				else
				{
					$nombrec = $mensaje->a_nombrec;
					$foto = $mensaje->a_foto;
				}
				
				require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoMsgResp');
			}
		}
		else
		{
			require_once mvc::obtenerRutaVista(dirname(__FILE__), 'nuevoMsg');
		}
	}
}