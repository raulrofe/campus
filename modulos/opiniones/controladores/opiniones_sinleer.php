<?php
	mvc::cargarModuloSoporte('opiniones');
	$modeloForo = new ModeloOpiniones();
	
	$idCurso = Usuario::getIdCurso();
	$idMatricula = Usuario::getIdMatricula();
	
	$mensajesOpinionesSinLeer = 0;
	
	if(Usuario::compareProfile('alumno'))
	{
		$fechasUltimaConexion = $modeloForo->obtenerUltimaFechaConexionAlumno($idMatricula);
		if($fechasUltimaConexion->num_rows > 0)
		{
			$fechasUltimaConexion = $fechasUltimaConexion->fetch_object();
			if(isset($fechasUltimaConexion->fecha_salida))
			{
				$ultimaConexion = $fechasUltimaConexion->fecha_salida;
			}
			else
			{
				$ultimaConexion = $fechasUltimaConexion->fecha_entrada;	
			}
		}
		
		$idUsuario = Usuario::getIdUser();
		if(isset($ultimaConexion))
		{
			$mensajesOpiniones = $modeloForo->obteneMensajesDesdeUltimoAccesoOpiniones($idCurso, $ultimaConexion);
			$mensajesOpinionesSinLeer = $mensajesOpiniones->num_rows;	
		}
	}
	else
	{
		$fechasUltimaConexion = $modeloForo->obtenerUltimaFechaConexionTutor($idMatricula);
		if($fechasUltimaConexion->num_rows > 0)
		{
			$fechasUltimaConexion = $fechasUltimaConexion->fetch_object();
			if(isset($fechasUltimaConexion->fecha_salida))
			{
				$ultimaConexion = $fechasUltimaConexion->fecha_salida;
			}
			else
			{
				$ultimaConexion = $fechasUltimaConexion->fecha_entrada;	
			}
		}
		else
		{
			$ultimaConexion = null;	
		}
		
		$idUsuario = Usuario::getIdUser(true);
		$mensajesOpiniones = $modeloForo->obteneMensajesDesdeUltimoAccesoOpiniones($idCurso, $ultimaConexion);
		$mensajesOpinionesSinLeer = $mensajesOpiniones->num_rows;	
	}