<?php
class Opiniones
{
	private static $_footerOptions = true;

	private function __construct() {}

	public static function listarMensajes($mensajes)
	{
		$ultimaConexion  = null;
		$modeloForo = new ModeloOpiniones();

		$idCurso = Usuario::getIdCurso();
		$idMatricula = Usuario::getIdMatricula();

		if(Usuario::compareProfile('alumno'))
		{
			$fechasUltimaConexion = $modeloForo->obtenerUltimaFechaConexionAlumno($idMatricula);
			if($fechasUltimaConexion->num_rows > 0)
			{
				$fechasUltimaConexion = $fechasUltimaConexion->fetch_object();
				if(isset($fechasUltimaConexion->fecha_salida))
				{
					$ultimaConexion = $fechasUltimaConexion->fecha_salida;
				}
				else
				{
					$ultimaConexion = $fechasUltimaConexion->fecha_entrada;
				}
			}

			$idUsuario = Usuario::getIdUser();
			$mensajesOpiniones = $modeloForo->obteneMensajesDesdeUltimoAccesoOpiniones($idCurso, $ultimaConexion);
		}
		else
		{
			$fechasUltimaConexion = $modeloForo->obtenerUltimaFechaConexionTutor($idMatricula);
			if($fechasUltimaConexion->num_rows > 0)
			{
				$fechasUltimaConexion = $fechasUltimaConexion->fetch_object();
				if(isset($fechasUltimaConexion->fecha_salida))
				{
					$ultimaConexion = $fechasUltimaConexion->fecha_salida;
				}
				else
				{
					$ultimaConexion = $fechasUltimaConexion->fecha_entrada;
				}
			}

			$idUsuario = Usuario::getIdUser(true);
			$mensajesOpiniones = $modeloForo->obteneMensajesDesdeUltimoAccesoOpiniones($idCurso, $ultimaConexion);
		}

		$idOpiniones = array();

		if($mensajesOpiniones->num_rows > 0)
		{
			while($mensajeOpiniones = $mensajesOpiniones->fetch_object())
			{
				//var_dump($mensajeOpiniones);
				$idOpiniones[] = $mensajeOpiniones->idopiniones;
			}
		}

		//Miro a ver el tema general
		foreach($idOpiniones as $idOpinion)
		{
			$esRespuestaOpiniones = $modeloForo->esResuesta($idOpinion, $idUsuario, $idCurso);
			if($esRespuestaOpiniones->num_rows > 0)
			{
				$esRespuestaOpiniones = $esRespuestaOpiniones->fetch_object();
				$idOpiniones['responseNueva'][] = $esRespuestaOpiniones->idopiniones_mensaje;
			}
		}

		if(isset($idOpiniones['responseNueva']) && count($idOpiniones['responseNueva']) > 0)
		{
			$idOpiniones['responseNueva'] = array_unique($idOpiniones['responseNueva']);
		}
		//var_dump($idOpiniones['responseNueva']);

		$html = '';
		$cont=0;
		$nRespuesta = 0;
		while($mensaje = $mensajes->fetch_object())
		{
			$mensajes_respuesta = $modeloForo->obtenerMensajesRespuesta($mensaje->idopiniones);
			$nRespuesta = $mensajes_respuesta->num_rows;
			$cont++;
			//if($mensaje->id_mensaje_respuesta == null)
			//{
				$html .= self::pintarUnMensaje($mensaje, false, $cont, $nRespuesta, $idOpiniones);

				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta, $idOpiniones);
					}
					$html.="</div>";
				}
			//}
		}

		return $html;
	}

	/*
	public static function listarMensajesForOneMsg($mensajes)
	{
		$modeloForo = new modeloForo();

		$html = '';
		$cont=0;
		$nRespuesta = 0;
		while($mensaje = $mensajes->fetch_object())
		{
			$mensajes_respuesta = $modeloForo->obtenerMensajesRespuesta($mensaje->idopiniones);
			$nRespuesta = $mensajes_respuesta->num_rows;
			$cont++;
			if($mensaje->id_mensaje_respuesta == null)
			{
				$html .= self::pintarUnMensaje($mensaje, false, $cont,$nRespuesta);

				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta);
					}
					$html.="</div>";
				}
			}
			else
			{
				$html .= self::pintarUnMensaje($mensaje, false, $cont,$nRespuesta);

				if($mensajes_respuesta->num_rows > 0)
				{
					$html.= "<div id='mensajesDeRespuesta".$cont."' class='ocultarRespuestasInicio'>";
					while($mensaje_respuesta = $mensajes_respuesta->fetch_object())
					{
						$html .= self::pintarUnMensaje($mensaje_respuesta, true, $cont, $nRespuesta);
					}
					$html.="</div>";
				}
			}
		}

		return $html;
	}*/

	/**
	 * Mostrar o no las opciones del pie de los mensajes
	 * Enter description here ...
	 * @param unknown_type $value
	 */
	public static function setConfigFooter($value)
	{
		self::$_footerOptions = $value;
	}

	public static function pintarUnMensaje($mensaje, $msg_respuesta = false, $cont, $nRespuesta, $idOpiniones)
	{

		$idUsuarioCreador = Usuario::getIdUser(true);

		$addClass = '';
		$addId = '';
		if($msg_respuesta)
		{
			$addClass =  'un_mensaje_respuesta respuestaForo';
			$addId = "id='ocultarRespuesta". $cont ."'";
			$imgNuevoForo = 'float:left; margin: -20px 10px 10px -71px;';
		}
		else
		{
			$addClass = 'beige';
			$imgNuevoForo = 'float:left; margin: -20px 10px 10px -30px;';
		}

		if(isset($mensaje->t_foto))
		{
			$nombrec = $mensaje->t_nombrec . ' (tutor/a)';
			$foto = $mensaje->t_foto;
		}
		else
		{
			$nombrec = $mensaje->a_nombrec;
			$foto = $mensaje->a_foto;
		}

		$html = '<div '. $addId .' class="elemento ' . $addClass . '">';

		//Icono de novedad para cuando el mensaje sea nuevo con respecto a la ultima visita
		if(in_array($mensaje->idopiniones, $idOpiniones) && $mensaje->idusuario != $idUsuarioCreador)
		{
			$html .= '<img src="imagenes/foro/new-forum.png" alt="" style="' . $imgNuevoForo . '"/>';
		}

		/*	'<div class="elementoAvatar"><img src="imagenes/fotos/' . $foto. '" alt="" /></div>' .*/
		$html .='<div class="elementoAll">' .
				'<div class="elementoNombreForo fleft"><p>' . Texto::textoFormateado($nombrec) . '</p></div>' .
				'<div class="fechaForo fright">' . Fecha::obtenerFechaFormateada($mensaje->fecha) . '</div>' .
				'<div class="clear"></div>' .
				'<div class="elementoContenido">' .
					'<span class="contenidoRespuesta">' . Texto::bbcode($mensaje->mensaje) . '</span></p>' .
				'</div>' .
				 '<div class="elementoPie optionsForo">' .
					'<ul class="elementoListOptions fright">';
						if(self::$_footerOptions)
						{
							if(!$msg_respuesta && $nRespuesta > 0)
							{

								if(isset($idOpiniones['responseNueva']) && count($idOpiniones['responseNueva'])> 0)
								{
									if(in_array($mensaje->idopiniones, $idOpiniones['responseNueva']))
									{
										$classResponse = 'galletaNueva';
									}
									else
									{
										$classResponse = 'galleta';
									}
								}
								else
								{
									$classResponse = 'galleta';
								}

								$html.= '<li class="' . $classResponse . '"><a href="#" onclick="desplegarRespuestas('.$cont.')" title="respuestas" data-translate-title="foro.respuestas"><span class="textGalleta">' . $nRespuesta .'</span><span data-translate-html="foro.respuestas">Respuestas</span></a></li>';
							}
							if(!Usuario::compareProfile(array('supervisor')))
							{
								$html .= '<li><a data-translate-title="foro.respondermensaje" href="opiniones/mensaje/responder/' . $mensaje->idopiniones . '" title="Responder a este mensaje" data-translate-title="foro.responser"><img src="imagenes/foro/responder.png" alt="responder" /><span class="alignMiddle" data-translate-html="foro.responder">Responder</span></a></li>';
							}

							if(Usuarios::comprobarPermisosLogueo($mensaje->idusuario))
							{
								$html .= '<li><a data-translate-title="foro.editarmensaje" href="opiniones/mensaje/editar/' . $mensaje->idopiniones . '" title="Editar este mensaje" data-translate-title="general.editar"><img src="imagenes/foro/editar.png" alt="editar" data-translate-title="general.editar" /><span data-translate-html="general.editar">Editar</span></a></li>' .
								'<li><a data-translate-title="foro.borrarmensaje" href="#" ' . Alerta::alertConfirmOnClick('borrarmensaje', '¿Deseas borrar el mensaje?', 'opiniones/mensaje/borrar/' . $mensaje->idopiniones) . ' title="Borrar mensaje" data-translate-title="general.borrar"><img src="imagenes/foro/borrar.png" alt="borrar" data-translate-title="general.borrar" /><span data-translate-html="general.borrar">Borrar</span></a></li>'; //borrar
							}
							else if(Usuario::compareProfile('tutor'))
							{
								$html .= '<li><a data-translate-title="foro.borrarmensaje" href="#" ' . Alerta::alertConfirmOnClick('borrarmensaje', '¿Deseas borrar el mensaje?', 'opiniones/mensaje/borrar/' . $mensaje->idopiniones) . ' title="Borrar mensaje"><img src="imagenes/foro/borrar.png" alt="" /><span>Borrar</span></a></li>'; //borrar
							}
						}
					$html .= '</ul>' .
					'<div class="clear"></div>' .
					'</div>' .
				'</div>' .
				'<div class="clear"></div>' .
			'</div>' .
			'<div class="clear"></div>';

		return html_entity_decode($html, ENT_QUOTES, 'ISO-8859-5');
	}

	public static function paginar($paginaActual, $paginaMax, $url)
	{
		$numMax = 3;
		$pageIni = 1;

		// calcula la pagina donde se empieza a mostrar el paginado
		if(($numMax - $paginaActual) < 0)
		{
			$pageIni = $paginaActual - $numMax;
		}

		$html = '<div class="paginado fright"><ul>';

		// volver a la primera pagina
		if($pageIni > 1)
		{
			$html .= '<li><a href="' . $url . '1" title="Ir a la primera p&aacute;gina" data-translate-title="foro.ir_primera" data-translate-html="foro.primera">Primera</a></li><li>...</li>';
		}

		// mostrar paginas antes de la pagina actual
		for($cont = $pageIni; $cont < $paginaActual; $cont++)
		{
			$html .= '<li><a href="' . $url . $cont . '" title="Ir a la p&aacute;gina ' . $cont . '" data-translate-title="foro.ir_pagina">' . $cont . '</a></li>';
		}

		$html .= '<li><b>' . $paginaActual . '</b></li>';

		// mostrar paginas despues de la actual
		if($paginaMax > $pageIni)
		{
			$pageNext = ($paginaActual + 1);
			$cont = 0;
			while($cont < 3 && $pageNext <= $paginaMax)
			{
				$html .= '<li><a href="' . $url . $pageNext . '" title="Ir a la p&aacute;gina ' . $pageNext . '" data-translate-title="foro.ir_pagina">' . $pageNext . '</a></li>';
				$pageNext++;
				$cont++;
			}
		}

		// ir a la ultima pagina
		if($paginaMax > ($paginaActual + 3))
		{
			$html .= '<li>...</li><li><a href="' . $url . $paginaMax .  '" title="Ir a la &uacute;ltima p&aacute;gina" data-translate-title="foro.ir_ultima" data-translate-html="foro.ultima">&Uacute;ltima</a></li>';
		}

		$html .= '</ul><div class="clear"></div></div><div class="clear"></div>';

		return $html;
	}
}