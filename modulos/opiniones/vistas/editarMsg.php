<div id="foroOpiniones">
	<div class='subtitle t_center'>
		<span data-translate-html="opiniones.titulo">
			Publicar opiniones
		</span>
	</div>
	<div class="popupIntro">
		<p class='t_justify' data-translate-html="opiniones.descripcion">
			Esta secci&oacute;n es un &aacute;mbito din&aacute;mico y flexible donde puede intercambiar ideas y compartir inquietudes con el profesorado del resto de los m&oacute;dulos y con sus alumnos/as acerca de todo tipo de temas.
		</p>
	</div>
	
	<div class='subtitleModulo' data-translate-html="general.editar">
		Editar mensaje
	</div>
	
	<div id="foroOpinionesEditarMensaje">
		<form method="post" action="opiniones/mensaje/editar/<?php echo $get['idMsg']?>">
			<ul>
				<li>
					<textarea id="foroOpinionesEditarMensaje_1" name="mensaje" rows="1" cols="1">
						<?php echo $mensaje->mensaje?>
					</textarea>
				</li>
				<li class="t_right">
					<button type="submit" data-translate-html="opinionesmodal.actualizar">
						Actualizar mensaje
					</button>
				</li>
			</ul>
		</form>
	</div>
</div>

<script type="text/javascript">
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link",
	});
</script>