<div id="foroOpiniones">
	<div class='subtitle t_center'>
		<span data-translate-html="opiniones.titulo">
			Publicar opiniones
		</span>
	</div>
	<div class="popupIntro">
		<p class='t_justify' data-translate-html="opiniones.descripcion1">Esta secci&oacute;n es un &aacute;mbito din&aacute;mico y flexible donde puede intercambiar
		ideas y compartir inquietudes con el profesorado del resto de los m&oacute;dulos y con sus alumnos/as
		acerca de todo tipo de temas.</p>
	</div>
	<?php if($_SESSION['perfil'] != 'supervisor'):?>
		<div id="foroOpinionesNuevoMensaje">
			<h1 class="subtitleModulo" data-translate-html="formulario.nuevo_mensaje">Nuevo mensaje</h1>
			<form method="post" action="opiniones/mensaje/nuevo">
				<ul>
					<li>
						<textarea id="foroOpinionesNuevoMensaje_1" name="mensaje" rows="1" cols="1"></textarea>
					</li>
					<li><button type="submit" data-translate-html="formulario.publicar">Publicar</button></li>
				</ul>
				<div class="clear"></div>
			</form>
		</div>
	<?php endif;?>
</div>

<script type="text/javascript">
	tinymce.init({
		plugins: ["link"],
		selector: "textarea",
		menubar: false,
		statusbar: false,
		toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link",
	});
</script>