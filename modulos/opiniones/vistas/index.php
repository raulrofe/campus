<div id="foroOpiniones">	
	<div class='subtitle t_center'>
		<img src="imagenes/foro/opiniones.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="opiniones.titulo">
			Publicar opiniones
		</span>
	</div>
	<br/>
	<?php if(!Usuario::compareProfile(array('alumno'))):?>
	<div class="popupIntro">
		<p class='t_justify' data-translate-html="opiniones.descripcion1">
			Esta secci&oacute;n es un &aacute;mbito din&aacute;mico y flexible donde puede intercambiar
		ideas y compartir inquietudes con el profesorado del resto de los cursos y con sus alumnos/as
		acerca de todo tipo de temas.
		</p>
	</div>
	<?php else: ?>
	<div class="popupIntro">
		<p class='t_justify' data-translate-html="opiniones.descripcion1">
			Esta secci&oacute;n es un &aacute;mbito din&aacute;mico y flexible donde puede intercambiar ideas y compartir inquietudes con 
			sus compa&ntilde;eros/as y con el tutor del curso.
		</p>
		<br/>
		<p class='t_justify' data-translate-html="opiniones.descripcion2">
			No se limite a leer el contenido de los mensajes, lance un tema al resto de sus compa&ntilde;eros/as para despertar su 
			reflexi&oacute;n o conteste a los mensajes remitidos al foro realizando sus aportaciones personales. 
			De esta forma el foro ser&aacute; un fiel reflejo de las motivaciones de todas las personas que acceden a &eacute;l.
		</p>
	</div>	
	<?php endif; ?>
	
	<?php if(!Usuario::compareProfile(array('supervisor'))):?>
		<div id="foroOpinionesNuevoMensajeBtn" class="button2">
			<a href="opiniones/mensaje/nuevo" data-translate-html="foro.nuevo">
				Escribir mensaje nuevo
			</a>
		</div>
		<br />
	<?php endif;?>
	
	<div id="foroOpinionesMensajes" class="listarElementos">
		<?php if($mensajes->num_rows > 0):?>
			<?php echo Opiniones::listarMensajes($mensajes)?>
		<?php else:?>
			<div class="elementosNoEncontrados">
				<strong data-translate-html="foro.nomensajes">
					No hay mensajes
				</strong>
			</div>
		<?php endif;?>
	</div>
	
	<?php echo Opiniones::paginar($numPagina, $maxPaging, 'opiniones/pagina/')?>
</div>

<script type="text/javascript" src="js-opiniones-default.js"></script>