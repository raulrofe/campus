<?php
class ModeloOpiniones extends modeloExtend
{
	public function obtenerMensajes($limitIni = null, $limitEnd = null)
	{
		$addQuery = null;
		if(isset($limitIni, $limitEnd))
		{
			$addQuery = ' LIMIT ' . $limitIni . ', ' . $limitEnd;
		}
		
		$sql = 'SELECT op.idopiniones, op.mensaje, op.fecha, op.idusuario, opr.idopiniones_mensaje AS id_mensaje_respuesta,' .
		' rh.foto AS t_foto, rh.nombrec AS t_nombrec, al.foto AS a_foto, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec' .
		' FROM opiniones AS op' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = op.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = op.idusuario' .
		' LEFT JOIN opiniones_respuesta AS opr ON opr.idopiniones_respuesta = op.idopiniones' .
		' WHERE opr.idopiniones_respuesta IS NULL' .
		' GROUP BY op.idopiniones ORDER BY op.fecha DESC' . $addQuery;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerNumMensajesPorAlumno($idAlumno, $idCurso)
	{
		$sql = 'SELECT op.idopiniones' .
		' FROM opiniones AS op' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = op.idusuario' .
		' LEFT JOIN staff AS st ON rh.idrrhh = st.idrrhh' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = op.idusuario' .
		' LEFT JOIN opiniones_respuesta AS opr ON opr.idopiniones_respuesta = op.idopiniones' .
		' WHERE op.idusuario = "' . $idAlumno . '" AND op.idcurso = ' . $idCurso .
		' GROUP BY op.idopiniones ORDER BY op.fecha DESC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado->num_rows;
	}
	
	public function obtenerMensajesRespuesta($idMsg)
	{
		$sql = 'SELECT op.idopiniones, op.mensaje, op.fecha, op.idusuario, opr.idopiniones_mensaje AS id_mensaje_respuesta,' .
		' rh.foto AS t_foto, rh.nombrec AS t_nombrec, al.foto AS a_foto, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec' .
		' FROM opiniones AS op' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = op.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = op.idusuario' .
		' LEFT JOIN opiniones_respuesta AS opr ON opr.idopiniones_respuesta = op.idopiniones' .
		' WHERE opr.idopiniones_respuesta IS NOT NULL AND opr.idopiniones_mensaje = ' . $idMsg .
		' GROUP BY op.idopiniones ORDER BY op.fecha DESC';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUnMensaje($idMensaje, $fromParent = true)
	{
		if($fromParent)
		{
			$addSql = 'opr.idopiniones_mensaje = op.idopiniones';
		}
		else
		{
			$addSql = 'opr.idopiniones_respuesta = op.idopiniones';
		}
		
		$sql = 'SELECT op.idopiniones, op.mensaje, op.fecha, op.idusuario, opr.idopiniones_mensaje AS idforo_mensaje_respondido,' .
		' rh.foto AS t_foto, rh.nombrec AS t_nombrec, al.foto AS a_foto, CONCAT(al.nombre, " ", al.apellidos) AS a_nombrec' .
		' FROM opiniones AS op' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = op.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = op.idusuario' .
		' LEFT JOIN opiniones_respuesta AS opr ON ' . $addSql .
		' WHERE op.idopiniones = ' . $idMensaje .
		' GROUP BY op.idopiniones';

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function actualizarMensaje($idMsg, $mensaje)
	{
		$sql = 'UPDATE opiniones SET mensaje = "' . $mensaje . '"' .
		' WHERE idopiniones = ' . $idMsg;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function nuevoMensaje($idUsuario, $idCurso, $mensaje)
	{
		$sql = 'INSERT INTO opiniones (mensaje, idusuario, fecha, idcurso)' .
		' VALUES ("' . $mensaje . '", "' . $idUsuario . '", "' . date('Y-m-d H:i:s') . '", ' . $idCurso . ')';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function nuevoMensajeRespuesta($idMsg, $idMsgRespuesta)
	{
		$sql = 'INSERT INTO opiniones_respuesta (idopiniones_mensaje, idopiniones_respuesta)' .
		' VALUES (' . $idMsg . ', ' . $idMsgRespuesta . ')';
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function borrarMensaje($idMsg)
	{
		$sql = 'DELETE FROM opiniones WHERE idopiniones = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obteneMensajesDesdeUltimoAccesoOpiniones($idCurso, $ultimaConexion)
	{
		$sql = "SELECT o.idopiniones" .
		" FROM opiniones AS o" .
		" WHERE o.fecha > '" . $ultimaConexion . "'" .
		" AND o.idcurso = " . $idCurso;
	
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
		
	public function esResuesta($idMensajeOpiniones, $idUsuario, $idCurso)
	{
		$sql = "SELECT *" .
		" FROM opiniones_respuesta AS ores" .
		" LEFT JOIN opiniones AS o ON o.idopiniones = ores.idopiniones_respuesta" .
		" WHERE ores.idopiniones_respuesta = " . $idMensajeOpiniones . 
		" AND o.idusuario <> '" . $idUsuario . "'" . 
		" AND o.idcurso = " . $idCurso;
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUltimaFechaConexionAlumno($idMatricula)
	{
		$sql = "SELECT la.fecha_entrada, la.fecha_salida" .
		" FROM logs_acceso AS la" .
		" WHERE idmatricula = " . $idMatricula . 
		" AND idlugar = 6" .
		" ORDER BY la.idaccesos DESC" . 
		" LIMIT 0,1";
	
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerUltimaFechaConexionTutor($idMatricula)
	{
		$sql = "SELECT las.fecha_entrada, las.fecha_salida" .
		" FROM logs_acceso_staff AS las" .
		" WHERE idstaff = " . $idMatricula . 
		" AND idlugar = 6" .
		" ORDER BY las.idaccesos DESC" . 
		" LIMIT 0,1";
	
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
}