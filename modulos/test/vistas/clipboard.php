	<?php require DIRNAME(__FILE__) . '/intro_autoevaluaciones.php';?>	
<div id="vistaAnimada">
	<div id="clipboard">
			<div class="menuVistaClipboard menuVista2" style="float:right">
				<div class="vista1" >
					<a href="aula/autoevaluaciones/listado">
						<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.listado" alt="Listado"/></a>
				</div>
				<div class="vista2" >
					<a href="#" onclick="return false;">
						<img src="imagenes/archivador/vista-nueva.png"  data-translate-title="general.clasica" alt="Vista Clásica"/></a>
				</div>
			</div>
			
			<form method='post' action=''>
			<div>
				<div class="allTest">
					<?php if(mysqli_num_rows($resultado_autoevaluacion2) > 0): ?>	
					
					<!-- CABECERA -->
					
					<div class="registroTestCabecera">
						<div class="tituloClipboard" data-translate-html="autoevaluaciones.autoevaluaciones">
							Autoevaluaciones
						</div>
						<div class="notaClipboard" data-translate-html="autoevaluaciones.superado">
							Superado
						</div>
						<div class="numeroAccesoClipboard" data-translate-html="autoevaluaciones.intentos">
							Intentos disponibles
						</div>		
					</div>	

					<div class="registrosTests">			
						<?php while($f = mysqli_fetch_assoc($resultado_autoevaluacion2)): ?>	
							<?php 
							
								//Obtengo el numero de accesos posibles a la autoevaluacion
								$numeroAccesosTotal = $mi_autoevaluacion->configuracion_numero_acceso($f['idautoeval']); 
								$mi_autoevaluacion->set_autoeval($f['idautoeval']);				
								$preguntas = $mi_autoevaluacion->preguntas();	
								
								$finalizado = true;
								
								if($_SESSION['perfil'] == 'alumno')
								{
									$logSuperado = 0;
									
									//obtengo el numero de intentos del alumno
									$idmatricula = $mi_alumno->obtenerIdmatricula();
									$resultado = $mi_autoevaluacion->numero_intentos($idmatricula,$f['idautoeval']);
									$numero_intentos = mysqli_num_rows($resultado);
									$intentosRestantes = $numeroAccesosTotal - $numero_intentos;
									
									//Obtengo la nota de los alumnos
									$resultado_test = $mis_notas->obtenerNotasTestsAutoevalAlumno($_SESSION['idusuario'], $_SESSION['idcurso'], $f['idautoeval']);	
									if(mysqli_num_rows($resultado_test) > 0)
									{
										while($notaTestAlumno = mysqli_fetch_assoc($resultado_test))
										{
											if($notaTestAlumno['nota'] >= $f['nota_minima'])
											{
												$logSuperado++;
											}
										}

										if($logSuperado > 0) $finalizado = true;
										else $finalizado = false;
										
									}
									else
									{
										$finalizado = false;
									}
								}
																
							?>
							<?php if(mysqli_num_rows($preguntas) > 0):?>
								<div class="registroTest">
									<div class="tituloClipboard" style="width:350px;">
										<img src='imagenes/clipboard/test.png' alt='' style='margin-right:10px;vertical-align:middle;' />
										<span style='vertical-align:middle'>
											<a href='aula/autoevaluaciones/<?php  echo $f['idautoeval'] ?>'>
												<?php echo $f['titulo_autoeval'] ?>
											</a>
										</span>
									</div>
									
									<div class="notaClipboard">
										<?php if($finalizado): ?>
											<img src="imagenes/correos/check_ok.png"/>
										<?php else: ?>
											<img src="imagenes/correos/check_fail.png"/>
										<?php endif ?>
									</div>
									
									<div class="numeroAccesoClipboard">
										<?php if(!isset($intentosRestantes)): ?>
											<?php echo $numeroAccesosTotal ?>
										<?php else: ?>
											<?php echo $intentosRestantes ?>
										<?php endif ?>
									</div>
									
								</div>	
								<div class="clear"></div>							
							<?php endif ?>
						<?php endwhile ?>
						</div>
					<?php else: ?>
						<p class='t_center'data-translate-html="autoevaluaciones.noauto">
							No existen autoevaluaciones
						</p>
					<?php endif ?>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="autoeval_boton_atras" class="hide">
	<a title="Ir a atrás"
		onclick="LibPopupModal.historyBack('popupModal_autoevaluaciones'); return false;" href="#"
		class="popupModalHeaderHistory_back">
			<img alt="" src="imagenes/popupModal/window_back.png">
	</a>
</div>

<script type="text/javascript">
	$('#popupModal_autoevaluaciones .popupModalHeaderIconReload').removeClass('popupModalHeaderIconDisabled');
	$('#popupModal_autoevaluaciones .popupModalHeaderHistory_back').replaceWith($('#autoeval_boton_atras').html());		
	var count = $('#popupModal_autoevaluaciones').find('.popupHistory li').size();

	$('#popupModal_autoevaluaciones').find('.popupHistory li').remove();
</script>

<script type="text/javascript" src="js-test-vista_autoevaluacion.js"></script>

<!--  Cambio de aspecto para scroll -->

<script type="text/javascript" src="js-test-mousewheel.js"></script>
<script type="text/javascript" src="js-test-mCustomScrollbar.js"></script>
<script type="text/javascript">
	if($.browser.msie == undefined || ($.browser.msie != undefined && $.browser.msie == true && $.browser.version != '7.0' && $.browser.version != '6.0'))
	{
		$(".registrosTests").mCustomScrollbar({
			set_height: '280px',
			advanced: {
				updateOnBrowserResize: true,
				updateOnContentResize: true
			},
			scrollButtons: {
				enable: true
			}
		});
	}
</script>