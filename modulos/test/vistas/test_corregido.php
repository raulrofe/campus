<?php
if (isset($correccion['errores']))
	$preguntas_acertadas = count($correccion['errores']);
else {
	$preguntas_acertadas = 0;
}
$preguntas_falladas = mysqli_num_rows($registros_preguntas) - $preguntas_acertadas;
?>
<div id='autoevaluaciones'>	
	<div class='subtitle t_center' style='padding: 0 20px;'>
		<span data-translate-html="autoevaluaciones.test">
			AUTOEVALUACI&Oacute;N
		</span>
	</div>
	<div id='test'>	
		<div>
			<span class='negrita cabeceraTest' data-translate-html="autoevaluaciones.test">
				Test: 
			</span>
			<?php echo Texto::textoPlano($f_autoevaluacion['titulo_autoeval']); ?>
		</div>
		<div>
			<span class='negrita cabeceraTest' data-translate-html="autoevaluaciones.preguntas">
				Preguntas: 
			</span>
			<?php echo mysqli_num_rows($registros_preguntas); ?>
		</div>		
		<div>
			<span class='negrita cabeceraTest' data-translate-html="autoevaluaciones.preguntasacertadas">
				Preguntas acertadas: 
			</span>
			<?php echo $preguntas_acertadas; ?>
		</div>
		<div>
			<span class='negrita cabeceraTest' data-translate-html="autoevaluaciones.preguntasfalladas">
				Preguntas falladas: 
			</span>
			<?php echo $preguntas_falladas; ?>
		</div>
		<div>
			<span class='negrita cabeceraTest' data-translate-html="autoevaluaciones.resultado">
				Resultado:
			</span>
			<?php echo $correccion['nota']; ?>
		</div>
		<div>

			<div class='notaAlumnoFinal'>
				<span class='tituloResultado' data-translate-html="autoevaluaciones.resultadofinal">
					Resultado final
				</span>
				<br/>
				<br/>
				<?php
				if ($correccion['nota'] < $correccion['nota_minima'])
					echo "<span class='rojo' style='padding: 0 32px 15px;'>" . $correccion['nota'] . "</span>";
				if ($correccion['nota'] >= $correccion['nota_minima'])
					echo "<span class='verde' style='padding: 0 32px 15px;'>" . $correccion['nota'] . "</span>";
				?>
			</div>
		</div>
	</div>	
	<?php
	$numero = 0;
	$numero_eleccion = 1;
	?>
	<div style='padding: 0 20px;'>
		<?php
		if (!empty($registros_preguntas) and ( mysqli_num_rows($registros_preguntas) > 0)) {
			while ($f = mysqli_fetch_assoc($registros_preguntas)) {

				//separo el idpregunta del id respuesta
				if (!empty($post[$numero_eleccion . 'eleccion'])) {
					$ids = explode('_', $post[$numero_eleccion . 'eleccion']);
					$id_contestacion_pregunta = $ids[0];
					$id_contestacion_respuesta = $ids[1];

					$sql_tu = "SELECT * from respuesta where idpreguntas = " . $id_contestacion_pregunta . " and idrespuesta = " . $id_contestacion_respuesta;
					$resultado_tu = $mi_autoevaluacion->consultaSql($sql_tu);
					$f_tu = mysqli_fetch_assoc($resultado_tu);
				} else
					$respuesta_blanco = true;

				$numero = $numero + 1;
				echo "<div class='PreguntaMasRespuesta' style='margin-bottom:7px;padding:0;'>";
				echo "<div class='pregunta'>" . $numero . ". " . $f['pregunta'];
				if (!empty($f['imagen_pregunta'])) {
					echo "<img alt='' src='" . $f['imagen_pregunta'] . "' class='imagenAutoevaluacion' />";
				}
				echo "</div>";

				//consulta para obtener todas las repuestas
				$sql = "SELECT * from respuesta where idpreguntas = " . $f['idpreguntas'];
				$resultado = $mi_autoevaluacion->consultaSql($sql);
				$miletra = 0;

				//Bucle en el cual muestro las respuestas
				while ($fila = mysqli_fetch_assoc($resultado)) {
					echo "<div class='las_respuestas'>";
					//obtengo la letra
					$miletra = $miletra + 1;
					$letra = Autoevaluaciones::obtener_letra($miletra);

					// Si existe la pregunta enviada por post y es igual a la obtenida en la sql
					if (isset($id_contestacion_pregunta) and $id_contestacion_pregunta == $f['idpreguntas']) {

						// Si el id de respuesta obtenido por post es igual a la obtenida en la sql
						if ($id_contestacion_respuesta == $fila['idrespuesta']) {
							if ($fila['correcta'] == 1) {
								if ($datosConfTest['correccion'] == 2)
									echo "<div class='info_respuesta_ok'><p>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</p></div>";
								else
									echo "<div style='padding-left:30px;'><p>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</p></div>";
							}
							else {
								if ($datosConfTest['correccion'] == 2)
									echo "<div class='info_respuesta_fail'><p>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</p></div>";
								else
									echo "<div style='padding-left:30px;'><p>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</p></div>";
							}
						}

						else {
							echo "<div class='info_respuesta_blanco'>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</div>";
						}
					} else { //controla que se muestres las respuestas en blanco
						echo "<div class='info_respuesta_blanco'>" . $letra . " " . Texto::textoPlano($fila['respuesta']) . "</div>";
					}
					echo "</div>";

					//Obtengo la respuesta correcta de esa pregunta
					if ($fila['correcta'] == 1)
						$respuesta_correcta = $letra . $fila['respuesta'];
				}


				$fallo = 1;

				if (isset($correccion['errores'])) {
					$numero_aciertos = count($correccion['errores']);
					if ($numero_aciertos > 0) {
						for ($j = 0; $j <= $numero_aciertos - 1; $j++) {
							if ($correccion['errores'][$j] == $f['idpreguntas']) {
								$fallo = 0;
							}
						}
					}
				}

				if ($fallo == 1) {
					if ($datosConfTest['correccion'] == 1 || $datosConfTest['correccion'] == 2) {
						echo '<div class="correccionRespuestasRojo">';
						echo '<span class="rojo" data-translate-html="evaluaciones.incorrecta">&#161;Respuesta incorrecta&#33;</span><br/>';
						if ($datosConfTest['correccion'] == 2) {
							echo '<br/><span class="verde">: ' . $respuesta_correcta . '</span>';
						}
						echo '</div>';
					}
				} else {
					if ($datosConfTest['correccion'] == 1 || $datosConfTest['correccion'] == 2) {
						echo '<div class="correccionRespuestasVerde">';
						echo '<span class="verde" data-translate-html="autoevaluaciones.muybien">'
						. '¡Muy bien!'
						. '</span>';
						echo '</div>';
					}
				}

				echo "</div>";
				$numero_eleccion++;
			}
		}
		?>
	</div>

	<p class='t_center'>
		<a href='aula/autoevaluaciones/<?php echo $idautoevaluacion; ?>'  data-translate-html="autoevaluaciones.repetir">
			Repetir test de autoevaluaci&oacute;n
		</a> | 
		<a href='aula/autoevaluaciones/grafica' data-translate-html="autoevaluaciones.ir">
			Ir a Autoevaluaciones
		</a>
	</p>

</div>

<script type="text/javascript">
	$('#popupModal_autoevaluaciones .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');
	$('#popupModal_autoevaluaciones_wrapper_aux').scrollTop(0);
</script>