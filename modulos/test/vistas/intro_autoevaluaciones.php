<div class="caja_separada">
	<div class='subtitle t_center'>
		<img src="imagenes/clipboard/tests.png" alt="" style="vertical-align:middle;" />
		<span data-translate-html="autoevaluaciones.titulo">
			Test / Ejercicios de autoevaluaci&oacute;n
		</span>
	</div>
	<br/>
	<div class='popupIntro'>
		<p class="t_center" data-translate-html="autoevaluaciones.descripcion">
			Selecciona el ejercicio de autoevaluaci&oacute;n que vas a realizar:
		</p>
	</div>
</div>