<div class='caja_separada'>
	<?php require DIRNAME(__FILE__) . '/intro_autoevaluaciones.php';?>	
	<div id="vistaClasica">
	
	
		<div class="menuVista2 fright">
			<div class="vista3" >
				<a href="#" onclick="return false;">
					<img src="imagenes/archivador/vista-clasica.png" data-translate-title="general.listado" alt="Listado"/>
				</a>
			</div>
			<div class="vista4" >
				<a href="aula/autoevaluaciones/grafica">
					<img src="imagenes/archivador/vista-nueva.png" data-translate-title="general.clasica" alt="Vista Clásica"/>
				</a>
			</div>
		</div>
		<div class="clear"></div>
		<br/>
	<div>
		<form method='post' action=''>
			<div>
				
						<?php  if(mysqli_num_rows($resultado_autoevaluacion) > 0): ?>		
											
							<!-- CABECERA -->
							<div class="listRegistroTestCabecera">
								<div class="tituloClasica" data-translate-html="autoevaluaciones.autoevaluaciones">
									Autoevaluaci&oacute;n
								</div>
								<div class="notaClasica" data-translate-html="autoevaluaciones.superado">
									Superado
								</div>
								<div class="accesoClasica" data-translate-html="autoevaluaciones.intentos" style="margin-right:10px;">
									Intentos disponibles
								</div>
							</div>
							<div class="clear"></div>
									
							<?php while($f = mysqli_fetch_assoc($resultado_autoevaluacion)): ?>	
								<?php
								//Obtengo el numero de accesos posibles a la autoevaluacion
								$numeroAccesosTotal = $mi_autoevaluacion->configuracion_numero_acceso($f['idautoeval']); 
								$mi_autoevaluacion->set_autoeval($f['idautoeval']);				
								$preguntas = $mi_autoevaluacion->preguntas();	
							
								$finalizado = true;
							
								if($_SESSION['perfil'] == 'alumno')
								{
									$logSuperado = 0;
								
									//obtengo el numero de intentos del alumno
									$idmatricula = $mi_alumno->obtenerIdmatricula();
									$resultado = $mi_autoevaluacion->numero_intentos($idmatricula,$f['idautoeval']);
									$numero_intentos = mysqli_num_rows($resultado);
									$intentosRestantes = $numeroAccesosTotal - $numero_intentos;
									
									//Obtengo la nota de los alumnos
									$resultado_test = $mis_notas->obtenerNotasTestsAutoevalAlumno($_SESSION['idusuario'], $_SESSION['idcurso'], $f['idautoeval']);	
									if(mysqli_num_rows($resultado_test) > 0)
									{
										while($notaTestAlumno = mysqli_fetch_assoc($resultado_test))
										{
											if($notaTestAlumno['nota'] >= $f['nota_minima'])
											{
												$logSuperado++;
											}
										}

										if($logSuperado > 0) $finalizado = true;
										else $finalizado = false;
										
									}
									else
									{
										$finalizado = false;
									}
								}							
								?>
								
							<?php if(mysqli_num_rows($preguntas) > 0):?>
								<div class="listRegistroTest">
									<div class="tituloClasica">
										<img src='imagenes/clipboard/test.png' style='margin-right:10px;vertical-align:middle;' />
										<span style='vertical-align:middle'>
											<a href='aula/autoevaluaciones/<?php  echo $f['idautoeval'] ?>'>
												<?php echo $f['titulo_autoeval'] ?>
											</a>
										</span>
									</div>
									
									<div class="notaClasica">
										<?php if($finalizado): ?>
											<img src="imagenes/correos/check_ok.png" />
										<?php else: ?>
											<img src="imagenes/correos/check_fail.png" />
										<?php endif ?>
									</div>
									
									<div class="accesoClasica">
										<?php if(!isset($intentosRestantes)): ?>
											<?php echo $numeroAccesosTotal ?>
										<?php else: ?>
											<?php echo $intentosRestantes ?>
										<?php endif ?>
									</div>
									
								</div>	
								<div class="clear"></div>							
							<?php endif ?>
						<?php endwhile ?>
					<?php else: ?>
						<p class='t_center' data-translate-html="autoevaluaciones.noauto">
							No existen autoevaluaciones
						</p>
					<?php endif ?>
				</div>
		</form>
	</div>
</div>

<div id="autoeval_boton_atras" class="hide">
	<a title="Ir a atrás"
		onclick="LibPopupModal.historyBack('popupModal_autoevaluaciones'); return false;" href="#"
		class="popupModalHeaderHistory_back">
			<img alt="" src="imagenes/popupModal/window_back.png">
	</a>
</div>
</div>
<script type="text/javascript">
	$('#popupModal_autoevaluaciones .popupModalHeaderIconReload').removeClass('popupModalHeaderIconDisabled');
	$('#popupModal_autoevaluaciones .popupModalHeaderHistory_back').replaceWith($('#autoeval_boton_atras').html());		
	var count = $('#popupModal_autoevaluaciones').find('.popupHistory li').size();

	$('#popupModal_autoevaluaciones').find('.popupHistory li').remove();
</script>

<script type="text/javascript" src="js-test-vista_autoevaluacion.js"></script>