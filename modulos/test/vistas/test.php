<?php if (empty($idcalificaciones)) $idcalificaciones = 0; ?>
<div id='autoevaluaciones'>
	<div class='subtitle t_center' style='padding: 0 20px;'>
		<span data-translate-html="autoevaluaciones.autoevaluacionM">
			AUTOEVALUACI&Oacute;N
		</span>
	</div>
	<div id='test'>
		<div>
			<span class='negrita' data-translate-html="autoevaluaciones.test">
				Test: 
			</span>
			<?php echo Texto::textoPlano($f_autoevaluacion['titulo_autoeval']); ?>
		</div>
		<div>
			<span class='negrita' data-translate-html="autoevaluaciones.preguntas">
				N&deg; preguntas: 
			</span><?php echo mysqli_num_rows($registros_preguntas); ?>
		</div>
		<div>
			<span class='negrita' data-translate-html="autoevaluaciones.blanco">
				Penalización respuesta en blanco: 
			</span><?php echo '0' //echo mysqli_num_rows($f_autoevaluacion['penalizacion_blanco']);  ?>
		</div>
		<div>
			<span class='negrita' data-translate-html="autoevaluaciones.error">
				Penalización respuesta errónea: 
			</span>
				<?php echo '0' //echo mysqli_num_rows($f_autoevaluacion['penalizacion_nc']);  ?>
		</div>
		<?php if ($datosConfTest ['tiempo'] != '00:00:00'): ?>
			<div id="defaultCountdown" data-countdown="<?php echo (date('H', strtotime($datosConfTest['tiempo'])) * 60) + (date('i', strtotime($datosConfTest['tiempo']))); ?>"></div>
		<?php endif; ?>
		<div class="clear"></div>
	</div>
	<div style='padding: 0 20px;'>
		<form name='frm_test' action='' method='post'>
			<?php
			$numero = 0;
			if (!empty($registros_preguntas) and ( mysqli_num_rows($registros_preguntas) > 0)) {
				while ($f = mysqli_fetch_assoc($registros_preguntas)) {
					$res = $mi_autoevaluacion->buscar_respuestas($f['idpreguntas']);
					if (mysqli_num_rows($res) > 0) {
						$numero = $numero + 1;
						echo "<div class='PreguntaMasRespuesta' style='margin-bottom:5px;'>
									<div class='pregunta'>" . $numero . ". " . Texto::textoPlano($f['pregunta']);
						if (!empty($f['imagen_pregunta'])) {
							echo "<img alt='' src='http://campusaulainteractiva.aulasmart.net/" . $f['imagen_pregunta'] . "' style='margin:10px 0 0 10px;'/>";
						}
						echo "</div>";
						$sql = "SELECT * from respuesta where idpreguntas = " . $f['idpreguntas'];
						$resultado = $mi_autoevaluacion->consultaSql($sql);
						$miletra = 0;
						while ($fila = mysqli_fetch_assoc($resultado)) {
							$miletra = $miletra + 1;
							$letra = Autoevaluaciones::obtener_letra($miletra);
							echo "<div class='respuesta'>
										<div class='tu_respuesta'><input type='radio' name='" . $numero . "eleccion' value='" . $f['idpreguntas'] . "_" . $fila['idrespuesta'] . "' />&nbsp;&nbsp;" . $letra . " " . Texto::textoPlano($fila['respuesta']);
							if ($fila['imagen_respuesta']) {
								echo "<img alt='' src='http://campusaulainteractiva.aulasmart.net/" . $fila['imagen_respuesta'] . "' style='margin:10px 0 0 10px;'/>";
							}
							echo "</div>
									 </div><div class='clear'></div>";
						}
						echo "</div>";
					}
				}
				echo "  <input type='hidden' name='idautoevaluacion' value='" . $idautoevaluacion . "' />
						<input type='hidden' name='idcalificaciones' value='" . $idcalificaciones . "' />
						<input type='hidden' name='corregir' value='true' />
						<div class='t_center'>
						<input type='submit' data-translate-value='formularios.enviarrespuestas' value='Enviar respuestas' class='enviar_autoevaluacion'/></div>";
			}
			?>
		</form>
	</div>
</div>

<div id="autoeval_boton_atras_confirmacion" class="hide">
	<a class="popupModalHeaderHistory_back" href="#" onclick="$('#autoeval_boton_atras_confirmacion_alert a').click(); return false;">
		<img alt="" src="imagenes/popupModal/window_back.png">
	</a>
</div>

<div id="autoeval_boton_atras_confirmacion_alert" class="hide">
	<a href="#" <?php echo Alerta::alertConfirmOnClick('salirautoevaluacion', '¿Realmente quieres salir de esta autoevaluación?', 'aula/autoevaluaciones'); ?>>
		<img alt="" src="imagenes/popupModal/window_back.png">
	</a>
</div>

<script type="text/javascript" src="js-test-ini.js"></script>

<script type="text/javascript">
		$('#popupModal_autoevaluaciones .popupModalHeaderIconReload').addClass('popupModalHeaderIconDisabled');
		$('#popupModal_autoevaluaciones .popupModalHeaderHistory_back').replaceWith($('#autoeval_boton_atras_confirmacion').html());
</script>