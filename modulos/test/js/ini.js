	(function($) {
		$.countdown.regional['es'] = {
			labels: ['Años', 'Meses', 'Semanas', 'Días', 'Horas', 'Minutos', 'Segundos'],
			labels1: ['Año', 'Mes', 'Semana', 'Día', 'Hora', 'Minuto', 'Segundo'],
			compactLabels: ['a', 'm', 's', 'g'],
			whichLabels: null,
			timeSeparator: ':', isRTL: false};
		$.countdown.setDefaults($.countdown.regional['es']);
	})(jQuery);

	var objDate = new Date();
	objDate.setMinutes(objDate.getMinutes() + parseInt($('#defaultCountdown').attr('data-countdown')));
	$('#defaultCountdown').countdown({until: objDate, onExpiry: testCloseCountdown});
	//objDate.setTime(objDate.getTime() + 6000);

	$('#defaultCountdown').countdown({until: objDate, onExpiry: testCloseCountdown});

	function testCloseCountdown()
	{
		alert('Se acabo el tiempo');
		$('form[name="frm_test"]').submit();
	}
	
	$(".pregunta img, .respuesta img").thumbPopup({
		  imgSmallFlag: "_s",
		  imgLargeFlag: "_l",
		  loadingHtml: "<span style='padding:5px;'>Cargando</span>"
		});
	
function test_back_btn()
{
	$('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').remove();
}
