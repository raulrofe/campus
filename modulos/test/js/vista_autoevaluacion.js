function cambiarVista(id)
{
	if(id == 'vistaClasica')
	{
		$('#clipboard').addClass('hide');
		$('#vistaClasica').removeClass('hide');
	}
	else
	{
		$('#clipboard').removeClass('hide');
		$('#vistaClasica').addClass('hide');	
	}
}
