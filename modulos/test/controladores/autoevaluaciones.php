<?php
require_once(PATH_ROOT."modulos/autoevaluaciones/modelos/modelo.php");
require_once(PATH_ROOT."modulos/autoevaluaciones/autoevaluaciones.php");
require_once(PATH_ROOT."modulos/notas/modelos/modelo.php");

mvc::cargarModuloSoporte('autoevaluaciones');
mvc::cargarModuloSoporte('notas');

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

if(isset($get['idautoevaluacion'])) $idautoevaluacion = $get['idautoevaluacion'];
if(isset($post['idautoevaluacion'])) $idautoevaluacion = $post['idautoevaluacion'];

$mi_autoevaluacion = new Autoevaluacion();
$mi_alumno = new Alumnos();
$mi_modulo = new Modulos();
$mis_notas = new ModeloNotas();

//obtengo los registros de los test de autoevaluacion del curso
$resultado_autoevaluacion = $mi_autoevaluacion->autoevalCursoOptimize(Usuario::getIdCurso());
//$resultado_autoevaluacion2 = $mi_autoevaluacion->autoeval_curso();
$resultado_autoevaluacion2 = $resultado_autoevaluacion;

//Cuando exista el id tengo que comprobar el numero de intentos
if(!empty($idautoevaluacion))
{
	
	//El numero de accesos segun la configuracion
	$conf_accesos = $mi_autoevaluacion->configuracion_numero_acceso($idautoevaluacion);
		
	//obtengo los registros para las preguntas
	$mi_autoevaluacion->set_autoeval($idautoevaluacion);
	$registros_preguntas = $mi_autoevaluacion->preguntas();
	
	if($_SESSION['perfil'] == 'alumno')
	{
		//obtengo el numero de intentos
		$idmatricula = $mi_alumno->obtenerIdmatricula();
		$resultado = $mi_autoevaluacion->numero_intentos($idmatricula,$idautoevaluacion);
		$numero_intentos = mysqli_num_rows($resultado);
	}
	else $numero_intentos = 0;
	
	if($numero_intentos<$conf_accesos)
	{
		$dispones = Alerta::getStringAlerts("dispones", "Dispones de ");
		$gastado = Alerta::getStringAlerts("gastado", " intentos para esta autoevaluación, llevas gastado ");
		$intentos = Alerta::getStringAlerts("intentos", " intentos, ¿deseas continuar?");
		$string = $dispones .$conf_accesos. $gastado .$numero_intentos. $intentos;

		Alerta::alertConfirm("", $string, "aula/autoevaluaciones/test/".$idautoevaluacion);
	}
	else
	{
		Alerta::mostrarMensajeInfo('nmaximo','Has realizado el n&uacute;mero m&aacute;ximo de intentos para esta autoevaluaci&oacute;n');
	}
}


if(isset($get['vista']) && $get['vista'] == 'listado')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'autoevaluaciones');	
}
else
//if(isset($get['vista']) && $get['vista'] == 'grafica')
{
	require_once mvc::obtenerRutaVista(dirname(__FILE__), 'clipboard');
}



