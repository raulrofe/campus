<?php

$post = Peticion::obtenerPost();
$get = Peticion::obtenerGet();

require_once(PATH_ROOT."modulos/autoevaluaciones/modelos/modelo.php");
require_once(PATH_ROOT."modulos/autoevaluaciones/autoevaluaciones.php");

if(isset($get['idautoevaluacion'])) $idautoevaluacion = $get['idautoevaluacion'];
if(isset($post['idautoevaluacion'])) $idautoevaluacion = $post['idautoevaluacion'];

$mi_autoevaluacion = new Autoevaluacion();
$mi_alumno = new Alumnos();


//comprobamos si la autoevaluacion corresponde al curso
$verTest=false;
$verTest = $mi_autoevaluacion->compruebaAutoeval($idautoevaluacion);

if(!$verTest)
{
	Url::redirect('aula/autoevaluaciones');
}

//Cuando exista el id tengo que comprobar el numero de intentos
if(!empty($idautoevaluacion) and is_numeric($idautoevaluacion))
{
	//El numero de accesos segun la configuracion
	$conf_accesos = $mi_autoevaluacion->configuracion_numero_acceso($idautoevaluacion);
	
	//establezco autoevaluacion
	$mi_autoevaluacion->set_autoeval($idautoevaluacion);
	
	//obtengo los datos de la autoevaluacion
	$resultado_autoevaluacion = $mi_autoevaluacion->buscar_autoeval();
	$f_autoevaluacion = mysqli_fetch_assoc($resultado_autoevaluacion);
	
	//obtengo los datos de configuracion tiempo y modo de correccion
	$resultado_confTest = $mi_autoevaluacion->datosConfTest($idautoevaluacion);
	$datosConfTest = mysqli_fetch_assoc($resultado_confTest);	
	
	//obtengo los registros para las preguntas
	$registros_preguntas = $mi_autoevaluacion->preguntas();
		
	if($_SESSION['perfil'] == 'alumno')
	{
		//obtengo el id de matricula
		$idmatricula = $mi_alumno->obtenerIdmatricula();
		
		// Calculo el numero de intentos
		$resultado = $mi_autoevaluacion->numero_intentos($idmatricula,$idautoevaluacion);
		$numero_intentos = mysqli_num_rows($resultado);
	}
	else $numero_intentos = 0;
	
	
	if(!isset($post['corregir']))
	{
		//Creo el registro para ver que se ha accedido
		if($_SESSION['perfil'] == 'alumno' && $numero_intentos < $conf_accesos)
		{
			$idcalificaciones = $mi_autoevaluacion->cuenta_intento($idmatricula,$idautoevaluacion);	
		}
		else $idcalificaciones=0;
	}
	
	else if(isset($post['idcalificaciones']) && is_numeric($post['idcalificaciones']))
	{
		$correccion = $mi_autoevaluacion->corregir_test();
		if($_SESSION['perfil'] == 'alumno')
		{
			if($numero_intentos <= $conf_accesos and $_SESSION['perfil'] == 'alumno')
			{
				$mi_autoevaluacion->insertar_calificacion($idmatricula,$idautoevaluacion,$correccion['nota'],$post['idcalificaciones']);
				$mi_autoevaluacion->respuestasAlumnos($post['idcalificaciones']);
			}
		}
	}
}

// Cargo la vista correspondiente
if(!isset($correccion['nota']) && $numero_intentos < $conf_accesos)
{
	require_once(PATH_ROOT."modulos/test/vistas/test.php");
}

elseif(isset($correccion['nota']))
{
	require_once(PATH_ROOT."modulos/test/vistas/test_corregido.php");
}

else
{
	//Url::redirect('aula/autoevaluaciones');
}
