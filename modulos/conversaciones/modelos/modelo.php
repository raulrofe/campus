<?php

class ModeloConversaciones extends modeloExtend {

	public function obtenerConversaciones($id_usuario, $perfil) {
		$sql = 'SELECT c.*' .
			' FROM conversaciones as c' .
			' LEFT JOIN conversaciones_participantes as cp ON c.id = cp.id_conversacion' .
			' WHERE cp.id_' . $perfil . ' = ' . $id_usuario .
			' GROUP BY cp.id_conversacion' .
			' ORDER BY c.updated_at DESC';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerConversacionInfo($id_conversacion) {
		$sql = 'SELECT * FROM conversaciones WHERE id = ' . $id_conversacion;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function obtenerConversacion($id_usuario, $perfil, $id_conversacion) {
		$sql = 'SELECT cm.*, a.nombre, a.apellidos, a.foto, r.nombrec, r.foto as foto_tutor, cml.leido' .
			' FROM conversaciones_mensajes as cm' .
			' LEFT JOIN conversaciones_mensajes_leidos as cml ON cm.id = cml.id_mensaje' .
			' LEFT JOIN alumnos as a ON cm.id_usuario = a.idalumnos' .
			' LEFT JOIN rrhh as r ON cm.id_tutor = r.idrrhh' .
			' WHERE cm.id_conversacion = ' . $id_conversacion .
			' AND cml.id_' . $perfil . ' = ' . $id_usuario .
			' ORDER BY id ASC';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function mensajesNoLeidosTotal($id_usuario, $perfil) {
		$sql = 'SELECT COUNT(*) as contador' .
			' FROM conversaciones_mensajes_leidos as cml' .
			' WHERE cml.leido = 0' .
			' AND cml.id_' . $perfil . ' = ' . $id_usuario;
		$resultado = $this->consultaSql($sql);
		if ($resultado->num_rows > 0) {
			$no_leidos = $resultado->fetch_object();
		}
		return $no_leidos;
	}

	public function mensajesNoLeidos($id_usuario, $perfil, $id_conversacion) {
		$sql = 'SELECT COUNT(*) as contador' .
			' FROM conversaciones_mensajes as cm' .
			' LEFT JOIN conversaciones_mensajes_leidos as cml ON cm.id = cml.id_mensaje' .
			' WHERE cm.id_conversacion = ' . $id_conversacion .
			' AND cml.leido = 0' .
			' AND cml.id_' . $perfil . ' = ' . $id_usuario;

		$resultado = $this->consultaSql($sql);
		if ($resultado->num_rows > 0) {
			$no_leidos = $resultado->fetch_object();
		}
		return $no_leidos;
	}

	public function guardarMensaje($id_usuario, $perfil, $id_conversacion, $mensaje) {
		$sql = 'INSERT INTO conversaciones_mensajes (id_conversacion, id_' . $perfil . ', mensaje)' .
			' VALUES ("' . $id_conversacion . '", "' . $id_usuario . '", "' . $mensaje . '")';

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function actualizarFechaConversacion($id_conversacion) {
		$sql = 'UPDATE conversaciones SET updated_at = NOW() WHERE id = ' . $id_conversacion;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerParticipantesConversacion($id_conversacion) {
		$sql = 'SELECT * FROM conversaciones_participantes WHERE id_conversacion = ' . $id_conversacion;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajeNoLeidoAlumno($id_mensaje, $id_usuario) {
		$sql = 'INSERT INTO conversaciones_mensajes_leidos (id_mensaje, id_usuario, leido)' .
			' VALUES ("' . $id_mensaje . '", "' . $id_usuario . '", 0)';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajeLeidoAlumno($id_mensaje, $id_usuario) {
		$sql = 'INSERT INTO conversaciones_mensajes_leidos (id_mensaje, id_usuario, leido)' .
			' VALUES ("' . $id_mensaje . '", "' . $id_usuario . '", 1)';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajeNoLeidoTutor($id_mensaje, $id_usuario) {
		$sql = 'INSERT INTO conversaciones_mensajes_leidos (id_mensaje, id_tutor, leido)' .
			' VALUES ("' . $id_mensaje . '", "' . $id_usuario . '", 0)';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajeLeidoTutor($id_mensaje, $id_usuario) {
		$sql = 'INSERT INTO conversaciones_mensajes_leidos (id_mensaje, id_tutor, leido)' .
			' VALUES ("' . $id_mensaje . '", "' . $id_usuario . '", 1)';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function marcarComoLeidos($id_usuario, $perfil, $id_conversacion) {
		$sql = 'SELECT id FROM conversaciones_mensajes WHERE id_conversacion = ' . $id_conversacion;
		$resultado = $this->consultaSql($sql);
		if ($resultado->num_rows > 0) {
			while ($mensaje = $resultado->fetch_object()) {
				$sql2 = 'UPDATE conversaciones_mensajes_leidos' .
					' SET leido = 1' .
					' WHERE id_mensaje = ' . $mensaje->id .
					' AND leido = 0' .
					' AND id_' . $perfil . ' = ' . $id_usuario;
				$this->consultaSql($sql2);
			}
		}
	}

	public function buscarConversacion($id_usuario, $perfil, $alumno) {
		$id_conversacion = null;
		
		// Buscamos las conversaciones donde coincidimos con el otro alumno
		$sql = 'SELECT t1.id_conversacion
				FROM (SELECT * FROM `conversaciones_participantes` WHERE id_' . $perfil . ' = ' . $id_usuario . ') t1
				INNER JOIN (SELECT * FROM `conversaciones_participantes` WHERE id_' . $alumno[0] . ' = ' . $alumno[1] . ') t2
				ON t1.id_conversacion = t2.id_conversacion';
		$conversaciones_comunes = $this->consultaSql($sql);

		if ($conversaciones_comunes->num_rows > 0) {
			while ($conversacion = $conversaciones_comunes->fetch_object()) {
				
				// Con los IDS de las conversaciones miramos donde participamos solo 2 personas 
				// No puede haber una conversacion duplicada con los mismos particpantes
				$sql = 'SELECT * FROM conversaciones_participantes'
					. ' WHERE id_conversacion = ' . $conversacion->id_conversacion
					. ' HAVING COUNT(*) = 2';
				$resultado = $this->consultaSql($sql);

				if ($resultado->num_rows > 0) {
					while ($row = $resultado->fetch_object()) {
						$id_conversacion = $conversacion->id_conversacion;
					}
				}
			}
		}
		return $id_conversacion;
	}

	public function iniciarConversacion($asunto) {
		$sql = 'INSERT INTO conversaciones (asunto, updated_at)' .
			' VALUES ("' . $asunto . '", NOW())';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function anadirAConversacion($id_conversacion, $id_usuario, $perfil) {
		$sql = 'INSERT INTO conversaciones_participantes (id_conversacion, id_' . $perfil . ')' .
			' VALUES ("' . $id_conversacion . '", "' . $id_usuario . '")';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function mensajeToHtml($mensaje_propio, $foto_usuario, $nombrec, $texto, $fecha, $leido) {
		$html = '';
		if ($mensaje_propio) {
			$html .= '<div class="leido-' . $leido . ' mensaje mensaje_propio">';
		} else {
			$html .= '<div class="leido-' . $leido . ' mensaje mensaje_ajeno">';
		}
		$html .= '	<p class="avatar_chat">';
		$html .= '		<img src="imagenes/fotos/' . $foto_usuario . '"/>';
		$html .= '	</p>';
		$html .= '<p class="name">' . ucwords(strtolower(htmlentities($nombrec, ENT_QUOTES, 'utf-8'))) . '</p>';
		$html .= '<p class="time">(' . date('d/m/Y H:i', strtotime($fecha)) . ')</p>';
		$html .= '<p class="message">' . $texto . '</p>';
		$html .= '</div>';

		return $html;
	}

}
