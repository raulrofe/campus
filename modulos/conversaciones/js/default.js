var intervaloRecargaConversaciones;


$(document).ready(function () {
	// Vaciamos el id del chat para que no se abra automaticamente el último que vimos
	// QUITADO PARA PODER ABRIR CONVERSACION DESDE USUARIOS CONECTADOS
//	 sessionStorage.removeItem('openchat');

	// Cargamos las conversaciones nada mas abrir el Chat
	cargarConversaciones();
	cargarConversacion();
	
	// Actualizar cada X segundos listado de conversaciones y chat abierto si no estqaba el intervalo arrancado de antes
	if (intervaloRecargaConversaciones == undefined) {
		intervaloRecargaConversaciones = setInterval(function () {
			cargarConversaciones();
			cargarConversacion();
		}, tiempoRecarga * 1000);
	}
});


$(document).on('click', '#popupModal_conversaciones .popupModalHeaderBtn_close, #popupModal_conversaciones .windowPopupCloseBtn', function () {
	clearInterval(intervaloRecargaConversaciones);
});