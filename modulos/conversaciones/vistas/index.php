<?php if (!defined('I_EXEC')) exit('Acceso no permitido'); ?>

<div id="popup_conversaciones">
	<div id="contactos" class="chat-left hide">
	</div>
	<div id="conversaciones" class="chat-left">
	</div>
	<div id="conversacion">
		<div id="content">
			
		</div>
		<div class="div-chat-input">
			<form id="chat-input" method="POST" action="">
				<input type="text"/>
				<input type="submit" title="Enviar mensaje" value=">" data-translate-title="conversaciones.enviar">
			</form>
		</div>
	</div>
</div>