<?php

// Modelos
mvc::cargarModuloSoporte('integrantes_curso');
$objModeloIntegrantes = new ModeloIntegrantesCurso();
$objModelo = new ModeloConversaciones();

// Variables
$id_curso = Usuario::getIdCurso();

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	$esAlumno = true;
} else {
	$id_usuario = Usuario::getIdUser(true);
	$esAlumno = false;
}

// Integrantes
$profesores = $objModeloIntegrantes->obtenerProfesores($id_curso);
$alumnos = $objModeloIntegrantes->obtenerAlumnosActivos($id_curso);


// Formamos el html con los contactos con los que se puede inciar una conversacion
$html = '';

if ($profesores->num_rows > 0 || $alumnos->num_rows > 0) {
	$html .= '<ul id="listado_conversaciones">';

	if ($profesores->num_rows > 0) {
		while ($profesor = $profesores->fetch_object()) {
			// Prevenimos abrir conversacion con nosotros mismos
			if ($esAlumno || (!$esAlumno && $id_usuario != $profesor->idrrhh)) {
				$html .= ''
					. '<li>'
					. '	<a href="#" onclick="iniciarConversacion(\'t_' . $profesor->idrrhh . '\'); return false;" title="Enviar un mensaje emergente">'
					. '		<img src="imagenes/mini_circle_green.png" alt="" />'
					. '		<span>' . ucwords(mb_strtolower($profesor->nombrec)) . ' </span>'
					. '	</a>'
					. '<small>(' . ucfirst($profesor->perfil_mostrar) . ')</small>'
					. '</li>';
			}
		}
	}


	if ($alumnos->num_rows > 0) {
		while ($alumno = $alumnos->fetch_object()) {
			// Prevenimos abrir conversacion con nosotros mismos
			if (!$esAlumno || ($esAlumno && $id_usuario != $alumno->idalumnos)) {
				$html .= ''
					. '<li>'
					. '	<a href="#" onclick="iniciarConversacion(' . $alumno->idalumnos . '); return false;" title="Enviar un mensaje emergente">'
					. '		<img src="imagenes/mini_circle_green.png" alt="" />'
					. '		<span>' . ucwords(mb_strtolower(htmlentities($alumno->nombrec, ENT_QUOTES, 'utf-8'))) . ' </span>'
					. '	</a>'
					. '</li>';
			}
		}
	}

	$html .= '</ul>';
} else {
	$html .= '<p class="empty" data-translate-html="conversaciones.nocontactos">No hay contactos disponibles</p>';
}

// Generamos el HTML del boton de "Volver"
$html .= '<div class="div-chat-back">';
$html .= '	<input id="chat-back" type="button" value="     " data-translate-value="conversaciones.volver">';
$html .= '</div>';


echo json_encode(array('html' => $html));
