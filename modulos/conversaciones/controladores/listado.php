<?php

// Variables
$objModelo = new ModeloConversaciones();

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	$perfil = 'usuario';
} else {
	$id_usuario = Usuario::getIdUser(true);
	$id_usuario = str_replace("t_", "", $id_usuario);
	$perfil = 'tutor';
}



// Obtenemos las conversaciones
$conversaciones = $objModelo->obtenerConversaciones($id_usuario, $perfil);



// Generamos el HTML de las conversaciones
$html = '<ul id="listado_conversaciones">';
if ($conversaciones->num_rows > 0) {

	while ($conversacion = $conversaciones->fetch_object()) {

		$html .= ''
			. '<li>'
			. '	<a href="#" onclick="abrirConversacion(' . $conversacion->id . ');return false;" title="Abrir conversación">';

		// Miramos los mensajes sin leer de esta conversacion
		$mensajes_no_leidos = $objModelo->mensajesNoLeidos($id_usuario, $perfil, $conversacion->id);
		if (!empty($mensajes_no_leidos->contador)) {
			$html .= '<span class="contador">' . $mensajes_no_leidos->contador . '</span>';
		} else {
			$html .= '<img class="logo_chat" src="imagenes/logo_chat.png"/>';
		}

		$html .= ''
			. '		<span>' . ucfirst($conversacion->asunto) . '</span>'
			. '		<img class="arrow" src="imagenes/arrow-right.png"/>'
			. '		<br>'
			. '		<span class="hace_tiempo">' . fecha::hace_tiempo($conversacion->updated_at) . '</span>'
			. '	</a>'
			. '</li>';
	}
} else {
	$html .= '<p class="empty" data-translate-html="conversaciones.noabiertas">No hay conversaciones abiertas</p>';
}
$html .= '</ul>';




// Generamos el HTML del boton de "Nueva conversacion"
$html .= '<div class="div-chat-new">';
$html .= '	<input id="chat-new" type="button" value="Nuevo Chat" data-translate-value="conversaciones.nuevo">';
$html .= '</div>';



// Devolvemos la respuesta
echo json_encode(array('html' => $html));
