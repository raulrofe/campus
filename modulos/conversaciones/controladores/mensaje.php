<?php

// Variables
$post = Peticion::obtenerPost();
$objModelo = new ModeloConversaciones();

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	$perfil = 'usuario';
} else {
	$id_usuario = Usuario::getIdUser(true);
	$id_usuario = str_replace("t_", "", $id_usuario);
	$perfil = 'tutor';
}


// Guardamos el mensaje en la conversacion
$objModelo->guardarMensaje($id_usuario, $perfil, $post['id'], $post['mensaje']);



// Obetenemos el ID del mensaje
$id_mensaje = $objModelo->obtenerUltimoIdInsertado();



// Actualizamos la fecha de mofificacion de la conversacion
$objModelo->actualizarFechaConversacion($post['id']);



// Obetenemos los participantes para añadirles los campos visto/no visto
$participantes = $objModelo->obtenerParticipantesConversacion($post['id']);



// Por cada participante guardamos un registro de no leido
if ($participantes->num_rows > 0) {
	while ($participante = $participantes->fetch_object()) {
		if (!empty($participante->id_usuario)) {
			// Si somos los autores del mensaje
			if ($id_usuario == $participante->id_usuario) {
				$objModelo->mensajeLeidoAlumno($id_mensaje, $participante->id_usuario);
			} else {
				$objModelo->mensajeNoLeidoAlumno($id_mensaje, $participante->id_usuario);
			}
		} elseif (!empty($participante->id_tutor)) {
			// Si somos los autores del mensaje
			if ($id_usuario == $participante->id_tutor) {
				$objModelo->mensajeLeidoTutor($id_mensaje, $participante->id_tutor);
			} else {
				$objModelo->mensajeNoLeidoTutor($id_mensaje, $participante->id_tutor);
			}
		}
	}
}



// Generamos el HTML del mensaje para pintarlo en pantalla
$mensaje_propio = true;
$foto_usuario = Usuario::getFoto();
$nombrec = Usuario::getNombreCompleto();
$mensaje = $post['mensaje'];
$fecha = date('c');
$leido = true;

$mensajeHtml = $objModelo->mensajeToHtml($mensaje_propio, $foto_usuario, $nombrec, $mensaje, $fecha,$leido);



// Devolvemos la respuesta
echo json_encode(array('html' => $mensajeHtml));
