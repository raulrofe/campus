<?php

// Variables
$post = Peticion::obtenerPost();
$objModelo = new ModeloConversaciones();

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	$perfil = 'usuario';
} else {
	$id_usuario = Usuario::getIdUser(true);
	$id_usuario = str_replace("t_", "", $id_usuario);
	$perfil = 'tutor';
}

$html = '';

// Obetenemos la informacion de la conversacion
$conversacionInfo = $objModelo->obtenerConversacionInfo($post['id']);
if ($conversacionInfo->num_rows > 0) {
	if ($conversacion = $conversacionInfo->fetch_object()) {
		// Generamos el HTML de la información de la conversacion
		$html = '<h1>' .
			'<img class="logo_chat" src="imagenes/logo_chat.png"/>' .
			$conversacion->asunto .
			'</h1>';
	}
}
$html .= '<div id="listado_conversacion">'
	. '<div id="l_c">';



// Obetenemos los mensajes de la conversacion
$conversacion = $objModelo->obtenerConversacion($id_usuario, $perfil, $post['id']);



// Generamos el HTML de la conversacion con los mensajes
if ($conversacion->num_rows > 0) {
	while ($mensaje = $conversacion->fetch_object()) {
		
		if ($perfil == 'usuario') {
			$mensaje_propio = ($id_usuario == $mensaje->id_usuario) ? true : false;
		} elseif ($perfil == 'tutor') {
			$mensaje_propio = ($id_usuario == $mensaje->id_tutor) ? true : false;
		}
		
		$foto_usuario = (!empty($mensaje->foto)) ? $mensaje->foto : $mensaje->foto_tutor;
		$nombrec = (!empty($mensaje->nombrec)) ? $mensaje->nombrec : $mensaje->nombre . ' ' . $mensaje->apellidos;
		$texto = $mensaje->mensaje;
		$fecha = $mensaje->created_at;
		$leido = $mensaje->leido;

		$mensajeHtml = $objModelo->mensajeToHtml($mensaje_propio, $foto_usuario, $nombrec, $texto, $fecha, $leido);

		$html .= $mensajeHtml;
	}
}

$html .= '<div class="clearfix"></div>';
$html .= '</div>';
$html .= '</div>';



// Marcamos los mensajes como leidos
$id_conversacion = $post['id'];
$objModelo->marcarComoLeidos($id_usuario, $perfil, $id_conversacion);

// Devolvemos la respuesta
echo json_encode(array('html' => $html));
