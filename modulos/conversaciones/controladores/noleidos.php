<?php

// Variables
$objModelo = new ModeloConversaciones();

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	$perfil = 'usuario';
} else {
	$id_usuario = Usuario::getIdUser(true);
	$id_usuario = str_replace("t_", "", $id_usuario);
	$perfil = 'tutor';
}

// Obtenemos las conversaciones
$noleidos = $objModelo->mensajesNoLeidosTotal($id_usuario, $perfil);

// Devolvemos la respuesta
echo json_encode(array('contador' => $noleidos->contador));
