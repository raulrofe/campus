<?php

// Variables
$post = Peticion::obtenerPost();
$objModelo = new ModeloConversaciones();
$id_conversacion = null;

if (Usuario::compareProfile('alumno')) {
	$id_usuario = Usuario::getIdUser();
	// Asunto => Mi nombre - Nombre de con quien quiero hablar
	$asunto = ucwords(strtolower(htmlentities(Usuario::getNombreSimple($id_usuario), ENT_QUOTES, 'utf-8'))) . ' - ' . ucwords(strtolower(htmlentities(Usuario::getNombreSimple($post['id_otro']), ENT_QUOTES, 'utf-8')));
	$perfil = 'usuario';
	$esAlumno = true;
} else {
	$id_usuario = Usuario::getIdUser(true);
	// Asunto => Mi nombre - Nombre de con quien quiero hablar
	$asunto = ucwords(strtolower(htmlentities(Usuario::getNombreSimple($id_usuario), ENT_QUOTES, 'utf-8'))) . ' - ' . ucwords(strtolower(htmlentities(Usuario::getNombreSimple($post['id_otro']), ENT_QUOTES, 'utf-8')));
	$id_usuario = str_replace("t_", "", $id_usuario);
	$perfil = 'tutor';
	$esAlumno = false;
}


// Prevenimos abrir conversacion con nosotros mismos
if ($esAlumno && $id_usuario != $post['id_otro'] ||
	!$esAlumno && $id_usuario != 't_' . $post['id_otro']) {

	// Cogemos los datos del usuario con el que queremos hablar
	$alumno = Usuario::esAlumno($post['id_otro']);

	if ($alumno[0] == 'alumno') {
		$alumno[0] = 'usuario';
	}

	// Buscamos a ver si ya existe la conversacion con ese alumno
	$id = $objModelo->buscarConversacion($id_usuario, $perfil, $alumno);

	if (!empty($id)) {
		$id_conversacion = $id;
	} else {
		// Iniciamos la conversacion
		$conversacion = $objModelo->iniciarConversacion($asunto);

		// Obetenemos el ID de la conversacion
		$id_conversacion = $objModelo->obtenerUltimoIdInsertado();

		// Añadimos a la conversacion a nosotros mismos
		$conversacion = $objModelo->anadirAConversacion($id_conversacion, $id_usuario, $perfil);

		// Añadimos a la conversacion al otro usuario
		$conversacion = $objModelo->anadirAConversacion($id_conversacion, $alumno[1], $alumno[0]);
	}
}

// Devolvemos la respuesta
echo json_encode(array('id_conversacion' => $id_conversacion));
