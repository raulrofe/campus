<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/dbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /**
     * 
     * ********************** Métodos de la tabla CENTRO ********************* *
     * 
     */

    /**
     * Según la tabla del punto 9 de la página 4 del "Modelo de datos de seguimiento Teleformación 20140328.pdf",
     * si es correcto, "CODIGO_RETORNO" debe ser 0 y "ETIQUETA_ERROR" debe ser "Correcto".
     * 
     * @return array con los datos.
     * 
     */
    public function obtenerDatosCentro() {

        //Se consulta a la tabla CENTRO y solo recogerá la primera tupla,
        //ya que no debemos tener más centros en nuestra tabla
        $stmt = $this->conn->prepare("SELECT * FROM sepe_centro LIMIT 1");

        if ($stmt->execute()) {

            $res = array();

            $stmt->bind_result($id, $origenCentro, $codigoCentro, $nombreCentro, $urlPlataforma, $urlSeguimiento, $telefono, $email);
            $stmt->fetch();

            $res["ORIGEN_CENTRO"] = $origenCentro;
            $res["CODIGO_CENTRO"] = $codigoCentro;
            $res["NOMBRE_CENTRO"] = $nombreCentro;
            $res["URL_PLATAFORMA"] = $urlPlataforma;
            $res["URL_SEGUIMIENTO"] = $urlSeguimiento;
            $res["TELEFONO"] = $telefono;
            $res["EMAIL"] = $email;

            $stmt->close();

            return $res;
        } else {
            return null;
        }
    }
}

?>