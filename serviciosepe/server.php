<?php

//Se importa el archivo con las constantes
require_once 'config.php';
require_once 'include/dbHandler.php';

//Se carga la librería soap
if (!extension_loaded("soap")) {
    dl("php_soap.dll");
}

//Deshabilita que se guarde en caché la info del wsdl
ini_set("soap.wsdl_cache_enabled", "0");

//Se crea un objeto SoapServer poniendo la ruta donde esta el archivo .wsdl
//La ruta se recoge de la constante URL_WSDL
$server = new SoapServer(URL_WSDL, array('encoding' => 'ISO-8859-1'));

/**
 * Devuelve los datos del centro
 *
 * @return array 
 */
function obtenerDatosCentro() {

    //Se crea un objeto de la clase DbHandler, donde hay métodos auxiliares que
    //trabajan con la BD
    $dbHandler = new DbHandler();

    $res=$dbHandler->obtenerDatosCentro();

    //Se devuelve el array con el codigo y etiqueta correspondientes y además,
    //los datos serán los mismos que le llegan, pueden estar correctos o vacíos
    //pero es el codigo_error el encargado de avisar
    return array('RESPUESTA_DATOS_CENTRO' => array(
            'CODIGO_RETORNO' => 0,
            'ETIQUETA_ERROR' => "Correcto", 'DATOS_IDENTIFICATIVOS' => array(
                'ID_CENTRO' => array('ORIGEN_CENTRO' => $res['ORIGEN_CENTRO'], 'CODIGO_CENTRO' => $res['CODIGO_CENTRO']),
                'NOMBRE_CENTRO' => $res['NOMBRE_CENTRO'],
                'URL_PLATAFORMA' => $res['URL_PLATAFORMA'],
                'URL_SEGUIMIENTO' => $res['URL_SEGUIMIENTO'],
                'TELEFONO' => $res['TELEFONO'],
                'EMAIL' => $res['EMAIL']))

}


$server->AddFunction("obtenerDatosCentro");
$server->handle();
