<?php

require_once 'config.php';

try {

    $clientSOAP = new SoapClient(URL_WSDL, array(
    	// 'trace' => 1,
    	 // 'keep_alive' => false,
    	 // 'connection_timeout'=>5,
    	 'soap_version'   => SOAP_1_1));

    $result = $clientSOAP->obtenerDatosCentro();

    var_dump($result);

} catch (SoapFault $e) {
	echo '<pre>';
    	print_r($e);
    echo '</pre>';
}

?>