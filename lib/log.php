<?php
class Log
{
	//private static $_path = dirname(__FILE__) . 'log.txt';
	
	public static function start()
	{
		if(!LOG_MOSTRAR_ERRORES)
		{
			error_reporting(0);
		}
		
		if(LOG_SAVE)
		{
			error_reporting(E_ALL | E_STRICT);
			ini_set('error_log', PATH_ROOT . LOG_FILE);
		}
	}
	
	public static function set($strLog, $dirLog, $isDie = true)
	{
		if(LOG_SAVE)
		{
			$path =  PATH_ROOT . LOG_FILE;
			
			$file = fopen($path, 'a+');
			
			//$string = fgets($file);
			$string = date('H:i:s d-m-Y') . ' ' . $strLog . ' en ' . str_ireplace(array('\n', '\r', '\n\r'), '', $dirLog) . "\n";
			
			fwrite($file, $string);
			fclose($file);
		}
		
		if($isDie)
		{
			if(!LOG_MOSTRAR_ERRORES)
			{
				echo 'Hubo un error. Comprueba el log';
			}
			
			die();
		}
	}
	
}