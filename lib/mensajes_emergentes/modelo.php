<?php
class LibModeloMsgEmergente extends modeloExtend
{
	public function obtenerMensajesParaMostrar($idusuario, $idcurso)
	{
		/*$sql = 'SELECT me.idmensaje_emergente, me.mensaje, me.mensaje, me.fecha, rh.nombrec' .
		' FROM mensaje_emergente AS me' .
		' LEFT JOIN mensaje_emergente_destinatario AS med ON (med.idmensaje_emergente = me.idmensaje_emergente AND med.idalumno = ' . $idusuario . ')' .
		' LEFT JOIN staff AS st ON st.idstaff = me.idstaff  AND st.idcurso = ' . $idcurso .
		' LEFT JOIN rrhh AS rh ON rh.idrrhh = st.idrrhh' .
		' WHERE med.ocultar = 0' .
		' GROUP BY me.idmensaje_emergente ORDER BY me.fecha ASC';*/
		
		/*
		// SQL para solo mostrar los mensajes emergentes en el curso
		$sql = 'SELECT me.idmensaje_emergente, me.mensaje, me.fuente_letra, me.tamano_letra, me.color_letra, me.negrita_letra, me.fecha, rh.idrrhh, al.idalumnos, rh.nombrec AS nombrec_rh, CONCAT(al.nombre, " ", al.apellidos) as nombrec_al, me.url_icono' .
		' FROM mensaje_emergente AS me' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = me.idusuario_envio' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = me.idusuario_envio' .
		' WHERE me.ocultar = 0 AND me.idusuario_destino = "' . $idusuario . '" AND me.idcurso = ' . $idcurso .
		' GROUP BY me.idmensaje_emergente ORDER BY me.fecha ASC';
		*/
		
		$sql = 'SELECT me.idmensaje_emergente, me.mensaje, me.fecha, rh.idrrhh, al.idalumnos, rh.nombrec AS nombrec_rh, CONCAT(al.nombre, " ", al.apellidos) as nombrec_al, me.url_icono, c.titulo' .
		' FROM mensaje_emergente AS me' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = me.idusuario_envio' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = me.idusuario_envio' .
		' LEFT JOIN curso AS c ON c.idcurso = me.idcurso' .
		' WHERE me.ocultar = 0 AND me.idusuario_destino = "' . $idusuario . '"' .
		' GROUP BY me.idmensaje_emergente ORDER BY me.fecha ASC';
		
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function obtenerAvisosParaMostrar($idusuario)
	{
		$sql = 'SELECT ap.idmensaje_emergente_personal, ap.mensaje, ap.fecha_a_recibir, ap.dias_repeticion, rh.idrrhh, al.idalumnos, rh.nombrec AS nombrec_rh, CONCAT(al.nombre, " ", al.apellidos) as nombrec_al, ap.url_icono,' .
		' ap.fecha_lunes, ap.fecha_martes, ap.fecha_miercoles, ap.fecha_jueves, ap.fecha_viernes, ap.fecha_sabado, ap.fecha_domingo' .
		' FROM avisos_personales AS ap' .
		' LEFT JOIN rrhh AS rh ON CONCAT("t_", rh.idrrhh) = ap.idusuario' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = ap.idusuario' .
		' WHERE ap.idusuario = "' . $idusuario . '" AND ((ap.fecha_a_recibir <= "' . date('Y-m-d H:i:s') . '" AND ap.ocultar = 0) OR ap.dias_repeticion != 0)' .
		' GROUP BY ap.idmensaje_emergente_personal ORDER BY ap.fecha_a_recibir ASC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	public function marcarLeido($idMsg)
	{
		$sql = 'UPDATE mensaje_emergente SET ocultar=1 WHERE idmensaje_emergente = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $sql;
	}
	
	public function marcarLeidoAviso($idMsg)
	{
		$sql = 'UPDATE avisos_personales SET ocultar=1 WHERE idmensaje_emergente_personal = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $sql;
	}
	
	public function marcarLeidoAvisoPorDia($idMsg, $dia)
	{
		$sql = 'UPDATE avisos_personales SET fecha_' . $dia . ' = "' . date('Y-m-d H:i:s') . '" WHERE idmensaje_emergente_personal = ' . $idMsg;
		$resultado = $this->consultaSql($sql);
		
		return $sql;
	}
}