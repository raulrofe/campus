<?php
class LibMensajesEmergentes
{
	private $_objModelo;
	
	public function __construct()
	{
		$this->_objModelo = new LibModeloMsgEmergente();
	}
	
	public function mostrarMensajes()
	{
		$html = null;
		$json = array();
		
		////////// MENSAJES EMERGENTES ///////////
		if(Usuario::compareProfile(array('alumno', 'tutor', 'coordinador')))
		{
			$idAlumno = Usuario::getIdUser();
			$mensajes = $this->_objModelo->obtenerMensajesParaMostrar(Usuario::getIdUser(true), Usuario::getIdCurso());
						
			if($mensajes->num_rows > 0)
			{
				while($mensaje = $mensajes->fetch_object())
				{
					// obtiene el nombre de que envio el mensaje
					$nombrec = null;
					if(isset($mensaje->nombrec_rh))
					{
						$idUsuario = $mensaje->idrrhh;
						$nombrec = $mensaje->nombrec_rh;
						$esAlumno = false;
					}
					else if(isset($mensaje->nombrec_al))
					{
						$idUsuario = $mensaje->idalumnos;
						$nombrec = $mensaje->nombrec_al;
						$esAlumno = true;
					}
					
					$json[] =
						array(
							'url_icono'     => $mensaje->url_icono,
							'mensaje'       => $mensaje->mensaje,
							'fecha'         => Fecha::obtenerFechaFormateada($mensaje->fecha),
							'idUsuario'     => $idUsuario,
							'esAlumno'      => $esAlumno,
							'autor'         => Texto::textoPlano($nombrec),
							'curso'			=> $mensaje->titulo
						);
						
					$this->_objModelo->marcarLeido($mensaje->idmensaje_emergente);
				}
				$html = '<script type="text/javascript">msgPopupOpen(' . json_encode($json) . ', ' . count($json) . ');</script>';
			}
		}
		
		////////////// AVISOS PERSONALES /////////
		$mensajes_personales = $this->_objModelo->obtenerAvisosParaMostrar(Usuario::getIdUser(true));
		if($mensajes_personales->num_rows > 0)
		{
			while($mensaje = $mensajes_personales->fetch_array())
			{
				if(Usuario::compareProfile('alumno'))
				{
					$nombre = Texto::textoPlano(ucwords($mensaje['nombrec_al']));
				}
				else
				{
					$nombre = Texto::textoPlano(ucwords($mensaje['nombrec_rh']));				}
				
				$arrayDiasSemana = array('lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo');
				$mostrarMensajes = true;
				
				// dias donde se dice el mensaje
				$diasRepeticion = array();
				if($mensaje['dias_repeticion'] != 0)
				{
					$diasRepeticion = explode(',', $mensaje['dias_repeticion']);
					
					// si hoy no es el dia de la repeticion del mensaje
					if(!in_array(date('N'), $diasRepeticion))
					{
						$mostrarMensajes = false;
					}
					// evita que el mensaje se repita dos veces en un mismo dia
					else if(isset($mensaje['fecha_' . $arrayDiasSemana[date('N') - 1]])
						&& $mensaje['fecha_' . $arrayDiasSemana[date('N') - 1]] <= date('Y-m-d H:i:s')
						&& date('Y-m-d', strtotime($mensaje['fecha_' . $arrayDiasSemana[date('N') - 1]])) == date('Y-m-d'))
					{
						$mostrarMensajes = false;
					}
					
					// marca el mensaje como leido solo para este dia de hoy
					if($mostrarMensajes)
					{
						$this->_objModelo->marcarLeidoAvisoPorDia(
							$mensaje['idmensaje_emergente_personal'],
							$arrayDiasSemana[date('N') - 1]
						);
					}
				}
				// marca el mensaje como leido si solo
				else
				{
					$this->_objModelo->marcarLeidoAviso($mensaje['idmensaje_emergente_personal']);
				}
										
				if($mostrarMensajes)
				{
					$json[] =
						array(
							'url_icono'     => $mensaje['url_icono'],
							'mensaje'       => $mensaje['mensaje'],
							'fecha'         => Fecha::obtenerFechaFormateada($mensaje['fecha_a_recibir']),
							'autor'         => $nombre . ' (Yo)',
							'curso'			=> Texto::textoPlano($_SESSION['nombrecurso'])
						);
				}
					
			}
			
			if(!empty($json))
			{
				$html = '<script type="text/javascript">msgPopupOpen(' . json_encode($json) . ', ' . count($json) . ');</script>';
			}
		}
			
		
		return $html;
	}
}