<?php

class Usuario extends modeloExtend {
	/*
	 * Comprueba si estas logeado
	 */

	public static function is_login() {
		$is_login = false;

		if (isset($_SESSION['idusuario'])) {
			$is_login = true;
		}

		return $is_login;
	}

	/*
	 * Obtenemo el id del perfil
	 */

	public static function obtenerIdPerfilAdmin($perfil) {
		//Buscamos el id del perfil
		switch ($perfil) {
			case 'tutores': //posiblemente en desuso
				$idPerfil = 4;
				break;
			case 'tutor':
				$idPerfil = 4;
				break;
			case 'coordinador':
				$idPerfil = 3;
				break;
			case 'supervisor':
				$idPerfil = 2;
				break;
			case 'administrador':
				$idPerfil = 1;
				break;
			default:
				$idPerfil = null;
		}

		return $idPerfil;
	}

	/*
	 * Obtenemo el id del perfil
	 */

	public static function obtenerIdPerfilLogueo() {
		//Buscamos el id del perfil
		switch ($_SESSION['perfil']) {
			case 'tutores': //posiblemente en desuso
				$idperfil = 4;
				break;
			case 'tutor':
				$idperfil = 4;
				break;
			case 'coordinador':
				$idperfil = 3;
				break;
			case 'supervisor':
				$idperfil = 2;
				break;
			default:
				$idperfil = null;
		}

		return $idperfil;
	}

	/*
	 * Obtenemos el perfil
	 */

	public static function getProfile() {
		$perfil = null;

		if (isset($_SESSION['perfil'])) {
			$perfil = $_SESSION['perfil'];
		}

		return $perfil;
	}

	/*
	 * Indica los perfiles que tienen acceso a determinados sitios
	 */

	public static function compareProfile($perfil) {
		$permiso = false;
		$perfilSession = self::getProfile();

		if (isset($perfilSession)) {
			if (is_array($perfil) && count($perfil) > 0) {
				$endSearch = false;
				foreach ($perfil as $item) {
					if (!$endSearch && $perfilSession == $item) {
						$endSearch = true;
						$permiso = true;
					}
				}
			} else {
				if ($perfilSession == $perfil) {
					$permiso = true;
				}
			}
		}

		return $permiso;
	}

	/*
	 * Obtienes el id de usuario y si es de la tabla rrhh te lo devuelve con una t_ delante del id
	 */

	public static function getIdUser($conT = false) {
		$idUsuario = null;
		$get = Peticion::obtenerGet();

		/* if(isset($_SESSION['idusuario2'], $get['m']) && $get['m'] == 'gestor_correos')
		  {
		  $idUsuario = $_SESSION['idusuario2'];
		  }
		  else if(isset($_SESSION['idusuario']))
		  {
		  $idUsuario = $_SESSION['idusuario'];
		  } */

		$idUsuario = $_SESSION['idusuario'];

		if (isset($idUsuario)) {
			if ($conT) {
				if (self::getProfile() != 'alumno') {
					$idUsuario = 't_' . $idUsuario;
				}
			}
		}

		return $idUsuario;
	}

	/*
	 * Devuelve la convocatoria
	 */

	public static function getIdConvocatoria() {
		$idConv = null;

		if (isset($_SESSION['idconvocatoria'])) {
			$idConv = $_SESSION['idconvocatoria'];
		}

		return $idConv;
	}

	/*
	 * Devuelve el id del curso
	 */

	public static function getIdCurso() {
		$idcurso = null;

		if (isset($_SESSION['idcurso'])) {
			$idcurso = $_SESSION['idcurso'];
		}

		return $idcurso;
	}

	public static function getNameCurso() {
		$nombreCurso = null;

		if (isset($_SESSION['nombrecurso'])) {
			$nombreCurso = $_SESSION['nombrecurso'];
		}

		return $nombreCurso;
	}

	public static function checkAlumno($idUsuario) {
		$isAlumno = false;

		// comprueba si es un perfil alumno o no
		if (!preg_match('#^t_([0-9]+)$#', $idUsuario)) {
			$isAlumno = true;
		}

		return $isAlumno;
	}

	/*
	 * Comprueba segun un id si es alumno o no
	 */

	public static function esAlumno($idUsuario) {

		// comprueba si es un perfil alumno o no
		if (preg_match('#^t_([0-9]+)$#', $idUsuario)) {
			$idUsuario = explode('_', $idUsuario);
			$perfil = 'tutor';
			$array = array($perfil, $idUsuario[1]);
		} else {
			$perfil = 'alumno';
			$array = array($perfil, $idUsuario);
		}

		return $array;
	}

	public function actualizarFechaConexion($idusuario, $idcurso, $perfil) {
		if ($perfil == 'alumno') {
			$sql = 'UPDATE matricula SET fecha_conexion_actual = "' . date('Y-m-d H:i:s') . '" WHERE idalumnos = ' . $idusuario . ' AND idcurso = ' . $idcurso;
		} else {
			$sql = 'UPDATE staff SET fecha_conexion_actual = "' . date('Y-m-d H:i:s') . '"WHERE idrrhh = ' . $idusuario . ' AND idcurso = ' . $idcurso;
		}

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* public function actualizarFechaAccesoCurso($idusuario, $idcurso, $perfil)
	  {
	  if($perfil == 'alumno')
	  {
	  $sql = 'UPDATE matricula SET fecha_acceso_curso = "' . date('Y-m-d H:i:s') . '" WHERE idalumnos = ' . $idusuario . ' AND idcurso = ' . $idcurso;
	  }
	  else
	  {
	  $sql = 'UPDATE staff SET fecha_acceso_curso = "' . date('Y-m-d H:i:s') . '"WHERE idrrhh = ' . $idusuario . ' AND idcurso = ' . $idcurso;
	  }

	  $resultado = $this->consultaSql($sql);

	  return $resultado;
	  } */

	public static function actualizarFechaForo($idtema) {
		if (!isset($_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema])) {
			$_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema] = $_SESSION['usuario_fecha_acceso_curso'];
		} else {
			$_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema] = date('Y-m-d H:i:s');
		}
	}

	public static function obtFechaForo($idtema) {
		$fecha = null;

		if (!isset($_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema])) {
			$_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema] = $_SESSION['usuario_fecha_acceso_curso'];
			$fecha = $_SESSION['usuario_fecha_acceso_curso'];
		} else {
			$fecha = $_SESSION['usuario_fecha_ult_interaccion_foro'][$idtema];
		}

		return $fecha;
	}

	public static function getNameUser($idusuario) {
		$objusuario = new Usuario();

		$nombreUsuario = '';

		$arrayIdUsuario = self::esAlumno($idusuario);

		if (self::checkAlumno($idusuario)) {

			$sql = "SELECT CONCAT(A.nombre, ' ', A.apellidos) AS nombre_usuario from alumnos A where idalumnos = " . $arrayIdUsuario[1];

			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {

				$row = $resultado->fetch_object();
				$nombreUsuario = $row->nombre_usuario;
			}
		} else {

			$sql = "SELECT RH.nombrec from rrhh RH where idrrhh = " . $arrayIdUsuario[1];

			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {

				$row = $resultado->fetch_object();
				$nombreUsuario = $row->nombrec;
			}
		}

		return $nombreUsuario;
	}
	
	public static function getNombreSimple($idusuario) {
		$objusuario = new Usuario();

		$nombreUsuario = '';

		$arrayIdUsuario = self::esAlumno($idusuario);

		if (self::checkAlumno($idusuario)) {

			$sql = "SELECT nombre from alumnos A where idalumnos = " . $arrayIdUsuario[1];

			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {

				$row = $resultado->fetch_object();
				$nombreUsuario = $row->nombre;
			}
		} else {

			$sql = "SELECT RH.nombre from rrhh RH where idrrhh = " . $arrayIdUsuario[1];

			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {

				$row = $resultado->fetch_object();
				$nombreUsuario = $row->nombre;
			}
		}

		return $nombreUsuario;
	}

	public static function getNombreCompleto() {
		$nombrec = '';
		$objusuario = new Usuario();

		if (Usuario::compareProfile('alumno')) {
			$id_usuario = Usuario::getIdUser();
			$sql = "SELECT CONCAT(A.nombre, ' ', A.apellidos) AS nombrec from alumnos A where idalumnos = " . $id_usuario;
			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {
				$row = $resultado->fetch_object();
				$nombrec = $row->nombrec;
			}
		} else {
			$id_tutor = Usuario::getIdUser(true);
			$id_tutor = str_replace("t_", "", $id_tutor);
			$sql = "SELECT nombrec from rrhh RH where idrrhh = " . $id_tutor;
			$resultado = $objusuario->consultaSql($sql);
			if ($resultado->num_rows > 0) {
				$row = $resultado->fetch_object();
				$nombrec = $row->nombrec;
			}
		}

		return $nombrec;
	}

	public static function getFoto() {
		$foto = '';
		$objusuario = new Usuario();

		if (Usuario::compareProfile('alumno')) {
			$id_usuario = Usuario::getIdUser();
			$sql = "SELECT foto from alumnos A where idalumnos = " . $id_usuario;
			$resultado = $objusuario->consultaSql($sql);

			if ($resultado->num_rows > 0) {
				$row = $resultado->fetch_object();
				$foto = $row->foto;
			}
		} else {
			$id_tutor = Usuario::getIdUser(true);
			$id_tutor = str_replace("t_", "", $id_tutor);
			$sql = "SELECT foto from rrhh RH where idrrhh = " . $id_tutor;
			$resultado = $objusuario->consultaSql($sql);
			if ($resultado->num_rows > 0) {
				$row = $resultado->fetch_object();
				$foto = $row->foto;
			}
		}

		return $foto;
	}

	public static function getIdMatricula() {
		$objusuario = new Usuario();

		if (Usuario::compareProfile('alumno')) {
			$sql = "SELECT idmatricula as idmatricula from matricula " .
				" WHERE idalumnos = " . self::getIdUser() .
				" AND idcurso = " . self::getIdCurso();
		} else {
			$sql = "SELECT idstaff as idmatricula from staff " .
				" WHERE idrrhh = " . self::getIdUser() .
				" AND idcurso = " . self::getIdCurso();
		}

		$resultado = $objusuario->consultaSql($sql);
		$idMatricula = $resultado->fetch_object();

		return $idMatricula->idmatricula;
	}

	public static function getIdMatriculaDatos($idalumnos, $idcurso) {
		$objusuario = new Usuario();
		$idMatricula = null;

		$sql = "SELECT idmatricula as idmatricula from matricula " .
			" WHERE idalumnos = " . $idalumnos .
			" AND idcurso = " . $idcurso;

		$resultado = $objusuario->consultaSql($sql);
		if ($resultado->num_rows == 1) {
			$idMatricula = $resultado->fetch_object();
			$idMatricula = $idMatricula->idmatricula;
		}

		return $idMatricula;
	}

	public static function getFechaInicioCurso() {
		$objCurso = new Cursos();
		$fechaInicio = null;

		$idCurso = self::getIdCurso();

		$sql = "SELECT f_inicio from curso " .
			" WHERE idcurso = " . $idCurso;

		$resultado = $objCurso->consultaSql($sql);
		if ($resultado->num_rows == 1) {
			$fechaInicio = $resultado->fetch_object();
			$fechaInicio = $fechaInicio->f_inicio;
		}

		return $fechaInicio;
	}

	public static function getFechaFinCurso() {
		$objCurso = new Cursos();
		$fechaFin = null;

		$idCurso = self::getIdCurso();

		$sql = "SELECT f_fin from curso " .
			" WHERE idcurso = " . $idCurso;

		$resultado = $objCurso->consultaSql($sql);
		if ($resultado->num_rows == 1) {
			$fechaFin = $resultado->fetch_object();
			$fechaFin = $fechaFin->f_fin;
		}

		return $fechaFin;
	}

	public function fehcaInicioSesion($idMatricula) {
		$objCurso = new Cursos();
		$sql = "INSERT into log_tiempo_sesion (idmatricula, inicio_sesion) VALUES ('" . $idMatricula . "','" . date('Y-m-d H:i:s') . "')";
		$resultado = $objCurso->consultaSql($sql);
		return $objCurso->obtenerUltimoIdInsertado();
	}

	public static function actualizarEncuestaRealizada($idUsuario) {
		$objCurso = new Cursos();
		$sql = 'UPDATE alumnos SET encuesta_realizada = 1 WHERE idalumnos = ' . $idUsuario;
		$resultado = $objCurso->consultaSql($sql);
		return $resultado;
	}

	public static function getIdUsuarioDesdeMatricula($idMatricula) {
		$objCurso = new Cursos();
		
		$idAlumno = null;

		$sql = "SELECT idalumnos from matricula " .
		" WHERE idmatricula = " . $idMatricula ;

		$resultado = $objCurso->consultaSql($sql);

		if($resultado->num_rows == 1)
		{
			$resultado = $resultado->fetch_object();
			$idAlumno = $resultado->idalumnos;
		}

		return $idAlumno;		
	}

}