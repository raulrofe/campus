<?php
class Cursos extends modeloExtend{

	private $con;
	private $tipo_curso;
	private $idcurso;
	private $idtutoria;
	private $idrecomendacion;
	private $idusuario;

	//constructor
	public function Cursos(){parent::__construct();}

	public function set_usuario($idusuario){$this->idusuario = $idusuario;}

//**********************************************************************************************
//FUNCIONES PARA EL TIPO DE CURSO
//**********************************************************************************************

	// Establecemos el tipo de curso
	public function set_tipo_curso($id_tipo_curso)
	{
		$this->tipo_curso = $id_tipo_curso;
	}

	//insertamos rama para el tipo de curso
	public function insertar_tipo_curso($tipo_curso)
	{
		$sql="INSERT into rama_curso (rama_curso) VALUES ('$tipo_curso')";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos todos los tipos de curso
	public function ver_tipos_curso()
	{
		$sql="SELECT * from rama_curso WHERE borrado=0";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Select con todo los tipos de curso
	public function select_tipo_curso($resultado)
	{
		echo "<select name='idtipo_curso'><option value=''> - Selecciona un tipo de curso - </option>";
		while($registro = mysqli_fetch_assoc($resultado))
		{
			if($this->tipo_curso == $registro['idrama_curso']) echo "<option value='".$registro['idrama_curso']."' selected>".$registro['rama_curso']."</option>";
			else echo "<option value='".$registro['idrama_curso']."'>".$registro['rama_curso']."</option>";
		}
		echo "</select>";
	}

	//Select con todo los tipos de curso sin onchange
	public function select_tipo_curso_noauto($resultado,$idRama)
	{
		echo "<select name='idtipo_curso'>";
		while($registro = mysqli_fetch_assoc($resultado))
		{
			if($idRama == $registro['idrama_curso']) echo "<option value='".$registro['idrama_curso']."' selected>".$registro['rama_curso']."</option>";
			else echo "<option value='".$registro['idrama_curso']."'>".$registro['rama_curso']."</option>";
		}
		echo "</select>";
	}

	//devolvemos un tipo de curso pasamo como parametro su id
	public function buscar_tipo_curso()
	{
		$sql="SELECT * from rama_curso where borrado=0 AND idrama_curso = ".$this->tipo_curso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//eliminar rama del curso
	public function eliminar_tipo_curso()
	{
		$sql ="UPDATE rama_curso SET borrado=1 where idrama_curso = ".$this->tipo_curso;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//actualizar tipo de curso
	public function actualizar_tipo_curso($atipo_curso)
	{
		$sql="UPDATE rama_curso SET rama_curso = '$atipo_curso' where idrama_curso = ".$this->tipo_curso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

//**********************************************************************************************
// FUNCIONES PARA LOS CURSOS
//**********************************************************************************************

	//Establecemos el valor para el curso
	public function set_curso($idcurso){$this->idcurso = $idcurso;}

	//Chekeamos si el curso existe
	public function checkCursoExists($nombreAccionFormativa, $idConvocatoria, $idAccionFormativa)
	{
		$sql = "SELECT idcurso FROM curso" .
		" WHERE titulo = '" . $nombreAccionFormativa . "'" .
		" AND idconvocatoria = " . $idConvocatoria .
		" AND idaccion_formativa = " . $idAccionFormativa;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Buscamos los cursos que pertenecen a una convocatoria que no estan dados de alta
	public function cursosParaAlta()
	{
		$sql="SELECT * FROM curso WHERE borrado = 0";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Buscamos los cursos que pertenecen a una convocatoria que estan dados de alta
	public function cursosConvocatoriasAlta($idConvocatoria)
	{
		$sql="SELECT * from curso
		where alta = 1
		and borrado = 0
		and idconvocatoria = ".$idConvocatoria;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos los cursos que pertenecen a una convocatoria
	public function cursosConvocatoriaActivos($idconv)
	{
		$sql = "SELECT * from curso where idconvocatoria = ".$idconv." and borrado = 0 ";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos los cursos que pertenecen a una convocatoria
	public function cursosConvocatoria($idconv)
	{
		$sql = "SELECT * from curso where idconvocatoria = ".$idconv;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos todos los cursos y devolvemos un array
	public function all_cursos($activos = 'true')
	{
		$sql="SELECT * from curso ";

		if($activos == 'true')
		{
			$sql.="where borrado = 0";
		}

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//select de todos los cursos
	public function select_cursos($resultado)
	{
		echo "<select name='idcurso'>
				<option value='0'>Selecciona un curso</option>";
		while($registro = mysqli_fetch_assoc($resultado)){
			if($this->idcurso == $registro['idcurso']) echo "<option value='".$registro['idcurso']."' selected>".$registro['titulo']."</option>";
			else echo "<option value='".$registro['idcurso']."'>".$registro['titulo']."</option>";
		}
		echo "</select>";
	}

	//select de todo los cursos excluyendo los dados de alta
	public function select_cursos_noalta($idConvocatoria)
	{
		$sql="SELECT * from curso
		where alta = 0
		and borrado = 0
		and idconvocatoria = ".$idConvocatoria;
		$resultado = $this->consultaSql($sql);

		if(mysqli_num_rows($resultado) > 0)
		{
			echo "<select name='idcurso'>";
			while($registro = mysqli_fetch_assoc($resultado))
			{
				echo "<option value='".$registro['idcurso']."'>".$registro['titulo']."</option>";
			}
			echo "</select>";
		}
		else echo "No existen cursos para dar de alta o ya est&aacute;n todos dados de alta";
	}

	//select de todo los cursos dados de alta
	public function select_cursos_alta($idcurso)
	{
		$sql="SELECT * from curso where alta = 1 and borrado = 0";
		$resultado = $this->consultaSql($sql);
		echo "<select name='idcurso' onchange='this.form.submit()'>
				<option>Selecciona un curso</option>";
		while($registro = mysqli_fetch_assoc($resultado))
		{
			if ($idcurso == $registro['idcurso']) echo "<option value='".$registro['idcurso']."' selected>".$registro['titulo']."</option>";
			else echo "<option value='".$registro['idcurso']."'>".$registro['titulo']."</option>";
		}
		echo "</select>";
	}

	//paso 1 : insertamos un curso
	public function insertar_curso($titulo,$descripcion,$idrama,$idaf,$idconv)
	{
		$sql = "INSERT into curso (titulo,descripcion,idrama_curso,idaccion_formativa,idconvocatoria)
		VALUES ('$titulo','$descripcion','$idrama','$idaf','$idconv')";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/*
	 * paso 2 copiamos la configuracion de calificaciones y la insertamos en el curso
	 */
	public function insertar_cc_curso($autoevaluaciones,$trabajosPracticos,$scorm,$trabajoEquipo,$tutoria,$foro,$extra1,$extra2,$notaCoordinador,$ultimoid)
	{
		$sql = "UPDATE curso SET
		autoevaluaciones = '$autoevaluaciones',
		trabajos_practicos = '$trabajosPracticos',
		scorm = '$scorm',
		trabajo_equipo = '$trabajoEquipo',
		tutoria = '$tutoria',
		foro = '$foro',
		extra1 = '$extra1',
		extra2 = '$extra2',
		nota_coordinador = '$notaCoordinador'
		WHERE idcurso = ".$ultimoid;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	/*
	 * paso 3 : copiamos la configuracion de notas y la insertamos en el curso
	 */
	public function insertar_cn_notas($numAccesos,$numTrabajos,$numForo,$scorm,$ultimoid)
	{
		$sql = "UPDATE curso SET
		n_accesos = '$numAccesos',
		num_msg_foro = '$numForo',
		num_trabajos = '$numTrabajos',
		calificacion_scorm = '$scorm'
		WHERE idcurso = ".$ultimoid;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//devolvemos un curso pasamos como parametro su id
	public function buscar_curso()
	{
		$sql="SELECT * from curso where idcurso = ".$this->idcurso;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	public function buscar_curso_para_un_usuario($idusuario, $active = null)
	{
		// para mostrar todos los cursos, los activos o los no activos
		$addSql = null;
		if(isset($active))
		{
			$date = date('Y-m-d');
			if($active)
			{
				$addSql = ' AND (c.f_inicio <= "' . $date . '" AND c.f_fin >= "' . $date . '")';
			}
			else
			{
				$addSql = ' AND c.f_fin <= "' . $date . '"';
			}
		}

		$sql="SELECT *, m.fecha_conexion_actual AS fecha_actual_al, st.fecha_conexion_actual AS fecha_actual_tutor from curso As c" .
		" LEFT JOIN staff AS st ON c.idcurso = st.idcurso AND CONCAT('t_', st.idrrhh) = '" . $idusuario . "'" .
		" LEFT JOIN matricula AS m ON c.idcurso = m.idcurso AND m.idalumnos = '" . $idusuario . "'" .
		" where st.idcurso = " . $this->idcurso . " OR m.idcurso = " . $this->idcurso . $addSql;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Buscamos los curso que pertenecen a un usuario
	public function cursos_usuario($idusuario = null, $active = null, $convocatoriaActual = null, $fechaInicio = null, $fechaFin = null)
	{
		if(!isset($idusuario))
		{
			$idusuario = $_SESSION['idusuario'];
		}

		// para mostrar todos los cursos, los activos o los no activos
		$addSql = null;
		if(isset($active))
		{
			$date = date('Y-m-d');
			if($active)
			{
				// $addSql = 'AND (C.f_inicio <= "' . $date . '" AND C.f_fin >= "' . $date . '")';
				$addSql = 'AND (C.f_fin >= "' . $date . '")';
			}
			else
			{
				$addSql = 'AND C.f_fin <= "' . $date . '"';
			}
		}
		else
		{
			if(isset($convocatoriaActual))
			{
				$addSql = 'AND (C.f_inicio = "' . $fechaInicio . '" AND C.f_fin = "' . $fechaFin . '") AND (C.idcurso > 98 OR C.idcurso < 69) AND (C.idcurso <> 206 AND C.idcurso <> 207)';
			}
		}

		if(isset($_SESSION['perfil']) && $_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from staff S, curso C
			where C.borrado = 0
			AND S.idrrhh = " . $idusuario ." and S.idcurso = C.idcurso $addSql
			GROUP BY C.idcurso
			order by C.f_inicio, C.titulo ASC";
			//log::set($sql, __FILE__);
			//echo $sql;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
		else
		{
			$sql = "SELECT * from matricula M, curso C
			where M.idalumnos = ".$idusuario."
			AND M.borrado = 0
			AND C.borrado = 0
			AND M.idcurso = C.idcurso $addSql
			GROUP BY C.idcurso
			order by C.f_inicio, C.titulo ASC";
			//echo "consulta de cursos".$sql;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}

	//actualizar curso
	public function actualizar_curso($atitulo, $adescripcion, $idtipo_curso, $idaf, $metodologia, $if, $ff, $im, $fm, $horas)
	{
		$if = Fecha::invertir_fecha($if, '/', '-');
		$ff = Fecha::invertir_fecha($ff, '/', '-');
		$im = Fecha::invertir_fecha($im, '/', '-');
		$fm = Fecha::invertir_fecha($fm, '/', '-');

		$sql = "UPDATE curso SET
		titulo='$atitulo',
		descripcion='$adescripcion',
		idaccion_formativa='$idaf',
		idrama_curso = '$idtipo_curso',
		f_inicio = '$if',
		f_fin = '$ff',
		inicio_matriculacion = '$im',
		fin_matriculacion = '$fm',
		metodologia = '$metodologia',
		n_horas = '$horas'
		where idcurso = ".$this->idcurso;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//eliminar curso
	public function eliminar_curso()
	{
		$sql = "UPDATE curso  SET borrado = 1 where idcurso = ".$this->idcurso;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// DEBE IR SIEMPRE DESPUES DE INSERTAR_CURSO debido al mysqli_insert_id()
	public function insertar_tema_curso()
	{
		$id_temario = mysqli_insert_id();
		$id_rrhh = $_SESSION['idusuario'];

		$titulos = array('Presentación del curso','Dudas generales');

		foreach($titulos as $titulo)
		{
			$sql="INSERT into foro_temas (idtemario, titulo, fecha, idrrhh) VALUES ('$id_temario', '$titulo', '" . date('Y-m-d H:i:s') . "', '$id_rrhh')";
			$this->consultaSql($sql);
		}
	}

	public function changeAlta($idCurso, $estadoCurso)
	{
		$sql = "UPDATE curso SET alta = '" . $estadoCurso . "' where idcurso = " . $idCurso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
//********************************************************
// FUNCIONES PARA EL ALTA DE LOS CURSOS
//********************************************************

	public function updateAltaCurso($idCurso)
	{
		$sql = "UPDATE curso SET alta = '1' where idcurso = " . $idCurso;

		return $resultado = $this->consultaSql($sql);
	}

	public function asignarCursoTutor($idCurso, $idTutor, $status)
	{
		$sql = "INSERT into staff" .
		" (idcurso, idrrhh, status)" .
		" VALUES ('" . $idCurso . "', '" . $idTutor . "', '" . $status . "')";
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//alta curso
	public function alta_curso($idcurso,$idrrhh,$mostrar,$registros)
	{
		$f = mysqli_fetch_assoc($registros);
		if($f['alta'] == 0)
		{
			if(($mostrar != '1') or (empty ($mostrar)))
			{
					$mostrar = '0';
			}

			if($idrrhh[2] != $idrrhh[3])
			{
				$status = array("coordinador","supervisor","tutor1","tutor2");
				for($i=0;$i<=count($idrrhh)-1;$i++)
				{
					//compruebo si ya existe el registro en la base de datos
					$sql = "SELECT idstaff, status FROM staff WHERE idcurso = " . $idcurso . " AND idrrhh = " . $idrrhh[$i];
					$checkTutor = $this->consultaSql($sql);
					if($checkTutor->num_rows == 0)
					{
						$sql = "INSERT into staff (idcurso, idrrhh, status) VALUES ('$idcurso', '$idrrhh[$i]', '$status[$i]')";
						$this->consultaSql($sql);
					}
					else
					{
						$checkTutor = $checkTutor->fetch_object();
						$sql = "UPDATE staff SET status = '" . $checkTutor->status . "' WHERE idstaff = " . $checkTutor->idstaff;
						$this->consultaSql($sql);
					}
				}

				$sql = "UPDATE curso SET alta = '1' where idcurso = ".$idcurso;

				return $resultado = $this->consultaSql($sql);

			}
			else
			{
				Alerta::guardarMensajeInfo('tutoresnoiguales','El tutor 1 y tutor 2 no pueden ser iguales');
			}
		}
		else echo "<script>alert('Este curso ya est&aacute; dado de alta')</script>";
	}

	//Buscu un usuario que tenga un status determinado en la tabla staff al dar de alta los cursos
	public function busca_status($status,$idcurso)
	{
		$sql = "SELECT * from staff where idcurso = ".$idcurso." and status = '".$status."'";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f['idrrhh'];
	}

	//Select usuario en los cursos dados de alta le pasamos como parametro el id del perfil y el id del usuario para seleccionarlo
	public function select_usuarios2($idperfil,$idusuario)
	{
		$sql = "SELECT * from rrhh
		where idperfil = $idperfil";

		$resultado = $this->consultaSql($sql);
		echo "<select name='idrrhh[]'>";
		while($f = mysqli_fetch_assoc($resultado))
		{
			if ($idusuario == $f['idrrhh']) echo "<option value='".$f['idrrhh']."' selected>".$f['nombrec']."</oprion>";
			else echo "<option value='".$f['idrrhh']."'>".$f['nombrec']."</oprion>";
		}
		echo "</select>";
	}

	public function eliminarStaffCurso($idCurso)
	{
		$sql = "DELETE FROM staff WHERE idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function eliminarPerfilStaffCurso($idCurso, $perfil = 'tutor')
	{
		$sql = "DELETE FROM staff WHERE idcurso = " . $idCurso . " AND status like '" . $perfil . "%'";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Actualizar alta del curso, le pasamos como parametro el id de un curso
	public function actualizar_alta_curso($idCurso, $idrrhh)
	{
		if($idrrhh[2] != $idrrhh[3])
		{
			$status = array("coordinador","supervisor","tutor1","tutor2");
			for($i=0;$i<=count($idrrhh)-1;$i++)
			{
				$sql = "UPDATE staff SET  idrrhh = '$idrrhh[$i]' , status = '$status[$i]'
				where idcurso = ".$this->idcurso." and status = '".$status[$i]."'";
				//echo $sql."<br/>";
				$resultado = $this->consultaSql($sql);
				if($resultado){Alerta::mostrarMensajeInfo('altaactualizada','El Alta de curso ha sido actualizada correctamente');}
			}
		}
		//else return false;
		else Alerta::mostrarMensajeInfo('tutoresnomismos','El tutor principal y el tutor secundario no pueden ser los mismos');
	}

	// Buscamos el id del staff
	public function buscar_idstaff()
	{
		$sql = "SELECT * from staff where idrrhh = ".$this->idusuario." and idcurso = ".$this->idcurso;
		$resultado = mysqli_query($this->con,$sql);
		$f = mysqli_fetch_assoc($resultado);
		return $f['idstaff'];
	}

	public function obtenerStaffCurso($idCurso)
	{
		$sql = "SELECT * from staff where idcurso = ". $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

//********************************************************
// FUNCIONES PARA AGENDA DE RECOMENDACIONES
//********************************************************

	// Establecer agenda
	public function set_agenda($idrecomendacion){$this->idrecomendacion = $idrecomendacion;}

	//Buscamos todas las agendas
	public function ver_agendas()
	{
		$sql = "SELECT * from recomendacion";
		$resultado = mysqli_query($sql);
		return $resultado;
	}

	// Insertamos una agenda de recomendacion
	public function insertar_agenda($titulo_agenda, $descripcion_agenda)
	{
		$sql = "INSERT INTO recomendacion (titulo_recomendacion, descripcion)
		VALUES ('" . $titulo_agenda . "', '" . $descripcion_agenda . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	// Select con todas las agendas
	public function select_agendas($registros)
	{
		echo "<select name='idrecomendacion'>";
			while($f = mysqli_fetch_assoc($registros))
			{
				echo "<option value='".$f['idrecomendacion']."'>".$f['titulo_recomendacion']."</option>";
			}
		echo "</select>";
	}

	// Buscamos una agenda segun su id
	public function buscar_agenda()
	{
		$sql = "SELECT * from recomendacion where idrecomendacion = ".$this->idrecomendacion;
		$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}

	// Actualizar agenda
	public function actualizar_agenda($titulo_agenda,$descripcion_agenda)
	{
		$sql = "UPDATE recomendacion SET
		titulo_recomendacion  = '$titulo_agenda',
		descripcion = '$descripcion_agenda'
		where idrecomendacion = ".$this->idrecomendacion;
		//echo $sql;
		mysqli_query($this->con,$sql) or die ("error al actualizar la agenda");
	}

//********************************************************
// FUNCIONES PARA LOS EVENTOS DE LA AGENDA
//********************************************************

	// Insertamos un evento para la agenda
	public function insertar_evento($nombre_evento,$fecha_evento,$descripcion_evento,$idrecomendacion)
	{
		$sql = "INSERT INTO events (event_name,details,idrecomendacion) VALUES ('$nombre_evento','$descripcion_evento','2')";
		echo $sql;
		mysqli_query($this->con,$sql) or die ("error al insertar evento en la agenda");
	}

//********************************************************
// FUNCIONES PARA ACCION FORMATIVA
//********************************************************

	public function obtenerAccionFormativa($idAF)
	{
		$sql = 'SELECT idaccion_formativa FROM accion_formativa WHERE accion_formativa = ' . $idAF;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	//obtenemos la accion formativa a partir del idcurso
	public function obtenerIGAFCurso($idCurso)
	{
		$sql = "SELECT *".
		" FROM accion_formativa as AF" .
		" LEFT JOIN curso as C ON C.idaccion_formativa = AF.idaccion_formativa" .
		" LEFT JOIN inicio_grupo as IG ON AF.idaccion_formativa = IG.idaccion_formativa" .
		" WHERE C.idcurso = " . $idCurso;
		
		// ACTIVAR ESTA SQL PARA SACAR INFORMES ANTERIORES AL CAMBIO DE LEY
		// $sql = "SELECT *
		// from accion_formativa AF, curso C, inicio_grupo IG
		// where C.idcurso = ".$idCurso."
		// and C.idaccion_formativa = AF.idaccion_formativa
		// and AF.idaccion_formativa = IG.idaccion_formativa
		// and C.idconvocatoria = IG.idconvocatoria";
		
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}


	//obtenemos el inicio de grupo a partir del idcurso
	public function obtenerIGCurso($idCurso)
	{
		$sql = "SELECT AF.accion_formativa from accion_formativa AF, curso C where C.idcurso = ".$idCurso." and C.idaccion_formativa = AF.idaccion_formativa";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

//********************************************************
// FUNCIONES PARA CONFIG CALIFICACIONES
//********************************************************

	public function obtenerConfigCalificaciones($idCC)
	{
		$sql = 'SELECT * FROM conf_calificaciones WHERE idconf_calificaciones = ' . $idCC;
		$resultado = $this->consultaSql($sql);
		//echo $sql;
		return $resultado;
	}


	public function obtenerTodasConfigCalificaciones()
	{
		$sql = 'SELECT * FROM conf_calificaciones';
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

//********************************************************
// FUNCIONES PARA RAMA CURSO
//********************************************************

	public function obtenerRamaCurso()
	{
		$sql = 'SELECT * FROM rama_curso';
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

/***************************************************************************
 * FUNCIONES UTILES PARA LOS INFORMES
 **************************************************************************/
	public function obtenerTutoresCurso($idCurso)
	{
		$sql = "SELECT rh.nombrec, rh.nif, s.status" .
		" FROM rrhh AS rh".
		" LEFT JOIN staff AS s ON s.idrrhh = rh.idrrhh" .
		" WHERE s.idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function obtenerCoordinadoraCurso($idCurso)
	{

	}

	/*
	public function obtenerTutoriaCursoTripartita($idCodigoGrupo)
	{
		$sql = "SELECT t.dias_tutoria, t.n_horas, t.hora_inicio, t.hora_fin, eo.cif, eo.nombre_entidad" .
		" FROM tutoriasxml AS t" .
		" LEFT JOIN inicio_grupo AS ig ON ig.tutorias = t.idtutorias" .
		" LEFT JOIN entidad_organizadora AS eo ON eo.identidad_organizadora = ig.identidad_organizadora" .
		" WHERE ig.idinicio_grupo = " . $idCodigoGrupo;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	*/

	public function obtenerTutoriaCursoTripartita($idCurso)
	{
		$sql = "SELECT t.dias_tutoria, t.n_horas, t.hora_inicio, t.hora_fin" .
		" FROM tutoriasxml AS t" .
		" LEFT JOIN curso AS c ON c.tutoria = t.idtutorias" .
		" WHERE c.idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/*****************************************************************************
	 * TODOS LOS ARCHIVOS DE UN CURSO
	 ****************************************************************************/

	public function archivosTemarioCurso($idCurso)
	{
		$sql = "SELECT at.idarchivo_temario, at.titulo, at.descripcion, at.prioridad, at.enlace_archivo, at.imagen_archivo, at.f_creacion, m.nombre" .
		" FROM archivo_temario AS at" .
		" LEFT JOIN modulo AS m ON m.idmodulo = at.idmodulo" .
		" WHERE at.idcurso = " . $idCurso .
		" AND at.activo = 1" .
		" AND at.borrado = 0" .
		" AND at.idcategoria_archivo = 1" .
		" ORDER BY at.prioridad ASC";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}


	/************************************************************************************************************************************************************
	 * INSERTAR CURSO POR STEP CON JQUERY
	 *************************************************************************************************************************************************************/

	/* PASO 1: Inserto los datos de curso */

	public function insertDatosCurso($titulo, $horas, $fechaInicio, $fechaFin, $inicioMatriculacion, $finMatriculacion, $modalidad, $guiaDidactica, $descripcion, $idAF)
	{
		$sql = "INSERT into curso (titulo, n_horas, f_inicio, f_fin, inicio_matriculacion, fin_matriculacion, metodologia, guia_didactica, descripcion, idaccion_formativa)
		VALUES ('$titulo', '$horas', '$fechaInicio', '$fechaFin',  '$inicioMatriculacion' , '$finMatriculacion', '$modalidad', '$guiaDidactica', '$descripcion', '$idAF')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* PASO 1.1: Asigno los servicios al curso*/
	public function asignarServiciosCurso($idServicio, $idCurso)
	{
		$sql = "INSERT INTO servicios_link (idServicio, idCurso) VALUES ('" . $idServicio . "', '" . $idCurso . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* PASO 1.2: Asigno los servicios al curso*/
	public function asignarRecomendacionCurso($idRecomendacion, $idCurso)
	{
		$sql = "UPDATE curso SET idrecomendacion = '" . $idRecomendacion . "' WHERE idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* PASO 2: Asigno los modulos al curso */

	public function asignarModuloCurso($idCurso, $idModulo, $idAccionFormativa)
	{
		$sql = "INSERT into temario (idcurso, idmodulo, idaccion_formativa) VALUES ('" . $idCurso . "', '" . $idModulo . "', '" . $idAccionFormativa . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/*****************************************************************************************************************************************
	* EMPRESA ORGANIZADORA
	*****************************************************************************************************************************************/

	public function getEntidadOrganizadora()
	{
		$sql = "SELECT nombre_entidad, cif FROM entidad_organizadora WHERE identidad_organizadora = 1";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/* SPEAKING */
	public function getNotaSpeaking($idMatricula) {
		$sql = "SELECT id, nota FROM calificacion_speaking WHERE idmatricula = " . $idMatricula;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function updateNotaSpeaking($idMatricula, $nota) {
		$sql = "UPDATE calificacion_speaking SET nota = '" . $nota . "' WHERE idmatricula = " . $idMatricula;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertNotaSpeaking($idMatricula, $nota) {
		$sql = "INSERT into calificacion_speaking (idmatricula, nota) VALUES ('" . $idMatricula . "', '" . $nota . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function getSpeakingsCurso($idCurso) {
		$sql = "SELECT cs.idmatricula, cs.nota FROM calificacion_speaking as cs " .
			   " LEFT JOIN matricula AS mt ON mt.idmatricula = cs.idmatricula " .
			   " WHERE mt.idcurso = " . $idCurso;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function deleteSpeakingCurso($idMatricula) {
		$sql = "DELETE FROM calificacion_speaking WHERE idmatricula = " . $idMatricula;

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	/**************************************************************************************************************************
	* INSERTO ACUERDO DE COLABORACION CON ASE
	**************************************************************************************************************************/

	public function insertAcuerdoColaboracion($idAlumno, $idAse, $fechaInicio, $fechaFin, $anio){
		$sql = "INSERT into acuerdo_colaboracion_ase (id_alumno, estado_clave, idcurso_matriculado, fecha_matriculado, fecha_fin, anio) VALUES ('" . $idAlumno . "', 1, '" . $idAse . "', '" . $fechaInicio . "', '" . $fechaFin . "', '" . $anio . "')";

		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

	public function getAcuerdoColaboracion($idAlumno){
		$sql = "SELECT id FROM acuerdo_colaboracion_ase WHERE id_alumno = " . $idAlumno;

		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

	public function updateAcurdoColaboracion($id, $fechaFin, $idAse){
		$sql = "UPDATE acuerdo_colaboracion_ase SET pendiente = 0, fecha_fin = '" . $fechaFin . "', idcurso_matriculado = " . $idAse . " WHERE id = " . $id;

		$resultado = $this->consultaSql($sql);

		return $resultado;			
	}

	public function deleteAcurdoColaboracion($idAlumno){
		$sql = "UPDATE acuerdo_colaboracion_ase SET pendiente = 0, estado_clave = 0, idcurso_matriculado = '', fecha_fin = '' WHERE id_alumno = " . $idAlumno;

		$resultado = $this->consultaSql($sql);

		return $resultado;		
	}

}
?>