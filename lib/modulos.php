<?php
class Modulos extends modeloExtend{

	private $idmodulo;
	private $max;
	private $idarchivo;
	private $idcategoria_archivo;
	private $mimesListWhite = array(
			'application/msword', 
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/excel', 
			'application/vnd.ms-excel', 
			'application/x-excel', 
			'application/x-msexcel',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
			'application/pdf', 
			'application/download', 
			'application/x-download',
			'application/powerpoint', 
			'application/vnd.ms-powerpoint',
			'application/x-shockwave-flash',
			'image/bmp', 
			'image/x-windows-bmp',
			'image/tiff',
			'image/tiff',
			'text/plain',
			'text/html',
			'text/css',
			'application/x-zip', 
			'application/zip', 
			'application/x-zip-compressed'	
		);
	private $extensionsListWhite = array(
			'doc', 
			'docx', 
			'xls', 
			'xlsx', 
			'pdf',
			'ppt',
			'pptx',
			'swf',
			'bmp',
			'tiff',
			'tif',
			'txt',
			'text',
			'html',
			'htm',
			'css',
			'zip',
			'rar'
	);		
	
//*************************************
// FUNCIONES PARA LOS MODULOS
//*************************************
	
	//Establecemos el modulo
	public function set_modulo($idmodulo){$this->idmodulo = $idmodulo;}

	
	//select de todo los modulos 
	public function select_modulos_noauto($resultado){
		echo "<select name='idmodulo'>";
		echo "<option value='0'> - Selecciona un m&oacute;dulo de la lista - </option>";
		while($registro = mysqli_fetch_assoc($resultado)){
			if($this->idmodulo == $registro['idmodulo']) echo "<option value='".$registro['idmodulo']."' selected>".$registro['referencia_modulo']." - ".$registro['nombre']."</option>";
			else echo "<option value='".$registro['idmodulo']."'>".$registro['referencia_modulo']." - ".$registro['nombre']."</option>";
		}
		echo "</select>";
	}
	
	//select de todo los modulos automatico
	public function select_modulos($resultado)
	{
		echo "<select name='idmodulo' onchange='this.form.submit()'>";
		echo "<option value=''>- Seleccione un M&oacute;dulo -</option>";
		while($registro = mysqli_fetch_assoc($resultado)){
			if($this->idmodulo == $registro['idmodulo']) echo "<option value='".$registro['idmodulo']."' selected>".$registro['referencia_modulo']." - ".$registro['nombre']."</option>";
			else echo "<option value='".$registro['idmodulo']."'>".$registro['referencia_modulo']." - ".$registro['nombre']."</option>";
		}
		echo "</select>";
	}
	
	//select multiple con todo los modulos
	public function selectmultiple_modulos($resultado){
		echo "<select multiple name='idmodulo[]' class='selectMultipleAdmin' size='20'>";
		while($registro = mysqli_fetch_assoc($resultado)){
			echo "<option value='".$registro['idmodulo']."'>(".$registro['referencia_modulo'].") ".$registro['nombre']."</option>";
		}
		echo "</select>";
	}
	
	//Select categoria de archivos
	public function select_categoria_archivos($idcategoria_archivo){
		$sql="SELECT * from categoria_archivo";
		$resultado = $this->consultaSql($sql);
		echo "<select name='idcategoria_archivo'>";
		while($registro = mysqli_fetch_assoc($resultado)){
			if($idcategoria_archivo == $registro['idcategoria_archivo']) echo "<option value='".$registro['idcategoria_archivo']."' selected>".$registro['categoria']."</option>";
			else echo "<option value='".$registro['idcategoria_archivo']."'>".$registro['categoria']."</option>";
		}
		echo "</select>";
	}
			
	//insertamos un modulo
	public function insertar_modulo($titulo,$nmodulo,$descripcion,$contenidos,$objetivos){
		$sql = "INSERT into modulo(nombre,referencia_modulo,descripcion,contenidos,objetivos) VALUES ('$titulo','$nmodulo','$descripcion','$contenidos','$objetivos')";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//devolvemos un modulo pasamos como parametro su id
	public function buscar_modulo($idmodulo){
		$sql="SELECT * from modulo where idmodulo = ".$idmodulo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//devolvemos un modulo pasamos como parametro su id
	public function buscar_modulo_referencia($referencia){
		$sql="SELECT * from modulo where referencia_modulo = ".$referencia;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// buscamos todos los modulos existentes
	public function ver_modulos($activos = 'true'){
		$sql = "SELECT * from modulo";
		if($activos == 'true')
		{
			$sql.=" where borrado = 0";
		}
		$sql.=" order by referencia_modulo ASC";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//actualizar modulo
	public function actualizar_modulo($atitulo,$anmodulo,$adescripcion,$acontenidos,$aobjetivos){
		$sql = "UPDATE modulo SET
		nombre='$atitulo',
		referencia_modulo='$anmodulo',
		descripcion='$adescripcion',
		contenidos='$acontenidos',
		objetivos='$aobjetivos'
		where idmodulo = ".$this->idmodulo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//eliminamos el modulo
	public function eliminar_modulo($idmodulo)
	{
		$sql = "UPDATE modulo SET borrado = 1 where idmodulo = ".$idmodulo;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

//******************************
// FUNCIONES PARA LOS ARCHIVOS
//******************************	

	//Establecemos el valor de un archivo
	public function set_archivo($idarch){
		$this->idarchivo = $idarch;
	}
	
	//Establecemos la categoria de archivos
	public function set_categoria_archivo($idcategoria_archivo){
		$this->idcategoria_archivo = $idcategoria_archivo;
	}
	
	/*
	//Obtenemos el ultimo registro insertado en la base de datos y lo devolvemos
	public function ultimo_registro(){
		$sql = "SELECT MAX(idarchivo_temario) from archivo_temario";
		$resultado = $this->consultaSql($sql);
		$f = mysqli_fetch_row($resultado);
		$this->max = $f['0'];
	}
	*/
	
	//Insertamos ficheros
	public function insertar_fichero($titulo,$descripcion,$prioridad,$idcurso,$idcategoria_archivo)
	{
		$fecha = date("Y-m-d");
		$sql="INSERT into archivo_temario (titulo, descripcion, f_creacion, prioridad, idmodulo, idcurso, idcategoria_archivo)
		VALUES ('$titulo','$descripcion','$fecha','$prioridad','$this->idmodulo','$idcurso','$idcategoria_archivo')";
		
		$this->consultaSql($sql);

		return $this->obtenerUltimoIdInsertado();
	}

	//Actualizamos el enlace del archivo
	public function updateLinkFileTemario($idFichero, $ruta)
	{
		$sql = "UPDATE archivo_temario SET enlace_archivo =  '" . $ruta . "' " . 
		"WHERE idarchivo_temario = " . $idFichero;

		$resultado = $this->consultaSql($sql);

		return $resultado;	
	}
	
	// Guardamos el archivo una vez que hemos creado el registro con todos sus datos le pasamos como par_metro el ultimo registro para un curso
	public function guardar_ficheroCurso($titulo,$descripcion,$idcurso,$prioridad,$idcategoria_archivo)
	{
		$resultado = false;
		
		//Pongo el nombre a la carpeta segun sea trabajo practico o tema
		switch($idcategoria_archivo)
		{
			case 1 :
			$folder = 'temas';
			break;
			
			case 2 :
			$folder = 'trabajos_practicos';
			break;
		}
		
		if(isset($_FILES['archivo']) && !empty($_FILES['archivo']['tmp_name']))
		{
			if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idcurso))
			{
				mkdir(PATH_ROOT . "archivos/cursos/" . $idcurso);
			}
			if(!file_exists(PATH_ROOT . "archivos/cursos/" . $idcurso . "/" . $folder))
			{
					mkdir(PATH_ROOT . "archivos/cursos/" . $idcurso . "/" . $folder);	
			}
			
			$dir = "archivos/cursos/" . $idcurso . "/" . $folder . "/";
			if(file_exists(PATH_ROOT . $dir))
			{
				//obtenemos la extension del archivo recien subido
				$extension = Texto::obtenerExtension($_FILES['archivo']['name']); 
					
				// si el tipo de archivo es valido
				if(in_array($_FILES['archivo']['type'], $this->mimesListWhite) && in_array($extension, $this->extensionsListWhite))
				{
					if($this->insertar_fichero($titulo,$descripcion,$prioridad,$idcurso,$idcategoria_archivo))
					{
						//$ultimoRegistro = $this->ultimo_registro();
						$ultimoRegistro = $this->obtenerUltimoIdInsertado();
						
						//obtenemos la extension del archivo recien subido
						$partes = explode(".", $_FILES['archivo']['name']); 
						$extension = end($partes); 
	
						chmod(PATH_ROOT . $dir, 0777);
						
						if(move_uploaded_file($_FILES['archivo']['tmp_name'],PATH_ROOT . $dir . $ultimoRegistro . "." . $extension))
						{
							$archivo = $dir . $ultimoRegistro . "." . $extension;
							//Actualizamos el registro e insesrtamos el enlace en la base de datos
							$sql="UPDATE archivo_temario SET enlace_archivo = '$archivo' where idarchivo_temario = ".$ultimoRegistro;
							$resultado = $this->consultaSql($sql);	
						}
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivos','El archivo debe ser .doc .docx .xls .xlsx o .pdf');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('directorionocreado','El directorio no ha sido creado');
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('nofichero','No existe fichero');
		}
		
		return $resultado;
	}
	
	// Guardamos el archivo una vez que hemos creado el registro con todos sus datos le pasamos como par_metro el ultimo registro para la plantilla
	public function guardar_fichero($titulo,$descripcion,$prioridad,$idcategoria_archivo)
	{
		$resultado = false;
		
		//Pongo el nombre a la carpeta segun sea trabajo practico o tema
		switch($idcategoria_archivo)
		{
			case 1 :
			$folder = 'temas';
			break;
			
			case 2 :
			$folder = 'trabajos_practicos';
			break;
		}

		if(isset($_FILES['archivo']) && !empty($_FILES['archivo']['tmp_name']))
		{
			if(!file_exists(PATH_ROOT . "archivos/temarios/" . $this->idmodulo))
			{
				mkdir(PATH_ROOT . "archivos/temarios/" . $this->idmodulo);
			}
			if(!file_exists(PATH_ROOT . "archivos/temarios/" . $this->idmodulo . "/" . $folder))
			{
				mkdir(PATH_ROOT . "archivos/temarios/" . $this->idmodulo . "/" . $folder);	
			}
			
			$dir = "archivos/temarios/" . $this->idmodulo . "/" . $folder . "/";
			if(file_exists(PATH_ROOT . $dir))
			{
				//obtenemos la extension del archivo recien subido
				$extension = Texto::obtenerExtension($_FILES['archivo']['name']); 
					
				// si el tipo de archivo es valido
				if(in_array($_FILES['archivo']['type'], $this->mimesListWhite) && in_array($extension, $this->extensionsListWhite))
				{
					if($this->insertar_ficheroPlantilla($titulo,$descripcion,$prioridad,$idcategoria_archivo))
					{
						//$ultimoRegistro = $this->ultimo_registro();
						$ultimoRegistro = $this->obtenerUltimoIdInsertado();
						
						//obtenemos la extension del archivo recien subido
						$partes = explode(".", $_FILES['archivo']['name']); 
						$extension = end($partes); 
	
						chmod(PATH_ROOT . $dir, 0777);
						
						if(move_uploaded_file($_FILES['archivo']['tmp_name'],PATH_ROOT . $dir . $ultimoRegistro . "." . $extension))
						{
							$archivo = $dir . $ultimoRegistro . "." . $extension;
							//Actualizamos el registro e insesrtamos el enlace en la base de datos
							$sql="UPDATE plantilla_archivos_temario SET enlace_archivo = '$archivo' where idarchivo_temario = ".$ultimoRegistro;
							$resultado = $this->consultaSql($sql);	
						}
					}
				}
				else
				{
					Alerta::guardarMensajeInfo('tipoarchivos','El archivo debe ser .doc .docx .xls .xlsx o .pdf');
				}
			}
			else
			{
				Alerta::guardarMensajeInfo('directorionocreado','El directorio no ha sido creado');
			}
		}
		else
		{
			Alerta::guardarMensajeInfo('nofichero','No existe fichero');
		}
		
		return $resultado;
	}
	
	// Buscamos un archivo segun el id pasado como parametro y devuelve un array
	public function buscar_archivo($idcurso = null)
	{
		$addQuery = null;
		if(isset($idcurso))
		{
			$addQuery = ' AND idcurso = ' . $idcurso;
		}
		
		$sql = "SELECT * from archivo_temario where idarchivo_temario = " . $this->idarchivo . $addQuery;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Eliminamo un archivo segun el id pasado como parametro
	public function eliminar_archivo(){
		
		$sql="SELECT * from archivo_temario where idarchivo_temario = ".$this->idarchivo;
		$resultadoArchivo = $this->consultaSql($sql);
		$f = mysqli_fetch_assoc($resultadoArchivo);
				
		if(!empty ($f['enlace_archivo'])) 
		{
			if(file_exists(PATH_ROOT.$f['enlace_archivo']))
			{
				chmod(PATH_ROOT . $f['enlace_archivo'], 0777);
				unlink(PATH_ROOT.$f['enlace_archivo']);	
			}	
		}
		$sql="DELETE from archivo_temario where idarchivo_temario = ".$this->idarchivo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Activar/desactivar un archivo segun el id pasado como parametro
	public function vistaTrabajo($idArchivo)
	{
		
		$sql="SELECT * from archivo_temario where idarchivo_temario = ".$idArchivo;
		echo $sql;
		$resultadoArchivo = $this->consultaSql($sql);
		if(mysqli_num_rows($resultadoArchivo) > 0)
		{
			$archivo = mysqli_fetch_assoc($resultadoArchivo);
			if($archivo['activo'] == 1) $activo = 0;
			else $activo = 1;
			
			$sql = "UPDATE archivo_temario SET activo = '$activo' where idarchivo_temario = ".$idArchivo;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}

	}
	
	// Eliminamos un trabajo practico segun el id pasado como parametro
	public function borradoLogicoTrabajo($idArchivo)
	{
		$sql="SELECT * from archivo_temario where idarchivo_temario = ".$idArchivo;
		$resultadoArchivo = $this->consultaSql($sql);
		if(mysqli_num_rows($resultadoArchivo) > 0)
		{
			$archivo = mysqli_fetch_assoc($resultadoArchivo);
			$sql = "UPDATE archivo_temario SET borrado = 1 where idarchivo_temario = ".$idArchivo;
			$resultado = $this->consultaSql($sql);
			return $resultado;
		}
	}
	
	// Devuelve un array con archivos del modulo 
	public function archivos_modulo(){
		$sql="SELECT * from archivo_temario T, categoria_archivo CA 
		where T.idmodulo = ".$this->idmodulo."
		and T.idcurso = ".$_SESSION['idcurso']."
		and T.idcategoria_archivo = CA.idcategoria_archivo ";
		if(!empty ($this->idcategoria_archivo))
		{
			$sql.= "and CA.idcategoria_archivo = ".$this->idcategoria_archivo;
		}
		$sql.=" order by T.idcategoria_archivo , T.prioridad";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	// Devuelve un array con archivos del modulo quitando los borrados logicamente 
	public function archivos_moduloNoBorrados(){
		$sql="SELECT * from archivo_temario T, categoria_archivo CA 
		where T.idmodulo = ".$this->idmodulo."
		and T.idcategoria_archivo = CA.idcategoria_archivo 
		and borrado = 0 ";
		if(!empty ($this->idcategoria_archivo))
		{
			$sql.= "and CA.idcategoria_archivo = ".$this->idcategoria_archivo;
		}
		$sql.=" order by T.idcategoria_archivo , T.prioridad";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Devuelve un array con archivos del modulo pasamos el id del modulo como parametro
	public function archivosModulosActivos(){
		$sql = "SELECT * FROM archivo_temario T, categoria_archivo CA" . 
		" WHERE T.idcurso = " . $_SESSION['idcurso'] .
		" AND T.idcategoria_archivo = CA.idcategoria_archivo" .
		" AND activo = 1 ";

		if(!empty ($this->idcategoria_archivo))
		{
			$sql .= "and CA.idcategoria_archivo = ".$this->idcategoria_archivo;
		}

		$sql.=" order by T.idcategoria_archivo , T.prioridad";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	
	//Actualizamos nuestro archivo
	public function actualizar_archivo($titulo,$descripcion,$prioridad,$idmodulo)
	{
		$resultado = false;
		$resultadoImg = false;
		
		if(!empty($_FILES['archivo']['tmp_name']))
		{
			//obtenemos la extension del archivo recien subido
			$extension = Texto::obtenerExtension($_FILES['archivo']['name']); 
			
			// si el tipo de archivo es valido
			if(in_array($_FILES['archivo']['type'], $this->mimesListWhite) && in_array($extension, $this->extensionsListWhite))
			{
				//Buscamos el archivo
				$sql = "SELECT * from archivo_temario where idarchivo_temario = ".$this->idarchivo;
				$resultadoArchivo = $this->consultaSql($sql);
				$miArchivo = mysqli_fetch_assoc($resultadoArchivo);
						
				// Lo borramos si existe
				if(!empty ($miArchivo['enlace_archivo'])) 
				{
					unlink(PATH_ROOT.$miArchivo['enlace_archivo']);
				}
				
				// Establecemos la carpeta de actualizacion
				switch($miArchivo['idcategoria_archivo'])
				{
					case 1 :
						$folder = 'temas';
						break;
					
					case 2 :
						$folder = ' trabajos_practicos';
						break;
				}
				
				//estableces la ruta y le damos permiso
				$dir = "archivos/temarios/" . $idmodulo . "/" . $folder . "/";
				chmod(PATH_ROOT . $dir, 0777);
				
				//movemos el archvio
				if(move_uploaded_file($_FILES['archivo']['tmp_name'],PATH_ROOT . $dir . $this->idarchivo . "." . $extension))
				{
					$archivo = $dir . $this->idarchivo . "." . $extension;
					$sql = "UPDATE archivo_temario SET titulo = '$titulo',
					descripcion = '$descripcion',
					idmodulo = '$idmodulo',
					prioridad = '$prioridad'";
					//echo $sql;die();
					$resultadoImg = true;	
				}

			}
			else
			{
				Alerta::mostrarMensajeInfo('debedocumento','El archivo debe ser .doc .docx .xls .xlsx o .pdf');
			}
		}
		else
		{
			$resultadoImg = true;
		}
		
		if($resultadoImg)
		{
			$sql = "UPDATE archivo_temario 
			SET titulo = '$titulo',
			idmodulo = '$idmodulo',
			descripcion = '$descripcion',";
			if(isset($archivo))
			{
				$sql.="enlace_archivo = '$archivo', ";
			}	
			$sql.="prioridad = '$prioridad'";
			$sql.=" where idarchivo_temario = ".$this->idarchivo;
			//echo $sql;die();
			$resultado = $this->consultaSql($sql);
		}
		
		return $resultado;
	}
	
	// Buscamos todos los modulos que pertenecen a un curso
	public function modulos_asignado_curso($idcurso = false)
	{
		if(!$idcurso)
		{
			$idcurso = Usuario::getIdCurso();
		}

		$sql="SELECT DISTINCT *" . 
		" from temario T , modulo M, curso C, accion_formativa AF" .
		" where C.idcurso = ".$idcurso.
		" and C.idaccion_formativa = AF.idaccion_formativa" .
		" and AF.idaccion_formativa = T.idaccion_formativa" .
		" and T.idmodulo = M.idmodulo" .
		" order by M.referencia_modulo ASC";

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos las autoevaluaciones que pertenecen a un modulo
	public function autoevaluacionesModulo($idmodulo)
	{
		$sql = "SELECT * from test T, autoeval A 
		where T.idmodulo = ".$idmodulo." 
		and T.idautoeval = A.idautoeval 
		GROUP BY T.idautoeval";
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	/****************************************************************************************************************************
	* METODOS TRABAJOS PRACTICOS PARA EL CURSO
	****************************************************************************************************************************/

	public function getTrabajosPracticosCurso($idCurso)
	{
		$sql = "SELECT * FROM archivo_temario where idcurso = " . $idcurso . " AND idcategoria_archivo = 2";
		$resultado = $this->consultaSql($sql);
		return $reusltado;
	}
	



/****************************** GESTOR DE FICHEROS Y TRABAJOS PRACTICOS PARA PLANTILLAS *****************************************/

	//Insertamos ficheros como plantilla
	public function insertar_ficheroPlantilla($titulo,$descripcion,$prioridad,$idcategoria_archivo)
	{
		$fecha = Fecha::fechaActual(false);
		$sql="INSERT into plantilla_archivos_temario (titulo,descripcion,f_creacion,prioridad,idmodulo,idcategoria_archivo)
		VALUES ('$titulo','$descripcion','$fecha','$prioridad','$this->idmodulo','$idcategoria_archivo')";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	//Actualizamos nuestro archivo
	public function actualizar_archivoPlantilla($titulo,$descripcion,$prioridad,$idmodulo)
	{
		$resultado = false;
		$resultadoImg = false;
		
		if(!empty($_FILES['archivo']['tmp_name']))
		{
			//obtenemos la extension del archivo recien subido
			$extension = Texto::obtenerExtension($_FILES['archivo']['name']); 
			
			// si el tipo de archivo es valido
			if(in_array($_FILES['archivo']['type'], $this->mimesListWhite) && in_array($extension, $this->extensionsListWhite))
			{
				//Buscamos el archivo con el id establecido por defecto
				$resultadoArchivo = $this->buscar_archivoPlantilla();
				$miArchivo = mysqli_fetch_assoc($resultadoArchivo);
						
				// Lo borramos si existe
				if(!empty ($miArchivo['enlace_archivo']) && file_exists(PATH_ROOT.$miArchivo['enlace_archivo']) )
				{
					chmod(PATH_ROOT . $miArchivo['enlace_archivo'], 0777);
					unlink(PATH_ROOT.$miArchivo['enlace_archivo']);
				}
				// Establecemos la carpeta de actualizacion
				switch($miArchivo['idcategoria_archivo'])
				{
					case 1 :
						$folder = 'temas';
						break;
					
					case 2 :
						$folder = 'trabajos_practicos';
						break;
				}
				
				//estableces la ruta y le damos permiso
				$dir = "archivos/temarios/" . $idmodulo . "/" . $folder . "/";
				chmod(PATH_ROOT . "archivos/temarios/" . $idmodulo . "/" . $folder, 0777);
				
				//movemos el archvio
				if(move_uploaded_file($_FILES['archivo']['tmp_name'],PATH_ROOT . $dir . $this->idarchivo . "." . $extension))
				{
					$archivo = $dir . $this->idarchivo . "." . $extension;
					$sql = "UPDATE plantilla_archivos_temario SET titulo = '$titulo',
					descripcion = '$descripcion',
					idmodulo = '$idmodulo',
					prioridad = '$prioridad'";
					//echo $sql;die();
					$resultadoImg = true;	
				}

			}
			else
			{
				Alerta::mostrarMensajeInfo('debedocumento','El archivo debe ser .doc .docx .xls .xlsx o .pdf');
			}
		}
		else
		{
			$resultadoImg = true;
		}
		
		if($resultadoImg)
		{
			$sql = "UPDATE plantilla_archivos_temario 
			SET titulo = '$titulo',
			idmodulo = '$idmodulo',
			descripcion = '$descripcion',";
			if(isset($archivo))
			{
				$sql.="enlace_archivo = '$archivo', ";
			}	
			$sql.="prioridad = '$prioridad'";
			$sql.=" where idarchivo_temario = ".$this->idarchivo;
			$resultado = $this->consultaSql($sql);
		}
		
		return $resultado;
	}
	
	
	// Devuelve un array con archivos del modulo 
	public function archivos_moduloPlantilla(){
		$sql="SELECT * from plantilla_archivos_temario T, categoria_archivo CA 
		where T.idmodulo = ".$this->idmodulo."
		and T.idcategoria_archivo = CA.idcategoria_archivo ";
		if(!empty ($this->idcategoria_archivo))
		{
			$sql.= "and CA.idcategoria_archivo = ".$this->idcategoria_archivo;
		}
		$sql.=" order by T.idcategoria_archivo , T.prioridad";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	// Buscamos un archivo segun el id pasado como parametro y devuelve un array
	public function buscar_archivoPlantilla()
	{
		$sql = "SELECT * from plantilla_archivos_temario where idarchivo_temario = ".$this->idarchivo;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Eliminamo un archivo segun el id pasado como parametro
	public function eliminar_archivoPlantilla()
	{
		$resultadoArchivo = $this->buscar_archivoPlantilla();
		$f = mysqli_fetch_assoc($resultadoArchivo);
				
		if(!empty ($f['enlace_archivo'])) 
		{
			if(file_exists(PATH_ROOT.$f['enlace_archivo']))
			{
				chmod(PATH_ROOT.$f['enlace_archivo'], 0777);
				unlink(PATH_ROOT.$f['enlace_archivo']);	
			}	
		}
		
		$sql="DELETE from plantilla_archivos_temario where idarchivo_temario = " . $this->idarchivo;

		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	// Devuelve un array con archivos del modulo quitando los borrados logicamente 
	public function archivos_moduloNoBorradosPlantilla(){
		$sql="SELECT * from plantilla_archivos_temario T, categoria_archivo CA 
		where T.idmodulo = ".$this->idmodulo."
		and T.idcategoria_archivo = CA.idcategoria_archivo ";
		if(!empty ($this->idcategoria_archivo))
		{
			$sql.= "and CA.idcategoria_archivo = ".$this->idcategoria_archivo;
		}
		$sql.=" order by T.idcategoria_archivo , T.prioridad";
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	
	/******************************************************************************************************************************************************************
	 * ARCHIVOS DEL TEMARIO EN LA TABLA PLANTILLA
	******************************************************************************************************************************************************************/

	public function getTemerioFilePlantillaById($idTemario)
	{
		$sql =  "SELECT * " .
				"FROM plantilla_archivos_temario as pat " .
				"WHERE pat.idarchivo_temario = " . $idTemario;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}	
	
	public function deleteTemarioFilePlantilla($id)
	{
		$sql="DELETE from plantilla_archivos_temario where idarchivo_temario = " . $id;	

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
}