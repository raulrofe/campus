<?php
class Aula{

	private $con;
	private $idcurso;
	private $idtutoria;
	private $idusuario;
	private $idstaff;
	private $idanuncio;
	
	//constructor
	public function Aula($con){$this->con = $con;}
	
	public function set_tutoria($idtutoria){$this->idtutoria = $idtutoria;}	
	public function set_curso($idcurso){$this->idcurso = $idcurso;}	
	public function set_staff($idstaff){$this->idstaff = $idstaff;}	
		


//******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************
// FUNCIONES PARA EL TABLON DE ANUNCIOS
//******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************

	public function set_anuncio($idanuncio){$this->idanuncio = $idanuncio;}
	
	public function insertar_anuncio($titulo,$descripcion){
		$fecha = date("Y-m-d");
		$sql = "INSERT into anuncio (titulo_anuncio,descripcion_anuncio,fecha,idstaff)
		VALUES ('$titulo','$descripcion','$fecha','$this->idstaff')";
		//echo $sql;
		mysqli_query($this->con,$sql);
	}

	public function modificar_anuncio($titulo,$descripcion){
		$fecha = date("Y-m-d");
		$sql = "UPDATE anuncio SET titulo_anuncio = '$titulo' , descripcion_anuncio = '$descripcion'
		where idanuncio = ".$this->idanuncio;
		//echo $sql;
		mysqli_query($this->con,$sql);
	}
	
	public function ver_anuncios(){
		$sql = "SELECT * from rrhh RH, staff S, anuncio A, curso C
		where S.idstaff = ".$this->idstaff." 
		and S.idrrhh = RH.idrrhh 
		and S.idcurso = C.idcurso 
		and A.idstaff = S.idstaff";
		//echo $sql;
		return mysqli_query($this->con,$sql);
	}
	
	public function buscar_anuncio(){
		$sql = "SELECT * from anuncio where idanuncio = ".$this->idanuncio;
		//echo $sql;
		$resultado = mysqli_query($this->con,$sql);
		return mysqli_fetch_assoc($resultado);
	}
	
	public function eliminar_anuncio(){
		$sql = "DELETE from anuncio where idanuncio = ".$this->idanuncio;
		//echo $sql;
		mysqli_query($this->con,$sql);
	}
	
/******************************************************************************************
 * PERSONALIZACI_N GR_FICA DE LA PLATAFORMA
******************************************************************************************/
	// Insertamos la imagen
	public function insertar_imagenlogo($descripcion){
		if(!empty ($_FILES['imagen']['tmp_name'])){
			$sql = "INSERT into personalizacion (descripcion_imagen) VALUES ('$descripcion')";
			//echo $sql."<br/>";
			mysqli_query($this->con,$sql);
			
			$sql = "SELECT MAX(idpersonalizacion) from personalizacion";
			$resultado = mysqli_query($this->con,$sql);
			$f=mysqli_fetch_assoc($resultado);
			
			$extension = end(explode(".", $_FILES['imagen']['name']));
			
			$dir = "../imagenes/personalizacion/";
			copy($_FILES["imagen"]["tmp_name"],$dir.$f['MAX(idpersonalizacion)'].".".$extension);
			
			$sql = "UPDATE personalizacion SET imagen_logo = 'imagenes/personalizacion/".$f['MAX(idpersonalizacion)'].".".$extension."' where idpersonalizacion = ".$f['MAX(idpersonalizacion)'];
			//echo $sql;
			mysqli_query($this->con,$sql) or die ("error al introducir la ruta de la imagen");
		}		
	}
	
	// Buscamos todas las imagenes
	public function imageneslogo(){
		$sql = "SELECT * from personalizacion";
		$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}
	
	// Buscamos la imagen con id pasado como parametro
	public function buscar_imagenlogo($idimagenlogo){
		$sql = "SELECT * from personalizacion where idpersonalizacion = ".$idimagenlogo;
		$resultado = mysqli_query($this->con,$sql);
		$fila = mysqli_fetch_assoc($resultado);
		return $fila;
	}
	
	// Eliminamos la imagen del logo
	public function eliminar_imageneslogo($idimagenlogo){
		$sql = "DELETE from personalizacion where idpersonalizacion = ".$idimagenlogo;
		//echo $sql;
		mysqli_query($this->con,$sql);
	}
	
	//editamos la imagen seleccionada
	public function editar_imageneslogo($idimagenlogo,$adescripcion_imagen){
		$sql = "UPDATE personalizacion SET descripcion_imagen = '$adescripcion_imagen' where idpersonalizacion = ".$idimagenlogo;
		mysqli_query($this->con,$sql);
		if(!empty ($_FILES['imagen']['tmp_name'])){
			$sql = "SELECT * from personalizacion where idpersonalizacion = ".$idimagenlogo;
			//echo $sql;
			$resultado = mysqli_query($this->con,$sql);
			$f = mysqli_fetch_assoc($resultado);
			unlink("../".$f['imagen_logo']);
			$extension = end(explode(".", $_FILES['imagen']['name']));
			$dir = "../imagenes/personalizacion/";
			copy($_FILES["imagen"]["tmp_name"],$dir.$idimagenlogo.".".$extension);
		}
		$sql = "UPDATE personalizacion SET imagen_logo = 'imagenes/personalizacion/".$idimagenlogo.".".$extension."' where idpersonalizacion = ".$idimagenlogo;
		mysqli_query($this->con,$sql);		
	}
	
	//Obtemos la imagen de la empresa para personalizar la interfaz gr_fica
	public function custom_logo(){
		$sql = "SELECT * from entidad_organizadora LIMIT 0,1";
		$resultado = mysqli_query($this->con,$sql);
		$fila = mysqli_fetch_assoc($resultado);
		return $fila;
	}
	
}
?>
