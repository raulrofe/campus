<?php
class Cofinanciacion extends modeloExtend
{
	public static function orderArrayCofinanciacion($array) {
		usort($array, "self::compareFechaEntrada");
	}

	// Funcion auxiliar para ordenar un array por el campo fecha_entrada
	public static function compareFechaEntrada($a, $b){
		if(!empty($a->fecha_entrada) && !empty($b->fecha_entrada)){
		    if (strtotime($a->fecha_entrada) == strtotime($b->fecha_entrada)) {
		        return 0;
		    }
		    return (strtotime($a->fecha_entrada) < strtotime($b->fecha_entrada)) ? -1 : 1;
		}else{
			return 0;
		}
	}


	// Funcion auxiliar para ordenar un array por el campo fecha_entrada
	public static function secondToFormat($cantidad){
		$seconds = $cantidad;
		$hours = floor($seconds / 3600);
		$seconds -= $hours * 3600;
		$minutes = floor($seconds / 60);
		$seconds -= $minutes * 60;

		$horas = ($hours == 0) ?  '0': $hours; 
		$horas = ($hours >= 10) ? $hours : '0'.$hours; 
		$minutes = ($minutes >= 10) ? $minutes : '0'.$minutes; 
		$seconds = ($seconds >= 10) ? $seconds : '0'.$seconds;
	     
	  	$str = $horas . ':' . $minutes . ':' . $seconds; 

	  	return $str;
	}

	// Funcion para crear un registro real
	public static function createRegistroReal($InicioReal, $FinReal, $accesosReales){
		$diferencia = strtotime($FinReal) - strtotime($InicioReal);
		$registroReal = array(
			'fecha_entrada' 		=> $InicioReal,
			'fecha_salida' 			=> $FinReal,
			'diferencia' 			=> $diferencia,
			'diferenciaFormateada' 	=> self::secondToFormat($diferencia)
		);
		array_push ( $accesosReales, $registroReal );

		return $accesosReales;
	}

	// Funcion para sumar tiempos
	public static function sumaTiempos($InicioReal, $FinReal, $sumaTiempos){
		$diferencia = strtotime($FinReal) - strtotime($InicioReal);
		$sumaTiempos += $diferencia;

		return $sumaTiempos;
	}
}