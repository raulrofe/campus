<?php
class Url
{
	public static function redirect($url, $isReferer = false, $addBase = true)
	{
		$urlRedir = '';
		
		if($addBase)
		{
			if(defined('IS_ADMIN') && IS_ADMIN)
			{
				$urlRedir = URL_BASE_ADMIN;
			}
			else
			{
				$urlRedir = URL_BASE;
			}
		}
		
		if($isReferer && isset($_SERVER['HTTP_REFERER']))
		{
			$get = Peticion::obtenerGet();
			$post = Peticion::obtenerPost();
			
			if(isset($get['url-referer']))
			{
				if(defined('IS_ADMIN') && IS_ADMIN)
				{
					$urlRedir = URL_BASE_ADMIN;
				}
				else
				{
					$urlRedir = URL_BASE;
				}
				$urlRedir .= $get['url-referer'];
			}
			else if(isset($post['url-referer']))
			{
				if(defined('IS_ADMIN') && IS_ADMIN)
				{
					$urlRedir = URL_BASE_ADMIN;
				}
				else
				{
					$urlRedir = URL_BASE;
				}
				$urlRedir .= $post['url-referer'];
			}
			else
			{
				$urlRedir = $_SERVER['HTTP_REFERER'];
			}
		}
		else
		{
			if(!isset($url))
			{
				$urlRedir = '';
			}
			else
			{
				$urlRedir .= $url;
			}
		}
				// exit($urlRedir);
		
		header('Location: ' . $urlRedir);
		die();
	}
	
	public static function reload()
	{
		self::redirect(URL_CURRENT, false, false);
	}
	
	public static function lanzar404()
	{
		require_once PATH_ROOT . 'plantillas/error404.php';
		die();
	}
	
	public static function getIpAddress()
	{
		$ipAddress = '';
		
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
		{
        	$ipAddress = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ipAddress = $_SERVER['REMOTE_ADDR'];
		}
		
		return $ipAddress;
	}
	
	public static function isValid($url)
	{
		$isLink = false;
		
		if(preg_match('/(((f|ht){1}tp(s)?:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/', $url))
		{
			$isLink = true;
		}
		
		return $isLink;
	}
}