<?php
class Html
{
/******************************************************************************************
 *  SELECT CON LOS DIAS DE LA SEMANA
******************************************************************************************/
	public static function dias_semana(){
	?>
	<select name='dias_semana'>
		<option value='1'>Lunes</option>
		<option value='2'>Martes</option>
		<option value='3'>Mi&eacute;rcoles</option>
		<option value='4'>Jueves</option>
		<option value='5'>Viernes</option>
		<option value='6'>S&aacute;bado</option>
		<option value='7'>Domingo</option>
	</select>
	<?php 
	}
	
/******************************************************************************************
 *  SELECT CON LAS HORAS Y LOS MINUTOS
******************************************************************************************/
	public static function horas(){
	?>
		<option value='00'>00</option>
		<option value='01'>01</option>
		<option value='02'>02</option>
		<option value='03'>03</option>
		<option value='04'>04</option>
		<option value='05'>05</option>
		<option value='06'>06</option>
		<option value='07'>07</option>
		<option value='08'>08</option>
		<option value='09'>09</option>
		<option value='10'>10</option>
		<option value='11'>11</option>
		<option value='12'>12</option>
		<option value='13'>13</option>
		<option value='14'>14</option>
		<option value='15'>15</option>
		<option value='16'>16</option>
		<option value='17'>17</option>
		<option value='18'>18</option>
		<option value='19'>19</option>
		<option value='20'>20</option>
		<option value='21'>21</option>
		<option value='22'>22</option>
		<option value='23'>23</option>
	<?php 
	}
	
	public static function minutos(){
	?>
		<option value='00'>00</option>
		<option value='15'>15</option>
		<option value='30'>30</option>
		<option value='45'>45</option>

	<?php 
	}
}