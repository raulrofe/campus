<?php
class Plantilla
{
	private $_templateWrapper;
	private $_template = null;
	private $_contenido = '';
	private $_post;
	private $_objUsuarios;
	private $_rowEntOrg;
	private $_rowCurso;
	private $_rowUsuarioLogueado;
	private $_contentEmail;
	//private $_idAlumno;
	
	public function set($idCurso, $idUsuario)
	{
		$this->_templateWrapper = file_get_contents(PATH_ROOT . 'plantillas/email_general.html');
		
		$this->_post = Peticion::obtenerPost();
		$this->_objUsuarios = new Usuarios();
		
		// obtenemos al usuario logueado (tutor)
		$this->_objUsuarios->set_usuario($idUsuario);
		$this->_rowUsuarioLogueado = $this->_objUsuarios->obtenerUsuarioMatricula('tutor');
		$this->_rowUsuarioLogueado = $this->_rowUsuarioLogueado->fetch_object();
		
		// datos de la empresa
		mvc::cargarModuloSoporte('gestor_correos');
		$objModeloGestCorreos = new Gestor_correos();
		$rowEntOrg = $objModeloGestCorreos->obtenerConfigEntidadOrg();
		$this->_rowEntOrg = $rowEntOrg->fetch_object();
		
		// obtenemos el curso
		$objCurso = new Cursos();
		$objCurso->set_curso($idCurso);
		$rowCurso = $objCurso->buscar_curso();
		$this->_rowCurso = $rowCurso->fetch_object();
	}
	
	/*public function setIdAlumno($idAlumno)
	{
		
	}*/
	
	public function cargarPlantilla($templateName)
	{
		$this->_template = $templateName;
	}
	
	/*public function establecerContenido($contenido)
	{
		$this->_contenido = $contenido;
	}*/
	
	/*public function establecerAlumno($alumno)
	{
		$this->_alumno = $alumno;
	}*/
	
	private function _buscarVars()
	{
		$vars = array();
		
		preg_match_all('/\{([A-Za-z0-9_]+)\}/', $this->_template, $matches);
		$vars = $matches[1];
		
		return $vars;
	}
	
	public function reemplazar()
	{
		$arrayNamesVars = array();
		$arrayValuesVars = array();
		
		$nameVars = $this->_buscarVars();
		
		//if(is_array($nameVars))
		//{
			foreach($nameVars as $item)
			{
				switch($item)
				{
					// obtenemos el nombre del tutor logueado
					case 'tutor':
						$arrayNamesVars[] = '{tutor}';
						$arrayValuesVars[] = $this->_rowUsuarioLogueado->nombrec;
						
						break;
						
					/*case 'tutores':
						$arrayNamesVars[] = '{tutores}';
						break;*/
						
					case 'fecha_curso':
						$arrayNamesVars[] = '{fecha_curso}';
						
						// mes de inicio, mes de fin y a_o de fin de curso
						$arrayValuesVars[] = date('F', strtotime($this->_rowCurso->f_inicio)) . ' - ' . date('F', strtotime($this->_rowCurso->f_fin)) . ' de ' . date('Y', strtotime($this->_rowCurso->f_fin));
						
						break;
						
					case 'fecha_entrega_certificado':
						$arrayNamesVars[] = '{fecha_entrega_certificado}';
						$arrayValuesVars[] = date('F', strtotime($this->_rowCurso->f_fin) + 8035200); // solo el mes
						
						break;
						
					case 'curso':
						$arrayNamesVars[] = '{curso}';
						$arrayValuesVars[] = '"' . $this->_rowCurso->titulo . '"';
						break;
						
					/*case 'alumno':
						//if(isset($post['idalumnos']) && is_array($post['idalumnos']))
						//{
							$arrayNamesVars[] = '{alumno}';
							
							$this->_objUsuarios->set_usuario($this->_idAlumno);
							$rowAlumno = $this->_objUsuarios->obtenerUsuarioMatricula('alumno');
							if($rowAlumno->num_rows == 1)
							{
								$alumno = $alumno->fetch_object();
								$arrayValuesVars[] = $alumno->nombre . ' ' . $alumno->apellidos;
							}
							else
							{
								$arrayValuesVars[] = '';
							}
						//}
						break;*/
					
					case 'fax':
						$arrayNamesVars[] = '{fax}';
						$arrayValuesVars[] = $this->_rowEntOrg->fax_contacto;
						break;
						
					// ficha de contacto con a empresa
					case 'contacto':
						// obtenemos el cargo del perfil
						switch($this->_rowUsuarioLogueado->status)
						{
							case 'coordinador':
								$profileName = 'Responsable de Formaci&oacute;n';
								break;
							case 'tutor1':
								$profileName = 'Tutor/a';
								break;
							case 'tutor2':
								$profileName = 'Tutor/a';
								break;
							default:
								$profileName = '';
								break;
						}
						
						$arrayNamesVars[] = '{contacto}';
						
						$arrayValuesVars[] = 
							$this->_rowUsuarioLogueado->nombrec . "\n" .
							$profileName . "\n" .
							$this->_rowUsuarioLogueado->email . "\n" .
							'Tel. +34 ' . $this->_rowEntOrg->telefono_contacto . "\n" .
							'Fax +34 ' . $this->_rowEntOrg->fax_contacto . "\n" .
							$this->_rowEntOrg->web_corporativa;
						
						break;
						
					// ficha de saludoc del responsable de la empresa
					case 'saludo_coordinador':
						$arrayNamesVars[] = '{saludo_coordinador}';
						
						$arrayValuesVars[] = 
							Texto::decode("Un saludo,\n" .
							$this->_rowEntOrg->responsable . "\n" .
							"Responsable de Formaci&oacute;n\n" .
							"Fundaci&oacute;n AULA_SMART\n");
						
						break;
					
						// ficha de contacto con la responsable de la empresa
					case 'contacto_coordinador':
						$arrayNamesVars[] = '{contacto_coordinador}';
						
						$arrayValuesVars[] =
							Texto::decode($this->_rowEntOrg->responsable . "\n" .
							"Responsable de Formaci&oacute;n\n" .
							$this->_rowEntOrg->responsable_email . "\n" .
							'Tel. +34 ' . $this->_rowEntOrg->telefono_contacto . "\n" .
							'Fax +34 ' . $this->_rowEntOrg->fax_contacto . "\n" .
							$this->_rowEntOrg->web_corporativa);
						
						break;
						
					case 'email_secretaria':
						$arrayNamesVars[] = '{email_secretaria}';
						$arrayValuesVars[] = $this->_rowEntOrg->email_secretaria;
						break;
						
					case 'fecha_curso':
						$arrayNamesVars[] = '{fecha_curso}';
						
						$arrayValuesVars[] = date('F', strtotime($rowCurso->f_inicio)) . ' - ' . date('F de Y', strtotime($rowCurso->f_fin));
						
						break;
						
					default:
						break;
				}
			}
			
			// a_adimos las variables en la plantilla
			$this->_contentEmail = str_replace($arrayNamesVars, $arrayValuesVars, $this->_template);
			//$contentEmail = str_replace('{contenido_email}', $contentEmail, $this->_templateWrapper);
		//}
		
		return $this->_contentEmail;
	}
	
	public function limpiar()
	{
		$this->_templateWrapper = null;
		$this->_template = null;
		$this->_contenido = '';
		$this->_post = null;
		$this->_objUsuarios = null;
		$this->_rowEntOrg = null;
		$this->_rowUsuarioLogueado = null;
		$this->_contentEmail = null;
		$this->_rowCurso = null;
		//$this->_idAlumno = null;
	}
}