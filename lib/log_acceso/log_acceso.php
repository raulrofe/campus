<?php
class LogAcceso
{
	private static $_instancia;
	private $_objModelo;
	private static $_idAcceso;

	private function __construct()
	{
		$this->_objModelo = new ModeloLogAcceso();
	}

	public function insertarLog()
	{
		$idLugar = self::obtenerIdLugarAcceso();
		if(isset($idLugar))
		{
			if(Usuario::compareProfile('alumno'))
			{
				$this->_objModelo->insertarLog(Usuario::getIdUser(), Usuario::getIdCurso(), $idLugar);
				self::$_idAcceso = $this->_objModelo->obtenerUltimoIdInsertado();
			}
			else
			{
				$this->_objModelo->insertarLogStaff(Usuario::getIdUser(), Usuario::getIdCurso(), $idLugar);
				self::$_idAcceso = $this->_objModelo->obtenerUltimoIdInsertado();
			}
		}
	}

	public function cerrarLog($idAcceso)
	{
		if(Usuario::compareProfile('alumno'))
		{
			$this->_objModelo->cerrarLog($idAcceso);
		}
		else
		{
			$this->_objModelo->cerrarLogStaff($idAcceso);
		}
	}

	public function cerrarSession($idLogSession){
		if(Usuario::compareProfile('alumno'))
		{
			$this->_objModelo->cerrarSession($idLogSession);
		}
	}

	public static function obtenerIdAcceso()
	{
		return self::$_idAcceso;
	}

	public static function obtenerIdLugarAcceso()
	{
		$idLugar = null;

		$get = Peticion::obtenerGet();
		if(isset($get['m']))
		{
			switch($get['m'])
			{
				case 'menus':
					if(isset($get['v']))
					{
						switch($get['v'])
						{
							case 'hall':
								$idLugar = 1;
								break;
							case 'aula':
								$idLugar = 2;
								break;
							case 'cafeteria':
								$idLugar = 3;
								break;
							case 'biblioteca':
								$idLugar = 5;
								break;
							default:
								break;
						}
					}
					break;
				case 'foro':
					$idLugar = 13;
					break;
				case 'agenda':
					$idLugar = 14;
					break;
				case 'chat':
					$idLugar = 7;
					break;
				case 'gestor_correos':
					$idLugar = 8;
					break;
				case 'tablon_anuncio':
					$idLugar = 4;
					break;
				case 'contenidos':
					$idLugar = 18;
					break;
				case 'archivador_electronico':
					$idLugar = 12;
					break;
				case 'trabajos_practicos':
					$idLugar = 4;
					break;
				case 'faq':
					$idLugar = 10;
					break;
				case 'opiniones':
					$idLugar = 6;
					break;
				case 'tablon_anuncios':
					$idLugar = 20;
					break;
				case 'publicaciones_educativas':
					$idLugar = 21;
					break;
				case 'chat_tutoria':
					$idLugar = 22;
					break;
				case 'test':
					$idLugar = 16;
					break;
				default:
					break;
			}
		}

		return $idLugar;
	}

	public static function obtenerInstancia()
	{
		if(!isset(self::$_instancia))
		{
			self::$_instancia = new self();
		}

		return self::$_instancia;
	}
}