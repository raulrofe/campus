<?php
class ModeloLogAcceso extends modeloExtend
{
	public function insertarLog($idAlumno, $idCurso, $idLugar)
	{
		$sql = 'INSERT INTO logs_acceso (idlugar, fecha_entrada, idmatricula) VALUES (' . $idLugar . ', "' . date('Y-m-d H:i:s') . '",' .
		' (SELECT idmatricula FROM matricula WHERE idalumnos = ' . $idAlumno . ' AND idcurso = ' . $idCurso  . '))';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function insertarLogStaff($idrrhh, $idCurso, $idLugar)
	{
		$sql = 'INSERT INTO logs_acceso_staff (idlugar, fecha_entrada, idstaff) VALUES (' . $idLugar . ', "' . date('Y-m-d H:i:s') . '",' .
		' (SELECT idstaff FROM staff WHERE idrrhh = ' . $idrrhh . ' AND idcurso = ' . $idCurso  . '))';
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function cerrarLog($idAcceso)
	{
		$sql = 'UPDATE logs_acceso SET fecha_salida = "' . date('Y-m-d H:i:s') . '"' .
		' WHERE fecha_salida IS NULL AND idaccesos = ' . $idAcceso;
		//' WHERE idmatricula = (SELECT idmatricula FROM matricula WHERE idalumnos = ' . $idAlumno . ' AND idcurso = ' . $idCurso . ') AND fecha_salida IS NULL AND idaccesos = ' . $idAcceso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	public function cerrarLogStaff($idAcceso)
	{
		$sql = 'UPDATE logs_acceso_staff SET fecha_salida = "' . date('Y-m-d H:i:s') . '"' .
		' WHERE fecha_salida IS NULL AND idaccesos = ' . $idAcceso;
		//' WHERE idmatricula = (SELECT idmatricula FROM matricula WHERE idalumnos = ' . $idAlumno . ' AND idcurso = ' . $idCurso . ') AND fecha_salida IS NULL AND idaccesos = ' . $idAcceso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
	public function cerrarSession($idLogTiempoSesion)
	{
		$sql = 'UPDATE log_tiempo_sesion SET fin_sesion = "' . date('Y-m-d H:i:s') . '"' .
		' WHERE id = ' . $idLogTiempoSesion;
		//' WHERE idmatricula = (SELECT idmatricula FROM matricula WHERE idalumnos = ' . $idAlumno . ' AND idcurso = ' . $idCurso . ') AND fecha_salida IS NULL AND idaccesos = ' . $idAcceso;
		$resultado = $this->consultaSql($sql);

		return $resultado;
	}
}