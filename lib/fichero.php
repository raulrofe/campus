<?php
class Fichero
{
	public static function obtenerDirectorios($ruta)
	{
		$directorios = array();
		
		// abrir un directorio y listarlo recursivo
   		if(is_dir($ruta))
   		{
      		if($dh = opendir($ruta))
      		{
        		while(($file = readdir($dh)) !== false)
        		{
            		//esta l_nea la utilizar_amos si queremos listar todo lo que hay en el directorio
            		//mostrar_a tanto archivos como directorios
            		//echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file);
            		if(is_dir($ruta . $file) && $file!="." && $file!="..")
            		{
               			//solo si el archivo es un directorio, distinto que "." y ".."
               			$directorios[] = $ruta . $file . "/";
               			self::obtenerDirectorios($ruta . $file . "/");
            		}
         		}
         		
      			closedir($dh);
      		}
   		}
   		else
   		{
   			//echo "<br>No es ruta valida";
		}
		
		return $directorios;
	}

	public static function obtenerFicheros($ruta)
	{
		$directorios = array();
		
		// abrir un directorio y listarlo recursivo
   		if(is_dir($ruta))
   		{
      		if($dh = opendir($ruta))
      		{
        		while(($file = readdir($dh)) !== false)
        		{
            		//esta l_nea la utilizar_amos si queremos listar todo lo que hay en el directorio
            		//mostrar_a tanto archivos como directorios
            		//echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file);
            		if(!is_dir($ruta . $file)  && $file!="..")
            		{
               			//solo si el archivo es un directorio, distinto que "." y ".."
               			$directorios[] = $ruta . $file;
            		}
         		}
         		
      			closedir($dh);
      		}
   		}
   		else
   		{
   			//echo "<br>No es ruta valida";
		}
		
		return $directorios;
	}
	
	public static function removeDir($directorio, $borrarRaiz = true)
	{
		$result = false;
		
		$directorio = rtrim($directorio, '/') . '/';
	 	
		if($carpetaActual = opendir($directorio))
		{
			while(($contenidoCarpeta = readdir($carpetaActual)) !== false)
			{
		  		$rutaCompleta = $directorio . $contenidoCarpeta;
		  		if($contenidoCarpeta != '.' && $contenidoCarpeta != '..') 
		  		{           
		  			if(is_dir($rutaCompleta))
		  			{
		  				self::removeDir($rutaCompleta, true);
		  			} 
		   			else
		   			{
		   				unlink($rutaCompleta);
		   			}
		  		}
		 	}
		 	closedir($carpetaActual);
		 	
		 	$result = true;
		 
		 	if($borrarRaiz)
		 	{
			 	if(!rmdir($directorio))
				{
					$result = false;
				}
		 	}
		}
		
	 	return $result;
	}
	
	public static function obtenerExtension($fichero)
	{
		$ext = pathinfo($fichero);
		if(isset($ext['extension']))
		{
			return $ext['extension'];
		}
		else
		{
			return '';
		}
	}
	
	public static function importar($path)
	{
		require $path;
	}
	
	public static function validarTipos($fichero, $mimeFichero, $tipos)
	{
		$valido = false;
		
		require PATH_ROOT . 'configMimes.php';
		
		$ext = self::obtenerExtension($fichero);

		if(isset($mimes[$ext]) && in_array($ext, $tipos) && ((is_array($mimes[$ext]) && in_array($mimeFichero, $mimes[$ext]))
			|| (!is_array($mimes[$ext]) && $mimes[$ext] == $mimeFichero)))
		{
			$valido = true;
		}

		return $valido;
	}
	
	public static function obtenerMime($fichero)
	{
		$valido = false;
		
		require PATH_ROOT . 'configMimes.php';
		
		$ext = self::obtenerExtension($fichero);
		//$ext = 'xsl';
		
		if(isset($mimes[$ext]))
		{
			if(is_array($mimes[$ext]))
			{
				$valido = $mimes[$ext][0];
			}
			else 
			{
				$valido = $mimes[$ext];
			}
		}
		
		return $valido;
	}
}