<?php

class Alerta {

	public static function getStringNotification($key, $message) {
		$string = '';
		if (empty($message)) {
			// If only exist one parameters, $key it's $message default
			$string = $key;
		} else {
			// If exist two parameteres, set message string as default value
			$string = $message;
			$translate = self::translateString('notificaciones', $key);
			// If return translation value
			if (!empty($translate)) {
				$string = $translate;
			}
		}
		return $string;
	}

	public static function getStringAlerts($key, $message) {
		$string = '';
		if (empty($message)) {
			// If only exist one parameters, $key it's $message default
			$string = $key;
		} else {
			// If exist two parameteres, set message string as default value
			$string = $message;
			$translate = self::translateString('alertas', $key);
			// If return translation value
			if (!empty($translate)) {
				$string = $translate;
			}
		}
		return $string;
	}

	public static function translateString($subkey, $key) {
		// Set vars
		$language = (!empty($_COOKIE["language"])) ? $_COOKIE["language"] : "es";
		$key = $subkey . "." . $key;
		$arrayKey = explode(".", $key);
		// Get JSON
		$content = file_get_contents('translate/' . $language . '.json');
		$json = json_decode($content);
		// Get value for this key in JSON
		$value = $json;
		// Search in JSON with key index
		for ($i = 0; $i < count($arrayKey); $i++) {
			if ($i < count($arrayKey)) {
				if (!empty($value->$arrayKey[$i])) {
					$value = $value->$arrayKey[$i];
				} else {
					$value = '';
				}
			}
		}
		return $value;
	}

	public static function mostrarMensajeInfo($key, $message = null) {
		// Get string to show on notification
		$string = self::getStringNotification($key, $message);

		// Set variable in Javascript Script, to show notificacion on Frontend
		$html = "<script type='text/javascript' src='js/alert.js'></script>
				<script type='text/javascript'>alertMsg('" . $string . "');</script>";

		echo $html;
	}

	public static function guardarMensajeInfo($key, $message = null) {
		// Get string to show on notification
		$string = self::getStringNotification($key, $message);

		// Set variable in session, to show notificacion on Frontend
		if (isset($_SESSION['mensajeInfo'])) {
			$_SESSION['mensajeInfo'] = $_SESSION['mensajeInfo'] . '<br />' . trim($string);
		} else {
			$_SESSION['mensajeInfo'] = trim($string);
		}
	}

	public static function leerMensajeInfo() {
		if (isset($_SESSION['mensajeInfo']) && !empty($_SESSION['mensajeInfo'])) {
			self::mostrarMensajeInfo(addslashes($_SESSION['mensajeInfo']));
			unset($_SESSION['mensajeInfo']);
		}
	}

	public static function alertConfirm($key, $mensaje, $urlConfirm) {
		$get = Peticion::obtenerGet();
		if (!IS_ADMIN) {
			$partsUrl = parse_url($urlConfirm);

			// si ya esista la variable url-referer no la crea
			if (isset($partsUrl['query']) && preg_match('/^(\?|&)(url-referer=)(.*)$/', $partsUrl['query'])) {
				
			}
			// si no existe la variable url-referer
			else {
				$urlReferer = URL_CURRENT;

				// si hay mas variables en GET, usamos &, sino usamos ?
				$operatorQuery = '?';
				if (isset($partsUrl['query']) && !empty($partsUrl['query'])) {
					$operatorQuery = '&';
				}

				$urlConfirm .= $operatorQuery . 'url-referer=' . $urlReferer;
			}
		}
		$id = 'alertConfimLink_' . uniqid();

		// Get string to show on notification
		$string = self::getStringAlerts($key, $mensaje);

		echo '<a id="' . $id . '" href="' . $urlConfirm . '" title="" class="hide">ejemplo</a>' .
		'<script type="text/javascript">alertConfirm($("#' . $id . '"), "' . $string . '", "' . $urlConfirm . '");</script>';
	}

	public static function alertConfirmOnClick($key, $mensaje, $urlConfirm) {
		$get = Peticion::obtenerGet();
		if (!empty($urlConfirm)) {
			if (!IS_ADMIN) {
				$partsUrl = parse_url($urlConfirm);

				// si ya esista la variable url-referer no la crea
				if (isset($partsUrl['query']) && preg_match('/^(\?|&)(url-referer=)(.*)$/', $partsUrl['query'])) {
					
				}
				// si no existe la variable url-referer
				else {
					$urlReferer = URL_CURRENT;

					// si hay mas variables en GET, usamos &, sino usamos ?
					$operatorQuery = '?';
					if (isset($partsUrl['query']) && !empty($partsUrl['query'])) {
						$operatorQuery = '&';
					}

					$urlConfirm .= $operatorQuery . 'url-referer=' . $urlReferer;
				}
			}

			// Get string to show on notification
			$string = self::getStringAlerts($key, $mensaje);

			return 'data-alert-confirm="true" '
				. 'data-url="' . $urlConfirm . '" '
				. 'onclick="alertConfirm(this, \'' . $string . '\', \'' . $urlConfirm . '\'); return false;" '
				. 'data-nofollow="true"';
		}
		return 'no-urlconfirm';
	}

	public static function alertSimple($key, $mensaje) {
		// Get string to show on notification
		$string = self::getStringAlerts($key, $mensaje);

		echo '<script type="text/javascript">alert("' . $string . '");</script>';
	}

}
