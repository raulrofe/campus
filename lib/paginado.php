<?php
class Paginado
{
	public static function crear($paginaActual, $paginaMax, $url, $urlEnd = '')
	{
		$numMax = 3;
		$pageIni = 1;

		// calcula la pagina donde se empieza a mostrar el paginado
		if(($numMax - $paginaActual) < 0)
		{
			$pageIni = $paginaActual - $numMax;
		}

		$html = '<div class="paginado"><ul class="fright">';

		// volver a la primera pagina
		if($pageIni > 1)
		{
			$html .= '<li><a href="' . $url . '1' . $urlEnd . '" title="Ir a la primera p&aacute;gina">Primera</a></li><li>...</li>';
		}

		// mostrar paginas antes de la pagina actual
		for($cont = $pageIni; $cont < $paginaActual; $cont++)
		{
			$html .= '<li><a href="' . $url . $cont . $urlEnd . '" title="Ir a la p&aacute;gina ' . $cont . '">' . $cont . '</a></li>';
		}

		$html .= '<li><b>' . $paginaActual . '</b></li>';

		// mostrar paginas despues de la actual
		if($paginaMax > $pageIni)
		{
			$pageNext = ($paginaActual + 1);
			$cont = 0;
			while($cont < 3 && $pageNext <= $paginaMax)
			{
				$html .= '<li><a href="' . $url . $pageNext . $urlEnd . '" title="Ir a la p&aacute;gina ' . $pageNext . '">' . $pageNext . '</a></li>';
				$pageNext++;
				$cont++;
			}
		}

		// ir a la ultima pagina
		if($paginaMax > ($paginaActual + 3))
		{
			$html .= '<li>...</li><li><a href="' . $url . $paginaMax . $urlEnd . '" title="Ir a la &uacute;ltima p&aacute;gina">&Uacute;ltima</a></li>';
		}

		$html .= '</ul><div class="clear"></div></div><div class="clear"></div>';

		return $html;
	}

	public static function crearAjax($paginaActual, $paginaMax, $url, $urlEnd = '')
	{
		$numMax = 10000000;
		$pageIni = 1;

		// calcula la pagina donde se empieza a mostrar el paginado
		if(($numMax - $paginaActual) < 0)
		{
			$pageIni = $paginaActual - $numMax;
		}

		$html = '<div class="paginado" data-page-current="1"><ul class="fright">';

		// // volver a la primera pagina
		// if($pageIni > 1)
		// {
		// 	$html .= '<li><a data-page="1" href="#" onclick="seguimientoPagina(1); return false;" title="Ir a la primera p&aacute;gina">Primera</a></li><li>...</li>';
		// }

		// // mostrar paginas antes de la pagina actual
		// for($cont = $pageIni; $cont < $paginaActual; $cont++)
		// {
		// 	$html .= '<li><a data-page="' . $cont . '" href="#" onclick="seguimientoPagina(' . $cont . '); return false;" title="Ir a la p&aacute;gina ' . $cont . '">' . $cont . '</a></li>';
		// }

		// $html .= '<li><a data-page="' . $paginaActual . '" class="negrita" href="#" onclick="seguimientoPagina(' . $paginaActual . '); return false;" title="">' . $paginaActual . '</a></li>';

		// // mostrar paginas despues de la actual
		// if($paginaMax > $pageIni)
		// {
		// 	$pageNext = ($paginaActual + 1);
		// 	$cont = 0;
		// 	while($cont < 3 && $pageNext <= $paginaMax)
		// 	{
		// 		$html .= '<li><a data-page="' . $pageNext . '" href="#" onclick="seguimientoPagina(' . $pageNext . '); return false;" title="Ir a la p&aacute;gina ' . $pageNext . '">' . $pageNext . '</a></li>';
		// 		$pageNext++;
		// 		$cont++;
		// 	}
		// }

		// // ir a la ultima pagina
		// if($paginaMax > ($paginaActual + 3))
		// {
		// 	$html .= '<li>...</li><li><a data-page="' . $paginaMax . '" href="#" onclick="seguimientoPagina(' . $paginaMax . '); return false;" title="Ir a la &uacute;ltima p&aacute;gina">&Uacute;ltima</a></li>';
		// }

		for($cont = $pageIni; $cont <= $paginaMax; $cont++)
		{
			$html .= '<li><a data-page="' . $cont . '" href="#" onclick="seguimientoPagina(' . $cont . '); return false;" title="Ir a la p&aacute;gina ' . $cont . '">' . $cont . '</a></li>';
		}

		$html .= '</ul><div class="clear"></div></div><div class="clear"></div>';

		return $html;
	}
}