<?php
class Database
{
	const servidor = CONNECT_SERVER;
	const bd = CONNECT_BBDD;
	const usuario = CONNECT_USERNAME;
	const pass = CONNECT_PASS;

	private static $con;

	//constructor
	public function Database()
	{
		if(!isset(self::$con))
		{
			if(self::$con = mysqli_connect(self::servidor,self::usuario,self::pass,self::bd))
			{
				$sql = "SET NAMES 'UTF8'";
				mysqli_query(self::$con, $sql);
			}
			else
			{
				Log::set('Error al conectar con la BBDD');
			}
		}
	}
	
	public function conectar()
	{
		return self::$con;
	}

	// Le pasamos como parametro una sql y devuelve un array con los resultados
	public function query($sql)
	{
		$resultado =  mysqli_query(self::$con, $sql);
		return mysqli_fetch_assoc($resultado);
	}
		
	//Liberamos resultados
	public function liberar($resultado)
	{
		mysqli_free_results($resultado);
	}
	
	//desconectamos
	public function desconectar()
	{
		mysqli_close(self::$con);
	}
}
?>