<?php
class Alumnos extends modeloExtend{

	public $user;
	
	//Establecemos un alumno
	public function set_alumno($idalumno){$this->user = $idalumno;}
	
	//insertamos un alumno
	public function insertar_alumno($nombre,$apellidos,$dni,$f_nacimiento,$lugar_nacimiento,$telefono,$telefono2,$direccion,$cp,$localidad,$provincia,$pais,$email,$usuario,$pass,$idcentros){
		$pass = md5($pass);
		$sql = "SELECT * from alumnos where dni like '%".$dni."%'";
		$resultado = $this->consultaSql($sql);
		if(mysqli_num_rows($resultado) == 0){
			$sql = "INSERT into alumnos 
			(nombre,apellidos,dni,f_nacimiento,lugar_nacimiento,telefono,telefono_secundario,direccion,cp,poblacion,provincia,pais,email,usuario,pass)
			VALUES ('$nombre','$apellidos','$dni','$f_nacimiento','$lugar_nacimiento','$telefono','$telefono2','$direccion','$cp','$localidad','$provincia','$pais','$email','$usuario','$pass')";
			$this->consultaSql($sql);
			return $valor = 'true';
		}
		else return $valor = 'error01';
	}
	
	//Buscamos un alumno
	public function ver_alumno(){
		$sql = "SELECT *, CONCAT(apellidos, ', ', nombre) AS nombrec from alumnos where idalumnos = ".$this->user;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//buscamos todos los alumnos que esten cursando un curso indicado
	public function alumnos_del_curso_activos($idcurso)
	{
	 $sql = "SELECT * from matricula M , alumnos A" . 
	 " where $idcurso = M.idcurso" .
	 " and M.idalumnos = A.idalumnos" .
	 " and M.borrado = 0" .
	 " and A.borrado = 0" .
	 " ORDER BY A.apellidos ASC";
	 //echo $sql;
	 $resultado = $this->consultaSql($sql);
	 return $resultado;
	}
	
	//buscamos todos los alumnos que esten cursando un curso indicado
	public function alumnos_del_curso($idcurso)
	{
	 $sql = "SELECT * from matricula M , alumnos A 
	 where $idcurso = M.idcurso
	 and M.idalumnos = A.idalumnos";
	 //echo $sql;
	 $resultado = $this->consultaSql($sql);
	 return $resultado;
	}
	
	//buscamos todos los alumnos que esten cursando un curso indicado
	public function alumnos_del_curso_buscado($idcurso)
	{
	$post = Peticion::obtenerPost();
		
	 $sql = "SELECT * from matricula M , alumnos A 
	 where $idcurso = M.idcurso
	 and M.idalumnos = A.idalumnos
	 and A.apellidos like '%".preg_quote($post['buscar'], '-')."%'
	 GROUP BY A.idalumnos 
	 ORDER BY A.apellidos ASC";
	 //echo $sql;
	 $resultado = $this->consultaSql($sql);
	 return $resultado;
	}
	
	public function actualizar_foto_usuario($foto){
		$sql = "UPDATE alumnos SET
		foto = '$foto'
		where idalumnos = ".$this->user;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	//buscar alumnos por varios parametros nombre, apellidos, telefonos, email, dni, curso
	public function buscar_alumnos($nombre,$apellidos,$telefono,$telefono2,$email,$dni,$idcurso){
		$sql="SELECT * from alumnos A ";
		$sql.=" where 1 = 1 ";
			if(!empty($nombre)) $sql.="and A.nombre like '%".$nombre."%' ";
			if(!empty($apellidos)) $sql.="and A.apellidos like '%".$apellidos."%' ";
			if(!empty($telefono)) $sql.="and A.telefono like '%".$telefono."%' ";
			if(!empty($telefono2)) $sql.="and A.telefono_secundario like '%".$telefono2."%' ";
			if(!empty($email)) $sql.="and A.email like '%".$email."%' ";
			if(!empty($dni)) $sql.="and A.dni like '%".$dni."%' ";
			if(!empty($idcurso)) $sql.="and M.idcurso = ".$idcurso;
		//$sql.=" and A.idalumnos = M.idalumnos and M.idcurso = C.idcurso";
		$sql.=" order by A.nombre ASC";
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//eliminar alumno
	public function eliminar_alumno(){
		$sql ="DELETE from alumnos where idalumnos = ".$this->user;
		//echo $sql;
		log::set("Error al eliminar alumno");
		$this->consultaSql($sql);
	}
	
	//actualizar alumno
	public function actualizar_alumno($anombrec,$aapellidos,$adni,$af_nacimiento,$atelefono,$atelefono2,$adireccion,$acp,$alocalidad,$aprovincia,$apais,$ausuario,$alugar_nacimiento,$aemail){
		$sql = "UPDATE alumnos SET
		nombre='$anombrec',apellidos='$aapellidos',dni='$adni',f_nacimiento='$af_nacimiento',
		lugar_nacimiento='$alugar_nacimiento',telefono='$atelefono',
		telefono_secundario='$atelefono2',direccion='$adireccion',cp='$acp',
		poblacion='$alocalidad',provincia='$aprovincia',pais='$apais',email='$aemail',
		usuario='$ausuario' 
		where idalumnos = ".$this->user;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	//select de todos los alumnos
	public function select_alumnos($idalumno){
		$sql="SELECT * from alumnos";
		$resultado = $this->consultaSql($sql);
		echo "<select name='idalumno'>";
			while($f = mysqli_fetch_assoc($resultado)){
				if($idaulumno == $f['idalumnos']) echo "<option value='".$f['idalumnos']."' selected>".$f['nombre']." ".$f['apellidos']." - (".$f['dni'].")</option>";
				else echo "<option value='".$f['idalumnos']."'>".$f['nombre']." ".$f['apellidos']." ".$f['dni']."</option>";
			}
		echo "</select>";
	}

/*******************************
FUNCIONES PARA LA MATRICULACION
*******************************/

	//Realizar la matriculacion
	public function matricular($idcurso,$idalumno)
	{
		$fecha = date("Y-m-d");
		$sql="INSERT into matricula (idalumnos,idcurso,fecha) VALUES ('$idalumno','$idcurso','$fecha')";
		//echo $sql;
		$this->consultaSql($sql);
	}

	//Obtenemos el id de la matricula
	public function obtenerIdmatricula()
	{
			$sql = "SELECT * from matricula where idcurso = ".$_SESSION['idcurso']." and idalumnos = ".$_SESSION['idusuario'];
			$resultado = $this->consultaSql($sql);
			$f = mysqli_fetch_assoc($resultado);
			return $f['idmatricula'];
	}
	
}