<?php
class mvc
{
	private static $_modulo;
	
	public static function accesoServicio($tipo)
	{
		$objUsuarios = new Usuarios();
		
		$rolesUsuario = $objUsuarios->obtenerRoles(Usuario::getIdUser(), Usuario::getIdCurso());
		$rolesUsuarioArray = array();
		while($row = $rolesUsuario->fetch_object())
		{
			$rolesUsuarioArray[] = $row->idservicio;
		}
		
		$acceso = false;
		
		switch($tipo)
		{
			case 'autoevaluacion':
				if(in_array(1, $rolesUsuarioArray))
				{
					$acceso = true;
				}
				break;
			case 'trabajos_practicos':
				if(in_array(2, $rolesUsuarioArray))
				{
					$acceso = true;
				}
				break;
			default:
		}
		
		return $acceso;
	}
	
	public static function obtenerRutaControlador($loadLoader = true)
	{
		$path = null;
		
		if(defined('IS_ADMIN') && IS_ADMIN == true)
		{
			$path = self::_obtenerRutaControladorAdmin();
		}
		else
		{
			$path = self::_obtenerRutaControlador($loadLoader);
		}
		
		return $path;
	}
	
	private static function _obtenerRutaControlador($loadLoader = true)
	{
		$get = Peticion::obtenerGet();
		
		$pathReturn = null;
		$is_login = Usuario::is_login();
		
		if(isset($get['m']))
		{
			$modulo = addslashes($get['m']);
		}
		else
		{
			/*if($is_login)
			{*/
				$modulo = 'acceso';
			/*}
			else
			{
				$modulo = 'acceso';
			}*/
		}
		
		if(isset($get['c']))
		{
			$controlador = addslashes($get['c']);
		}
		else
		{
			$controlador = 'index';
		}

		if(($modulo == 'acceso' && $controlador == 'logout')
		  || ($modulo == 'acceso' && $controlador == 'index' && !$is_login)
		  || ($modulo == 'acceso' && $controlador == 'lost_pass' && !$is_login)
		  || ($modulo == 'acceso' && $controlador == 'new_pass' && !$is_login)
		  || ($modulo == 'admin' && $controlador == 'index' && !$is_login)
		  || ((($modulo == 'acceso' && $controlador != 'index') || ($modulo == 'admin' && $controlador != 'index') || ($modulo != 'acceso')) && $is_login))
		{
			// para que no acceda a la web logueado (aprate del seleccionar curso) sin la sesion idcurso
			// if($modulo == 'notificaciones' || !$is_login || ($is_login && ($modulo == 'cursos' || $modulo == 'menus' || (($modulo != 'cursos' && $modulo != 'menus') 
			// 	&& (isset($_SESSION['idcurso']) || ($modulo == 'acceso' && $controlador == 'logout'))))))
			// {
				$path = 'modulos/' . $modulo . '/controladores/' . $controlador . '.php';
				
				if(preg_match('/^([0-9A-Za-z_\-]+)$/', $modulo) && preg_match('/^([0-9A-Za-z_\-]+)$/', $controlador) && file_exists($path))
				{
					// carga el load.php
					if($loadLoader)
					{
						self::_cargarLoader($modulo);
					}
					
					// cargar modelo
					self::_cargarModelo($modulo);
					
					// cargar clase base
					self::_cargarBase($modulo);
					
					self::$_modulo = $modulo;
					
					// ruta a devolver
					$pathReturn = $path;
				}
			// }
			// else
			// {
				// if($_SESSION['idcurso'] == 820){
				// 	exit('ad');
				// }
				// Url::redirect('');
			// }
		}
			
		return $pathReturn;
	}
	
	private static function _obtenerRutaControladorAdmin()
	{
		$pathReturn = null;
		
		if(isset($_GET['m']))
		{
			$modulo = addslashes($_GET['m']);
		}
		else
		{
			$modulo = 'welcome';
		}
		
		if(isset($_GET['c']))
		{
			$controlador = addslashes($_GET['c']);
		}
		else
		{
			$controlador = 'index';
		}
		
		$path = 'modulos/' . $modulo . '/controladores/' . $controlador . '.php';
					
		if(preg_match('/^([0-9A-Za-z_\-]+)$/', $modulo) && preg_match('/^([0-9A-Za-z_\-]+)$/', $controlador) && is_file($path))
		{
			// carga el load.php
			self::_cargarLoader($modulo);
					
			// cargar modelo
			self::_cargarModelo($modulo);
					
			// cargar clase base
			self::_cargarBase($modulo);
					
			// ruta a devolver
			$pathReturn = $path;
		}
			
		return $pathReturn;
	}
	
	
	public static function obtenerRutaVista($dirname, $file)
	{
		$pathReturn = null;
		
		$path = $dirname . '/../vistas/' . $file . '.php';
			
		if(preg_match('/^([0-9A-Za-z_\-]+)$/', $file) && file_exists($path))
		{
			$pathReturn = $path;
		}
		
		return $pathReturn;
	}
	
	private static function _cargarModelo($modulo)
	{
		$path = 'modulos/' . $modulo . '/modelos/modelo.php';
		if(file_exists($path))
		{
			require_once $path;
		}
	}
	
	private static function _cargarBase($modulo)
	{
		$path = 'modulos/' . $modulo . '/' . $modulo . '.php';
		if(file_exists($path))
		{
			require_once $path;
		}
	}
	
	private static function _cargarLoader($modulo)
	{
		$path = 'modulos/' . $modulo . '/load.php';
		if(file_exists($path))
		{
			require_once $path;
		}
	}
	
	public static function cargarUnloader()
	{
		if(isset(self::$_modulo))
		{
			$path = 'modulos/' . self::$_modulo . '/unload.php';
			if(file_exists($path))
			{
				require_once $path;
			}
			
			self::$_modulo = null;
		}
	}
	
	public static function cargarModuloSoporte($modulo)
	{
		// cargar modelo
		self::_cargarModelo($modulo);
					
		// cargar clase base
		self::_cargarBase($modulo);
	}
	
	public static function importar($file)
	{
		if(is_file($file))
		{
			if(defined('IS_ADMIN') && IS_ADMIN == true)
			{
				if(file_exists(PATH_ROOT_ADMIN . $file))
				{
					require_once PATH_ROOT_ADMIN . $file;
				}
			}
			else
			{
				if(file_exists(PATH_ROOT . $file))
				{
					require_once PATH_ROOT . $file;
				}
			}
		}
		else
		{
			Url::lanzar404();
		}
		
	}
	
	public static function cargarModeloAdicional($modulo, $modelo = 'modelo', $force = null)
	{
		$filename = 'modulos/' . $modulo . '/modelos/' . $modelo . '.php';
		$pathbase = null;
		
		if(isset($force))
		{
			if($force == 'admin')
			{
				$pathbase = PATH_ROOT_ADMIN;
			}
			else
			{
				$pathbase = PATH_ROOT;
			}
		}
		else
		{
			if(defined('IS_ADMIN') && IS_ADMIN == true)
			{
				$pathbase = PATH_ROOT_ADMIN;
			}
			else
			{
				$pathbase = PATH_ROOT;
			}
		}
		
		if(file_exists($pathbase . $filename))
		{
			require_once $pathbase . $filename;
		}
	}

	public static function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $sub = null;

        if(preg_match('/MSIE *.*/i',$u_agent, $version))
        {
        	$dataExplorer = explode(';', $version[0]);
        	if($dataExplorer[0])
        	{
        		$versionExplorer = explode(' ', $dataExplorer[0]);
        		if($versionExplorer[1] < 8)
        		{
        			$sub = true;
        		}
        	}
        }

        return $sub;
    }
}