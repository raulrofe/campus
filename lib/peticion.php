<?php
class Peticion
{
	public static function obtenerPost($escapar = true, $postVar = null)
	{
		$post = array();
		
		$iniBucle = false;
		if(!isset($postVar))
		{
			$iniBucle = true;
			$postVar = $_POST;
		}
		
		foreach($postVar as $name => $value)
		{
			if(is_array($value))
			{
				$post[$name] = self::obtenerPost(true, $value);
			}
			else
			{
				$post[$name] = trim($value);
			}
		}
		
		if($iniBucle && $escapar)
		{
			$objModelo = new modeloExtend();
			$post = $objModelo->escaparTodo($post);
		}
		
		return $post;
	}
	
	public static function isPost()
	{
		$is_post = false;
		
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$is_post =  true;
		}
		
		return $is_post;
	}

	public static function isGet()
	{
		$is_get = false;
		
		if($_SERVER['REQUEST_METHOD'] == 'GET')
		{
			$is_get =  true;
		}
		
		return $is_get;
	}
	
	public static function obtenerGet()
	{
		$get = array();
		foreach($_GET as $name => $value)
		{
			if(($name == 'm' && preg_match('/[A-Za-z0-9_\-]+/', $value)) ||
				($name == 'c' && preg_match('/[A-Za-z0-9_\-]+/', $value))
				|| ($name != 'm' && $name != 'c'))
			{
				/*if(preg_match('/^id([A-Za-z]+)$/', $name))
				{
					if(filter_var($value, FILTER_VALIDATE_INT) !== false)
					{
						$get[$name] = trim($value);
					}
				}
				else
				{*/
					$get[$name] = trim($value);
				//}
			}
		}
		
		if(!isset($get['m']) || !isset($get['c']))
		{
			//Url::lanzar404();
		}
		
		return $get;
	}
	
}