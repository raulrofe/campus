<?php
class modeloExtend
{
	protected $_con;

	public function __construct()
	{
		$objDatabase = new Database();
		$this->_con = $objDatabase->conectar();
	}

	public function obtenerUltimoIdInsertado()
	{
		$id = mysqli_insert_id($this->_con);

		return $id;
	}

	public function consultaSql($sql)
	{
		$resultado = mysqli_query($this->_con, $sql);
		if(!$resultado)
		{
			echo 'Error de SQL ' . $sql;
			Log::set($sql, __FILE__);
		}

		return $resultado;
	}

	public function escaparCadena($texto)
	{
		$texto = mysqli_real_escape_string($this->_con, $texto);

		return $texto;
	}

	public function resetearPuntero($resultado)
	{
		mysql_data_seek($resultado, 1);
	}

	public function escaparTodoRef(&$array)
	{
		if(is_array($array))
		{
			foreach($array as $i => $item)
			{
				if(is_array($item))
				{
					$array[$i] = self::escaparTodo($item);
				}
				else
				{
					$array[$i] = $this->escaparCadena($item);
				}
			}
		}
		else
		{
			$array = $this->escaparCadena($array);
		}
	}

	public function escaparTodo($array)
	{
		if(is_array($array))
		{
			foreach($array as $i => $item)
			{
				if(is_array($item))
				{
					$array[$i] = self::escaparTodo($item);
				}
				else
				{
					$array[$i] = $this->escaparCadena($item);
				}
			}
		}
		else
		{
			$array = $this->escaparCadena($array);
		}

		return $array;
	}
}