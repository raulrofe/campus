<?php
class Texto
{
	public static function textoPlano($texto)
	{
		$texto = htmlentities($texto, ENT_QUOTES, 'UTF-8');
		
		return $texto;
	}
	
	public static function clearFilename($filename)
	{
		$filename = self::quitarTildes($filename);
		
		$filename = str_replace(array('\\', '/', ':', '*', '?', '"', '<', '>', '|'), array('', '-', '', '', '', '', '', '', ''), $filename);
		
		return $filename;
	}
	
	public static function decode($texto)
	{
		$texto = html_entity_decode($texto, ENT_QUOTES, 'UTF-8');
		
		return $texto;
	}
	
	public static function textoFormateado($texto)
	{
		$texto = self::textoPlano($texto);
		$texto = nl2br($texto);
		$texto = self::text2Links($texto);
		
		return $texto;
	}
	
	public static function cadenaAleatoria($length)
	{
		$string = "";
		$possible = "0123456789bcdfghjkmnpqrstvwxyz";
		$i = 0;
		while ($i < $length)
		{
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$string .= $char;
			$i++;
		}
		
		return $string;
	}
	
	public static function text2Links($text)
	{
		$text = preg_replace('/(((f|ht){1}tp(s)?:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/',
	    '<a href="$1" target="_blank">$1</a>', $text);
	
		$text = preg_replace('/(^|[[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/',
	    '$1<a href="http://$2" target="_blank">$2</a>', $text);
	
		$text = preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/',
	    '<a href="mailto:$1" target="_blank">$1</a>', $text);
		
		return $text;
	}
	
	public static function quitarTildes($str, $separator = '_')
	{
        $str = strtolower(htmlentities($str, ENT_COMPAT, 'UTF-8'));
        $str = preg_replace('/&(.)(acute|cedil|circ|lig|grave|ring|tilde|uml);/', "$1", $str);
        $str = preg_replace('/([^a-z0-9\.]+)/', $separator, html_entity_decode($str, ENT_COMPAT, 'UTF-8'));
        $str = trim($str, $separator);
        
        return $str;

	}
	
	public static function obtenerExtension($archivo)
	{
		 $ext = explode(".", $archivo);
		 $extension = end($ext);
		 return $extension;
	}
	
	public static function toEmoticons($texto)
	{
		$html_1 = '<img src="ckeditor/plugins/smiley/images/';
		$html_2 = '.gif" title="';
		$html_3 = '" alt="" />';
		
		/*
		smiley:':)',
		sad:':(',
		wink:';)',
		laugh:':D',
		cheeky:':P',
		blush:':*)',
		surprise:':-o',
		indecision:':|',
		angry:'>:(',
		angel:'o:)',
		cool:'8-)',
		devil:'>:-)',
		crying:';(',
		kiss:':-*'
		*/
		
		//smiley sad wink laugh cheeky blush surprise indecision cool crying kiss
		//'devil_smile.gif','cry_smile.gif','lightbulb.gif','thumbs_down.gif','thumbs_up.gif','heart.gif','broken_heart.gif','kiss.gif','envelope.gif'];
		
		$texto = str_replace(
			array(
				':)',
				':(',
				';)',
				':D',
				':*)',
				':-o',
				';(',
			),
			array(
				$html_1 . 'regular_smile' . $html_2 . 'Contento' . $html_3,
				$html_1 . 'sad_smile' . $html_2 . 'Triste' . $html_3,
				$html_1 . 'wink_smile' . $html_2 . 'Gui&ntilde;o' . $html_3,
				$html_1 . 'teeth_smile' . $html_2 . 'Alegre' . $html_3,
				$html_1 . 'embaressed_smile' . $html_2 . 'Avergonzado' . $html_3,
				$html_1 . 'omg_smile' . $html_2 . 'Sorprendido' . $html_3,
				$html_1 . 'cry_smile' . $html_2 . 'Llor&oacute;n' . $html_3,
			),
			$texto
		);
		
		return $texto;
	}
	
	public static function resume($text, $numChars)
	{
		$textReturn = substr($text, 0, $numChars);
		if(strlen($textReturn) == $numChars)
		{
			$textReturn .= '...';
		}
		
		$textReturn = self::textoPlano($textReturn);
		
		return $textReturn;
	}
	
	public static function bbcode($str)
	{
		$str = self::textoPlano($str);
		$str = self::toEmoticons($str);

		// Do simple BBCode's 
		//$str = preg_replace ('/\[b\]((.*)|\n|\r|\n\r|\r\n)?\[\/b\]/i', '<strong>$1</strong>', $str);
		//$str = preg_replace ('/\[i\](.*?)\[\/i\]/i', '<em>$1</em>', $str);
		//$str = preg_replace ('/\[u\](.*)?\[\/u\]/i', '<u>$1</u>', $str);
		//$str = preg_replace ('/\[url\](.*?)\[\/url\]/i', '<a href="$1" target="_blank">$1</a>', $str);
		//$str = preg_replace ('/\[url\=(.*?)\](.*?)\[\/url\]/i', '<a href="$1" target="_blank">$2</a>', $str);
		//$str = preg_replace ('/\[email\](.*?)\[\/email\]/i', '<a href="mailto:$1" title="">$1</a>', $str);
		//$str = preg_replace ('/\[align\=(left|center|right|justify)\](.*?)\[\/align\]/i', '<div style="text-align: $1;">$2</div>', $str);
		//$str = preg_replace ('/\[font\=(.*?)\](.*?)\[\/font\]/i', '<span style="font-family: $1;">$2</span>', $str);
		//$str = preg_replace ('/\[size\=([0-9]+)\](.*?)\[\/size\]/i', '<span style="font-size: $1px;">$2</span>', $str);
		
		//$str = preg_replace ('/\[color\=(#[A-Za-z0-9]{3,6})\](.*)\[\/color\]/msi', '<span style="color: $1;">$2</span>', $str);
		$str = preg_replace ('/\[color\=(#[A-Za-z0-9]{3,6})\]/i', '<span style="color: $1;">', $str);
		$str = preg_replace ('/\[\/color\]/i', '</span>', $str);
		
		//$str = preg_replace ('/\[enrespuesta\]/i', '<div class="correo_en_respuesta">', $str);
		//$str = preg_replace ('/\[\/enrespuesta\]/i', '</div>', $str);
		
		$str = preg_replace ('/\[list\=1\](.*?)\[\/list\]/msi', '<ol>$1</ol>', $str);
		$str = preg_replace ('/\[list\](.*)\[\/list\]/msi', '<ul>$1</ul>', $str);
		//$str = preg_replace ('/\[list\]/i', '<ul>', $str);
		
		//$str = preg_replace ('/\[\/list\]/i', '<ul>', $str);
		
		$str = preg_replace ('/(\n|\r|\n\r|\r\n)?\[\*](.*)(\n|\r|\n\r|\r\n)?/i', '<li>$2</li>', $str);
	
		$str = nl2br($str);
		
		$bb_code = array(
			//Letra negrita
			'/\[b\]/' => '<b>',  //La parte de [b] es en BBcode
			'/\[\/b\]/' => '</b>', //La parte de <strong> es lo que PHP va a remplazar y as� en todos los casos
			
			//Letra cursiva
			'/\[i\]/' => '<em>',
			'/\[\/i\]/' => '</em>',
			
			//Letra subrayada
			'/\[u\]/' => '<u>',
			'/\[\/u\]/' => '</u>',
			
			//Imagenes
			//'/\[img src=(.*?)\]/' => '<img src="',
			//'/\[\/img\]/' => '" />' ,
			
			//Emails
			'/\[email\](.*?)\[\/email\]/' => '<a href="mailto:$1" target="_blank">$1</a>',
			
			//Links
			'/\[url=(.*?)\]/' => '<a href="$1" target="_blank">',
			'/\[\/url\]/' => '</a>'
		);
		
		$search = array_keys( $bb_code );
		$str = preg_replace( $search, $bb_code, $str );
		//$str = str_replace( $search, $bb_code, $str );

		//$str = self::toEmoticons($str);
		
		return $str;
	}

	public static function getLetraTest($numero)
	{
		$letrasAbecedario = array(
			'1' => 'A',
			'2' => 'B',
			'3' => 'C',
			'4' => 'D',
			'5' => 'E',
			'6' => 'F',
			'7' => 'G',
			'8' => 'H',
			'9' => 'I',
			'10' => 'J',
			'11' => 'K',
			'12' => 'L',
			'13' => 'M',
			'14' => 'N',		
			'15' => 'Ñ',
			'16' => 'O',
			'17' => 'P',
			'18' => 'Q',
			'19' => 'R',
			'20' => 'S',
			'21' => 'T',
			'22' => 'U',
			'23' => 'V',
			'24' => 'W',
			'25' => 'X',
			'26' => 'Y',
			'27' => 'Z',
		);

		return $letrasAbecedario[$numero];
	}	
}