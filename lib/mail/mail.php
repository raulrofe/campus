<?php
class Mail
{
	private static $_instancia;
	private $_objPhpMailer;
	private $_connectPop;
	
	private function __construct()
	{
		$this->_objPhpMailer = new phpmailer();
		$this->_objPhpMailer->IsSMTP();
		$this->_objPhpMailer->CharSet = 'utf-8';
		$this->_objPhpMailer->Host = SMTP_HOST;
		$this->_objPhpMailer->Port = SMTP_PORT;
		$this->_objPhpMailer->SMTPAuth = true;
		$this->_objPhpMailer->Username = SMTP_USERNAME;
		$this->_objPhpMailer->Password = SMTP_PASSWORD;
		$this->_objPhpMailer->Helo = SMTP_HELO;
		$this->_objPhpMailer->AltBody = "Para ver este mensaje necesita un visor de correos compatible con html";
		$this->_objPhpMailer->IsHTML(true);
	}
	
	public function enviar($emailFrom, $emailDestino, $nombreFrom, $nombreDestino, $asunto, $mensaje)
	{
		$this->_objPhpMailer->ClearAddresses();
		
		$this->_objPhpMailer->Subject = $asunto;
		
		$this->_objPhpMailer->From = $emailFrom;
		$this->_objPhpMailer->FromName = $nombreFrom;
		
		$this->_objPhpMailer->AddAddress($emailDestino, $nombreDestino);
		
		$this->_objPhpMailer->MsgHTML($mensaje);
		$isSend = $this->_objPhpMailer->Send();
		
		return $isSend;
	}
	
	public function enviarSecretaria($emailFrom, $nombreFrom, $asunto, $mensaje)
	{
		$plantilla = file_get_contents('plantillas/email_secretaria.html');
		$mensaje = str_replace('{email_contenido}', nl2br($mensaje), $plantilla);
		
		$envio = $this->enviar($emailFrom, CONFIG_EMAIL_SECRETARIA_EMAIL, $nombreFrom, CONFIG_EMAIL_SECRETARIA_NAME, $asunto, $mensaje);
		
		return $envio;
	}
	
	public function enviarNotificacion($emailFrom, $nombreFrom, $emailTo, $nameTo, $remitente, $mensaje, $asunto)
	{
		$contenido = file_get_contents('plantillas/email_notificacion.html');
		$contenido = str_replace('{remitente}', $remitente, $contenido);
		$contenido = str_replace('{asunto}', $asunto, $contenido);
		$contenido = str_replace('{mensaje}', nl2br($mensaje), $contenido);
		$contenido = str_replace('{fecha}', date('d-m-Y H:i'), $contenido);
		
		$envio = $this->enviar(
			$emailFrom, $emailTo, $nombreFrom, $nameTo,
			'Nuevo correo en Campus Aula Interactiva',
			$contenido
		);
		
		return $envio;
	}
	
	public function addCC($address, $name)
	{
		$this->_objPhpMailer->AddCC($address, $name);
	}
	
	public function addAttachment($path, $name = '', $encoding = 'base64', $type = 'application/octet-stream')
	{
		$this->_objPhpMailer->AddAttachment($path, $name, $encoding, $type);
	}
	
	public function clearAttachments()
	{
		$this->_objPhpMailer->ClearAttachments();
	}
	
	public function connectPop3($host, $port, $user, $pass, $folder="INBOX", $ssl=false)
	{
	    $ssl=($ssl==false)?"/novalidate-cert":"";
	    $this->_connectPop = (imap_open("{"."$host:$port/pop3$ssl"."}$folder",$user,$pass));
	    
	    return $this->_connectPop;
	}
	
	public function getCurrentBox()
	{
		$mails = imap_sort($this->_connectPop, SORTDATE, 0, null);
		
		return $mails;
	}
	
	public function getHeaderMail($idMail)
	{
		$headerInfo = imap_headerinfo($this->_connectPop, $idMail);
		
		return $headerInfo;
	}
	
	public function closePop3()
	{
		imap_close($this->_connectPop);
	}
	
	public static function obtenerInstancia()
	{
		if(!isset(self::$_instancia))
		{
			self::$_instancia = new self();
		}
		
		return self::$_instancia;
	}
}