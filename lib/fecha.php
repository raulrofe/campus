<?php

class Fecha {

	public static function obtenerFechaFormateada($fecha, $mostrarHora = true) {
		$texto = date('d/m/Y', strtotime($fecha));
		if ($mostrarHora) {
			$texto .= ' <span data-translate-html="general.alas">a las</span> ' . date('H:i', strtotime($fecha));
		}

		return $texto;
	}

	public static function obtenerFechaFormateadaSegundos($fecha, $mostrarHora = true) {
		$texto = date('d/m/Y', strtotime($fecha));
		if ($mostrarHora) {
			$texto .= ' <span data-translate-html="general.alas">a las</span> ' . date('H:i:s', strtotime($fecha));
		}

		return $texto;
	}

	public static function invertir_fecha($fecha, $separador = '-', $separador2 = '-') {
		$f = explode($separador, $fecha);

		if (count($f) == 3) {
			$fecha = $f[2] . $separador2 . $f[1] . $separador2 . $f[0];
		} else {
			$fecha = null;
		}

		return $fecha;
	}

	public static function restarHoras($horaini, $horafin) {
		$horai = substr($horaini, 0, 2);
		$mini = substr($horaini, 3, 2);
		//$segi=substr($horaini,6,2);

		$horaf = substr($horafin, 0, 2);
		$minf = substr($horafin, 3, 2);
		//$segf=substr($horafin,6,2);

		$ini = ((($horai * 60) * 60) + ($mini * 60)/* +$segi */);
		$fin = ((($horaf * 60) * 60) + ($minf * 60)/* +$segf */);

		$dif = $fin - $ini;

		$difh = floor($dif / 3600);
		$difm = floor(($dif - ($difh * 3600)) / 60);
		//$difs=$dif-($difm*60)-($difh*3600);
		return date("H:i", mktime($difh, $difm));
		//return date("H-i-s",mktime($difh,$difm,$difs));
	}

	/**
	 * Devolvemos la fechaa con formato para base de datos
	 */
	public static function fechaActual($minutos = TRUE) {
		if ($minutos) {
			$fecha = date("Y-m-d H:i:s");
		} else {
			$fecha = date("Y-m-d");
		}
		return $fecha;
	}

	/**
	 * Devolvemos el día de la semana según los numeros
	 */
	public static function diaSemana($diasNumero) {
		$dias = array();
		$diasNumero = explode(',', $diasNumero);

		$cantidadDias = count($diasNumero) - 1;

		for ($i = 0; $i <= $cantidadDias; $i++) {
			switch ($diasNumero[$i]) {
				case 1:
					$dias[] = '</span data-translate-html="dias.lunes">Lunes</span> ';
					break;
				case 2:
					$dias[] = '</span data-translate-html="dias.martes">Martes</span> ';
					break;
				case 3:
					$dias[] = '</span data-translate-html="dias.miercoles">Mi&eacute;rcoles</span> ';
					break;
				case 4:
					$dias[] = '</span data-translate-html="dias.jueves">Jueves</span> ';
					break;
				case 5:
					$dias[] = '</span data-translate-html="dias.viernes">Viernes</span> ';
					break;
				case 6:
					$dias[] = '</span data-translate-html="dias.sabado">S&aacute;bado</span> ';
					break;
				case 7:
					$dias[] = '</span data-translate-html="dias.domingo">domingo</span> ';
					break;
			}
		}
		return $dias;
	}

	/**
	 * Devolvemos el día de la semana según el numero
	 */
	public static function dias($diasNumero) {
		$dias = '';
		switch ($diasNumero) {
			case 1:
				$dias = '<span data-translate-html="dias.lunes">Lunes </span>';
				break;
			case 2:
				$dias = '<span data-translate-html="dias.martes">Martes </span>';
				break;
			case 3:
				$dias = '<span data-translate-html="dias.miercoles">Mi&eacute;rcoles </span>';
				break;
			case 4:
				$dias = '<span data-translate-html="dias.jueves">Jueves </span>';
				break;
			case 5:
				$dias = '<span data-translate-html="dias.viernes">Viernes </span>';
				break;
			case 6:
				$dias = '<span data-translate-html="dias.sabado">S&aacute;bado </span>';
				break;
			case 0:
				$dias = '<span data-translate-html="dias.domingo">domingo </span>';
				break;
		}

		return $dias;
	}

	/**
	 * Devolvemos el mes según los numeros
	 */
	public static function mes($mesNumero) {
		$mes = '';

		switch ($mesNumero) {
			case 1:
				$mes = '<span data-translate-html="meses.enero">Enero </span>';
				break;
			case 2:
				$mes = '<span data-translate-html="meses.febrero">Febrero </span>';
				break;
			case 3:
				$mes = '<span data-translate-html="meses.marzo">Marzo </span>';
				break;
			case 4:
				$mes = '<span data-translate-html="meses.abril">Abril </span>';
				break;
			case 5:
				$mes = '<span data-translate-html="meses.mayo">Mayo </span>';
				break;
			case 6:
				$mes = '<span data-translate-html="meses.junio">Junio </span>';
				break;
			case 7:
				$mes = '<span data-translate-html="meses.julio">julio </span>';
				break;
			case 8:
				$mes = '<span data-translate-html="meses.agosto">Agosto </span>';
				break;
			case 9:
				$mes = '<span data-translate-html="meses.septiembre">Septiembre </span>';
				break;
			case 10:
				$mes = '<span data-translate-html="meses.octubre">Octubre </span>';
				break;
			case 11:
				$mes = '<span data-translate-html="meses.noviembre">Noviembre </span>';
				break;
			case 12:
				$mes = '<span data-translate-html="meses.diciembre">Diciembre </span>';
				break;
		}

		return $mes;
	}

	public static function formatearSegundosHoras($segundos) {
		$timezone = date_default_timezone_get();
		date_default_timezone_set('UTC');
		$horas = date('H:i:s', $segundos);
		date_default_timezone_set($timezone);

		return $horas;
	}

	function segundosHoras($segundos) {
		$horas = floor($segundos / 3600);
		$minutes = (($segundos / 60) % 60);
		$seconds = ($segundos % 60);

		$horas = str_pad($horas, 2, "0", STR_PAD_LEFT);
		$minutes = str_pad($minutes, 2, "0", STR_PAD_LEFT);
		$seconds = str_pad($seconds, 2, "0", STR_PAD_LEFT);

		return $horas . ':' . $minutes . ':' . $seconds;
	}

	public static function orderByDate($arrayFecha) {

		//Ordena el array por fecha
		function ordenarPorFecha($a, $b) {
			return strtotime($a["fecha"]) - strtotime($b["fecha"]);
		}

		usort($arrayFecha, "ordenarPorFecha");

		return $arrayFecha;
	}

	public static function orderByDate2($arrayFecha) {

		//Ordena el array por fecha
		function ordenarPorFecha2($a, $b, $c) {
			return strtotime($a["fecha"]) - strtotime($b["fecha"]) - strtotime($c["fecha"]);
		}

		usort($arrayFecha, "ordenarPorFecha2");

		return $arrayFecha;
	}

	public static function quitarSegundos($hora) {
		$tiempo = explode(':', $hora);
		if (is_array($tiempo) && count($tiempo) == 3) {
			$hora = $tiempo[0] . ':' . $tiempo[1];
		}

		return $hora;
	}

	public static function calcula_tiempo($total_seconds) {
		//$total_seconds = strtotime($end_time) - strtotime($start_time); 
		$horas = floor($total_seconds / 3600);
		$minutes = ( ( $total_seconds / 60 ) % 60 );
		$seconds = ( $total_seconds % 60 );

		$time['horas'] = str_pad($horas, 2, "0", STR_PAD_LEFT);
		$time['minutes'] = str_pad($minutes, 2, "0", STR_PAD_LEFT);
		$time['seconds'] = str_pad($seconds, 2, "0", STR_PAD_LEFT);

		$time = implode(':', $time);

		return $time;
	}

	public static function hace_tiempo($datetime) { {
			$estimate_time = time() - strtotime($datetime);

			if ($estimate_time < 60) {
				return '<span data-translate-html="conectados.menosminuto">hace menos de un minuto</span>';
			}

			$condition = array(
				12 * 30 * 24 * 60 * 60 => '<span data-translate-html="conectados.ano">año</span>',
				30 * 24 * 60 * 60 => '<span data-translate-html="conectados.mes">mes</span>',
				24 * 60 * 60 => '<span data-translate-html="conectados.dia">día</span>',
				60 * 60 => '<span data-translate-html="conectados.hora">hora</span>',
				60 => '<span data-translate-html="conectados.minuto">minuto</span>',
//				1 => 'segundo'
			);

			foreach ($condition as $secs => $str) {
				$d = $estimate_time / $secs;

				if ($d >= 1) {
					$r = round($d);
					return '<span data-translate-html="conectados.hace">hace</span> ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' <span data-translate-html="conectados.hace2"></span>';
				}
			}
		}
	}

}
