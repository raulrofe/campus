<?php
class LibServicios
{
	private $_objModelo;
	
	public function __construct()
	{
		$this->_objModelo = new LibModeloServicios();
	}
	
	public function comprobarAcceso($idServicio, $lanzar404 = false)
	{
		$access = false;
		
		$idCurso = Usuario::getIdCurso();
		$row = $this->_objModelo->obtenerUnServicio($idServicio, $idCurso);
		
		if($row->num_rows == 1)
		{
			$access = true;
		}
	
			
		if($lanzar404 && !$access)
		{
			Url::lanzar404();
		}
		
		return $access;
	}
}