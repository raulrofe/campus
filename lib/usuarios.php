<?php
class Usuarios extends modeloExtend{
	
	private $idperfil;
	private $user;
	
	//Constructor
	public function usuarios (){parent::__construct();}
	
	public static function comprobarPermisosLogueo($idUsuario, $perfil = null)
	{
		$resultado = false;
		
		// si se usa id de usuario con las t_1212 por ejemplo
		if(!isset($perfil))
		{
			// comprueba si es un perfil alumno o no
			if(preg_match('#^t_([0-9]+)$#', $idUsuario))
			{
				$perfilAlumno = false;
			}
			else
			{
				$perfilAlumno = true;
			}
			
			// obtiene el ID limpiado si no es un perfil de alumno
			if(!$perfilAlumno)
			{
				$idUsuario = explode('_', $idUsuario);
				$idUsuario = $idUsuario[1];
			}
		}
		// para cuando se pasa tambien por parametro el tipo de perfil
		else
		{
			// comprueba si es un perfil alumno o no
			if($perfil != 'alumno')
			{
				$perfilAlumno = false;
			}
			else
			{
				$perfilAlumno = true;
			}
		}
		
		// comprueba  si tiene permisos para la accion a realizar
		if($_SESSION['idusuario'] == $idUsuario)
		{
			if($perfilAlumno && $_SESSION['perfil'] == 'alumno')
			{
				$resultado = true;
			}
			else if(!$perfilAlumno && $_SESSION['perfil'] != 'alumno')
			{
				$resultado = true;
			}
		}
		
		return $resultado;
	}
	// Establecemos el id para un usuario usuario
	public function set_usuario($idusuario){$this->user = $idusuario;}
		
	//Buscamos todos los usuarios
	public function listado_usuarios(){
		$sql = "SELECT * from rrhh";
		$resultado = mysqli_query($this->con,$sql);
		return $resultado;
	}
	
	public function obtenerRoles($idtutor, $idcurso)
	{
		$sql = 'SELECT * FROM tutores_servicios WHERE idrrhh = ' . $idtutor . ' AND idcurso = ' . $idcurso;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos todo los usuario de un perfil determinado
	/*public function listado_uauarios_perfil(){
		$sql = "SELECT * from rrhh where idperfil = ".$this->idperfil;
		$resultado = mysqli_query($this->con,$sql);
		return $resultado;
		
	}*/
	
	public function obtenerUsuarioMatricula($perfil)
	{
		if($perfil != 'alumno')
		{
			$sql = "SELECT * FROM rrhh AS rh LEFT JOIN staff AS st ON rh.idrrhh = st.idrrhh WHERE rh.idrrhh = " . $_SESSION['idusuario'];	
		}
		else
		{
			$sql = "SELECT * FROM alumnos AS al LEFT JOIN matricula AS m ON al.idalumnos = m.idalumnos WHERE al.idalumnos  = " . $_SESSION['idusuario'];		
		}
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	//Obtenemos datos alumno y matricula segun id del alumno y perfil pasados como parametro
	public function obtenerAlumnoMatriculadoPorPerfil($idCurso, $idAlumno)
	{
		$sql = "SELECT  al.idalumnos, al.nombre, al.apellidos, al.dni, al.email, al.telefono, m.idmatricula, m.fecha, m.fecha_conexion_actual, m.fecha_conexion, c.nombre_centro, c.razon_social, c.cif 
				FROM alumnos AS al 
				LEFT JOIN matricula AS m ON al.idalumnos = m.idalumnos 
				LEFT JOIN centros AS c ON c.idcentros = c.idcentros 
				WHERE al.idalumnos  = " . $idAlumno . " AND m.idcurso = " . $idCurso;		
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	//Obtenemos datos alumno y matricula segun id del alumno y perfil pasados como parametro
	public function obtenerMatriculaInforme($idMatricula)
	{
		$sql = "SELECT  al.idalumnos, al.nombre, al.apellidos, al.dni, al.email, al.telefono, m.idmatricula, m.fecha, m.fecha_conexion_actual, m.fecha_conexion, c.nombre_centro, c.razon_social, c.cif 
				FROM matricula AS m
				LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos 
				LEFT JOIN centros AS c ON c.idcentros = al.idcentros 
				WHERE m.idmatricula   = " . $idMatricula;	
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	//Buscamos el usuario elejido como parametro le pasmos el id
	public function buscar_usuario(){
		if($_SESSION['perfil'] != 'alumno')
		{
			$sql = "SELECT * from rrhh where idrrhh = ".$_SESSION['idusuario'];	
		}
		else
		{
			$sql = "SELECT * from alumnos where idalumnos = ".$_SESSION['idusuario'];		
		}
		
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos el usuario elejido como parametro le pasmos el id
	public function buscar_usuario_auto($idUsuario)
	{
		if(Usuario::checkAlumno($idUsuario))
		{
			$sql = "SELECT a.idalumnos AS idusuario, CONCAT(a.nombre, ' ', a.apellidos) AS nombrec from alumnos AS a where idalumnos = " . $idUsuario;
		}
		else
		{
			// quitamos la t_ al ID del usuario
			preg_match_all('#^t_([0-9]+)$#', $idUsuario, $idUsuarioArray);
			$idUsuario = $idUsuarioArray[1][0];
			
			$sql = "SELECT rh.idrrhh AS idusuario, rh.nombrec from rrhh AS rh where idrrhh = " . $idUsuario;
		}
		
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	public function actualizarPass($pass)
	{
		$pass = md5($pass);
		
		if($_SESSION['perfil'] == 'alumno')
		{
			$sql = 'UPDATE alumnos SET pass = "' . $pass . '" WHERE idalumnos = ' . $_SESSION['idusuario'];
		}
		else
		{
			$sql = 'UPDATE rrhh SET pass = "' . $pass . '" WHERE idrrhh = ' . $_SESSION['idusuario'];
		}
		
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}
	
	//Buscamos todos los usuario que pertenecen a un curso que no esten borrado
	public function buscar_usuarios_curso($idcurso){
		$sql = "SELECT RH.idrrhh, RH.nombrec, S.status, P.perfil FROM rrhh RH 
		LEFT JOIN staff S ON S.idrrhh = RH.idrrhh
		LEFT JOIN perfil P ON P.idperfil = RH.idperfil
		where S.idcurso = " . $idcurso . " 
		and RH.borrado = 0
		GROUP BY RH.idrrhh
		ORDER BY RH.idperfil DESC";

		$resultado = $this->consultaSql($sql);

		return $resultado;
	}

	//Buscamos todos los usuario que pertenecen a un curso
	public function buscar_usuario_curso($idusuario, $idcurso)
	{
		if($_SESSION['perfil'] == 'alumno')
		{
			$sql = "SELECT * from matricula M
			where M.idcurso = ".$idcurso." 
			and m.idalumnos = $idusuario AND borrado = 0";
		}
		else
		{
			$sql = "SELECT * from staff S
			where S.idcurso = ".$idcurso." 
			and S.idrrhh = $idusuario";
		}
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		return $resultado;
	}

	//Select usuario le pasamos como parametro el id del perfil
	public function select_usuarios($resultado){
		echo "<select name='idusuario'>";
		while($f = mysqli_fetch_assoc($resultado)){
			if($this->user == $f['idrrhh']) echo "<option value='".$f['idrrhh']."' selected>".$f['nombrec']."</option>";
			else echo "<option value='".$f['idrrhh']."'>".$f['nombrec']."</option>";
		}
		echo "</select>";
	}

	//Select usuario de un perfil concreto
	public function select_usuarios_perfil($idperfil){
		$sql = "SELECT * from rrhh where idperfil = ".$idperfil;
		$resultado = $this->consultaSql($sql);
		echo "<select name='idrrhh[]'>";
		while($f = mysqli_fetch_assoc($resultado)){
			if($this->user == $f['idrrhh']) echo "<option value='".$f['idrrhh']."' selected>".$f['nombrec']."</option>";
			else echo "<option value='".$f['idrrhh']."'>".$f['nombrec']."</option>";
		}
		echo "</select>";
	}
	
	//insertamos un usuario se le pasan como parametros los datos del usuario a insertar
	public function insertar_usuario($nombrec,$pass,$alias,$direccion,$localidad,$provincia,$pais,$cp,$telefono,$email){
		$pass = md5($pass);
		$sql="SELECT * from rrhh where nombrec = '".$nombrec."'";
		//echo $sql;	
		$resultado = mysqli_query($this->con,$sql);
		if(mysqli_num_rows($resultado) == 0){
			$sql = "INSERT into rrhh (nombrec,pass,alias,direccion,localidad,provincia,pais,cp,telefono,email,idperfil)
			VALUES ('$nombrec','$pass','$alias','$direccion','$localidad','$provincia','$pais','$cp','$telefono','$email','$this->idperfil')";
			//echo $sql;
			mysqli_query($this->con,$sql) or die ("Error al insertar el usuario");
			return $valor = 'true';
		}
		else return $valor = 'error';		
	}
	
	//Actualizamos el usuario elejido parametro = id del usuario
	public function actualizar_usuario($anombrec,$aalias,$adireccion,$alocalidad,$aprovincia,$apais,$acp,$atelefono,$aemail){
		$sql = "UPDATE rrhh SET
		nombrec = '$anombrec' , alias = '$aalias' , direccion = '$adireccion' , 
		localidad = '$alocalidad' , provincia = '$aprovincia' , pais = '$apais' , cp = '$acp' , 
		telefono = '$atelefono' , email = '$aemail'
		where idrrhh = ".$this->user;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	public function actualizar_foto_usuario($foto){
		$sql = "UPDATE rrhh SET
		foto = '$foto'
		where idrrhh = ".$this->user;
		//echo $sql;
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
	
	// Eliminamos el usuario el parametro que le pasamos es el id del usuario
	public function eliminar_usuario(){
		$sql = "DELETE from rrhh where idrrhh = ".$this->user;
		echo $sql;
		mysqli_query($this->con,$sql) or die ("Error al eliminar el usuario");
	}
	
	//Obtenemos el tiempo de conexion de un alumno para el curso segun su id de matricula
	public function obtenerTiempoConexion($idMatricula)
	{
		$sql = "SELECT fecha_entrada, fecha_salida" .
		" FROM logs_acceso" .
		" WHERE idmatricula = " . $idMatricula;

		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

public function obtenerTiemposConexionPorAlumno($idMatricula)
	{
		$sql = 'SELECT al.idalumnos, la.fecha_entrada, la.fecha_salida FROM logs_acceso AS la' .
		' LEFT JOIN matricula AS m ON m.idmatricula = la.idmatricula' .
		' LEFT JOIN alumnos AS al ON al.idalumnos = m.idalumnos' .
		' WHERE fecha_salida IS NOT NULL AND m.idmatricula = ' . $idMatricula .
		' GROUP BY la.idaccesos ORDER BY la.idmatricula, la.fecha_entrada ASC, la.fecha_salida DESC';
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}

	// Obtenemos el primer acceso
	public function getFirstAccessParticipante($idMatricula)
	{
		$sql = "SELECT la.fecha_entrada FROM logs_acceso AS la" .
		" WHERE la.idmatricula = " . $idMatricula .
		" ORDER BY la.fecha_entrada ASC " . 
		" LIMIT 1";
		
		$resultado = $this->consultaSql($sql);
		
		return $resultado;
	}
}