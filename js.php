<?php
define('I_EXEC', true);

header("Content-type: text/javascript");

require_once 'config.php';

$js = array(
	//'jquery',
	//'jquery_ui',
	//'ckeditor',
	//'ckeditor_ini',
	//'prefixfree',
	'validate',
	'alert',
	'msgPopup',
	'hashchange',
	'imprimir',
	'calendario',
	'url',
	'insertAt',
	'popupModal',
	'modal_into',
	'match',
	'tipsy',
	'ini',
	'jquery.MultiFile',
	'thumbTipHover',
	'countdown/countdown',
	'jquery-blink',
	'cookie',
	'fancybox',
	'flowtype',
	'usuarios-conectados',
	'conversaciones'
);

if(isset($_GET['t']) && $_GET['t'] == 'admin')
{
	$path = PATH_ROOT_ADMIN;
}
else
{
	$path = PATH_ROOT;
}

$code = null;
if(isset($_GET['m'], $_GET['c']) && preg_match('/^([0-9A-Za-z_\-]+)$/', $_GET['m']) && preg_match('/^([0-9A-Za-z_\-]+)$/', $_GET['c']) )
{
	$file = $path . 'modulos/' . $_GET['m'] . '/js/' . $_GET['c'] . '.js';
	if(file_exists($file))
	{
		$code = file_get_contents($file);
	}
}
else if(isset($_GET['t']) && $_GET['t'] != 'admin')
{
	switch($_GET['t'])
	{
		case 'ga':
			if(defined('GA_ID'))
			{
				$gaId = constant('GA_ID');
				if(!empty($gaId))
				{
					$code = "var _gaq = _gaq || [];" .
						"_gaq.push(['_setAccount', 'UA-" . GA_ID . "']);" .
						"_gaq.push(['_trackPageview']);" .
						"(function() {" .
						"var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" .
						"ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" .
						"var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" .
						"})();";
				}
 			}
			break;
		case 'ajax':
			$code = file_get_contents($path . 'js/ajax.js');
			break;
		case 'notifi_usuarios':
			$code = file_get_contents($path . 'js/notifi_usuarios.js');
			break;
		case 'ie7':
			$code = file_get_contents($path . 'js/ie7/menu_principal.js');
			break;
		default:
			break;
	}
}
else
{
	foreach($js as $item)
	{
		if(file_exists($path . 'js/' . $item . '.js'))
		{
			$code .= file_get_contents($path . 'js/' . $item . '.js');
		}
	}
}
echo $code;