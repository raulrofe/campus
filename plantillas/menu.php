<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head profile="http://gmpg.org/xfn/11">
		<title>AulaInteractiva</title>

		<base href="<?php echo URL_BASE?>" />

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="Description" content="" />
		<meta name="Keywords" content="" />
		<meta name="robots" content="index,nofollow,noimageindex" />

		<link rel="shortcut icon" type="image/x-icon" href="archivos/config_web/favicon.ico" />

		<link href="css_default.css" rel="stylesheet" type="text/css" />

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>

		<script type="text/javascript" src="js_default.js" defer="defer"></script>
		<script type="text/javascript" src="js_ajax.js" defer="defer"></script>


		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<!--[if IE 7]>
			<link type="text/css" rel="stylesheet" href="css/ie7.css" />
		<![endif]-->
		
		
		<!-- Translate -->
		<script type="text/javascript" src="js/translate.js"></script>
	</head>
	<body lang="es">
		<div id="general">
			<?php
				if(!Usuario::is_login())
				{
					require_once 'plantillas/ajax.php';
					echo '<span id="usuario_no_logueado"></span>';
				}
			?>
		</div>
		<div id="popupsButtons"></div>
		<div class="clear"></div>
		
		<?php $stringAlerta = Alerta::translateString('opcionesmodal','cargando'); ?>
		
		<div id="msgCargaAjax" class="hide" data-translate-html="opcionesmodal.cargando">
			<?php echo $stringAlerta; ?>
		</div>

		<script type="text/javascript" src="js_notifi_usuarios.js"></script>

		<!-- MENU CONTEXTUAL PARA LA CREACION DE CARPETAS EN EL CORREO -->
			<div id="menuContextual" class="menuContextual hide">
				<ul>
					<li id="menuContextual_edit" data-translate-html="correo.editar_carpeta">Editar carpeta</li>
					<li id="menuContextual_delete" data-translate-html="correo.eliminar_carpeta">Eliminar carpeta</li>
				</ul>
			</div>
			<div id="menuContextual2" class="menuContextual hide">
				<ul>
					<li id="menuContextual_new" data-translate-html="correo.crear_carpeta">
						Crear carpeta
					</li>
				</ul>
			</div>
		<!-- FIN MENU CONTEXTUAL PAR LA CREACION DE CARPETAS CORREO -->

		<script type="text/javascript" src="js_ga.js"></script>

		<script type="text/javascript">
			tinymce.init({
				plugins: ["link"],
				selector: "textarea",
				menubar: false,
				statusbar: false,
				toolbar: "undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link",
			});
		</script>
	</body>
</html>