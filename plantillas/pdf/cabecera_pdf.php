<style type="text/Css">

.bold{font-weight: bold;}
.fullWidth{width: 720px;}
.t_center{text-align:center;}
.t_left{text-align:left;}
.t_right{text-align:right;}
.bright{border-right: 2px solid #000;}
.bbottom{border-bottom: 2px solid #000;}
.bleft{border-left: 2px solid #000;}
.btop{border-top: 2px solid #000;}
.cellpadding{padding:5px;}

.logoAulaSmart{
	font-size: 8pt;
	width:370px;
	text-align:left;
}

.logoAulaSmart img{
	height: 40px;
	margin-left: 35px;
}

.logoTripartita{
	width:370px;
	text-align:right;
}

.logoTripartita img{
	height: 100px;
}

.titulo{
	font-size: 14px;
	text-align:center;
	width: 740px;
}

.apartados{
	border:2px solid #000;
	width: 780px;
	font-size: 10px;
	margin-top: 10px;
	vertical-align: top;
}

.subtable{
	border-right:2px solid  #000;
	border-left:2px solid  #000;
	border-bottom:2px solid  #000;
	font-size: 10px;
}

</style>

<page>

	<table>
		<tr>
			<td class="logoAulaSmart">
				<img src='<?php echo PATH_ROOT ?>/imagenes/pdf/1_logo.png' alt=''/><br/>
				AULA_<span class="bold">SMART</span> Editorial
			</td>
			<td class="logoTripartita">
				<img src='<?php echo PATH_ROOT ?>/imagenes/pdf/logo_tripartita.jpg' alt='' style='margin-left:-25px;'/>		
			</td>
		</tr>
	</table>
	
	<table>
		<tr>
			<td class="titulo bold">INFORME DE SEGUIMIENTO DE LOS ALUMNOS/AS EN CURSOS A DISTANCIA</td>
		</tr>
	</table>
	
	<table class="apartados" cellspacing="0">
		<tr>
			<td class="bright bbottom cellpadding">1</td>
			<td colspan="3" class="bold fullWidth t_center bbottom cellpadding">DATOS DE LA ENTIDAD ORGANIZADORA</td>
		</tr>
		<tr>
			<td></td>
			<td class="cellpadding">ENTIDAD ORGANIZADORA<br/><?php echo $entidadOrganizadora->nombre_entidad; ?></td>
			<td class="bright bleft cellpadding">C.I.F.<br/><?php echo $entidadOrganizadora->cif; ?></td>
			<td class="cellpadding">C&Oacute;DIGO ID DE PERFIL</td>
		</tr>
	</table>
	
	<table class="apartados" cellspacing="0">
		<tr>
			<td class="bright bbottom cellpadding">2</td>
			<td colspan="3" class="bold fullWidth t_center bbottom cellpadding">DATOS DE LA <?php utf8_decode('ACCIÓN')?> FORMATIVA</td>
		</tr>
		<tr>
			<td></td>
			<td class="cellpadding">DENOMINACI&Oacute;N DE LA ACCI&Oacute;N<br/><?php  echo utf8_decode($curso->titulo); ?></td>
			<td class="bright bleft cellpadding">Nº:<br/><?php  echo $curso->accion_formativa; ?></td>
			<td class="cellpadding">GRUPO:<br/><?php  echo $curso->codigoIG; ?></td>
		</tr>
	</table>
	<table class="subtable" cellspacing="0">
		<tr>
			<td class="bbottom cellpadding" style="width:195px;">FECHA INICIO<br/><?php  echo Fecha::invertir_fecha($curso->f_inicio, '-', '/'); ?></td>
			<td class="bbottom bright bleft cellpadding" style="width:195px;">DNI RESPONSABLE<br/>74888743F</td>
			<td class="bbottom cellpadding" style="width:300px;">RESPONSABLE DE FORMACI&Oacute;N<br/>Sandra Sicilia Salcedo</td>
		</tr>
		<tr>
			<td class="bbottom cellpadding" style="width:195px;">FECHA FIN<br/><?php  echo Fecha::invertir_fecha($curso->f_fin, '-', '/'); ?></td>
			<td class="bright bbottom bleft cellpadding" style="width:191px;">DNI FORMADOR<br/>74888743F</td>
			<td class="bbottom cellpadding" style="width:300px; ">FORMADOR/TUTOR<br/>Natividad Luque Campaña</td>
		</tr>
		<tr>
			<td class="bbottom cellpadding" style="width:195px;">Nº HORAS<br/><?php  echo $curso->n_horas; ?></td>
			<td class="bleft cellpadding" style="width:300px;" colspan="2">FIRMADO</td>
		</tr>
		<tr>
			<td class="cellpadding" style="width:195px;">HORARIO TUTOR&Iacute;AS<br/>Lunes a Jueves: 09:00 a 13:00</td>
			<td class="cellpadding bleft" style="width:150px;">FORMADOR/TUTOR</td>
			<td class="cellpadding" style="width:150px;">RESPONSABLE</td>
		</tr>
	</table>

</page>
