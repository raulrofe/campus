<span id="general_popup_url_base" style="display:none"><?php echo URL_BASE?></span>
<div class="popupModalModuleName hide"><?php echo $_GET['m']?></div>
<div class="popupModalUrlCurrent hide"><?php echo URL_CURRENT_URI?></div>

<?php //Alerta::leerMensajeInfo(); ?>

<script type="text/javascript">
		/* EVITA CACHE DEL NAVEGADOR AL DESHACER CIERRE DE PESTANA */
		var checkCacheTab_reload = true;

		try
		{
			var checkCacheTab_obj = document.getElementById('general');

			if(typeof(eval(checkCacheTab_obj)) != 'undefined')
			{
				if (eval(checkCacheTab_obj) != null)
				{
					checkCacheTab_reload = false;
				}
			}
		}
		catch(e)
		{

		}

		if(checkCacheTab_reload && window == window.top)
		{
			var url = window.location.href;
			var urlBase = document.getElementById("general_popup_url_base").innerHTML;

			var regex = new RegExp('^' + urlBase);
			url = url.replace(regex, '');
			window.location.href = urlBase + 'cursos/#/' + url;

			// hide elements
			var checkCacheTab_array = new Array();
			checkCacheTab_array = document.getElementsByTagName("*");
			for(var i = 0; i < checkCacheTab_array.length; i++)
			{
				var checkCacheTab_tag = document.getElementsByTagName("*").item(i);
				checkCacheTab_tag.style.display = 'none';
			}
		}
</script>

<div class="general_popup">
	<?php
		$ruta = mvc::obtenerRutaControlador();
		if(isset($ruta))
		{
			mvc::importar($ruta);

			//if(Usuario::is_login() /*&& Usuario::compareProfile('alumno')*/)
			//{
			//	echo '<script type="text/javascript">popupLogAcceso("' . $_GET['m'] . '");</script>';
			//}

			if(Usuario::is_login() && isset($_GET['m']) && $_GET['m'] != 'cursos')
			{
				$objMsgEmergente = new LibMensajesEmergentes();
				echo $objMsgEmergente->mostrarMensajes();
			}
		}
		else
		{
			echo '<script type="text/javascript">window.location.reload();</script>';
		}
	?>
</div>

<?php //echo MensajesEmergentes::mostrarMensajes()?>

<!-- <script type="text/javascript" src="js/menu.js"></script> -->