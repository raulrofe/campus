<?php
$ruta = mvc::obtenerRutaControlador();
if(isset($ruta))
{
	mvc::importar($ruta);
				
	if(Usuario::is_login() && Usuario::compareProfile('alumno'))
	{
		if(isset($_GET['v']))
		{
			$objLogAcceso = LogAcceso::obtenerInstancia();
			$objLogAcceso->insertarLog();
		}
	}
	
	if(Usuario::compareProfile('alumno') && is_numeric(LogAcceso::obtenerIdAcceso()) && isset($_GET['v']))
	{
		echo '<script type="text/javascript">
				$(window).unload(function()
				{
					$.ajax({
						url : \'index.php?idAcceso=' . LogAcceso::obtenerIdAcceso() . '\',
						async: false
					});
				});
				msgPopupClose();
			 </script>';
		
		echo '<span id="idLogAcceso_menu" class="hide">' . LogAcceso::obtenerIdAcceso() . '</span>';
	}
	
	if(Usuario::is_login() && isset($_GET['m']) && $_GET['m'] != 'cursos')
	{
		$objMsgEmergente = new LibMensajesEmergentes();
		echo $objMsgEmergente->mostrarMensajes();
	}
}
else
{
	Url::redirect('');
}

Alerta::leerMensajeInfo();