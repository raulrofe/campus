function alertMsg(key, text){
	var mensaje = '';
	
	// If exist two parameteres, set message string as default value
	if (text && text.length > 0) {
		translateString( key, text);
	}
	
	
	setTimeout(function(){
		
		if (text && text.length > 0) {
			// If exist two parameteres, set message string as default value
			mensaje = getCookie('palabraTemp');
		} else{
			// If only exist one parameters, key it's "mensaje" default
			mensaje = key;
		}
		
		if($('#mensajeInformacion').size() > 0) {
			$('#mensajeInformacion').remove();
		}

		var html = '<div id="mensajeInformacion" class="hide">' + mensaje + '</div>';
		$('body').append(html);

		var left = ($(window).width() - $('#mensajeInformacion').width()) / 2;
		$('#mensajeInformacion').css('left', left + 'px');

		$('#mensajeInformacion').show();

		$('#mensajeInformacion').bind('click', function(){
			alertMsgClose();
		});

		setTimeout('alertMsgClose()', 5000);
		
	},500);
}


function alertMsgClose(){
	$('#mensajeInformacion').unbind('click');
	$('#mensajeInformacion').slideUp(function(){
		$(this).remove();
	});
}


function alertConfirm(element, msg, url){
	if(confirm(msg))
	{
		/*var id = $(element).attr('id');
		if(id == undefined)
		{
			var newDate = new Date;
			id = 'alertConfimLink_' + newDate.getTime();
		}*/
		
		//url += '?url-referer='  + encodeURIComponent($(element).parents('.popupModal').attr('data-url'));
		//alert(url);
		if($(element).attr('href') != undefined)
		{
			LibPopupModal._ajaxLoad($(element).parents('.popupModal').attr('id'), url);
		}
		else
		{
			var objDate = new Date();
			var idAnchor = 'alertConfirmAuxButton_' + objDate.getTime();
			$(element).parent().append('<a class="hide" href="' + $(element).attr('data-url') + '" id="' + idAnchor + '">a</a>');

			setTimeout(function(){
				$('#' + idAnchor).click();
			}, 50);
		}
		
		//window.location.href = url;
		
		return false;
	}
}

// Sin uso, ya declarada en campus/js/modal_into.js
// 
//function alertConfirmAjax(idModalbox, msg, url) {
//	if(confirm(msg)) {
//		LibPopupModal._ajaxLoad($(idModalbox).parents('.popupModal').attr('id'), url);
//	}
//	return false;
//}
