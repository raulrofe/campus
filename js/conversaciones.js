var tiempoRecarga = 10; // Segundos
var intervaloInicioConversaciones;



$(document).ready(function () {
	// Para esperar que tras el login y la pantalla de "Mis cursos" llegues al HALL y actualizar el numero de conectados
	intervaloInicioConversaciones = setInterval(function () {
		if ($('.listado_cursos').length == 0) {
			// Borramos el intervalo pues solo queremos ejecutarlo una vez
			clearInterval(intervaloInicioConversaciones);

			// Cargamos el numero de mensajes total sin leer
			mensajesNoLeidosTotal();

			// Actualizar cada X segundos numero de mensajes sin leer total
			setInterval(function () {
				mensajesNoLeidosTotal();
			}, tiempoRecarga * 1000);
		}
	}, 1000);
});




// Al enviar un mensaje
$(document).on('submit', '#chat-input', function (event) {
	// Evitamos la propagacion del evento
	event.preventDefault();

	// Variables
	var form = $(this);
	var _data = {
		id: sessionStorage.getItem('openchat'),
		mensaje: form.find('input[type="text"]').val()
	};

	// Si hemos escrito algo para enviar
	if (_data.mensaje.length > 0) {
		// Enviamos el mensaje
		$.ajax({
			url: 'conversaciones/mensaje',
			type: 'POST',
			data: _data
		}).success(function (data) {
			if (data) {
				data = JSON.parse(data);
				// Añadimos el mensaje a la conversacion en pantalla
				$('#listado_conversacion #l_c').append(data.html);

				// Bajamos el scroll de la conversacion hasta abajo del todo
				$('#listado_conversacion').scrollTop($('#l_c').height());

				// Vaciamos el input de texto
				form.find('input[type="text"]').val('');

				// Actualizamos el listado de conversaciones para actualizar la fecha del ultimo mensaje y posible reordenación
				cargarConversaciones();
			}
		});
	}
	// Quitamos el gif de carga del titulo del popup
	setTimeout(function () {
		$('.popupModalHeaderLoader').addClass('hide');
	}, 100);
});




// Repetida en usuarios-conectados.js para que cargue con el inicio
function mensajesNoLeidosTotal() {
	$.ajax({
		url: 'conversaciones/noleidos',
		type: 'GET'
	}).success(function (data) {
		if (data) {
			data = JSON.parse(data);
			var contador = data.contador;
			if (contador == 0) {
				$('.Sinleer').addClass('hide');
			} else {
				$('.Sinleer').removeClass('hide');
			}
			$('.Sinleer').html(data.contador);
		}
	});
}



function cargarConversaciones() {
	// Cargamos el listado de conversaciones
	$.ajax({
		url: 'conversaciones/listado',
		type: 'GET'
	}).success(function (data) {
		if (data) {
			data = JSON.parse(data);
			$('#conversaciones').html(data.html);
			translateAll(getCookie('language'));
		}
	});
}





// Abrir una conversacion
function abrirConversacion(_id) {
	// Guardamos en session para saber que conversacion queremos abrir
	sessionStorage.setItem('openchat', _id);

	// Cargamos la conversacion
	cargarConversacion();

	// Cargamos la conversaciones para actualizar el indicador de mensajes sin leer
	cargarConversaciones();
}





// Abrir una conversacion
function cargarConversacion() {
	// Obtenemos el ID de la conversacion que queremos abrir
	var id = sessionStorage.getItem('openchat');

	// Si existe un ID cargamos la conversacion
	if (id && id.length > 0) {
		// Cargamos la conversacion
		$.ajax({
			url: 'conversaciones/conversacion',
			type: 'POST',
			data: {id: id}
		}).success(function (data) {
			if (data) {
				data = JSON.parse(data);
				// Colocamos el html del chat
				$('#conversacion #content').html(data.html);

				// Hacemos scroll hasta el primer mensaje sin leer o el final del chat si todo esta leido
				var element = $('#l_c').find('.leido-0');
				var top = (element.length > 0) ? element.position().top : $('#l_c').height();
				$('#listado_conversacion').scrollTop(top);

				// Actualizamos el numero de mensajes sin leer
				mensajesNoLeidosTotal();
			}
		});
	}
}



// Mostrar contactos para abrir una nueva conversacion
$(document).on('click', '#chat-new', function (event) {
	cargarContactos();
	$('#conversaciones').addClass('hide');
	$('#contactos').removeClass('hide');
});



// Volver al listado de conversaciones si no abrimos un nuevo chat
$(document).on('click', '#chat-back', function (event) {
	volverConversaciones();
});


// Volver al listado de conversaciones si no abrimos un nuevo chat
function volverConversaciones() {
	$('#conversaciones').removeClass('hide');
	$('#contactos').addClass('hide');
}



function cargarContactos() {
	$.ajax({
		url: 'conversaciones/contactos',
		type: 'GET'
	}).success(function (data) {
		if (data) {
			data = JSON.parse(data);
			$('#contactos').html(data.html);
			translateAll(getCookie('language'));
		}
	});
}


function iniciarConversacion(_id) {
	$.ajax({
		url: 'conversaciones/iniciar',
		type: 'POST',
		data: {'id_otro': _id}
	}).success(function (data) {
		if (data) {
			data = JSON.parse(data);
			// Abrimos el modal del chat
			popup_open('Chat', 'conversaciones', 'conversaciones', 600, 800);

			// Abrimos la conversacion nueva del chat
			abrirConversacion(data.id_conversacion);

			// Hacemos foco en el input para empezar a escribir
			setTimeout(function () {
				$('#chat-input input[type=text]').focus();
			}, 100);
		}
	});
}
