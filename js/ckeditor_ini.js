﻿function ckeditor_create(id, height)
{
	var editor = CKEDITOR.instances[id];
	if(editor)
	{
		editor.destroy(true);
	}
		
	CKEDITOR.replace(id,
	{
		height: height,
		extraPlugins : 'bbcode',
		// Remove unused plugins.
		removePlugins : 'bidi,button,dialogadvtab,div,filebrowser,flash,format,forms,horizontalrule,iframe,indent,liststyle,pagebreak,showborders,stylescombo,table,tabletools,templates',
		// Width and height are not supported in the BBCode format, so object resizing is disabled.
		disableObjectResizing : true,
		toolbar :
		[
			['Undo','Redo'],
			['Find','Replace','SelectAll','RemoveFormat'],
			['Link', 'Unlink'],
			['Bold', 'Italic','Underline'],
			['TextColor', 'Smiley' /*,'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'*/],
			['NumberedList','BulletedList']
		],
		// Strip CKEditor smileys to those commonly used in BBCode.
		smiley_images :
		[
			'regular_smile.gif','sad_smile.gif','wink_smile.gif','teeth_smile.gif',
			'embaressed_smile.gif','omg_smile.gif', 'cry_smile.gif'
		],
		smiley_descriptions :
		[
			'Contento', 'Triste', 'Gui&ntilde;o', 'Alegre', 'Avergonzado', 'Sorprendido',
			'Llor&oacute;n'
		]
	});
	
	$('#' + id).parents('form').find("input[type='submit']").off('click').on('click', function()
	{
		CKEDITOR.instances[id].updateElement();
	});
	
	$('#' + id).parents('form').find("button[type='submit']").off('click').on('click', function()
	{
		CKEDITOR.instances[id].updateElement();
	});
}