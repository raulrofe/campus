// Set URL translations
//var urlTranslate = "/translate/";
var urlTranslate = "/campus/translate/";

// Event to Ipad
var ua = navigator.userAgent,
        event = (ua.match(/iPad/i)) ? "touchstart" : "click";

// Scripts onload
$(document).ready(function () {
	// Fuction call to change language
	$("body").on("click", "[data-change-translate]", function () {
		var language = $(this).attr('data-change-translate');
		setCookie("language", language);
		translateAll(language);
	});

	// If not language choosen, spanish is default
	if (!getCookie('language')) {
		setCookie("language", "es");
	}

	// Traslate onload
	translateAll(getCookie('language'));
});

// Function to Ipad
function changeLanguage(_language){ 
	setCookie("language", _language);
	translateAll(_language);
}

// Functions
function translateStringNotification(_key, _string) {
	return translateString('notificaciones.' + _key, _string);
}
function translateStringAlerts(_key, _string) {
	return translateString('alertas.' + _key, _string);
}


function translateString(_key, _string) {
	// Set languages
	var language = getCookie('language');

	// If exist language selected
	if (language && language.length > 0) {
		// Url translations files
		$.getJSON(urlTranslate + language + ".json", function (JSON) {
			// Si no existe _string, osea que no se ha añadido la clave
			if (_string == undefined || _string == '') {
				return _key;
			}

			// If exist key in this element
			if (_key && _key.length > 0) {

				// Convert key in Array
				var arrayKey = _key.split('.');
				var temp = JSON;
				// each array position, search in json nested
				for (var i = 0; i < arrayKey.length; i++) {
					if (i < arrayKey.length) {
						temp = temp[arrayKey[i]];
					}
				}
			}

			// If not found translations, set default value
			if (temp && temp.length == 0) {
				temp = _string;
			}
			
			// Fix bug asynchronous request
			setCookie("palabraTemp", temp);

			return temp;
			
		});
	}
}


function translateAll(_language) {

	// Pongo el icono del idioma segun el LocalStorage
	$('[data-flag-language]').attr('src', 'imagenes/banderas/' + _language + '.png');

	// Url translations files
	$.getJSON(urlTranslate + _language + ".json", function (JSON) {
		// Get all element to translate
		var elements = $('[data-translate-html], [data-translate-value], [data-translate-title], [data-translate-placeholder], [data-translate-href]');

		// Change each element 
		elements.each(function (index) {
			var type;
			var _this = $(this);

			// Set type of element
			if (_this.attr('data-translate-html') !== undefined && _this.attr('data-translate-html') !== false) {
				type = "html";
			} else if (_this.attr('data-translate-value') !== undefined && _this.attr('data-translate-value') !== false) {
				type = "value";
			} else if (_this.attr('data-translate-title') !== undefined && _this.attr('data-translate-title') !== false) {
				type = "title";
			} else if (_this.attr('data-translate-placeholder') !== undefined && _this.attr('data-translate-placeholder') !== false) {
				type = "placeholder";
			}

			// Choose attribute to translate
			var key;
			switch (type) {
				case "html":
					key = _this.attr('data-translate-html');
					break;
				case "value":
					key = _this.attr('data-translate-value');
					break;
				case "title":
					key = _this.attr('data-translate-title');
					break;
				case "placeholder":
					key = _this.attr('data-translate-placeholder');
					break;
			}


			// If exist key in this element
			if (key && key.length > 0) {
				// Convert key in Array
				var arrayKey = key.split('.');
				var temp = JSON;
				// each array position, search in json nested
				for (var i = 0; i < arrayKey.length; i++) {
					if (i < arrayKey.length) {
						temp = temp[arrayKey[i]];
					}
				}

				// Set value in HTML element
				switch (type) {
					case "html":
						_this.html(temp);
						break;
					case "value":
						_this.val(temp);
						break;
					case "title":
						_this.attr('alt', temp);
						_this.attr('title', temp);
						break;
					case "placeholder":
						_this.attr('placeholder', temp);
						break;
				}
			}

			// Para cambiar los enlaces
			if (_this.attr('data-translate-href') !== undefined && _this.attr('data-translate-href') !== false) {

				// Cojo el elemento del json
				key = _this.attr('data-translate-href');

				// Lo sustituyo segun el idioma
				if (key && key.length > 0) {
					// Convert key in Array
					var arrayKey = key.split('.');
					var temp = JSON;

					// each array position, search in json nested
					for (var i = 0; i < arrayKey.length; i++) {
						if (i < arrayKey.length) {
							temp = temp[arrayKey[i]];
						}
					}
					
					_this.attr('href', temp);
				}
			}
		});
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function setCookie(_name, _value) {
	var myDate = new Date();
	myDate.setMonth(myDate.getMonth() + 12);
	document.cookie = _name + "=" + _value + ";expires=" + myDate + ";path=/";
}
