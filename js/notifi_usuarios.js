
$(document).ready(function () {
	start_notification();
});



// cargamos los datos de los usuarios coenctados actualemente como datos de referencia para saber quien se conecta y desconecta
function start_notification() {
	if ($('.listado_cursos').length) {
		setTimeout(function () {
			start_notification();
		}, 5000);
	} else {
		$.ajax({
			url: 'notificaciones/usuarios',
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				setTimeout(function () {
					update_notification();
				}, 2000);
			},
			error: function () {
				setTimeout(function () {
					start_notification();
				}, 2000);
			}
		});
	}
}


function update_notification() {
	$.ajax({
		url: 'notificaciones/usuarios',
		type: 'POST',
		cache: false,
		contentType: false,
		processData: false
	}).done(function (data) {
		notifi_user_callback(data);
	}).error(function (data) {
//		console.log(data);
	});
}



function notifi_user_callback(_data) {

	var data = jQuery.parseJSON(_data);

	if (data) {
		if (data.html) {
			if ($('#notifiUsers').length) {
				$('#notifiUsers').addClass('hide');
				$('#notifiUsers').find('.fleft').html(data.html);
			} else {
				var element = '<div id="notifiUsers" class="hide">' +
					'<div class="fleft">' + data.html + '</div>' +
					'<div class="fright"><a href="#" onclick="close_notification(); return false;" title="Cerrar notificación">X</a></div></div>' +
					'<div class=""clear>' +
					'</div>';
				$('body').append(element);
			}

			$('#notifiUsers').css('left', (($(window).width() - $('#notifiUsers').width()) / 2));
			$('#notifiUsers').slideDown();


			setTimeout(function () {
				close_notification();
			}, 5000);
		}
	}

	// Volvemos a repetir todo el proceso cada 5s
	setTimeout(function () {
		update_notification();
	}, 30000); /* 30segundos */
//	}, 5000); 5segundos
}


// Auto-cerrado de las notificaciones
function close_notification() {
	$('#notifiUsers').slideUp();
}

