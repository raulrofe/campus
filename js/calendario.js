function crearCalendario(idInput,fechaFormato)
{

	if(fechaFormato == undefined)
	{
		fechaFormato = 'dd/mm/yy';
	}
	
	$("#" + idInput).datepicker({
		dateFormat: fechaFormato,
		firstDay : 1,
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio' ,'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
	});
}