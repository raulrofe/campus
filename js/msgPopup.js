function msgPopupOpen(json, num)
{
	var objDate = new Date();
	var id = 'msgPopupId_' + objDate.getTime();
	
	var title = 'Mensajes nuevos';
	var content = '';
	var addClass = '';
	for(var cont = 0; cont < num; cont++)
	{
		if(cont > 0)
		{
			addClass = 'hide';
		}

		content +=
			'<div class="msgPopup_' + (cont + 1) + ' msgPopupContentDinamic ' + addClass + '">' +
				'<div class="finTituloMensage">';
		
			if(num > 1)
			{
				title = 'Mensajes nuevos';
				content +=
					'<div class="msgPopupButtons">' +
						'<a class="msgPopupButtonPrev msgPopupButtonDisabled" href="#" onclick="msgPopupPrev(\'' + id + '\', ' + num + ');return false;" title="Anterior mensaje">' +
							'<img src="imagenes/mensajes_emergentes/anterior.jpg" alt="" style="margin-top:15px;" />' +
						'</a>' +
						'<a class="msgPopupButtonNext" href="#" onclick="msgPopupNext(\'' + id + '\', ' + num + ');return false;" title="Siguiente mensaje">' +
							'<img src="imagenes/mensajes_emergentes/siguiente.jpg" alt="" style="margin-top:15px;float:left;" />' +
						'</a>' +
						'<span class="msgPopupPage hide">1</span>' +
						'<div class="msgPopupVisualCount clear">' +
							'<span class="msgPopupVisualCountNum">1</span>' +
							'<span> de ' + num + '</span>' +
						'</div>'+
					'</div>';
			}
			
		content += '<div class="tituloMensage fleft">' +
						'<p>De: ' + json[cont]['autor'] + '</p>' +
						'<p class="titleCurso">' + json[cont]['curso'] + '</p>' +
						'<p class="titleCurso">' + json[cont]['fecha'] + '</p>' +
					'</div>' +
					'<div class="clear"></div>' +
				'</div>';
		
		if(json[cont]['url_icono'] != 'NULL')
		{
			content += '<div class="fleft" style="margin:25px 0 0 25px;"><img src="imagenes/mensajes_emergentes/' + json[cont]['url_icono'] + '" alt="" /></div>' +
						'<div class="elementoContenido">' +
							'<p class="msgPopupContent">' + json[cont]['mensaje'] + '</p>' +
						'</div>' +
						'<div class="clear"></div>';
		}
		else
		{
			content += '<div class="elementoContenido" style="margin:0;">' +
						'<p class="msgPopupContent" style="float:none;width:auto;">' + json[cont]['mensaje'] + '</p>' +
					'</div>' +
					'<div class="clear"></div>';
		}
		
		var objDate = new Date();
		if(json[cont]['esAlumno'])
		{
			var urlReply = 'mensajes-emergentes/nuevo/alumno/' + json[cont]['idUsuario'];
		}
		else
		{
			var urlReply = 'mensajes-emergentes/nuevo/tutor/' + json[cont]['idUsuario'];
		}
		
		if(json[cont]['esAlumno'] != undefined)
		{
			content += '<div class="msgPopupReply"><a href="#" onclick="popup_open(\'Mensajes emergentes\', \'Mensajes_emergentes_usuario_' + objDate.getTime() + '\',  \'' + urlReply + '\', 600, 600); return false;" title="Responder este mensaje emergente">' +
						'<img alt="responder" src="imagenes/foro/responder.png">Responder</a></div>';
		}
		content += '</div>';
			
	}
	
	content += '<div class="clear"></div>';
		
	var html =
	'<div id="' + id + '" class="messagePopup">' +
		'<div class="messagePopupAll">' +
			'<div class="messagePopupHeader">' +
				'<span class="numeroMensaje fleft">' + num + '</span>' +
				'<span><h2>' + title + '</span></h2>' +
				'<a href="#" onclick="msgPopupClose(\'' + id + '\'); return false;" title="">X</a>' +
			'</div>' +
			'<div class="messagePopupContent">' + content + '</div>' +
			'<div class="messagePopupNav"></div>' +
		'</div>' +
	'</div>';
	
	$('body').append(html);
	
	// POSICIONAMOS
	//var top = ($(window).height() - $('#messagePopup').height()) / 2;
	//$('#messagePopup').css('top', top + 'px');
	
	var left = ($(window).width() - $('#' + id).width()) / 2;
	$('#' + id).css('left', left + 'px');
	
	$('#' + id).draggable({
		handle : '.messagePopupHeader',
		start : function()
		{
			msgPopupIndexTop(id);
		}
	});
}

function msgPopupClose(id)
{
	$('#' + id).remove();
}

function msgPopupPrev(id, maxNum)
{
	var num = parseInt($('#' + id).find('.msgPopupPage').html()) - 1;
	
	if(num >= 1)
	{
		$('#' + id).find('.msgPopupContentDinamic').addClass('hide');
		$('#' + id).find('.msgPopup_' + num).removeClass('hide');
		
		$('#' + id).find('.msgPopupPage').html(num);
		$('#' + id).find('.msgPopupVisualCountNum').html(num);
		
		msgPopupButtonsControl(id, num, maxNum);
	}
}

function msgPopupNext(id, maxNum)
{
	var num = parseInt($('#' + id).find('.msgPopupPage').html()) + 1;
	
	if(num <= maxNum)
	{
		$('#' + id).find('.msgPopupContentDinamic').addClass('hide');
		$('#' + id).find('.msgPopup_' + num).removeClass('hide');
	
		$('#' + id).find('.msgPopupPage').html(num);
		$('#' + id).find('.msgPopupVisualCountNum').html(num);
	
		msgPopupButtonsControl(id, num, maxNum);
	}
}

function msgPopupButtonsControl(id, num, maxNum)
{
	// MOSTRAR O NO LOS BOTONES DE NAVEGACIOON ENTRE MENSAJES
	if(num <= 1 || num > maxNum)
	{
		$('#' + id).find('.msgPopup_' + num).find('.msgPopupButtonPrev').addClass('msgPopupButtonDisabled');
	}
	else
	{
		$('#' + id).find('.msgPopup_' + num).find('.msgPopupButtonPrev').removeClass('msgPopupButtonDisabled');
	}
	
	if(num >= maxNum)
	{
		$('#' + id).find('.msgPopup_' + num).find('.msgPopupButtonNext').addClass('msgPopupButtonDisabled');
	}
	else
	{
		$('#' + id).find('.msgPopup_' + num).find('.msgPopupButtonNext').removeClass('msgPopupButtonDisabled');
	}
}

function msgPopupIndexTop(id)
{
	$('.msgPopup').css('z-index', 30);
	$('#' + id).css('z-index', parseInt($('#' + id).css('z-index')) + 1);
};