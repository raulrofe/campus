LibModalInto = {};

LibModalInto.create = function (idPopupModal, url)
{
	$.ajax({
		url: url,
		success: function (data) {

			LibModalInto.draw(idPopupModal, data);
		}
	});
};

LibModalInto.draw = function (idPopupModal, content)
{
	popupId = 'popupModalInto';

	var html =
		'<div id="popupModalInto" class="popupModal">' +
		'<div class="popupModalWrapper">' +
		'<div class="popupModalHeader">' +
		'<a href="#" onclick="LibModalInto.close(); return false;" title="Cerrar"><img src="imagenes/popupModal/window_close.png" alt="" /></a>' +
		'</div>' +
		'<div class="popupModalContent">' +
		content
	'</div>' +
		'</div>' +
		//'<iframe id="' + popupId + '_form_iframe" name="' + popupId + '_form_iframe" class="hide"></iframe>' +
		'</div>';

	$('#' + idPopupModal).append(html);

	$('#' + popupId).draggable({
		handle: '.popupModalHeader',
		containment: 'document'
	});

	var left = ($(window).width() - $('#' + popupId).width()) / 2;
	$('#' + popupId).css('left', left + 'px');

	$('#' + popupId).css({
		top: '100px'
	});

	translateAll(getCookie('language'));
};

LibModalInto.close = function (form)
{
	$('#popupModalInto').remove();
}

LibModalInto.post = function (form)
{
	var url = $(form).attr('action');
	var target = $(form).attr('data-target');

	$.ajax({
		url: url,
		type: 'post',
		dataType: 'json',
		data: $(form).serialize(),
		success: function (json)
		{
			if (json != undefined && json != null)
			{
				if (json.url != undefined && json.url != null)
				{
					LibPopupModal._ajaxLoad(target, json.url, false);

					$('#popupModalInto').remove();
				}

				if (json.mensaje != undefined && json.mensaje != null)
				{
					alertMsg(json.key, json.mensaje);
				}
			}
		},
		complete: function ()
		{
			$(form).parents('#popupModalInto').parents('.popupModal ').find('.popupModalHeaderLoader').addClass('hide');
		}
	});
};

function alertConfirmAjax(idModalbox, key, msg, url) {

	translateString('alertas.' + key, msg);
	
	setTimeout(function(){
		
		var string = getCookie('palabraTemp');

		if(confirm(string))	{
			$.ajax({
				url: url,
				type: 'get',
				dataType: 'json',
				success: function(json)
				{
					if(json != undefined && json != null){

						if(json.url != undefined && json.url != null){

							LibPopupModal._ajaxLoad(idModalbox, json.url, false);
							$('#popupModalInto').remove();
						}

						if(json.mensaje != undefined && json.mensaje != null){
							alertMsg(json.mensaje);
						}
					}
				},
				complete: function()
				{
					$(idModalbox).find('.popupModalHeaderLoader').addClass('hide');
				}
			});
		}
	},500);

	return false;
}
