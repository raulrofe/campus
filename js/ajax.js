//window.idCurso = null;

function cambiarHash(hash)
{
	if($('#idLogAcceso_menu').size() == 1)
	{
		$.ajax({
			url : 'index.php?idAcceso=' + $('#idLogAcceso_menu').html(),
			async: false
		});
	}

	var hashActual = obtenerHash();

	if(hash == hashActual)
	{
		cargarPagina();
	}
	else
	{
		window.location.hash = '/' + hash;
	}
}

function cargarPagina()
{
	$('#msgCargaAjax').removeClass('hide');

	var hash = obtenerHash();

	// cierra los popups en el listado de cursos
	if(hash == '')
	{
		for(var i in window.popup_obj)
		{
			window.popup_obj[i].close();
		}
	}

	var method = 'GET';
	//var data = '';
	/*if(window.idCurso != null && isNaN(window.idCurso))
	{
		method = 'POST';
		data = 'idCurso=' + window.idCurso;
	}*/

	$.ajax({
		url : 'ajax/' + hash,
		dataType : 'html',
		type : method,
		//data: data,
		success : function(data)
		{
			$('#general').html(data);

			htmlRenderLinks('#general');
		},
		complete : function()
		{
			/*if(hash.match(/^hall\/([0-9]+)/))
			{
				cambiarHash('hall');
			}*/

			$('#msgCargaAjax').addClass('hide');

			translateAll(getCookie('language'));
		}
	});
}

function htmlRenderLinks(element)
{
	if(element != '#popupModal_archivador_electronico_iframe'){
		$(element + ' a').each(function()
		{
			var attrOnclick = $(this).attr('onclick');
			var hash = obtenerHash();
			if(attrOnclick != undefined && attrOnclick != null)
			{
				popupUrl = attrOnclick.match(/^(parent\.)?popup_open\(("|')(.*)("|'),([\s]+)("|')([a-zA-Z0-9_]+)("|'),([\s]+)("|')(.*)("|'),([\s]+)([0-9]+),([\s]+)([0-9]+)/, 'gi');

				if(popupUrl != null)
				{
					var urlAdd = '?name=' + encodeURIComponent(popupUrl[7]) +
					'&title=' + encodeURIComponent(popupUrl[3]) +
					'&width=' + encodeURIComponent(popupUrl[16]) +
					'&height=' + encodeURIComponent(popupUrl[14]) +
					'&hash=' + encodeURIComponent(hash);

					$(this).attr('href', popupUrl[9] + popupUrl[11] + urlAdd);
				}
			}
		});
	}
}

function cargarActualHash()
{
	var hash = obtenerHash();

	var url = window.location.href;
	var urlBase = $('base').attr('href');

	var regex = new RegExp('^' + urlBase);
	url = url.replace(regex, '');

	urlHash = url.match(/cursos\/#\/hall\/([0-9]+)/);

	if(urlHash == null)
	{
		urlHash2 = url.match(/(cursos\/#\/)([a-zA-Z0-9\-_\/]+)/);

		// cuando un modalbox en otra pestaña
		if(urlHash2 != null)
		{
			console.log('cargar hash');
			// variables del GET para el modalbox
			var m_name = decodeURIComponent(getParamRequestGet(url, 'name'));
			var m_title = decodeURIComponent(getParamRequestGet(url, 'title'));
			var m_width = decodeURIComponent(getParamRequestGet(url, 'width'));
			var m_height = decodeURIComponent(getParamRequestGet(url, 'height'));
			var m_hash = decodeURIComponent(getParamRequestGet(url, 'hash'));

			if(m_name != '' && m_title != '' && m_width != '' && m_height != '' && m_hash != '')
			{
				cambiarHash(m_hash);

				popup_open(m_title, m_name, urlHash2[2], m_height, m_width);
			}
			else
			{
				cambiarHash(hash);
			}
		}
		else
		{
			cambiarHash(hash);
		}
	}
	else
	{
		cambiarHash(hash);
	}
}

function obtenerHash()
{
	var hash = window.location.hash.replace(/^#\//, '');

	return hash;
}

$(document).ready(function()
{
	eventHash();
});

function eventHash()
{
	if($('#usuario_no_logueado').size() == 0)
	{
		$(window).hashchange(function()
		{
			cargarPagina();
		});

		cargarActualHash();
	}
}
