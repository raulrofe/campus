
var intervaloConectados = 10; // Segundos
var intervaloInicioUsuariosConectados;

$(document).ready(function () {
	// Para esperar que tras el login y la pantalla de "Mis cursos" llegues al HALL y actualizar el numero de conectados
	intervaloInicioUsuariosConectados = setInterval(function () {
		actualizarConectados();
	}, 1000);
});



function actualizarConectados() {
	// Si no estamos en la pantalla de "Mis cursos" 
	if ($('.listado_cursos').length == 0) {
		// Apagamos el intervalo
		clearInterval(intervaloInicioUsuariosConectados);
		intervaloInicioUsuariosConectados = setInterval(function () {
			actualizarConectados();
		}, intervaloConectados * 1000);
		// Traemos la plantilla de los usuarios conectados
		$.ajax({
			url: 'conectados/obtener',
			type: 'GET'
		}).success(function (data) {
			if (data) {
				data = JSON.parse(data);
				// Colocamos el html del chat
				$('#usuarios_conectados ul').html(data.html);
				// Forzamos a entero o sino a 0, además sumamos uno para contarse a uno mismo
				var numUsuarios = +(parseInt(data.numUsuarios)) || 0;
				numUsuarios++;
				$('#menuCurso .NumeroConectados').html(numUsuarios);
				translateAll(getCookie('language'));
			}
		});
	}
}
