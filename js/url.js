function urlRedirect(url)
{
	window.location.href = $('base').attr('href') + url;
}

function getParamRequestGet(url, name)
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( url );
  if( results == null )
    return "";
  else
    return results[1];
}

function removeParamRequestGet(url, parameter)
{
	if(url != undefined)
	{
	  var urlparts= url.split('?');
	
	  if (urlparts.length>=2)
	  {
	      var urlBase=urlparts.shift(); //get first part, and remove from array
	      var queryString=urlparts.join("?"); //join it back up
	
	      var prefix = encodeURIComponent(parameter)+'=';
	      var pars = queryString.split(/[&;]/g);
	      for (var i= pars.length; i-->0;)               //reverse iteration as may be destructive
	          if (pars[i].lastIndexOf(prefix, 0)!==-1)   //idiom for string.startsWith
	              pars.splice(i, 1);
	     if(pars != '')
	     {
	    	 url = urlBase+'?'+pars.join('&');
	    }
	    else
	    {
	    	url = urlBase;
	    }
	  }
	}
	else
	{
		url = '';
	}
  return url;
}

function addParamToUrl(url, param){
    url += (url.split('?')[1] ? '&':'?') + param;
    
    return url;
}