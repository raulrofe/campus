//window.urlAnchor = null;
//window.historyPosCurrent = 0;

function popup_real_open(name, url, height, width)
{
	window.open($('base').attr('href') + url, name, 'height=' + height + ',width=' + width);
}

function popup_open(titleShow, name, url, height, width)
{
	LibPopupModal.open(titleShow, name, url, height, width);
}

function popup_simple_open(name, url, height, width)
{
	LibPopupModal.open('', name, url, height, width);
}

var LibPopupModal = {};

LibPopupModal.open = function(titleShow, title, url, height, width, showIcons)
{
	// mostrar o no lo siconos de las cabeceras y el link de abajo del popupModal
	if(showIcons != undefined && showIcons != null && showIcons == false)
	{
		showIcons = false;
	}
	else
	{
		showIcons = true;
	}

	var popupId = 'popupModal_' + title;

	// calculamos las dimensiones exactas del modalbox segun la pantalla
	var top = 100;
	var heightScreen = $(window).height();
	if(heightScreen <= (height + top + 150))
	{
		height = heightScreen - 150;
		top = 50;
	}

	// si la ventana ya estaba minimizada ...
	if($('#popupBtn_' + popupId).size() > 0)
	{
		//$('#popupBtn_' + popupId).remove();
		$('#' + popupId).removeClass('hide');
	}

	// si no estaba abierta esta ventana
	if($('#' + popupId).size() == 0)
	{
		if($('#principalNombreCurso').size() == 1)
		{
			var nombreCurso = $('#principalNombreCurso').html();
			var addpuntos = '';
			if(nombreCurso.length > 30)
			{
				addpuntos = '...';
			}

			nombreCurso = nombreCurso.substr(0,30) + addpuntos;
		}
		else
		{
			var nombreCurso = '';
		}

		var html =
		'<div id="' + popupId + '" class="popupModal" data-url="' + url + '" style="width:' + (width - 6) + 'px; top: ' + top + 'px">' +
			'<div class="popupModalWrapper">' +
				'<div class="popupModalHeader">';
					if(showIcons)
					{
						html +=
						'<div class="popupModalHeaderHistory">' +
							'<a class="popupModalHeaderHistory_back" href="#" onclick="LibPopupModal.historyBack(\'' + popupId + '\'); return false;" title="Ir atr&aacute;s" data-translate-title="opcionesmodal.iratras"><img src="imagenes/popupModal/window_back.png" alt="" /></a>' +
							/*'<a class="popupModalHeaderHistory_forward" href="#" onclick="' + popupId + '_iframe.history.forward(); return false;" title="Ir hacia delante"><img src="imagenes/popupModal/window_forward.png" alt="" /></a>' +*/
						'</div>';
					}
					else
					{
						html +=
						'<div class="popupModalHeaderHistory">' +
							'<a class="popupModalHeaderHistory_back" href="#" onclick="return false;" title="Ir a atr&aacute;s" data-translate-title="opcionesmodal.iratras"><img src="imagenes/popupModal/window_back.png" alt="" /></a>' +
						'</div>';
					}

					html += '<h2 data-translate-html="popup.' + titleShow + '">' + titleShow + '</h2>' +
							'<div class="popupModalHeaderLoader hide"><img src="imagenes/loaderPopupMini.gif" alt=""></div>';
					if(showIcons)
					{
						html +=
						'<a href="#" class="popupModalHeaderIconReload" onclick="LibPopupModal.reload(\'' + popupId + '\'); return false;" title="Recargar" data-translate-title="opcionesmodal.actualizar"><img src="imagenes/popupModal/window_reload.png" alt="" /></a>' +
						'<a href="#" onclick="LibPopupModal.minimize(\'' + popupId + '\', \'' + title + '\'); return false;" title="Minimizar" data-translate-title="opcionesmodal.minimizar"><img src="imagenes/popupModal/window_minimize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_maximize" onclick="LibPopupModal.maximize(\'' + popupId + '\', \'' + title + '\'); return false;" title="Maximizar" data-translate-title="opcionesmodal.maximizar"><img src="imagenes/popupModal/window_maximize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_restore hide" onclick="LibPopupModal.restore(null, \'' + popupId + '\', ' + height + ', ' + width + '); return false;" title="Restaurar"><img src="imagenes/popupModal/window_restore.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_close" onclick="LibPopupModal.close(\'' + popupId + '\'); return false;" title="Cerrar" data-translate-title="opcionesmodal.cerrar"><img src="imagenes/popupModal/window_close.png" alt="" /></a>';
					}
					else
					{
						html +=
						'<a href="#" class="popupModalHeaderIconReload popupModalHeaderIconDisabled" onclick="return false;" title="Recargar" data-translate-title="opcionesmodal.actualizar"><img src="imagenes/popupModal/window_reload.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderIconDisabled" onclick="return false;" title="Minimizar" data-translate-title="opcionesmodal.minimizar"><img src="imagenes/popupModal/window_minimize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_maximize popupModalHeaderIconDisabled" onclick="return false;" title="Maximizar" data-translate-title="opcionesmodal.maximizar"><img src="imagenes/popupModal/window_maximize.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderBtn_restore popupModalHeaderIconDisabled hide" onclick="return false;" title="Restaurar"><img src="imagenes/popupModal/window_restore.png" alt="" /></a>' +
						'<a href="#" class="popupModalHeaderIconDisabled" onclick="return false;" title="Cerrar" data-translate-title="opcionesmodal.cerrar"><img src="imagenes/popupModal/window_close.png" alt="" /></a>';
					}

				html +=
				'</div>' +
				/*'<div class="popupNombreCurso clear"><b>Curso:</b> ' + $('#principalNombreCurso').html() + '</div>' +*/
					'<div id="' + popupId + '_wrapper_aux" style="width:100%;height:' + height + 'px;background:#fff;overflow:auto;overflow-y:scroll">' +
						'<div class="popupModalContent"><img class="popupModalLoader" src="imagenes/popupModalLoader.gif" alt="Cargando" data-translate-title="opcionesmodal.cargando" />'+
						'<div id="' + popupId + '_iframe" style="height:100%;" name="' + popupId + '_iframe" class="hide"></div></div>' +
					'</div>';
				if(showIcons)
				{
					html += '<div class="windowPopupCloseBtn"><a href="#" onclick="LibPopupModal.close(\'' + popupId + '\'); return false;" data-translate-html="general.cerrar_ventana">Cerrar ventana</a></div>';
				}
				else
				{
					html += '<div class="windowPopupCloseBtn popupModalHeaderIconDisabled"><a href="#" onclick="return false;" data-translate-html="general.cerrar_ventana">Cerrar ventana</a></div>';
				}

				
			html += '<iframe id="' + popupId + '_form_iframe" name="' + popupId + '_form_iframe" class="hide"></iframe>' +
					//'<div id="' + popupId + '_iframe_textarea_aux" class="hide"></div>'
				'<ul class="popupHistory hide"></ul>' +
				'</div>' +
		'</div>';

		//Creo ventana pop up
		$('body').append(html);

		//Ejecuto la traducción para que el timpo de espera del Ajax salga lo que hay en el idioma que es
		translateAll(getCookie('language'));

		// a_ade boton de popup
		$('#popupsButtons').append('<div id="popupBtn_' + popupId + '" class="popupButtonFixed"><a href="#" onclick="LibPopupModal.restore(this, \'' + popupId + '\'); return false;" title="" data-translate-html="popup.' + titleShow + '">' + titleShow + ' <img src="imagenes/popupModal/window_restore.png" alt="" /></a></div>');

		

		var left = ($(window).width() - $('#' + popupId).width()) / 2;
		$('#' + popupId).css('left', left + 'px');

		/*$('#' + popupId).draggable({
			handle : '.popupModalWrapper',
			containment : 'window'
		});*/

		$('#' + popupId + '_form_iframe').load(function()
		{
			setTimeout(function()
			{
				var iframeHtml = $('#' + popupId + '_form_iframe').contents().find('body').html();
				if(iframeHtml != null && iframeHtml != '')
				{
					if($.browser.msie && ($.browser.version == '8.0' || $.browser.version == '7.0'))
					{
						$('#' + popupId + '_iframe').css('visibility', 'hidden');

						$('#' + popupId + '_iframe').html(iframeHtml);

						if($('#' + popupId + '_iframe > pre').size() == 1)
						{
							var html2 = $('#' + popupId + '_iframe > pre').html();

						    //-- get rid of html-encoded characters:
							html2 = html2.replace(/&nbsp;/gi," ");
							html2 = html2.replace(/&amp;/gi,"&");
							html2 = html2.replace(/&quot;/gi,'"');
							html2 = html2.replace(/&lt;/gi,'<');
							html2 = html2.replace(/&gt;/gi,'>');

							$('#' + popupId + '_iframe').html(html2);
						}
					}
					else
					{
						//var htmlFinal = iframeHtml.substr(5, iframeHtml.length - 5).replace(/&lt;/gi, '<').replace(/&gt;/gi, '>');
						var htmlFinal = iframeHtml;

						$('#' + popupId + '_iframe').css('visibility', 'hidden');
						//alert(htmlFinal);
						$('#' + popupId + '_iframe').html(htmlFinal);
						if($('#' + popupId + '_iframe').size() == 1)
						{
							//$('#' + popupId + '_iframe').unwrap();
						}
					}

					LibPopupModal.resizePanel(popupId);
					$('#' + popupId + '_iframe').css('visibility', 'visible');

					// para en envio de formularios dentro del modalbox
					LibPopupModal._repareForms(popupId);

					htmlRenderLinks('#' + popupId + '_iframe');

					$('#' + popupId).find('.popupModalHeaderLoader').addClass('hide');

					var urlReturn = $('#' + popupId).find('.popupModalUrlCurrent').html();

					if($('#' + popupId).attr('data-url') != urlReturn)
					{
						LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
					}

					$('#' + popupId).attr('data-url', urlReturn);

					// deshabilita el boton del historial
					if($('#' + popupId).find('.popupHistory li').size() == 0)
					{
						$('#' + popupId).find('.popupModalHeaderHistory').css('opacity', 0.4);
						$('#' + popupId).find('.popupModalHeaderHistory a').css('cursor', 'default');
					}
					else
					{
						$('#' + popupId).find('.popupModalHeaderHistory').css('opacity', 1);
						$('#' + popupId).find('.popupModalHeaderHistory a').css('cursor', 'pointer');
					}
				}
			}, 200);
		});

		//LibPopupModal.indexTop(popupId);

		// cuando se cargue el contenido del iframe
		$.ajax({
			url: url,
			success: function(data)
			{
				$('#' + popupId).find('.popupModalLoader').addClass('hide');

				$('#' + popupId + '_iframe').html(data);
				//document.getElementById(popupId + '_iframe').innerHTML = data;

				$('#' + popupId + '_iframe').removeClass('hide');

				//alert($('#' + popupId + '_iframe').html());

				var left = ($(window).width() - $('#' + popupId).width()) / 2;
				$('#' + popupId).css('left', left + 'px');

				$('#' + popupId).draggable({
					handle : '.popupModalHeader',
					containment : 'document',
					start : function()
					{
						LibPopupModal.indexTop(popupId);
					}
				});

				$('#' + popupId).resizable({
					alsoResize  : /*'#' + popupId + ' .popupModalWrapper, */'#' + popupId + '_wrapper_aux',
					minHeight   : 400,
					minWidth    : 400,
					containment : 'document',
					resize : function(event, ui)
					{
						LibPopupModal.resizePanel(popupId);
						/*var height = ui.size.height - (ui.originalSize.height - ui.position.top);
						$('#' + popupId + '_wrapper_aux').height(height + 'px');
						$('#' + popupId + ' _wrapper_aux').html(height);*/
					}
				});

				$('#' + popupId).find('form').live('submit', function()
				{
					$('#' + popupId).find('.popupModalHeaderLoader').removeClass('hide');
				});

				$('#' + popupId).find('button[data-url]').live('click', function()
				{
					LibPopupModal.loadPage(this, $(this).attr('data-url'));
				});

				LibPopupModal.indexTop(popupId);

				// evento de click para z-index
				$('#' + popupId + '_iframe').click(function(){
					LibPopupModal.indexTop(popupId);
				});

				LibPopupModal._repareForms(popupId);

				htmlRenderLinks('#' + popupId + '_iframe');

				// crea enlaces para cuando un usuario abra un link en otra pestaña
				$('#' + popupId + '_iframe').on('mousedown', 'a', function()
				{
					if(popupId != 'popupModal_archivador_electronico'){
						//Si el enlace contiene este parametro no añadira lo necesario para el modalbox
						var externUrl = $(this).attr('data-externUrl');

						var attrHref = $(this).attr('href');

						if(attrHref != '#' && attrHref != '')
						{
							var modalbox = $(this).parents('.popupModal');
							var modalboxWidth = modalbox.width();
							var modalboxHeight = modalbox.height();
							var modalboxId = modalbox.attr('id');
							var modalboxTitle = modalbox.find('.popupModalHeader h2').html();
							var modalboxhash = obtenerHash();

							var urlAdd = 'name=' + encodeURIComponent(modalboxId) +
								'&title=' + encodeURIComponent(modalboxTitle) +
								'&width=' + encodeURIComponent(modalboxWidth) +
								'&height=' + encodeURIComponent(modalboxHeight) +
								'&hash=' + encodeURIComponent(modalboxhash);

							var urlCurrent = $(this).attr('href');
							if(urlCurrent != undefined)
							{
								if(urlCurrent == '#')
								{
									urlCurrent = '';
								}

								if(urlCurrent.match(/(.*)?([a-zA-Z\-_]+)=(.*)/i) && !urlCurrent.match(/(.*)name=([a-zA-Z\-_]+)&title=([a-zA-Z\-_]+)(.*)/i))
								{
									urlAdd = '&' + urlAdd;
								}
								else
								{
									urlAdd = '?' + urlAdd;
								}

								if(externUrl == undefined)
								{
									//Enlace interior de la plataforma con modalbox
									$(this).attr('href', urlCurrent + urlAdd);
								}
								else
								{
									//Enlace externo
									$(this).attr('href', urlCurrent);
								}
							}
						}
					}
				});

				// evento al hacer click en un link del modalbox
				$('#' + popupId + '_iframe a').live('click', function()
				{
					// para los links con href="#" y href=""
					if($.browser.msie &&  $.browser.version == '7.0'
						&& ($.trim($(this).attr('href')) == $.trim($('head base').attr('href') + '#')
								|| $.trim($(this).attr('href')) == $.trim($('head base').attr('href'))))
					{

					}
					/*else if($.trim($(this).attr('href')) != $.trim($('head base').attr('href') + '#')
						|| $(this).attr('data-alert-confirm') == undefined || $(this).parents('.cke_button').size() == 0
						|| $(this).parents('#cke_top_correo_textearea').size() == 0 || $(this).parents('#cke_top_mensajes_emergentes_textarea').size() == 0)
					{*/
					else if($.trim($(this).attr('href')) != $.trim($('head base').attr('href') + '#')
						&& ($(this).attr('onclick') == undefined || ($(this).attr('onclick') != undefined && !$(this).attr('onclick').match(/popup_open\(/)) && $(this).parents('.popupModal'))
						&& $(this).attr('data-alert-confirm') == undefined && $(this).parents('.cke_button').size() == 0
						&& $(this).parents('#cke_top_correo_textearea').size() == 0 && $(this).parents('#cke_top_mensajes_emergentes_textarea').size() == 0)
					{
						if($('#' + popupId).attr('data-url') != undefined)
						{
							LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
						}

						var urlAnchor = $(this).attr('href');
						var targetAnchor = $(this).attr('target');

						if(urlAnchor != '' && urlAnchor != '#'
							&& (targetAnchor == undefined || (targetAnchor != undefined && targetAnchor != '_blank')))
						{
							if($(this).attr('data-nofollow') == undefined)
							{
								$('#' + popupId).attr('data-url', urlAnchor);
								//$('#' + popupId).attr('data-url', $(this).attr('href'));
								//$('#' + popupId).attr('data-url-current', urlAnchor);
							}

							LibPopupModal._ajaxLoad(popupId, urlAnchor);
						}

						if(targetAnchor == undefined || (targetAnchor != undefined && targetAnchor != '_blank'))
						{
							return false;
						}
					}
				});

				LibPopupModal.sendLogAccess(popupId);
				LibPopupModal.resizePanel(popupId);
				
				// Translate popup content
				translateAll(getCookie('language'));
			},
			error : function()
			{
				$('#' + popupId + '_iframe').html('No se pudo cargar');
			}
		});

		// cargamos el contenido en el iframe
		//$('#' + popupId) .find('.popupModalContent iframe').attr('src', url + '?iframe_name=' + popupId);
	}
	// si estaba abierta se recarga el contenido del iframe
	else
	{
		// si la ventana ya estaba minimizada ...
		/*if($('#popupBtn_' + popupId).size() > 0)
		{
			$('#popupBtn_' + popupId).remove();
		}*/

		$('#' + popupId).removeClass('hide');
		(popupId);
		$('#' + popupId).focus();

		LibPopupModal.loadPage($('#' + popupId).find('> div'), url);
		
		translateAll(getCookie('language'));
	}
};

LibPopupModal.loadPage = function(element, url)
{
	var popupId = $(element).parents('.popupModal').attr('id');

	if($('#' + popupId).attr('data-url') != undefined)
	{
		LibPopupModal.addHistory(popupId, $('#' + popupId).attr('data-url'));
	}

	$('#' + popupId).attr('data-url', url);
	//$('#' + popupId).attr('data-url-current', url);

	LibPopupModal._ajaxLoad(popupId, url);
};

LibPopupModal._ajaxLoad = function(popupId, url, reload, zindex)
{
	$('#' + popupId + '_iframe').addClass('hide');
	$('#' + popupId).find('.popupModalHeaderLoader').removeClass('hide');

	var urlReferer = {};
	var urlCurrent = $('#' + popupId).attr('data-url');
	var getUrlReferer = getParamRequestGet(urlCurrent, 'url-referer');

	if(url != urlCurrent)
	{
		if(urlReferer == '')
		{
			urlReferer = {'url-referer' : encodeURIComponent(urlCurrent)};
		}
		else
		{
			urlCurrent = removeParamRequestGet(urlCurrent, 'url-referer');

			urlReferer = {'url-referer' : encodeURIComponent(urlCurrent)};
		}
	}

	if(reload != undefined && reload == true)
	{
		urlReferer.refresh = '1';
	}

	$.ajax({
		url: url,
		data : urlReferer,
		success: function(data)
		{
			$('#' + popupId).find('.popupModalHeaderLoader').addClass('hide');

			$('#' + popupId + '_iframe').html(data);
			//document.getElementById(popupId + '_iframe').innerHTML = data;

			$('#' + popupId + '_iframe').removeClass('hide');

			// para en envio de formularios dentro del modalbox
			LibPopupModal._repareForms(popupId);

			htmlRenderLinks('#' + popupId + '_iframe');
		},
		error: function()
		{
			$('#' + popupId + '_iframe').html('No se pudo cargar');
		},
		complete: function()
		{
			// disabled history back  button
			if($('#' + popupId).find('.popupHistory li').size() == 0)
			{
				$('#' + popupId).find('.popupModalHeaderHistory').css('opacity', 0.4);
				$('#' + popupId).find('.popupModalHeaderHistory a').css('cursor', 'default');
			}
			else
			{
				$('#' + popupId).find('.popupModalHeaderHistory').css('opacity', 1);
				$('#' + popupId).find('.popupModalHeaderHistory a').css('cursor', 'pointer');
			}

			LibPopupModal.resizePanel(popupId);
			if(zindex == undefined || (zindex != undefined && zindex == false))
			{
				//LibPopupModal.indexTop(popupId);
			}
			
			translateAll(getCookie('language'));
		}
	});

	// recargamos para las notificaciones
	if(popupId == 'popupModal_gestor_correos' || popupId == 'popupModal_foro' || popupId == 'popupModal_archivador_electronico')
	{
		if($('#popupModal_foro').size() == 1)
		{
			LibPopupModal._ajaxLoad('popupModal_tareas_tutor', $('#popupModal_tareas_tutor').attr('data-url'), true, false);
		}
		else if($('#popupModal_tareas_pendientes').size() == 1)
		{
			LibPopupModal._ajaxLoad('popupModal_tareas_pendientes', $('#popupModal_tareas_pendientes').attr('data-url'), true, false);
		}
		else if($('#popupModal_archivador_electronico').size() == 1)
		{
			LibPopupModal._ajaxLoad('popupModal_tareas_tutor', $('#popupModal_tareas_tutor').attr('data-url'), true, false);
		}
	}
};

LibPopupModal.historyBack = function(popupId)
{
	var count = $('#' + popupId).find('.popupHistory li').size();

	if(count > 0)
	{
		//count = window.historyPosCurrent;

		if($('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').size() == 1)
		{
			var url = $('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').html();

			LibPopupModal._ajaxLoad(popupId, url);

			//window.historyPosCurrent--;

			$('#' + popupId).attr('data-url', url);
			//$('#' + popupId).attr('data-url-current', url);

			//window.urlAnchor = url;

			$('#' + popupId).find('.popupHistory li[data-pos="' + count + '"]').remove();
		}
	}
};

LibPopupModal.addHistory = function(popupId, url)
{
	var count = $('#' + popupId).find('.popupHistory li').size();

	if($('#' + popupId).find('.popupHistory').find('li').last().html() != url)
	{
		$('#' + popupId).find('.popupHistory').append('<li data-pos="' + (count + 1) + '">' + url + '</li>');
	}
};

LibPopupModal._repareForms = function(popupId)
{
	// para en envio de formularios dentro del modalbox
	$('#' + popupId + '_iframe form').each(function(index)
	{
		var formAction = $(this).attr('action');
		//var url = /*$('head base').attr('href') + */ $('#' + popupId).attr('data-url-current');
		var url = $('#' + popupId).find('.popupModalUrlCurrent').html();

		if(formAction == undefined || formAction == null || formAction == '' || formAction == '#')
		{
			formAction = /*'action/' + */url;
		}
		else
		{
			console.log(formAction);
			//formAction = /*'action/' + */formAction;
		}

		$(this).attr('action', formAction);
		$(this).attr('target', popupId + '_form_iframe');

		$(this).append('<div><input type="hidden" name="url-referer" value="' + url + '" /></div>');

		//$("input:checkbox, input:radio, textarea, select,").uniform();

		$('#' + popupId).attr('data-url', url);
		//window.historyPosCurrent++;

		/*$(this).submit(function(e)
		{
			//var htmlOjData = e.target;

			//console.log(e);
		});*/

		translateAll(getCookie('language'));

	 });
};

LibPopupModal.reload = function(popupId)
{
	/*$('#' + popupId).find('.popupModalContent iframe').addClass('hide');
	$('#' + popupId).find('.popupModalLoader').removeClass('hide');

	// cuando se cargue el contenido del iframe
	$('#' + popupId).find('.popupModalContent iframe').load(function()
	{
		$('#' + popupId).find('.popupModalLoader').addClass('hide');

		$(this).removeClass('hide');

		// eliminamos el evento
		$('#' + popupId).find('.popupModalContent iframe').unbind('load');
	});*/

	//LibPopupModal._ajaxLoad(popupId, $('#' + popupId).attr('data-url-current'));

	LibPopupModal._ajaxLoad(popupId, $('#' + popupId).attr('data-url'), true);

	//document.getElementById(popupId + '_iframe').contentDocument.location.reload();
};

LibPopupModal.maximize = function(popupId)
{
	$('#' + popupId).css('width', $(window).width() + 'px');
	//$('#' + popupId).find('iframe').css('height', ($(window).height() - 130) + 'px');

	//var left = ($(window).width() - $('#' + popupId).width()) / 2;
	//$('#' + popupId).css('left', left + 'px');

	var heightModal = $(window).height();

	$('#' + popupId + '_wrapper_aux').css({
		width  : '100%',
		height : (heightModal - 60) + 'px'
	});

	$('#' + popupId).css({
		top    : '0px',
		left   : '0px',
		right  : '0px',
		height : (heightModal + 10) + 'px'
	});

	LibPopupModal.resizePanel(popupId);

	$('#' + popupId).resizable({disabled : true}).draggable({disabled : true});

	// cambia los botones
	$('#' + popupId).find('.popupModalHeaderBtn_maximize').addClass('hide');
	$('#' + popupId).find('.popupModalHeaderBtn_restore').removeClass('hide');

	if(popupId == 'popupModal_gestor_correos')
	{
		gestorCorreoReedimensionar();
	}
	else if(popupId == 'popupModal_foro')
	{
		foroReedimensionar();
	}

	//$('#' + popupId).attr('data-height', heightModal);
};

LibPopupModal.minimize = function(popupId, title)
{
	var style = $('#' + popupId).attr('style');

	/*var options = {
			to: '#popupBtn_' + popupId,
			className: 'ui-effects-transfer'
	};

	$('#' + popupId).effect('transfer', options, 500, function()
	{
		$('#' + popupId).addClass('hide');
		$('#' + popupId).attr('style', style);
	});
	*/
	$('#' + popupId).addClass('hide');
	$('#' + popupId).attr('style', style);
	//$('#' + popupId).addClass('hide');

	//LibPopupModal.resizePanel(popupId);

	// a_ade boton de popup
	//$('#popupsButtons').append('<div id="popupBtn_' + popupId + '" class="popupButtonFixed"><a href="#" onclick="LibPopupModal.restore(this, \'' + popupId + '\'); return false;" title="">' + title.replace(/_/g, ' ') + ' <img src="imagenes/popupModal/window_restore.png" alt="" /></a></div>');
};

LibPopupModal.restore = function(element, popupId, heightDefault, widthDefault)
{
	// restaurar desde minimizado
	if(element != null)
	{
		//$(element).remove();
		$('#' + popupId).removeClass('hide');
	}
	// restarurar desde maximizado
	else
	{
		$('#' + popupId).css('width', widthDefault + 'px');
		//$('#' + popupId).find('iframe').css('height', heightDefault + 'px');

		var left = ($(window).width() - $('#' + popupId).width()) / 2;
		$('#' + popupId).css('left', left + 'px');

		$('#' + popupId).css({
			top    : '100px',
			height : heightDefault + 'px'
		});

		$('#' + popupId).resizable({disabled : false}).draggable({disabled : false});

		$('#' + popupId + '_wrapper_aux').css({
			width  : '100%',
			height : (heightDefault - 60) + 'px'
		});

		// cambia los botones
		$('#' + popupId).find('.popupModalHeaderBtn_restore').addClass('hide');
		$('#' + popupId).find('.popupModalHeaderBtn_maximize').removeClass('hide');
	}

	LibPopupModal.resizePanel(popupId);

	LibPopupModal.indexTop(popupId);

	if(popupId == 'popupModal_gestor_correos')
	{
		gestorCorreoReedimensionar();
	}
	else if(popupId == 'popupModal_foro')
	{
		foroReedimensionar();
	}
};

LibPopupModal.close = function(popupId)
{
	$('#' + popupId).unbind('draggable').unbind('resizable');
	$('#' + popupId + '_iframe a').die('click');
	$('#' + popupId).find('form').die('submit');
	$('#' + popupId + '_form_iframe').unbind('load');

	LibPopupModal.closeLogAccess(popupId);
	//$('#' + popupId).find('iframe').attr('src', '');
	$('#' + popupId).remove();

	$('#popupsButtons').find('#popupBtn_' + popupId).remove();
};

LibPopupModal.closeAll = function()
{
	//Cierro los popup y guardo las fechas
	$('.popupModal').each(function(index)
	{
		LibPopupModal.closeLogAccess($(this).attr('id'));
	});

	$('.popupModal').remove();
	$('.popupButtonFixed').remove();
};

LibPopupModal.sendLogAccess = function(popupId)
{
	if($('#' + popupId).find('.popupModalModuleName').size() == 1)
	{
		var module = $('#' + popupId).find('.popupModalModuleName').html();

		$.ajax({
			url : 'index.php?log_acceso=get&m=' + module
		}).success(function(data)
		{
			$('#' + popupId).attr('data-id-log-access', data);
		});
	}
};

LibPopupModal.closeLogAccess = function(popupId)
{
	$.ajax({
		url : 'index.php?idAcceso=' + $('#' + popupId).attr('data-id-log-access'),
		async : false
	});
};


LibPopupModal.closeLogSession = function()
{
	$.ajax({
		url : 'index.php?closeIdSession=true',
		async : false
	});
};

LibPopupModal.indexTop = function(popupId)
{
	$('.popupModal').css('z-index', 20);
	$('#' + popupId).css('z-index', parseInt($('#' + popupId).css('z-index')) + 1);
};

LibPopupModal.resizePanel = function(popupId)
{
	if($('#' + popupId).find('.panelTabContent').size() == 1)
	{
		var widthModal = $('#' + popupId).find('.general_popup').width();
		var heightModal = $('#' + popupId).find('.general_popup').height();

		var widthPanelMenu = $('#' + popupId).find('.panelTabMenu').width();
		var heightPanelMenu = $('#' + popupId).find('.panelTabMenu').height();

		/*var heightPanelMenu = 0;
		$('#' + popupId).find('.panelTabMenu > div').each(function(i)
		{
			heightPanelMenu += $(this).height();
	   	});*/

		var positionModal = $('#' + popupId).find('.general_popup').offset();

		if($('#' + popupId).find('.panelTabMenu').size() == 1)
		{
			var positionPanelContent = $('#' + popupId).find('.panelTabMenu').offset();
			positionPanelContent = (positionPanelContent.top - positionModal.top) + 55;
		}
		else
		{
			var positionPanelContent = positionModal.top + 10;
		}

		$('#' + popupId).find('.panelTabContent').css('width', (widthModal - widthPanelMenu - 40 - 64 - 8) + 'px');
		//console.log(widthModal + ' - ' + widthPanelMenu);

		var heightFinal = heightModal - positionPanelContent;
		//console.log(positionPanelContent.top - positionModal.top + ' - ' + positionModal.top + ' ' + positionPanelContent.top);

		//var heightFinal = heightModal - 100;
		if(heightFinal < heightPanelMenu)
		{
			heightFinal = heightPanelMenu;
		}

		$('#' + popupId).find('.panelTabContent').css('height', heightFinal + 'px');

		setTimeout('LibPopupModal.resizePanel(\'' + popupId + '\')', 500);
	}
};

// al hacer click en una ventana modal (menos iframe)
$('.popupModal').live('click', function()
{
	$('.popupModal').css('z-index', 20);
	$(this).css('z-index', parseInt($(this).css('z-index')) + 1);
});

// al hacer unload en la web
window.onbeforeunload = function()
{
	LibPopupModal.closeLogSession();
	LibPopupModal.closeAll();
};


function viewInfo()
{
	$('.contentInfoPopupModal').fadeIn(1200);
}