<?php
	
	header('Content-Type: text/html; charset=utf-8');

	if( ! ini_get('date.timezone') ){
	    date_default_timezone_set('GMT');
	}

	$hostname = '87.106.222.75';
	$database = 'u75088563_campusaulainteractivainfantil';
	$username = 'user_u75088563';
	$password = 'PBD_u75088563';

	$conn  = new mysqli($hostname, $username, $password, $database);
	if ($conn->connect_errno) {

		die("Fallo la conexión a MySQL: (" . $conn ->mysqli_connect_errno(). ") " . $conn ->mysqli_connect_error());

	} else{

		$hoy = date("Y-m-d");
		$array = array();

		$sql = "SELECT * FROM acuerdo_colaboracion_ase";
		$resultado = $conn ->query($sql);

		// Para el control del Script para su salida por pantalla
		$contador = 0;
		$output ='';

		while ($alumno = $resultado->fetch_assoc()) {
			if(!empty($alumno['fecha_fin'])){

				if(strpos($alumno['fecha_fin'],"-")){
					$fecha = DateTime::createFromFormat('d-m-Y', $alumno['fecha_fin']); 
				}elseif(strpos($alumno['fecha_fin'],"/")){
					$fecha = DateTime::createFromFormat('d/m/Y', $alumno['fecha_fin']);
				}

				if(!empty($fecha)){
					$fechaDB = $fecha->format('Y-m-d');
					
					if(strtotime($hoy) > strtotime($fechaDB)){

						$sql = "UPDATE acuerdo_colaboracion_ase SET estado_clave=0, pendiente=0, idcurso_matriculado=NULL, fecha_matriculado=NULL, fecha_fin=NULL WHERE id=".$alumno['id'];

						if ($conn->query($sql) === TRUE) {
							$contador ++;
						    $output .= "* Reseteado estado de la clave para el alumno " . $alumno['id_alumno'] . " con fecha de finalización el " . $alumno['fecha_fin']. "\n";
						}
					}
					
				}
			}
		}

		$firstOutput = "";
		if($contador == 0){
			$firstOutput .= "No hay ningún curso que finalizar.\n";
		}else{
			$firstOutput .= "Se han finalizado " . $contador . " cursos.\n";
		}
	}

	echo $firstOutput . $output;

	mysqli_close($conn);


	// Enviamos un correo para saber el resultado
	$cabecera = "From: CRON" . "\r\n" .
				"Content-Type: text/html; charset=UTF-8";
	$mensaje = $firstOutput . $output;

	mail('raulrodriguez@ibericasur.com', 'CRON Campus - Finalizar Cursos', $mensaje, $cabecera);

?>